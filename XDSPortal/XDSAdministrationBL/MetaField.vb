Imports sembleWare.Runtime
Imports System
Public Class MetaField 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly MetaFieldName As New StringElement("MetaFieldName", Me, True, True, True, True, True, 250, "Field Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaFieldDesc As New StringElement("MetaFieldDesc", Me, False, True, True, True, True, 250, "Field Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataTypeInd As New IndicatorElement("DataTypeInd", Me, False, False, True, True, True, True, "Data Type", Nothing, MetaField.DataTypeIndOptions, Nothing)
  Public ReadOnly MetaTable As Relationship = New MetaTableRelationship("MetaTable", "Table", RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", "Database", RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _MetaTable As MetaTableRelationship = MetaTable
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDataTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DataTypeIndOptions() As IndicatorOptions
    Get
      If moDataTypeIndOptions Is Nothing Then
        moDataTypeIndOptions = New IndicatorOptions
        moDataTypeIndOptions.Add("S", "String")
        moDataTypeIndOptions.Add("N", "Numeric")
        moDataTypeIndOptions.Add("B", "Boolean")
        moDataTypeIndOptions.Add("D", "Date")
      End If
      Return moDataTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("MetaField", ApplicationSettings)
    _MetaTable._MetaTableName.LocalElement = _MetaTableName
    _MetaTable._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckMetaDatabaseType()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository
        Throw New BusinessRuleException("You may not add Tables as this Meta Database Type is not ""Credit Bureau""!")
    End Select
  End Sub


#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class MetaFieldRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _MetaFieldName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaTableName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New MetaField(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _MetaFieldName.ForeignElement = CType(Instance, MetaField).MetaFieldName
    _MetaTableName.ForeignElement = CType(Instance, MetaField)._MetaTableName
    _MetaDatabaseCode.ForeignElement = CType(Instance, MetaField)._MetaDatabaseCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaField")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, False, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 1, True, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldDesc", "[A0MetaField].[MetaFieldDesc]", "MetaFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description"))
    oList.Columns.Add(New ListColumn("DataTypeInd", "[A0MetaField].[DataTypeInd]", "DataTypeInd", DataType.String, Nothing, False, 0, True, 100, "Data Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaField]  [A0MetaField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaField")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, False, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 1, True, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldDesc", "[A0MetaField].[MetaFieldDesc]", "MetaFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description"))
    oList.Columns.Add(New ListColumn("DataTypeInd", "[A0MetaField].[DataTypeInd]", "DataTypeInd", DataType.String, Nothing, False, 0, True, 100, "Data Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaField]  [A0MetaField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaField")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, False, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 1, True, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldDesc", "[A0MetaField].[MetaFieldDesc]", "MetaFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description"))
    oList.Columns.Add(New ListColumn("DataTypeInd", "[A0MetaField].[DataTypeInd]", "DataTypeInd", DataType.String, Nothing, False, 0, True, 100, "Data Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaField]  [A0MetaField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function





  Public Function DeploymentForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaField")
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 0, True, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, True, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, True, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 0, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, True, 100, "Database Type"))
    oList.Columns.Add(New ListColumn("DataTypeInd", "[A0MetaField].[DataTypeInd]", "DataTypeInd", DataType.String, Nothing, False, 0, True, 100, "Data Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "([MetaField]  [A0MetaField]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0MetaField].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
