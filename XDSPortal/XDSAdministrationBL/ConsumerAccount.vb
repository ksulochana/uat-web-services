Imports sembleWare.Runtime
Imports System
Public Class ConsumerAccount 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerAccountID As New IdentityElement("ConsumerAccountID", Me, True, Nothing, Nothing)
  Public ReadOnly AccountNo As New StringElement("AccountNo", Me, False, True, True, True, False, 25, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubAccountNo As New StringElement("SubAccountNo", Me, False, True, True, True, False, 4, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, "Gender", Nothing, ConsumerAccount.GenderIndOptions, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly BranchCode As New StringElement("BranchCode", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdName As New StringElement("ThirdName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress1 As New StringElement("ResidentialAddress1", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress2 As New StringElement("ResidentialAddress2", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress3 As New StringElement("ResidentialAddress3", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress4 As New StringElement("ResidentialAddress4", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialPostalCode As New StringElement("ResidentialPostalCode", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddressTypeInd As New IndicatorElement("ResidentialAddressTypeInd", Me, False, False, True, True, True, False, "Residential Address Type", Nothing, ConsumerAccount.ResidentialAddressTypeIndOptions, Nothing)
  Public ReadOnly PostalAddress1 As New StringElement("PostalAddress1", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalAddress2 As New StringElement("PostalAddress2", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalAddress3 As New StringElement("PostalAddress3", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalAddress4 As New StringElement("PostalAddress4", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalCode As New StringElement("PostalCode", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AccountTypeInd As New IndicatorElement("AccountTypeInd", Me, False, False, True, True, True, False, "Account Type", Nothing, ConsumerAccount.AccountTypeIndOptions, Nothing)
  Public ReadOnly AccountOpenedDate As New DateTimeElement("AccountOpenedDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly DeferredPaymentDate As New DateTimeElement("DeferredPaymentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly LastPaymentDate As New DateTimeElement("LastPaymentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly OpeningBalanceAmt As New DecimalElement("OpeningBalanceAmt", Me, False, True, True, True, False, "Opening Balance Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly OpeningBalanceTypeInd As New IndicatorElement("OpeningBalanceTypeInd", Me, False, False, True, True, True, False, "Opening Balance Type", Nothing, ConsumerAccount.OpeningBalanceTypeIndOptions, Nothing)
  Public ReadOnly CurrentBalanceAmt As New DecimalElement("CurrentBalanceAmt", Me, False, True, True, True, False, "Current Balance Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly CurrentBalanceTypeInd As New IndicatorElement("CurrentBalanceTypeInd", Me, False, False, True, True, True, False, "Current Balance Type", Nothing, ConsumerAccount.CurrentBalanceTypeIndOptions, Nothing)
  Public ReadOnly BalanceOverdueAmt As New DecimalElement("BalanceOverdueAmt", Me, False, True, True, True, False, "Balance Overdue Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly BalanceOverdueTypeInd As New IndicatorElement("BalanceOverdueTypeInd", Me, False, False, True, True, True, False, "Balance Overdue Type", Nothing, ConsumerAccount.BalanceOverdueTypeIndOptions, Nothing)
  Public ReadOnly MonthlyInstalmentAmt As New DecimalElement("MonthlyInstalmentAmt", Me, False, True, True, True, False, "Monthly Instalment Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly LoadTypeInd As New IndicatorElement("LoadTypeInd", Me, False, False, True, True, True, False, "Load Type", Nothing, ConsumerAccount.LoadTypeIndOptions, Nothing)
  Public ReadOnly MonthsInArrears As New IntegerElement("MonthsInArrears", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly InstallmentTerms As New StringElement("InstallmentTerms", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WriteOffDate As New DateTimeElement("WriteOffDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly OldSupplierBranchCode As New StringElement("OldSupplierBranchCode", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldAccountNo As New StringElement("OldAccountNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldSubAccountNo As New StringElement("OldSubAccountNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldSupplierReferenceNo As New StringElement("OldSupplierReferenceNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly HomeTelephoneNo As New StringElement("HomeTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WorkTelephoneNo As New StringElement("WorkTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmployerDetail As New StringElement("EmployerDetail", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncomeAmt As New DecimalElement("IncomeAmt", Me, False, True, True, True, False, "Income Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly AccountSoldThirdPartyInd As New IndicatorElement("AccountSoldThirdPartyInd", Me, False, False, True, True, True, False, "Account Sold To Third Party", Nothing, ConsumerAccount.AccountSoldThirdPartyIndOptions, Nothing)
  Public ReadOnly NoOfParticipantsInJointLoan As New IntegerElement("NoOfParticipantsInJointLoan", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdPartyName As New StringElement("ThirdPartyName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerAccount.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IsVerifiedYN As New BooleanElement("IsVerifiedYN", Me, False, True, True, True, True, "Is Verified?", Nothing, False, "Yes;No")
  Public ReadOnly VerifiedDate As New DateTimeElement("VerifiedDate", Me, False, True, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Title As Relationship = New TitleRelationship("Title", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly DataStatus As Relationship = New DataStatusRelationship("DataStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly AccountType As Relationship = New AccountTypeRelationship("AccountType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly RepaymentFrequency As Relationship = New RepaymentFrequencyRelationship("RepaymentFrequency", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly PaymentType As Relationship = New PaymentTypeRelationship("PaymentType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly OwnershipType As Relationship = New OwnershipTypeRelationship("OwnershipType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly LoanEndUse As Relationship = New LoanEndUseRelationship("LoanEndUse", "Loan End-Use", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly IncomeFrequency As Relationship = New IncomeFrequencyRelationship("IncomeFrequency", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _TitleCode As New StringElement("TitleCode", Me, False)
  Public ReadOnly _DataStatusCode As New StringElement("DataStatusCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _AccountTypeCode As New StringElement("AccountTypeCode", Me, False)
  Public ReadOnly _RepaymentFrequencyCode As New StringElement("RepaymentFrequencyCode", Me, False)
  Public ReadOnly _PaymentTypeCode As New StringElement("PaymentTypeCode", Me, False)
  Public ReadOnly _OwnershipTypeCode As New StringElement("OwnershipTypeCode", Me, False)
  Public ReadOnly _LoanEndUseCode As New StringElement("LoanEndUseCode", Me, False)
  Public ReadOnly _IncomeFrequencyCode As New StringElement("IncomeFrequencyCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Title As TitleRelationship = Title
  Public ReadOnly _DataStatus As DataStatusRelationship = DataStatus
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _AccountType As AccountTypeRelationship = AccountType
  Public ReadOnly _RepaymentFrequency As RepaymentFrequencyRelationship = RepaymentFrequency
  Public ReadOnly _PaymentType As PaymentTypeRelationship = PaymentType
  Public ReadOnly _OwnershipType As OwnershipTypeRelationship = OwnershipType
  Public ReadOnly _LoanEndUse As LoanEndUseRelationship = LoanEndUse
  Public ReadOnly _IncomeFrequency As IncomeFrequencyRelationship = IncomeFrequency
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moResidentialAddressTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ResidentialAddressTypeIndOptions() As IndicatorOptions
    Get
      If moResidentialAddressTypeIndOptions Is Nothing Then
        moResidentialAddressTypeIndOptions = New IndicatorOptions
        moResidentialAddressTypeIndOptions.Add("O", "Owner")
        moResidentialAddressTypeIndOptions.Add("T", "Tenant")
      End If
      Return moResidentialAddressTypeIndOptions
    End Get
  End Property
  Private Shared moAccountTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccountTypeIndOptions() As IndicatorOptions
    Get
      If moAccountTypeIndOptions Is Nothing Then
        moAccountTypeIndOptions = New IndicatorOptions
        moAccountTypeIndOptions.Add("I", "Installment")
        moAccountTypeIndOptions.Add("R", "Revolving")
        moAccountTypeIndOptions.Add("O", "Open")
        moAccountTypeIndOptions.Add("C", "Credit Card")
        moAccountTypeIndOptions.Add("P", "Personal")
        moAccountTypeIndOptions.Add("H", "Home Loans")
        moAccountTypeIndOptions.Add("S", "Short Term Insurance")
        moAccountTypeIndOptions.Add("L", "Long Term Insurance")
      End If
      Return moAccountTypeIndOptions
    End Get
  End Property
  Private Shared moOpeningBalanceTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property OpeningBalanceTypeIndOptions() As IndicatorOptions
    Get
      If moOpeningBalanceTypeIndOptions Is Nothing Then
        moOpeningBalanceTypeIndOptions = New IndicatorOptions
        moOpeningBalanceTypeIndOptions.Add("H", "Highest Balance")
        moOpeningBalanceTypeIndOptions.Add("C", "Credit Limit")
        moOpeningBalanceTypeIndOptions.Add("O", "Opening Balance")
      End If
      Return moOpeningBalanceTypeIndOptions
    End Get
  End Property
  Private Shared moCurrentBalanceTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property CurrentBalanceTypeIndOptions() As IndicatorOptions
    Get
      If moCurrentBalanceTypeIndOptions Is Nothing Then
        moCurrentBalanceTypeIndOptions = New IndicatorOptions
        moCurrentBalanceTypeIndOptions.Add("D", "Debit")
        moCurrentBalanceTypeIndOptions.Add("C", "Credit")
      End If
      Return moCurrentBalanceTypeIndOptions
    End Get
  End Property
  Private Shared moBalanceOverdueTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property BalanceOverdueTypeIndOptions() As IndicatorOptions
    Get
      If moBalanceOverdueTypeIndOptions Is Nothing Then
        moBalanceOverdueTypeIndOptions = New IndicatorOptions
        moBalanceOverdueTypeIndOptions.Add("D", "Debit")
        moBalanceOverdueTypeIndOptions.Add("C", "Credit")
      End If
      Return moBalanceOverdueTypeIndOptions
    End Get
  End Property
  Private Shared moLoadTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property LoadTypeIndOptions() As IndicatorOptions
    Get
      If moLoadTypeIndOptions Is Nothing Then
        moLoadTypeIndOptions = New IndicatorOptions
        moLoadTypeIndOptions.Add("P", "PP")
        moLoadTypeIndOptions.Add("A", "Adverse")
        moLoadTypeIndOptions.Add("B", "Both")
      End If
      Return moLoadTypeIndOptions
    End Get
  End Property
  Private Shared moAccountSoldThirdPartyIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccountSoldThirdPartyIndOptions() As IndicatorOptions
    Get
      If moAccountSoldThirdPartyIndOptions Is Nothing Then
        moAccountSoldThirdPartyIndOptions = New IndicatorOptions
        moAccountSoldThirdPartyIndOptions.Add("01", "Yes")
        moAccountSoldThirdPartyIndOptions.Add("02", "No")
      End If
      Return moAccountSoldThirdPartyIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerAccount", ApplicationSettings)
    _Title._TitleCode.LocalElement = _TitleCode
    _DataStatus._DataStatusCode.LocalElement = _DataStatusCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _AccountType._AccountTypeCode.LocalElement = _AccountTypeCode
    _RepaymentFrequency._RepaymentFrequencyCode.LocalElement = _RepaymentFrequencyCode
    _PaymentType._PaymentTypeCode.LocalElement = _PaymentTypeCode
    _OwnershipType._OwnershipTypeCode.LocalElement = _OwnershipTypeCode
    _LoanEndUse._LoanEndUseCode.LocalElement = _LoanEndUseCode
    _IncomeFrequency._IncomeFrequencyCode.LocalElement = _IncomeFrequencyCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    XDSAdministrationBL.Consumer.ValidateIDNo(Me.IDNo.Value, ApplicationSettings)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ConsumerAccountRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerAccountID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerAccount(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerAccountID.ForeignElement = CType(Instance, ConsumerAccount).ConsumerAccountID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerAccount)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerAccount")
    oList.Columns.Add(New ListColumn("ConsumerAccountID", "[A0ConsumerAccount].[ConsumerAccountID]", "ConsumerAccountID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Account ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerAccount].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerAccount].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerAccount].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0ConsumerAccount].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A0ConsumerAccount].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 2, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("SubAccountNo", "[A0ConsumerAccount].[SubAccountNo]", "SubAccountNo", DataType.String, Nothing, False, 0, True, 100, "Sub Account No"))
    oList.Columns.Add(New ListColumn("RecordStatusInd", "[A0ConsumerAccount].[RecordStatusInd]", "RecordStatusInd", DataType.String, Nothing, False, 0, False, 100, "Record Status"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([ConsumerAccount]  [A0ConsumerAccount]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0ConsumerAccount].[SubscriberID] = [A1Subscriber].[SubscriberID])" + _
           "    join [Consumer]  [A2Consumer] on [A0ConsumerAccount].[ConsumerID] = [A2Consumer].[ConsumerID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A2Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("RecordStatusInd1", "[A0ConsumerAccount].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerAccount")
    oList.Columns.Add(New ListColumn("ConsumerAccountID", "[A0ConsumerAccount].[ConsumerAccountID]", "ConsumerAccountID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Account ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerAccount].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerAccount].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerAccount].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A0ConsumerAccount].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 1, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("SubAccountNo", "[A0ConsumerAccount].[SubAccountNo]", "SubAccountNo", DataType.String, Nothing, False, 0, True, 100, "Sub Account No"))
    oList.Columns.Add(New ListColumn("RecordStatusInd", "[A0ConsumerAccount].[RecordStatusInd]", "RecordStatusInd", DataType.String, Nothing, False, 0, False, 100, "Record Status"))
    oList.Columns.Add(New ListColumn("AccountTypeInd", "[A0ConsumerAccount].[AccountTypeInd]", "AccountTypeInd", DataType.String, Nothing, False, 0, True, 100, "Account Type"))
    oList.Columns.Add(New ListColumn("AccountOpenedDate", "[A0ConsumerAccount].[AccountOpenedDate]", "AccountOpenedDate", DataType.DateTime, "d", False, 0, True, 100, "Account Opened Date"))
    oList.Columns.Add(New ListColumn("OpeningBalanceTypeInd", "[A0ConsumerAccount].[OpeningBalanceTypeInd]", "OpeningBalanceTypeInd", DataType.String, Nothing, False, 0, True, 100, "Opening Balance Type"))
    oList.Columns.Add(New ListColumn("OpeningBalanceAmt", "[A0ConsumerAccount].[OpeningBalanceAmt]", "OpeningBalanceAmt", DataType.Decimal, "c", False, 0, True, 100, "Opening Balance Amount"))
    oList.Columns.Add(New ListColumn("CurrentBalanceAmt", "[A0ConsumerAccount].[CurrentBalanceAmt]", "CurrentBalanceAmt", DataType.Decimal, "c", False, 0, True, 100, "Current Balance Amount"))
    oList.Columns.Add(New ListColumn("BalanceOverdueTypeInd", "[A0ConsumerAccount].[BalanceOverdueTypeInd]", "BalanceOverdueTypeInd", DataType.String, Nothing, False, 0, True, 100, "Balance Overdue Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ConsumerAccount]  [A0ConsumerAccount]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
