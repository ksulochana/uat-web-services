Imports sembleWare.Runtime
Imports System
Public Class XDSAdministrationRoot 'sembleWare: Part
  Inherits Part

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Subscriber_OwnMany As Relationship = New SubscriberRelationship("Subscriber", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly FileFormat_OwnMany As Relationship = New FileFormatRelationship("FileFormat", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriber_OwnMany As Relationship = New SPSubscriberRelationship("SPSubscriber", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Consumer_OwnMany As Relationship = New ConsumerRelationship("Consumer", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPConsumer_OwnMany As Relationship = New SPConsumerRelationship("SPConsumer", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Title_OwnMany As Relationship = New TitleRelationship("Title", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly MaritalStatus_OwnMany As Relationship = New MaritalStatusRelationship("MaritalStatus", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Commercial_OwnMany As Relationship = New CommercialRelationship("Commercial", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly DataStatus_OwnMany As Relationship = New DataStatusRelationship("DataStatus", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Period_OwnMany As Relationship = New PeriodRelationship("Period", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPCommercial_OwnMany As Relationship = New SPCommercialRelationship("SPCommercial", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly MetaDatabase_OwnMany As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberProfile_OwnMany As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SecurityModule_OwnMany As Relationship = New SecurityModuleRelationship("SecurityModule", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemUser_OwnMany As Relationship = New SystemUserRelationship("SystemUser", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Role_OwnMany As Relationship = New RoleRelationship("Role", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SecurityForm_OwnMany As Relationship = New SecurityFormRelationship("SecurityForm", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemUserRole_OwnMany As Relationship = New SystemUserRoleRelationship("SystemUserRole", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemUserLogin_OwnMany As Relationship = New SystemUserLoginRelationship("SystemUserLogin", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ViewState_OwnMany As Relationship = New ViewStateRelationship("ViewState", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderThread_OwnMany As Relationship = New LoaderThreadRelationship("LoaderThread", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPLoaderProcess_OwnMany As Relationship = New SPLoaderProcessRelationship("SPLoaderProcess", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemDefault_OwnMany As Relationship = New SystemDefaultRelationship("SystemDefault", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Loader_OwnMany As Relationship = New LoaderRelationship("Loader", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderProcessType_OwnMany As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderProcess_OwnMany As Relationship = New LoaderProcessRelationship("LoaderProcess", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPDataProcedure_OwnMany As Relationship = New SPDataProcedureRelationship("SPDataProcedure", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPLoaderMatchingEngine_OwnMany As Relationship = New SPLoaderMatchingEngineRelationship("SPLoaderMatchingEngine", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberProfile_OwnMany As Relationship = New SPSubscriberProfileRelationship("SPSubscriberProfile", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSystemUser_OwnMany As Relationship = New SPSystemUserRelationship("SPSystemUser", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSecurityForm_OwnMany As Relationship = New SPSecurityFormRelationship("SPSecurityForm", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly vwLoaderStagingTable_OwnMany As Relationship = New vwLoaderStagingTableRelationship("vwLoaderStagingTable", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly AuditTrailControl_OwnMany As Relationship = New AuditTrailControlRelationship("AuditTrailControl", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPAuditTrailControl_OwnMany As Relationship = New SPAuditTrailControlRelationship("SPAuditTrailControl", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPLoaderProcessHistory_OwnMany As Relationship = New SPLoaderProcessHistoryRelationship("SPLoaderProcessHistory", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly NoteType_OwnMany As Relationship = New NoteTypeRelationship("NoteType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly vwDeploymentFileFormatMetaGroup_OwnMany As Relationship = New vwDeploymentFileFormatMetaGroupRelationship("vwDeploymentFileFormatMetaGroup", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly vwDeploymentFileFormatMetaGroupField_OwnMany As Relationship = New vwDeploymentFileFormatMetaGroupFieldRelationship("vwDeploymentFileFormatMetaGroupField", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly FileFormatType_OwnMany As Relationship = New FileFormatTypeRelationship("FileFormatType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPLoaderProcessMetaDatabase_OwnMany As Relationship = New SPLoaderProcessMetaDatabaseRelationship("SPLoaderProcessMetaDatabase", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly DataProcedure_OwnMany As Relationship = New DataProcedureRelationship("DataProcedure", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderMatchingEngine_OwnMany As Relationship = New LoaderMatchingEngineRelationship("LoaderMatchingEngine", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderProcessStatus_OwnMany As Relationship = New LoaderProcessStatusRelationship("LoaderProcessStatus", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly FileFormatDeployment_OwnMany As Relationship = New FileFormatDeploymentRelationship("FileFormatDeployment", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderUploadFile_OwnMany As Relationship = New LoaderUploadFileRelationship("LoaderUploadFile", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportingServicesServer_OwnMany As Relationship = New ReportingServicesServerRelationship("ReportingServicesServer", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportingServicesReport_OwnMany As Relationship = New ReportingServicesReportRelationship("ReportingServicesReport", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPReportingServicesReport_OwnMany As Relationship = New SPReportingServicesReportRelationship("SPReportingServicesReport", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Product_OwnMany As Relationship = New ProductRelationship("Product", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberRegistration_OwnMany As Relationship = New SubscriberRegistrationRelationship("SubscriberRegistration", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberRegistration_OwnMany As Relationship = New SPSubscriberRegistrationRelationship("SPSubscriberRegistration", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ProductType_OwnMany As Relationship = New ProductTypeRelationship("ProductType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly EnquiryMatchingEngine_OwnMany As Relationship = New EnquiryMatchingEngineRelationship("EnquiryMatchingEngine", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberBillingGroup_OwnMany As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemUserProfile_OwnMany As Relationship = New SystemUserProfileRelationship("SystemUserProfile", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly FileFormatDeploymentMetaLoaderProcedure_OwnMany As Relationship = New FileFormatDeploymentMetaLoaderProcedureRelationship("FileFormatDeploymentMetaLoaderProcedure", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemUserForgotPassword_OwnMany As Relationship = New SystemUserForgotPasswordRelationship("SystemUserForgotPassword", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly NotificationTemplate_OwnMany As Relationship = New NotificationTemplateRelationship("NotificationTemplate", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly NotificationHistory_OwnMany As Relationship = New NotificationHistoryRelationship("NotificationHistory", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly NotificationQueue_OwnMany As Relationship = New NotificationQueueRelationship("NotificationQueue", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberProfileProduct_OwnMany As Relationship = New SPSubscriberProfileProductRelationship("SPSubscriberProfileProduct", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSystemUserProfileProduct_OwnMany As Relationship = New SPSystemUserProfileProductRelationship("SPSystemUserProfileProduct", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberLogin_OwnMany As Relationship = New SubscriberLoginRelationship("SubscriberLogin", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberForgotPassword_OwnMany As Relationship = New SubscriberForgotPasswordRelationship("SubscriberForgotPassword", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberEnquiry_OwnMany As Relationship = New SPSubscriberEnquiryRelationship("SPSubscriberEnquiry", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberEnquiryConsumer_OwnMany As Relationship = New SPSubscriberEnquiryConsumerRelationship("SPSubscriberEnquiryConsumer", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberAuthentication_OwnMany As Relationship = New SPSubscriberAuthenticationRelationship("SPSubscriberAuthentication", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberTrace_OwnMany As Relationship = New SPSubscriberTraceRelationship("SPSubscriberTrace", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SIC_OwnMany As Relationship = New SICRelationship("SIC", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Country_OwnMany As Relationship = New CountryRelationship("Country", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CommercialStatus_OwnMany As Relationship = New CommercialStatusRelationship("CommercialStatus", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationConsumerStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationConsumerStatisticsDetailRelationship("ReportAuthenticationConsumerStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationConsumerStatisticsSummary_OwnMany As Relationship = New ReportAuthenticationConsumerStatisticsSummaryRelationship("ReportAuthenticationConsumerStatisticsSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationSystemUserStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationSystemUserStatisticsDetailRelationship("ReportAuthenticationSystemUserStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationSystemUserStatisticsSummary_OwnMany As Relationship = New ReportAuthenticationSystemUserStatisticsSummaryRelationship("ReportAuthenticationSystemUserStatisticsSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationQuestionStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationQuestionStatisticsDetailRelationship("ReportAuthenticationQuestionStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationQuestionStatisticsSummary_OwnMany As Relationship = New ReportAuthenticationQuestionStatisticsSummaryRelationship("ReportAuthenticationQuestionStatisticsSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly TraceAlertDuration_OwnMany As Relationship = New TraceAlertDurationRelationship("TraceAlertDuration", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberTraceAlert_OwnMany As Relationship = New SPSubscriberTraceAlertRelationship("SPSubscriberTraceAlert", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly PrioritizationRating_OwnMany As Relationship = New PrioritizationRatingRelationship("PrioritizationRating", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPFileFormatMetaDataProcedure_OwnMany As Relationship = New SPFileFormatMetaDataProcedureRelationship("SPFileFormatMetaDataProcedure", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportTraceConsumerSystemUserDateSummary_OwnMany As Relationship = New ReportTraceConsumerSystemUserDateSummaryRelationship("ReportTraceConsumerSystemUserDateSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportTraceConsumerSystemUserProductSummary_OwnMany As Relationship = New ReportTraceConsumerSystemUserProductSummaryRelationship("ReportTraceConsumerSystemUserProductSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportTraceConsumerSystemUserProductDetail_OwnMany As Relationship = New ReportTraceConsumerSystemUserProductDetailRelationship("ReportTraceConsumerSystemUserProductDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportCreditGrantorSystemUserDateSummary_OwnMany As Relationship = New ReportCreditGrantorSystemUserDateSummaryRelationship("ReportCreditGrantorSystemUserDateSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportCreditGrantorSystemUserProductSummary_OwnMany As Relationship = New ReportCreditGrantorSystemUserProductSummaryRelationship("ReportCreditGrantorSystemUserProductSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportCreditGrantorSystemUserProductDetail_OwnMany As Relationship = New ReportCreditGrantorSystemUserProductDetailRelationship("ReportCreditGrantorSystemUserProductDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportEnquiryCommercialSystemUserDateSummary_OwnMany As Relationship = New ReportEnquiryCommercialSystemUserDateSummaryRelationship("ReportEnquiryCommercialSystemUserDateSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportEnquiryCommercialSystemUserProductSummary_OwnMany As Relationship = New ReportEnquiryCommercialSystemUserProductSummaryRelationship("ReportEnquiryCommercialSystemUserProductSummary", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportEnquiryCommercialSystemUserProductDetail_OwnMany As Relationship = New ReportEnquiryCommercialSystemUserProductDetailRelationship("ReportEnquiryCommercialSystemUserProductDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAdminLoaderValidation_OwnMany As Relationship = New ReportAdminLoaderValidationRelationship("ReportAdminLoaderValidation", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ProductAuthenticationQuestion_OwnMany As Relationship = New ProductAuthenticationQuestionRelationship("ProductAuthenticationQuestion", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPFileFormatMetaGroupField_OwnMany As Relationship = New SPFileFormatMetaGroupFieldRelationship("SPFileFormatMetaGroupField", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportTraceConsumerIdentityVerification_OwnMany As Relationship = New ReportTraceConsumerIdentityVerificationRelationship("ReportTraceConsumerIdentityVerification", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly HomeAffairs_OwnMany As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly HomeAffairsSoundEx_OwnMany As Relationship = New HomeAffairsSoundExRelationship("HomeAffairsSoundEx", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ProductAuthenticationQuestionGroup_OwnMany As Relationship = New ProductAuthenticationQuestionGroupRelationship("ProductAuthenticationQuestionGroup", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberBusinessType_OwnMany As Relationship = New SubscriberBusinessTypeRelationship("SubscriberBusinessType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberProfileProductAuthenticationQuestion_OwnMany As Relationship = New SPSubscriberProfileProductAuthenticationQuestionRelationship("SPSubscriberProfileProductAuthenticationQuestion", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationAuditDetail_OwnMany As Relationship = New ReportAuthenticationAuditDetailRelationship("ReportAuthenticationAuditDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberAssociation_OwnMany As Relationship = New SubscriberAssociationRelationship("SubscriberAssociation", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPProductAuthenticationQuestion_OwnMany As Relationship = New SPProductAuthenticationQuestionRelationship("SPProductAuthenticationQuestion", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CullingRule_OwnMany As Relationship = New CullingRuleRelationship("CullingRule", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoaderProcessDebugLog_OwnMany As Relationship = New LoaderProcessDebugLogRelationship("LoaderProcessDebugLog", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CullingRuleDebugLog_OwnMany As Relationship = New CullingRuleDebugLogRelationship("CullingRuleDebugLog", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportTraceDetail_OwnMany As Relationship = New ReportTraceDetailRelationship("ReportTraceDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemVersion_OwnMany As Relationship = New SystemVersionRelationship("SystemVersion", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationRepeatAuthenticationStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationRepeatAuthenticationStatisticsDetailRelationship("ReportAuthenticationRepeatAuthenticationStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationNotFoundStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationNotFoundStatisticsDetailRelationship("ReportAuthenticationNotFoundStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationStatisticsDetailRelationship("ReportAuthenticationStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ConsumerRandom_OwnMany As Relationship = New ConsumerRandomRelationship("ConsumerRandom", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SubscriberGroup_OwnMany As Relationship = New SubscriberGroupRelationship("SubscriberGroup", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly vwConsumerAccountAge_OwnMany As Relationship = New vwConsumerAccountAgeRelationship("vwConsumerAccountAge", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportAuthenticationCumulativeUsageStatisticsDetail_OwnMany As Relationship = New ReportAuthenticationCumulativeUsageStatisticsDetailRelationship("ReportAuthenticationCumulativeUsageStatisticsDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Profession_OwnMany As Relationship = New ProfessionRelationship("Profession", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly AuditorType_OwnMany As Relationship = New AuditorTypeRelationship("AuditorType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly AuditorStatus_OwnMany As Relationship = New AuditorStatusRelationship("AuditorStatus", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly DirectorType_OwnMany As Relationship = New DirectorTypeRelationship("DirectorType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly DirectorStatus_OwnMany As Relationship = New DirectorStatusRelationship("DirectorStatus", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly DirectorDesignation_OwnMany As Relationship = New DirectorDesignationRelationship("DirectorDesignation", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CommercialChangeType_OwnMany As Relationship = New CommercialChangeTypeRelationship("CommercialChangeType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CommercialType_OwnMany As Relationship = New CommercialTypeRelationship("CommercialType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SystemDatabase_OwnMany As Relationship = New SystemDatabaseRelationship("SystemDatabase", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Auditor_OwnMany As Relationship = New AuditorRelationship("Auditor", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Director_OwnMany As Relationship = New DirectorRelationship("Director", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly Language_OwnMany As Relationship = New LanguageRelationship("Language", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberCreditGrantor_OwnMany As Relationship = New SPSubscriberCreditGrantorRelationship("SPSubscriberCreditGrantor", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportCreditGrantorDetail_OwnMany As Relationship = New ReportCreditGrantorDetailRelationship("ReportCreditGrantorDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CreditGrantorEnquiryReason_OwnMany As Relationship = New CreditGrantorEnquiryReasonRelationship("CreditGrantorEnquiryReason", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly CommercialDirector_OwnMany As Relationship = New CommercialDirectorRelationship("CommercialDirector", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ConsumerAddressRandom_OwnMany As Relationship = New ConsumerAddressRandomRelationship("ConsumerAddressRandom", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ConsumerEmploymentRandom_OwnMany As Relationship = New ConsumerEmploymentRandomRelationship("ConsumerEmploymentRandom", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ConsumerTelephoneRandom_OwnMany As Relationship = New ConsumerTelephoneRandomRelationship("ConsumerTelephoneRandom", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ConsumerAccountRandom_OwnMany As Relationship = New ConsumerAccountRandomRelationship("ConsumerAccountRandom", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly JudgementType_OwnMany As Relationship = New JudgementTypeRelationship("JudgementType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly LoanEndUse_OwnMany As Relationship = New LoanEndUseRelationship("LoanEndUse", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly OwnershipType_OwnMany As Relationship = New OwnershipTypeRelationship("OwnershipType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly PaymentType_OwnMany As Relationship = New PaymentTypeRelationship("PaymentType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly AccountType_OwnMany As Relationship = New AccountTypeRelationship("AccountType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly RepaymentFrequency_OwnMany As Relationship = New RepaymentFrequencyRelationship("RepaymentFrequency", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly IncomeFrequency_OwnMany As Relationship = New IncomeFrequencyRelationship("IncomeFrequency", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPConsumerConflict_OwnMany As Relationship = New SPConsumerConflictRelationship("SPConsumerConflict", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPAuditorConflict_OwnMany As Relationship = New SPAuditorConflictRelationship("SPAuditorConflict", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPCommercialConflict_OwnMany As Relationship = New SPCommercialConflictRelationship("SPCommercialConflict", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPDirectorConflict_OwnMany As Relationship = New SPDirectorConflictRelationship("SPDirectorConflict", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberCommercial_OwnMany As Relationship = New SPSubscriberCommercialRelationship("SPSubscriberCommercial", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberTraceConsumer_OwnMany As Relationship = New SPSubscriberTraceConsumerRelationship("SPSubscriberTraceConsumer", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSubscriberTraceConsumerAlert_OwnMany As Relationship = New SPSubscriberTraceConsumerAlertRelationship("SPSubscriberTraceConsumerAlert", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly ReportCommercialDetail_OwnMany As Relationship = New ReportCommercialDetailRelationship("ReportCommercialDetail", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly PropertyType_OwnMany As Relationship = New PropertyTypeRelationship("PropertyType", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly PropertyDeed_OwnMany As Relationship = New PropertyDeedRelationship("PropertyDeed", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SAFPSSubject_OwnMany As Relationship = New SAFPSSubjectRelationship("SAFPSSubject", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SAFPSSubjectSoundEx_OwnMany As Relationship = New SAFPSSubjectSoundExRelationship("SAFPSSubjectSoundEx", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly PropertyDeedSoundEx_OwnMany As Relationship = New PropertyDeedSoundExRelationship("PropertyDeedSoundEx", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly IncidentCategory_OwnMany As Relationship = New IncidentCategoryRelationship("IncidentCategory", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SAFPSProtectiveRegistration_OwnMany As Relationship = New SAFPSProtectiveRegistrationRelationship("SAFPSProtectiveRegistration", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SAFPSProtectiveRegistrationSoundEx_OwnMany As Relationship = New SAFPSProtectiveRegistrationSoundExRelationship("SAFPSProtectiveRegistrationSoundEx", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPIncidentCategory_OwnMany As Relationship = New SPIncidentCategoryRelationship("SPIncidentCategory", Nothing, "XDSAdministrationRoot", Me)
  Public ReadOnly SPSAFPSSubject_OwnMany As Relationship = New SPSAFPSSubjectRelationship("SPSAFPSSubject", Nothing, "Owner", Me)
  Public ReadOnly SPSAFPSSubjectConflict_OwnMany As Relationship = New SPSAFPSSubjectConflictRelationship("SPSAFPSSubjectConflict", Nothing, "Owner", Me)
  Public ReadOnly SPSAFPSProtectiveRegistration_OwnMany As Relationship = New SPSAFPSProtectiveRegistrationRelationship("SPSAFPSProtectiveRegistration", Nothing, "Owner", Me)
  Public ReadOnly SPSAFPSProtectiveRegistrationConflict_OwnMany As Relationship = New SPSAFPSProtectiveRegistrationConflictRelationship("SPSAFPSProtectiveRegistrationConflict", Nothing, "Owner", Me)
  Public ReadOnly DebtReviewStatus_OwnMany As Relationship = New DebtReviewStatusRelationship("DebtReviewStatus", Nothing, "XDSAdministrationRoot", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _Subscriber_OwnMany As SubscriberRelationship = Subscriber_OwnMany
  Public ReadOnly _FileFormat_OwnMany As FileFormatRelationship = FileFormat_OwnMany
  Public ReadOnly _SPSubscriber_OwnMany As SPSubscriberRelationship = SPSubscriber_OwnMany
  Public ReadOnly _Consumer_OwnMany As ConsumerRelationship = Consumer_OwnMany
  Public ReadOnly _SPConsumer_OwnMany As SPConsumerRelationship = SPConsumer_OwnMany
  Public ReadOnly _Title_OwnMany As TitleRelationship = Title_OwnMany
  Public ReadOnly _MaritalStatus_OwnMany As MaritalStatusRelationship = MaritalStatus_OwnMany
  Public ReadOnly _Commercial_OwnMany As CommercialRelationship = Commercial_OwnMany
  Public ReadOnly _DataStatus_OwnMany As DataStatusRelationship = DataStatus_OwnMany
  Public ReadOnly _Period_OwnMany As PeriodRelationship = Period_OwnMany
  Public ReadOnly _SPCommercial_OwnMany As SPCommercialRelationship = SPCommercial_OwnMany
  Public ReadOnly _MetaDatabase_OwnMany As MetaDatabaseRelationship = MetaDatabase_OwnMany
  Public ReadOnly _SubscriberProfile_OwnMany As SubscriberProfileRelationship = SubscriberProfile_OwnMany
  Public ReadOnly _SecurityModule_OwnMany As SecurityModuleRelationship = SecurityModule_OwnMany
  Public ReadOnly _SystemUser_OwnMany As SystemUserRelationship = SystemUser_OwnMany
  Public ReadOnly _Role_OwnMany As RoleRelationship = Role_OwnMany
  Public ReadOnly _SecurityForm_OwnMany As SecurityFormRelationship = SecurityForm_OwnMany
  Public ReadOnly _SystemUserRole_OwnMany As SystemUserRoleRelationship = SystemUserRole_OwnMany
  Public ReadOnly _SystemUserLogin_OwnMany As SystemUserLoginRelationship = SystemUserLogin_OwnMany
  Public ReadOnly _ViewState_OwnMany As ViewStateRelationship = ViewState_OwnMany
  Public ReadOnly _LoaderThread_OwnMany As LoaderThreadRelationship = LoaderThread_OwnMany
  Public ReadOnly _SPLoaderProcess_OwnMany As SPLoaderProcessRelationship = SPLoaderProcess_OwnMany
  Public ReadOnly _SystemDefault_OwnMany As SystemDefaultRelationship = SystemDefault_OwnMany
  Public ReadOnly _Loader_OwnMany As LoaderRelationship = Loader_OwnMany
  Public ReadOnly _LoaderProcessType_OwnMany As LoaderProcessTypeRelationship = LoaderProcessType_OwnMany
  Public ReadOnly _LoaderProcess_OwnMany As LoaderProcessRelationship = LoaderProcess_OwnMany
  Public ReadOnly _SPDataProcedure_OwnMany As SPDataProcedureRelationship = SPDataProcedure_OwnMany
  Public ReadOnly _SPLoaderMatchingEngine_OwnMany As SPLoaderMatchingEngineRelationship = SPLoaderMatchingEngine_OwnMany
  Public ReadOnly _SPSubscriberProfile_OwnMany As SPSubscriberProfileRelationship = SPSubscriberProfile_OwnMany
  Public ReadOnly _SPSystemUser_OwnMany As SPSystemUserRelationship = SPSystemUser_OwnMany
  Public ReadOnly _SPSecurityForm_OwnMany As SPSecurityFormRelationship = SPSecurityForm_OwnMany
  Public ReadOnly _vwLoaderStagingTable_OwnMany As vwLoaderStagingTableRelationship = vwLoaderStagingTable_OwnMany
  Public ReadOnly _AuditTrailControl_OwnMany As AuditTrailControlRelationship = AuditTrailControl_OwnMany
  Public ReadOnly _SPAuditTrailControl_OwnMany As SPAuditTrailControlRelationship = SPAuditTrailControl_OwnMany
  Public ReadOnly _SPLoaderProcessHistory_OwnMany As SPLoaderProcessHistoryRelationship = SPLoaderProcessHistory_OwnMany
  Public ReadOnly _NoteType_OwnMany As NoteTypeRelationship = NoteType_OwnMany
  Public ReadOnly _vwDeploymentFileFormatMetaGroup_OwnMany As vwDeploymentFileFormatMetaGroupRelationship = vwDeploymentFileFormatMetaGroup_OwnMany
  Public ReadOnly _vwDeploymentFileFormatMetaGroupField_OwnMany As vwDeploymentFileFormatMetaGroupFieldRelationship = vwDeploymentFileFormatMetaGroupField_OwnMany
  Public ReadOnly _FileFormatType_OwnMany As FileFormatTypeRelationship = FileFormatType_OwnMany
  Public ReadOnly _SPLoaderProcessMetaDatabase_OwnMany As SPLoaderProcessMetaDatabaseRelationship = SPLoaderProcessMetaDatabase_OwnMany
  Public ReadOnly _DataProcedure_OwnMany As DataProcedureRelationship = DataProcedure_OwnMany
  Public ReadOnly _LoaderMatchingEngine_OwnMany As LoaderMatchingEngineRelationship = LoaderMatchingEngine_OwnMany
  Public ReadOnly _LoaderProcessStatus_OwnMany As LoaderProcessStatusRelationship = LoaderProcessStatus_OwnMany
  Public ReadOnly _FileFormatDeployment_OwnMany As FileFormatDeploymentRelationship = FileFormatDeployment_OwnMany
  Public ReadOnly _LoaderUploadFile_OwnMany As LoaderUploadFileRelationship = LoaderUploadFile_OwnMany
  Public ReadOnly _ReportingServicesServer_OwnMany As ReportingServicesServerRelationship = ReportingServicesServer_OwnMany
  Public ReadOnly _ReportingServicesReport_OwnMany As ReportingServicesReportRelationship = ReportingServicesReport_OwnMany
  Public ReadOnly _SPReportingServicesReport_OwnMany As SPReportingServicesReportRelationship = SPReportingServicesReport_OwnMany
  Public ReadOnly _Product_OwnMany As ProductRelationship = Product_OwnMany
  Public ReadOnly _SubscriberRegistration_OwnMany As SubscriberRegistrationRelationship = SubscriberRegistration_OwnMany
  Public ReadOnly _SPSubscriberRegistration_OwnMany As SPSubscriberRegistrationRelationship = SPSubscriberRegistration_OwnMany
  Public ReadOnly _ProductType_OwnMany As ProductTypeRelationship = ProductType_OwnMany
  Public ReadOnly _EnquiryMatchingEngine_OwnMany As EnquiryMatchingEngineRelationship = EnquiryMatchingEngine_OwnMany
  Public ReadOnly _SubscriberBillingGroup_OwnMany As SubscriberBillingGroupRelationship = SubscriberBillingGroup_OwnMany
  Public ReadOnly _SystemUserProfile_OwnMany As SystemUserProfileRelationship = SystemUserProfile_OwnMany
  Public ReadOnly _FileFormatDeploymentMetaLoaderProcedure_OwnMany As FileFormatDeploymentMetaLoaderProcedureRelationship = FileFormatDeploymentMetaLoaderProcedure_OwnMany
  Public ReadOnly _SystemUserForgotPassword_OwnMany As SystemUserForgotPasswordRelationship = SystemUserForgotPassword_OwnMany
  Public ReadOnly _NotificationTemplate_OwnMany As NotificationTemplateRelationship = NotificationTemplate_OwnMany
  Public ReadOnly _NotificationHistory_OwnMany As NotificationHistoryRelationship = NotificationHistory_OwnMany
  Public ReadOnly _NotificationQueue_OwnMany As NotificationQueueRelationship = NotificationQueue_OwnMany
  Public ReadOnly _SPSubscriberProfileProduct_OwnMany As SPSubscriberProfileProductRelationship = SPSubscriberProfileProduct_OwnMany
  Public ReadOnly _SPSystemUserProfileProduct_OwnMany As SPSystemUserProfileProductRelationship = SPSystemUserProfileProduct_OwnMany
  Public ReadOnly _SubscriberLogin_OwnMany As SubscriberLoginRelationship = SubscriberLogin_OwnMany
  Public ReadOnly _SubscriberForgotPassword_OwnMany As SubscriberForgotPasswordRelationship = SubscriberForgotPassword_OwnMany
  Public ReadOnly _SPSubscriberEnquiry_OwnMany As SPSubscriberEnquiryRelationship = SPSubscriberEnquiry_OwnMany
  Public ReadOnly _SPSubscriberEnquiryConsumer_OwnMany As SPSubscriberEnquiryConsumerRelationship = SPSubscriberEnquiryConsumer_OwnMany
  Public ReadOnly _SPSubscriberAuthentication_OwnMany As SPSubscriberAuthenticationRelationship = SPSubscriberAuthentication_OwnMany
  Public ReadOnly _SPSubscriberTrace_OwnMany As SPSubscriberTraceRelationship = SPSubscriberTrace_OwnMany
  Public ReadOnly _SIC_OwnMany As SICRelationship = SIC_OwnMany
  Public ReadOnly _Country_OwnMany As CountryRelationship = Country_OwnMany
  Public ReadOnly _CommercialStatus_OwnMany As CommercialStatusRelationship = CommercialStatus_OwnMany
  Public ReadOnly _ReportAuthenticationConsumerStatisticsDetail_OwnMany As ReportAuthenticationConsumerStatisticsDetailRelationship = ReportAuthenticationConsumerStatisticsDetail_OwnMany
  Public ReadOnly _ReportAuthenticationConsumerStatisticsSummary_OwnMany As ReportAuthenticationConsumerStatisticsSummaryRelationship = ReportAuthenticationConsumerStatisticsSummary_OwnMany
  Public ReadOnly _ReportAuthenticationSystemUserStatisticsDetail_OwnMany As ReportAuthenticationSystemUserStatisticsDetailRelationship = ReportAuthenticationSystemUserStatisticsDetail_OwnMany
  Public ReadOnly _ReportAuthenticationSystemUserStatisticsSummary_OwnMany As ReportAuthenticationSystemUserStatisticsSummaryRelationship = ReportAuthenticationSystemUserStatisticsSummary_OwnMany
  Public ReadOnly _ReportAuthenticationQuestionStatisticsDetail_OwnMany As ReportAuthenticationQuestionStatisticsDetailRelationship = ReportAuthenticationQuestionStatisticsDetail_OwnMany
  Public ReadOnly _ReportAuthenticationQuestionStatisticsSummary_OwnMany As ReportAuthenticationQuestionStatisticsSummaryRelationship = ReportAuthenticationQuestionStatisticsSummary_OwnMany
  Public ReadOnly _TraceAlertDuration_OwnMany As TraceAlertDurationRelationship = TraceAlertDuration_OwnMany
  Public ReadOnly _SPSubscriberTraceAlert_OwnMany As SPSubscriberTraceAlertRelationship = SPSubscriberTraceAlert_OwnMany
  Public ReadOnly _PrioritizationRating_OwnMany As PrioritizationRatingRelationship = PrioritizationRating_OwnMany
  Public ReadOnly _SPFileFormatMetaDataProcedure_OwnMany As SPFileFormatMetaDataProcedureRelationship = SPFileFormatMetaDataProcedure_OwnMany
  Public ReadOnly _ReportTraceConsumerSystemUserDateSummary_OwnMany As ReportTraceConsumerSystemUserDateSummaryRelationship = ReportTraceConsumerSystemUserDateSummary_OwnMany
  Public ReadOnly _ReportTraceConsumerSystemUserProductSummary_OwnMany As ReportTraceConsumerSystemUserProductSummaryRelationship = ReportTraceConsumerSystemUserProductSummary_OwnMany
  Public ReadOnly _ReportTraceConsumerSystemUserProductDetail_OwnMany As ReportTraceConsumerSystemUserProductDetailRelationship = ReportTraceConsumerSystemUserProductDetail_OwnMany
  Public ReadOnly _ReportCreditGrantorSystemUserDateSummary_OwnMany As ReportCreditGrantorSystemUserDateSummaryRelationship = ReportCreditGrantorSystemUserDateSummary_OwnMany
  Public ReadOnly _ReportCreditGrantorSystemUserProductSummary_OwnMany As ReportCreditGrantorSystemUserProductSummaryRelationship = ReportCreditGrantorSystemUserProductSummary_OwnMany
  Public ReadOnly _ReportCreditGrantorSystemUserProductDetail_OwnMany As ReportCreditGrantorSystemUserProductDetailRelationship = ReportCreditGrantorSystemUserProductDetail_OwnMany
  Public ReadOnly _ReportEnquiryCommercialSystemUserDateSummary_OwnMany As ReportEnquiryCommercialSystemUserDateSummaryRelationship = ReportEnquiryCommercialSystemUserDateSummary_OwnMany
  Public ReadOnly _ReportEnquiryCommercialSystemUserProductSummary_OwnMany As ReportEnquiryCommercialSystemUserProductSummaryRelationship = ReportEnquiryCommercialSystemUserProductSummary_OwnMany
  Public ReadOnly _ReportEnquiryCommercialSystemUserProductDetail_OwnMany As ReportEnquiryCommercialSystemUserProductDetailRelationship = ReportEnquiryCommercialSystemUserProductDetail_OwnMany
  Public ReadOnly _ReportAdminLoaderValidation_OwnMany As ReportAdminLoaderValidationRelationship = ReportAdminLoaderValidation_OwnMany
  Public ReadOnly _ProductAuthenticationQuestion_OwnMany As ProductAuthenticationQuestionRelationship = ProductAuthenticationQuestion_OwnMany
  Public ReadOnly _SPFileFormatMetaGroupField_OwnMany As SPFileFormatMetaGroupFieldRelationship = SPFileFormatMetaGroupField_OwnMany
  Public ReadOnly _ReportTraceConsumerIdentityVerification_OwnMany As ReportTraceConsumerIdentityVerificationRelationship = ReportTraceConsumerIdentityVerification_OwnMany
  Public ReadOnly _HomeAffairs_OwnMany As HomeAffairsRelationship = HomeAffairs_OwnMany
  Public ReadOnly _HomeAffairsSoundEx_OwnMany As HomeAffairsSoundExRelationship = HomeAffairsSoundEx_OwnMany
  Public ReadOnly _ProductAuthenticationQuestionGroup_OwnMany As ProductAuthenticationQuestionGroupRelationship = ProductAuthenticationQuestionGroup_OwnMany
  Public ReadOnly _SubscriberBusinessType_OwnMany As SubscriberBusinessTypeRelationship = SubscriberBusinessType_OwnMany
  Public ReadOnly _SPSubscriberProfileProductAuthenticationQuestion_OwnMany As SPSubscriberProfileProductAuthenticationQuestionRelationship = SPSubscriberProfileProductAuthenticationQuestion_OwnMany
  Public ReadOnly _ReportAuthenticationAuditDetail_OwnMany As ReportAuthenticationAuditDetailRelationship = ReportAuthenticationAuditDetail_OwnMany
  Public ReadOnly _SubscriberAssociation_OwnMany As SubscriberAssociationRelationship = SubscriberAssociation_OwnMany
  Public ReadOnly _SPProductAuthenticationQuestion_OwnMany As SPProductAuthenticationQuestionRelationship = SPProductAuthenticationQuestion_OwnMany
  Public ReadOnly _CullingRule_OwnMany As CullingRuleRelationship = CullingRule_OwnMany
  Public ReadOnly _LoaderProcessDebugLog_OwnMany As LoaderProcessDebugLogRelationship = LoaderProcessDebugLog_OwnMany
  Public ReadOnly _CullingRuleDebugLog_OwnMany As CullingRuleDebugLogRelationship = CullingRuleDebugLog_OwnMany
  Public ReadOnly _ReportTraceDetail_OwnMany As ReportTraceDetailRelationship = ReportTraceDetail_OwnMany
  Public ReadOnly _SystemVersion_OwnMany As SystemVersionRelationship = SystemVersion_OwnMany
  Public ReadOnly _ReportAuthenticationRepeatAuthenticationStatisticsDetail_OwnMany As ReportAuthenticationRepeatAuthenticationStatisticsDetailRelationship = ReportAuthenticationRepeatAuthenticationStatisticsDetail_OwnMany
  Public ReadOnly _ReportAuthenticationNotFoundStatisticsDetail_OwnMany As ReportAuthenticationNotFoundStatisticsDetailRelationship = ReportAuthenticationNotFoundStatisticsDetail_OwnMany
  Public ReadOnly _ReportAuthenticationStatisticsDetail_OwnMany As ReportAuthenticationStatisticsDetailRelationship = ReportAuthenticationStatisticsDetail_OwnMany
  Public ReadOnly _ConsumerRandom_OwnMany As ConsumerRandomRelationship = ConsumerRandom_OwnMany
  Public ReadOnly _SubscriberGroup_OwnMany As SubscriberGroupRelationship = SubscriberGroup_OwnMany
  Public ReadOnly _vwConsumerAccountAge_OwnMany As vwConsumerAccountAgeRelationship = vwConsumerAccountAge_OwnMany
  Public ReadOnly _ReportAuthenticationCumulativeUsageStatisticsDetail_OwnMany As ReportAuthenticationCumulativeUsageStatisticsDetailRelationship = ReportAuthenticationCumulativeUsageStatisticsDetail_OwnMany
  Public ReadOnly _Profession_OwnMany As ProfessionRelationship = Profession_OwnMany
  Public ReadOnly _AuditorType_OwnMany As AuditorTypeRelationship = AuditorType_OwnMany
  Public ReadOnly _AuditorStatus_OwnMany As AuditorStatusRelationship = AuditorStatus_OwnMany
  Public ReadOnly _DirectorType_OwnMany As DirectorTypeRelationship = DirectorType_OwnMany
  Public ReadOnly _DirectorStatus_OwnMany As DirectorStatusRelationship = DirectorStatus_OwnMany
  Public ReadOnly _DirectorDesignation_OwnMany As DirectorDesignationRelationship = DirectorDesignation_OwnMany
  Public ReadOnly _CommercialChangeType_OwnMany As CommercialChangeTypeRelationship = CommercialChangeType_OwnMany
  Public ReadOnly _CommercialType_OwnMany As CommercialTypeRelationship = CommercialType_OwnMany
  Public ReadOnly _SystemDatabase_OwnMany As SystemDatabaseRelationship = SystemDatabase_OwnMany
  Public ReadOnly _Auditor_OwnMany As AuditorRelationship = Auditor_OwnMany
  Public ReadOnly _Director_OwnMany As DirectorRelationship = Director_OwnMany
  Public ReadOnly _Language_OwnMany As LanguageRelationship = Language_OwnMany
  Public ReadOnly _SPSubscriberCreditGrantor_OwnMany As SPSubscriberCreditGrantorRelationship = SPSubscriberCreditGrantor_OwnMany
  Public ReadOnly _ReportCreditGrantorDetail_OwnMany As ReportCreditGrantorDetailRelationship = ReportCreditGrantorDetail_OwnMany
  Public ReadOnly _CreditGrantorEnquiryReason_OwnMany As CreditGrantorEnquiryReasonRelationship = CreditGrantorEnquiryReason_OwnMany
  Public ReadOnly _CommercialDirector_OwnMany As CommercialDirectorRelationship = CommercialDirector_OwnMany
  Public ReadOnly _ConsumerAddressRandom_OwnMany As ConsumerAddressRandomRelationship = ConsumerAddressRandom_OwnMany
  Public ReadOnly _ConsumerEmploymentRandom_OwnMany As ConsumerEmploymentRandomRelationship = ConsumerEmploymentRandom_OwnMany
  Public ReadOnly _ConsumerTelephoneRandom_OwnMany As ConsumerTelephoneRandomRelationship = ConsumerTelephoneRandom_OwnMany
  Public ReadOnly _ConsumerAccountRandom_OwnMany As ConsumerAccountRandomRelationship = ConsumerAccountRandom_OwnMany
  Public ReadOnly _JudgementType_OwnMany As JudgementTypeRelationship = JudgementType_OwnMany
  Public ReadOnly _LoanEndUse_OwnMany As LoanEndUseRelationship = LoanEndUse_OwnMany
  Public ReadOnly _OwnershipType_OwnMany As OwnershipTypeRelationship = OwnershipType_OwnMany
  Public ReadOnly _PaymentType_OwnMany As PaymentTypeRelationship = PaymentType_OwnMany
  Public ReadOnly _AccountType_OwnMany As AccountTypeRelationship = AccountType_OwnMany
  Public ReadOnly _RepaymentFrequency_OwnMany As RepaymentFrequencyRelationship = RepaymentFrequency_OwnMany
  Public ReadOnly _IncomeFrequency_OwnMany As IncomeFrequencyRelationship = IncomeFrequency_OwnMany
  Public ReadOnly _SPConsumerConflict_OwnMany As SPConsumerConflictRelationship = SPConsumerConflict_OwnMany
  Public ReadOnly _SPAuditorConflict_OwnMany As SPAuditorConflictRelationship = SPAuditorConflict_OwnMany
  Public ReadOnly _SPCommercialConflict_OwnMany As SPCommercialConflictRelationship = SPCommercialConflict_OwnMany
  Public ReadOnly _SPDirectorConflict_OwnMany As SPDirectorConflictRelationship = SPDirectorConflict_OwnMany
  Public ReadOnly _SPSubscriberCommercial_OwnMany As SPSubscriberCommercialRelationship = SPSubscriberCommercial_OwnMany
  Public ReadOnly _SPSubscriberTraceConsumer_OwnMany As SPSubscriberTraceConsumerRelationship = SPSubscriberTraceConsumer_OwnMany
  Public ReadOnly _SPSubscriberTraceConsumerAlert_OwnMany As SPSubscriberTraceConsumerAlertRelationship = SPSubscriberTraceConsumerAlert_OwnMany
  Public ReadOnly _ReportCommercialDetail_OwnMany As ReportCommercialDetailRelationship = ReportCommercialDetail_OwnMany
  Public ReadOnly _PropertyType_OwnMany As PropertyTypeRelationship = PropertyType_OwnMany
  Public ReadOnly _PropertyDeed_OwnMany As PropertyDeedRelationship = PropertyDeed_OwnMany
  Public ReadOnly _SAFPSSubject_OwnMany As SAFPSSubjectRelationship = SAFPSSubject_OwnMany
  Public ReadOnly _SAFPSSubjectSoundEx_OwnMany As SAFPSSubjectSoundExRelationship = SAFPSSubjectSoundEx_OwnMany
  Public ReadOnly _PropertyDeedSoundEx_OwnMany As PropertyDeedSoundExRelationship = PropertyDeedSoundEx_OwnMany
  Public ReadOnly _IncidentCategory_OwnMany As IncidentCategoryRelationship = IncidentCategory_OwnMany
  Public ReadOnly _SAFPSProtectiveRegistration_OwnMany As SAFPSProtectiveRegistrationRelationship = SAFPSProtectiveRegistration_OwnMany
  Public ReadOnly _SAFPSProtectiveRegistrationSoundEx_OwnMany As SAFPSProtectiveRegistrationSoundExRelationship = SAFPSProtectiveRegistrationSoundEx_OwnMany
  Public ReadOnly _SPIncidentCategory_OwnMany As SPIncidentCategoryRelationship = SPIncidentCategory_OwnMany
  Public ReadOnly _SPSAFPSSubject_OwnMany As SPSAFPSSubjectRelationship = SPSAFPSSubject_OwnMany
  Public ReadOnly _SPSAFPSSubjectConflict_OwnMany As SPSAFPSSubjectConflictRelationship = SPSAFPSSubjectConflict_OwnMany
  Public ReadOnly _SPSAFPSProtectiveRegistration_OwnMany As SPSAFPSProtectiveRegistrationRelationship = SPSAFPSProtectiveRegistration_OwnMany
  Public ReadOnly _SPSAFPSProtectiveRegistrationConflict_OwnMany As SPSAFPSProtectiveRegistrationConflictRelationship = SPSAFPSProtectiveRegistrationConflict_OwnMany
  Public ReadOnly _DebtReviewStatus_OwnMany As DebtReviewStatusRelationship = DebtReviewStatus_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("XDSAdministrationRoot", ApplicationSettings)
    PartState = PartStates.ExistingPart
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Get ApplicationSettings"

  'sembleWare: Get ApplicationSettings Start - Do Not Modify
  Public Shared Function GetApplicationSettings(ByVal ConnectString As String) As ApplicationSettings
    Return New ApplicationSettings(New System.Data.SqlClient.SqlConnection(ConnectString), New sembleWare.Runtime.SQLData.SQLQueryBuilder, 60)
  End Function
  'sembleWare: Get ApplicationSettings End - Do Not Modify

#End Region 'sembleWare: Get ApplicationSettings

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class XDSAdministrationRootRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New XDSAdministrationRoot(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
