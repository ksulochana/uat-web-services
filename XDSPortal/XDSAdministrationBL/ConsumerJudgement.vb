Imports sembleWare.Runtime
Imports System
Public Class ConsumerJudgement 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerJudgementID As New IdentityElement("ConsumerJudgementID", Me, True, Nothing, Nothing)
  Public ReadOnly CourtCaseID As New IntegerElement("CourtCaseID", Me, False, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdName As New StringElement("ThirdName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address1 As New StringElement("Address1", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address2 As New StringElement("Address2", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address3 As New StringElement("Address3", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address4 As New StringElement("Address4", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalCode As New StringElement("PostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly HomeTelephoneCode As New StringElement("HomeTelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly HomeTelephoneNo As New StringElement("HomeTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WorkTelephoneCode As New StringElement("WorkTelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WorkTelephoneNo As New StringElement("WorkTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CellularNo As New StringElement("CellularNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FaxCode As New StringElement("FaxCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FaxNo As New StringElement("FaxNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CaseNumber As New StringElement("CaseNumber", Me, False, True, True, True, False, 50, "Case Number", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CaseFilingDate As New DateTimeElement("CaseFilingDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CaseReason As New StringElement("CaseReason", Me, False, True, True, True, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CaseType As New StringElement("CaseType", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DisputeAmt As New DecimalElement("DisputeAmt", Me, False, True, True, True, False, "Dispute Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly CourtName As New StringElement("CourtName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CourtCity As New StringElement("CourtCity", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CourtType As New StringElement("CourtType", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PlaintiffName As New StringElement("PlaintiffName", Me, False, True, True, True, False, 200, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PlaintiffAddress1 As New StringElement("PlaintiffAddress1", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PlaintiffAddress2 As New StringElement("PlaintiffAddress2", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PlaintiffAddress3 As New StringElement("PlaintiffAddress3", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PlaintiffAddress4 As New StringElement("PlaintiffAddress4", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PlaintiffPostalCode As New StringElement("PlaintiffPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyName As New StringElement("AttorneyName", Me, False, True, True, True, False, 200, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyTelephoneCode As New StringElement("AttorneyTelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyTelephoneNo As New StringElement("AttorneyTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyFaxCode As New StringElement("AttorneyFaxCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyFaxNo As New StringElement("AttorneyFaxNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyAddress1 As New StringElement("AttorneyAddress1", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyAddress2 As New StringElement("AttorneyAddress2", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyAddress3 As New StringElement("AttorneyAddress3", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyAddress4 As New StringElement("AttorneyAddress4", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyPostalCode As New StringElement("AttorneyPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, True, True, True, False, 50, "Case Number", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerJudgement.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IsVerifiedYN As New BooleanElement("IsVerifiedYN", Me, False, True, True, True, True, "Is Verified?", Nothing, False, "Yes;No")
  Public ReadOnly VerifiedDate As New DateTimeElement("VerifiedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Comment As New TextElement("Comment", Me, True, True, False, False, Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly JudgementType As Relationship = New JudgementTypeRelationship("JudgementType", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _JudgementTypeCode As New StringElement("JudgementTypeCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _JudgementType As JudgementTypeRelationship = JudgementType
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerJudgement", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _JudgementType._JudgementTypeCode.LocalElement = _JudgementTypeCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    AddHandler IsVerifiedYN.Changed, AddressOf IsVerifiedYN_Changed
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub IsVerifiedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRIsVerifiedYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub

  Private Sub BRIsVerifiedYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Element.ValueAsObject Then
        Me.VerifiedDate.Value = System.DateTime.Now
      Else
        Me.VerifiedDate.SetToNullValue()
      End If
    End If
  End Sub
#End Region

#Region "User Code"
  Public Function getCommentID() As String
    Dim oString As String
    Dim iCommentID As Integer
    Dim oDataTable As DataTable

    oString = "select Max(CommentID) from Comments where ConsumerID=" + Me._ConsumerID.Value.ToString() + _
          " and ConsumerJudgementID=" + Me.ConsumerJudgementID.Value.ToString()

    oDataTable = ApplicationSettings.ResultQuery(oString)

    If oDataTable.Rows.Count > 0 Then
      Dim oDataRow As DataRow = oDataTable.Rows(0)

      If Not oDataRow(0) Is System.DBNull.Value Then
        Return oDataRow(0)
      End If

    End If

    Return -1
  End Function
#End Region
End Class 'sembleWare: Part

Public Class ConsumerJudgementRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerJudgementID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerJudgement(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerJudgementID.ForeignElement = CType(Instance, ConsumerJudgement).ConsumerJudgementID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerJudgement)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerJudgement")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerJudgement].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerJudgement].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerJudgement].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0ConsumerJudgement].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("CourtCaseID", "[A0ConsumerJudgement].[CourtCaseID]", "CourtCaseID", DataType.Integer, Nothing, False, 0, True, 100, "Court Case ID"))
    oList.Columns.Add(New ListColumn("JudgementTypeCode", "[A3JudgementType].[JudgementTypeCode]", "JudgementTypeCode", DataType.String, Nothing, False, 0, True, 100, "Judgement Type Code"))
    oList.Columns.Add(New ListColumn("JudgementTypeDesc", "[A3JudgementType].[JudgementTypeDesc]", "JudgementTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Judgement Type Description"))
    oList.Columns.Add(New ListColumn("CaseNumber", "[A0ConsumerJudgement].[CaseNumber]", "CaseNumber", DataType.String, Nothing, False, 0, True, 150, "Case Number"))
    oList.Columns.Add(New ListColumn("CaseFilingDate", "[A0ConsumerJudgement].[CaseFilingDate]", "CaseFilingDate", DataType.DateTime, "d", False, 0, True, 100, "Case Filing Date"))
    oList.Columns.Add(New ListColumn("CaseReason", "[A0ConsumerJudgement].[CaseReason]", "CaseReason", DataType.String, Nothing, False, 0, True, 200, "Case Reason"))
    oList.Columns.Add(New ListColumn("DisputeAmt", "[A0ConsumerJudgement].[DisputeAmt]", "DisputeAmt", DataType.Decimal, "c", False, 0, True, 100, "Dispute Amount"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("ConsumerJudgementID", "[A0ConsumerJudgement].[ConsumerJudgementID]", "ConsumerJudgementID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Judgement ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "((([ConsumerJudgement]  [A0ConsumerJudgement]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0ConsumerJudgement].[SubscriberID] = [A1Subscriber].[SubscriberID])" + _
           "    join [Consumer]  [A2Consumer] on [A0ConsumerJudgement].[ConsumerID] = [A2Consumer].[ConsumerID])" + _
           "    join [JudgementType]  [A3JudgementType] on [A0ConsumerJudgement].[JudgementTypeCode] = [A3JudgementType].[JudgementTypeCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
