Imports sembleWare.Runtime
Imports System
Public Class HomeAffairs 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly HomeAffairsID As New IdentityElement("HomeAffairsID", Me, True, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdName As New StringElement("ThirdName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NameCombo As New StringElement("NameCombo", Me, False, True, True, True, False, 300, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly HomeAffairsRunYN As New BooleanElement("HomeAffairsRunYN", Me, False, True, True, True, True, "Home Affairs Run?", Nothing, False, "Yes;No")
  Public ReadOnly HomeAffairsMessage As New StringElement("HomeAffairsMessage", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DeceasedDate As New DateTimeElement("DeceasedDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CauseOfDeath As New StringElement("CauseOfDeath", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, HomeAffairs.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly HomeAffairsHistory_OwnMany As Relationship = New HomeAffairsHistoryRelationship("HomeAffairsHistory", Nothing, "HomeAffairs", Me)
  Public ReadOnly HomeAffairsConflict_OwnMany As Relationship = New HomeAffairsConflictRelationship("HomeAffairsConflict", Nothing, "HomeAffairs", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _HomeAffairsHistory_OwnMany As HomeAffairsHistoryRelationship = HomeAffairsHistory_OwnMany
  Public ReadOnly _HomeAffairsConflict_OwnMany As HomeAffairsConflictRelationship = HomeAffairsConflict_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("HomeAffairs", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class HomeAffairsRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _HomeAffairsID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New HomeAffairs(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _HomeAffairsID.ForeignElement = CType(Instance, HomeAffairs).HomeAffairsID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0HomeAffairs")
    oList.Columns.Add(New ListColumn("HomeAffairsID", "[A0HomeAffairs].[HomeAffairsID]", "HomeAffairsID", DataType.Long, Nothing, True, 1, True, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0HomeAffairs].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0HomeAffairs].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0HomeAffairs].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ThirdName", "[A0HomeAffairs].[ThirdName]", "ThirdName", DataType.String, Nothing, False, 0, True, 100, "Third Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0HomeAffairs].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("HomeAffairsRunYN", "[A0HomeAffairs].[HomeAffairsRunYN]", "HomeAffairsRunYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Home Affairs Run?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[HomeAffairs]  [A0HomeAffairs]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0HomeAffairs")
    oList.Columns.Add(New ListColumn("HomeAffairsID", "[A0HomeAffairs].[HomeAffairsID]", "HomeAffairsID", DataType.Long, Nothing, True, 0, True, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0HomeAffairs].[IDNo]", "IDNo", DataType.String, Nothing, False, 1, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0HomeAffairs].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0HomeAffairs].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ThirdName", "[A0HomeAffairs].[ThirdName]", "ThirdName", DataType.String, Nothing, False, 0, True, 100, "Third Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0HomeAffairs].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("HomeAffairsRunYN", "[A0HomeAffairs].[HomeAffairsRunYN]", "HomeAffairsRunYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Home Affairs Run?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[HomeAffairs]  [A0HomeAffairs]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0HomeAffairs")
    oList.Columns.Add(New ListColumn("HomeAffairsID", "[A0HomeAffairs].[HomeAffairsID]", "HomeAffairsID", DataType.Long, Nothing, True, 1, True, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0HomeAffairs].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0HomeAffairs].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0HomeAffairs].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ThirdName", "[A0HomeAffairs].[ThirdName]", "ThirdName", DataType.String, Nothing, False, 0, True, 100, "Third Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0HomeAffairs].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("HomeAffairsRunYN", "[A0HomeAffairs].[HomeAffairsRunYN]", "HomeAffairsRunYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Home Affairs Run?"))
    oList.Columns.Add(New ListColumn("IsPossibleNameConflictYN", "[A0HomeAffairs].[IsPossibleNameConflictYN]", "IsPossibleNameConflictYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Name Conflict?"))
    oList.Columns.Add(New ListColumn("IsPossibleDuplicateRecordYN", "[A0HomeAffairs].[IsPossibleDuplicateRecordYN]", "IsPossibleDuplicateRecordYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Duplicate Record?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[HomeAffairs]  [A0HomeAffairs]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
