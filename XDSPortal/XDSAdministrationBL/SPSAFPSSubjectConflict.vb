Imports sembleWare.Runtime
Imports System
Public Class SPSAFPSSubjectConflict 'sembleWare: Part
    Inherits CustomMemoryPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
    Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, Nothing, Nothing, SPSAFPSSubjectConflict.GenderIndOptions, Nothing)
    Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly Owner As Relationship = New XDSAdministrationRootRelationship("Owner", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _Owner As XDSAdministrationRootRelationship = Owner
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    Private Shared moGenderIndOptions As IndicatorOptions
    Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
        Get
            If moGenderIndOptions Is Nothing Then
                moGenderIndOptions = New IndicatorOptions
                moGenderIndOptions.Add("M", "Male")
                moGenderIndOptions.Add("F", "Female")
            End If
            Return moGenderIndOptions
        End Get
    End Property
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SPSAFPSSubjectConflict", ApplicationSettings)
        'sembleWare: Constructor End - Do Not Modify

    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPSAFPSSubjectConflictRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SPSAFPSSubjectConflict(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
