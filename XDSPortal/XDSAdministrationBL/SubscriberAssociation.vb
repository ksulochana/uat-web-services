Imports sembleWare.Runtime
Imports System
Public Class SubscriberAssociation 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberAssociationCode As New StringElement("SubscriberAssociationCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberAssociationDesc As New StringElement("SubscriberAssociationDesc", Me, False, True, True, True, True, 50, "Subscriber Association Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberAssociationConsumerTraceReport_OwnMany As Relationship = New SubscriberAssociationConsumerTraceReportRelationship("SubscriberAssociationConsumerTraceReport", Nothing, "SubscriberAssociation", Me)
  Public ReadOnly SubscriberAssociationConsumerCreditGrantorReport_OwnMany As Relationship = New SubscriberAssociationConsumerCreditGrantorReportRelationship("SubscriberAssociationConsumerCreditGrantorReport", Nothing, "SubscriberAssociation", Me)
  Public ReadOnly SubscriberAssociationCommercialEnquiryReport_OwnMany As Relationship = New SubscriberAssociationCommercialEnquiryReportRelationship("SubscriberAssociationCommercialEnquiryReport", Nothing, "SubscriberAssociation", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SubscriberAssociationConsumerTraceReport_OwnMany As SubscriberAssociationConsumerTraceReportRelationship = SubscriberAssociationConsumerTraceReport_OwnMany
  Public ReadOnly _SubscriberAssociationConsumerCreditGrantorReport_OwnMany As SubscriberAssociationConsumerCreditGrantorReportRelationship = SubscriberAssociationConsumerCreditGrantorReport_OwnMany
  Public ReadOnly _SubscriberAssociationCommercialEnquiryReport_OwnMany As SubscriberAssociationCommercialEnquiryReportRelationship = SubscriberAssociationCommercialEnquiryReport_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAssociation", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberAssociationRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAssociation(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberAssociationCode.ForeignElement = CType(Instance, SubscriberAssociation).SubscriberAssociationCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAssociation")
    oList.Columns.Add(New ListColumn("SubscriberAssociationCode", "[A0SubscriberAssociation].[SubscriberAssociationCode]", "SubscriberAssociationCode", DataType.String, Nothing, True, 1, True, 100, "Subscriber Association Code"))
    oList.Columns.Add(New ListColumn("SubscriberAssociationDesc", "[A0SubscriberAssociation].[SubscriberAssociationDesc]", "SubscriberAssociationDesc", DataType.String, Nothing, False, 0, True, 200, "Subscriber Association Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAssociation]  [A0SubscriberAssociation]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAssociation")
    oList.Columns.Add(New ListColumn("SubscriberAssociationCode", "[A0SubscriberAssociation].[SubscriberAssociationCode]", "SubscriberAssociationCode", DataType.String, Nothing, True, 1, True, 100, "Subscriber Association Code"))
    oList.Columns.Add(New ListColumn("SubscriberAssociationDesc", "[A0SubscriberAssociation].[SubscriberAssociationDesc]", "SubscriberAssociationDesc", DataType.String, Nothing, False, 0, True, 200, "Subscriber Association Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAssociation]  [A0SubscriberAssociation]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
