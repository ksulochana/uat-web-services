Imports sembleWare.Runtime
Imports System
Public Class MetaGroupField 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly MetaGroupFieldTypeInd As New IndicatorElement("MetaGroupFieldTypeInd", Me, False, False, True, True, True, True, "Group Field Type", Nothing, MetaGroupField.MetaGroupFieldTypeIndOptions, "F")
  Public ReadOnly LoaderDataUpdateUseForInsertYN As New BooleanElement("LoaderDataUpdateUseForInsertYN", Me, False, True, True, True, True, "Use To Check For Record When Inserting?", Nothing, False, "Yes;No")
  Public ReadOnly LoaderDataUpdateUseForUpdateYN As New BooleanElement("LoaderDataUpdateUseForUpdateYN", Me, False, True, True, True, True, "Use To Check For Record When Updating?", Nothing, False, "Yes;No")
  Public ReadOnly MetaField As Relationship = New MetaFieldRelationship("MetaField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaGroup As Relationship = New MetaGroupRelationship("MetaGroup", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaFieldName As New StringElement("MetaFieldName", Me, True)
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _MetaGroupCode As New StringElement("MetaGroupCode", Me, True)
  Public ReadOnly _MetaField As MetaFieldRelationship = MetaField
  Public ReadOnly _MetaGroup As MetaGroupRelationship = MetaGroup
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moMetaGroupFieldTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property MetaGroupFieldTypeIndOptions() As IndicatorOptions
    Get
      If moMetaGroupFieldTypeIndOptions Is Nothing Then
        moMetaGroupFieldTypeIndOptions = New IndicatorOptions
        moMetaGroupFieldTypeIndOptions.Add("F", "File Format Field")
        moMetaGroupFieldTypeIndOptions.Add("V", "Assigned Value")
      End If
      Return moMetaGroupFieldTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("MetaGroupField", ApplicationSettings)
    _MetaField._MetaFieldName.LocalElement = _MetaFieldName
    _MetaField._MetaTableName.LocalElement = _MetaTableName
    _MetaField._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroup._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroup._MetaGroupCode.LocalElement = _MetaGroupCode
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
    AddHandler MetaField.LimitRelatedList, AddressOf MetaField_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub MetaField_LimitRelatedList(ByVal List As List)
    If Not Me.MetaGroup.IsEmpty Then
      Dim oMetaGroup As MetaGroup = Me.MetaGroup.Instance
      List.LimitByRelatedPart(oMetaGroup.MetaTable.Instance, "MetaTable", True)
    End If
  End Sub
#End Region

#Region " Change Events"
  Private Sub IncludeInChecksumYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckMetaDatabaseType()
    Dim oMetaGroup As MetaGroup = Me.MetaGroup.Instance
    Dim oMetaDatabase As MetaDatabase = oMetaGroup.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository, Constants.MetaDatabase.Types.Enquiry
        Throw New BusinessRuleException("You may not add Meta Group Fields as this Meta Database Type is not ""Credit Bureau""!")
    End Select
  End Sub


#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      .BeginTransaction()
      Try
        BRBeforeDelete()
        MyBase.Delete()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      InsertDefaultFileFormatGroupField(Me, ApplicationSettings)
    Else
      UpdateDefaultFileFormatGroupField(Me, ApplicationSettings)
    End If
  End Sub

  Public Sub BRBeforeDelete()
    DeleteDefaultFileFormatGroupField(Me, ApplicationSettings)
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertDefaultFileFormatGroupField(ByVal MetaGroupField As MetaGroupField, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spMetaGroupField_I_FileFormatMetaGroupField"

    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaGroupCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaTableName", SqlDbType.VarChar, 250)
    oCommand.Parameters.Add("@MetaFieldName", SqlDbType.VarChar, 250)

    oCommand.Parameters("@MetaDatabaseCode").Value = MetaGroupField._MetaDatabaseCode.Value
    oCommand.Parameters("@MetaGroupCode").Value = MetaGroupField._MetaGroupCode.Value
    oCommand.Parameters("@MetaTableName").Value = MetaGroupField._MetaTableName.Value
    oCommand.Parameters("@MetaFieldName").Value = MetaGroupField._MetaFieldName.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub UpdateDefaultFileFormatGroupField(ByVal MetaGroupField As MetaGroupField, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spMetaGroupField_U_FileFormatMetaGroupField"

    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaGroupCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaTableName", SqlDbType.VarChar, 250)
    oCommand.Parameters.Add("@MetaFieldName", SqlDbType.VarChar, 250)

    oCommand.Parameters("@MetaDatabaseCode").Value = MetaGroupField._MetaDatabaseCode.Value
    oCommand.Parameters("@MetaGroupCode").Value = MetaGroupField._MetaGroupCode.Value
    oCommand.Parameters("@MetaTableName").Value = MetaGroupField._MetaTableName.Value
    oCommand.Parameters("@MetaFieldName").Value = MetaGroupField._MetaFieldName.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub DeleteDefaultFileFormatGroupField(ByVal MetaGroupField As MetaGroupField, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spMetaGroupField_D_FileFormatMetaGroupField"

    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaGroupCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaTableName", SqlDbType.VarChar, 250)
    oCommand.Parameters.Add("@MetaFieldName", SqlDbType.VarChar, 250)

    oCommand.Parameters("@MetaDatabaseCode").Value = MetaGroupField._MetaDatabaseCode.Value
    oCommand.Parameters("@MetaGroupCode").Value = MetaGroupField._MetaGroupCode.Value
    oCommand.Parameters("@MetaTableName").Value = MetaGroupField._MetaTableName.Value
    oCommand.Parameters("@MetaFieldName").Value = MetaGroupField._MetaFieldName.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class MetaGroupFieldRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _MetaFieldName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaTableName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaGroupCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New MetaGroupField(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _MetaFieldName.ForeignElement = CType(Instance, MetaGroupField)._MetaFieldName
    _MetaTableName.ForeignElement = CType(Instance, MetaGroupField)._MetaTableName
    _MetaDatabaseCode.ForeignElement = CType(Instance, MetaGroupField)._MetaDatabaseCode
    _MetaGroupCode.ForeignElement = CType(Instance, MetaGroupField)._MetaGroupCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaGroupField")
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaGroupField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, False, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaGroupField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0MetaGroupField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 0, False, 100, "Meta Field Name"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0MetaGroupField].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, False, 100, "Meta Group Code"))
    oList.Columns.Add(New ListColumn("MetaFieldName1", "[A1MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, False, 2, True, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldDesc", "[A1MetaField].[MetaFieldDesc]", "MetaFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description"))
    oList.Columns.Add(New ListColumn("MetaGroupFieldTypeInd", "[A0MetaGroupField].[MetaGroupFieldTypeInd]", "MetaGroupFieldTypeInd", DataType.String, Nothing, False, 0, True, 100, "Group Field Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "([MetaGroupField]  [A0MetaGroupField]" + _
           "    join [MetaField]  [A1MetaField] on [A0MetaGroupField].[MetaFieldName] = [A1MetaField].[MetaFieldName] and [A0MetaGroupField].[MetaTableName] = [A1MetaField].[MetaTableName] and [A0MetaGroupField].[MetaDatabaseCode] = [A1MetaField].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaGroupField")
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0MetaGroupField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 0, False, 100, "Meta Field Name"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaGroupField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, False, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaGroupField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0MetaGroupField].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, False, 100, "Meta Group Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaGroupField]  [A0MetaGroupField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
