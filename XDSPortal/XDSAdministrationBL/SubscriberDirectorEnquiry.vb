Imports sembleWare.Runtime
Imports System
Public Class SubscriberDirectorEnquiry 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberDirectorEnquiryID As New IdentityElement("SubscriberDirectorEnquiryID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, True, "Enquiry Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryStatusInd As New IndicatorElement("EnquiryStatusInd", Me, False, False, False, True, True, True, "Enquiry Status", Nothing, SubscriberDirectorEnquiry.EnquiryStatusIndOptions, "Q")
  Public ReadOnly EnquiryResultInd As New IndicatorElement("EnquiryResultInd", Me, False, False, False, True, True, True, "Enquiry Result", Nothing, SubscriberDirectorEnquiry.EnquiryResultIndOptions, "P")
  Public ReadOnly ProductPointValue As New DecimalElement("ProductPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, "External Reference", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MaidenName As New StringElement("MaidenName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, "Gender", Nothing, SubscriberDirectorEnquiry.GenderIndOptions, Nothing)
  Public ReadOnly ResultIDNo As New StringElement("ResultIDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultPassportNo As New StringElement("ResultPassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultFirstInitial As New StringElement("ResultFirstInitial", Me, False, False, True, True, False, 1, "First Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSecondInitial As New StringElement("ResultSecondInitial", Me, False, False, True, True, False, 1, "Second Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultFirstName As New StringElement("ResultFirstName", Me, False, False, True, True, False, 150, "First Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSecondName As New StringElement("ResultSecondName", Me, False, False, True, True, False, 150, "Second Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSurname As New StringElement("ResultSurname", Me, False, False, True, True, False, 150, "Surname", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultBirthDate As New DateTimeElement("ResultBirthDate", Me, False, False, True, True, False, "Birth Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ResultGenderInd As New IndicatorElement("ResultGenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, SubscriberDirectorEnquiry.ResultGenderIndOptions, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberDirectorEnquiryResult_OwnMany As Relationship = New SubscriberDirectorEnquiryResultRelationship("SubscriberDirectorEnquiryResult", Nothing, "SubscriberDirectorEnquiry", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberDirectorEnquiryResult_OwnMany As SubscriberDirectorEnquiryResultRelationship = SubscriberDirectorEnquiryResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moEnquiryStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryStatusIndOptions() As IndicatorOptions
    Get
      If moEnquiryStatusIndOptions Is Nothing Then
        moEnquiryStatusIndOptions = New IndicatorOptions
        moEnquiryStatusIndOptions.Add("Q", "Queued")
        moEnquiryStatusIndOptions.Add("P", "Processing")
        moEnquiryStatusIndOptions.Add("C", "Completed")
      End If
      Return moEnquiryStatusIndOptions
    End Get
  End Property
  Private Shared moEnquiryResultIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryResultIndOptions() As IndicatorOptions
    Get
      If moEnquiryResultIndOptions Is Nothing Then
        moEnquiryResultIndOptions = New IndicatorOptions
        moEnquiryResultIndOptions.Add("P", "Pending")
        moEnquiryResultIndOptions.Add("N", "No Record Found")
        moEnquiryResultIndOptions.Add("F", "Record Found")
        moEnquiryResultIndOptions.Add("M", "Multiple Records Found")
        moEnquiryResultIndOptions.Add("S", "Record Selected")
      End If
      Return moEnquiryResultIndOptions
    End Get
  End Property
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moResultGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ResultGenderIndOptions() As IndicatorOptions
    Get
      If moResultGenderIndOptions Is Nothing Then
        moResultGenderIndOptions = New IndicatorOptions
        moResultGenderIndOptions.Add("M", "Male")
        moResultGenderIndOptions.Add("F", "Female")
      End If
      Return moResultGenderIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberDirectorEnquiry", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler FirstName.Changed, AddressOf FirstName_Changed
    AddHandler SecondName.Changed, AddressOf SecondName_Changed
    AddHandler Product.Changed, AddressOf Product_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Product_Changed(ByVal Relationship As Relationship)
    BRProduct(Relationship, True, True)
  End Sub

  Private Sub FirstName_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRFirstName(Element, True, True)
  End Sub

  Private Sub SecondName_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRSecondName(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Validate()
    Dim bAllowYN As Boolean = False
    Dim oException As Exception

    Select Case Me._ProductID.Value
      Case Constants.Product.Records.EnquiryCommercialDirector
        oException = New MissingValuesException("(ID No) or (Passport No / Other ID No) or (Surname and First Initial and Date of Birth)", MyBase.Caption, "")
        If Not Me.IDNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.PassportNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.Surname.IsEmpty And Not Me.FirstInitial.IsEmpty And Not Me.BirthDate.IsEmpty Then
          bAllowYN = True
        End If
      Case Else
        bAllowYN = True
    End Select

    If Not bAllowYN Then
      Throw New BusinessRuleException(oException.Message)
    End If
  End Sub

  Public Sub QueueMatch()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberDirectorEnquiry.Statuses.Queued
    Me.Save()
  End Sub

  Public Sub Match()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberDirectorEnquiry.Statuses.Processing
    Me.Save()

    Dim oDataRow As DataRow
    Dim nEnquiryMatchingEngineID As Integer = EnquiryMatchingEngine.DirectorMatch(Me.IDNo, Me.Surname, Me.FirstInitial, Me.FirstName, _
                                                                                  Me.SecondInitial, Me.SecondName, Me.BirthDate, _
                                                                                  Me._ProductID.Value, ApplicationSettings)
    Dim oString As Text.StringBuilder = New Text.StringBuilder

    oString.Append("insert into SubscriberDirectorEnquiryResult(SubscriberID, SubscriberDirectorEnquiryID, DirectorID, DetailsViewedYN, DirectorSelectedYN, IDNo, FirstInitial, SecondInitial, FirstName, Surname, BirthDate)" & vbNewLine)
    oString.Append("select " & Me._SubscriberID.Value & ", " & Me.SubscriberDirectorEnquiryID.Value & ", edr.DirectorID, 0, 0, d.IDNo, d.FirstInitial, d.SecondInitial, d.FirstName, d.Surname, d.BirthDate" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineDirectorResult edr inner join EnquiryMatchingEngineDirector med")
    oString.Append("                                                  on edr.EnquiryMatchingEngineID = med.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                 and edr.EnquiryMatchingEngineDirectorID = med.EnquiryMatchingEngineDirectorID" & vbNewLine)
    oString.Append("                                               inner join Director d" & vbNewLine)
    oString.Append("                                                  on edr.DirectorID = d.DirectorID" & vbNewLine)
    oString.Append(" where med.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and edr.IgnoreRecordYN = 0")
    ApplicationSettings.ActionQuery(oString.ToString())

    oString = New Text.StringBuilder
    oString.Append("select count(edr.DirectorID)" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineDirectorResult edr inner join EnquiryMatchingEngineDirector med" & vbNewLine)
    oString.Append("                                                  on edr.EnquiryMatchingEngineID = med.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                 and edr.EnquiryMatchingEngineDirectorID = med.EnquiryMatchingEngineDirectorID" & vbNewLine)
    oString.Append(" where med.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and edr.IgnoreRecordYN = 0")
    For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
      Select Case System.Convert.ToInt32(oDataRow(0))
        Case 0
          Me.EnquiryResultInd.Value = Constants.SubscriberDirectorEnquiry.Results.NoRecordFound
        Case 1
          Me.EnquiryResultInd.Value = Constants.SubscriberDirectorEnquiry.Results.RecordFound
        Case Else
          Me.EnquiryResultInd.Value = Constants.SubscriberDirectorEnquiry.Results.MultipleRecordsFound
      End Select
    Next

    Me.EnquiryStatusInd.Value = Constants.SubscriberDirectorEnquiry.Statuses.Completed
    Me.Save()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now
      ' If SystemUser is not empty, then the XDSAdministration Services has populated this value.
      If SystemUser.IsEmpty Then
        Me.SystemUser.Instance = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If

      Me.ResultIDNo.Value = Me.IDNo.Value
      Me.ResultPassportNo.ValueAsObject = Me.PassportNo.ValueAsObject
      Me.ResultFirstInitial.ValueAsObject = Me.FirstInitial.ValueAsObject
      Me.ResultSecondInitial.ValueAsObject = Me.SecondInitial.ValueAsObject
      Me.ResultSurname.ValueAsObject = Me.Surname.ValueAsObject
      Me.ResultBirthDate.ValueAsObject = Me.BirthDate.ValueAsObject
      Me.ResultGenderInd.Value = Me.GenderInd.Value
    End If
    XDSAdministrationBL.Consumer.ValidateIDNo(Me.IDNo.Value, ApplicationSettings)
  End Sub

  Private Sub BRProduct(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oProduct As Product = Nothing

    If Not Relationship.IsEmpty Then
      oProduct = Relationship.Instance
    End If
    Utility.BRProductTraceDirector(oProduct, Me.IDNo, Me.Surname, Me.FirstInitial, _
                                   Me.SecondInitial, Me.FirstName, Me.SecondName, _
                                   Me.BirthDate, SetEnableDisableYN, PerformRulesYN)

    If PerformRulesYN Then
      If Not oProduct Is Nothing Then
        Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
        Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfileProduct.GetSubscriberProfileProduct(Me._SubscriberID.Value, oProduct.ProductID.Value, ApplicationSettings)

        If oSubscriberProfileProduct.OverrideDefaultPointValueYN.Value Then
          Me.ProductPointValue.Value = oSubscriberProfileProduct.OverridePointValue.Value
        Else
          Me.ProductPointValue.Value = oProduct.DefaultPointValue.Value
        End If
      End If
    End If
  End Sub

  Private Sub BRFirstName(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Not Element.IsEmpty Then
        Dim sFirstName As String = Element.ValueAsObject

        Me.FirstInitial.Value = sFirstName.Substring(0, 1)
      End If
    End If
  End Sub

  Private Sub BRSecondName(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Not Element.IsEmpty Then
        Dim sSecondName As String = Element.ValueAsObject

        Me.SecondInitial.Value = sSecondName.Substring(0, 1)
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Function GetSubscriberDirectorEnquiryResult() As SubscriberDirectorEnquiryResult
    If Me.EnquiryResultInd.Value = Constants.SubscriberDirectorEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberDirectorEnquiry.Results.RecordSelected Then
      Dim oList As List = Me._SubscriberDirectorEnquiryResult_OwnMany.GridList
      Dim oDataTable As DataTable = oList.DataTable
      Dim oDataRow As DataRow = oDataTable.Rows(0)
      Dim oSubscriberDirectorEnquiryResult As SubscriberDirectorEnquiryResult = Me.SubscriberDirectorEnquiryResult_OwnMany.NewInstance()

      oSubscriberDirectorEnquiryResult._DirectorID.Load(oDataRow("DirectorID"))
      oSubscriberDirectorEnquiryResult.Load()
      Return oSubscriberDirectorEnquiryResult
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberDirectorEnquiryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberDirectorEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberDirectorEnquiry(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberDirectorEnquiryID.ForeignElement = CType(Instance, SubscriberDirectorEnquiry).SubscriberDirectorEnquiryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberDirectorEnquiry)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function MultipleDirectorGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberDirectorEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberDirectorEnquiryID", "[A0SubscriberDirectorEnquiry].[SubscriberDirectorEnquiryID]", "SubscriberDirectorEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Director Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberDirectorEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberDirectorEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A0SubscriberDirectorEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultPassportNo", "[A0SubscriberDirectorEnquiry].[ResultPassportNo]", "ResultPassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A0SubscriberDirectorEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultFirstInitial", "[A0SubscriberDirectorEnquiry].[ResultFirstInitial]", "ResultFirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A0SubscriberDirectorEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A0SubscriberDirectorEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A0SubscriberDirectorEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberDirectorEnquiry]  [A0SubscriberDirectorEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
