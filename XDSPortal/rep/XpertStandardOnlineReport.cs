using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XdsPortalReports
{
    public partial class XpertStandardOnlineReport : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet dsXML = new DataSet();
        public XpertStandardOnlineReport(string strXML)
        {
            InitializeComponent();
            //System.IO.StringReader Objsr = new System.IO.StringReader(strXML);

            //dsXML.ReadXml(Objsr);
            DataSet dsXML = new DataSet();
            System.IO.StringReader Objsr = new System.IO.StringReader(strXML.Replace("''", "'"));
            dsXML.ReadXml(Objsr);


            if (dsXML.Tables.Contains("CommercialScoring"))
            {
                DateTime dt = DateTime.Parse(dsXML.Tables["CommercialScoring"].Rows[0]["ScoreDate"].ToString());
                dsXML.Tables["CommercialScoring"].Rows[0]["ScoreDate"] = dt.ToShortDateString();
            }

            if (dsXML.Tables.Contains("SubscriberInputDetails"))
            {
                dsXML.Tables["SubscriberInputDetails"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate = '', '1900-01-01T00:00:00', EnquiryDate)");
                this.lblEnquiryDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - HH:mm:ss}") });
            }

            if (!(dsXML.Tables.Contains("CommercialBusinessInformation")))
            {
                DCommBusinfo.Visible = false;
                lblDetailedBusInfoH.Text += " - (No Data Available)";
            }
            else
            {
                dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FSIC", System.Type.GetType("System.String"), "Iif(SIC = '0 - Unkown Data', 'No Information Available',SIC)");
                this.lblSICCodeD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FSIC") });
                dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(AuthorisedCapitalAmt='',0,AuthorisedCapitalAmt)");
                this.LblAuthorisedcapitalD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FAmount", "{0:c}") });
                dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FBmount", System.Type.GetType("System.Double"), "Iif(IssuedCapitalAmt='',0,IssuedCapitalAmt)");
                this.LblIssuedCapitalAmntD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FBmount", "{0:c}") });
            }
            if (!(dsXML.Tables.Contains("CommercialPossibleNames")))
            {
                DCommNames.Visible = false;
                lblCommNamesH.Text += " - (No Data Available)";

                // this.GroupHeader15.HeightF = 50F;
            }

            if (!(dsXML.Tables.Contains("CommercialVATInformation")))
            {
                DCommVATInfo.Visible = false;
                lblVATInfoH.Text += " - (No Data Available)";
                tblTAXInfoC.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialJudgment")))
            {
                //DetailJudgements.Visible = false;
                lblJudgmentsH.Text += " - (No Data Available)";
                tblCJud.Visible = false;
                DCommJudgSumm.Visible = false;
            }
            else
            {
                // dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["CommercialJudgment"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.lblJDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialJudgment.FAmount", "{0:c}") });
            }

            if (!(dsXML.Tables.Contains("CommercialPossibleJudgment")))
            {
                //DetailJudgements.Visible = false;
                lblposJudgmentsH.Text += " - (No Data Available)";
                tbPosslCJud.Visible = false;
                DCommPosJudg.Visible = false;
                tbPosslCJud.Visible = false;
               
            }
            else
            {
                // dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["CommercialPossibleJudgment"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.LblPosJDAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialPossibleJudgment.FAmount", "{0:c}") });
            }

            if (!(dsXML.Tables.Contains("CommercialAuditorInformation")))
            {
                DCommAUDInfo.Visible = false;
                lblAUDInfoH.Text += " - (No Data Available)";
                lblAuditorH.Visible = false;
            }

            if (!(dsXML.Tables.Contains("CommercialTradeReferencesInformation")))
            {
                DRTradeReference.Visible = false;
            }
            else
            {
                dsXML.Tables["CommercialTradeReferencesInformation"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(MaxLimit='',0,MaxLimit)");
                this.lblMaxCreditLmt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialTradeReferencesInformation.FAmount", "{0:c}") });

                dsXML.Tables["CommercialTradeReferencesInformation"].Columns.Add("FBmount", System.Type.GetType("System.Double"), "Iif(SuretyValue='',0,SuretyValue)");
                this.lblSuretyAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialTradeReferencesInformation.FBmount", "{0:c}") });

                dsXML.Tables["CommercialTradeReferencesInformation"].Columns.Add("FCmount", System.Type.GetType("System.Double"), "Iif(CreditLimit='',0,CreditLimit)");
                this.lblCreditLmt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialTradeReferencesInformation.FCmount", "{0:c}") });

                //dsXML.Tables["CommercialTradeReferencesInformation"].Columns.Add("FDmount", System.Type.GetType("System.Double"), "Iif(AveragePurchases='',0,AveragePurchases)");
                //this.lblAvgPurch.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialTradeReferencesInformation.FDmount", "{0:c}") });
           
            
            }
            if (!(dsXML.Tables.Contains("CommercialBankDetails")))
            {
                DRBankInformation.Visible = false;
            }
            else
            {
                this.lblBankAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBankCodeHistory.FAmount", "{0:c}") });
            
            }
            if ((!(dsXML.Tables.Contains("XDSCommercialAddressHistory"))) && (!(dsXML.Tables.Contains("XDSCommercialContactHistory"))))
            {
                ContactDataH.Visible = false;
            }
            if (!(dsXML.Tables.Contains("XDSCommercialAddressHistory")))
            {
                DRCommAddress.Visible = false;
            }
            if (!(dsXML.Tables.Contains("XDSCommercialContactHistory")))
            {
                DRCommContactHistory.Visible = false;
            }
            if (!(dsXML.Tables.Contains("XDSCommercialPrincipalInformation")))
            {
            DRXDSPrincipals.Visible= false;
            }
            if (!(dsXML.Tables.Contains("CommercialBranchInformation")))
            {
                DCommBranchInfo.Visible = false;
                lblBranchH.Text += " - (No Data Available)";
                tblBranchInfoC.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialDivisionInformation")))
            {
                DCommDivisionInfo.Visible = false;
                lblDivisionInfoH.Text += " - (No Data Available)";
                tblDivisonC.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialPropertyInformation")))
            {
                DCommDeedsInfo.Visible = false;
                lblPropInterestsH.Text += " - (No Data Available)";
            }
            else 
            {
                dsXML.Tables["CommercialPropertyInformation"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(PurchasePriceAmt='',0,PurchasePriceAmt)");
                this.lblpurchaseprc.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialPropertyInformation.FAmount", "{0:c}") });

                dsXML.Tables["CommercialPropertyInformation"].Columns.Add("FBmount", System.Type.GetType("System.Double"), "Iif(BondAmt='',0,BondAmt)");
                this.lblBondAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialPropertyInformation.FBmount", "{0:c}") });
           
            
            }

            if (!(dsXML.Tables.Contains("CommercialBankCodeHistory")))
            {
                BankcodeHistory.Visible = false; 
            }
            if (!(dsXML.Tables.Contains("CommercialBankDetails")))
            {
                BankDetails.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialAccountVerification")))
            {
                AccountVerificationDetail.Visible = false;               
            }

            if (!(dsXML.Tables.Contains("CommercialInactivePrincipalInformation")))
            {
                InactivePrincipalInformationDetail.Visible = false;
            }
            
            if (!(dsXML.Tables.Contains("BusinessEnquiryHistory")))
            {
                DEnquiryHistory.Visible = false;
                lblEnquiryHistoryH.Text += " - (No Data Available)";
                tblEnquiryC.Visible = false;
            }

          

            #region Active principals
     

            if (!(dsXML.Tables.Contains("CommercialActivePrincipalInformation")))
            {
                lblPrincipalInfoH.Text += " - (No Data Available)";
                DCommDIRInfo.Visible = false;
                DRDirJudgInfo.Visible = false;
                DRDirCurrBusInterests.Visible = false;
                DRDirPrevBusInterests.Visible = false;
                tblDirPrincipalInfoSummary.Visible = false;
                DDirAddressHistory.Visible = false;
                DDirContactInfo.Visible = false;
                DDirPaymentNotification.Visible = false;
                DDirDefaults.Visible = false;
                DRDirPropertyInterests.Visible = false;
                DRDirDebtReviewInfo.Visible = false;
                ContactHeader.Visible = false;
            }
            else
            {
            

                if (!(dsXML.Tables.Contains("ActiveDirectorAddressHistory")))
                {
                    tblAddressHeader.Visible = false;
                    tlbDirectorAddress.Visible = false;
                    tblDirectorAddressD.Visible = false;                  

                }
               

                else
                {
                    DataRelation drDirAddress = new DataRelation("DirAddress", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorAddressHistory"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drDirAddress);
                    this.DDirAddressHistory.DataMember = "CommercialActivePrincipalInformation.DirAddress";

                    this.lblDirAddType.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.AddressTypeInd") });
                    this.lblDirAddLine1.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.Address1") });
                    this.lblDirAddLine2.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.Address2") });
                    this.lblDirAddLine3.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.Address3") });
                    this.lblDirAddLine4.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.Address4") });
                    this.lblDirAddPostalCode.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.PostalCode") });
                    this.lblDirAddCreatedOnDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirAddress.CreatedOnDate") });
                }


                if ((!(dsXML.Tables.Contains("ActiveDirectorContactHistory"))))
                {
                    GroupHeader22.Visible = false;
                    

                }
                
                else
                {

                    DataRelation drDirCont = new DataRelation("DirContactInfo", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorContactHistory"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drDirCont);
                    this.DDirContactInfo.DataMember = "CommercialActivePrincipalInformation.DirContactInfo";

                    this.lblDirConCreatedOndate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirContactInfo.BureauUpdate") });
                    this.lblDirConType.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirContactInfo.ContactType") });
                    this.lblDirConDetail.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirContactInfo.Detail") });
                }

                if (!(dsXML.Tables.Contains("ActiveDirectorPaymentNotification")))
                {
                    DDirecPaymentNotifocationD.Visible = false;
                    DDirecPaymentNotificationH.Visible = false;                    
                    tblDirectorPaymentNotD.Visible = false;
                    lblDPaymentNotNoInfo.Visible = true;
                }
                else
                {

                    DataRelation drPayNot = new DataRelation("DirPaymentNotification", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorPaymentNotification"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drPayNot);
                    this.DDirPaymentNotification.DataMember = "CommercialActivePrincipalInformation.DirPaymentNotification";

                    this.lblDirPaymentAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialActivePrincipalInformation.DirPaymentNotification.FAmount", "{0:c}") });
                    dsXML.Tables["DirectorPaymentNotification"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                    this.lblDirPaymentAccNo.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.AccountNo") });
                    this.lblDirPaymentAccType.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.AccountType") });
                    this.lblDirPaymentComment.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.Comments") });
                    this.lblDirPaymentDateLoaded.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.DateLoaded") });
                    this.lblDirPaymentFirstName.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.FirstName") });
                    this.lblDirPaymentIDNo.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.IDNo") });
                    this.lblDirPaymentStatus.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.Statuscode") });
                    this.lblDirPaymentSub.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.Company") });
                    this.lblDirPaymentSurname.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.Surname") });
                    this.lblDirPaymentAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirPaymentNotification.Amount") });
                }


                if (!(dsXML.Tables.Contains("ActiveDirectorDefaultAlert")))
                {
                    DDirDefaultAlerts.Visible = false;
                    DDirectorDefaultsD.Visible = false;
                    DDirectorDefaultsH.Visible = false;
                    lblDDefaultsNoInfo.Visible = true;
                }
                else
                {
                    DataRelation drDirDef = new DataRelation("DirDefaultAlert", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorDefaultAlert"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drDirDef);
                    this.DDirDefaults.DataMember = "CommercialActivePrincipalInformation.DirDefaultAlert";

                    this.LblDirDefaultAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialActivePrincipalInformation.DirDefaultAlert.FAmount", "{0:c}") });
                    dsXML.Tables["DirectorDefaultAlert"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                    this.LblDirDefaultAccNo.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.AccountNo") });
                    this.LblDirDefaultAccType.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.AccountType") });
                    this.LblDirDefaultComment.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.Comments") });
                    this.LblDirDefaultDateloaded.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.DateLoaded") });
                    this.LblDirDefaultFirstName.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.FirstName") });
                    this.LblDirDefaultIDNo.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.IDNo") });
                    this.LblDirDefaultStatus.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.Statuscode") });
                    this.LblDirDefaultSub.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.Company") });
                    this.LblDirDefaultSurname.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.Surname") });
                    this.LblDirDefaultAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDefaultAlert.Amount") });
               
                }
                    
                if (!(dsXML.Tables.Contains("ActiveDirectorJudgments")))
                {
                    tblDirJudgD.Visible=false;
                    lblDJudgmentsNoInfo.Visible = true;
                    DDirectorJudgement.Visible=false;
                  

                }
               
                else
                {
                    dsXML.Tables["ActiveDirectorJudgments"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                    DataRelation drdirjudg = new DataRelation("DirJudgInfo", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorJudgments"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drdirjudg);
                    this.DRDirJudgInfo.DataMember = "CommercialActivePrincipalInformation.DirJudgInfo";

                    this.lblDirJudgAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.FAmount", "{0:c}") });
                    dsXML.Tables["ActiveDirectorJudgments"].Columns.Add("FBmount", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");
                    
                    this.lblDirJudgAttorneyD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.AttorneyName") });
                    this.lblDirJudgAttorneyNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.TelephoneNo") });
                    this.lblDirJudgcasenoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.CaseNumber") });
                    this.lblDirJudgReasonD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.CaseReason") });
                    this.lblDirJudgCourtD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.CourtName") });
                    this.lblDirJudgJudDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.JudgmentDate") });
                    this.lblDirJudgPlaintiffD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.PlaintiffName") });
                    this.lblDirJudgTypeD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.CaseType") });
                    this.lblDirJudgIDnoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.IDNo") });
                    this.lblDirJudgFirstnameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.FirstName") });
                    this.lblDirJudgSurnameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirJudgInfo.Surname") });
                }

                if (!(dsXML.Tables.Contains("ActiveDirectorDebtReview")))
                {
                    DDirectorDebtRD.Visible = false;
                    lblDDebtReveiwNoInfo.Visible = true;
                    
                }      
                else
                {
                    DataRelation drDebtRvw = new DataRelation("DirDebtReviewInfo", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorDebtReview"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drDebtRvw);
                    this.DRDirDebtReviewInfo.DataMember = "CommercialActivePrincipalInformation.DirDebtReviewInfo";

                    this.lblDebtCounsellorNameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.DebtCounselorName") });
                    this.lblDebtCounsellorNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.DebtCounselorContactNo") });
                    this.lblDebtRvwDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.ApplicationCreationDate") });
                    this.lblDebtStatusD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.Status") });
                    this.lblDebtStatusDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.StatusDate") });
                    this.lblDebtRvwIDNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.IDNo") });
                    this.lblDebtRvwNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.DebtRevierwNumber") });
                    this.lblDebtRvwSurnameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.Surname") });
                    this.lblDebtRvwFirstNameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.FirstName") });
                    this.lblDebtCounsellorRegNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.DirDebtReviewInfo.DebtCounsellorRegNo") });
               
                }

                if (!(dsXML.Tables.Contains("ActiveDirectorCurrentBusinessinterests")))
                {
                    DDirCurrBusInterests.Visible = false;
                    tblCurrentBusInterestC.Visible = false;
                    lblDCurrentBusNoInfo.Visible = true;

                }

                else
                {
                    DataRelation drCurrentBusInterests = new DataRelation("CurrentBusInterests", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorCurrentBusinessinterests"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drCurrentBusInterests);
                    this.DRDirCurrBusInterests.DataMember = "CommercialActivePrincipalInformation.CurrentBusInterests";

                    this.lblDCCompanyNameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.CurrentBusInterests.CommercialName") });
                    this.lblDCRegNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.CurrentBusInterests.RegistrationNo") });
                    this.lblDCStatusD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.CurrentBusInterests.CommercialStatus") });
                    this.lblDcJudgD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.CurrentBusInterests.JudgmentsCount") });
                    this.lblDcAgeOfBusD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.CurrentBusInterests.AgeOfBusiness") });
                    this.lblDcDefaultsD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.CurrentBusInterests.DefaultCount") });

                }


                if (!(dsXML.Tables.Contains("ActiveDirectorPreviousBusinessInterests")))
                {
                    // DDirPrevBusInterests.Visible = false;
                   
                    
                    DDirPrevBusInterests.Visible = false;
                    tblPrevBusIntD.Visible = false;
                    lblDPreviousBusNoInfo.Visible = true;

                }

                else
                {                    
                    
                    DataRelation drPreviousBusInterests = new DataRelation("PreviousBusInterests", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorPreviousBusinessInterests"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drPreviousBusInterests);
                    this.DRDirPrevBusInterests.DataMember = "CommercialActivePrincipalInformation.PreviousBusInterests";

                    this.lblDPCompanyNameD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PreviousBusInterests.CommercialName") });
                    this.lblDPRegNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PreviousBusInterests.RegistrationNo") });
                    this.lblDPStatusD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PreviousBusInterests.CommercialStatus") });
                    this.lblDPJudgD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PreviousBusInterests.JudgmentsCount") });
                    this.lblDPDefaultsD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PreviousBusInterests.DefaultCount") });
                    this.lblDPAgeOfBusD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PreviousBusInterests.AgeOfBusiness") });


                }

                if (!(dsXML.Tables.Contains("ActiveDirectorPropertyInterests")))
                {
                    DirPropertyInterests.Visible = false;
                    lblDirPropertyInterests.Text += " - (No Data Available)";
                }
                else
                {
                    DataRelation drDirPropertyInterests = new DataRelation("PropertyInterest", dsXML.Tables["CommercialActivePrincipalInformation"].Columns["DirectorID"], dsXML.Tables["ActiveDirectorPropertyInterests"].Columns["DirectorID"]);
                    dsXML.Relations.Add(drDirPropertyInterests);
                    this.DRDirPropertyInterests.DataMember = "CommercialActivePrincipalInformation.PropertyInterest";

                    this.lblDirProBondAmntD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.FAmount", "{0:c}") });
                    dsXML.Tables["ActiveDirectorPropertyInterests"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(BondAmt='',0,BondAmt)");

                    this.lblDirProPurchasePriceD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.Fbmount", "{0:c}") });
                    dsXML.Tables["ActiveDirectorPropertyInterests"].Columns.Add("Fbmount", System.Type.GetType("System.Double"), "Iif(PurchasePriceAmt='',0,PurchasePriceAmt)");

                    this.lblDirProBondHolderD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.BondHolderName") });
                    this.lblDirProDeedNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.TitleDeedNo") });
                    this.lblDirProDeedOffD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.DeedsOffice") });
                    this.lblDirProDeedTypeD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.PropertyTypeDesc") });
                    this.lblDirProErfSizedD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.ErfNo") });
                    this.lblDirProExtSizeD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.ErfSize") });
                    this.lblDirProFarmD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.FarmName") });
                    this.lblDirProPhysicalAddresD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.PhysicalAddress") });
                    this.lblDirProPortionNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.PortionNo") });
                    this.lblDirProPurchaseDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.PurchaseDate") });
                    this.lblDirProSchemeNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.SchemeName") });
                    this.lblDirProSharePecD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.BuyerSharePerc") });
                    this.lblDirProStandNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.StandNo") });
                    this.lblDirProTownshipD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.TownshipName") });
                    this.lblDirProTransfereDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.TransferDate") });
                    this.lblDirProBondNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", null, "CommercialActivePrincipalInformation.PropertyInterest.BondAccountNo") });
                   
                }


            }
            #endregion

           

            if (!(dsXML.Tables.Contains("XDSPaymentNotification")))
            {
                tblPmtNotificationD.Visible = false;
                lblPaymentNotificationsH.Text += " - (No Data Available)";
                tblPmtNotificationC.Visible = false;
            }
            else
            {
                dsXML.Tables["XDSPaymentNotification"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                this.lblPmtAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "XDSPaymentNotification.FAmount", "{0:c}") });

            }
            if (!(dsXML.Tables.Contains("CommercialDefaultAlert")))
            {
                DDefaultListing.Visible = false;
                lblDefaultAlertH.Text += " - (No Data Available)";
                tblDefaultAlertC.Visible = false;
            }
            else
            {
                dsXML.Tables["CommercialDefaultAlert"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                this.lblDAAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialDefaultAlert.FAmount", "{0:c}") });
            }
            if (!(dsXML.Tables.Contains("BusinessRescue")))
            {
                DRescue.Visible = false;
                lblBusinessRescueH.Text += " - (No Data Available)";
                tblCRescue.Visible = false;
            }

            this.DataSource = dsXML;
            dsXML.Dispose();
        }

       
        private void tblDirectorAddressContactD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.lblDirAddCreatedOnDate.Text == string.Empty && this.lblDirAddType.Text == string.Empty && this.lblDirAddLine1.Text == string.Empty &&
                this.lblDirAddLine2.Text == string.Empty && this.lblDirAddLine3.Text == string.Empty && this.lblDirAddLine4.Text == string.Empty
                && this.lblDirAddPostalCode.Text == string.Empty && this.lblDirConCreatedOndate.Text == string.Empty &&
                this.lblDirConDetail.Text == string.Empty && this.lblDirConType.Text == string.Empty)
            {
            lblContactAddressH.Visible = true;


            }
            else
            {
              lblContactAddressH.Visible = true;


            }

        }
        private void tblDirectorAddressD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.lblDirAddCreatedOnDate.Text == string.Empty && this.lblDirAddType.Text == string.Empty && this.lblDirAddLine1.Text == string.Empty &&
                this.lblDirAddLine2.Text == string.Empty && this.lblDirAddLine3.Text == string.Empty && this.lblDirAddLine4.Text == string.Empty
                && this.lblDirAddPostalCode.Text == string.Empty) 
            {
                
               
               tblDirectorAddressD.Visible = false;
               //tlbDirectorAddress.Visible = true;
               lblDirAddressNoInfo.Visible = true;

            }
            else
            {

                tblDirectorAddressD.Visible = true;
               // tlbDirectorAddress.Visible = false;
                lblDirAddressNoInfo.Visible = false;

            }
        }

        private void tblDirectorContactDetailsD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.lblDirConCreatedOndate.Text == string.Empty &&
                this.lblDirConDetail.Text == string.Empty && this.lblDirConType.Text == string.Empty)
            {

                
                tblDirectorContactD.Visible = false;
                //tblDirectorContact.Visible = true;
                lblDirectorContactNoInfo.Visible = true;
                
            }

            else
            {
                tblDirectorContactD.Visible = true;
                //tblDirectorContact.Visible = false;
                lblDirectorContactNoInfo.Visible = false;
               
            }
        }
        private void tblDirectorPaymentNotD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.lblDirPaymentAccNo.Text == string.Empty && this.lblDirPaymentAccType.Text == string.Empty && this.lblDirPaymentAmnt.Text == string.Empty && this.lblDirPaymentComment.Text == string.Empty && this.lblDirPaymentDateLoaded.Text == string.Empty && this.lblDirPaymentFirstName.Text == string.Empty && this.lblDirPaymentIDNo.Text == string.Empty && this.lblDirPaymentStatus.Text == string.Empty && this.lblDirPaymentSurname.Text == string.Empty)
            {
                tblDirectorPaymentNotD.Visible = true;
                lblDPaymentNotNoInfo.Visible = true;

            }
            else
            {
                tblDirectorPaymentNotD.Visible = false;
                lblDPaymentNotNoInfo.Visible = false;
            }

        }


        private void tblDEfaultsD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.LblDirDefaultAccNo.Text == string.Empty && this.LblDirDefaultAccType.Text == string.Empty && this.LblDirDefaultAmnt.Text == string.Empty && this.LblDirDefaultComment.Text == string.Empty && this.LblDirDefaultDateloaded.Text == string.Empty && this.LblDirDefaultFirstName.Text == string.Empty && this.LblDirDefaultIDNo.Text == string.Empty && this.LblDirDefaultStatus.Text == string.Empty && this.LblDirDefaultSub.Text == string.Empty && LblDirDefaultSurname.Text == string.Empty)
            {
              DDirectorDefaultsD.Visible = false;
              lblDDefaultsNoInfo.Visible = true;
               
            }
            else
            {
                DDirectorDefaultsD.Visible = true;
                lblDDefaultsNoInfo.Visible = false;
            }

        }
        
        private void tblDirJudgD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.lblDirJudgAmountD.Text == string.Empty && this.lblDirJudgAttorneyD.Text == string.Empty && this.lblDirJudgcasenoD.Text == string.Empty && this.lblDirJudgCourtD.Text == string.Empty && this.lblDirJudgPlaintiffD.Text == string.Empty && this.lblDirJudgTypeD.Text == string.Empty && this.lblDirJudgAttorneyNoD.Text == string.Empty)
            {
                tblDirJudgD.Visible = false;
                lblDJudgmentsNoInfo.Visible = true;

            }
            else
            {
                tblDirJudgD.Visible = true;
               lblDJudgmentsNoInfo.Visible = false;
            }

        }

        private void tblDirDebtRvwD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            if (this.lblDebtCounsellorNameD.Text == string.Empty && this.lblDebtCounsellorNoD.Text == string.Empty && this.lblDebtRvwDateD.Text == string.Empty && this.lblDebtStatusD.Text == string.Empty)
            {
                DDirectorDebtRD.Visible = false;
                lblDDebtReveiwNoInfo.Visible = true;
            }
            else
            {
                DDirectorDebtRD.Visible = true;
                lblDDebtReveiwNoInfo.Visible = false;
            }
        }


        private void tblCurrentBusInterestD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            if (this.lblDCCompanyNameD.Text == string.Empty && this.lblDCRegNoD.Text == string.Empty && this.lblDCStatusD.Text == string.Empty && this.lblDcAgeOfBusD.Text == string.Empty)
            {
                tblCurrentBusInterestD.Visible = false;
                lblDCurrentBusNoInfo.Visible = true;

            }    
            else
            {
                tblCurrentBusInterestD.Visible = true;
                lblDCurrentBusNoInfo.Visible = false;

            }
            
        }

        private void tblPrevBusIntD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            if (this.lblDPCompanyNameD.Text == string.Empty && this.lblDPRegNoD.Text == string.Empty && this.lblDPStatusD.Text == string.Empty )
            {
                tblPrevBusIntD.Visible = false;
                lblDPreviousBusNoInfo.Visible = true;
               
            }
            else
            {
                tblPrevBusIntD.Visible = true;
                lblDPreviousBusNoInfo.Visible = false;
              
            }


        }

        private void tblDirectoPropInterests_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.lblDirProBondAmntD.Text == string.Empty && this.lblDirProBondHolderD.Text == string.Empty && this.lblDirProDeedNoD.Text == string.Empty && this.lblDirProDeedOffD.Text == string.Empty && this.lblDirProDeedTypeD.Text == string.Empty && this.lblDirProErfSizedD.Text == string.Empty && this.lblDirProExtSizeD.Text == string.Empty && this.lblDirProFarmD.Text == string.Empty && this.lblDirProPhysicalAddresD.Text == string.Empty && this.lblDirProPortionNoD.Text == string.Empty && this.lblDirProPurchaseDateD.Text == string.Empty && this.lblDirProPurchasePriceD.Text == string.Empty && this.lblDirProSchemeNoD.Text == string.Empty && this.lblDirProSharePecD.Text == string.Empty && this.lblDirProStandNoD.Text == string.Empty && this.lblDirProTownshipD.Text == string.Empty && this.lblDirProTransfereDateD.Text == string.Empty)
            //if (this.lblDirProDeedNoD.Text == string.Empty && this.lblDirProDeedOffD.Text == string.Empty && this.lblDirProDeedTypeD.Text == string.Empty)
            {
                   lblDPropertyNoInfo.Visible=true;
                   tblDirPropInterests.Visible = false;

                }
                else
                {

                     lblDPropertyNoInfo.Visible=false;
                     tblDirPropInterests.Visible = true;

                }

       }

       

      
        }
       
  
    }

