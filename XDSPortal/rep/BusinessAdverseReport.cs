using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XdsPortalReports
{
    public partial class BusinessAdverseReport : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet dsXML = new DataSet();

        public BusinessAdverseReport(string strXML)
        {
            InitializeComponent();
            //System.IO.StringReader Objsr = new System.IO.StringReader(strXML);

            //dsXML.ReadXml(Objsr);
            DataSet dsXML = new DataSet();
            System.IO.StringReader Objsr = new System.IO.StringReader(strXML.Replace("''", "'"));
            dsXML.ReadXml(Objsr);

            if (dsXML.Tables.Contains("SubscriberInputDetails"))
            {
                dsXML.Tables["SubscriberInputDetails"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate = '', '1900-01-01T00:00:00', EnquiryDate)");
                this.lblEnquiryDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - HH:mm:ss}") });
            }

            if (!(dsXML.Tables.Contains("CommercialBusinessInformation")))
            {
                DCommBusinfo.Visible = false;
                lblDetailedBusInfoH.Text += " - (No Data Available)";
            }
            else
            {
                dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FSIC", System.Type.GetType("System.String"), "Iif(SIC = '0 - Unkown Data', 'No Information Available',SIC)");
                this.lblSICCodeD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FSIC") });
                dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(AuthorisedCapitalAmt='',0,AuthorisedCapitalAmt)");
                this.LblAuthorisedcapitalD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FAmount", "{0:c}") });
                dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FBmount", System.Type.GetType("System.Double"), "Iif(IssuedCapitalAmt='',0,IssuedCapitalAmt)");
                this.LblIssuedCapitalAmntD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FBmount", "{0:c}") });
            }

            if (!(dsXML.Tables.Contains("CreditCircleInformation")))
            {
                DCreditCircleInfo.Visible = false;
                lblVATInfoH.Text += " - (No Data Available)";
                tblTAXInfoC.Visible = false;
            }
            else
            {
                dsXML.Tables["CreditCircleInformation"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                this.lblVATLiableDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CreditCircleInformation.FAmount", "{0:c}") });
            }

            if (!(dsXML.Tables.Contains("CommercialJudgment")))
            {
                //DetailJudgements.Visible = false;
                lblJudgmentsH.Text += " - (No Data Available)";
                tblCJud.Visible = false;
                DCommJudgSumm.Visible = false;
            }
            else
            {
                // dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["CommercialJudgment"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.lblJDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialJudgment.FAmount", "{0:c}") });
            }

            if (!(dsXML.Tables.Contains("CommercialPossibleJudgment")))
            {
                //DetailJudgements.Visible = false;
                lblposJudgmentsH.Text += " - (No Data Available)";
                tbPosslCJud.Visible = false;
                DCommPosJudg.Visible = false;
                tbPosslCJud.Visible = false;

            }
            else
            {
                // dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["CommercialPossibleJudgment"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.LblPosJDAmnt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialPossibleJudgment.FAmount", "{0:c}") });
            }

            if (!(dsXML.Tables.Contains("CommercialAuditorInformation")))
            {
                DCommAUDInfo.Visible = false;
                lblAUDInfoH.Text += " - (No Data Available)";
                lblAuditorH.Visible = false;
            }

            if (!(dsXML.Tables.Contains("CommercialBankDetails")))
            {
                DRBankInformation.Visible = false;
            }
            else
            {
                //this.lblBankAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBankCodeHistory.FAmount", "{0:c}") });            
            }
            if (!(dsXML.Tables.Contains("XDSCommercialPrincipalInformation")))
            {
                DRXDSPrincipals.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialBranchInformation")))
            {
                DCommBranchInfo.Visible = false;
                lblBranchH.Text += " - (No Data Available)";
                tblBranchInfoC.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialDivisionInformation")))
            {
                DCommDivisionInfo.Visible = false;
                lblDivisionInfoH.Text += " - (No Data Available)";
                tblDivisonC.Visible = false;
            }
            if (!(dsXML.Tables.Contains("CommercialBankDetails")))
            {
                BankDetails.Visible = false;
            }

            #region Active principals

            if (!(dsXML.Tables.Contains("CommercialActivePrincipalInformation")))
            {
                lblPrincipalInfoH.Text += " - (No Data Available)";
                DCommDIRInfo.Visible = false;
                //DRDirJudgInfo.Visible = false;
                //DRDirCurrBusInterests.Visible = false;
                //DRDirPrevBusInterests.Visible = false;
                tblDirPrincipalInfoSummary.Visible = false;
                //DDirAddressHistory.Visible = false;
                //DDirContactInfo.Visible = false;
                //DDirPaymentNotification.Visible = false;
                //DDirDefaults.Visible = false;
                //DRDirPropertyInterests.Visible = false;
                //DRDirDebtReviewInfo.Visible = false;
                //ContactHeader.Visible = false;
            }
            else
            {

            }
            #endregion

            if (!(dsXML.Tables.Contains("XDSPaymentNotification")))
            {
                tblPmtNotificationD.Visible = false;
                lblPaymentNotificationsH.Text += " - (No Data Available)";
                tblPmtNotificationC.Visible = false;
            }
            else
            {
                dsXML.Tables["XDSPaymentNotification"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                this.lblPmtAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "XDSPaymentNotification.FAmount", "{0:c}") });

            }
            if (!(dsXML.Tables.Contains("CommercialDefaultAlert")))
            {
                DDefaultListing.Visible = false;
                lblDefaultAlertH.Text += " - (No Data Available)";
                tblDefaultAlertC.Visible = false;
            }
            else
            {
                dsXML.Tables["CommercialDefaultAlert"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                this.lblDAAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialDefaultAlert.FAmount", "{0:c}") });
            }
            if (!(dsXML.Tables.Contains("BusinessRescue")))
            {
                DRescue.Visible = false;
                lblBusinessRescueH.Text += " - (No Data Available)";
                tblCRescue.Visible = false;
            }

            this.DataSource = dsXML;
            dsXML.Dispose();
        }
    }
}

