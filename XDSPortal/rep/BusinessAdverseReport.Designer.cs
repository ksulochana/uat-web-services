namespace XdsPortalReports
{
    partial class BusinessAdverseReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BusinessAdverseReport));
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.DRCommBusinfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommBusinfo = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow156 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow157 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSICCodeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblAuthorisedcapitalD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblIssuedCapitalAmntD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDetailedBusInfoH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.DRCreditCircleInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCreditCircleInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.tblTAXInfoD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTCompanyNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVATNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVATLiableDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblVATInfoH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblTAXInfoC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCompanyNameC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVATNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVATLiableDateC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblStatusC = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRCommJudgSumm = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommJudgSumm = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDJudg = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJDCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAttroneyNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAttroneyNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell492 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJCaseReasonD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCJud = new DevExpress.XtraReports.UI.XRTable();
            this.tblJudgementH = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJCaseNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAttroneyNameC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAttroneyNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJCaseReason = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblJudgmentsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJudgmentsH = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRCommAUDInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommAUDInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.lblAuditorH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable49 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow158 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow159 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow160 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow161 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow163 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow164 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAUDInfoH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRCommDIRInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommDIRInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow177 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblFullName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow178 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblPreviousSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow180 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow198 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow199 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDAge = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAgeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDYearswithBus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDYearswithBusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow200 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow201 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow142 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow202 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow147 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDirectorRecPosition = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.DRSubInputDtls = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DSubInputDtls = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow97 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow98 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow99 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEnquiryDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow227 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow162 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo4 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DRCommPosJudg = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommPosJudg = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable69 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell530 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell531 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell532 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblposjudgAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblPosJDAmnt = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell534 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell535 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell536 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell537 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell538 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tbPosslCJud = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblposJudgmentsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblposJudgmentsH = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRCommBranchInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommBranchInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.tblBranchInfoD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBranchNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblCoutryCD = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblProvinceD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBranchH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblBranchInfoC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblBrachNameC = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblCountry = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblProvince = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRCommDivisionInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DCommDivisionInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDivisonD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDivisionCodeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDivisionNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDivisionInfoH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDivisonC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDivisionCodeC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDivisionNameC = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRXDSPaymentNotification = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DPaymentNotification = new DevExpress.XtraReports.UI.DetailBand();
            this.tblPmtNotificationD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow135 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblpmtSubscriberD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAccNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtDatelistedD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAmount = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPaymentAmnt = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPmtAccStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtCommentD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell489 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader23 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPaymentNotificationsH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPaymentNotificationH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblPmtNotificationC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow134 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblpmtSubscriberC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAccNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtDatelistedC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAmountC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAccStatusC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRDefaultListing = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DDefaultListing = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDefaultAlertD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow146 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDACompanyD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAccnoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDADLoadeddateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDACommentsD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell490 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell491 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader26 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDefaultAlertC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow145 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDefaultAlertH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDefaultAlertH = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRRescue = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DRescue = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDRescue = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow149 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDReportedby = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell542 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell541 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDReportedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader27 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.lblBusinessRescueH = new DevExpress.XtraReports.UI.XRLabel();
            this.tblCRescue = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow148 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCReportedby = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell540 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell539 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDateReported = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRBankInformation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.BankDetails = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow136 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblBank = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblBankD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblBranch = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblBranchD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblAccountNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.LbLAccountNumberD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblTypeOfAccount = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblTypeOfAccountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblAccountHolderName = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblAccountHolderNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblOverDraftFac = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblOverDarftFacD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblBankCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblBankCodeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.LblBankComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblBankCommentD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader29 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CommercialPreviousBankDetails = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow169 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.LblYearsWithBankD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader35 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow144 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow165 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DRXDSPrincipals = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow186 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow187 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow188 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow189 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow190 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow191 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow192 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow193 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow194 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow195 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow196 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow197 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow185 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow184 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable52 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow171 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow181 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow182 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow203 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow204 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow205 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell397 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow206 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader30 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable51 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDirPrincipalInfoSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNoOfPrincipals = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoOfPrincipalsD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDirAvgAge = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDirAvgAgeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader21 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPrincipalInfoH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTAXInfoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTAXInfoC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDJudg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCJud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJudgmentsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPosslCJud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblposJudgmentsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBranchInfoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBranchInfoC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDivisonD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDivisonC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPmtNotificationD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPmtNotificationC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDRescue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCRescue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirPrincipalInfoSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 6F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 20F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.ReportHeader.HeightF = 126F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable2.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.ForeColor = System.Drawing.Color.Gray;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable2.SizeF = new System.Drawing.SizeF(792F, 114.375F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseForeColor = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.xrTableCell181});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.BackColor = System.Drawing.Color.White;
            this.xrTableCell179.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell179.BorderWidth = 0;
            this.xrTableCell179.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBackColor = false;
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.StylePriority.UseBorderWidth = false;
            this.xrTableCell179.Weight = 1.0917721518987342;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(2.710207F, 10.00001F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(150.5F, 89.99999F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.BackColor = System.Drawing.Color.White;
            this.xrTableCell181.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell181.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell181.BorderWidth = 0;
            this.xrTableCell181.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17,
            this.xrLabel18});
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.StylePriority.UseBackColor = false;
            this.xrTableCell181.StylePriority.UseBorderColor = false;
            this.xrTableCell181.StylePriority.UseBorders = false;
            this.xrTableCell181.StylePriority.UseBorderWidth = false;
            this.xrTableCell181.Weight = 1.9082278481012658;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.White;
            this.xrLabel17.BorderColor = System.Drawing.Color.Gray;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(265.6806F, 55.20837F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(237.3333F, 59.1666F);
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorderColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseForeColor = false;
            this.xrLabel17.StylePriority.UsePadding = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "11-13 St. Andrews Street, \r\nOakhurst Building,Parktown, Johannesburg\r\nTel No:+27 " +
                "11 645 9100, Fax No:+27 11 484 6588\r\nwebsite:www.xds.co.za\r\nEmail : info@xds.co." +
                "za";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.White;
            this.xrLabel18.BorderColor = System.Drawing.Color.Gray;
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(107.9422F, 10F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(395.83F, 45.20836F);
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseBorderColor = false;
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseForeColor = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Xpert Adverse Report";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DRCommBusinfo
            // 
            this.DRCommBusinfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommBusinfo,
            this.GroupHeader1});
            this.DRCommBusinfo.DataMember = "CommercialBusinessInformation";
            this.DRCommBusinfo.Level = 2;
            this.DRCommBusinfo.Name = "DRCommBusinfo";
            this.DRCommBusinfo.XmlDataPath = "C:\\Users\\jsetshwaro\\Desktop\\CommXml.xml";
            // 
            // DCommBusinfo
            // 
            this.DCommBusinfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.DCommBusinfo.HeightF = 310F;
            this.DCommBusinfo.Name = "DCommBusinfo";
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable5.ForeColor = System.Drawing.Color.DimGray;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32,
            this.xrTableRow49,
            this.xrTableRow53,
            this.xrTableRow54,
            this.xrTableRow156,
            this.xrTableRow69,
            this.xrTableRow70,
            this.xrTableRow85,
            this.xrTableRow157,
            this.xrTableRow86,
            this.xrTableRow89,
            this.xrTableRow90,
            this.xrTableRow91,
            this.xrTableRow93,
            this.xrTableRow94});
            this.xrTable5.SizeF = new System.Drawing.SizeF(790F, 300F);
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseForeColor = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell31});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell33.BorderColor = System.Drawing.Color.White;
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell33.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell33.StylePriority.UseBackColor = false;
            this.xrTableCell33.StylePriority.UseBorderColor = false;
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseForeColor = false;
            this.xrTableCell33.StylePriority.UsePadding = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Registered Business Name";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell33.Weight = 0.73029760956350853;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.BackColor = System.Drawing.Color.White;
            this.xrTableCell31.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.CommercialName")});
            this.xrTableCell31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell31.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell31.StylePriority.UseBackColor = false;
            this.xrTableCell31.StylePriority.UseBorderColor = false;
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseForeColor = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "xrTableCell35";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 2.2697023904364912;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell41});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell37.BorderColor = System.Drawing.Color.White;
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell37.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell37.StylePriority.UseBackColor = false;
            this.xrTableCell37.StylePriority.UseBorderColor = false;
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseForeColor = false;
            this.xrTableCell37.StylePriority.UsePadding = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "Trading Name";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell37.Weight = 0.73029760956350853;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.BackColor = System.Drawing.Color.White;
            this.xrTableCell41.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.TradeName")});
            this.xrTableCell41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell41.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell41.StylePriority.UseBackColor = false;
            this.xrTableCell41.StylePriority.UseBorderColor = false;
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UseForeColor = false;
            this.xrTableCell41.StylePriority.UsePadding = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = "xrTableCell43";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 2.2697023904364912;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell47});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell45.BorderColor = System.Drawing.Color.White;
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell45.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell45.StylePriority.UseBackColor = false;
            this.xrTableCell45.StylePriority.UseBorderColor = false;
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseForeColor = false;
            this.xrTableCell45.StylePriority.UsePadding = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.Text = "Previous Business Name\t";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell45.Weight = 0.73029760956350853;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.BackColor = System.Drawing.Color.White;
            this.xrTableCell47.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell47.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.PreviousBussName")});
            this.xrTableCell47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell47.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell47.StylePriority.UseBackColor = false;
            this.xrTableCell47.StylePriority.UseBorderColor = false;
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseForeColor = false;
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "xrTableCell47";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 2.2697023904364912;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell51,
            this.xrTableCell53,
            this.xrTableCell55});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell49.BorderColor = System.Drawing.Color.White;
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell49.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell49.StylePriority.UseBackColor = false;
            this.xrTableCell49.StylePriority.UseBorderColor = false;
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseForeColor = false;
            this.xrTableCell49.StylePriority.UsePadding = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "Registration Number";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.73029760956350853;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.BackColor = System.Drawing.Color.White;
            this.xrTableCell51.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.RegistrationNo")});
            this.xrTableCell51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell51.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell51.StylePriority.UseBackColor = false;
            this.xrTableCell51.StylePriority.UseBorderColor = false;
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseForeColor = false;
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "xrTableCell51";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 0.77509808723797879;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell53.BorderColor = System.Drawing.Color.White;
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell53.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell53.StylePriority.UseBackColor = false;
            this.xrTableCell53.StylePriority.UseBorderColor = false;
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UseForeColor = false;
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = "Previous Registration Number";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell53.Weight = 0.72908162235839358;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.BackColor = System.Drawing.Color.White;
            this.xrTableCell55.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.RegistrationNoOld")});
            this.xrTableCell55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell55.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell55.StylePriority.UseBackColor = false;
            this.xrTableCell55.StylePriority.UseBorderColor = false;
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UseForeColor = false;
            this.xrTableCell55.StylePriority.UsePadding = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "xrTableCell55";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.76552268084011887;
            // 
            // xrTableRow156
            // 
            this.xrTableRow156.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell324,
            this.xrTableCell325,
            this.xrTableCell326,
            this.xrTableCell327});
            this.xrTableRow156.Name = "xrTableRow156";
            this.xrTableRow156.Weight = 1;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell324.BorderColor = System.Drawing.Color.White;
            this.xrTableCell324.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell324.StylePriority.UseBackColor = false;
            this.xrTableCell324.StylePriority.UseBorderColor = false;
            this.xrTableCell324.StylePriority.UseFont = false;
            this.xrTableCell324.StylePriority.UsePadding = false;
            this.xrTableCell324.Text = "Converted Registration Number";
            this.xrTableCell324.Weight = 0.73029760956350853;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.BackColor = System.Drawing.Color.White;
            this.xrTableCell325.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell325.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.RegistrationNoConverted")});
            this.xrTableCell325.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell325.StylePriority.UseBackColor = false;
            this.xrTableCell325.StylePriority.UseBorderColor = false;
            this.xrTableCell325.StylePriority.UseFont = false;
            this.xrTableCell325.StylePriority.UsePadding = false;
            this.xrTableCell325.Text = "xrTableCell325";
            this.xrTableCell325.Weight = 0.77509808723797879;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell326.BorderColor = System.Drawing.Color.White;
            this.xrTableCell326.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell326.StylePriority.UseBackColor = false;
            this.xrTableCell326.StylePriority.UseBorderColor = false;
            this.xrTableCell326.StylePriority.UseFont = false;
            this.xrTableCell326.StylePriority.UsePadding = false;
            this.xrTableCell326.Text = "Name Change Date";
            this.xrTableCell326.Weight = 0.72908162235839358;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.BackColor = System.Drawing.Color.White;
            this.xrTableCell327.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell327.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.NameChangeDate")});
            this.xrTableCell327.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell327.StylePriority.UseBackColor = false;
            this.xrTableCell327.StylePriority.UseBorderColor = false;
            this.xrTableCell327.StylePriority.UseFont = false;
            this.xrTableCell327.StylePriority.UsePadding = false;
            this.xrTableCell327.Text = "xrTableCell327";
            this.xrTableCell327.Weight = 0.76552268084011887;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell59,
            this.xrTableCell63,
            this.xrTableCell65});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell57.BorderColor = System.Drawing.Color.White;
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell57.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell57.StylePriority.UseBackColor = false;
            this.xrTableCell57.StylePriority.UseBorderColor = false;
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UseForeColor = false;
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "Registration Date";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 0.73029760956350853;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.BackColor = System.Drawing.Color.White;
            this.xrTableCell59.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.RegistrationDate")});
            this.xrTableCell59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell59.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell59.StylePriority.UseBackColor = false;
            this.xrTableCell59.StylePriority.UseBorderColor = false;
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseForeColor = false;
            this.xrTableCell59.StylePriority.UsePadding = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "xrTableCell55";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell59.Weight = 0.77509808723797879;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell63.BorderColor = System.Drawing.Color.White;
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell63.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell63.StylePriority.UseBackColor = false;
            this.xrTableCell63.StylePriority.UseBorderColor = false;
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.StylePriority.UseForeColor = false;
            this.xrTableCell63.StylePriority.UsePadding = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "Age of Business";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell63.Weight = 0.72908162235839358;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.BackColor = System.Drawing.Color.White;
            this.xrTableCell65.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.AgeofBusiness")});
            this.xrTableCell65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell65.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell65.StylePriority.UseBackColor = false;
            this.xrTableCell65.StylePriority.UseBorderColor = false;
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.StylePriority.UseForeColor = false;
            this.xrTableCell65.StylePriority.UsePadding = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "xrTableCell57";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell65.Weight = 0.76552268084011887;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell69});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell67.BorderColor = System.Drawing.Color.White;
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell67.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell67.StylePriority.UseBackColor = false;
            this.xrTableCell67.StylePriority.UseBorderColor = false;
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.StylePriority.UseForeColor = false;
            this.xrTableCell67.StylePriority.UsePadding = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.Text = "Tax Number";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell67.Weight = 0.73029755272442409;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.BackColor = System.Drawing.Color.White;
            this.xrTableCell69.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell69.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell69.StylePriority.UseBackColor = false;
            this.xrTableCell69.StylePriority.UseBorderColor = false;
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.StylePriority.UseForeColor = false;
            this.xrTableCell69.StylePriority.UsePadding = false;
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.Text = "[TaxNo]";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell69.Weight = 2.2697024472755754;
            // 
            // xrTableRow85
            // 
            this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell78,
            this.xrTableCell80,
            this.xrTableCell86});
            this.xrTableRow85.Name = "xrTableRow85";
            this.xrTableRow85.Weight = 1;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell76.BorderColor = System.Drawing.Color.White;
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell76.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell76.StylePriority.UseBackColor = false;
            this.xrTableCell76.StylePriority.UseBorderColor = false;
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.StylePriority.UseForeColor = false;
            this.xrTableCell76.StylePriority.UsePadding = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = "Business Type\t";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell76.Weight = 0.73029755272442409;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.BackColor = System.Drawing.Color.White;
            this.xrTableCell78.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell78.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.CommercialType")});
            this.xrTableCell78.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell78.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell78.StylePriority.UseBackColor = false;
            this.xrTableCell78.StylePriority.UseBorderColor = false;
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.StylePriority.UseFont = false;
            this.xrTableCell78.StylePriority.UseForeColor = false;
            this.xrTableCell78.StylePriority.UsePadding = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.Text = "xrTableCell65";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell78.Weight = 0.77509802693543173;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell80.BorderColor = System.Drawing.Color.White;
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell80.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell80.StylePriority.UseBackColor = false;
            this.xrTableCell80.StylePriority.UseBorderColor = false;
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.StylePriority.UseForeColor = false;
            this.xrTableCell80.StylePriority.UsePadding = false;
            this.xrTableCell80.StylePriority.UseTextAlignment = false;
            this.xrTableCell80.Text = "Business Start Date";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell80.Weight = 0.72908156523252277;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.BackColor = System.Drawing.Color.White;
            this.xrTableCell86.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell86.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.BusinessStartDate")});
            this.xrTableCell86.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell86.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell86.StylePriority.UseBackColor = false;
            this.xrTableCell86.StylePriority.UseBorderColor = false;
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.StylePriority.UseFont = false;
            this.xrTableCell86.StylePriority.UseForeColor = false;
            this.xrTableCell86.StylePriority.UsePadding = false;
            this.xrTableCell86.StylePriority.UseTextAlignment = false;
            this.xrTableCell86.Text = "xrTableCell86";
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell86.Weight = 0.765522855107621;
            // 
            // xrTableRow157
            // 
            this.xrTableRow157.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell328,
            this.xrTableCell329,
            this.xrTableCell330,
            this.xrTableCell331});
            this.xrTableRow157.Name = "xrTableRow157";
            this.xrTableRow157.Weight = 1;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell328.BorderColor = System.Drawing.Color.White;
            this.xrTableCell328.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell328.StylePriority.UseBackColor = false;
            this.xrTableCell328.StylePriority.UseBorderColor = false;
            this.xrTableCell328.StylePriority.UseFont = false;
            this.xrTableCell328.StylePriority.UsePadding = false;
            this.xrTableCell328.Text = "Business Status";
            this.xrTableCell328.Weight = 0.73029755272442409;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.BackColor = System.Drawing.Color.White;
            this.xrTableCell329.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell329.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.CommercialStatus")});
            this.xrTableCell329.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell329.StylePriority.UseBackColor = false;
            this.xrTableCell329.StylePriority.UseBorderColor = false;
            this.xrTableCell329.StylePriority.UseFont = false;
            this.xrTableCell329.StylePriority.UsePadding = false;
            this.xrTableCell329.Text = "xrTableCell329";
            this.xrTableCell329.Weight = 0.77509802693543173;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell330.BorderColor = System.Drawing.Color.White;
            this.xrTableCell330.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell330.StylePriority.UseBackColor = false;
            this.xrTableCell330.StylePriority.UseBorderColor = false;
            this.xrTableCell330.StylePriority.UseFont = false;
            this.xrTableCell330.StylePriority.UsePadding = false;
            this.xrTableCell330.Text = "Business Status Date";
            this.xrTableCell330.Weight = 0.72908156523252277;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.BackColor = System.Drawing.Color.White;
            this.xrTableCell331.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell331.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.CommercialStatusDate")});
            this.xrTableCell331.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell331.StylePriority.UseBackColor = false;
            this.xrTableCell331.StylePriority.UseBorderColor = false;
            this.xrTableCell331.StylePriority.UseFont = false;
            this.xrTableCell331.StylePriority.UsePadding = false;
            this.xrTableCell331.Text = "xrTableCell331";
            this.xrTableCell331.Weight = 0.765522855107621;
            // 
            // xrTableRow86
            // 
            this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.lblSICCodeD,
            this.xrTableCell117,
            this.xrTableCell118});
            this.xrTableRow86.Name = "xrTableRow86";
            this.xrTableRow86.Weight = 1;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell91.BorderColor = System.Drawing.Color.White;
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell91.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell91.StylePriority.UseBackColor = false;
            this.xrTableCell91.StylePriority.UseBorderColor = false;
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.StylePriority.UseFont = false;
            this.xrTableCell91.StylePriority.UseForeColor = false;
            this.xrTableCell91.StylePriority.UsePadding = false;
            this.xrTableCell91.StylePriority.UseTextAlignment = false;
            this.xrTableCell91.Text = "SICC Code";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell91.Weight = 0.73029755272442409;
            // 
            // lblSICCodeD
            // 
            this.lblSICCodeD.BackColor = System.Drawing.Color.White;
            this.lblSICCodeD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblSICCodeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSICCodeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.SIC")});
            this.lblSICCodeD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSICCodeD.ForeColor = System.Drawing.Color.DimGray;
            this.lblSICCodeD.Name = "lblSICCodeD";
            this.lblSICCodeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSICCodeD.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSICCodeD.StylePriority.UseBackColor = false;
            this.lblSICCodeD.StylePriority.UseBorderColor = false;
            this.lblSICCodeD.StylePriority.UseBorders = false;
            this.lblSICCodeD.StylePriority.UseFont = false;
            this.lblSICCodeD.StylePriority.UseForeColor = false;
            this.lblSICCodeD.StylePriority.UsePadding = false;
            this.lblSICCodeD.StylePriority.UseTextAlignment = false;
            this.lblSICCodeD.Text = "lblSICCodeD";
            this.lblSICCodeD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSICCodeD.Weight = 0.77509802693543173;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell117.BorderColor = System.Drawing.Color.White;
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell117.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell117.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell117.Multiline = true;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell117.StylePriority.UseBackColor = false;
            this.xrTableCell117.StylePriority.UseBorderColor = false;
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.StylePriority.UseFont = false;
            this.xrTableCell117.StylePriority.UseForeColor = false;
            this.xrTableCell117.StylePriority.UsePadding = false;
            this.xrTableCell117.StylePriority.UseTextAlignment = false;
            this.xrTableCell117.Text = "Desciption of Business\r\n";
            this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell117.Weight = 0.72908156523252277;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.BackColor = System.Drawing.Color.White;
            this.xrTableCell118.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell118.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.BusinessDesc")});
            this.xrTableCell118.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell118.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell118.StylePriority.UseBackColor = false;
            this.xrTableCell118.StylePriority.UseBorderColor = false;
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.StylePriority.UseFont = false;
            this.xrTableCell118.StylePriority.UseForeColor = false;
            this.xrTableCell118.StylePriority.UsePadding = false;
            this.xrTableCell118.StylePriority.UseTextAlignment = false;
            this.xrTableCell118.Text = "xrTableCell71";
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell118.Weight = 0.765522855107621;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xrTableCell120});
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Weight = 1;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell119.BorderColor = System.Drawing.Color.White;
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell119.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell119.StylePriority.UseBackColor = false;
            this.xrTableCell119.StylePriority.UseBorderColor = false;
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.StylePriority.UseFont = false;
            this.xrTableCell119.StylePriority.UsePadding = false;
            this.xrTableCell119.Text = "Registered Address";
            this.xrTableCell119.Weight = 0.73029755272442409;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.BackColor = System.Drawing.Color.White;
            this.xrTableCell120.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell120.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.PhysicalAddress")});
            this.xrTableCell120.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell120.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell120.StylePriority.UseBackColor = false;
            this.xrTableCell120.StylePriority.UseBorderColor = false;
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.StylePriority.UseFont = false;
            this.xrTableCell120.StylePriority.UsePadding = false;
            this.xrTableCell120.Text = "xrTableCell41";
            this.xrTableCell120.Weight = 2.2697024472755754;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122});
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.Weight = 1;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell121.BorderColor = System.Drawing.Color.White;
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell121.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell121.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell121.StylePriority.UseBackColor = false;
            this.xrTableCell121.StylePriority.UseBorderColor = false;
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.StylePriority.UseFont = false;
            this.xrTableCell121.StylePriority.UsePadding = false;
            this.xrTableCell121.Text = "Postal Address";
            this.xrTableCell121.Weight = 0.73029755272442409;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.BackColor = System.Drawing.Color.White;
            this.xrTableCell122.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell122.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell122.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.PostalAddress")});
            this.xrTableCell122.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell122.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell122.StylePriority.UseBackColor = false;
            this.xrTableCell122.StylePriority.UseBorderColor = false;
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.StylePriority.UseFont = false;
            this.xrTableCell122.StylePriority.UsePadding = false;
            this.xrTableCell122.Text = "xrTableCell79";
            this.xrTableCell122.Weight = 2.2697024472755754;
            // 
            // xrTableRow91
            // 
            this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125,
            this.xrTableCell252});
            this.xrTableRow91.Name = "xrTableRow91";
            this.xrTableRow91.Weight = 1;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell123.BorderColor = System.Drawing.Color.White;
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell123.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell123.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell123.StylePriority.UseBackColor = false;
            this.xrTableCell123.StylePriority.UseBorderColor = false;
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.StylePriority.UseFont = false;
            this.xrTableCell123.StylePriority.UsePadding = false;
            this.xrTableCell123.Text = "Financial Year End";
            this.xrTableCell123.Weight = 0.73029755272442409;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.BackColor = System.Drawing.Color.White;
            this.xrTableCell124.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.FinancialYearEnd")});
            this.xrTableCell124.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell124.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell124.StylePriority.UseBackColor = false;
            this.xrTableCell124.StylePriority.UseBorderColor = false;
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.StylePriority.UseFont = false;
            this.xrTableCell124.StylePriority.UsePadding = false;
            this.xrTableCell124.Text = "xrTableCell97";
            this.xrTableCell124.Weight = 0.77509802693543173;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell125.BorderColor = System.Drawing.Color.White;
            this.xrTableCell125.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell125.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell125.StylePriority.UseBackColor = false;
            this.xrTableCell125.StylePriority.UseBorderColor = false;
            this.xrTableCell125.StylePriority.UseFont = false;
            this.xrTableCell125.StylePriority.UsePadding = false;
            this.xrTableCell125.Text = "Financial Effective Date";
            this.xrTableCell125.Weight = 0.72908156523252277;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.BackColor = System.Drawing.Color.White;
            this.xrTableCell252.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell252.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell252.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.FinancialEffectiveDate")});
            this.xrTableCell252.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell252.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell252.StylePriority.UseBackColor = false;
            this.xrTableCell252.StylePriority.UseBorderColor = false;
            this.xrTableCell252.StylePriority.UseBorders = false;
            this.xrTableCell252.StylePriority.UseFont = false;
            this.xrTableCell252.StylePriority.UsePadding = false;
            this.xrTableCell252.Text = "xrTableCell252";
            this.xrTableCell252.Weight = 0.765522855107621;
            // 
            // xrTableRow93
            // 
            this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell253,
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTableCell256});
            this.xrTableRow93.Name = "xrTableRow93";
            this.xrTableRow93.Weight = 1;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell253.BorderColor = System.Drawing.Color.White;
            this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell253.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell253.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell253.StylePriority.UseBackColor = false;
            this.xrTableCell253.StylePriority.UseBorderColor = false;
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.StylePriority.UseFont = false;
            this.xrTableCell253.StylePriority.UsePadding = false;
            this.xrTableCell253.Text = "Authorised Capital Amount";
            this.xrTableCell253.Weight = 0.73029755272442409;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.BackColor = System.Drawing.Color.White;
            this.xrTableCell254.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell254.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.LblAuthorisedcapitalD});
            this.xrTableCell254.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell254.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell254.StylePriority.UseBackColor = false;
            this.xrTableCell254.StylePriority.UseBorderColor = false;
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.StylePriority.UseFont = false;
            this.xrTableCell254.StylePriority.UsePadding = false;
            this.xrTableCell254.Text = "xrTableCell101";
            this.xrTableCell254.Weight = 0.77509802693543173;
            // 
            // LblAuthorisedcapitalD
            // 
            this.LblAuthorisedcapitalD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.AuthorisedCapitalAmt")});
            this.LblAuthorisedcapitalD.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.LblAuthorisedcapitalD.Name = "LblAuthorisedcapitalD";
            this.LblAuthorisedcapitalD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.LblAuthorisedcapitalD.SizeF = new System.Drawing.SizeF(202.9883F, 20.00003F);
            this.LblAuthorisedcapitalD.Text = "LblAuthorisedcapitalD";
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell255.BorderColor = System.Drawing.Color.White;
            this.xrTableCell255.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell255.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell255.StylePriority.UseBackColor = false;
            this.xrTableCell255.StylePriority.UseBorderColor = false;
            this.xrTableCell255.StylePriority.UseFont = false;
            this.xrTableCell255.StylePriority.UsePadding = false;
            this.xrTableCell255.Text = "Authorised Number of Shares";
            this.xrTableCell255.Weight = 0.72908156523252277;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.BackColor = System.Drawing.Color.White;
            this.xrTableCell256.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell256.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.AuthorisedNoOfShares")});
            this.xrTableCell256.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell256.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell256.StylePriority.UseBackColor = false;
            this.xrTableCell256.StylePriority.UseBorderColor = false;
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.StylePriority.UseFont = false;
            this.xrTableCell256.StylePriority.UsePadding = false;
            this.xrTableCell256.Text = "xrTableCell256";
            this.xrTableCell256.Weight = 0.765522855107621;
            // 
            // xrTableRow94
            // 
            this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell257,
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260});
            this.xrTableRow94.Name = "xrTableRow94";
            this.xrTableRow94.Weight = 1;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell257.BorderColor = System.Drawing.Color.White;
            this.xrTableCell257.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell257.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell257.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell257.StylePriority.UseBackColor = false;
            this.xrTableCell257.StylePriority.UseBorderColor = false;
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.StylePriority.UseFont = false;
            this.xrTableCell257.StylePriority.UsePadding = false;
            this.xrTableCell257.Text = "Issued Capital Amount";
            this.xrTableCell257.Weight = 0.73029755272442409;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.BackColor = System.Drawing.Color.White;
            this.xrTableCell258.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell258.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.LblIssuedCapitalAmntD});
            this.xrTableCell258.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell258.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell258.StylePriority.UseBackColor = false;
            this.xrTableCell258.StylePriority.UseBorderColor = false;
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.StylePriority.UseFont = false;
            this.xrTableCell258.StylePriority.UsePadding = false;
            this.xrTableCell258.Text = "xrTableCell258";
            this.xrTableCell258.Weight = 0.77509802693543173;
            // 
            // LblIssuedCapitalAmntD
            // 
            this.LblIssuedCapitalAmntD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.IssuedCapitalAmt")});
            this.LblIssuedCapitalAmntD.LocationFloat = new DevExpress.Utils.PointFloat(4.291534E-05F, 3.051758E-05F);
            this.LblIssuedCapitalAmntD.Name = "LblIssuedCapitalAmntD";
            this.LblIssuedCapitalAmntD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.LblIssuedCapitalAmntD.SizeF = new System.Drawing.SizeF(204.1091F, 20F);
            this.LblIssuedCapitalAmntD.Text = "LblIssuedCapitalAmntD";
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell259.BorderColor = System.Drawing.Color.White;
            this.xrTableCell259.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell259.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell259.StylePriority.UseBackColor = false;
            this.xrTableCell259.StylePriority.UseBorderColor = false;
            this.xrTableCell259.StylePriority.UseFont = false;
            this.xrTableCell259.StylePriority.UsePadding = false;
            this.xrTableCell259.Text = "Issued Number of Shares";
            this.xrTableCell259.Weight = 0.72908156523252277;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.BackColor = System.Drawing.Color.White;
            this.xrTableCell260.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell260.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBusinessInformation.IssuedNoOfShares")});
            this.xrTableCell260.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell260.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell260.StylePriority.UseBackColor = false;
            this.xrTableCell260.StylePriority.UseBorderColor = false;
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.StylePriority.UseFont = false;
            this.xrTableCell260.StylePriority.UsePadding = false;
            this.xrTableCell260.Text = "xrTableCell111";
            this.xrTableCell260.Weight = 0.765522855107621;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader1.HeightF = 50.62501F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable3.SizeF = new System.Drawing.SizeF(790F, 40.62F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseForeColor = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDetailedBusInfoH,
            this.xrTableCell32});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.8;
            // 
            // lblDetailedBusInfoH
            // 
            this.lblDetailedBusInfoH.BackColor = System.Drawing.Color.White;
            this.lblDetailedBusInfoH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDetailedBusInfoH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblDetailedBusInfoH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblDetailedBusInfoH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblDetailedBusInfoH.Name = "lblDetailedBusInfoH";
            this.lblDetailedBusInfoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDetailedBusInfoH.StylePriority.UseBackColor = false;
            this.lblDetailedBusInfoH.StylePriority.UseBorderColor = false;
            this.lblDetailedBusInfoH.StylePriority.UseBorders = false;
            this.lblDetailedBusInfoH.StylePriority.UseFont = false;
            this.lblDetailedBusInfoH.StylePriority.UseForeColor = false;
            this.lblDetailedBusInfoH.StylePriority.UsePadding = false;
            this.lblDetailedBusInfoH.Text = "Company Statutory Information";
            this.lblDetailedBusInfoH.Weight = 1.1756499500551834;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.BackColor = System.Drawing.Color.White;
            this.xrTableCell32.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell32.BorderWidth = 1;
            this.xrTableCell32.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4});
            this.xrTableCell32.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell32.StylePriority.UseBackColor = false;
            this.xrTableCell32.StylePriority.UseBorderColor = false;
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseBorderWidth = false;
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseForeColor = false;
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.Weight = 1.8243500499448164;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(456.25F, 23F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            // 
            // DRCreditCircleInfo
            // 
            this.DRCreditCircleInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCreditCircleInfo,
            this.GroupHeader2});
            this.DRCreditCircleInfo.DataMember = "CreditCircleInformation";
            this.DRCreditCircleInfo.Level = 4;
            this.DRCreditCircleInfo.Name = "DRCreditCircleInfo";
            // 
            // DCreditCircleInfo
            // 
            this.DCreditCircleInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblTAXInfoD});
            this.DCreditCircleInfo.HeightF = 25F;
            this.DCreditCircleInfo.Name = "DCreditCircleInfo";
            // 
            // tblTAXInfoD
            // 
            this.tblTAXInfoD.BackColor = System.Drawing.Color.White;
            this.tblTAXInfoD.BorderColor = System.Drawing.Color.Gainsboro;
            this.tblTAXInfoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblTAXInfoD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblTAXInfoD.ForeColor = System.Drawing.Color.DimGray;
            this.tblTAXInfoD.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.tblTAXInfoD.Name = "tblTAXInfoD";
            this.tblTAXInfoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblTAXInfoD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.tblTAXInfoD.SizeF = new System.Drawing.SizeF(790F, 25F);
            this.tblTAXInfoD.StylePriority.UseBackColor = false;
            this.tblTAXInfoD.StylePriority.UseBorderColor = false;
            this.tblTAXInfoD.StylePriority.UseBorders = false;
            this.tblTAXInfoD.StylePriority.UseFont = false;
            this.tblTAXInfoD.StylePriority.UseForeColor = false;
            this.tblTAXInfoD.StylePriority.UsePadding = false;
            this.tblTAXInfoD.StylePriority.UseTextAlignment = false;
            this.tblTAXInfoD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTCompanyNameD,
            this.xrTableCell305,
            this.lblVATNoD,
            this.lblVATLiableDateD,
            this.lblStatusD});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1;
            // 
            // lblTCompanyNameD
            // 
            this.lblTCompanyNameD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblTCompanyNameD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTCompanyNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CreditCircleInformation.Date")});
            this.lblTCompanyNameD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblTCompanyNameD.Name = "lblTCompanyNameD";
            this.lblTCompanyNameD.StylePriority.UseBorderColor = false;
            this.lblTCompanyNameD.StylePriority.UseBorders = false;
            this.lblTCompanyNameD.StylePriority.UseFont = false;
            this.lblTCompanyNameD.Text = "lblTCompanyNameD";
            this.lblTCompanyNameD.Weight = 0.49526489127170709;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell305.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell305.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CreditCircleInformation.TermsGranted")});
            this.xrTableCell305.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorderColor = false;
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.StylePriority.UseFont = false;
            this.xrTableCell305.Text = "xrTableCell305";
            this.xrTableCell305.Weight = 0.50912220969015753;
            // 
            // lblVATNoD
            // 
            this.lblVATNoD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblVATNoD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblVATNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CreditCircleInformation.TermsTaken")});
            this.lblVATNoD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblVATNoD.Name = "lblVATNoD";
            this.lblVATNoD.StylePriority.UseBorderColor = false;
            this.lblVATNoD.StylePriority.UseBorders = false;
            this.lblVATNoD.StylePriority.UseFont = false;
            this.lblVATNoD.Text = "lblVATNoD";
            this.lblVATNoD.Weight = 0.44844302589056328;
            // 
            // lblVATLiableDateD
            // 
            this.lblVATLiableDateD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblVATLiableDateD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblVATLiableDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CreditCircleInformation.Amount")});
            this.lblVATLiableDateD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblVATLiableDateD.Name = "lblVATLiableDateD";
            this.lblVATLiableDateD.StylePriority.UseBorderColor = false;
            this.lblVATLiableDateD.StylePriority.UseBorders = false;
            this.lblVATLiableDateD.StylePriority.UseFont = false;
            this.lblVATLiableDateD.Text = "lblVATLiableDateD";
            this.lblVATLiableDateD.Weight = 0.40893769165100724;
            // 
            // lblStatusD
            // 
            this.lblStatusD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblStatusD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CreditCircleInformation.Comments")});
            this.lblStatusD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblStatusD.Name = "lblStatusD";
            this.lblStatusD.StylePriority.UseBorderColor = false;
            this.lblStatusD.StylePriority.UseBorders = false;
            this.lblStatusD.StylePriority.UseFont = false;
            this.lblStatusD.Text = "lblStatusD";
            this.lblStatusD.Weight = 1.1382321814965648;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable20,
            this.tblTAXInfoC});
            this.GroupHeader2.HeightF = 86F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(8.209984F, 10.00001F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43});
            this.xrTable20.SizeF = new System.Drawing.SizeF(792.0001F, 40.625F);
            this.xrTable20.StylePriority.UseBorders = false;
            this.xrTable20.StylePriority.UseFont = false;
            this.xrTable20.StylePriority.UseForeColor = false;
            this.xrTable20.StylePriority.UsePadding = false;
            this.xrTable20.StylePriority.UseTextAlignment = false;
            this.xrTable20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblVATInfoH,
            this.xrTableCell38});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 0.8;
            // 
            // lblVATInfoH
            // 
            this.lblVATInfoH.BackColor = System.Drawing.Color.White;
            this.lblVATInfoH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblVATInfoH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblVATInfoH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblVATInfoH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblVATInfoH.Name = "lblVATInfoH";
            this.lblVATInfoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblVATInfoH.StylePriority.UseBackColor = false;
            this.lblVATInfoH.StylePriority.UseBorderColor = false;
            this.lblVATInfoH.StylePriority.UseBorders = false;
            this.lblVATInfoH.StylePriority.UseFont = false;
            this.lblVATInfoH.StylePriority.UseForeColor = false;
            this.lblVATInfoH.StylePriority.UsePadding = false;
            this.lblVATInfoH.Text = "Credit Circle Information";
            this.lblVATInfoH.Weight = 1.4272807521510318;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.BackColor = System.Drawing.Color.White;
            this.xrTableCell38.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell38.BorderWidth = 1;
            this.xrTableCell38.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell38.StylePriority.UseBackColor = false;
            this.xrTableCell38.StylePriority.UseBorderColor = false;
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.StylePriority.UseBorderWidth = false;
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseForeColor = false;
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.Weight = 1.572719247848968;
            // 
            // tblTAXInfoC
            // 
            this.tblTAXInfoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblTAXInfoC.BorderColor = System.Drawing.Color.White;
            this.tblTAXInfoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblTAXInfoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblTAXInfoC.ForeColor = System.Drawing.Color.Gray;
            this.tblTAXInfoC.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 61F);
            this.tblTAXInfoC.Name = "tblTAXInfoC";
            this.tblTAXInfoC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblTAXInfoC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.tblTAXInfoC.SizeF = new System.Drawing.SizeF(790F, 25F);
            this.tblTAXInfoC.StylePriority.UseBackColor = false;
            this.tblTAXInfoC.StylePriority.UseBorderColor = false;
            this.tblTAXInfoC.StylePriority.UseBorders = false;
            this.tblTAXInfoC.StylePriority.UseFont = false;
            this.tblTAXInfoC.StylePriority.UseForeColor = false;
            this.tblTAXInfoC.StylePriority.UsePadding = false;
            this.tblTAXInfoC.StylePriority.UseTextAlignment = false;
            this.tblTAXInfoC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyNameC,
            this.xrTableCell304,
            this.lblVATNoC,
            this.lblVATLiableDateC,
            this.lblStatusC});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1;
            // 
            // lblCompanyNameC
            // 
            this.lblCompanyNameC.Name = "lblCompanyNameC";
            this.lblCompanyNameC.Text = "Date";
            this.lblCompanyNameC.Weight = 0.4952649202624399;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Text = "Terms Granted";
            this.xrTableCell304.Weight = 0.50912212275465607;
            // 
            // lblVATNoC
            // 
            this.lblVATNoC.Name = "lblVATNoC";
            this.lblVATNoC.Text = "Terms Taken";
            this.lblVATNoC.Weight = 0.44844299693652756;
            // 
            // lblVATLiableDateC
            // 
            this.lblVATLiableDateC.Name = "lblVATLiableDateC";
            this.lblVATLiableDateC.Text = "Amount";
            this.lblVATLiableDateC.Weight = 0.40893789462283442;
            // 
            // lblStatusC
            // 
            this.lblStatusC.Name = "lblStatusC";
            this.lblStatusC.Text = "Comments";
            this.lblStatusC.Weight = 1.1382320654235418;
            // 
            // DRCommJudgSumm
            // 
            this.DRCommJudgSumm.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommJudgSumm,
            this.GroupHeader3});
            this.DRCommJudgSumm.DataMember = "CommercialJudgment";
            this.DRCommJudgSumm.Level = 7;
            this.DRCommJudgSumm.Name = "DRCommJudgSumm";
            // 
            // DCommJudgSumm
            // 
            this.DCommJudgSumm.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDJudg});
            this.DCommJudgSumm.HeightF = 20F;
            this.DCommJudgSumm.Name = "DCommJudgSumm";
            // 
            // tblDJudg
            // 
            this.tblDJudg.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblDJudg.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.tblDJudg.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDJudg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDJudg.LocationFloat = new DevExpress.Utils.PointFloat(12.49995F, 0F);
            this.tblDJudg.Name = "tblDJudg";
            this.tblDJudg.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow88});
            this.tblDJudg.SizeF = new System.Drawing.SizeF(792.0002F, 19.99995F);
            this.tblDJudg.StylePriority.UseBackColor = false;
            this.tblDJudg.StylePriority.UseBorderColor = false;
            this.tblDJudg.StylePriority.UseBorders = false;
            this.tblDJudg.StylePriority.UseFont = false;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.BackColor = System.Drawing.Color.White;
            this.xrTableRow88.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJDCaseno,
            this.lblJDIssueDate,
            this.lblJDType,
            this.lblJDAmount,
            this.lblJDPlaintiff,
            this.lblJDCourt,
            this.lblJAttroneyNameD,
            this.lblAttroneyNoD,
            this.xrTableCell492,
            this.lblJCaseReasonD});
            this.xrTableRow88.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow88.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableRow88.StylePriority.UseBackColor = false;
            this.xrTableRow88.StylePriority.UseBorderColor = false;
            this.xrTableRow88.StylePriority.UseBorders = false;
            this.xrTableRow88.StylePriority.UseFont = false;
            this.xrTableRow88.StylePriority.UseForeColor = false;
            this.xrTableRow88.StylePriority.UsePadding = false;
            this.xrTableRow88.StylePriority.UseTextAlignment = false;
            this.xrTableRow88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow88.Weight = 0.8;
            // 
            // lblJDCaseno
            // 
            this.lblJDCaseno.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJDCaseno.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJDCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.CaseNumber")});
            this.lblJDCaseno.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJDCaseno.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDCaseno.Name = "lblJDCaseno";
            this.lblJDCaseno.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDCaseno.StylePriority.UseBorderColor = false;
            this.lblJDCaseno.StylePriority.UseBorders = false;
            this.lblJDCaseno.StylePriority.UseFont = false;
            this.lblJDCaseno.StylePriority.UseForeColor = false;
            this.lblJDCaseno.StylePriority.UsePadding = false;
            this.lblJDCaseno.Text = "lblJDCaseno";
            this.lblJDCaseno.Weight = 0.24139520564115574;
            // 
            // lblJDIssueDate
            // 
            this.lblJDIssueDate.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJDIssueDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJDIssueDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.CaseFilingDate")});
            this.lblJDIssueDate.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJDIssueDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDIssueDate.Name = "lblJDIssueDate";
            this.lblJDIssueDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDIssueDate.StylePriority.UseBorderColor = false;
            this.lblJDIssueDate.StylePriority.UseBorders = false;
            this.lblJDIssueDate.StylePriority.UseFont = false;
            this.lblJDIssueDate.StylePriority.UseForeColor = false;
            this.lblJDIssueDate.StylePriority.UsePadding = false;
            this.lblJDIssueDate.Text = "lblJDIssueDate";
            this.lblJDIssueDate.Weight = 0.26134107920613386;
            // 
            // lblJDType
            // 
            this.lblJDType.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJDType.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJDType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.CaseType")});
            this.lblJDType.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJDType.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDType.Name = "lblJDType";
            this.lblJDType.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDType.StylePriority.UseBorderColor = false;
            this.lblJDType.StylePriority.UseBorders = false;
            this.lblJDType.StylePriority.UseFont = false;
            this.lblJDType.StylePriority.UseForeColor = false;
            this.lblJDType.StylePriority.UsePadding = false;
            this.lblJDType.Text = "lblJDType";
            this.lblJDType.Weight = 0.31734267951125883;
            // 
            // lblJDAmount
            // 
            this.lblJDAmount.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJDAmount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.DisputeAmt")});
            this.lblJDAmount.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJDAmount.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDAmount.Name = "lblJDAmount";
            this.lblJDAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDAmount.StylePriority.UseBorderColor = false;
            this.lblJDAmount.StylePriority.UseBorders = false;
            this.lblJDAmount.StylePriority.UseFont = false;
            this.lblJDAmount.StylePriority.UseForeColor = false;
            this.lblJDAmount.StylePriority.UsePadding = false;
            this.lblJDAmount.Text = "lblJDAmount";
            this.lblJDAmount.Weight = 0.24657268173885949;
            // 
            // lblJDPlaintiff
            // 
            this.lblJDPlaintiff.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJDPlaintiff.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJDPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.PlaintiffName")});
            this.lblJDPlaintiff.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJDPlaintiff.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDPlaintiff.Name = "lblJDPlaintiff";
            this.lblJDPlaintiff.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDPlaintiff.StylePriority.UseBorderColor = false;
            this.lblJDPlaintiff.StylePriority.UseBorders = false;
            this.lblJDPlaintiff.StylePriority.UseFont = false;
            this.lblJDPlaintiff.StylePriority.UseForeColor = false;
            this.lblJDPlaintiff.StylePriority.UsePadding = false;
            this.lblJDPlaintiff.Text = "lblJDPlaintiff";
            this.lblJDPlaintiff.Weight = 0.26134111877034005;
            // 
            // lblJDCourt
            // 
            this.lblJDCourt.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJDCourt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJDCourt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.CourtName")});
            this.lblJDCourt.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJDCourt.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDCourt.Name = "lblJDCourt";
            this.lblJDCourt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDCourt.StylePriority.UseBorderColor = false;
            this.lblJDCourt.StylePriority.UseBorders = false;
            this.lblJDCourt.StylePriority.UseFont = false;
            this.lblJDCourt.StylePriority.UseForeColor = false;
            this.lblJDCourt.StylePriority.UsePadding = false;
            this.lblJDCourt.Text = "lblJDCourt";
            this.lblJDCourt.Weight = 0.27610947863366164;
            // 
            // lblJAttroneyNameD
            // 
            this.lblJAttroneyNameD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJAttroneyNameD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJAttroneyNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.AttorneyName")});
            this.lblJAttroneyNameD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJAttroneyNameD.ForeColor = System.Drawing.Color.DimGray;
            this.lblJAttroneyNameD.Name = "lblJAttroneyNameD";
            this.lblJAttroneyNameD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJAttroneyNameD.StylePriority.UseBorderColor = false;
            this.lblJAttroneyNameD.StylePriority.UseBorders = false;
            this.lblJAttroneyNameD.StylePriority.UseFont = false;
            this.lblJAttroneyNameD.StylePriority.UseForeColor = false;
            this.lblJAttroneyNameD.StylePriority.UsePadding = false;
            this.lblJAttroneyNameD.Text = "lblJAttroneyNameD";
            this.lblJAttroneyNameD.Weight = 0.31444425764330108;
            // 
            // lblAttroneyNoD
            // 
            this.lblAttroneyNoD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblAttroneyNoD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAttroneyNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.TelephoneNo")});
            this.lblAttroneyNoD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblAttroneyNoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblAttroneyNoD.Name = "lblAttroneyNoD";
            this.lblAttroneyNoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAttroneyNoD.StylePriority.UseBorderColor = false;
            this.lblAttroneyNoD.StylePriority.UseBorders = false;
            this.lblAttroneyNoD.StylePriority.UseFont = false;
            this.lblAttroneyNoD.StylePriority.UseForeColor = false;
            this.lblAttroneyNoD.StylePriority.UsePadding = false;
            this.lblAttroneyNoD.Text = "lblAttroneyNoD";
            this.lblAttroneyNoD.Weight = 0.32024077561330266;
            // 
            // xrTableCell492
            // 
            this.xrTableCell492.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell492.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell492.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.CommercialName")});
            this.xrTableCell492.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell492.Name = "xrTableCell492";
            this.xrTableCell492.StylePriority.UseBorderColor = false;
            this.xrTableCell492.StylePriority.UseBorders = false;
            this.xrTableCell492.StylePriority.UseFont = false;
            this.xrTableCell492.Text = "xrTableCell492";
            this.xrTableCell492.Weight = 0.43977812367462249;
            // 
            // lblJCaseReasonD
            // 
            this.lblJCaseReasonD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblJCaseReasonD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJCaseReasonD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialJudgment.CaseReason")});
            this.lblJCaseReasonD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblJCaseReasonD.ForeColor = System.Drawing.Color.DimGray;
            this.lblJCaseReasonD.Name = "lblJCaseReasonD";
            this.lblJCaseReasonD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJCaseReasonD.StylePriority.UseBorderColor = false;
            this.lblJCaseReasonD.StylePriority.UseBorders = false;
            this.lblJCaseReasonD.StylePriority.UseFont = false;
            this.lblJCaseReasonD.StylePriority.UseForeColor = false;
            this.lblJCaseReasonD.StylePriority.UsePadding = false;
            this.lblJCaseReasonD.Text = "lblJCaseReasonD";
            this.lblJCaseReasonD.Weight = 0.28580866210816747;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCJud,
            this.tblJudgmentsH});
            this.GroupHeader3.HeightF = 59F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // tblCJud
            // 
            this.tblCJud.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.tblCJud.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCJud.LocationFloat = new DevExpress.Utils.PointFloat(12.15833F, 39F);
            this.tblCJud.Name = "tblCJud";
            this.tblCJud.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tblJudgementH});
            this.tblCJud.SizeF = new System.Drawing.SizeF(792.3418F, 20F);
            this.tblCJud.StylePriority.UseBorderColor = false;
            this.tblCJud.StylePriority.UseBorders = false;
            // 
            // tblJudgementH
            // 
            this.tblJudgementH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblJudgementH.BorderColor = System.Drawing.Color.White;
            this.tblJudgementH.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJCaseNo,
            this.lblJIssueDate,
            this.lblJType,
            this.lblJAmount,
            this.lblJPlaintiff,
            this.lblJCourt,
            this.lblJAttroneyNameC,
            this.lblAttroneyNoC,
            this.xrTableCell363,
            this.lblJCaseReason});
            this.tblJudgementH.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblJudgementH.ForeColor = System.Drawing.Color.Gray;
            this.tblJudgementH.Name = "tblJudgementH";
            this.tblJudgementH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblJudgementH.StylePriority.UseBackColor = false;
            this.tblJudgementH.StylePriority.UseBorderColor = false;
            this.tblJudgementH.StylePriority.UseFont = false;
            this.tblJudgementH.StylePriority.UseForeColor = false;
            this.tblJudgementH.StylePriority.UsePadding = false;
            this.tblJudgementH.StylePriority.UseTextAlignment = false;
            this.tblJudgementH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.tblJudgementH.Weight = 0.8;
            // 
            // lblJCaseNo
            // 
            this.lblJCaseNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJCaseNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJCaseNo.Name = "lblJCaseNo";
            this.lblJCaseNo.StylePriority.UseBorders = false;
            this.lblJCaseNo.StylePriority.UseFont = false;
            this.lblJCaseNo.Text = "Case No.";
            this.lblJCaseNo.Weight = 0.24621212814793442;
            // 
            // lblJIssueDate
            // 
            this.lblJIssueDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJIssueDate.Name = "lblJIssueDate";
            this.lblJIssueDate.StylePriority.UseFont = false;
            this.lblJIssueDate.Text = "Issue date";
            this.lblJIssueDate.Weight = 0.26515152208732851;
            // 
            // lblJType
            // 
            this.lblJType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJType.Name = "lblJType";
            this.lblJType.StylePriority.UseFont = false;
            this.lblJType.Text = "Judgment Type";
            this.lblJType.Weight = 0.32196970390551016;
            // 
            // lblJAmount
            // 
            this.lblJAmount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJAmount.Name = "lblJAmount";
            this.lblJAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblJAmount.StylePriority.UseFont = false;
            this.lblJAmount.StylePriority.UsePadding = false;
            this.lblJAmount.Text = "Amount";
            this.lblJAmount.Weight = 0.24621212814793445;
            // 
            // lblJPlaintiff
            // 
            this.lblJPlaintiff.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJPlaintiff.Name = "lblJPlaintiff";
            this.lblJPlaintiff.StylePriority.UseFont = false;
            this.lblJPlaintiff.Text = "Plaintiff Name";
            this.lblJPlaintiff.Weight = 0.26515151515151508;
            // 
            // lblJCourt
            // 
            this.lblJCourt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJCourt.Name = "lblJCourt";
            this.lblJCourt.StylePriority.UseFont = false;
            this.lblJCourt.Text = "Court Name";
            this.lblJCourt.Weight = 0.28409090909090912;
            // 
            // lblJAttroneyNameC
            // 
            this.lblJAttroneyNameC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJAttroneyNameC.Name = "lblJAttroneyNameC";
            this.lblJAttroneyNameC.StylePriority.UseFont = false;
            this.lblJAttroneyNameC.Text = "Attorney Name";
            this.lblJAttroneyNameC.Weight = 0.32196969754768134;
            // 
            // lblAttroneyNoC
            // 
            this.lblAttroneyNoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAttroneyNoC.Name = "lblAttroneyNoC";
            this.lblAttroneyNoC.StylePriority.UseFont = false;
            this.lblAttroneyNoC.Text = "Attorney Phone No";
            this.lblAttroneyNoC.Weight = 0.32196966835946766;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseFont = false;
            this.xrTableCell363.Text = "Commercial Name";
            this.xrTableCell363.Weight = 0.44618979008663284;
            // 
            // lblJCaseReason
            // 
            this.lblJCaseReason.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblJCaseReason.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJCaseReason.Name = "lblJCaseReason";
            this.lblJCaseReason.StylePriority.UseBorders = false;
            this.lblJCaseReason.StylePriority.UseFont = false;
            this.lblJCaseReason.Text = "Case reason";
            this.lblJCaseReason.Weight = 0.28997583700040319;
            // 
            // tblJudgmentsH
            // 
            this.tblJudgmentsH.ForeColor = System.Drawing.Color.Black;
            this.tblJudgmentsH.LocationFloat = new DevExpress.Utils.PointFloat(12.15832F, 13.99994F);
            this.tblJudgmentsH.Name = "tblJudgmentsH";
            this.tblJudgmentsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.tblJudgmentsH.SizeF = new System.Drawing.SizeF(792.3418F, 25F);
            this.tblJudgmentsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJudgmentsH});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1;
            // 
            // lblJudgmentsH
            // 
            this.lblJudgmentsH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblJudgmentsH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblJudgmentsH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblJudgmentsH.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblJudgmentsH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblJudgmentsH.Name = "lblJudgmentsH";
            this.lblJudgmentsH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJudgmentsH.StylePriority.UseBackColor = false;
            this.lblJudgmentsH.StylePriority.UseBorderColor = false;
            this.lblJudgmentsH.StylePriority.UseBorders = false;
            this.lblJudgmentsH.StylePriority.UseFont = false;
            this.lblJudgmentsH.StylePriority.UseForeColor = false;
            this.lblJudgmentsH.StylePriority.UsePadding = false;
            this.lblJudgmentsH.StylePriority.UseTextAlignment = false;
            this.lblJudgmentsH.Text = "Judgments";
            this.lblJudgmentsH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblJudgmentsH.Weight = 3;
            // 
            // DRCommAUDInfo
            // 
            this.DRCommAUDInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommAUDInfo,
            this.GroupHeader4});
            this.DRCommAUDInfo.DataMember = "CommercialAuditorInformation";
            this.DRCommAUDInfo.Level = 3;
            this.DRCommAUDInfo.Name = "DRCommAUDInfo";
            // 
            // DCommAUDInfo
            // 
            this.DCommAUDInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblAuditorH,
            this.xrTable49});
            this.DCommAUDInfo.HeightF = 195F;
            this.DCommAUDInfo.Name = "DCommAUDInfo";
            // 
            // lblAuditorH
            // 
            this.lblAuditorH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAuditorH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblAuditorH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAuditorH.BorderWidth = 2;
            this.lblAuditorH.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblAuditorH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblAuditorH.LocationFloat = new DevExpress.Utils.PointFloat(13.20012F, 10.0001F);
            this.lblAuditorH.Name = "lblAuditorH";
            this.lblAuditorH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAuditorH.SizeF = new System.Drawing.SizeF(789.9999F, 19.99995F);
            this.lblAuditorH.StylePriority.UseBackColor = false;
            this.lblAuditorH.StylePriority.UseBorderColor = false;
            this.lblAuditorH.StylePriority.UseBorders = false;
            this.lblAuditorH.StylePriority.UseBorderWidth = false;
            this.lblAuditorH.StylePriority.UseFont = false;
            this.lblAuditorH.StylePriority.UseForeColor = false;
            this.lblAuditorH.StylePriority.UsePadding = false;
            this.lblAuditorH.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "Active Auditor- {0}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            this.lblAuditorH.Summary = xrSummary1;
            this.lblAuditorH.Text = "Active Auditor: [AuditorName]";
            this.lblAuditorH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable49
            // 
            this.xrTable49.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable49.ForeColor = System.Drawing.Color.DimGray;
            this.xrTable49.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 35F);
            this.xrTable49.Name = "xrTable49";
            this.xrTable49.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow158,
            this.xrTableRow159,
            this.xrTableRow160,
            this.xrTableRow161,
            this.xrTableRow2,
            this.xrTableRow11,
            this.xrTableRow163,
            this.xrTableRow164});
            this.xrTable49.SizeF = new System.Drawing.SizeF(790F, 160F);
            this.xrTable49.StylePriority.UseFont = false;
            this.xrTable49.StylePriority.UseForeColor = false;
            // 
            // xrTableRow158
            // 
            this.xrTableRow158.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell335,
            this.xrTableCell336});
            this.xrTableRow158.Name = "xrTableRow158";
            this.xrTableRow158.Weight = 1.0000008583075941;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell335.BorderColor = System.Drawing.Color.White;
            this.xrTableCell335.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell335.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell335.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell335.StylePriority.UseBackColor = false;
            this.xrTableCell335.StylePriority.UseBorderColor = false;
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.StylePriority.UseFont = false;
            this.xrTableCell335.StylePriority.UseForeColor = false;
            this.xrTableCell335.StylePriority.UsePadding = false;
            this.xrTableCell335.StylePriority.UseTextAlignment = false;
            this.xrTableCell335.Text = "Auditor Name";
            this.xrTableCell335.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell335.Weight = 0.73029760956350853;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.BackColor = System.Drawing.Color.White;
            this.xrTableCell336.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell336.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell336.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.AuditorName")});
            this.xrTableCell336.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell336.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell336.StylePriority.UseBackColor = false;
            this.xrTableCell336.StylePriority.UseBorderColor = false;
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.StylePriority.UseFont = false;
            this.xrTableCell336.StylePriority.UseForeColor = false;
            this.xrTableCell336.StylePriority.UsePadding = false;
            this.xrTableCell336.StylePriority.UseTextAlignment = false;
            this.xrTableCell336.Text = "xrTableCell61";
            this.xrTableCell336.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell336.Weight = 2.2697023904364912;
            // 
            // xrTableRow159
            // 
            this.xrTableRow159.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340});
            this.xrTableRow159.Name = "xrTableRow159";
            this.xrTableRow159.Weight = 1.0000008583075941;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell337.BorderColor = System.Drawing.Color.White;
            this.xrTableCell337.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell337.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell337.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell337.Multiline = true;
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell337.StylePriority.UseBackColor = false;
            this.xrTableCell337.StylePriority.UseBorderColor = false;
            this.xrTableCell337.StylePriority.UseBorders = false;
            this.xrTableCell337.StylePriority.UseFont = false;
            this.xrTableCell337.StylePriority.UseForeColor = false;
            this.xrTableCell337.StylePriority.UsePadding = false;
            this.xrTableCell337.StylePriority.UseTextAlignment = false;
            this.xrTableCell337.Text = "Profession No\r\n";
            this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell337.Weight = 0.73029760956350853;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.BackColor = System.Drawing.Color.White;
            this.xrTableCell338.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell338.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell338.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.ProfessionNo")});
            this.xrTableCell338.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell338.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell338.StylePriority.UseBackColor = false;
            this.xrTableCell338.StylePriority.UseBorderColor = false;
            this.xrTableCell338.StylePriority.UseBorders = false;
            this.xrTableCell338.StylePriority.UseFont = false;
            this.xrTableCell338.StylePriority.UseForeColor = false;
            this.xrTableCell338.StylePriority.UsePadding = false;
            this.xrTableCell338.StylePriority.UseTextAlignment = false;
            this.xrTableCell338.Text = "xrTableCell104";
            this.xrTableCell338.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell338.Weight = 0.77509808723797879;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell339.BorderColor = System.Drawing.Color.White;
            this.xrTableCell339.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell339.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell339.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell339.StylePriority.UseBackColor = false;
            this.xrTableCell339.StylePriority.UseBorderColor = false;
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.StylePriority.UseFont = false;
            this.xrTableCell339.StylePriority.UseForeColor = false;
            this.xrTableCell339.StylePriority.UsePadding = false;
            this.xrTableCell339.StylePriority.UseTextAlignment = false;
            this.xrTableCell339.Text = "Update Date";
            this.xrTableCell339.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell339.Weight = 0.72908162235839358;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.BackColor = System.Drawing.Color.White;
            this.xrTableCell340.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell340.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell340.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.LastUpdatedDate")});
            this.xrTableCell340.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell340.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell340.StylePriority.UseBackColor = false;
            this.xrTableCell340.StylePriority.UseBorderColor = false;
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.StylePriority.UseFont = false;
            this.xrTableCell340.StylePriority.UseForeColor = false;
            this.xrTableCell340.StylePriority.UsePadding = false;
            this.xrTableCell340.StylePriority.UseTextAlignment = false;
            this.xrTableCell340.Text = "xrTableCell106";
            this.xrTableCell340.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell340.Weight = 0.76552268084011887;
            // 
            // xrTableRow160
            // 
            this.xrTableRow160.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell341,
            this.xrTableCell342,
            this.xrTableCell343,
            this.xrTableCell344});
            this.xrTableRow160.Name = "xrTableRow160";
            this.xrTableRow160.Weight = 1.0000008583075941;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell341.BorderColor = System.Drawing.Color.White;
            this.xrTableCell341.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell341.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell341.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell341.StylePriority.UseBackColor = false;
            this.xrTableCell341.StylePriority.UseBorderColor = false;
            this.xrTableCell341.StylePriority.UseBorders = false;
            this.xrTableCell341.StylePriority.UseFont = false;
            this.xrTableCell341.StylePriority.UseForeColor = false;
            this.xrTableCell341.StylePriority.UsePadding = false;
            this.xrTableCell341.StylePriority.UseTextAlignment = false;
            this.xrTableCell341.Text = "Auditor Status\t";
            this.xrTableCell341.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell341.Weight = 0.73029760956350853;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.BackColor = System.Drawing.Color.White;
            this.xrTableCell342.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell342.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell342.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.AuditorStatusDesc")});
            this.xrTableCell342.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell342.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell342.StylePriority.UseBackColor = false;
            this.xrTableCell342.StylePriority.UseBorderColor = false;
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.StylePriority.UseFont = false;
            this.xrTableCell342.StylePriority.UseForeColor = false;
            this.xrTableCell342.StylePriority.UsePadding = false;
            this.xrTableCell342.StylePriority.UseTextAlignment = false;
            this.xrTableCell342.Text = "xrTableCell293";
            this.xrTableCell342.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell342.Weight = 0.77509808723797879;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell343.BorderColor = System.Drawing.Color.White;
            this.xrTableCell343.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell343.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell343.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell343.StylePriority.UseBackColor = false;
            this.xrTableCell343.StylePriority.UseBorderColor = false;
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.StylePriority.UseFont = false;
            this.xrTableCell343.StylePriority.UseForeColor = false;
            this.xrTableCell343.StylePriority.UsePadding = false;
            this.xrTableCell343.StylePriority.UseTextAlignment = false;
            this.xrTableCell343.Text = "No of years in Business";
            this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell343.Weight = 0.72908162235839358;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.BackColor = System.Drawing.Color.White;
            this.xrTableCell344.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell344.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell344.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.YearswithAuditor")});
            this.xrTableCell344.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell344.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell344.StylePriority.UseBackColor = false;
            this.xrTableCell344.StylePriority.UseBorderColor = false;
            this.xrTableCell344.StylePriority.UseBorders = false;
            this.xrTableCell344.StylePriority.UseFont = false;
            this.xrTableCell344.StylePriority.UseForeColor = false;
            this.xrTableCell344.StylePriority.UsePadding = false;
            this.xrTableCell344.StylePriority.UseTextAlignment = false;
            this.xrTableCell344.Text = "xrTableCell114";
            this.xrTableCell344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell344.Weight = 0.76552268084011887;
            // 
            // xrTableRow161
            // 
            this.xrTableRow161.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell345,
            this.xrTableCell346,
            this.xrTableCell347,
            this.xrTableCell348});
            this.xrTableRow161.Name = "xrTableRow161";
            this.xrTableRow161.Weight = 1.0000008583075941;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell345.BorderColor = System.Drawing.Color.White;
            this.xrTableCell345.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell345.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell345.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell345.StylePriority.UseBackColor = false;
            this.xrTableCell345.StylePriority.UseBorderColor = false;
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.StylePriority.UseFont = false;
            this.xrTableCell345.StylePriority.UseForeColor = false;
            this.xrTableCell345.StylePriority.UsePadding = false;
            this.xrTableCell345.StylePriority.UseTextAlignment = false;
            this.xrTableCell345.Text = "Auditor Start Date ";
            this.xrTableCell345.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell345.Weight = 0.73029755272442409;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.BackColor = System.Drawing.Color.White;
            this.xrTableCell346.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell346.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell346.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.ActstartDate")});
            this.xrTableCell346.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell346.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell346.StylePriority.UseBackColor = false;
            this.xrTableCell346.StylePriority.UseBorderColor = false;
            this.xrTableCell346.StylePriority.UseBorders = false;
            this.xrTableCell346.StylePriority.UseFont = false;
            this.xrTableCell346.StylePriority.UseForeColor = false;
            this.xrTableCell346.StylePriority.UsePadding = false;
            this.xrTableCell346.StylePriority.UseTextAlignment = false;
            this.xrTableCell346.Text = "xrTableCell297";
            this.xrTableCell346.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell346.Weight = 0.77509802693543173;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell347.BorderColor = System.Drawing.Color.White;
            this.xrTableCell347.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell347.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell347.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell347.StylePriority.UseBackColor = false;
            this.xrTableCell347.StylePriority.UseBorderColor = false;
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.StylePriority.UseFont = false;
            this.xrTableCell347.StylePriority.UseForeColor = false;
            this.xrTableCell347.StylePriority.UsePadding = false;
            this.xrTableCell347.StylePriority.UseTextAlignment = false;
            this.xrTableCell347.Text = "Auditor End Date";
            this.xrTableCell347.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell347.Weight = 0.72908156523252277;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.BackColor = System.Drawing.Color.White;
            this.xrTableCell348.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell348.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.ActEndDate")});
            this.xrTableCell348.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell348.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell348.StylePriority.UseBackColor = false;
            this.xrTableCell348.StylePriority.UseBorderColor = false;
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.StylePriority.UseFont = false;
            this.xrTableCell348.StylePriority.UseForeColor = false;
            this.xrTableCell348.StylePriority.UsePadding = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.Text = "xrTableCell299";
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell348.Weight = 0.765522855107621;
            // 
            // xrTableRow163
            // 
            this.xrTableRow163.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell349,
            this.xrTableCell350});
            this.xrTableRow163.Name = "xrTableRow163";
            this.xrTableRow163.Weight = 1.0000008583075941;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell349.BorderColor = System.Drawing.Color.White;
            this.xrTableCell349.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell349.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell349.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell349.StylePriority.UseBackColor = false;
            this.xrTableCell349.StylePriority.UseBorderColor = false;
            this.xrTableCell349.StylePriority.UseBorders = false;
            this.xrTableCell349.StylePriority.UseFont = false;
            this.xrTableCell349.StylePriority.UsePadding = false;
            this.xrTableCell349.Text = "Physical Address";
            this.xrTableCell349.Weight = 0.73029755272442409;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.BackColor = System.Drawing.Color.White;
            this.xrTableCell350.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell350.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell350.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.PhysicalAddress")});
            this.xrTableCell350.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell350.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell350.StylePriority.UseBackColor = false;
            this.xrTableCell350.StylePriority.UseBorderColor = false;
            this.xrTableCell350.StylePriority.UseBorders = false;
            this.xrTableCell350.StylePriority.UseFont = false;
            this.xrTableCell350.StylePriority.UsePadding = false;
            this.xrTableCell350.Text = "xrTableCell128";
            this.xrTableCell350.Weight = 2.2697024472755754;
            // 
            // xrTableRow164
            // 
            this.xrTableRow164.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell351,
            this.xrTableCell352});
            this.xrTableRow164.Name = "xrTableRow164";
            this.xrTableRow164.Weight = 1.0000006675725732;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell351.BorderColor = System.Drawing.Color.White;
            this.xrTableCell351.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell351.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell351.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell351.StylePriority.UseBackColor = false;
            this.xrTableCell351.StylePriority.UseBorderColor = false;
            this.xrTableCell351.StylePriority.UseBorders = false;
            this.xrTableCell351.StylePriority.UseFont = false;
            this.xrTableCell351.StylePriority.UsePadding = false;
            this.xrTableCell351.Text = "Postal Address";
            this.xrTableCell351.Weight = 0.73029755272442409;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.BackColor = System.Drawing.Color.White;
            this.xrTableCell352.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell352.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell352.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.PostalAddress")});
            this.xrTableCell352.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell352.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell352.StylePriority.UseBackColor = false;
            this.xrTableCell352.StylePriority.UseBorderColor = false;
            this.xrTableCell352.StylePriority.UseBorders = false;
            this.xrTableCell352.StylePriority.UseFont = false;
            this.xrTableCell352.StylePriority.UsePadding = false;
            this.xrTableCell352.Text = "xrTableCell130";
            this.xrTableCell352.Weight = 2.2697024472755754;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable23});
            this.GroupHeader4.HeightF = 59F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.xrTable23.SizeF = new System.Drawing.SizeF(790F, 40.62F);
            this.xrTable23.StylePriority.UseBorders = false;
            this.xrTable23.StylePriority.UseFont = false;
            this.xrTable23.StylePriority.UseForeColor = false;
            this.xrTable23.StylePriority.UsePadding = false;
            this.xrTable23.StylePriority.UseTextAlignment = false;
            this.xrTable23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAUDInfoH,
            this.xrTableCell46});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 0.8;
            // 
            // lblAUDInfoH
            // 
            this.lblAUDInfoH.BackColor = System.Drawing.Color.White;
            this.lblAUDInfoH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAUDInfoH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblAUDInfoH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblAUDInfoH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblAUDInfoH.Name = "lblAUDInfoH";
            this.lblAUDInfoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAUDInfoH.StylePriority.UseBackColor = false;
            this.lblAUDInfoH.StylePriority.UseBorderColor = false;
            this.lblAUDInfoH.StylePriority.UseBorders = false;
            this.lblAUDInfoH.StylePriority.UseFont = false;
            this.lblAUDInfoH.StylePriority.UseForeColor = false;
            this.lblAUDInfoH.StylePriority.UsePadding = false;
            this.lblAUDInfoH.Text = "Auditors";
            this.lblAUDInfoH.Weight = 1.1756499500551834;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.BackColor = System.Drawing.Color.White;
            this.xrTableCell46.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell46.BorderWidth = 1;
            this.xrTableCell46.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell46.StylePriority.UseBackColor = false;
            this.xrTableCell46.StylePriority.UseBorderColor = false;
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.StylePriority.UseBorderWidth = false;
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseForeColor = false;
            this.xrTableCell46.StylePriority.UsePadding = false;
            this.xrTableCell46.Weight = 1.8243500499448164;
            // 
            // DRCommDIRInfo
            // 
            this.DRCommDIRInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommDIRInfo,
            this.GroupHeader5});
            this.DRCommDIRInfo.DataMember = "CommercialActivePrincipalInformation";
            this.DRCommDIRInfo.Level = 15;
            this.DRCommDIRInfo.Name = "DRCommDIRInfo";
            // 
            // DCommDIRInfo
            // 
            this.DCommDIRInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable33,
            this.xrLabel3,
            this.xrLabel2,
            this.lblDirectorRecPosition});
            this.DCommDIRInfo.HeightF = 349F;
            this.DCommDIRInfo.Name = "DCommDIRInfo";
            // 
            // xrTable33
            // 
            this.xrTable33.ForeColor = System.Drawing.Color.DimGray;
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 77.58331F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow177,
            this.xrTableRow178,
            this.xrTableRow180,
            this.xrTableRow198,
            this.xrTableRow199,
            this.xrTableRow200,
            this.xrTableRow201,
            this.xrTableRow19,
            this.xrTableRow142,
            this.xrTableRow202,
            this.xrTableRow147});
            this.xrTable33.SizeF = new System.Drawing.SizeF(790F, 220F);
            this.xrTable33.StylePriority.UseForeColor = false;
            // 
            // xrTableRow177
            // 
            this.xrTableRow177.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.LblFullName});
            this.xrTableRow177.Name = "xrTableRow177";
            this.xrTableRow177.Weight = 0.90909090909090906;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell44.BorderColor = System.Drawing.Color.White;
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell44.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell44.StylePriority.UseBackColor = false;
            this.xrTableCell44.StylePriority.UseBorderColor = false;
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseForeColor = false;
            this.xrTableCell44.StylePriority.UsePadding = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "Full Name";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell44.Weight = 2.1750001525878906;
            // 
            // LblFullName
            // 
            this.LblFullName.BackColor = System.Drawing.Color.White;
            this.LblFullName.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.LblFullName.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.LblFullName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.Fullname")});
            this.LblFullName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFullName.ForeColor = System.Drawing.Color.DimGray;
            this.LblFullName.Name = "LblFullName";
            this.LblFullName.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblFullName.StylePriority.UseBackColor = false;
            this.LblFullName.StylePriority.UseBorderColor = false;
            this.LblFullName.StylePriority.UseBorders = false;
            this.LblFullName.StylePriority.UseFont = false;
            this.LblFullName.StylePriority.UseForeColor = false;
            this.LblFullName.StylePriority.UsePadding = false;
            this.LblFullName.StylePriority.UseTextAlignment = false;
            this.LblFullName.Text = "LblFullName";
            this.LblFullName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblFullName.Weight = 5.7546867328219946;
            // 
            // xrTableRow178
            // 
            this.xrTableRow178.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.LblPreviousSurname,
            this.xrTableCell84,
            this.xrTableCell93});
            this.xrTableRow178.Name = "xrTableRow178";
            this.xrTableRow178.Weight = 0.909090909090909;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell77.BorderColor = System.Drawing.Color.White;
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell77.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell77.StylePriority.UseBackColor = false;
            this.xrTableCell77.StylePriority.UseBorderColor = false;
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.StylePriority.UseFont = false;
            this.xrTableCell77.StylePriority.UseForeColor = false;
            this.xrTableCell77.StylePriority.UsePadding = false;
            this.xrTableCell77.StylePriority.UseTextAlignment = false;
            this.xrTableCell77.Text = "Previous Surname";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell77.Weight = 2.1750001525878906;
            // 
            // LblPreviousSurname
            // 
            this.LblPreviousSurname.BackColor = System.Drawing.Color.White;
            this.LblPreviousSurname.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.LblPreviousSurname.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.LblPreviousSurname.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.SurnamePrevious")});
            this.LblPreviousSurname.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPreviousSurname.ForeColor = System.Drawing.Color.DimGray;
            this.LblPreviousSurname.Name = "LblPreviousSurname";
            this.LblPreviousSurname.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblPreviousSurname.StylePriority.UseBackColor = false;
            this.LblPreviousSurname.StylePriority.UseBorderColor = false;
            this.LblPreviousSurname.StylePriority.UseBorders = false;
            this.LblPreviousSurname.StylePriority.UseFont = false;
            this.LblPreviousSurname.StylePriority.UseForeColor = false;
            this.LblPreviousSurname.StylePriority.UsePadding = false;
            this.LblPreviousSurname.StylePriority.UseTextAlignment = false;
            this.LblPreviousSurname.Text = "LblPreviousSurname";
            this.LblPreviousSurname.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblPreviousSurname.Weight = 2.1326242425706652;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell84.BorderColor = System.Drawing.Color.White;
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell84.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell84.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell84.StylePriority.UseBackColor = false;
            this.xrTableCell84.StylePriority.UseBorderColor = false;
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.StylePriority.UseFont = false;
            this.xrTableCell84.StylePriority.UseForeColor = false;
            this.xrTableCell84.StylePriority.UsePadding = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.Text = "Principle Type";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell84.Weight = 1.7008968501620823;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.BackColor = System.Drawing.Color.White;
            this.xrTableCell93.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell93.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.PrincipalType")});
            this.xrTableCell93.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell93.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell93.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell93.StylePriority.UseBackColor = false;
            this.xrTableCell93.StylePriority.UseBorderColor = false;
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.StylePriority.UseFont = false;
            this.xrTableCell93.StylePriority.UseForeColor = false;
            this.xrTableCell93.StylePriority.UsePadding = false;
            this.xrTableCell93.StylePriority.UseTextAlignment = false;
            this.xrTableCell93.Text = "xrTableCell93";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell93.Weight = 1.9211656400892472;
            // 
            // xrTableRow180
            // 
            this.xrTableRow180.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell99,
            this.xrTableCell100});
            this.xrTableRow180.Name = "xrTableRow180";
            this.xrTableRow180.Weight = 0.90909090909090917;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell94.BorderColor = System.Drawing.Color.White;
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell94.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell94.StylePriority.UseBackColor = false;
            this.xrTableCell94.StylePriority.UseBorderColor = false;
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.StylePriority.UseFont = false;
            this.xrTableCell94.StylePriority.UseForeColor = false;
            this.xrTableCell94.StylePriority.UsePadding = false;
            this.xrTableCell94.StylePriority.UseTextAlignment = false;
            this.xrTableCell94.Text = "ID Number";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell94.Weight = 2.1750001525878906;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.BackColor = System.Drawing.Color.White;
            this.xrTableCell95.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.IDNo")});
            this.xrTableCell95.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell95.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell95.StylePriority.UseBackColor = false;
            this.xrTableCell95.StylePriority.UseBorderColor = false;
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.StylePriority.UseFont = false;
            this.xrTableCell95.StylePriority.UseForeColor = false;
            this.xrTableCell95.StylePriority.UsePadding = false;
            this.xrTableCell95.StylePriority.UseTextAlignment = false;
            this.xrTableCell95.Text = "xrTableCell95";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell95.Weight = 2.1326242425706652;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell99.BorderColor = System.Drawing.Color.White;
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell99.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell99.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell99.StylePriority.UseBackColor = false;
            this.xrTableCell99.StylePriority.UseBorderColor = false;
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseFont = false;
            this.xrTableCell99.StylePriority.UseForeColor = false;
            this.xrTableCell99.StylePriority.UsePadding = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.Text = "ID Verification";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell99.Weight = 1.7008968501620823;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.BackColor = System.Drawing.Color.White;
            this.xrTableCell100.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.ISIDVerified")});
            this.xrTableCell100.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell100.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell100.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell100.StylePriority.UseBackColor = false;
            this.xrTableCell100.StylePriority.UseBorderColor = false;
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.StylePriority.UseForeColor = false;
            this.xrTableCell100.StylePriority.UsePadding = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "xrTableCell100";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell100.Weight = 1.9211656400892472;
            // 
            // xrTableRow198
            // 
            this.xrTableRow198.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell108,
            this.xrTableCell110,
            this.xrTableCell111});
            this.xrTableRow198.Name = "xrTableRow198";
            this.xrTableRow198.Weight = 0.90909090909090906;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell101.BorderColor = System.Drawing.Color.White;
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell101.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell101.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell101.StylePriority.UseBackColor = false;
            this.xrTableCell101.StylePriority.UseBorderColor = false;
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.StylePriority.UseForeColor = false;
            this.xrTableCell101.StylePriority.UsePadding = false;
            this.xrTableCell101.StylePriority.UseTextAlignment = false;
            this.xrTableCell101.Text = "Birth Date ";
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell101.Weight = 2.1750001525878906;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.BackColor = System.Drawing.Color.White;
            this.xrTableCell108.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell108.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.BirthDate")});
            this.xrTableCell108.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell108.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell108.StylePriority.UseBackColor = false;
            this.xrTableCell108.StylePriority.UseBorderColor = false;
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.StylePriority.UseFont = false;
            this.xrTableCell108.StylePriority.UseForeColor = false;
            this.xrTableCell108.StylePriority.UsePadding = false;
            this.xrTableCell108.StylePriority.UseTextAlignment = false;
            this.xrTableCell108.Text = "xrTableCell108";
            this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell108.Weight = 2.1326242425706652;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell110.BorderColor = System.Drawing.Color.White;
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell110.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell110.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell110.StylePriority.UseBackColor = false;
            this.xrTableCell110.StylePriority.UseBorderColor = false;
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.StylePriority.UseFont = false;
            this.xrTableCell110.StylePriority.UseForeColor = false;
            this.xrTableCell110.StylePriority.UsePadding = false;
            this.xrTableCell110.StylePriority.UseTextAlignment = false;
            this.xrTableCell110.Text = "Age";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell110.Weight = 1.7008968501620823;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.BackColor = System.Drawing.Color.White;
            this.xrTableCell111.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell111.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.Age")});
            this.xrTableCell111.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell111.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell111.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell111.StylePriority.UseBackColor = false;
            this.xrTableCell111.StylePriority.UseBorderColor = false;
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.StylePriority.UseFont = false;
            this.xrTableCell111.StylePriority.UseForeColor = false;
            this.xrTableCell111.StylePriority.UsePadding = false;
            this.xrTableCell111.StylePriority.UseTextAlignment = false;
            this.xrTableCell111.Text = "xrTableCell111";
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell111.Weight = 1.9211656400892472;
            // 
            // xrTableRow199
            // 
            this.xrTableRow199.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDAge,
            this.lblDAgeD,
            this.lblDYearswithBus,
            this.lblDYearswithBusD});
            this.xrTableRow199.Name = "xrTableRow199";
            this.xrTableRow199.Weight = 0.90909090909090917;
            // 
            // lblDAge
            // 
            this.lblDAge.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDAge.BorderColor = System.Drawing.Color.White;
            this.lblDAge.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDAge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDAge.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAge.Name = "lblDAge";
            this.lblDAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAge.StylePriority.UseBackColor = false;
            this.lblDAge.StylePriority.UseBorderColor = false;
            this.lblDAge.StylePriority.UseBorders = false;
            this.lblDAge.StylePriority.UseFont = false;
            this.lblDAge.StylePriority.UseForeColor = false;
            this.lblDAge.StylePriority.UsePadding = false;
            this.lblDAge.Text = "South African Resident";
            this.lblDAge.Weight = 2.1750001525878906;
            // 
            // lblDAgeD
            // 
            this.lblDAgeD.BackColor = System.Drawing.Color.White;
            this.lblDAgeD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDAgeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDAgeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.ISRSAResident")});
            this.lblDAgeD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDAgeD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAgeD.Name = "lblDAgeD";
            this.lblDAgeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAgeD.StylePriority.UseBackColor = false;
            this.lblDAgeD.StylePriority.UseBorderColor = false;
            this.lblDAgeD.StylePriority.UseBorders = false;
            this.lblDAgeD.StylePriority.UseFont = false;
            this.lblDAgeD.StylePriority.UseForeColor = false;
            this.lblDAgeD.StylePriority.UsePadding = false;
            this.lblDAgeD.Text = "lblDAgeD";
            this.lblDAgeD.Weight = 2.1326242425706652;
            // 
            // lblDYearswithBus
            // 
            this.lblDYearswithBus.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDYearswithBus.BorderColor = System.Drawing.Color.White;
            this.lblDYearswithBus.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDYearswithBus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDYearswithBus.ForeColor = System.Drawing.Color.DimGray;
            this.lblDYearswithBus.Name = "lblDYearswithBus";
            this.lblDYearswithBus.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDYearswithBus.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDYearswithBus.StylePriority.UseBackColor = false;
            this.lblDYearswithBus.StylePriority.UseBorderColor = false;
            this.lblDYearswithBus.StylePriority.UseBorders = false;
            this.lblDYearswithBus.StylePriority.UseFont = false;
            this.lblDYearswithBus.StylePriority.UseForeColor = false;
            this.lblDYearswithBus.StylePriority.UsePadding = false;
            this.lblDYearswithBus.Text = "Destignation";
            this.lblDYearswithBus.Weight = 1.7008968501620823;
            // 
            // lblDYearswithBusD
            // 
            this.lblDYearswithBusD.BackColor = System.Drawing.Color.White;
            this.lblDYearswithBusD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDYearswithBusD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDYearswithBusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.Designation")});
            this.lblDYearswithBusD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDYearswithBusD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDYearswithBusD.Name = "lblDYearswithBusD";
            this.lblDYearswithBusD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDYearswithBusD.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDYearswithBusD.StylePriority.UseBackColor = false;
            this.lblDYearswithBusD.StylePriority.UseBorderColor = false;
            this.lblDYearswithBusD.StylePriority.UseBorders = false;
            this.lblDYearswithBusD.StylePriority.UseFont = false;
            this.lblDYearswithBusD.StylePriority.UseForeColor = false;
            this.lblDYearswithBusD.StylePriority.UsePadding = false;
            this.lblDYearswithBusD.Text = "lblDYearswithBusD";
            this.lblDYearswithBusD.Weight = 1.9211656400892472;
            // 
            // xrTableRow200
            // 
            this.xrTableRow200.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell207,
            this.xrTableCell210});
            this.xrTableRow200.Name = "xrTableRow200";
            this.xrTableRow200.Weight = 0.90909090909090917;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell207.BorderColor = System.Drawing.Color.White;
            this.xrTableCell207.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell207.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell207.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell207.StylePriority.UseBackColor = false;
            this.xrTableCell207.StylePriority.UseBorderColor = false;
            this.xrTableCell207.StylePriority.UseBorders = false;
            this.xrTableCell207.StylePriority.UseFont = false;
            this.xrTableCell207.StylePriority.UseForeColor = false;
            this.xrTableCell207.StylePriority.UsePadding = false;
            this.xrTableCell207.StylePriority.UseTextAlignment = false;
            this.xrTableCell207.Text = "Principal Status";
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell207.Weight = 2.1750001525878906;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.BackColor = System.Drawing.Color.White;
            this.xrTableCell210.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell210.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.DirectorStatusCode")});
            this.xrTableCell210.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell210.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell210.StylePriority.UseBackColor = false;
            this.xrTableCell210.StylePriority.UseBorderColor = false;
            this.xrTableCell210.StylePriority.UseBorders = false;
            this.xrTableCell210.StylePriority.UseFont = false;
            this.xrTableCell210.StylePriority.UseForeColor = false;
            this.xrTableCell210.StylePriority.UsePadding = false;
            this.xrTableCell210.StylePriority.UseTextAlignment = false;
            this.xrTableCell210.Text = "xrTableCell210";
            this.xrTableCell210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell210.Weight = 5.7546867328219946;
            // 
            // xrTableRow201
            // 
            this.xrTableRow201.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell213,
            this.xrTableCell221,
            this.xrTableCell222,
            this.xrTableCell223});
            this.xrTableRow201.Name = "xrTableRow201";
            this.xrTableRow201.Weight = 0.90909090909090906;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell213.BorderColor = System.Drawing.Color.White;
            this.xrTableCell213.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell213.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell213.Multiline = true;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell213.StylePriority.UseBackColor = false;
            this.xrTableCell213.StylePriority.UseBorderColor = false;
            this.xrTableCell213.StylePriority.UseBorders = false;
            this.xrTableCell213.StylePriority.UseFont = false;
            this.xrTableCell213.StylePriority.UsePadding = false;
            this.xrTableCell213.Text = "Appointment Date\r\n";
            this.xrTableCell213.Weight = 2.1750001525878906;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.BackColor = System.Drawing.Color.White;
            this.xrTableCell221.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell221.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.AppointmentDate")});
            this.xrTableCell221.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell221.StylePriority.UseBackColor = false;
            this.xrTableCell221.StylePriority.UseBorderColor = false;
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.StylePriority.UseFont = false;
            this.xrTableCell221.StylePriority.UsePadding = false;
            this.xrTableCell221.Text = "xrTableCell221";
            this.xrTableCell221.Weight = 2.1326235702610674;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell222.BorderColor = System.Drawing.Color.White;
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell222.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell222.StylePriority.UseBackColor = false;
            this.xrTableCell222.StylePriority.UseBorderColor = false;
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.StylePriority.UseFont = false;
            this.xrTableCell222.StylePriority.UsePadding = false;
            this.xrTableCell222.Text = "Years with Business";
            this.xrTableCell222.Weight = 1.7008970241333774;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.BackColor = System.Drawing.Color.White;
            this.xrTableCell223.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell223.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell223.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.YearsWithBusiness")});
            this.xrTableCell223.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell223.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell223.StylePriority.UseBackColor = false;
            this.xrTableCell223.StylePriority.UseBorderColor = false;
            this.xrTableCell223.StylePriority.UseBorders = false;
            this.xrTableCell223.StylePriority.UseFont = false;
            this.xrTableCell223.StylePriority.UsePadding = false;
            this.xrTableCell223.Text = "xrTableCell223";
            this.xrTableCell223.Weight = 1.9211661384275496;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell288,
            this.xrTableCell289});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.909090909090909;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell288.BorderColor = System.Drawing.Color.White;
            this.xrTableCell288.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell288.StylePriority.UseBackColor = false;
            this.xrTableCell288.StylePriority.UseBorderColor = false;
            this.xrTableCell288.StylePriority.UseFont = false;
            this.xrTableCell288.StylePriority.UsePadding = false;
            this.xrTableCell288.Text = "Residential Address";
            this.xrTableCell288.Weight = 2.1750001525878906;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.BackColor = System.Drawing.Color.White;
            this.xrTableCell289.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell289.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.PhysicalAddress")});
            this.xrTableCell289.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell289.StylePriority.UseBackColor = false;
            this.xrTableCell289.StylePriority.UseBorderColor = false;
            this.xrTableCell289.StylePriority.UseFont = false;
            this.xrTableCell289.StylePriority.UsePadding = false;
            this.xrTableCell289.Text = "xrTableCell289";
            this.xrTableCell289.Weight = 5.7546867328219946;
            // 
            // xrTableRow142
            // 
            this.xrTableRow142.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell292,
            this.xrTableCell293});
            this.xrTableRow142.Name = "xrTableRow142";
            this.xrTableRow142.Weight = 0.90909090909090906;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell292.BorderColor = System.Drawing.Color.White;
            this.xrTableCell292.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell292.StylePriority.UseBackColor = false;
            this.xrTableCell292.StylePriority.UseBorderColor = false;
            this.xrTableCell292.StylePriority.UseFont = false;
            this.xrTableCell292.StylePriority.UsePadding = false;
            this.xrTableCell292.Text = "Postal Address";
            this.xrTableCell292.Weight = 2.1750001525878906;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.BackColor = System.Drawing.Color.White;
            this.xrTableCell293.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell293.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.PostalAddress")});
            this.xrTableCell293.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell293.StylePriority.UseBackColor = false;
            this.xrTableCell293.StylePriority.UseBorderColor = false;
            this.xrTableCell293.StylePriority.UseFont = false;
            this.xrTableCell293.StylePriority.UsePadding = false;
            this.xrTableCell293.Text = "xrTableCell293";
            this.xrTableCell293.Weight = 5.7546867328219946;
            // 
            // xrTableRow202
            // 
            this.xrTableRow202.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell224,
            this.xrTableCell225});
            this.xrTableRow202.Name = "xrTableRow202";
            this.xrTableRow202.Weight = 0.90909090909090928;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell224.BorderColor = System.Drawing.Color.White;
            this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell224.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell224.StylePriority.UseBackColor = false;
            this.xrTableCell224.StylePriority.UseBorderColor = false;
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.StylePriority.UseFont = false;
            this.xrTableCell224.StylePriority.UsePadding = false;
            this.xrTableCell224.Text = "% Share";
            this.xrTableCell224.Weight = 2.1750001525878906;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.BackColor = System.Drawing.Color.White;
            this.xrTableCell225.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell225.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.MemberControlPerc")});
            this.xrTableCell225.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell225.StylePriority.UseBackColor = false;
            this.xrTableCell225.StylePriority.UseBorderColor = false;
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.StylePriority.UseFont = false;
            this.xrTableCell225.StylePriority.UsePadding = false;
            this.xrTableCell225.Text = "xrTableCell225";
            this.xrTableCell225.Weight = 5.7546867328219946;
            // 
            // xrTableRow147
            // 
            this.xrTableRow147.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell294,
            this.xrTableCell299});
            this.xrTableRow147.Name = "xrTableRow147";
            this.xrTableRow147.Weight = 0.90909090909090917;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell294.BorderColor = System.Drawing.Color.White;
            this.xrTableCell294.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell294.StylePriority.UseBackColor = false;
            this.xrTableCell294.StylePriority.UseBorderColor = false;
            this.xrTableCell294.StylePriority.UseFont = false;
            this.xrTableCell294.StylePriority.UsePadding = false;
            this.xrTableCell294.Text = "Contribution Value";
            this.xrTableCell294.Weight = 2.1750001525878906;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.BackColor = System.Drawing.Color.White;
            this.xrTableCell299.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell299.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.MemberSize")});
            this.xrTableCell299.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell299.StylePriority.UseBackColor = false;
            this.xrTableCell299.StylePriority.UseBorderColor = false;
            this.xrTableCell299.StylePriority.UseFont = false;
            this.xrTableCell299.StylePriority.UsePadding = false;
            this.xrTableCell299.Text = "xrTableCell299";
            this.xrTableCell299.Weight = 5.7546867328219946;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.White;
            this.xrLabel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel3.BorderWidth = 2;
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.IDNo")});
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(440.3F, 8F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(357.21F, 27F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseBorderColor = false;
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseBorderWidth = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "of {0}";
            xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel3.Summary = xrSummary2;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.White;
            this.xrLabel2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel2.BorderWidth = 2;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.Name")});
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(8F, 38.17F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(786.9996F, 20.00002F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseBorderColor = false;
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseBorderWidth = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0}";
            xrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            this.xrLabel2.Summary = xrSummary3;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblDirectorRecPosition
            // 
            this.lblDirectorRecPosition.BackColor = System.Drawing.Color.White;
            this.lblDirectorRecPosition.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblDirectorRecPosition.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.lblDirectorRecPosition.BorderWidth = 2;
            this.lblDirectorRecPosition.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.IDNo")});
            this.lblDirectorRecPosition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblDirectorRecPosition.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblDirectorRecPosition.LocationFloat = new DevExpress.Utils.PointFloat(8F, 7.999992F);
            this.lblDirectorRecPosition.Name = "lblDirectorRecPosition";
            this.lblDirectorRecPosition.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDirectorRecPosition.SizeF = new System.Drawing.SizeF(432.29F, 27.17F);
            this.lblDirectorRecPosition.StylePriority.UseBackColor = false;
            this.lblDirectorRecPosition.StylePriority.UseBorderColor = false;
            this.lblDirectorRecPosition.StylePriority.UseBorders = false;
            this.lblDirectorRecPosition.StylePriority.UseBorderWidth = false;
            this.lblDirectorRecPosition.StylePriority.UseFont = false;
            this.lblDirectorRecPosition.StylePriority.UseForeColor = false;
            this.lblDirectorRecPosition.StylePriority.UsePadding = false;
            this.lblDirectorRecPosition.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "Active Principal - {0}";
            xrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.lblDirectorRecPosition.Summary = xrSummary4;
            this.lblDirectorRecPosition.Text = "lblDirectorRecPosition";
            this.lblDirectorRecPosition.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Expanded = false;
            this.GroupHeader5.HeightF = 113F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // DRSubInputDtls
            // 
            this.DRSubInputDtls.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DSubInputDtls});
            this.DRSubInputDtls.DataMember = "SubscriberInputDetails";
            this.DRSubInputDtls.Level = 1;
            this.DRSubInputDtls.Name = "DRSubInputDtls";
            // 
            // DSubInputDtls
            // 
            this.DSubInputDtls.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable36,
            this.xrTable17,
            this.xrTable4,
            this.xrTable15});
            this.DSubInputDtls.HeightF = 301F;
            this.DSubInputDtls.Name = "DSubInputDtls";
            // 
            // xrTable36
            // 
            this.xrTable36.BorderColor = System.Drawing.Color.White;
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 183.7917F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow95,
            this.xrTableRow96,
            this.xrTableRow97,
            this.xrTableRow98,
            this.xrTableRow99});
            this.xrTable36.SizeF = new System.Drawing.SizeF(790F, 100F);
            this.xrTable36.StylePriority.UseBorderColor = false;
            this.xrTable36.StylePriority.UseBorders = false;
            this.xrTable36.StylePriority.UseFont = false;
            this.xrTable36.StylePriority.UsePadding = false;
            this.xrTable36.StylePriority.UseTextAlignment = false;
            this.xrTable36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow95
            // 
            this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell268,
            this.xrTableCell269,
            this.xrTableCell270,
            this.xrTableCell271});
            this.xrTableRow95.Name = "xrTableRow95";
            this.xrTableRow95.Weight = 0.8;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell268.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell268.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBackColor = false;
            this.xrTableCell268.StylePriority.UseFont = false;
            this.xrTableCell268.StylePriority.UseForeColor = false;
            this.xrTableCell268.Text = "Country";
            this.xrTableCell268.Weight = 0.59755134281200639;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.BackColor = System.Drawing.Color.White;
            this.xrTableCell269.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell269.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.Country")});
            this.xrTableCell269.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell269.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseBackColor = false;
            this.xrTableCell269.StylePriority.UseBorderColor = false;
            this.xrTableCell269.StylePriority.UseBorders = false;
            this.xrTableCell269.StylePriority.UseFont = false;
            this.xrTableCell269.StylePriority.UseForeColor = false;
            this.xrTableCell269.Text = "xrTableCell269";
            this.xrTableCell269.Weight = 0.59777125520608931;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell270.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell270.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBackColor = false;
            this.xrTableCell270.StylePriority.UseFont = false;
            this.xrTableCell270.StylePriority.UseForeColor = false;
            this.xrTableCell270.Text = "Legal Entity Type";
            this.xrTableCell270.Weight = 0.59777125520608931;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.BackColor = System.Drawing.Color.White;
            this.xrTableCell271.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell271.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell271.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.LegalEntity")});
            this.xrTableCell271.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell271.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBackColor = false;
            this.xrTableCell271.StylePriority.UseBorderColor = false;
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.StylePriority.UseFont = false;
            this.xrTableCell271.StylePriority.UseForeColor = false;
            this.xrTableCell271.Text = "xrTableCell271";
            this.xrTableCell271.Weight = 1.1955425104121786;
            // 
            // xrTableRow96
            // 
            this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell272,
            this.xrTableCell273});
            this.xrTableRow96.Name = "xrTableRow96";
            this.xrTableRow96.Weight = 0.8;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell272.BorderColor = System.Drawing.Color.White;
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell272.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell272.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell272.StylePriority.UseBackColor = false;
            this.xrTableCell272.StylePriority.UseBorderColor = false;
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.StylePriority.UseFont = false;
            this.xrTableCell272.StylePriority.UseForeColor = false;
            this.xrTableCell272.StylePriority.UsePadding = false;
            this.xrTableCell272.Text = "Business Name";
            this.xrTableCell272.Weight = 0.59755134281200639;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.BackColor = System.Drawing.Color.White;
            this.xrTableCell273.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell273.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell273.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.BusinessName")});
            this.xrTableCell273.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell273.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBackColor = false;
            this.xrTableCell273.StylePriority.UseBorderColor = false;
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.StylePriority.UseFont = false;
            this.xrTableCell273.StylePriority.UseForeColor = false;
            this.xrTableCell273.Text = "xrTableCell273";
            this.xrTableCell273.Weight = 2.3910850208243573;
            // 
            // xrTableRow97
            // 
            this.xrTableRow97.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell274,
            this.xrTableCell275,
            this.xrTableCell276,
            this.xrTableCell277});
            this.xrTableRow97.Name = "xrTableRow97";
            this.xrTableRow97.Weight = 0.79999999999999993;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell274.BorderColor = System.Drawing.Color.White;
            this.xrTableCell274.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell274.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell274.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell274.StylePriority.UseBackColor = false;
            this.xrTableCell274.StylePriority.UseBorderColor = false;
            this.xrTableCell274.StylePriority.UseBorders = false;
            this.xrTableCell274.StylePriority.UseFont = false;
            this.xrTableCell274.StylePriority.UseForeColor = false;
            this.xrTableCell274.StylePriority.UsePadding = false;
            this.xrTableCell274.Text = "Registration Number";
            this.xrTableCell274.Weight = 0.59755134281200639;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.BackColor = System.Drawing.Color.White;
            this.xrTableCell275.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell275.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.RegistrationNumber")});
            this.xrTableCell275.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell275.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBackColor = false;
            this.xrTableCell275.StylePriority.UseBorderColor = false;
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.StylePriority.UseFont = false;
            this.xrTableCell275.StylePriority.UseForeColor = false;
            this.xrTableCell275.Text = "xrTableCell275";
            this.xrTableCell275.Weight = 0.59777125520608931;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell276.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell276.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.StylePriority.UseBackColor = false;
            this.xrTableCell276.StylePriority.UseFont = false;
            this.xrTableCell276.StylePriority.UseForeColor = false;
            this.xrTableCell276.Text = "Vat Number";
            this.xrTableCell276.Weight = 0.59777125520608931;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.BackColor = System.Drawing.Color.White;
            this.xrTableCell277.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell277.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.VatNumber")});
            this.xrTableCell277.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell277.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell277.StylePriority.UseBackColor = false;
            this.xrTableCell277.StylePriority.UseBorderColor = false;
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.StylePriority.UseFont = false;
            this.xrTableCell277.StylePriority.UseForeColor = false;
            this.xrTableCell277.StylePriority.UsePadding = false;
            this.xrTableCell277.Text = "xrTableCell277";
            this.xrTableCell277.Weight = 1.1955425104121786;
            // 
            // xrTableRow98
            // 
            this.xrTableRow98.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell278,
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTableCell281});
            this.xrTableRow98.Name = "xrTableRow98";
            this.xrTableRow98.Weight = 0.79999999999999993;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell278.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell278.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBackColor = false;
            this.xrTableCell278.StylePriority.UseFont = false;
            this.xrTableCell278.StylePriority.UseForeColor = false;
            this.xrTableCell278.Text = "ID Number";
            this.xrTableCell278.Weight = 0.59755134281200639;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.BackColor = System.Drawing.Color.White;
            this.xrTableCell279.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell279.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell279.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.IDNumber")});
            this.xrTableCell279.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell279.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBackColor = false;
            this.xrTableCell279.StylePriority.UseBorderColor = false;
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.StylePriority.UseFont = false;
            this.xrTableCell279.StylePriority.UseForeColor = false;
            this.xrTableCell279.Text = "xrTableCell279";
            this.xrTableCell279.Weight = 0.59777125520608931;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell280.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell280.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell280.Multiline = true;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBackColor = false;
            this.xrTableCell280.StylePriority.UseFont = false;
            this.xrTableCell280.StylePriority.UseForeColor = false;
            this.xrTableCell280.Text = "NPO Number\r\n";
            this.xrTableCell280.Weight = 0.59777125520608931;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.BackColor = System.Drawing.Color.White;
            this.xrTableCell281.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell281.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.NPONumber")});
            this.xrTableCell281.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell281.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBackColor = false;
            this.xrTableCell281.StylePriority.UseBorderColor = false;
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.StylePriority.UseFont = false;
            this.xrTableCell281.StylePriority.UseForeColor = false;
            this.xrTableCell281.Text = "xrTableCell281";
            this.xrTableCell281.Weight = 1.1955425104121786;
            // 
            // xrTableRow99
            // 
            this.xrTableRow99.BorderWidth = 0;
            this.xrTableRow99.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell282,
            this.xrTableCell283});
            this.xrTableRow99.Name = "xrTableRow99";
            this.xrTableRow99.StylePriority.UseBorderWidth = false;
            this.xrTableRow99.Weight = 0.79999999999999993;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell282.BorderWidth = 1;
            this.xrTableCell282.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell282.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseBackColor = false;
            this.xrTableCell282.StylePriority.UseBorderWidth = false;
            this.xrTableCell282.StylePriority.UseFont = false;
            this.xrTableCell282.StylePriority.UseForeColor = false;
            this.xrTableCell282.Text = "Trust Number\t";
            this.xrTableCell282.Weight = 0.59755134281200639;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.BackColor = System.Drawing.Color.White;
            this.xrTableCell283.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.TrustNumber")});
            this.xrTableCell283.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell283.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseBackColor = false;
            this.xrTableCell283.StylePriority.UseFont = false;
            this.xrTableCell283.StylePriority.UseForeColor = false;
            this.xrTableCell283.Text = "xrTableCell283";
            this.xrTableCell283.Weight = 2.3910850208243573;
            // 
            // xrTable17
            // 
            this.xrTable17.ForeColor = System.Drawing.Color.Black;
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(13.20005F, 158.7917F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable17.SizeF = new System.Drawing.SizeF(790F, 25F);
            this.xrTable17.StylePriority.UseForeColor = false;
            this.xrTable17.StylePriority.UsePadding = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell267.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell267.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell267.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell267.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBackColor = false;
            this.xrTableCell267.StylePriority.UseBorderColor = false;
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.StylePriority.UseFont = false;
            this.xrTableCell267.StylePriority.UseForeColor = false;
            this.xrTableCell267.StylePriority.UseTextAlignment = false;
            this.xrTableCell267.Text = "Input Details";
            this.xrTableCell267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell267.Weight = 3;
            // 
            // xrTable4
            // 
            this.xrTable4.BorderColor = System.Drawing.Color.White;
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(13.20012F, 64.58334F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12,
            this.xrTableRow227,
            this.xrTableRow13,
            this.xrTableRow27});
            this.xrTable4.SizeF = new System.Drawing.SizeF(790F, 80F);
            this.xrTable4.StylePriority.UseBorderColor = false;
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UsePadding = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell22,
            this.lblEnquiryDateValue});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.8;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell18.BorderColor = System.Drawing.Color.White;
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell18.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseBorderColor = false;
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseForeColor = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.Text = "XDS Reference No.";
            this.xrTableCell18.Weight = 0.59755134281200639;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.White;
            this.xrTableCell19.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.Reference")});
            this.xrTableCell19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorderColor = false;
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.Text = "xrTableCell19";
            this.xrTableCell19.Weight = 0.59777125520608931;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell22.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBackColor = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseForeColor = false;
            this.xrTableCell22.Text = "Enquiry Date";
            this.xrTableCell22.Weight = 0.59777125520608931;
            // 
            // lblEnquiryDateValue
            // 
            this.lblEnquiryDateValue.BackColor = System.Drawing.Color.White;
            this.lblEnquiryDateValue.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblEnquiryDateValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblEnquiryDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryDate")});
            this.lblEnquiryDateValue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnquiryDateValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblEnquiryDateValue.Name = "lblEnquiryDateValue";
            this.lblEnquiryDateValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEnquiryDateValue.StylePriority.UseBackColor = false;
            this.lblEnquiryDateValue.StylePriority.UseBorderColor = false;
            this.lblEnquiryDateValue.StylePriority.UseBorders = false;
            this.lblEnquiryDateValue.StylePriority.UseFont = false;
            this.lblEnquiryDateValue.StylePriority.UseForeColor = false;
            this.lblEnquiryDateValue.StylePriority.UsePadding = false;
            this.lblEnquiryDateValue.Text = "lblEnquiryDateValue";
            this.lblEnquiryDateValue.Weight = 1.1955425104121786;
            // 
            // xrTableRow227
            // 
            this.xrTableRow227.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell14,
            this.xrTableCell244,
            this.xrTableCell404});
            this.xrTableRow227.Name = "xrTableRow227";
            this.xrTableRow227.Weight = 0.8;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBackColor = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Product";
            this.xrTableCell4.Weight = 0.59755134281200639;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.White;
            this.xrTableCell14.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryType")});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseBorderColor = false;
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.Weight = 0.59777125520608931;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell244.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBackColor = false;
            this.xrTableCell244.StylePriority.UseFont = false;
            this.xrTableCell244.Text = "Prebuilt Package";
            this.xrTableCell244.Weight = 0.59777125520608931;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.BackColor = System.Drawing.Color.White;
            this.xrTableCell404.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell404.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell404.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.PrebuiltPackage")});
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseBackColor = false;
            this.xrTableCell404.StylePriority.UseBorderColor = false;
            this.xrTableCell404.StylePriority.UseBorders = false;
            this.xrTableCell404.Text = "xrTableCell404";
            this.xrTableCell404.Weight = 1.1955425104121786;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell26,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.79999999999999993;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell23.BorderColor = System.Drawing.Color.White;
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell23.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseForeColor = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.Text = "Subscriber Name";
            this.xrTableCell23.Weight = 0.59755134281200639;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.White;
            this.xrTableCell26.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberName")});
            this.xrTableCell26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell26.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseBorderColor = false;
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseForeColor = false;
            this.xrTableCell26.Text = "xrTableCell89";
            this.xrTableCell26.Weight = 0.59777125520608931;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell261.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell261.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBackColor = false;
            this.xrTableCell261.StylePriority.UseFont = false;
            this.xrTableCell261.StylePriority.UseForeColor = false;
            this.xrTableCell261.Text = "Subscriber User Name";
            this.xrTableCell261.Weight = 0.59777125520608931;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.BackColor = System.Drawing.Color.White;
            this.xrTableCell262.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell262.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberUserName")});
            this.xrTableCell262.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell262.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell262.StylePriority.UseBackColor = false;
            this.xrTableCell262.StylePriority.UseBorderColor = false;
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.StylePriority.UseFont = false;
            this.xrTableCell262.StylePriority.UseForeColor = false;
            this.xrTableCell262.StylePriority.UsePadding = false;
            this.xrTableCell262.Text = "xrTableCell14";
            this.xrTableCell262.Weight = 1.1955425104121786;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265,
            this.xrTableCell266});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.79999999999999993;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell263.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell263.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBackColor = false;
            this.xrTableCell263.StylePriority.UseFont = false;
            this.xrTableCell263.StylePriority.UseForeColor = false;
            this.xrTableCell263.Text = "Voucher Code";
            this.xrTableCell263.Weight = 0.59755134281200639;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.BackColor = System.Drawing.Color.White;
            this.xrTableCell264.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell264.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell264.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.VoucherCode")});
            this.xrTableCell264.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell264.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBackColor = false;
            this.xrTableCell264.StylePriority.UseBorderColor = false;
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.StylePriority.UseFont = false;
            this.xrTableCell264.StylePriority.UseForeColor = false;
            this.xrTableCell264.Text = "xrTableCell264";
            this.xrTableCell264.Weight = 0.59777125520608931;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell265.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell265.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBackColor = false;
            this.xrTableCell265.StylePriority.UseFont = false;
            this.xrTableCell265.StylePriority.UseForeColor = false;
            this.xrTableCell265.Text = "External Reference";
            this.xrTableCell265.Weight = 0.59777125520608931;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.BackColor = System.Drawing.Color.White;
            this.xrTableCell266.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell266.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.ExternalReference")});
            this.xrTableCell266.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell266.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBackColor = false;
            this.xrTableCell266.StylePriority.UseBorderColor = false;
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.StylePriority.UseFont = false;
            this.xrTableCell266.StylePriority.UseForeColor = false;
            this.xrTableCell266.Text = "xrTableCell266";
            this.xrTableCell266.Weight = 1.1955425104121786;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow162});
            this.xrTable15.SizeF = new System.Drawing.SizeF(792.0001F, 40.625F);
            this.xrTable15.StylePriority.UseBorders = false;
            this.xrTable15.StylePriority.UseFont = false;
            this.xrTable15.StylePriority.UseForeColor = false;
            this.xrTable15.StylePriority.UsePadding = false;
            this.xrTable15.StylePriority.UseTextAlignment = false;
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow162
            // 
            this.xrTableRow162.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144,
            this.xrTableCell145});
            this.xrTableRow162.Name = "xrTableRow162";
            this.xrTableRow162.Weight = 0.8;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.BackColor = System.Drawing.Color.White;
            this.xrTableCell144.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell144.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell144.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell144.StylePriority.UseBackColor = false;
            this.xrTableCell144.StylePriority.UseBorderColor = false;
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseFont = false;
            this.xrTableCell144.StylePriority.UseForeColor = false;
            this.xrTableCell144.StylePriority.UsePadding = false;
            this.xrTableCell144.Text = "Enquiry Input Details";
            this.xrTableCell144.Weight = 1.1756499500551834;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.BackColor = System.Drawing.Color.White;
            this.xrTableCell145.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell145.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell145.BorderWidth = 1;
            this.xrTableCell145.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell145.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell145.StylePriority.UseBackColor = false;
            this.xrTableCell145.StylePriority.UseBorderColor = false;
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.StylePriority.UseBorderWidth = false;
            this.xrTableCell145.StylePriority.UseFont = false;
            this.xrTableCell145.StylePriority.UseForeColor = false;
            this.xrTableCell145.StylePriority.UsePadding = false;
            this.xrTableCell145.Weight = 1.8243500499448164;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo4,
            this.xrLabel1,
            this.xrPageInfo3});
            this.PageFooter.Expanded = false;
            this.PageFooter.HeightF = 25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo4
            // 
            this.xrPageInfo4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo4.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo4.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo4.LocationFloat = new DevExpress.Utils.PointFloat(750F, 0F);
            this.xrPageInfo4.Name = "xrPageInfo4";
            this.xrPageInfo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo4.SizeF = new System.Drawing.SizeF(54.49994F, 25F);
            this.xrPageInfo4.StylePriority.UseBackColor = false;
            this.xrPageInfo4.StylePriority.UseBorderColor = false;
            this.xrPageInfo4.StylePriority.UseBorders = false;
            this.xrPageInfo4.StylePriority.UseFont = false;
            this.xrPageInfo4.StylePriority.UseForeColor = false;
            this.xrPageInfo4.StylePriority.UseTextAlignment = false;
            this.xrPageInfo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel1.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.DimGray;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(707.0834F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(42.91663F, 25F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Page:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo3.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo3.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo3.Format = "Printed Date: {0:dddd, dd MMMM yyyy H:mm}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(699.0834F, 25F);
            this.xrPageInfo3.StylePriority.UseBackColor = false;
            this.xrPageInfo3.StylePriority.UseBorderColor = false;
            this.xrPageInfo3.StylePriority.UseBorders = false;
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.StylePriority.UseForeColor = false;
            this.xrPageInfo3.StylePriority.UsePadding = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DRCommPosJudg
            // 
            this.DRCommPosJudg.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommPosJudg,
            this.GroupHeader11});
            this.DRCommPosJudg.DataMember = "CommercialPossibleJudgment";
            this.DRCommPosJudg.Level = 8;
            this.DRCommPosJudg.Name = "DRCommPosJudg";
            // 
            // DCommPosJudg
            // 
            this.DCommPosJudg.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable69});
            this.DCommPosJudg.HeightF = 20F;
            this.DCommPosJudg.Name = "DCommPosJudg";
            // 
            // xrTable69
            // 
            this.xrTable69.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable69.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.xrTable69.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable69.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.xrTable69.Name = "xrTable69";
            this.xrTable69.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow60});
            this.xrTable69.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.xrTable69.StylePriority.UseBackColor = false;
            this.xrTable69.StylePriority.UseBorderColor = false;
            this.xrTable69.StylePriority.UseBorders = false;
            this.xrTable69.StylePriority.UseFont = false;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.BackColor = System.Drawing.Color.White;
            this.xrTableRow60.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell530,
            this.xrTableCell531,
            this.xrTableCell532,
            this.lblposjudgAmountD,
            this.xrTableCell534,
            this.xrTableCell535,
            this.xrTableCell536,
            this.xrTableCell537,
            this.xrTableCell538,
            this.xrTableCell364});
            this.xrTableRow60.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow60.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableRow60.StylePriority.UseBackColor = false;
            this.xrTableRow60.StylePriority.UseBorderColor = false;
            this.xrTableRow60.StylePriority.UseBorders = false;
            this.xrTableRow60.StylePriority.UseFont = false;
            this.xrTableRow60.StylePriority.UseForeColor = false;
            this.xrTableRow60.StylePriority.UsePadding = false;
            this.xrTableRow60.StylePriority.UseTextAlignment = false;
            this.xrTableRow60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow60.Weight = 0.8;
            // 
            // xrTableCell530
            // 
            this.xrTableCell530.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell530.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell530.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseNumber")});
            this.xrTableCell530.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell530.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell530.Name = "xrTableCell530";
            this.xrTableCell530.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell530.StylePriority.UseBorderColor = false;
            this.xrTableCell530.StylePriority.UseBorders = false;
            this.xrTableCell530.StylePriority.UseFont = false;
            this.xrTableCell530.StylePriority.UseForeColor = false;
            this.xrTableCell530.StylePriority.UsePadding = false;
            this.xrTableCell530.Text = "xrTableCell530";
            this.xrTableCell530.Weight = 0.2462122001799148;
            // 
            // xrTableCell531
            // 
            this.xrTableCell531.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell531.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell531.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseFilingDate")});
            this.xrTableCell531.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell531.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell531.Name = "xrTableCell531";
            this.xrTableCell531.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell531.StylePriority.UseBorderColor = false;
            this.xrTableCell531.StylePriority.UseBorders = false;
            this.xrTableCell531.StylePriority.UseFont = false;
            this.xrTableCell531.StylePriority.UseForeColor = false;
            this.xrTableCell531.StylePriority.UsePadding = false;
            this.xrTableCell531.Text = "xrTableCell531";
            this.xrTableCell531.Weight = 0.26769041095178375;
            // 
            // xrTableCell532
            // 
            this.xrTableCell532.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell532.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell532.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseType")});
            this.xrTableCell532.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell532.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell532.Name = "xrTableCell532";
            this.xrTableCell532.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell532.StylePriority.UseBorderColor = false;
            this.xrTableCell532.StylePriority.UseBorders = false;
            this.xrTableCell532.StylePriority.UseFont = false;
            this.xrTableCell532.StylePriority.UseForeColor = false;
            this.xrTableCell532.StylePriority.UsePadding = false;
            this.xrTableCell532.Text = "xrTableCell532";
            this.xrTableCell532.Weight = 0.31547512873906125;
            // 
            // lblposjudgAmountD
            // 
            this.lblposjudgAmountD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblposjudgAmountD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblposjudgAmountD.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.LblPosJDAmnt});
            this.lblposjudgAmountD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblposjudgAmountD.ForeColor = System.Drawing.Color.DimGray;
            this.lblposjudgAmountD.Name = "lblposjudgAmountD";
            this.lblposjudgAmountD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblposjudgAmountD.StylePriority.UseBorderColor = false;
            this.lblposjudgAmountD.StylePriority.UseBorders = false;
            this.lblposjudgAmountD.StylePriority.UseFont = false;
            this.lblposjudgAmountD.StylePriority.UseForeColor = false;
            this.lblposjudgAmountD.StylePriority.UsePadding = false;
            this.lblposjudgAmountD.Text = "lblJDAmount";
            this.lblposjudgAmountD.Weight = 0.25016788033103088;
            // 
            // LblPosJDAmnt
            // 
            this.LblPosJDAmnt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.DisputeAmt")});
            this.LblPosJDAmnt.LocationFloat = new DevExpress.Utils.PointFloat(2.765656E-05F, 0F);
            this.LblPosJDAmnt.Name = "LblPosJDAmnt";
            this.LblPosJDAmnt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.LblPosJDAmnt.SizeF = new System.Drawing.SizeF(65.87752F, 19.99995F);
            this.LblPosJDAmnt.Text = "LblPosJDAmnt";
            // 
            // xrTableCell534
            // 
            this.xrTableCell534.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell534.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell534.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.PlaintiffName")});
            this.xrTableCell534.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell534.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell534.Name = "xrTableCell534";
            this.xrTableCell534.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell534.StylePriority.UseBorderColor = false;
            this.xrTableCell534.StylePriority.UseBorders = false;
            this.xrTableCell534.StylePriority.UseFont = false;
            this.xrTableCell534.StylePriority.UseForeColor = false;
            this.xrTableCell534.StylePriority.UsePadding = false;
            this.xrTableCell534.Text = "xrTableCell534";
            this.xrTableCell534.Weight = 0.26515158120834637;
            // 
            // xrTableCell535
            // 
            this.xrTableCell535.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell535.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell535.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CourtName")});
            this.xrTableCell535.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell535.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell535.Name = "xrTableCell535";
            this.xrTableCell535.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell535.StylePriority.UseBorderColor = false;
            this.xrTableCell535.StylePriority.UseBorders = false;
            this.xrTableCell535.StylePriority.UseFont = false;
            this.xrTableCell535.StylePriority.UseForeColor = false;
            this.xrTableCell535.StylePriority.UsePadding = false;
            this.xrTableCell535.Text = "xrTableCell535";
            this.xrTableCell535.Weight = 0.26894531660448173;
            // 
            // xrTableCell536
            // 
            this.xrTableCell536.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell536.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell536.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.AttorneyName")});
            this.xrTableCell536.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell536.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell536.Name = "xrTableCell536";
            this.xrTableCell536.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell536.StylePriority.UseBorderColor = false;
            this.xrTableCell536.StylePriority.UseBorders = false;
            this.xrTableCell536.StylePriority.UseFont = false;
            this.xrTableCell536.StylePriority.UseForeColor = false;
            this.xrTableCell536.StylePriority.UsePadding = false;
            this.xrTableCell536.Text = "xrTableCell536";
            this.xrTableCell536.Weight = 0.33711552041335469;
            // 
            // xrTableCell537
            // 
            this.xrTableCell537.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell537.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell537.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.TelephoneNo")});
            this.xrTableCell537.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell537.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell537.Name = "xrTableCell537";
            this.xrTableCell537.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell537.StylePriority.UseBorderColor = false;
            this.xrTableCell537.StylePriority.UseBorders = false;
            this.xrTableCell537.StylePriority.UseFont = false;
            this.xrTableCell537.StylePriority.UseForeColor = false;
            this.xrTableCell537.StylePriority.UsePadding = false;
            this.xrTableCell537.Text = "xrTableCell537";
            this.xrTableCell537.Weight = 0.32196968350341748;
            // 
            // xrTableCell538
            // 
            this.xrTableCell538.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell538.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell538.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CommercialName")});
            this.xrTableCell538.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell538.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell538.Name = "xrTableCell538";
            this.xrTableCell538.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell538.StylePriority.UseBorderColor = false;
            this.xrTableCell538.StylePriority.UseBorders = false;
            this.xrTableCell538.StylePriority.UseFont = false;
            this.xrTableCell538.StylePriority.UseForeColor = false;
            this.xrTableCell538.StylePriority.UsePadding = false;
            this.xrTableCell538.Text = "xrTableCell538";
            this.xrTableCell538.Weight = 0.45454502586971418;
            // 
            // xrTableCell364
            // 
            this.xrTableCell364.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell364.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell364.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseReason")});
            this.xrTableCell364.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell364.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell364.Name = "xrTableCell364";
            this.xrTableCell364.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell364.StylePriority.UseBorderColor = false;
            this.xrTableCell364.StylePriority.UseBorders = false;
            this.xrTableCell364.StylePriority.UseFont = false;
            this.xrTableCell364.StylePriority.UseForeColor = false;
            this.xrTableCell364.StylePriority.UsePadding = false;
            this.xrTableCell364.Text = "xrTableCell364";
            this.xrTableCell364.Weight = 0.27272771458644229;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tbPosslCJud,
            this.tblposJudgmentsH});
            this.GroupHeader11.HeightF = 52F;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // tbPosslCJud
            // 
            this.tbPosslCJud.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbPosslCJud.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tbPosslCJud.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 32F);
            this.tbPosslCJud.Name = "tbPosslCJud";
            this.tbPosslCJud.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59});
            this.tbPosslCJud.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tbPosslCJud.StylePriority.UseBorderColor = false;
            this.tbPosslCJud.StylePriority.UseBorders = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow59.BorderColor = System.Drawing.Color.White;
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell302,
            this.xrTableCell360,
            this.xrTableCell361,
            this.xrTableCell366,
            this.xrTableCell367,
            this.xrTableCell368,
            this.xrTableCell369,
            this.xrTableCell372,
            this.xrTableCell373});
            this.xrTableRow59.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow59.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow59.StylePriority.UseBackColor = false;
            this.xrTableRow59.StylePriority.UseBorderColor = false;
            this.xrTableRow59.StylePriority.UseFont = false;
            this.xrTableRow59.StylePriority.UseForeColor = false;
            this.xrTableRow59.StylePriority.UsePadding = false;
            this.xrTableRow59.StylePriority.UseTextAlignment = false;
            this.xrTableRow59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow59.Weight = 0.8;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Case No.";
            this.xrTableCell5.Weight = 0.24621212814793442;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseFont = false;
            this.xrTableCell302.Text = "Issue date";
            this.xrTableCell302.Weight = 0.26515152208732851;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseFont = false;
            this.xrTableCell360.Text = "Judgment Type";
            this.xrTableCell360.Weight = 0.32196970390551016;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell361.StylePriority.UseFont = false;
            this.xrTableCell361.StylePriority.UsePadding = false;
            this.xrTableCell361.Text = "Amount";
            this.xrTableCell361.Weight = 0.24621212814793445;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.StylePriority.UseFont = false;
            this.xrTableCell366.Text = "Plaintiff Name";
            this.xrTableCell366.Weight = 0.26515151515151508;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseFont = false;
            this.xrTableCell367.Text = "Court Name";
            this.xrTableCell367.Weight = 0.28409090909090912;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseFont = false;
            this.xrTableCell368.Text = "Attorney Name";
            this.xrTableCell368.Weight = 0.32196969754768134;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseFont = false;
            this.xrTableCell369.Text = "Attorney Phone No";
            this.xrTableCell369.Weight = 0.32196966835946766;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.StylePriority.UseFont = false;
            this.xrTableCell372.Text = "Commercial Name";
            this.xrTableCell372.Weight = 0.45454545468995067;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell373.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.StylePriority.UseBorders = false;
            this.xrTableCell373.StylePriority.UseFont = false;
            this.xrTableCell373.Text = "Case reason";
            this.xrTableCell373.Weight = 0.27272727287176884;
            // 
            // tblposJudgmentsH
            // 
            this.tblposJudgmentsH.ForeColor = System.Drawing.Color.Black;
            this.tblposJudgmentsH.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 7F);
            this.tblposJudgmentsH.Name = "tblposJudgmentsH";
            this.tblposJudgmentsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow65});
            this.tblposJudgmentsH.SizeF = new System.Drawing.SizeF(790F, 25F);
            this.tblposJudgmentsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblposJudgmentsH});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1;
            // 
            // lblposJudgmentsH
            // 
            this.lblposJudgmentsH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblposJudgmentsH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblposJudgmentsH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblposJudgmentsH.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblposJudgmentsH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblposJudgmentsH.Name = "lblposJudgmentsH";
            this.lblposJudgmentsH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblposJudgmentsH.StylePriority.UseBackColor = false;
            this.lblposJudgmentsH.StylePriority.UseBorderColor = false;
            this.lblposJudgmentsH.StylePriority.UseBorders = false;
            this.lblposJudgmentsH.StylePriority.UseFont = false;
            this.lblposJudgmentsH.StylePriority.UseForeColor = false;
            this.lblposJudgmentsH.StylePriority.UsePadding = false;
            this.lblposJudgmentsH.StylePriority.UseTextAlignment = false;
            this.lblposJudgmentsH.Text = "Possible Judgments";
            this.lblposJudgmentsH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblposJudgmentsH.Weight = 3;
            // 
            // DRCommBranchInfo
            // 
            this.DRCommBranchInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommBranchInfo,
            this.GroupHeader8});
            this.DRCommBranchInfo.Level = 11;
            this.DRCommBranchInfo.Name = "DRCommBranchInfo";
            this.DRCommBranchInfo.Visible = false;
            // 
            // DCommBranchInfo
            // 
            this.DCommBranchInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblBranchInfoD});
            this.DCommBranchInfo.Expanded = false;
            this.DCommBranchInfo.HeightF = 20F;
            this.DCommBranchInfo.Name = "DCommBranchInfo";
            // 
            // tblBranchInfoD
            // 
            this.tblBranchInfoD.BackColor = System.Drawing.Color.White;
            this.tblBranchInfoD.BorderColor = System.Drawing.Color.Gainsboro;
            this.tblBranchInfoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblBranchInfoD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblBranchInfoD.ForeColor = System.Drawing.Color.DimGray;
            this.tblBranchInfoD.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.tblBranchInfoD.Name = "tblBranchInfoD";
            this.tblBranchInfoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblBranchInfoD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73});
            this.tblBranchInfoD.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblBranchInfoD.StylePriority.UseBackColor = false;
            this.tblBranchInfoD.StylePriority.UseBorderColor = false;
            this.tblBranchInfoD.StylePriority.UseBorders = false;
            this.tblBranchInfoD.StylePriority.UseFont = false;
            this.tblBranchInfoD.StylePriority.UseForeColor = false;
            this.tblBranchInfoD.StylePriority.UsePadding = false;
            this.tblBranchInfoD.StylePriority.UseTextAlignment = false;
            this.tblBranchInfoD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBranchNameD,
            this.LblCoutryCD,
            this.LblProvinceD});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 0.8;
            // 
            // lblBranchNameD
            // 
            this.lblBranchNameD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblBranchNameD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBranchNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBranchInformation.BranchName")});
            this.lblBranchNameD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblBranchNameD.Name = "lblBranchNameD";
            this.lblBranchNameD.StylePriority.UseBorderColor = false;
            this.lblBranchNameD.StylePriority.UseBorders = false;
            this.lblBranchNameD.StylePriority.UseFont = false;
            this.lblBranchNameD.Text = "lblBranchNameD";
            this.lblBranchNameD.Weight = 1.0726720654111333;
            // 
            // LblCoutryCD
            // 
            this.LblCoutryCD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.LblCoutryCD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblCoutryCD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBranchInformation.Country")});
            this.LblCoutryCD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.LblCoutryCD.Name = "LblCoutryCD";
            this.LblCoutryCD.StylePriority.UseBorderColor = false;
            this.LblCoutryCD.StylePriority.UseBorders = false;
            this.LblCoutryCD.StylePriority.UseFont = false;
            this.LblCoutryCD.Text = "LblCoutryCD";
            this.LblCoutryCD.Weight = 1.0462325315372787;
            // 
            // LblProvinceD
            // 
            this.LblProvinceD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.LblProvinceD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblProvinceD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBranchInformation.Province")});
            this.LblProvinceD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.LblProvinceD.Multiline = true;
            this.LblProvinceD.Name = "LblProvinceD";
            this.LblProvinceD.StylePriority.UseBorderColor = false;
            this.LblProvinceD.StylePriority.UseBorders = false;
            this.LblProvinceD.StylePriority.UseFont = false;
            this.LblProvinceD.Text = "LblProvinceD";
            this.LblProvinceD.Weight = 0.88109540305158773;
            // 
            // GroupHeader8
            // 
            this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable21,
            this.tblBranchInfoC});
            this.GroupHeader8.Expanded = false;
            this.GroupHeader8.HeightF = 84F;
            this.GroupHeader8.Name = "GroupHeader8";
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable21.SizeF = new System.Drawing.SizeF(790F, 40.62F);
            this.xrTable21.StylePriority.UseBorders = false;
            this.xrTable21.StylePriority.UseFont = false;
            this.xrTable21.StylePriority.UseForeColor = false;
            this.xrTable21.StylePriority.UsePadding = false;
            this.xrTable21.StylePriority.UseTextAlignment = false;
            this.xrTable21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBranchH,
            this.xrTableCell40});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.8;
            // 
            // lblBranchH
            // 
            this.lblBranchH.BackColor = System.Drawing.Color.White;
            this.lblBranchH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblBranchH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblBranchH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblBranchH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblBranchH.Name = "lblBranchH";
            this.lblBranchH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblBranchH.StylePriority.UseBackColor = false;
            this.lblBranchH.StylePriority.UseBorderColor = false;
            this.lblBranchH.StylePriority.UseBorders = false;
            this.lblBranchH.StylePriority.UseFont = false;
            this.lblBranchH.StylePriority.UseForeColor = false;
            this.lblBranchH.StylePriority.UsePadding = false;
            this.lblBranchH.Text = "Company Branch Information";
            this.lblBranchH.Weight = 1.1756499500551834;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.BackColor = System.Drawing.Color.White;
            this.xrTableCell40.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell40.BorderWidth = 1;
            this.xrTableCell40.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell40.StylePriority.UseBackColor = false;
            this.xrTableCell40.StylePriority.UseBorderColor = false;
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.StylePriority.UseBorderWidth = false;
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UseForeColor = false;
            this.xrTableCell40.StylePriority.UsePadding = false;
            this.xrTableCell40.Weight = 1.8243500499448164;
            // 
            // tblBranchInfoC
            // 
            this.tblBranchInfoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblBranchInfoC.BorderColor = System.Drawing.Color.White;
            this.tblBranchInfoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblBranchInfoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblBranchInfoC.ForeColor = System.Drawing.Color.Gray;
            this.tblBranchInfoC.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 64F);
            this.tblBranchInfoC.Name = "tblBranchInfoC";
            this.tblBranchInfoC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblBranchInfoC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.tblBranchInfoC.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblBranchInfoC.StylePriority.UseBackColor = false;
            this.tblBranchInfoC.StylePriority.UseBorderColor = false;
            this.tblBranchInfoC.StylePriority.UseBorders = false;
            this.tblBranchInfoC.StylePriority.UseFont = false;
            this.tblBranchInfoC.StylePriority.UseForeColor = false;
            this.tblBranchInfoC.StylePriority.UsePadding = false;
            this.tblBranchInfoC.StylePriority.UseTextAlignment = false;
            this.tblBranchInfoC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblBrachNameC,
            this.LblCountry,
            this.LblProvince});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 0.8;
            // 
            // LblBrachNameC
            // 
            this.LblBrachNameC.Name = "LblBrachNameC";
            this.LblBrachNameC.Text = "Branch Name";
            this.LblBrachNameC.Weight = 1.0726718721395807;
            // 
            // LblCountry
            // 
            this.LblCountry.Name = "LblCountry";
            this.LblCountry.Text = "Country";
            this.LblCountry.Weight = 1.0462327682949304;
            // 
            // LblProvince
            // 
            this.LblProvince.Name = "LblProvince";
            this.LblProvince.Text = "Province";
            this.LblProvince.Weight = 0.88109535956548846;
            // 
            // DRCommDivisionInfo
            // 
            this.DRCommDivisionInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DCommDivisionInfo,
            this.GroupHeader12});
            this.DRCommDivisionInfo.Level = 10;
            this.DRCommDivisionInfo.Name = "DRCommDivisionInfo";
            this.DRCommDivisionInfo.Visible = false;
            // 
            // DCommDivisionInfo
            // 
            this.DCommDivisionInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDivisonD});
            this.DCommDivisionInfo.Expanded = false;
            this.DCommDivisionInfo.HeightF = 20F;
            this.DCommDivisionInfo.Name = "DCommDivisionInfo";
            // 
            // tblDivisonD
            // 
            this.tblDivisonD.BackColor = System.Drawing.Color.White;
            this.tblDivisonD.BorderColor = System.Drawing.Color.Gainsboro;
            this.tblDivisonD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblDivisonD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDivisonD.ForeColor = System.Drawing.Color.DimGray;
            this.tblDivisonD.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.tblDivisonD.Name = "tblDivisonD";
            this.tblDivisonD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDivisonD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow76});
            this.tblDivisonD.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblDivisonD.StylePriority.UseBackColor = false;
            this.tblDivisonD.StylePriority.UseBorderColor = false;
            this.tblDivisonD.StylePriority.UseBorders = false;
            this.tblDivisonD.StylePriority.UseFont = false;
            this.tblDivisonD.StylePriority.UseForeColor = false;
            this.tblDivisonD.StylePriority.UsePadding = false;
            this.tblDivisonD.StylePriority.UseTextAlignment = false;
            this.tblDivisonD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDivisionCodeD,
            this.lblDivisionNameD});
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.Weight = 0.8;
            // 
            // lblDivisionCodeD
            // 
            this.lblDivisionCodeD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDivisionCodeD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDivisionCodeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDivisionInformation.DivisionCode")});
            this.lblDivisionCodeD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDivisionCodeD.Name = "lblDivisionCodeD";
            this.lblDivisionCodeD.StylePriority.UseBorderColor = false;
            this.lblDivisionCodeD.StylePriority.UseBorders = false;
            this.lblDivisionCodeD.StylePriority.UseFont = false;
            this.lblDivisionCodeD.Text = "lblTCompanyNameD";
            this.lblDivisionCodeD.Weight = 0.64597844805726523;
            // 
            // lblDivisionNameD
            // 
            this.lblDivisionNameD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDivisionNameD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDivisionNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDivisionInformation.DivisionName")});
            this.lblDivisionNameD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDivisionNameD.Name = "lblDivisionNameD";
            this.lblDivisionNameD.StylePriority.UseBorderColor = false;
            this.lblDivisionNameD.StylePriority.UseBorders = false;
            this.lblDivisionNameD.StylePriority.UseFont = false;
            this.lblDivisionNameD.Text = "lblVATNoD";
            this.lblDivisionNameD.Weight = 2.3540215519427345;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable22,
            this.tblDivisonC});
            this.GroupHeader12.Expanded = false;
            this.GroupHeader12.HeightF = 87F;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow71});
            this.xrTable22.SizeF = new System.Drawing.SizeF(790F, 40.62F);
            this.xrTable22.StylePriority.UseBorders = false;
            this.xrTable22.StylePriority.UseFont = false;
            this.xrTable22.StylePriority.UseForeColor = false;
            this.xrTable22.StylePriority.UsePadding = false;
            this.xrTable22.StylePriority.UseTextAlignment = false;
            this.xrTable22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDivisionInfoH,
            this.xrTableCell43});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 0.8;
            // 
            // lblDivisionInfoH
            // 
            this.lblDivisionInfoH.BackColor = System.Drawing.Color.White;
            this.lblDivisionInfoH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDivisionInfoH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblDivisionInfoH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblDivisionInfoH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblDivisionInfoH.Name = "lblDivisionInfoH";
            this.lblDivisionInfoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDivisionInfoH.StylePriority.UseBackColor = false;
            this.lblDivisionInfoH.StylePriority.UseBorderColor = false;
            this.lblDivisionInfoH.StylePriority.UseBorders = false;
            this.lblDivisionInfoH.StylePriority.UseFont = false;
            this.lblDivisionInfoH.StylePriority.UseForeColor = false;
            this.lblDivisionInfoH.StylePriority.UsePadding = false;
            this.lblDivisionInfoH.Text = "Company Division Information";
            this.lblDivisionInfoH.Weight = 1.1756499500551834;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.BackColor = System.Drawing.Color.White;
            this.xrTableCell43.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell43.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell43.BorderWidth = 1;
            this.xrTableCell43.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell43.StylePriority.UseBackColor = false;
            this.xrTableCell43.StylePriority.UseBorderColor = false;
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.StylePriority.UseBorderWidth = false;
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseForeColor = false;
            this.xrTableCell43.StylePriority.UsePadding = false;
            this.xrTableCell43.Weight = 1.8243500499448164;
            // 
            // tblDivisonC
            // 
            this.tblDivisonC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblDivisonC.BorderColor = System.Drawing.Color.White;
            this.tblDivisonC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDivisonC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDivisonC.ForeColor = System.Drawing.Color.Gray;
            this.tblDivisonC.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 67F);
            this.tblDivisonC.Name = "tblDivisonC";
            this.tblDivisonC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDivisonC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.tblDivisonC.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblDivisonC.StylePriority.UseBackColor = false;
            this.tblDivisonC.StylePriority.UseBorderColor = false;
            this.tblDivisonC.StylePriority.UseBorders = false;
            this.tblDivisonC.StylePriority.UseFont = false;
            this.tblDivisonC.StylePriority.UseForeColor = false;
            this.tblDivisonC.StylePriority.UsePadding = false;
            this.tblDivisonC.StylePriority.UseTextAlignment = false;
            this.tblDivisonC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDivisionCodeC,
            this.lblDivisionNameC});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 0.8;
            // 
            // lblDivisionCodeC
            // 
            this.lblDivisionCodeC.Name = "lblDivisionCodeC";
            this.lblDivisionCodeC.Text = "Division Code";
            this.lblDivisionCodeC.Weight = 0.64597839007579949;
            // 
            // lblDivisionNameC
            // 
            this.lblDivisionNameC.Name = "lblDivisionNameC";
            this.lblDivisionNameC.Text = "Division Name";
            this.lblDivisionNameC.Weight = 2.3540216099242004;
            // 
            // DRXDSPaymentNotification
            // 
            this.DRXDSPaymentNotification.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DPaymentNotification,
            this.GroupHeader23});
            this.DRXDSPaymentNotification.DataMember = "XDSPaymentNotification";
            this.DRXDSPaymentNotification.Level = 5;
            this.DRXDSPaymentNotification.Name = "DRXDSPaymentNotification";
            // 
            // DPaymentNotification
            // 
            this.DPaymentNotification.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPmtNotificationD});
            this.DPaymentNotification.HeightF = 20F;
            this.DPaymentNotification.Name = "DPaymentNotification";
            // 
            // tblPmtNotificationD
            // 
            this.tblPmtNotificationD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblPmtNotificationD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblPmtNotificationD.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.tblPmtNotificationD.Name = "tblPmtNotificationD";
            this.tblPmtNotificationD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow135});
            this.tblPmtNotificationD.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblPmtNotificationD.StylePriority.UseBorderColor = false;
            this.tblPmtNotificationD.StylePriority.UseBorders = false;
            // 
            // xrTableRow135
            // 
            this.xrTableRow135.BackColor = System.Drawing.Color.White;
            this.xrTableRow135.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow135.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow135.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblpmtSubscriberD,
            this.lblPmtAccNoD,
            this.lblPmtDatelistedD,
            this.lblPmtAmountD,
            this.lblPmtAccStatusD,
            this.lblPmtCommentD,
            this.xrTableCell54,
            this.xrTableCell489});
            this.xrTableRow135.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow135.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow135.Name = "xrTableRow135";
            this.xrTableRow135.StylePriority.UseBackColor = false;
            this.xrTableRow135.StylePriority.UseBorderColor = false;
            this.xrTableRow135.StylePriority.UseBorders = false;
            this.xrTableRow135.StylePriority.UseFont = false;
            this.xrTableRow135.StylePriority.UseForeColor = false;
            this.xrTableRow135.StylePriority.UseTextAlignment = false;
            this.xrTableRow135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow135.Weight = 0.8;
            // 
            // lblpmtSubscriberD
            // 
            this.lblpmtSubscriberD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblpmtSubscriberD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblpmtSubscriberD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Company")});
            this.lblpmtSubscriberD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblpmtSubscriberD.ForeColor = System.Drawing.Color.DimGray;
            this.lblpmtSubscriberD.Name = "lblpmtSubscriberD";
            this.lblpmtSubscriberD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblpmtSubscriberD.StylePriority.UseBorderColor = false;
            this.lblpmtSubscriberD.StylePriority.UseBorders = false;
            this.lblpmtSubscriberD.StylePriority.UseFont = false;
            this.lblpmtSubscriberD.StylePriority.UseForeColor = false;
            this.lblpmtSubscriberD.StylePriority.UsePadding = false;
            this.lblpmtSubscriberD.Text = "lblpmtSubscriberD";
            this.lblpmtSubscriberD.Weight = 0.28409090331106474;
            // 
            // lblPmtAccNoD
            // 
            this.lblPmtAccNoD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblPmtAccNoD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPmtAccNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.AccountNo")});
            this.lblPmtAccNoD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblPmtAccNoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblPmtAccNoD.Name = "lblPmtAccNoD";
            this.lblPmtAccNoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPmtAccNoD.StylePriority.UseBorderColor = false;
            this.lblPmtAccNoD.StylePriority.UseBorders = false;
            this.lblPmtAccNoD.StylePriority.UseFont = false;
            this.lblPmtAccNoD.StylePriority.UseForeColor = false;
            this.lblPmtAccNoD.StylePriority.UsePadding = false;
            this.lblPmtAccNoD.Text = "lblPmtAccNoD";
            this.lblPmtAccNoD.Weight = 0.29114815192100568;
            // 
            // lblPmtDatelistedD
            // 
            this.lblPmtDatelistedD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblPmtDatelistedD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPmtDatelistedD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.AccountType")});
            this.lblPmtDatelistedD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblPmtDatelistedD.ForeColor = System.Drawing.Color.DimGray;
            this.lblPmtDatelistedD.Name = "lblPmtDatelistedD";
            this.lblPmtDatelistedD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPmtDatelistedD.StylePriority.UseBorderColor = false;
            this.lblPmtDatelistedD.StylePriority.UseBorders = false;
            this.lblPmtDatelistedD.StylePriority.UseFont = false;
            this.lblPmtDatelistedD.StylePriority.UseForeColor = false;
            this.lblPmtDatelistedD.StylePriority.UsePadding = false;
            this.lblPmtDatelistedD.Text = "lblPmtDatelistedD";
            this.lblPmtDatelistedD.Weight = 0.3483728026549886;
            // 
            // lblPmtAmountD
            // 
            this.lblPmtAmountD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblPmtAmountD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPmtAmountD.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPmtAmount,
            this.lblPaymentAmnt});
            this.lblPmtAmountD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblPmtAmountD.ForeColor = System.Drawing.Color.DimGray;
            this.lblPmtAmountD.Name = "lblPmtAmountD";
            this.lblPmtAmountD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPmtAmountD.StylePriority.UseBorderColor = false;
            this.lblPmtAmountD.StylePriority.UseBorders = false;
            this.lblPmtAmountD.StylePriority.UseFont = false;
            this.lblPmtAmountD.StylePriority.UseForeColor = false;
            this.lblPmtAmountD.StylePriority.UsePadding = false;
            this.lblPmtAmountD.Text = "lblDAmount";
            this.lblPmtAmountD.Weight = 0.25147016049212795;
            // 
            // lblPmtAmount
            // 
            this.lblPmtAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Amount")});
            this.lblPmtAmount.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lblPmtAmount.Name = "lblPmtAmount";
            this.lblPmtAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPmtAmount.SizeF = new System.Drawing.SizeF(65.75031F, 19.99995F);
            this.lblPmtAmount.Text = "lblPmtAmount";
            // 
            // lblPaymentAmnt
            // 
            this.lblPaymentAmnt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Amount")});
            this.lblPaymentAmnt.LocationFloat = new DevExpress.Utils.PointFloat(8.869171E-05F, 0F);
            this.lblPaymentAmnt.Name = "lblPaymentAmnt";
            this.lblPaymentAmnt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPaymentAmnt.SizeF = new System.Drawing.SizeF(65.75021F, 19.99995F);
            this.lblPaymentAmnt.Text = "lblPaymentAmnt";
            // 
            // lblPmtAccStatusD
            // 
            this.lblPmtAccStatusD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblPmtAccStatusD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPmtAccStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Statuscode")});
            this.lblPmtAccStatusD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblPmtAccStatusD.ForeColor = System.Drawing.Color.DimGray;
            this.lblPmtAccStatusD.Name = "lblPmtAccStatusD";
            this.lblPmtAccStatusD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPmtAccStatusD.StylePriority.UseBorderColor = false;
            this.lblPmtAccStatusD.StylePriority.UseBorders = false;
            this.lblPmtAccStatusD.StylePriority.UseFont = false;
            this.lblPmtAccStatusD.StylePriority.UseForeColor = false;
            this.lblPmtAccStatusD.StylePriority.UsePadding = false;
            this.lblPmtAccStatusD.Text = "lblDAccStatus";
            this.lblPmtAccStatusD.Weight = 0.28325132420189725;
            // 
            // lblPmtCommentD
            // 
            this.lblPmtCommentD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblPmtCommentD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPmtCommentD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.DateLoaded")});
            this.lblPmtCommentD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblPmtCommentD.ForeColor = System.Drawing.Color.DimGray;
            this.lblPmtCommentD.Name = "lblPmtCommentD";
            this.lblPmtCommentD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPmtCommentD.StylePriority.UseBorderColor = false;
            this.lblPmtCommentD.StylePriority.UseBorders = false;
            this.lblPmtCommentD.StylePriority.UseFont = false;
            this.lblPmtCommentD.StylePriority.UseForeColor = false;
            this.lblPmtCommentD.StylePriority.UsePadding = false;
            this.lblPmtCommentD.Text = "lblPmtCommentD";
            this.lblPmtCommentD.Weight = 0.32196971020301768;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell54.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell54.StylePriority.UseBorderColor = false;
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.StylePriority.UseForeColor = false;
            this.xrTableCell54.StylePriority.UsePadding = false;
            this.xrTableCell54.Text = "[CommercialName]";
            this.xrTableCell54.Weight = 0.68181819083102391;
            // 
            // xrTableCell489
            // 
            this.xrTableCell489.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell489.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell489.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Comments")});
            this.xrTableCell489.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell489.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell489.Name = "xrTableCell489";
            this.xrTableCell489.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell489.StylePriority.UseBorderColor = false;
            this.xrTableCell489.StylePriority.UseBorders = false;
            this.xrTableCell489.StylePriority.UseFont = false;
            this.xrTableCell489.StylePriority.UseForeColor = false;
            this.xrTableCell489.StylePriority.UsePadding = false;
            this.xrTableCell489.Text = "xrTableCell489";
            this.xrTableCell489.Weight = 0.54924250802637908;
            // 
            // GroupHeader23
            // 
            this.GroupHeader23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6,
            this.xrTable24,
            this.tblPmtNotificationC});
            this.GroupHeader23.HeightF = 113F;
            this.GroupHeader23.Name = "GroupHeader23";
            // 
            // xrTable6
            // 
            this.xrTable6.ForeColor = System.Drawing.Color.Black;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 68F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable6.SizeF = new System.Drawing.SizeF(790F, 25F);
            this.xrTable6.StylePriority.UseForeColor = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPaymentNotificationsH});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1;
            // 
            // lblPaymentNotificationsH
            // 
            this.lblPaymentNotificationsH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPaymentNotificationsH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblPaymentNotificationsH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblPaymentNotificationsH.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblPaymentNotificationsH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblPaymentNotificationsH.Name = "lblPaymentNotificationsH";
            this.lblPaymentNotificationsH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPaymentNotificationsH.StylePriority.UseBackColor = false;
            this.lblPaymentNotificationsH.StylePriority.UseBorderColor = false;
            this.lblPaymentNotificationsH.StylePriority.UseBorders = false;
            this.lblPaymentNotificationsH.StylePriority.UseFont = false;
            this.lblPaymentNotificationsH.StylePriority.UseForeColor = false;
            this.lblPaymentNotificationsH.StylePriority.UsePadding = false;
            this.lblPaymentNotificationsH.StylePriority.UseTextAlignment = false;
            this.lblPaymentNotificationsH.Text = "XDS Payment Notifications";
            this.lblPaymentNotificationsH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPaymentNotificationsH.Weight = 3;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable24.SizeF = new System.Drawing.SizeF(790F, 40.62F);
            this.xrTable24.StylePriority.UseBorders = false;
            this.xrTable24.StylePriority.UseFont = false;
            this.xrTable24.StylePriority.UseForeColor = false;
            this.xrTable24.StylePriority.UsePadding = false;
            this.xrTable24.StylePriority.UseTextAlignment = false;
            this.xrTable24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPaymentNotificationH,
            this.xrTableCell50});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.8;
            // 
            // lblPaymentNotificationH
            // 
            this.lblPaymentNotificationH.BackColor = System.Drawing.Color.White;
            this.lblPaymentNotificationH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblPaymentNotificationH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblPaymentNotificationH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblPaymentNotificationH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblPaymentNotificationH.Name = "lblPaymentNotificationH";
            this.lblPaymentNotificationH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPaymentNotificationH.StylePriority.UseBackColor = false;
            this.lblPaymentNotificationH.StylePriority.UseBorderColor = false;
            this.lblPaymentNotificationH.StylePriority.UseBorders = false;
            this.lblPaymentNotificationH.StylePriority.UseFont = false;
            this.lblPaymentNotificationH.StylePriority.UseForeColor = false;
            this.lblPaymentNotificationH.StylePriority.UsePadding = false;
            this.lblPaymentNotificationH.Text = "Adverse Information";
            this.lblPaymentNotificationH.Weight = 1.1756499500551834;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.BackColor = System.Drawing.Color.White;
            this.xrTableCell50.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell50.BorderWidth = 1;
            this.xrTableCell50.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell50.StylePriority.UseBackColor = false;
            this.xrTableCell50.StylePriority.UseBorderColor = false;
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UseBorderWidth = false;
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UseForeColor = false;
            this.xrTableCell50.StylePriority.UsePadding = false;
            this.xrTableCell50.Weight = 1.8243500499448164;
            // 
            // tblPmtNotificationC
            // 
            this.tblPmtNotificationC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblPmtNotificationC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblPmtNotificationC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblPmtNotificationC.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 93F);
            this.tblPmtNotificationC.Name = "tblPmtNotificationC";
            this.tblPmtNotificationC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow134});
            this.tblPmtNotificationC.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblPmtNotificationC.StylePriority.UseBorderColor = false;
            this.tblPmtNotificationC.StylePriority.UseBorders = false;
            // 
            // xrTableRow134
            // 
            this.xrTableRow134.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow134.BorderColor = System.Drawing.Color.White;
            this.xrTableRow134.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblpmtSubscriberC,
            this.lblPmtAccNoC,
            this.xrTableCell228,
            this.lblPmtDatelistedC,
            this.lblPmtAmountC,
            this.lblPmtAccStatusC,
            this.xrTableCell229,
            this.xrTableCell303});
            this.xrTableRow134.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow134.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow134.Name = "xrTableRow134";
            this.xrTableRow134.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow134.StylePriority.UseBackColor = false;
            this.xrTableRow134.StylePriority.UseBorderColor = false;
            this.xrTableRow134.StylePriority.UseFont = false;
            this.xrTableRow134.StylePriority.UseForeColor = false;
            this.xrTableRow134.StylePriority.UsePadding = false;
            this.xrTableRow134.StylePriority.UseTextAlignment = false;
            this.xrTableRow134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow134.Weight = 1;
            // 
            // lblpmtSubscriberC
            // 
            this.lblpmtSubscriberC.CanGrow = false;
            this.lblpmtSubscriberC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblpmtSubscriberC.Name = "lblpmtSubscriberC";
            this.lblpmtSubscriberC.StylePriority.UseFont = false;
            this.lblpmtSubscriberC.Text = "Subscriber";
            this.lblpmtSubscriberC.Weight = 0.28409090331106474;
            // 
            // lblPmtAccNoC
            // 
            this.lblPmtAccNoC.CanGrow = false;
            this.lblPmtAccNoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPmtAccNoC.Name = "lblPmtAccNoC";
            this.lblPmtAccNoC.StylePriority.UseFont = false;
            this.lblPmtAccNoC.Text = "Account No.";
            this.lblPmtAccNoC.Weight = 0.29114813066050149;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.CanGrow = false;
            this.xrTableCell228.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseFont = false;
            this.xrTableCell228.Text = "Account type";
            this.xrTableCell228.Weight = 0.34837301909077595;
            // 
            // lblPmtDatelistedC
            // 
            this.lblPmtDatelistedC.CanGrow = false;
            this.lblPmtDatelistedC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPmtDatelistedC.Name = "lblPmtDatelistedC";
            this.lblPmtDatelistedC.StylePriority.UseFont = false;
            this.lblPmtDatelistedC.Text = "Amount";
            this.lblPmtDatelistedC.Weight = 0.25063037233605068;
            // 
            // lblPmtAmountC
            // 
            this.lblPmtAmountC.CanGrow = false;
            this.lblPmtAmountC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPmtAmountC.Name = "lblPmtAmountC";
            this.lblPmtAmountC.StylePriority.UseFont = false;
            this.lblPmtAmountC.Text = "Status";
            this.lblPmtAmountC.Weight = 0.28409091544873782;
            // 
            // lblPmtAccStatusC
            // 
            this.lblPmtAccStatusC.CanGrow = false;
            this.lblPmtAccStatusC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPmtAccStatusC.Name = "lblPmtAccStatusC";
            this.lblPmtAccStatusC.StylePriority.UseFont = false;
            this.lblPmtAccStatusC.Text = "Date loaded";
            this.lblPmtAccStatusC.Weight = 0.32196969870365039;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.CanGrow = false;
            this.xrTableCell229.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseFont = false;
            this.xrTableCell229.Text = "Commercial Name ";
            this.xrTableCell229.Weight = 0.6818181387583413;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.CanGrow = false;
            this.xrTableCell303.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseFont = false;
            this.xrTableCell303.Text = "Comment";
            this.xrTableCell303.Weight = 0.54924245805451377;
            // 
            // DRDefaultListing
            // 
            this.DRDefaultListing.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DDefaultListing,
            this.GroupHeader26});
            this.DRDefaultListing.DataMember = "CommercialDefaultAlert";
            this.DRDefaultListing.Level = 6;
            this.DRDefaultListing.Name = "DRDefaultListing";
            // 
            // DDefaultListing
            // 
            this.DDefaultListing.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDefaultAlertD});
            this.DDefaultListing.HeightF = 20F;
            this.DDefaultListing.Name = "DDefaultListing";
            // 
            // tblDefaultAlertD
            // 
            this.tblDefaultAlertD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDefaultAlertD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDefaultAlertD.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.tblDefaultAlertD.Name = "tblDefaultAlertD";
            this.tblDefaultAlertD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow146});
            this.tblDefaultAlertD.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblDefaultAlertD.StylePriority.UseBorderColor = false;
            this.tblDefaultAlertD.StylePriority.UseBorders = false;
            // 
            // xrTableRow146
            // 
            this.xrTableRow146.BackColor = System.Drawing.Color.White;
            this.xrTableRow146.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow146.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow146.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDACompanyD,
            this.lblDAAccnoD,
            this.lblDADLoadeddateD,
            this.lblDAAmountD,
            this.lblDAStatusD,
            this.lblDACommentsD,
            this.xrTableCell490,
            this.xrTableCell491});
            this.xrTableRow146.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow146.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow146.Name = "xrTableRow146";
            this.xrTableRow146.StylePriority.UseBackColor = false;
            this.xrTableRow146.StylePriority.UseBorderColor = false;
            this.xrTableRow146.StylePriority.UseBorders = false;
            this.xrTableRow146.StylePriority.UseFont = false;
            this.xrTableRow146.StylePriority.UseForeColor = false;
            this.xrTableRow146.StylePriority.UseTextAlignment = false;
            this.xrTableRow146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow146.Weight = 0.8;
            // 
            // lblDACompanyD
            // 
            this.lblDACompanyD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDACompanyD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDACompanyD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.Company")});
            this.lblDACompanyD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDACompanyD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDACompanyD.Name = "lblDACompanyD";
            this.lblDACompanyD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDACompanyD.StylePriority.UseBorderColor = false;
            this.lblDACompanyD.StylePriority.UseBorders = false;
            this.lblDACompanyD.StylePriority.UseFont = false;
            this.lblDACompanyD.StylePriority.UseForeColor = false;
            this.lblDACompanyD.StylePriority.UsePadding = false;
            this.lblDACompanyD.Text = "lblDACompanyD";
            this.lblDACompanyD.Weight = 0.28409093221028642;
            // 
            // lblDAAccnoD
            // 
            this.lblDAAccnoD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDAAccnoD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDAAccnoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.AccountNo")});
            this.lblDAAccnoD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDAAccnoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAAccnoD.Name = "lblDAAccnoD";
            this.lblDAAccnoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAAccnoD.StylePriority.UseBorderColor = false;
            this.lblDAAccnoD.StylePriority.UseBorders = false;
            this.lblDAAccnoD.StylePriority.UseFont = false;
            this.lblDAAccnoD.StylePriority.UseForeColor = false;
            this.lblDAAccnoD.StylePriority.UsePadding = false;
            this.lblDAAccnoD.Text = "lblDAAccnoD";
            this.lblDAAccnoD.Weight = 0.29093832725299978;
            // 
            // lblDADLoadeddateD
            // 
            this.lblDADLoadeddateD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDADLoadeddateD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDADLoadeddateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.AccountType")});
            this.lblDADLoadeddateD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDADLoadeddateD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDADLoadeddateD.Name = "lblDADLoadeddateD";
            this.lblDADLoadeddateD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDADLoadeddateD.StylePriority.UseBorderColor = false;
            this.lblDADLoadeddateD.StylePriority.UseBorders = false;
            this.lblDADLoadeddateD.StylePriority.UseFont = false;
            this.lblDADLoadeddateD.StylePriority.UseForeColor = false;
            this.lblDADLoadeddateD.StylePriority.UsePadding = false;
            this.lblDADLoadeddateD.Text = "lblDADLoadeddateD";
            this.lblDADLoadeddateD.Weight = 0.34824598677861357;
            // 
            // lblDAAmountD
            // 
            this.lblDAAmountD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDAAmountD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDAAmountD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.Amount")});
            this.lblDAAmountD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDAAmountD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAAmountD.Name = "lblDAAmountD";
            this.lblDAAmountD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAAmountD.StylePriority.UseBorderColor = false;
            this.lblDAAmountD.StylePriority.UseBorders = false;
            this.lblDAAmountD.StylePriority.UseFont = false;
            this.lblDAAmountD.StylePriority.UseForeColor = false;
            this.lblDAAmountD.StylePriority.UsePadding = false;
            this.lblDAAmountD.Text = "lblDAmount";
            this.lblDAAmountD.Weight = 0.2509672288631547;
            // 
            // lblDAStatusD
            // 
            this.lblDAStatusD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDAStatusD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDAStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.Statuscode")});
            this.lblDAStatusD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDAStatusD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAStatusD.Name = "lblDAStatusD";
            this.lblDAStatusD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAStatusD.StylePriority.UseBorderColor = false;
            this.lblDAStatusD.StylePriority.UseBorders = false;
            this.lblDAStatusD.StylePriority.UseFont = false;
            this.lblDAStatusD.StylePriority.UseForeColor = false;
            this.lblDAStatusD.StylePriority.UsePadding = false;
            this.lblDAStatusD.Text = "lblDAccStatus";
            this.lblDAStatusD.Weight = 0.28409086747602963;
            // 
            // lblDACommentsD
            // 
            this.lblDACommentsD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDACommentsD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDACommentsD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.StatusDate")});
            this.lblDACommentsD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDACommentsD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDACommentsD.Name = "lblDACommentsD";
            this.lblDACommentsD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDACommentsD.StylePriority.UseBorderColor = false;
            this.lblDACommentsD.StylePriority.UseBorders = false;
            this.lblDACommentsD.StylePriority.UseFont = false;
            this.lblDACommentsD.StylePriority.UseForeColor = false;
            this.lblDACommentsD.StylePriority.UsePadding = false;
            this.lblDACommentsD.Text = "lblDACommentsD";
            this.lblDACommentsD.Weight = 0.32196969852778362;
            // 
            // xrTableCell490
            // 
            this.xrTableCell490.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell490.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell490.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.CommercialName")});
            this.xrTableCell490.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell490.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell490.Name = "xrTableCell490";
            this.xrTableCell490.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell490.StylePriority.UseBorderColor = false;
            this.xrTableCell490.StylePriority.UseBorders = false;
            this.xrTableCell490.StylePriority.UseFont = false;
            this.xrTableCell490.StylePriority.UseForeColor = false;
            this.xrTableCell490.StylePriority.UsePadding = false;
            this.xrTableCell490.Text = "xrTableCell490";
            this.xrTableCell490.Weight = 0.6818182025976911;
            // 
            // xrTableCell491
            // 
            this.xrTableCell491.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell491.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell491.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialDefaultAlert.Comments")});
            this.xrTableCell491.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell491.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell491.Name = "xrTableCell491";
            this.xrTableCell491.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell491.StylePriority.UseBorderColor = false;
            this.xrTableCell491.StylePriority.UseBorders = false;
            this.xrTableCell491.StylePriority.UseFont = false;
            this.xrTableCell491.StylePriority.UseForeColor = false;
            this.xrTableCell491.StylePriority.UsePadding = false;
            this.xrTableCell491.Text = "xrTableCell491";
            this.xrTableCell491.Weight = 0.54814433782901162;
            // 
            // GroupHeader26
            // 
            this.GroupHeader26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDefaultAlertC,
            this.tblDefaultAlertH});
            this.GroupHeader26.HeightF = 55F;
            this.GroupHeader26.Name = "GroupHeader26";
            // 
            // tblDefaultAlertC
            // 
            this.tblDefaultAlertC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblDefaultAlertC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDefaultAlertC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDefaultAlertC.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 35F);
            this.tblDefaultAlertC.Name = "tblDefaultAlertC";
            this.tblDefaultAlertC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow145});
            this.tblDefaultAlertC.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblDefaultAlertC.StylePriority.UseBorderColor = false;
            this.tblDefaultAlertC.StylePriority.UseBorders = false;
            // 
            // xrTableRow145
            // 
            this.xrTableRow145.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow145.BorderColor = System.Drawing.Color.White;
            this.xrTableRow145.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell353,
            this.xrTableCell354,
            this.xrTableCell355,
            this.xrTableCell356,
            this.xrTableCell357,
            this.xrTableCell358,
            this.xrTableCell359,
            this.xrTableCell362});
            this.xrTableRow145.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow145.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow145.Name = "xrTableRow145";
            this.xrTableRow145.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow145.StylePriority.UseBackColor = false;
            this.xrTableRow145.StylePriority.UseBorderColor = false;
            this.xrTableRow145.StylePriority.UseFont = false;
            this.xrTableRow145.StylePriority.UseForeColor = false;
            this.xrTableRow145.StylePriority.UsePadding = false;
            this.xrTableRow145.StylePriority.UseTextAlignment = false;
            this.xrTableRow145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow145.Weight = 1;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.CanGrow = false;
            this.xrTableCell353.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseFont = false;
            this.xrTableCell353.Text = "Subscriber";
            this.xrTableCell353.Weight = 0.28409090331106474;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.CanGrow = false;
            this.xrTableCell354.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseFont = false;
            this.xrTableCell354.Text = "Account No.";
            this.xrTableCell354.Weight = 0.29114813066050149;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.CanGrow = false;
            this.xrTableCell355.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.StylePriority.UseFont = false;
            this.xrTableCell355.Text = "Account type";
            this.xrTableCell355.Weight = 0.34837313541928872;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.CanGrow = false;
            this.xrTableCell356.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseFont = false;
            this.xrTableCell356.Text = "Amount";
            this.xrTableCell356.Weight = 0.25063025600753791;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.CanGrow = false;
            this.xrTableCell357.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseFont = false;
            this.xrTableCell357.Text = "Status";
            this.xrTableCell357.Weight = 0.28409091544873782;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.CanGrow = false;
            this.xrTableCell358.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.StylePriority.UseFont = false;
            this.xrTableCell358.Text = "Status Date";
            this.xrTableCell358.Weight = 0.32196969870365033;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.CanGrow = false;
            this.xrTableCell359.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.StylePriority.UseFont = false;
            this.xrTableCell359.Text = "Commercial Name";
            this.xrTableCell359.Weight = 0.68181816274469553;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.CanGrow = false;
            this.xrTableCell362.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.StylePriority.UseFont = false;
            this.xrTableCell362.Text = "Comment";
            this.xrTableCell362.Weight = 0.54924243406815965;
            // 
            // tblDefaultAlertH
            // 
            this.tblDefaultAlertH.ForeColor = System.Drawing.Color.Black;
            this.tblDefaultAlertH.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 10F);
            this.tblDefaultAlertH.Name = "tblDefaultAlertH";
            this.tblDefaultAlertH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51});
            this.tblDefaultAlertH.SizeF = new System.Drawing.SizeF(790F, 25F);
            this.tblDefaultAlertH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefaultAlertH});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1;
            // 
            // lblDefaultAlertH
            // 
            this.lblDefaultAlertH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDefaultAlertH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblDefaultAlertH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDefaultAlertH.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblDefaultAlertH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblDefaultAlertH.Name = "lblDefaultAlertH";
            this.lblDefaultAlertH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDefaultAlertH.StylePriority.UseBackColor = false;
            this.lblDefaultAlertH.StylePriority.UseBorderColor = false;
            this.lblDefaultAlertH.StylePriority.UseBorders = false;
            this.lblDefaultAlertH.StylePriority.UseFont = false;
            this.lblDefaultAlertH.StylePriority.UseForeColor = false;
            this.lblDefaultAlertH.StylePriority.UsePadding = false;
            this.lblDefaultAlertH.StylePriority.UseTextAlignment = false;
            this.lblDefaultAlertH.Text = "Default Listing";
            this.lblDefaultAlertH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDefaultAlertH.Weight = 3;
            // 
            // DRRescue
            // 
            this.DRRescue.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DRescue,
            this.GroupHeader27});
            this.DRRescue.Level = 9;
            this.DRRescue.Name = "DRRescue";
            // 
            // DRescue
            // 
            this.DRescue.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDRescue});
            this.DRescue.HeightF = 20F;
            this.DRescue.Name = "DRescue";
            // 
            // tblDRescue
            // 
            this.tblDRescue.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblDRescue.BackColor = System.Drawing.Color.White;
            this.tblDRescue.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDRescue.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDRescue.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 0F);
            this.tblDRescue.Name = "tblDRescue";
            this.tblDRescue.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow149});
            this.tblDRescue.SizeF = new System.Drawing.SizeF(790.42F, 20F);
            this.tblDRescue.StylePriority.UseBackColor = false;
            this.tblDRescue.StylePriority.UseBorderColor = false;
            this.tblDRescue.StylePriority.UseBorders = false;
            // 
            // xrTableRow149
            // 
            this.xrTableRow149.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow149.BorderColor = System.Drawing.Color.White;
            this.xrTableRow149.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDReportedby,
            this.xrTableCell542,
            this.xrTableCell541,
            this.lblDReportedDate});
            this.xrTableRow149.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow149.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow149.Name = "xrTableRow149";
            this.xrTableRow149.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow149.StylePriority.UseBackColor = false;
            this.xrTableRow149.StylePriority.UseBorderColor = false;
            this.xrTableRow149.StylePriority.UseFont = false;
            this.xrTableRow149.StylePriority.UseForeColor = false;
            this.xrTableRow149.StylePriority.UsePadding = false;
            this.xrTableRow149.StylePriority.UseTextAlignment = false;
            this.xrTableRow149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow149.Weight = 1;
            // 
            // lblDReportedby
            // 
            this.lblDReportedby.BackColor = System.Drawing.Color.White;
            this.lblDReportedby.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDReportedby.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDReportedby.CanGrow = false;
            this.lblDReportedby.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDReportedby.ForeColor = System.Drawing.Color.DimGray;
            this.lblDReportedby.Name = "lblDReportedby";
            this.lblDReportedby.StylePriority.UseBackColor = false;
            this.lblDReportedby.StylePriority.UseBorderColor = false;
            this.lblDReportedby.StylePriority.UseBorders = false;
            this.lblDReportedby.StylePriority.UseFont = false;
            this.lblDReportedby.StylePriority.UseForeColor = false;
            this.lblDReportedby.Text = "Reported by";
            this.lblDReportedby.Weight = 0.94152155802269522;
            // 
            // xrTableCell542
            // 
            this.xrTableCell542.BackColor = System.Drawing.Color.White;
            this.xrTableCell542.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell542.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell542.CanGrow = false;
            this.xrTableCell542.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell542.Name = "xrTableCell542";
            this.xrTableCell542.StylePriority.UseBackColor = false;
            this.xrTableCell542.StylePriority.UseBorderColor = false;
            this.xrTableCell542.StylePriority.UseBorders = false;
            this.xrTableCell542.StylePriority.UseFont = false;
            this.xrTableCell542.Text = "xrTableCell542";
            this.xrTableCell542.Weight = 1.4452689444885745;
            // 
            // xrTableCell541
            // 
            this.xrTableCell541.BackColor = System.Drawing.Color.White;
            this.xrTableCell541.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell541.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell541.CanGrow = false;
            this.xrTableCell541.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell541.Name = "xrTableCell541";
            this.xrTableCell541.StylePriority.UseBackColor = false;
            this.xrTableCell541.StylePriority.UseBorderColor = false;
            this.xrTableCell541.StylePriority.UseBorders = false;
            this.xrTableCell541.StylePriority.UseFont = false;
            this.xrTableCell541.Text = "xrTableCell541";
            this.xrTableCell541.Weight = 1.2016581343788182;
            // 
            // lblDReportedDate
            // 
            this.lblDReportedDate.BackColor = System.Drawing.Color.White;
            this.lblDReportedDate.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDReportedDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDReportedDate.CanGrow = false;
            this.lblDReportedDate.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblDReportedDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDReportedDate.Name = "lblDReportedDate";
            this.lblDReportedDate.StylePriority.UseBackColor = false;
            this.lblDReportedDate.StylePriority.UseBorderColor = false;
            this.lblDReportedDate.StylePriority.UseBorders = false;
            this.lblDReportedDate.StylePriority.UseFont = false;
            this.lblDReportedDate.StylePriority.UseForeColor = false;
            this.lblDReportedDate.Text = "Date Reported";
            this.lblDReportedDate.Weight = 1.9107768284184314;
            // 
            // GroupHeader27
            // 
            this.GroupHeader27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblBusinessRescueH,
            this.tblCRescue});
            this.GroupHeader27.HeightF = 55F;
            this.GroupHeader27.Name = "GroupHeader27";
            // 
            // lblBusinessRescueH
            // 
            this.lblBusinessRescueH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblBusinessRescueH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.lblBusinessRescueH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblBusinessRescueH.BorderWidth = 2;
            this.lblBusinessRescueH.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblBusinessRescueH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblBusinessRescueH.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 14.9998F);
            this.lblBusinessRescueH.Name = "lblBusinessRescueH";
            this.lblBusinessRescueH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblBusinessRescueH.SizeF = new System.Drawing.SizeF(788.3834F, 20.0002F);
            this.lblBusinessRescueH.StylePriority.UseBackColor = false;
            this.lblBusinessRescueH.StylePriority.UseBorderColor = false;
            this.lblBusinessRescueH.StylePriority.UseBorders = false;
            this.lblBusinessRescueH.StylePriority.UseBorderWidth = false;
            this.lblBusinessRescueH.StylePriority.UseFont = false;
            this.lblBusinessRescueH.StylePriority.UseForeColor = false;
            this.lblBusinessRescueH.StylePriority.UsePadding = false;
            this.lblBusinessRescueH.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "Bankcode - {0}";
            xrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            this.lblBusinessRescueH.Summary = xrSummary5;
            this.lblBusinessRescueH.Text = "Business Rescue";
            this.lblBusinessRescueH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // tblCRescue
            // 
            this.tblCRescue.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblCRescue.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblCRescue.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCRescue.LocationFloat = new DevExpress.Utils.PointFloat(13.2F, 35F);
            this.tblCRescue.Name = "tblCRescue";
            this.tblCRescue.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow148});
            this.tblCRescue.SizeF = new System.Drawing.SizeF(790F, 20F);
            this.tblCRescue.StylePriority.UseBorderColor = false;
            this.tblCRescue.StylePriority.UseBorders = false;
            // 
            // xrTableRow148
            // 
            this.xrTableRow148.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow148.BorderColor = System.Drawing.Color.White;
            this.xrTableRow148.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCReportedby,
            this.xrTableCell540,
            this.xrTableCell539,
            this.lblCDateReported});
            this.xrTableRow148.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow148.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow148.Name = "xrTableRow148";
            this.xrTableRow148.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow148.StylePriority.UseBackColor = false;
            this.xrTableRow148.StylePriority.UseBorderColor = false;
            this.xrTableRow148.StylePriority.UseFont = false;
            this.xrTableRow148.StylePriority.UseForeColor = false;
            this.xrTableRow148.StylePriority.UsePadding = false;
            this.xrTableRow148.StylePriority.UseTextAlignment = false;
            this.xrTableRow148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow148.Weight = 1;
            // 
            // lblCReportedby
            // 
            this.lblCReportedby.CanGrow = false;
            this.lblCReportedby.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCReportedby.Name = "lblCReportedby";
            this.lblCReportedby.StylePriority.UseFont = false;
            this.lblCReportedby.Text = "Date";
            this.lblCReportedby.Weight = 0.97156996976778542;
            // 
            // xrTableCell540
            // 
            this.xrTableCell540.CanGrow = false;
            this.xrTableCell540.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell540.Name = "xrTableCell540";
            this.xrTableCell540.StylePriority.UseFont = false;
            this.xrTableCell540.Text = "Business Rescue Practitioner";
            this.xrTableCell540.Weight = 1.3879571761385317;
            // 
            // xrTableCell539
            // 
            this.xrTableCell539.CanGrow = false;
            this.xrTableCell539.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell539.Name = "xrTableCell539";
            this.xrTableCell539.StylePriority.UseFont = false;
            this.xrTableCell539.Text = "Contact Information";
            this.xrTableCell539.Weight = 1.2491614100967898;
            // 
            // lblCDateReported
            // 
            this.lblCDateReported.CanGrow = false;
            this.lblCDateReported.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCDateReported.Name = "lblCDateReported";
            this.lblCDateReported.StylePriority.UseFont = false;
            this.lblCDateReported.Text = "Details";
            this.lblCDateReported.Weight = 1.9153808251929863;
            // 
            // DRBankInformation
            // 
            this.DRBankInformation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.BankDetails,
            this.GroupHeader29,
            this.CommercialPreviousBankDetails});
            this.DRBankInformation.DataMember = "CommercialBankDetails";
            this.DRBankInformation.Level = 12;
            this.DRBankInformation.Name = "DRBankInformation";
            this.DRBankInformation.Visible = false;
            // 
            // BankDetails
            // 
            this.BankDetails.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
            this.BankDetails.Expanded = false;
            this.BankDetails.HeightF = 179.2858F;
            this.BankDetails.Name = "BankDetails";
            // 
            // xrTable16
            // 
            this.xrTable16.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow136,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow3,
            this.xrTableRow29,
            this.xrTableRow46,
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow5});
            this.xrTable16.SizeF = new System.Drawing.SizeF(792F, 179.2858F);
            this.xrTable16.StylePriority.UseBorderColor = false;
            this.xrTable16.StylePriority.UseBorders = false;
            this.xrTable16.StylePriority.UseFont = false;
            // 
            // xrTableRow136
            // 
            this.xrTableRow136.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106});
            this.xrTableRow136.Name = "xrTableRow136";
            this.xrTableRow136.Weight = 0.834319526627219;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell106.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell106.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell106.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell106.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell106.StylePriority.UseBackColor = false;
            this.xrTableCell106.StylePriority.UseBorderColor = false;
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.StylePriority.UseFont = false;
            this.xrTableCell106.StylePriority.UseForeColor = false;
            this.xrTableCell106.StylePriority.UsePadding = false;
            this.xrTableCell106.Text = "Bank details ";
            this.xrTableCell106.Weight = 3;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblBank,
            this.LblBankD});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.83431952662721887;
            // 
            // LblBank
            // 
            this.LblBank.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblBank.BorderColor = System.Drawing.Color.White;
            this.LblBank.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblBank.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblBank.ForeColor = System.Drawing.Color.Gray;
            this.LblBank.Name = "LblBank";
            this.LblBank.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblBank.StylePriority.UseBackColor = false;
            this.LblBank.StylePriority.UseBorderColor = false;
            this.LblBank.StylePriority.UseBorders = false;
            this.LblBank.StylePriority.UseFont = false;
            this.LblBank.StylePriority.UseForeColor = false;
            this.LblBank.StylePriority.UsePadding = false;
            this.LblBank.StylePriority.UseTextAlignment = false;
            this.LblBank.Text = "Bank";
            this.LblBank.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblBank.Weight = 1.5;
            // 
            // LblBankD
            // 
            this.LblBankD.BackColor = System.Drawing.Color.White;
            this.LblBankD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblBankD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.Bank")});
            this.LblBankD.ForeColor = System.Drawing.Color.DimGray;
            this.LblBankD.Name = "LblBankD";
            this.LblBankD.StylePriority.UseBackColor = false;
            this.LblBankD.StylePriority.UseBorderColor = false;
            this.LblBankD.StylePriority.UseForeColor = false;
            this.LblBankD.Text = "LblBankD";
            this.LblBankD.Weight = 1.5;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblBranch,
            this.LblBranchD});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.83431952662721887;
            // 
            // LblBranch
            // 
            this.LblBranch.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblBranch.BorderColor = System.Drawing.Color.White;
            this.LblBranch.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblBranch.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblBranch.ForeColor = System.Drawing.Color.Gray;
            this.LblBranch.Name = "LblBranch";
            this.LblBranch.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblBranch.StylePriority.UseBackColor = false;
            this.LblBranch.StylePriority.UseBorderColor = false;
            this.LblBranch.StylePriority.UseBorders = false;
            this.LblBranch.StylePriority.UseFont = false;
            this.LblBranch.StylePriority.UseForeColor = false;
            this.LblBranch.StylePriority.UsePadding = false;
            this.LblBranch.StylePriority.UseTextAlignment = false;
            this.LblBranch.Text = "Branch";
            this.LblBranch.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblBranch.Weight = 1.5;
            // 
            // LblBranchD
            // 
            this.LblBranchD.BackColor = System.Drawing.Color.White;
            this.LblBranchD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblBranchD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.BranchName")});
            this.LblBranchD.ForeColor = System.Drawing.Color.DimGray;
            this.LblBranchD.Name = "LblBranchD";
            this.LblBranchD.StylePriority.UseBackColor = false;
            this.LblBranchD.StylePriority.UseBorderColor = false;
            this.LblBranchD.StylePriority.UseForeColor = false;
            this.LblBranchD.Text = "LblBranchD";
            this.LblBranchD.Weight = 1.5;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblAccountNumber,
            this.LbLAccountNumberD});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.83431952662721887;
            // 
            // LblAccountNumber
            // 
            this.LblAccountNumber.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblAccountNumber.BorderColor = System.Drawing.Color.White;
            this.LblAccountNumber.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblAccountNumber.ForeColor = System.Drawing.SystemColors.GrayText;
            this.LblAccountNumber.Name = "LblAccountNumber";
            this.LblAccountNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblAccountNumber.StylePriority.UseBackColor = false;
            this.LblAccountNumber.StylePriority.UseBorderColor = false;
            this.LblAccountNumber.StylePriority.UseBorders = false;
            this.LblAccountNumber.StylePriority.UseForeColor = false;
            this.LblAccountNumber.StylePriority.UsePadding = false;
            this.LblAccountNumber.Text = "Bank Account Number";
            this.LblAccountNumber.Weight = 1.5;
            // 
            // LbLAccountNumberD
            // 
            this.LbLAccountNumberD.BackColor = System.Drawing.Color.White;
            this.LbLAccountNumberD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LbLAccountNumberD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.BankAccountNumber")});
            this.LbLAccountNumberD.ForeColor = System.Drawing.Color.DimGray;
            this.LbLAccountNumberD.Name = "LbLAccountNumberD";
            this.LbLAccountNumberD.StylePriority.UseBackColor = false;
            this.LbLAccountNumberD.StylePriority.UseBorderColor = false;
            this.LbLAccountNumberD.StylePriority.UseForeColor = false;
            this.LbLAccountNumberD.Text = "LbLAccountNumberD";
            this.LbLAccountNumberD.Weight = 1.5;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblTypeOfAccount,
            this.LblTypeOfAccountD});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.83431952662721875;
            // 
            // LblTypeOfAccount
            // 
            this.LblTypeOfAccount.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblTypeOfAccount.BorderColor = System.Drawing.Color.White;
            this.LblTypeOfAccount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblTypeOfAccount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblTypeOfAccount.ForeColor = System.Drawing.Color.Gray;
            this.LblTypeOfAccount.Name = "LblTypeOfAccount";
            this.LblTypeOfAccount.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblTypeOfAccount.StylePriority.UseBackColor = false;
            this.LblTypeOfAccount.StylePriority.UseBorderColor = false;
            this.LblTypeOfAccount.StylePriority.UseBorders = false;
            this.LblTypeOfAccount.StylePriority.UseFont = false;
            this.LblTypeOfAccount.StylePriority.UseForeColor = false;
            this.LblTypeOfAccount.StylePriority.UsePadding = false;
            this.LblTypeOfAccount.StylePriority.UseTextAlignment = false;
            this.LblTypeOfAccount.Text = "Type Of Account";
            this.LblTypeOfAccount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblTypeOfAccount.Weight = 1.5;
            // 
            // LblTypeOfAccountD
            // 
            this.LblTypeOfAccountD.BackColor = System.Drawing.Color.White;
            this.LblTypeOfAccountD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblTypeOfAccountD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.TypeOfAccount")});
            this.LblTypeOfAccountD.ForeColor = System.Drawing.Color.DimGray;
            this.LblTypeOfAccountD.Name = "LblTypeOfAccountD";
            this.LblTypeOfAccountD.StylePriority.UseBackColor = false;
            this.LblTypeOfAccountD.StylePriority.UseBorderColor = false;
            this.LblTypeOfAccountD.StylePriority.UseForeColor = false;
            this.LblTypeOfAccountD.Text = "LblTypeOfAccountD";
            this.LblTypeOfAccountD.Weight = 1.5;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblAccountHolderName,
            this.LblAccountHolderNameD});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.834319526627219;
            // 
            // LblAccountHolderName
            // 
            this.LblAccountHolderName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblAccountHolderName.BorderColor = System.Drawing.Color.White;
            this.LblAccountHolderName.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblAccountHolderName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblAccountHolderName.ForeColor = System.Drawing.Color.Gray;
            this.LblAccountHolderName.Name = "LblAccountHolderName";
            this.LblAccountHolderName.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblAccountHolderName.StylePriority.UseBackColor = false;
            this.LblAccountHolderName.StylePriority.UseBorderColor = false;
            this.LblAccountHolderName.StylePriority.UseBorders = false;
            this.LblAccountHolderName.StylePriority.UseFont = false;
            this.LblAccountHolderName.StylePriority.UseForeColor = false;
            this.LblAccountHolderName.StylePriority.UsePadding = false;
            this.LblAccountHolderName.StylePriority.UseTextAlignment = false;
            this.LblAccountHolderName.Text = "Account Holder Name";
            this.LblAccountHolderName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblAccountHolderName.Weight = 1.5;
            // 
            // LblAccountHolderNameD
            // 
            this.LblAccountHolderNameD.BackColor = System.Drawing.Color.White;
            this.LblAccountHolderNameD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblAccountHolderNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.AccountHolderName")});
            this.LblAccountHolderNameD.ForeColor = System.Drawing.Color.DimGray;
            this.LblAccountHolderNameD.Name = "LblAccountHolderNameD";
            this.LblAccountHolderNameD.StylePriority.UseBackColor = false;
            this.LblAccountHolderNameD.StylePriority.UseBorderColor = false;
            this.LblAccountHolderNameD.StylePriority.UseForeColor = false;
            this.LblAccountHolderNameD.Text = "LblAccountHolderNameD";
            this.LblAccountHolderNameD.Weight = 1.5;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblOverDraftFac,
            this.LblOverDarftFacD});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 0.834319526627219;
            // 
            // LblOverDraftFac
            // 
            this.LblOverDraftFac.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblOverDraftFac.BorderColor = System.Drawing.Color.White;
            this.LblOverDraftFac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblOverDraftFac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblOverDraftFac.ForeColor = System.Drawing.Color.Gray;
            this.LblOverDraftFac.Name = "LblOverDraftFac";
            this.LblOverDraftFac.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblOverDraftFac.StylePriority.UseBackColor = false;
            this.LblOverDraftFac.StylePriority.UseBorderColor = false;
            this.LblOverDraftFac.StylePriority.UseBorders = false;
            this.LblOverDraftFac.StylePriority.UseFont = false;
            this.LblOverDraftFac.StylePriority.UseForeColor = false;
            this.LblOverDraftFac.StylePriority.UsePadding = false;
            this.LblOverDraftFac.StylePriority.UseTextAlignment = false;
            this.LblOverDraftFac.Text = "Overdraft Facility";
            this.LblOverDraftFac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblOverDraftFac.Weight = 1.5;
            // 
            // LblOverDarftFacD
            // 
            this.LblOverDarftFacD.BackColor = System.Drawing.Color.White;
            this.LblOverDarftFacD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblOverDarftFacD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.OverdraftFacilities")});
            this.LblOverDarftFacD.ForeColor = System.Drawing.Color.DimGray;
            this.LblOverDarftFacD.Name = "LblOverDarftFacD";
            this.LblOverDarftFacD.StylePriority.UseBackColor = false;
            this.LblOverDarftFacD.StylePriority.UseBorderColor = false;
            this.LblOverDarftFacD.StylePriority.UseForeColor = false;
            this.LblOverDarftFacD.Text = "LblOverDarftFacD";
            this.LblOverDarftFacD.Weight = 1.5;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblBankCode,
            this.LblBankCodeD});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 0.79289940828402361;
            // 
            // LblBankCode
            // 
            this.LblBankCode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblBankCode.BorderColor = System.Drawing.Color.White;
            this.LblBankCode.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblBankCode.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblBankCode.ForeColor = System.Drawing.Color.Gray;
            this.LblBankCode.Name = "LblBankCode";
            this.LblBankCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblBankCode.StylePriority.UseBackColor = false;
            this.LblBankCode.StylePriority.UseBorderColor = false;
            this.LblBankCode.StylePriority.UseBorders = false;
            this.LblBankCode.StylePriority.UseFont = false;
            this.LblBankCode.StylePriority.UseForeColor = false;
            this.LblBankCode.StylePriority.UsePadding = false;
            this.LblBankCode.StylePriority.UseTextAlignment = false;
            this.LblBankCode.Text = "Bank Code";
            this.LblBankCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.LblBankCode.Weight = 1.5;
            // 
            // LblBankCodeD
            // 
            this.LblBankCodeD.BackColor = System.Drawing.Color.White;
            this.LblBankCodeD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblBankCodeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.BankCode")});
            this.LblBankCodeD.ForeColor = System.Drawing.Color.DimGray;
            this.LblBankCodeD.Name = "LblBankCodeD";
            this.LblBankCodeD.StylePriority.UseBackColor = false;
            this.LblBankCodeD.StylePriority.UseBorderColor = false;
            this.LblBankCodeD.StylePriority.UseForeColor = false;
            this.LblBankCodeD.Text = "LblBankCodeD";
            this.LblBankCodeD.Weight = 1.5;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.LblBankComment,
            this.LblBankCommentD});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.79289940828402361;
            // 
            // LblBankComment
            // 
            this.LblBankComment.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LblBankComment.BorderColor = System.Drawing.Color.White;
            this.LblBankComment.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.LblBankComment.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LblBankComment.ForeColor = System.Drawing.Color.Gray;
            this.LblBankComment.Name = "LblBankComment";
            this.LblBankComment.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblBankComment.StylePriority.UseBackColor = false;
            this.LblBankComment.StylePriority.UseBorderColor = false;
            this.LblBankComment.StylePriority.UseBorders = false;
            this.LblBankComment.StylePriority.UseFont = false;
            this.LblBankComment.StylePriority.UseForeColor = false;
            this.LblBankComment.StylePriority.UsePadding = false;
            this.LblBankComment.Text = "Bank Comment\t";
            this.LblBankComment.Weight = 1.5;
            // 
            // LblBankCommentD
            // 
            this.LblBankCommentD.BackColor = System.Drawing.Color.White;
            this.LblBankCommentD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblBankCommentD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.LblBankCommentD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialBankDetails.BankComment")});
            this.LblBankCommentD.ForeColor = System.Drawing.Color.DimGray;
            this.LblBankCommentD.Name = "LblBankCommentD";
            this.LblBankCommentD.StylePriority.UseBackColor = false;
            this.LblBankCommentD.StylePriority.UseBorderColor = false;
            this.LblBankCommentD.StylePriority.UseBorders = false;
            this.LblBankCommentD.StylePriority.UseForeColor = false;
            this.LblBankCommentD.Text = "LblBankCommentD";
            this.LblBankCommentD.Weight = 1.5;
            // 
            // GroupHeader29
            // 
            this.GroupHeader29.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.GroupHeader29.Expanded = false;
            this.GroupHeader29.HeightF = 61F;
            this.GroupHeader29.Name = "GroupHeader29";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10.37499F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(792.0001F, 40.625F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell71});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.8;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BackColor = System.Drawing.Color.White;
            this.xrTableCell68.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell68.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell68.StylePriority.UseBackColor = false;
            this.xrTableCell68.StylePriority.UseBorderColor = false;
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UseForeColor = false;
            this.xrTableCell68.StylePriority.UsePadding = false;
            this.xrTableCell68.Text = "Bank Information";
            this.xrTableCell68.Weight = 1.1756499500551834;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.BackColor = System.Drawing.Color.White;
            this.xrTableCell71.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell71.BorderWidth = 1;
            this.xrTableCell71.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell71.StylePriority.UseBackColor = false;
            this.xrTableCell71.StylePriority.UseBorderColor = false;
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.StylePriority.UseBorderWidth = false;
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UseForeColor = false;
            this.xrTableCell71.StylePriority.UsePadding = false;
            this.xrTableCell71.Weight = 1.8243500499448164;
            // 
            // CommercialPreviousBankDetails
            // 
            this.CommercialPreviousBankDetails.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail10,
            this.GroupHeader35});
            this.CommercialPreviousBankDetails.Level = 0;
            this.CommercialPreviousBankDetails.Name = "CommercialPreviousBankDetails";
            this.CommercialPreviousBankDetails.Visible = false;
            // 
            // Detail10
            // 
            this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable18});
            this.Detail10.Expanded = false;
            this.Detail10.HeightF = 20.14286F;
            this.Detail10.Name = "Detail10";
            // 
            // xrTable18
            // 
            this.xrTable18.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow169});
            this.xrTable18.SizeF = new System.Drawing.SizeF(792F, 20.14286F);
            this.xrTable18.StylePriority.UseBorderColor = false;
            this.xrTable18.StylePriority.UseBorders = false;
            this.xrTable18.StylePriority.UseFont = false;
            // 
            // xrTableRow169
            // 
            this.xrTableRow169.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116,
            this.LblYearsWithBankD});
            this.xrTableRow169.Name = "xrTableRow169";
            this.xrTableRow169.Weight = 0.83431952662721887;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.BackColor = System.Drawing.Color.White;
            this.xrTableCell112.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_PreviousBankAccounts.Banker")});
            this.xrTableCell112.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell112.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell112.StylePriority.UseBackColor = false;
            this.xrTableCell112.StylePriority.UseBorderColor = false;
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UseFont = false;
            this.xrTableCell112.StylePriority.UseForeColor = false;
            this.xrTableCell112.StylePriority.UsePadding = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "xrTableCell112";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell112.Weight = 0.40178570422259241;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.BackColor = System.Drawing.Color.White;
            this.xrTableCell113.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell113.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell113.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_PreviousBankAccounts.BranchCode")});
            this.xrTableCell113.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell113.StylePriority.UseBackColor = false;
            this.xrTableCell113.StylePriority.UseBorderColor = false;
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.StylePriority.UseForeColor = false;
            this.xrTableCell113.StylePriority.UsePadding = false;
            this.xrTableCell113.Text = "xrTableCell113";
            this.xrTableCell113.Weight = 0.4017857855016535;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.BackColor = System.Drawing.Color.White;
            this.xrTableCell114.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_PreviousBankAccounts.AccountHolder")});
            this.xrTableCell114.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell114.StylePriority.UseBackColor = false;
            this.xrTableCell114.StylePriority.UseBorderColor = false;
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseForeColor = false;
            this.xrTableCell114.StylePriority.UsePadding = false;
            this.xrTableCell114.Text = "xrTableCell114";
            this.xrTableCell114.Weight = 0.40178562294353137;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.BackColor = System.Drawing.Color.White;
            this.xrTableCell115.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_PreviousBankAccounts.AccountNumber")});
            this.xrTableCell115.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell115.StylePriority.UseBackColor = false;
            this.xrTableCell115.StylePriority.UseBorderColor = false;
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseForeColor = false;
            this.xrTableCell115.StylePriority.UsePadding = false;
            this.xrTableCell115.Text = "[Report_PreviousBankAccounts.AccountNumber]";
            this.xrTableCell115.Weight = 0.40178589387373487;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.BackColor = System.Drawing.Color.White;
            this.xrTableCell116.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell116.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_PreviousBankAccounts.DateOpened")});
            this.xrTableCell116.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell116.StylePriority.UseBackColor = false;
            this.xrTableCell116.StylePriority.UseBorderColor = false;
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.StylePriority.UseForeColor = false;
            this.xrTableCell116.StylePriority.UsePadding = false;
            this.xrTableCell116.Text = "xrTableCell116";
            this.xrTableCell116.Weight = 0.686890650879253;
            // 
            // LblYearsWithBankD
            // 
            this.LblYearsWithBankD.BackColor = System.Drawing.Color.White;
            this.LblYearsWithBankD.BorderColor = System.Drawing.Color.Gainsboro;
            this.LblYearsWithBankD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.LblYearsWithBankD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_PreviousBankAccounts.YearsWithBank")});
            this.LblYearsWithBankD.ForeColor = System.Drawing.Color.DimGray;
            this.LblYearsWithBankD.Name = "LblYearsWithBankD";
            this.LblYearsWithBankD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.LblYearsWithBankD.StylePriority.UseBackColor = false;
            this.LblYearsWithBankD.StylePriority.UseBorderColor = false;
            this.LblYearsWithBankD.StylePriority.UseBorders = false;
            this.LblYearsWithBankD.StylePriority.UseForeColor = false;
            this.LblYearsWithBankD.StylePriority.UsePadding = false;
            this.LblYearsWithBankD.Text = "LblYearsWithBankD";
            this.LblYearsWithBankD.Weight = 0.51846634257923474;
            // 
            // GroupHeader35
            // 
            this.GroupHeader35.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable37});
            this.GroupHeader35.Expanded = false;
            this.GroupHeader35.HeightF = 50.28571F;
            this.GroupHeader35.Name = "GroupHeader35";
            // 
            // xrTable37
            // 
            this.xrTable37.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable37.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow144,
            this.xrTableRow165});
            this.xrTable37.SizeF = new System.Drawing.SizeF(792F, 40.28571F);
            this.xrTable37.StylePriority.UseBorderColor = false;
            this.xrTable37.StylePriority.UseBorders = false;
            this.xrTable37.StylePriority.UseFont = false;
            // 
            // xrTableRow144
            // 
            this.xrTableRow144.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow144.Name = "xrTableRow144";
            this.xrTableRow144.Weight = 0.834319526627219;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell3.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.xrTableCell3.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseBorderColor = false;
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.Text = "Previous Bank Account";
            this.xrTableCell3.Weight = 3;
            // 
            // xrTableRow165
            // 
            this.xrTableRow165.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131});
            this.xrTableRow165.Name = "xrTableRow165";
            this.xrTableRow165.Weight = 0.83431952662721887;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell126.BorderColor = System.Drawing.Color.White;
            this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell126.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell126.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell126.StylePriority.UseBackColor = false;
            this.xrTableCell126.StylePriority.UseBorderColor = false;
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.StylePriority.UseFont = false;
            this.xrTableCell126.StylePriority.UseForeColor = false;
            this.xrTableCell126.StylePriority.UsePadding = false;
            this.xrTableCell126.StylePriority.UseTextAlignment = false;
            this.xrTableCell126.Text = "Banker";
            this.xrTableCell126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell126.Weight = 0.375;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell127.BorderColor = System.Drawing.Color.White;
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell127.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell127.StylePriority.UseBackColor = false;
            this.xrTableCell127.StylePriority.UseBorderColor = false;
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.StylePriority.UseFont = false;
            this.xrTableCell127.StylePriority.UseForeColor = false;
            this.xrTableCell127.StylePriority.UsePadding = false;
            this.xrTableCell127.Text = "Branch Code";
            this.xrTableCell127.Weight = 0.375;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell128.BorderColor = System.Drawing.Color.White;
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell128.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell128.StylePriority.UseBackColor = false;
            this.xrTableCell128.StylePriority.UseBorderColor = false;
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.StylePriority.UseFont = false;
            this.xrTableCell128.StylePriority.UseForeColor = false;
            this.xrTableCell128.StylePriority.UsePadding = false;
            this.xrTableCell128.Text = "Account Holder";
            this.xrTableCell128.Weight = 0.375;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell129.BorderColor = System.Drawing.Color.White;
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell129.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell129.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell129.StylePriority.UseBackColor = false;
            this.xrTableCell129.StylePriority.UseBorderColor = false;
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.StylePriority.UseFont = false;
            this.xrTableCell129.StylePriority.UseForeColor = false;
            this.xrTableCell129.StylePriority.UsePadding = false;
            this.xrTableCell129.Text = "AccountNumber";
            this.xrTableCell129.Weight = 0.375;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell130.BorderColor = System.Drawing.Color.White;
            this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell130.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell130.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell130.StylePriority.UseBackColor = false;
            this.xrTableCell130.StylePriority.UseBorderColor = false;
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.StylePriority.UseFont = false;
            this.xrTableCell130.StylePriority.UseForeColor = false;
            this.xrTableCell130.StylePriority.UsePadding = false;
            this.xrTableCell130.Text = "Date Opened";
            this.xrTableCell130.Weight = 0.64109832951516821;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell131.BorderColor = System.Drawing.Color.White;
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell131.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell131.StylePriority.UseBackColor = false;
            this.xrTableCell131.StylePriority.UseBorderColor = false;
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.StylePriority.UseFont = false;
            this.xrTableCell131.StylePriority.UseForeColor = false;
            this.xrTableCell131.StylePriority.UsePadding = false;
            this.xrTableCell131.Text = "Years With Bank";
            this.xrTableCell131.Weight = 0.48390167048483185;
            // 
            // DRXDSPrincipals
            // 
            this.DRXDSPrincipals.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12,
            this.ReportHeader1});
            this.DRXDSPrincipals.DataMember = "XDSCommercialPrincipalInformation";
            this.DRXDSPrincipals.Level = 13;
            this.DRXDSPrincipals.Name = "DRXDSPrincipals";
            this.DRXDSPrincipals.Visible = false;
            // 
            // Detail12
            // 
            this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable44,
            this.xrTable43});
            this.Detail12.Expanded = false;
            this.Detail12.HeightF = 324.0835F;
            this.Detail12.Name = "Detail12";
            // 
            // xrTable44
            // 
            this.xrTable44.LocationFloat = new DevExpress.Utils.PointFloat(15.21021F, 60.08352F);
            this.xrTable44.Name = "xrTable44";
            this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow186,
            this.xrTableRow187,
            this.xrTableRow188,
            this.xrTableRow189,
            this.xrTableRow190,
            this.xrTableRow191,
            this.xrTableRow192,
            this.xrTableRow193,
            this.xrTableRow194,
            this.xrTableRow195,
            this.xrTableRow196,
            this.xrTableRow197});
            this.xrTable44.SizeF = new System.Drawing.SizeF(787.4999F, 264F);
            // 
            // xrTableRow186
            // 
            this.xrTableRow186.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159});
            this.xrTableRow186.Name = "xrTableRow186";
            this.xrTableRow186.Weight = 1;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell156.BorderColor = System.Drawing.Color.White;
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell156.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell156.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell156.StylePriority.UseBackColor = false;
            this.xrTableCell156.StylePriority.UseBorderColor = false;
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseForeColor = false;
            this.xrTableCell156.StylePriority.UsePadding = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.Text = "ID Number";
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell156.Weight = 1.4802087393664964;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.BackColor = System.Drawing.Color.White;
            this.xrTableCell157.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell157.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell157.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.IDNumber")});
            this.xrTableCell157.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell157.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell157.StylePriority.UseBackColor = false;
            this.xrTableCell157.StylePriority.UseBorderColor = false;
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.StylePriority.UseFont = false;
            this.xrTableCell157.StylePriority.UseForeColor = false;
            this.xrTableCell157.StylePriority.UsePadding = false;
            this.xrTableCell157.StylePriority.UseTextAlignment = false;
            this.xrTableCell157.Text = "lblDIDNoD";
            this.xrTableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell157.Weight = 2.4888652844497781;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell158.BorderColor = System.Drawing.Color.White;
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell158.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell158.StylePriority.UseBackColor = false;
            this.xrTableCell158.StylePriority.UseBorderColor = false;
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.StylePriority.UseFont = false;
            this.xrTableCell158.StylePriority.UseForeColor = false;
            this.xrTableCell158.StylePriority.UsePadding = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.Text = "Principal Type";
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell158.Weight = 1.5149970005912281;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.BackColor = System.Drawing.Color.White;
            this.xrTableCell159.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell159.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell159.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell159.StylePriority.UseBackColor = false;
            this.xrTableCell159.StylePriority.UseBorderColor = false;
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.StylePriority.UseFont = false;
            this.xrTableCell159.StylePriority.UseForeColor = false;
            this.xrTableCell159.StylePriority.UsePadding = false;
            this.xrTableCell159.StylePriority.UseTextAlignment = false;
            this.xrTableCell159.Text = "lblDDirIndD";
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell159.Weight = 2.4456158610023828;
            // 
            // xrTableRow187
            // 
            this.xrTableRow187.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.xrTableCell163});
            this.xrTableRow187.Name = "xrTableRow187";
            this.xrTableRow187.Weight = 1;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell160.BorderColor = System.Drawing.Color.White;
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell160.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell160.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell160.StylePriority.UseBackColor = false;
            this.xrTableCell160.StylePriority.UseBorderColor = false;
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.StylePriority.UseFont = false;
            this.xrTableCell160.StylePriority.UseForeColor = false;
            this.xrTableCell160.StylePriority.UsePadding = false;
            this.xrTableCell160.Text = "Initials";
            this.xrTableCell160.Weight = 1.4802087393664964;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.BackColor = System.Drawing.Color.White;
            this.xrTableCell161.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell161.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell161.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell161.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell161.StylePriority.UseBackColor = false;
            this.xrTableCell161.StylePriority.UseBorderColor = false;
            this.xrTableCell161.StylePriority.UseBorders = false;
            this.xrTableCell161.StylePriority.UseFont = false;
            this.xrTableCell161.StylePriority.UseForeColor = false;
            this.xrTableCell161.StylePriority.UsePadding = false;
            this.xrTableCell161.Text = "lblDinitialsD";
            this.xrTableCell161.Weight = 2.4888652844497781;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell162.BorderColor = System.Drawing.Color.White;
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell162.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell162.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell162.StylePriority.UseBackColor = false;
            this.xrTableCell162.StylePriority.UseBorderColor = false;
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.StylePriority.UseFont = false;
            this.xrTableCell162.StylePriority.UseForeColor = false;
            this.xrTableCell162.StylePriority.UsePadding = false;
            this.xrTableCell162.Text = "Appointment Date";
            this.xrTableCell162.Weight = 1.5149970005912281;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.BackColor = System.Drawing.Color.White;
            this.xrTableCell163.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell163.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell163.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_ActivePrincipals.AppointmentDate")});
            this.xrTableCell163.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell163.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell163.StylePriority.UseBackColor = false;
            this.xrTableCell163.StylePriority.UseBorderColor = false;
            this.xrTableCell163.StylePriority.UseBorders = false;
            this.xrTableCell163.StylePriority.UseFont = false;
            this.xrTableCell163.StylePriority.UseForeColor = false;
            this.xrTableCell163.StylePriority.UsePadding = false;
            this.xrTableCell163.Text = "lblDAppDateD";
            this.xrTableCell163.Weight = 2.4456158610023828;
            // 
            // xrTableRow188
            // 
            this.xrTableRow188.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell167});
            this.xrTableRow188.Name = "xrTableRow188";
            this.xrTableRow188.Weight = 1;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell164.BorderColor = System.Drawing.Color.White;
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell164.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell164.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell164.StylePriority.UseBackColor = false;
            this.xrTableCell164.StylePriority.UseBorderColor = false;
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.StylePriority.UseFont = false;
            this.xrTableCell164.StylePriority.UseForeColor = false;
            this.xrTableCell164.StylePriority.UsePadding = false;
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.Text = "First Name";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell164.Weight = 1.4802087393664964;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.BackColor = System.Drawing.Color.White;
            this.xrTableCell165.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell165.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.FirstName")});
            this.xrTableCell165.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell165.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell165.StylePriority.UseBackColor = false;
            this.xrTableCell165.StylePriority.UseBorderColor = false;
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.StylePriority.UseFont = false;
            this.xrTableCell165.StylePriority.UseForeColor = false;
            this.xrTableCell165.StylePriority.UsePadding = false;
            this.xrTableCell165.StylePriority.UseTextAlignment = false;
            this.xrTableCell165.Text = "lblDFNameD";
            this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell165.Weight = 2.4888652844497781;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell166.BorderColor = System.Drawing.Color.White;
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell166.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell166.StylePriority.UseBackColor = false;
            this.xrTableCell166.StylePriority.UseBorderColor = false;
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.StylePriority.UseFont = false;
            this.xrTableCell166.StylePriority.UseForeColor = false;
            this.xrTableCell166.StylePriority.UsePadding = false;
            this.xrTableCell166.StylePriority.UseTextAlignment = false;
            this.xrTableCell166.Text = "Designation";
            this.xrTableCell166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell166.Weight = 1.5149970005912281;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.BackColor = System.Drawing.Color.White;
            this.xrTableCell167.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell167.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.Designation")});
            this.xrTableCell167.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell167.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell167.StylePriority.UseBackColor = false;
            this.xrTableCell167.StylePriority.UseBorderColor = false;
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.StylePriority.UseFont = false;
            this.xrTableCell167.StylePriority.UseForeColor = false;
            this.xrTableCell167.StylePriority.UsePadding = false;
            this.xrTableCell167.StylePriority.UseTextAlignment = false;
            this.xrTableCell167.Text = "lblDDesignationD";
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell167.Weight = 2.4456158610023828;
            // 
            // xrTableRow189
            // 
            this.xrTableRow189.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171});
            this.xrTableRow189.Name = "xrTableRow189";
            this.xrTableRow189.Weight = 1;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell168.BorderColor = System.Drawing.Color.White;
            this.xrTableCell168.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell168.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell168.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell168.StylePriority.UseBackColor = false;
            this.xrTableCell168.StylePriority.UseBorderColor = false;
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.StylePriority.UseFont = false;
            this.xrTableCell168.StylePriority.UseForeColor = false;
            this.xrTableCell168.StylePriority.UsePadding = false;
            this.xrTableCell168.StylePriority.UseTextAlignment = false;
            this.xrTableCell168.Text = "Second Name";
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell168.Weight = 1.4802087393664964;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.BackColor = System.Drawing.Color.White;
            this.xrTableCell169.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.OtherName")});
            this.xrTableCell169.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell169.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell169.StylePriority.UseBackColor = false;
            this.xrTableCell169.StylePriority.UseBorderColor = false;
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.StylePriority.UseFont = false;
            this.xrTableCell169.StylePriority.UseForeColor = false;
            this.xrTableCell169.StylePriority.UsePadding = false;
            this.xrTableCell169.StylePriority.UseTextAlignment = false;
            this.xrTableCell169.Text = "lblDSecondnameD";
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell169.Weight = 2.4888652844497781;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell170.BorderColor = System.Drawing.Color.White;
            this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell170.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell170.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell170.StylePriority.UseBackColor = false;
            this.xrTableCell170.StylePriority.UseBorderColor = false;
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.StylePriority.UseFont = false;
            this.xrTableCell170.StylePriority.UseForeColor = false;
            this.xrTableCell170.StylePriority.UsePadding = false;
            this.xrTableCell170.StylePriority.UseTextAlignment = false;
            this.xrTableCell170.Text = "Share Percentage";
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell170.Weight = 1.5149970005912281;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.BackColor = System.Drawing.Color.White;
            this.xrTableCell171.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell171.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.ShareHolding")});
            this.xrTableCell171.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell171.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell171.StylePriority.UseBackColor = false;
            this.xrTableCell171.StylePriority.UseBorderColor = false;
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.StylePriority.UseFont = false;
            this.xrTableCell171.StylePriority.UseForeColor = false;
            this.xrTableCell171.StylePriority.UsePadding = false;
            this.xrTableCell171.StylePriority.UseTextAlignment = false;
            this.xrTableCell171.Text = "lblDSharePercD";
            this.xrTableCell171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell171.Weight = 2.4456158610023828;
            // 
            // xrTableRow190
            // 
            this.xrTableRow190.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175});
            this.xrTableRow190.Name = "xrTableRow190";
            this.xrTableRow190.Weight = 1;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell172.BorderColor = System.Drawing.Color.White;
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell172.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell172.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell172.StylePriority.UseBackColor = false;
            this.xrTableCell172.StylePriority.UseBorderColor = false;
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.StylePriority.UseFont = false;
            this.xrTableCell172.StylePriority.UseForeColor = false;
            this.xrTableCell172.StylePriority.UsePadding = false;
            this.xrTableCell172.StylePriority.UseTextAlignment = false;
            this.xrTableCell172.Text = "Surname";
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell172.Weight = 1.4802087393664964;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.BackColor = System.Drawing.Color.White;
            this.xrTableCell173.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell173.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell173.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.Surname")});
            this.xrTableCell173.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell173.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell173.StylePriority.UseBackColor = false;
            this.xrTableCell173.StylePriority.UseBorderColor = false;
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.StylePriority.UseFont = false;
            this.xrTableCell173.StylePriority.UseForeColor = false;
            this.xrTableCell173.StylePriority.UsePadding = false;
            this.xrTableCell173.StylePriority.UseTextAlignment = false;
            this.xrTableCell173.Text = "lblDSurnameD";
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell173.Weight = 2.4888652844497781;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell174.BorderColor = System.Drawing.Color.White;
            this.xrTableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell174.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell174.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell174.StylePriority.UseBackColor = false;
            this.xrTableCell174.StylePriority.UseBorderColor = false;
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.StylePriority.UseFont = false;
            this.xrTableCell174.StylePriority.UseForeColor = false;
            this.xrTableCell174.StylePriority.UsePadding = false;
            this.xrTableCell174.StylePriority.UseTextAlignment = false;
            this.xrTableCell174.Text = "Member\'s Contribution";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell174.Weight = 1.5149970005912281;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.BackColor = System.Drawing.Color.White;
            this.xrTableCell175.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell175.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell175.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell175.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell175.StylePriority.UseBackColor = false;
            this.xrTableCell175.StylePriority.UseBorderColor = false;
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.StylePriority.UseFont = false;
            this.xrTableCell175.StylePriority.UseForeColor = false;
            this.xrTableCell175.StylePriority.UsePadding = false;
            this.xrTableCell175.StylePriority.UseTextAlignment = false;
            this.xrTableCell175.Text = "lblDMemContributionD";
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell175.Weight = 2.4456158610023828;
            // 
            // xrTableRow191
            // 
            this.xrTableRow191.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell180});
            this.xrTableRow191.Name = "xrTableRow191";
            this.xrTableRow191.Weight = 1;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell176.BorderColor = System.Drawing.Color.White;
            this.xrTableCell176.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell176.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell176.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell176.StylePriority.UseBackColor = false;
            this.xrTableCell176.StylePriority.UseBorderColor = false;
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.StylePriority.UseFont = false;
            this.xrTableCell176.StylePriority.UseForeColor = false;
            this.xrTableCell176.StylePriority.UsePadding = false;
            this.xrTableCell176.StylePriority.UseTextAlignment = false;
            this.xrTableCell176.Text = "CM 29 Date";
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell176.Weight = 1.4802087473592085;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.BackColor = System.Drawing.Color.White;
            this.xrTableCell177.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell177.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell177.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell177.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell177.StylePriority.UseBackColor = false;
            this.xrTableCell177.StylePriority.UseBorderColor = false;
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.StylePriority.UseFont = false;
            this.xrTableCell177.StylePriority.UseForeColor = false;
            this.xrTableCell177.StylePriority.UsePadding = false;
            this.xrTableCell177.StylePriority.UseTextAlignment = false;
            this.xrTableCell177.Text = "lblCM29DateD";
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell177.Weight = 2.4888487744544241;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell178.BorderColor = System.Drawing.Color.White;
            this.xrTableCell178.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell178.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell178.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell178.StylePriority.UseBackColor = false;
            this.xrTableCell178.StylePriority.UseBorderColor = false;
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.StylePriority.UseFont = false;
            this.xrTableCell178.StylePriority.UseForeColor = false;
            this.xrTableCell178.StylePriority.UsePadding = false;
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.Text = "Status";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell178.Weight = 1.5150139141517436;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.BackColor = System.Drawing.Color.White;
            this.xrTableCell180.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell180.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.Status")});
            this.xrTableCell180.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell180.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell180.StylePriority.UseBackColor = false;
            this.xrTableCell180.StylePriority.UseBorderColor = false;
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.StylePriority.UseFont = false;
            this.xrTableCell180.StylePriority.UseForeColor = false;
            this.xrTableCell180.StylePriority.UsePadding = false;
            this.xrTableCell180.StylePriority.UseTextAlignment = false;
            this.xrTableCell180.Text = "lblDStatusD";
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell180.Weight = 2.4456154494445097;
            // 
            // xrTableRow192
            // 
            this.xrTableRow192.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185});
            this.xrTableRow192.Name = "xrTableRow192";
            this.xrTableRow192.Weight = 1;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell182.BorderColor = System.Drawing.Color.White;
            this.xrTableCell182.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell182.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell182.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell182.StylePriority.UseBackColor = false;
            this.xrTableCell182.StylePriority.UseBorderColor = false;
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseForeColor = false;
            this.xrTableCell182.StylePriority.UsePadding = false;
            this.xrTableCell182.StylePriority.UseTextAlignment = false;
            this.xrTableCell182.Text = "South African Resident";
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell182.Weight = 1.4802087473592085;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.BackColor = System.Drawing.Color.White;
            this.xrTableCell183.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell183.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell183.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell183.StylePriority.UseBackColor = false;
            this.xrTableCell183.StylePriority.UseBorderColor = false;
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.StylePriority.UseFont = false;
            this.xrTableCell183.StylePriority.UseForeColor = false;
            this.xrTableCell183.StylePriority.UsePadding = false;
            this.xrTableCell183.StylePriority.UseTextAlignment = false;
            this.xrTableCell183.Text = "lblISRSAD";
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell183.Weight = 2.4888487744544241;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell184.BorderColor = System.Drawing.Color.White;
            this.xrTableCell184.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell184.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell184.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell184.StylePriority.UseBackColor = false;
            this.xrTableCell184.StylePriority.UseBorderColor = false;
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.StylePriority.UseFont = false;
            this.xrTableCell184.StylePriority.UseForeColor = false;
            this.xrTableCell184.StylePriority.UsePadding = false;
            this.xrTableCell184.StylePriority.UseTextAlignment = false;
            this.xrTableCell184.Text = "Country Of Residence";
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell184.Weight = 1.5150139141517436;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.BackColor = System.Drawing.Color.White;
            this.xrTableCell185.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell185.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.CountryCode")});
            this.xrTableCell185.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell185.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell185.StylePriority.UseBackColor = false;
            this.xrTableCell185.StylePriority.UseBorderColor = false;
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.StylePriority.UseFont = false;
            this.xrTableCell185.StylePriority.UseForeColor = false;
            this.xrTableCell185.StylePriority.UsePadding = false;
            this.xrTableCell185.StylePriority.UseTextAlignment = false;
            this.xrTableCell185.Text = "lblCountryD";
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell185.Weight = 2.4456154494445097;
            // 
            // xrTableRow193
            // 
            this.xrTableRow193.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189});
            this.xrTableRow193.Name = "xrTableRow193";
            this.xrTableRow193.Weight = 1;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell186.BorderColor = System.Drawing.Color.White;
            this.xrTableCell186.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell186.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell186.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell186.StylePriority.UseBackColor = false;
            this.xrTableCell186.StylePriority.UseBorderColor = false;
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.StylePriority.UseFont = false;
            this.xrTableCell186.StylePriority.UseForeColor = false;
            this.xrTableCell186.StylePriority.UsePadding = false;
            this.xrTableCell186.StylePriority.UseTextAlignment = false;
            this.xrTableCell186.Text = "ID Verification";
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell186.Weight = 1.4802087473592085;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.BackColor = System.Drawing.Color.White;
            this.xrTableCell187.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell187.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell187.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.VerifiedIDNumber")});
            this.xrTableCell187.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell187.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell187.StylePriority.UseBackColor = false;
            this.xrTableCell187.StylePriority.UseBorderColor = false;
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.StylePriority.UseFont = false;
            this.xrTableCell187.StylePriority.UseForeColor = false;
            this.xrTableCell187.StylePriority.UsePadding = false;
            this.xrTableCell187.StylePriority.UseTextAlignment = false;
            this.xrTableCell187.Text = "lblIDVerificationD";
            this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell187.Weight = 2.4888487744544241;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell188.BorderColor = System.Drawing.Color.White;
            this.xrTableCell188.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell188.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell188.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell188.StylePriority.UseBackColor = false;
            this.xrTableCell188.StylePriority.UseBorderColor = false;
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseForeColor = false;
            this.xrTableCell188.StylePriority.UsePadding = false;
            this.xrTableCell188.StylePriority.UseTextAlignment = false;
            this.xrTableCell188.Text = "Confirmed By CIPRO";
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell188.Weight = 1.5150139141517436;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.BackColor = System.Drawing.Color.White;
            this.xrTableCell189.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell189.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell189.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInformation.ISCIPROConfirmed")});
            this.xrTableCell189.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell189.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell189.StylePriority.UseBackColor = false;
            this.xrTableCell189.StylePriority.UseBorderColor = false;
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.StylePriority.UseFont = false;
            this.xrTableCell189.StylePriority.UseForeColor = false;
            this.xrTableCell189.StylePriority.UsePadding = false;
            this.xrTableCell189.StylePriority.UseTextAlignment = false;
            this.xrTableCell189.Text = "lblCIPROConfirmedD";
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell189.Weight = 2.4456154494445097;
            // 
            // xrTableRow194
            // 
            this.xrTableRow194.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193});
            this.xrTableRow194.Name = "xrTableRow194";
            this.xrTableRow194.Weight = 1;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell190.BorderColor = System.Drawing.Color.White;
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell190.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell190.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell190.StylePriority.UseBackColor = false;
            this.xrTableCell190.StylePriority.UseBorderColor = false;
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.StylePriority.UseFont = false;
            this.xrTableCell190.StylePriority.UseForeColor = false;
            this.xrTableCell190.StylePriority.UsePadding = false;
            this.xrTableCell190.StylePriority.UseTextAlignment = false;
            this.xrTableCell190.Text = "Home Address";
            this.xrTableCell190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell190.Weight = 1.4802087473592085;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.BackColor = System.Drawing.Color.White;
            this.xrTableCell191.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell191.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell191.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.PhysicalAddress")});
            this.xrTableCell191.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell191.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell191.StylePriority.UseBackColor = false;
            this.xrTableCell191.StylePriority.UseBorderColor = false;
            this.xrTableCell191.StylePriority.UseBorders = false;
            this.xrTableCell191.StylePriority.UseFont = false;
            this.xrTableCell191.StylePriority.UseForeColor = false;
            this.xrTableCell191.StylePriority.UsePadding = false;
            this.xrTableCell191.StylePriority.UseTextAlignment = false;
            this.xrTableCell191.Text = "lblHAddD";
            this.xrTableCell191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell191.Weight = 2.4888487744544241;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell192.BorderColor = System.Drawing.Color.White;
            this.xrTableCell192.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell192.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell192.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell192.StylePriority.UseBackColor = false;
            this.xrTableCell192.StylePriority.UseBorderColor = false;
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.StylePriority.UseFont = false;
            this.xrTableCell192.StylePriority.UseForeColor = false;
            this.xrTableCell192.StylePriority.UsePadding = false;
            this.xrTableCell192.StylePriority.UseTextAlignment = false;
            this.xrTableCell192.Text = "Postal Address";
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell192.Weight = 1.5150139141517436;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.BackColor = System.Drawing.Color.White;
            this.xrTableCell193.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell193.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell193.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.PostalAddress")});
            this.xrTableCell193.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell193.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell193.StylePriority.UseBackColor = false;
            this.xrTableCell193.StylePriority.UseBorderColor = false;
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.StylePriority.UseForeColor = false;
            this.xrTableCell193.StylePriority.UsePadding = false;
            this.xrTableCell193.StylePriority.UseTextAlignment = false;
            this.xrTableCell193.Text = "lblPAddD";
            this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell193.Weight = 2.4456154494445097;
            // 
            // xrTableRow195
            // 
            this.xrTableRow195.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197});
            this.xrTableRow195.Name = "xrTableRow195";
            this.xrTableRow195.Weight = 1;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell194.BorderColor = System.Drawing.Color.White;
            this.xrTableCell194.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell194.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell194.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell194.StylePriority.UseBackColor = false;
            this.xrTableCell194.StylePriority.UseBorderColor = false;
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.StylePriority.UseFont = false;
            this.xrTableCell194.StylePriority.UseForeColor = false;
            this.xrTableCell194.StylePriority.UsePadding = false;
            this.xrTableCell194.StylePriority.UseTextAlignment = false;
            this.xrTableCell194.Text = "Telephone No. (H)";
            this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell194.Weight = 1.4802087473592085;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.BackColor = System.Drawing.Color.White;
            this.xrTableCell195.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell195.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.CurrentTelNumber")});
            this.xrTableCell195.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell195.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell195.StylePriority.UseBackColor = false;
            this.xrTableCell195.StylePriority.UseBorderColor = false;
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.StylePriority.UseFont = false;
            this.xrTableCell195.StylePriority.UseForeColor = false;
            this.xrTableCell195.StylePriority.UsePadding = false;
            this.xrTableCell195.StylePriority.UseTextAlignment = false;
            this.xrTableCell195.Text = "lblHomeTelNoD";
            this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell195.Weight = 2.4888487744544241;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell196.BorderColor = System.Drawing.Color.White;
            this.xrTableCell196.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell196.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell196.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell196.StylePriority.UseBackColor = false;
            this.xrTableCell196.StylePriority.UseBorderColor = false;
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.StylePriority.UseFont = false;
            this.xrTableCell196.StylePriority.UseForeColor = false;
            this.xrTableCell196.StylePriority.UsePadding = false;
            this.xrTableCell196.StylePriority.UseTextAlignment = false;
            this.xrTableCell196.Text = "Telephone No. (W)";
            this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell196.Weight = 1.5150139141517436;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.BackColor = System.Drawing.Color.White;
            this.xrTableCell197.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell197.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell197.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell197.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell197.StylePriority.UseBackColor = false;
            this.xrTableCell197.StylePriority.UseBorderColor = false;
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.StylePriority.UseFont = false;
            this.xrTableCell197.StylePriority.UseForeColor = false;
            this.xrTableCell197.StylePriority.UsePadding = false;
            this.xrTableCell197.StylePriority.UseTextAlignment = false;
            this.xrTableCell197.Text = "lblWorkTelNoD";
            this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell197.Weight = 2.4456154494445097;
            // 
            // xrTableRow196
            // 
            this.xrTableRow196.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201});
            this.xrTableRow196.Name = "xrTableRow196";
            this.xrTableRow196.Weight = 1;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell198.BorderColor = System.Drawing.Color.White;
            this.xrTableCell198.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell198.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell198.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell198.StylePriority.UseBackColor = false;
            this.xrTableCell198.StylePriority.UseBorderColor = false;
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.StylePriority.UseFont = false;
            this.xrTableCell198.StylePriority.UseForeColor = false;
            this.xrTableCell198.StylePriority.UsePadding = false;
            this.xrTableCell198.StylePriority.UseTextAlignment = false;
            this.xrTableCell198.Text = "Cellular/Mobile";
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell198.Weight = 1.4802087473592085;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.BackColor = System.Drawing.Color.White;
            this.xrTableCell199.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell199.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.CellNumber")});
            this.xrTableCell199.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell199.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell199.StylePriority.UseBackColor = false;
            this.xrTableCell199.StylePriority.UseBorderColor = false;
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.StylePriority.UseFont = false;
            this.xrTableCell199.StylePriority.UseForeColor = false;
            this.xrTableCell199.StylePriority.UsePadding = false;
            this.xrTableCell199.StylePriority.UseTextAlignment = false;
            this.xrTableCell199.Text = "lblMobileNoD";
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell199.Weight = 2.4888487744544241;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell200.BorderColor = System.Drawing.Color.White;
            this.xrTableCell200.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell200.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell200.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell200.StylePriority.UseBackColor = false;
            this.xrTableCell200.StylePriority.UseBorderColor = false;
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.StylePriority.UseFont = false;
            this.xrTableCell200.StylePriority.UseForeColor = false;
            this.xrTableCell200.StylePriority.UsePadding = false;
            this.xrTableCell200.StylePriority.UseTextAlignment = false;
            this.xrTableCell200.Text = "E-mail Address";
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell200.Weight = 1.5150139141517436;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.BackColor = System.Drawing.Color.White;
            this.xrTableCell201.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell201.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell201.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell201.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell201.StylePriority.UseBackColor = false;
            this.xrTableCell201.StylePriority.UseBorderColor = false;
            this.xrTableCell201.StylePriority.UseBorders = false;
            this.xrTableCell201.StylePriority.UseFont = false;
            this.xrTableCell201.StylePriority.UseForeColor = false;
            this.xrTableCell201.StylePriority.UsePadding = false;
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.Text = "lblEmailAddressD";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell201.Weight = 2.4456154494445097;
            // 
            // xrTableRow197
            // 
            this.xrTableRow197.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205});
            this.xrTableRow197.Name = "xrTableRow197";
            this.xrTableRow197.Weight = 1;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell202.BorderColor = System.Drawing.Color.White;
            this.xrTableCell202.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell202.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell202.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell202.StylePriority.UseBackColor = false;
            this.xrTableCell202.StylePriority.UseBorderColor = false;
            this.xrTableCell202.StylePriority.UseBorders = false;
            this.xrTableCell202.StylePriority.UseFont = false;
            this.xrTableCell202.StylePriority.UseForeColor = false;
            this.xrTableCell202.StylePriority.UsePadding = false;
            this.xrTableCell202.Text = "Age";
            this.xrTableCell202.Weight = 1.4802087473592085;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.BackColor = System.Drawing.Color.White;
            this.xrTableCell203.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell203.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipal.CurrentAge")});
            this.xrTableCell203.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell203.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell203.StylePriority.UseBackColor = false;
            this.xrTableCell203.StylePriority.UseBorderColor = false;
            this.xrTableCell203.StylePriority.UseFont = false;
            this.xrTableCell203.StylePriority.UseForeColor = false;
            this.xrTableCell203.StylePriority.UsePadding = false;
            this.xrTableCell203.Text = "lblAgeD";
            this.xrTableCell203.Weight = 2.4888487744544241;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell204.BorderColor = System.Drawing.Color.White;
            this.xrTableCell204.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell204.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell204.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell204.StylePriority.UseBackColor = false;
            this.xrTableCell204.StylePriority.UseBorderColor = false;
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.StylePriority.UseFont = false;
            this.xrTableCell204.StylePriority.UseForeColor = false;
            this.xrTableCell204.StylePriority.UsePadding = false;
            this.xrTableCell204.Text = "Years With Business";
            this.xrTableCell204.Weight = 1.5150139141517436;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.BackColor = System.Drawing.Color.White;
            this.xrTableCell205.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell205.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell205.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell205.StylePriority.UseBackColor = false;
            this.xrTableCell205.StylePriority.UseBorderColor = false;
            this.xrTableCell205.StylePriority.UseFont = false;
            this.xrTableCell205.StylePriority.UseForeColor = false;
            this.xrTableCell205.StylePriority.UsePadding = false;
            this.xrTableCell205.Text = "lblYearswithBusD";
            this.xrTableCell205.Weight = 2.4456154494445097;
            // 
            // xrTable43
            // 
            this.xrTable43.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTable43.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow185});
            this.xrTable43.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.xrTable43.StylePriority.UseBorderColor = false;
            this.xrTable43.StylePriority.UsePadding = false;
            this.xrTable43.StylePriority.UseTextAlignment = false;
            this.xrTable43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow185
            // 
            this.xrTableRow185.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155});
            this.xrTableRow185.Name = "xrTableRow185";
            this.xrTableRow185.Weight = 1;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.BackColor = System.Drawing.Color.White;
            this.xrTableCell155.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTableCell155.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell155.BorderWidth = 2;
            this.xrTableCell155.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_ActivePrincipals.PrincipalName", "Personal Information - {0}")});
            this.xrTableCell155.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell155.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBackColor = false;
            this.xrTableCell155.StylePriority.UseBorderColor = false;
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.StylePriority.UseBorderWidth = false;
            this.xrTableCell155.StylePriority.UseFont = false;
            this.xrTableCell155.StylePriority.UseForeColor = false;
            this.xrTableCell155.Text = "lblDPersonalInfo";
            this.xrTableCell155.Weight = 3;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable42});
            this.ReportHeader1.Expanded = false;
            this.ReportHeader1.HeightF = 60F;
            this.ReportHeader1.Name = "ReportHeader1";
            this.ReportHeader1.Visible = false;
            // 
            // xrTable42
            // 
            this.xrTable42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable42.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable42.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow184});
            this.xrTable42.SizeF = new System.Drawing.SizeF(792.0001F, 40.625F);
            this.xrTable42.StylePriority.UseBorders = false;
            this.xrTable42.StylePriority.UseFont = false;
            this.xrTable42.StylePriority.UseForeColor = false;
            this.xrTable42.StylePriority.UsePadding = false;
            this.xrTable42.StylePriority.UseTextAlignment = false;
            this.xrTable42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow184
            // 
            this.xrTableRow184.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154});
            this.xrTableRow184.Name = "xrTableRow184";
            this.xrTableRow184.Weight = 0.8;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.BackColor = System.Drawing.Color.White;
            this.xrTableCell153.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell153.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell153.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell153.StylePriority.UseBackColor = false;
            this.xrTableCell153.StylePriority.UseBorderColor = false;
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.StylePriority.UseFont = false;
            this.xrTableCell153.StylePriority.UseForeColor = false;
            this.xrTableCell153.StylePriority.UsePadding = false;
            this.xrTableCell153.Text = "XDS Principals Information";
            this.xrTableCell153.Weight = 1.1756499500551834;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.BackColor = System.Drawing.Color.White;
            this.xrTableCell154.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell154.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell154.BorderWidth = 1;
            this.xrTableCell154.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell154.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell154.StylePriority.UseBackColor = false;
            this.xrTableCell154.StylePriority.UseBorderColor = false;
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.StylePriority.UseBorderWidth = false;
            this.xrTableCell154.StylePriority.UseFont = false;
            this.xrTableCell154.StylePriority.UseForeColor = false;
            this.xrTableCell154.StylePriority.UsePadding = false;
            this.xrTableCell154.Weight = 1.8243500499448164;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader30});
            this.DetailReport3.Level = 0;
            this.DetailReport3.Name = "DetailReport3";
            this.DetailReport3.Visible = false;
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable52});
            this.Detail4.Expanded = false;
            this.Detail4.HeightF = 187F;
            this.Detail4.Name = "Detail4";
            // 
            // xrTable52
            // 
            this.xrTable52.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 12F);
            this.xrTable52.Name = "xrTable52";
            this.xrTable52.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow171,
            this.xrTableRow181,
            this.xrTableRow182,
            this.xrTableRow203,
            this.xrTableRow204,
            this.xrTableRow205,
            this.xrTableRow206});
            this.xrTable52.SizeF = new System.Drawing.SizeF(792.0001F, 175F);
            // 
            // xrTableRow171
            // 
            this.xrTableRow171.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell378,
            this.xrTableCell379});
            this.xrTableRow171.Name = "xrTableRow171";
            this.xrTableRow171.Weight = 1;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell376.BorderColor = System.Drawing.Color.White;
            this.xrTableCell376.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell376.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell376.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell376.StylePriority.UseBackColor = false;
            this.xrTableCell376.StylePriority.UseBorderColor = false;
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.StylePriority.UseFont = false;
            this.xrTableCell376.StylePriority.UseForeColor = false;
            this.xrTableCell376.StylePriority.UsePadding = false;
            this.xrTableCell376.StylePriority.UseTextAlignment = false;
            this.xrTableCell376.Text = "Debt Review #";
            this.xrTableCell376.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell376.Weight = 0.64772718427814646;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.BackColor = System.Drawing.Color.White;
            this.xrTableCell377.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell377.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell377.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseNumber")});
            this.xrTableCell377.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell377.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell377.StylePriority.UseBackColor = false;
            this.xrTableCell377.StylePriority.UseBorderColor = false;
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.StylePriority.UseFont = false;
            this.xrTableCell377.StylePriority.UseForeColor = false;
            this.xrTableCell377.StylePriority.UsePadding = false;
            this.xrTableCell377.Text = "lblposjudgCaseNoD";
            this.xrTableCell377.Weight = 0.84090936326234111;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell378.BorderColor = System.Drawing.Color.White;
            this.xrTableCell378.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell378.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell378.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell378.StylePriority.UseBackColor = false;
            this.xrTableCell378.StylePriority.UseBorderColor = false;
            this.xrTableCell378.StylePriority.UseBorders = false;
            this.xrTableCell378.StylePriority.UseFont = false;
            this.xrTableCell378.StylePriority.UseForeColor = false;
            this.xrTableCell378.StylePriority.UsePadding = false;
            this.xrTableCell378.StylePriority.UseTextAlignment = false;
            this.xrTableCell378.Text = "Issue Date";
            this.xrTableCell378.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell378.Weight = 0.45296683818417655;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.BackColor = System.Drawing.Color.White;
            this.xrTableCell379.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell379.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell379.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseFilingDate")});
            this.xrTableCell379.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell379.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell379.StylePriority.UseBackColor = false;
            this.xrTableCell379.StylePriority.UseBorderColor = false;
            this.xrTableCell379.StylePriority.UseBorders = false;
            this.xrTableCell379.StylePriority.UseFont = false;
            this.xrTableCell379.StylePriority.UseForeColor = false;
            this.xrTableCell379.StylePriority.UsePadding = false;
            this.xrTableCell379.Text = "lblposjudgissueDateD";
            this.xrTableCell379.Weight = 1.0583966142753361;
            // 
            // xrTableRow181
            // 
            this.xrTableRow181.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell380,
            this.xrTableCell381});
            this.xrTableRow181.Name = "xrTableRow181";
            this.xrTableRow181.Weight = 1;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell380.BorderColor = System.Drawing.Color.White;
            this.xrTableCell380.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell380.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell380.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell380.StylePriority.UseBackColor = false;
            this.xrTableCell380.StylePriority.UseBorderColor = false;
            this.xrTableCell380.StylePriority.UseBorders = false;
            this.xrTableCell380.StylePriority.UseFont = false;
            this.xrTableCell380.StylePriority.UseForeColor = false;
            this.xrTableCell380.StylePriority.UsePadding = false;
            this.xrTableCell380.StylePriority.UseTextAlignment = false;
            this.xrTableCell380.Text = "Status";
            this.xrTableCell380.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell380.Weight = 0.64772718427814646;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.BackColor = System.Drawing.Color.White;
            this.xrTableCell381.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell381.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell381.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.CaseType")});
            this.xrTableCell381.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell381.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell381.StylePriority.UseBackColor = false;
            this.xrTableCell381.StylePriority.UseBorderColor = false;
            this.xrTableCell381.StylePriority.UseBorders = false;
            this.xrTableCell381.StylePriority.UseFont = false;
            this.xrTableCell381.StylePriority.UseForeColor = false;
            this.xrTableCell381.StylePriority.UsePadding = false;
            this.xrTableCell381.Text = "lblposjudgTypeD";
            this.xrTableCell381.Weight = 2.3522728157218538;
            // 
            // xrTableRow182
            // 
            this.xrTableRow182.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell384,
            this.xrTableCell385});
            this.xrTableRow182.Name = "xrTableRow182";
            this.xrTableRow182.Weight = 1;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell384.BorderColor = System.Drawing.Color.White;
            this.xrTableCell384.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell384.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell384.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell384.StylePriority.UseBackColor = false;
            this.xrTableCell384.StylePriority.UseBorderColor = false;
            this.xrTableCell384.StylePriority.UseBorders = false;
            this.xrTableCell384.StylePriority.UseFont = false;
            this.xrTableCell384.StylePriority.UseForeColor = false;
            this.xrTableCell384.StylePriority.UsePadding = false;
            this.xrTableCell384.StylePriority.UseTextAlignment = false;
            this.xrTableCell384.Text = "Debt Counsellor Name";
            this.xrTableCell384.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell384.Weight = 0.64772718427814646;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.BackColor = System.Drawing.Color.White;
            this.xrTableCell385.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell385.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell385.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.PlaintiffName")});
            this.xrTableCell385.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell385.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell385.StylePriority.UseBackColor = false;
            this.xrTableCell385.StylePriority.UseBorderColor = false;
            this.xrTableCell385.StylePriority.UseBorders = false;
            this.xrTableCell385.StylePriority.UseFont = false;
            this.xrTableCell385.StylePriority.UseForeColor = false;
            this.xrTableCell385.StylePriority.UsePadding = false;
            this.xrTableCell385.Text = "lblposjudgPlaintiffnameD";
            this.xrTableCell385.Weight = 2.3522728157218538;
            // 
            // xrTableRow203
            // 
            this.xrTableRow203.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell388,
            this.xrTableCell389,
            this.xrTableCell390,
            this.xrTableCell391});
            this.xrTableRow203.Name = "xrTableRow203";
            this.xrTableRow203.Weight = 1;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell388.BorderColor = System.Drawing.Color.White;
            this.xrTableCell388.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell388.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell388.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell388.StylePriority.UseBackColor = false;
            this.xrTableCell388.StylePriority.UseBorderColor = false;
            this.xrTableCell388.StylePriority.UseBorders = false;
            this.xrTableCell388.StylePriority.UseFont = false;
            this.xrTableCell388.StylePriority.UseForeColor = false;
            this.xrTableCell388.StylePriority.UsePadding = false;
            this.xrTableCell388.StylePriority.UseTextAlignment = false;
            this.xrTableCell388.Text = "Attorney Name";
            this.xrTableCell388.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell388.Weight = 0.64772718427814646;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.BackColor = System.Drawing.Color.White;
            this.xrTableCell389.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell389.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell389.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.AttorneyName")});
            this.xrTableCell389.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell389.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell389.StylePriority.UseBackColor = false;
            this.xrTableCell389.StylePriority.UseBorderColor = false;
            this.xrTableCell389.StylePriority.UseBorders = false;
            this.xrTableCell389.StylePriority.UseFont = false;
            this.xrTableCell389.StylePriority.UseForeColor = false;
            this.xrTableCell389.StylePriority.UsePadding = false;
            this.xrTableCell389.Text = "lblposjudgAttorneyNameD";
            this.xrTableCell389.Weight = 0.84090936326234111;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell390.BorderColor = System.Drawing.Color.White;
            this.xrTableCell390.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell390.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell390.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell390.StylePriority.UseBackColor = false;
            this.xrTableCell390.StylePriority.UseBorderColor = false;
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.StylePriority.UseFont = false;
            this.xrTableCell390.StylePriority.UseForeColor = false;
            this.xrTableCell390.StylePriority.UsePadding = false;
            this.xrTableCell390.StylePriority.UseTextAlignment = false;
            this.xrTableCell390.Text = "Attorney Phone No.";
            this.xrTableCell390.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell390.Weight = 0.45296683818417655;
            // 
            // xrTableCell391
            // 
            this.xrTableCell391.BackColor = System.Drawing.Color.White;
            this.xrTableCell391.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell391.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell391.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialPossibleJudgment.TelephoneNo")});
            this.xrTableCell391.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell391.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell391.Name = "xrTableCell391";
            this.xrTableCell391.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell391.StylePriority.UseBackColor = false;
            this.xrTableCell391.StylePriority.UseBorderColor = false;
            this.xrTableCell391.StylePriority.UseBorders = false;
            this.xrTableCell391.StylePriority.UseFont = false;
            this.xrTableCell391.StylePriority.UseForeColor = false;
            this.xrTableCell391.StylePriority.UsePadding = false;
            this.xrTableCell391.Text = "lblposjudgAttorneyPhoneNoD";
            this.xrTableCell391.Weight = 1.0583966142753361;
            // 
            // xrTableRow204
            // 
            this.xrTableRow204.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell392,
            this.xrTableCell393,
            this.xrTableCell394,
            this.xrTableCell395});
            this.xrTableRow204.Name = "xrTableRow204";
            this.xrTableRow204.Weight = 1;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell392.BorderColor = System.Drawing.Color.White;
            this.xrTableCell392.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell392.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell392.StylePriority.UseBackColor = false;
            this.xrTableCell392.StylePriority.UseBorderColor = false;
            this.xrTableCell392.StylePriority.UseFont = false;
            this.xrTableCell392.StylePriority.UseForeColor = false;
            this.xrTableCell392.StylePriority.UsePadding = false;
            this.xrTableCell392.Text = "Debt Counsellor Reg No";
            this.xrTableCell392.Weight = 0.64772718427814646;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.BackColor = System.Drawing.Color.White;
            this.xrTableCell393.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell393.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell393.StylePriority.UseBackColor = false;
            this.xrTableCell393.StylePriority.UseBorderColor = false;
            this.xrTableCell393.StylePriority.UseFont = false;
            this.xrTableCell393.StylePriority.UsePadding = false;
            this.xrTableCell393.Text = "xrTableCell367";
            this.xrTableCell393.Weight = 0.84090936326234111;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell394.BorderColor = System.Drawing.Color.White;
            this.xrTableCell394.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell394.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell394.StylePriority.UseBackColor = false;
            this.xrTableCell394.StylePriority.UseBorderColor = false;
            this.xrTableCell394.StylePriority.UseFont = false;
            this.xrTableCell394.StylePriority.UseForeColor = false;
            this.xrTableCell394.StylePriority.UsePadding = false;
            this.xrTableCell394.Text = "Surname";
            this.xrTableCell394.Weight = 0.45296683818417655;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.BackColor = System.Drawing.Color.White;
            this.xrTableCell395.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell395.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell395.StylePriority.UseBackColor = false;
            this.xrTableCell395.StylePriority.UseBorderColor = false;
            this.xrTableCell395.StylePriority.UseFont = false;
            this.xrTableCell395.StylePriority.UsePadding = false;
            this.xrTableCell395.Text = "xrTableCell369";
            this.xrTableCell395.Weight = 1.0583966142753361;
            // 
            // xrTableRow205
            // 
            this.xrTableRow205.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell396,
            this.xrTableCell397,
            this.xrTableCell398,
            this.xrTableCell399});
            this.xrTableRow205.Name = "xrTableRow205";
            this.xrTableRow205.Weight = 1;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell396.BorderColor = System.Drawing.Color.White;
            this.xrTableCell396.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell396.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell396.StylePriority.UseBackColor = false;
            this.xrTableCell396.StylePriority.UseBorderColor = false;
            this.xrTableCell396.StylePriority.UseFont = false;
            this.xrTableCell396.StylePriority.UseForeColor = false;
            this.xrTableCell396.StylePriority.UsePadding = false;
            this.xrTableCell396.Text = "Debt Counsellor Contact no";
            this.xrTableCell396.Weight = 0.64772718427814646;
            // 
            // xrTableCell397
            // 
            this.xrTableCell397.BackColor = System.Drawing.Color.White;
            this.xrTableCell397.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell397.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell397.Name = "xrTableCell397";
            this.xrTableCell397.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell397.StylePriority.UseBackColor = false;
            this.xrTableCell397.StylePriority.UseBorderColor = false;
            this.xrTableCell397.StylePriority.UseFont = false;
            this.xrTableCell397.StylePriority.UsePadding = false;
            this.xrTableCell397.Text = "xrTableCell371";
            this.xrTableCell397.Weight = 0.84090936326234111;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell398.BorderColor = System.Drawing.Color.White;
            this.xrTableCell398.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell398.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell398.StylePriority.UseBackColor = false;
            this.xrTableCell398.StylePriority.UseBorderColor = false;
            this.xrTableCell398.StylePriority.UseFont = false;
            this.xrTableCell398.StylePriority.UseForeColor = false;
            this.xrTableCell398.StylePriority.UsePadding = false;
            this.xrTableCell398.Text = "Case Reason";
            this.xrTableCell398.Weight = 0.45296683818417655;
            // 
            // xrTableCell399
            // 
            this.xrTableCell399.BackColor = System.Drawing.Color.White;
            this.xrTableCell399.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell399.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell399.Name = "xrTableCell399";
            this.xrTableCell399.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell399.StylePriority.UseBackColor = false;
            this.xrTableCell399.StylePriority.UseBorderColor = false;
            this.xrTableCell399.StylePriority.UseFont = false;
            this.xrTableCell399.StylePriority.UsePadding = false;
            this.xrTableCell399.Text = "xrTableCell373";
            this.xrTableCell399.Weight = 1.0583966142753361;
            // 
            // xrTableRow206
            // 
            this.xrTableRow206.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell400,
            this.xrTableCell401,
            this.xrTableCell402,
            this.xrTableCell403});
            this.xrTableRow206.Name = "xrTableRow206";
            this.xrTableRow206.Weight = 1;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell400.BorderColor = System.Drawing.Color.White;
            this.xrTableCell400.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell400.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell400.StylePriority.UseBackColor = false;
            this.xrTableCell400.StylePriority.UseBorderColor = false;
            this.xrTableCell400.StylePriority.UseFont = false;
            this.xrTableCell400.StylePriority.UseForeColor = false;
            this.xrTableCell400.StylePriority.UsePadding = false;
            this.xrTableCell400.Text = "Creation Date";
            this.xrTableCell400.Weight = 0.64772718427814646;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.BackColor = System.Drawing.Color.White;
            this.xrTableCell401.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell401.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell401.StylePriority.UseBackColor = false;
            this.xrTableCell401.StylePriority.UseBorderColor = false;
            this.xrTableCell401.StylePriority.UseFont = false;
            this.xrTableCell401.StylePriority.UsePadding = false;
            this.xrTableCell401.Text = "xrTableCell401";
            this.xrTableCell401.Weight = 0.84090936326234111;
            // 
            // xrTableCell402
            // 
            this.xrTableCell402.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell402.BorderColor = System.Drawing.Color.White;
            this.xrTableCell402.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell402.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell402.Name = "xrTableCell402";
            this.xrTableCell402.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell402.StylePriority.UseBackColor = false;
            this.xrTableCell402.StylePriority.UseBorderColor = false;
            this.xrTableCell402.StylePriority.UseFont = false;
            this.xrTableCell402.StylePriority.UseForeColor = false;
            this.xrTableCell402.StylePriority.UsePadding = false;
            this.xrTableCell402.Text = "Surname";
            this.xrTableCell402.Weight = 0.45296683818417655;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.BackColor = System.Drawing.Color.White;
            this.xrTableCell403.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell403.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell403.StylePriority.UseBackColor = false;
            this.xrTableCell403.StylePriority.UseBorderColor = false;
            this.xrTableCell403.StylePriority.UseFont = false;
            this.xrTableCell403.StylePriority.UsePadding = false;
            this.xrTableCell403.Text = "xrTableCell403";
            this.xrTableCell403.Weight = 1.0583966142753361;
            // 
            // GroupHeader30
            // 
            this.GroupHeader30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable51});
            this.GroupHeader30.Expanded = false;
            this.GroupHeader30.HeightF = 58F;
            this.GroupHeader30.Name = "GroupHeader30";
            // 
            // xrTable51
            // 
            this.xrTable51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable51.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable51.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable51.Name = "xrTable51";
            this.xrTable51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable51.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow61});
            this.xrTable51.SizeF = new System.Drawing.SizeF(792.0001F, 40.625F);
            this.xrTable51.StylePriority.UseBorders = false;
            this.xrTable51.StylePriority.UseFont = false;
            this.xrTable51.StylePriority.UseForeColor = false;
            this.xrTable51.StylePriority.UsePadding = false;
            this.xrTable51.StylePriority.UseTextAlignment = false;
            this.xrTable51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell374,
            this.xrTableCell375});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 0.8;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.BackColor = System.Drawing.Color.White;
            this.xrTableCell374.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell374.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell374.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell374.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell374.StylePriority.UseBackColor = false;
            this.xrTableCell374.StylePriority.UseBorderColor = false;
            this.xrTableCell374.StylePriority.UseBorders = false;
            this.xrTableCell374.StylePriority.UseFont = false;
            this.xrTableCell374.StylePriority.UseForeColor = false;
            this.xrTableCell374.StylePriority.UsePadding = false;
            this.xrTableCell374.Text = "Debt Review";
            this.xrTableCell374.Weight = 1.1756499500551834;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.BackColor = System.Drawing.Color.White;
            this.xrTableCell375.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell375.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell375.BorderWidth = 1;
            this.xrTableCell375.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell375.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell375.StylePriority.UseBackColor = false;
            this.xrTableCell375.StylePriority.UseBorderColor = false;
            this.xrTableCell375.StylePriority.UseBorders = false;
            this.xrTableCell375.StylePriority.UseBorderWidth = false;
            this.xrTableCell375.StylePriority.UseFont = false;
            this.xrTableCell375.StylePriority.UseForeColor = false;
            this.xrTableCell375.StylePriority.UsePadding = false;
            this.xrTableCell375.Weight = 1.8243500499448164;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader21});
            this.DetailReport.DataMember = "CommercialActivePrincipalInfoSummary";
            this.DetailReport.Level = 14;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDirPrincipalInfoSummary});
            this.Detail1.HeightF = 69F;
            this.Detail1.Name = "Detail1";
            // 
            // tblDirPrincipalInfoSummary
            // 
            this.tblDirPrincipalInfoSummary.LocationFloat = new DevExpress.Utils.PointFloat(17.51936F, 23.95833F);
            this.tblDirPrincipalInfoSummary.Name = "tblDirPrincipalInfoSummary";
            this.tblDirPrincipalInfoSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44,
            this.xrTableRow48});
            this.tblDirPrincipalInfoSummary.SizeF = new System.Drawing.SizeF(453.0415F, 40F);
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNoOfPrincipals,
            this.lblNoOfPrincipalsD});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.8;
            // 
            // lblNoOfPrincipals
            // 
            this.lblNoOfPrincipals.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblNoOfPrincipals.BorderColor = System.Drawing.Color.White;
            this.lblNoOfPrincipals.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNoOfPrincipals.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfPrincipals.ForeColor = System.Drawing.Color.Gray;
            this.lblNoOfPrincipals.Multiline = true;
            this.lblNoOfPrincipals.Name = "lblNoOfPrincipals";
            this.lblNoOfPrincipals.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblNoOfPrincipals.StylePriority.UseBackColor = false;
            this.lblNoOfPrincipals.StylePriority.UseBorderColor = false;
            this.lblNoOfPrincipals.StylePriority.UseBorders = false;
            this.lblNoOfPrincipals.StylePriority.UseFont = false;
            this.lblNoOfPrincipals.StylePriority.UseForeColor = false;
            this.lblNoOfPrincipals.StylePriority.UsePadding = false;
            this.lblNoOfPrincipals.Text = "Number of Active Principals\r\n";
            this.lblNoOfPrincipals.Weight = 2.1913021850585936;
            // 
            // lblNoOfPrincipalsD
            // 
            this.lblNoOfPrincipalsD.BackColor = System.Drawing.Color.White;
            this.lblNoOfPrincipalsD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblNoOfPrincipalsD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblNoOfPrincipalsD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInfoSummary.NoOfPrincipals")});
            this.lblNoOfPrincipalsD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfPrincipalsD.ForeColor = System.Drawing.Color.DimGray;
            this.lblNoOfPrincipalsD.Name = "lblNoOfPrincipalsD";
            this.lblNoOfPrincipalsD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblNoOfPrincipalsD.StylePriority.UseBackColor = false;
            this.lblNoOfPrincipalsD.StylePriority.UseBorderColor = false;
            this.lblNoOfPrincipalsD.StylePriority.UseBorders = false;
            this.lblNoOfPrincipalsD.StylePriority.UseFont = false;
            this.lblNoOfPrincipalsD.StylePriority.UseForeColor = false;
            this.lblNoOfPrincipalsD.StylePriority.UsePadding = false;
            this.lblNoOfPrincipalsD.Text = "lblNoOfPrincipalsD";
            this.lblNoOfPrincipalsD.Weight = 2.3391128540039059;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDirAvgAge,
            this.lblDirAvgAgeD});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 0.79999999999999993;
            // 
            // lblDirAvgAge
            // 
            this.lblDirAvgAge.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDirAvgAge.BorderColor = System.Drawing.Color.White;
            this.lblDirAvgAge.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDirAvgAge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirAvgAge.ForeColor = System.Drawing.Color.Gray;
            this.lblDirAvgAge.Multiline = true;
            this.lblDirAvgAge.Name = "lblDirAvgAge";
            this.lblDirAvgAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDirAvgAge.StylePriority.UseBackColor = false;
            this.lblDirAvgAge.StylePriority.UseBorderColor = false;
            this.lblDirAvgAge.StylePriority.UseBorders = false;
            this.lblDirAvgAge.StylePriority.UseFont = false;
            this.lblDirAvgAge.StylePriority.UseForeColor = false;
            this.lblDirAvgAge.StylePriority.UsePadding = false;
            this.lblDirAvgAge.Text = "Average Age of Active Principals\r\n";
            this.lblDirAvgAge.Weight = 2.1913021850585932;
            // 
            // lblDirAvgAgeD
            // 
            this.lblDirAvgAgeD.BackColor = System.Drawing.Color.White;
            this.lblDirAvgAgeD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDirAvgAgeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDirAvgAgeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialActivePrincipalInfoSummary.AverageAge")});
            this.lblDirAvgAgeD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirAvgAgeD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDirAvgAgeD.Name = "lblDirAvgAgeD";
            this.lblDirAvgAgeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDirAvgAgeD.StylePriority.UseBackColor = false;
            this.lblDirAvgAgeD.StylePriority.UseBorderColor = false;
            this.lblDirAvgAgeD.StylePriority.UseBorders = false;
            this.lblDirAvgAgeD.StylePriority.UseFont = false;
            this.lblDirAvgAgeD.StylePriority.UseForeColor = false;
            this.lblDirAvgAgeD.StylePriority.UsePadding = false;
            this.lblDirAvgAgeD.Text = "lblDirAvgAgeD";
            this.lblDirAvgAgeD.Weight = 2.3391128540039059;
            // 
            // GroupHeader21
            // 
            this.GroupHeader21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable28});
            this.GroupHeader21.HeightF = 72F;
            this.GroupHeader21.Name = "GroupHeader21";
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 25F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable28.SizeF = new System.Drawing.SizeF(790F, 40.62F);
            this.xrTable28.StylePriority.UseBorders = false;
            this.xrTable28.StylePriority.UseFont = false;
            this.xrTable28.StylePriority.UseForeColor = false;
            this.xrTable28.StylePriority.UsePadding = false;
            this.xrTable28.StylePriority.UseTextAlignment = false;
            this.xrTable28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPrincipalInfoH,
            this.xrTableCell75});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.8;
            // 
            // lblPrincipalInfoH
            // 
            this.lblPrincipalInfoH.BackColor = System.Drawing.Color.White;
            this.lblPrincipalInfoH.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblPrincipalInfoH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblPrincipalInfoH.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblPrincipalInfoH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.lblPrincipalInfoH.Name = "lblPrincipalInfoH";
            this.lblPrincipalInfoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPrincipalInfoH.StylePriority.UseBackColor = false;
            this.lblPrincipalInfoH.StylePriority.UseBorderColor = false;
            this.lblPrincipalInfoH.StylePriority.UseBorders = false;
            this.lblPrincipalInfoH.StylePriority.UseFont = false;
            this.lblPrincipalInfoH.StylePriority.UseForeColor = false;
            this.lblPrincipalInfoH.StylePriority.UsePadding = false;
            this.lblPrincipalInfoH.Text = "Principals";
            this.lblPrincipalInfoH.Weight = 1.1756499500551834;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.BackColor = System.Drawing.Color.White;
            this.xrTableCell75.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell75.BorderWidth = 1;
            this.xrTableCell75.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell75.StylePriority.UseBackColor = false;
            this.xrTableCell75.StylePriority.UseBorderColor = false;
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.StylePriority.UseBorderWidth = false;
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.StylePriority.UseForeColor = false;
            this.xrTableCell75.StylePriority.UsePadding = false;
            this.xrTableCell75.Weight = 1.8243500499448164;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.0000008583075941;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell1.BorderColor = System.Drawing.Color.White;
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell1.StylePriority.UseBackColor = false;
            this.xrTableCell1.StylePriority.UseBorderColor = false;
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.Text = "Profession Code";
            this.xrTableCell1.Weight = 0.73029755272442409;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BackColor = System.Drawing.Color.White;
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.ProfessionDesc")});
            this.xrTableCell2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell2.StylePriority.UseBackColor = false;
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Text = "xrTableCell2";
            this.xrTableCell2.Weight = 2.2697024472755754;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1.0000008583075941;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell6.BorderColor = System.Drawing.Color.White;
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseBorderColor = false;
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.Text = "Auditor Type Code";
            this.xrTableCell6.Weight = 0.73029755272442409;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.White;
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CommercialAuditorInformation.AuditorTypeDesc")});
            this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 2.2697024472755754;
            // 
            // BusinessAdverseReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DRCommBusinfo,
            this.DRCreditCircleInfo,
            this.DRCommJudgSumm,
            this.DRCommAUDInfo,
            this.DRCommDIRInfo,
            this.DRSubInputDtls,
            this.PageFooter,
            this.DRCommPosJudg,
            this.DRCommBranchInfo,
            this.DRCommDivisionInfo,
            this.DRXDSPaymentNotification,
            this.DRDefaultListing,
            this.DRRescue,
            this.DRBankInformation,
            this.DRXDSPrincipals,
            this.DetailReport3,
            this.DetailReport});
            this.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic)
                            | System.Drawing.FontStyle.Underline))));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(22, 18, 6, 20);
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "9.3";
            this.XmlDataPath = "D:\\Work\\South Africa\\Projects\\A\\XMLXSD\\Detailed_new2.xml";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTAXInfoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTAXInfoC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDJudg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCJud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJudgmentsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPosslCJud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblposJudgmentsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBranchInfoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBranchInfoC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDivisonD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDivisonC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPmtNotificationD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPmtNotificationC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDRescue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCRescue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirPrincipalInfoSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommBusinfo;
        private DevExpress.XtraReports.UI.DetailBand DCommBusinfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.DetailReportBand DRCreditCircleInfo;
        private DevExpress.XtraReports.UI.DetailBand DCreditCircleInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommJudgSumm;
        private DevExpress.XtraReports.UI.DetailBand DCommJudgSumm;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommAUDInfo;
        private DevExpress.XtraReports.UI.DetailBand DCommAUDInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommDIRInfo;
        private DevExpress.XtraReports.UI.DetailBand DCommDIRInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRLabel lblDirectorRecPosition;
        private DevExpress.XtraReports.UI.XRTable tblTAXInfoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell lblTCompanyNameD;
        private DevExpress.XtraReports.UI.XRTableCell lblVATNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblVATLiableDateD;
        private DevExpress.XtraReports.UI.XRTableCell lblStatusD;
        private DevExpress.XtraReports.UI.XRTable tblTAXInfoC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyNameC;
        private DevExpress.XtraReports.UI.XRTableCell lblVATNoC;
        private DevExpress.XtraReports.UI.XRTableCell lblVATLiableDateC;
        private DevExpress.XtraReports.UI.XRTableCell lblStatusC;
        private DevExpress.XtraReports.UI.DetailReportBand DRSubInputDtls;
        private DevExpress.XtraReports.UI.DetailBand DSubInputDtls;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTable tblCJud;
        private DevExpress.XtraReports.UI.XRTableRow tblJudgementH;
        private DevExpress.XtraReports.UI.XRTableCell lblJCaseNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJType;
        private DevExpress.XtraReports.UI.XRTableCell lblJAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblJPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblJCourt;
        private DevExpress.XtraReports.UI.XRTable tblJudgmentsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell lblJudgmentsH;
        private DevExpress.XtraReports.UI.XRTable tblDJudg;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
        private DevExpress.XtraReports.UI.XRTableCell lblJDCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblJDIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJDType;
        private DevExpress.XtraReports.UI.XRTableCell lblJDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblJDPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblJDCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblJCaseReasonD;
        private DevExpress.XtraReports.UI.XRTableCell lblJCaseReason;
        private DevExpress.XtraReports.UI.XRTableCell lblJAttroneyNameD;
        private DevExpress.XtraReports.UI.XRTableCell lblAttroneyNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblJAttroneyNameC;
        private DevExpress.XtraReports.UI.XRTableCell lblAttroneyNoC;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommPosJudg;
        private DevExpress.XtraReports.UI.DetailBand DCommPosJudg;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.XRTable tblposJudgmentsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell lblposJudgmentsH;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommBranchInfo;
        private DevExpress.XtraReports.UI.DetailBand DCommBranchInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
        private DevExpress.XtraReports.UI.XRTable tblBranchInfoC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTable tblBranchInfoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell lblBranchNameD;
        private DevExpress.XtraReports.UI.DetailReportBand DRCommDivisionInfo;
        private DevExpress.XtraReports.UI.DetailBand DCommDivisionInfo;
        private DevExpress.XtraReports.UI.XRTable tblDivisonD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell lblDivisionCodeD;
        private DevExpress.XtraReports.UI.XRTableCell lblDivisionNameD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.XRTable tblDivisonC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell lblDivisionCodeC;
        private DevExpress.XtraReports.UI.XRTableCell lblDivisionNameC;
        private DevExpress.XtraReports.UI.DetailReportBand DRXDSPaymentNotification;
        private DevExpress.XtraReports.UI.DetailBand DPaymentNotification;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader23;
        private DevExpress.XtraReports.UI.XRTable tblPmtNotificationC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow134;
        private DevExpress.XtraReports.UI.XRTableCell lblpmtSubscriberC;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAccNoC;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtDatelistedC;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAmountC;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAccStatusC;
        private DevExpress.XtraReports.UI.XRTable tblPmtNotificationD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow135;
        private DevExpress.XtraReports.UI.XRTableCell lblpmtSubscriberD;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAccNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtDatelistedD;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAmountD;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAccStatusD;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtCommentD;
        private DevExpress.XtraReports.UI.DetailReportBand DRDefaultListing;
        private DevExpress.XtraReports.UI.DetailBand DDefaultListing;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader26;
        private DevExpress.XtraReports.UI.XRTable tblDefaultAlertH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell lblDefaultAlertH;
        private DevExpress.XtraReports.UI.XRTable tblDefaultAlertD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow146;
        private DevExpress.XtraReports.UI.XRTableCell lblDACompanyD;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAccnoD;
        private DevExpress.XtraReports.UI.XRTableCell lblDADLoadeddateD;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAmountD;
        private DevExpress.XtraReports.UI.XRTableCell lblDAStatusD;
        private DevExpress.XtraReports.UI.XRTableCell lblDACommentsD;
        private DevExpress.XtraReports.UI.DetailReportBand DRRescue;
        private DevExpress.XtraReports.UI.DetailBand DRescue;
        private DevExpress.XtraReports.UI.XRTable tblDRescue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow149;
        private DevExpress.XtraReports.UI.XRTableCell lblDReportedby;
        private DevExpress.XtraReports.UI.XRTableCell lblDReportedDate;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader27;
        private DevExpress.XtraReports.UI.XRTable tblCRescue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow148;
        private DevExpress.XtraReports.UI.XRTableCell lblCReportedby;
        private DevExpress.XtraReports.UI.XRTableCell lblCDateReported;
        private DevExpress.XtraReports.UI.DetailReportBand DRBankInformation;
        private DevExpress.XtraReports.UI.DetailBand BankDetails;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell LblBank;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell LblBranch;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell LblTypeOfAccount;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell LblAccountHolderName;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell LblOverDraftFac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell LblBankCode;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader29;
        private DevExpress.XtraReports.UI.XRTableCell LblBankD;
        private DevExpress.XtraReports.UI.XRTableCell LblBranchD;
        private DevExpress.XtraReports.UI.XRTableCell LblTypeOfAccountD;
        private DevExpress.XtraReports.UI.XRTableCell LblAccountHolderNameD;
        private DevExpress.XtraReports.UI.XRTableCell LblOverDarftFacD;
        private DevExpress.XtraReports.UI.XRTableCell LblBankCodeD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell LblBankComment;
        private DevExpress.XtraReports.UI.XRTableCell LblBankCommentD;
        private DevExpress.XtraReports.UI.XRTableCell LblCoutryCD;
        private DevExpress.XtraReports.UI.XRTableCell LblProvinceD;
        private DevExpress.XtraReports.UI.XRTableCell LblBrachNameC;
        private DevExpress.XtraReports.UI.XRTableCell LblCountry;
        private DevExpress.XtraReports.UI.XRTableCell LblProvince;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell LblAccountNumber;
        private DevExpress.XtraReports.UI.XRTableCell LbLAccountNumberD;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblDetailedBusInfoH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell lblVATInfoH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblBranchH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell lblDivisionInfoH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell lblAUDInfoH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblPaymentNotificationH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.DetailReportBand CommercialPreviousBankDetails;
        private DevExpress.XtraReports.UI.DetailBand Detail10;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell LblYearsWithBankD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader35;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.DetailReportBand DRXDSPrincipals;
        private DevExpress.XtraReports.UI.DetailBand Detail12;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTable xrTable44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell LblFullName;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell LblPreviousSurname;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow199;
        private DevExpress.XtraReports.UI.XRTableCell lblDAge;
        private DevExpress.XtraReports.UI.XRTableCell lblDAgeD;
        private DevExpress.XtraReports.UI.XRTableCell lblDYearswithBus;
        private DevExpress.XtraReports.UI.XRTableCell lblDYearswithBusD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell lblSICCodeD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTable xrTable49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTable tblDefaultAlertC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable xrTable52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell397;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader30;
        private DevExpress.XtraReports.UI.XRTable xrTable51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTable xrTable69;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell530;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell531;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell532;
        private DevExpress.XtraReports.UI.XRTableCell lblposjudgAmountD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell534;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell535;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell536;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell537;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell538;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell540;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell539;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell542;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell541;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell489;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell490;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell491;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell492;
        private DevExpress.XtraReports.UI.XRTable tbPosslCJud;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryDateValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRLabel LblAuthorisedcapitalD;
        private DevExpress.XtraReports.UI.XRLabel LblIssuedCapitalAmntD;
        private DevExpress.XtraReports.UI.XRLabel lblPaymentAmnt;
        private DevExpress.XtraReports.UI.XRLabel LblPosJDAmnt;
        private DevExpress.XtraReports.UI.XRLabel lblPmtAmount;
        private DevExpress.XtraReports.UI.XRLabel lblBusinessRescueH;
        private DevExpress.XtraReports.UI.XRLabel lblAuditorH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell lblPaymentNotificationsH;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable tblDirPrincipalInfoSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell lblNoOfPrincipals;
        private DevExpress.XtraReports.UI.XRTableCell lblNoOfPrincipalsD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell lblDirAvgAge;
        private DevExpress.XtraReports.UI.XRTableCell lblDirAvgAgeD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader21;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblPrincipalInfoH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
    }
}
