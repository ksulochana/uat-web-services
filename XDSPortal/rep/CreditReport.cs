﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XDSConsumerDirectReport
{
    public partial class CreditReport : DevExpress.XtraReports.UI.XtraReport
    {
        public CreditReport(string strXML)
        {
            InitializeComponent();
            System.IO.StringReader Objsr = new System.IO.StringReader(strXML.Replace("''", "'"));
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Objsr);

            //if (dsXML.Tables.Contains("SubscriberInputDetails"))
            //{
            //    dsXML.Tables["SubscriberInputDetails"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate = '', '1900-01-01T00:00:00', EnquiryDate)");
            //    this.lblEnquiryDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - hh:mm:ss}") });
            //}

            //if (!(dsXML.Tables.Contains("ConsumerDetail")))
            //{
            //    DetailConsumerDetail.Visible = false;
            //    //lblPersonalDetail.Text += " - (No Data Available)";
            //    lblPDNoInfo.Visible = true;
            //}
            ////else
            ////{
            ////    dsXML.Tables["ConsumerDetail"].Columns.Add("FBirthDate", System.Type.GetType("System.DateTime"), "Iif(BirthDate = '', '1900-01-01T00:00:00', BirthDate)");
            ////    this.lblDOBValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetail.FBirthDate", "{0:dd/MM/yyyy}") });
            ////}
            //if (!(dsXML.Tables.Contains("ConsumerFraudIndicatorsSummary")))
            //{
            //    DetailFraudIndicatorSummary.Visible = false;
            //    //lblFraudSummary.Text += " - (No Data Available)";
            //    lblPFNoINfo.Visible = true;
            //}
            ////if (!(dsXML.Tables.Contains("ConsumerPropertyInformationSummary")))
            ////{
            ////    DetailPropertyInformationSummary.Visible = false;
            ////    //lblPropertyInfoSummaryH.Text += " - (No Data Available)";
            ////    lblPropsummNoInfo.Visible = true;
            ////}
            ////else
            ////{
            ////    if (dsXML.Tables["ConsumerPropertyInformationSummary"].Columns.Contains("PurchasePrice"))
            ////    {
            ////        dsXML.Tables["ConsumerPropertyInformationSummary"].Columns.Add("FPurchasePrice", System.Type.GetType("System.Double"), "Iif(PurchasePrice = '', 0, PurchasePrice)");
            ////        this.sdrtgb.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformationSummary.FPurchasePrice", "{0:c0}") });
            ////    }
            ////}

            ////if (!(dsXML.Tables.Contains("ConsumerDirectorSummary")))
            ////{
            ////    DetailDirectorSummary.Visible = false;
            ////    //lblDirSummaryH.Text += " - (No Data Available)";
            ////    lblDirSumNoInfo.Visible = true;

            ////}

            if (!(dsXML.Tables.Contains("ConsumerScoring")))
            {
                //DetailScoring.Visible = false;
                ////lblScoreHeader.Text += " - (No Data Available)";
                //tblscoreColumns.Visible = false;
                //lblScoreNoInfo.Visible = true;
            }
            else
            {

                // dsXML.Tables["ConsumerScoring"].Columns.Add("FScoreDate", System.Type.GetType("System.DateTime"), "Iif(ScoreDate = '', '1900-01-01T00:00:00', ScoreDate)");
                dsXML.Tables["ConsumerScoring"].Columns.Add("FFinalScore", System.Type.GetType("System.Double"), "Iif(FinalScore = '', 0, FinalScore)");

                //this.lblDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerScoring.FScoreDate", "{0:dd/MM/yyyy}") });
                this.lblScore.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerScoring.FFinalScore") });
            }

            if (dsXML.Tables.Contains("ConsumerCPANLRDebtSummary"))
            {
                if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerPropertyInformation") || dsXML.Tables["ConsumerPropertyInformation"] == null))
                {
                    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["TotalProperty"] = 0;
                    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["PurchasePrice"] = 0;
                }
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FPurchasePrice", System.Type.GetType("System.Double"), "Iif(PurchasePrice = '', 0, PurchasePrice)");
                this.lblPurchPrice.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FPurchasePrice", "{0:c0}") });
            }

            if (dsXML.Tables.Contains("ConsumerCPANLRDebtSummary"))
            {
                if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerDirectorShipLink") || dsXML.Tables["ConsumerDirectorShipLink"] == null))
                {
                    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["NumberOfCompanyDirector"] = 0;
                }
            }

            //if (!(dsXML.Tables.Contains("ConsumerDebtSummary")) && !(dsXML.Tables.Contains("ConsumerNLRDebtSummary")) && !(dsXML.Tables.Contains("ConsumerCPANLRDebtSummary")))
            //{
            //    DetailDebtSummary.Visible = false;
            //    DetailNonCPADebtSummary.Visible = false;
            //    //lblDebtHeader.Text += " - (No Data Available)";
            //    lblDebtsummNoInfo.Visible = true;

            //    NLRAccountStatus.Visible = false;
            //    NLRMonthlyPaymentHeader.Visible = false;
            //    NLRMonthlyPayment.Visible = false;
            //    NLRConsumerDefinition.Visible = false;

            //    Scoring.Visible = false;
            //    AccountStatus.Visible = false;
            //    MonthlyPaymentHeader.Visible = false;
            //    MonthlyPayment.Visible = false;
            //    ConsumerDefinition.Visible = false;
            //}
            //else if (dsXML.Tables.Contains("ConsumerDebtSummary"))
            //{

            //    NLRAccountStatus.Visible = false;
            //    NLRMonthlyPaymentHeader.Visible = false;
            //    NLRMonthlyPayment.Visible = false;
            //    NLRConsumerDefinition.Visible = false;

            //    if (dsXML.Tables["ConsumerDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerAdverseInfo") || dsXML.Tables["ConsumerAdverseInfo"] == null))
            //    {
            //        dsXML.Tables["ConsumerDebtSummary"].Rows[0]["TotalAdverseAmt"] = 0;
            //    }
            //    if (dsXML.Tables["ConsumerDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerDefaultAlert") || dsXML.Tables["ConsumerDefaultAlert"] == null))
            //    {
            //        dsXML.Tables["ConsumerDebtSummary"].Rows[0]["DefaultListingAmt"] = 0;
            //    }
            //    if (dsXML.Tables["ConsumerDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerJudgement") || dsXML.Tables["ConsumerJudgement"] == null))
            //    {
            //        dsXML.Tables["ConsumerDebtSummary"].Rows[0]["TotalJudgmentAmt"] = 0;
            //    }
            //    if (dsXML.Tables["ConsumerDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerAdminOrder") || dsXML.Tables["ConsumerAdminOrder"] == null) && (!dsXML.Tables.Contains("ConsumerSequestration") || dsXML.Tables["ConsumerSequestration"] == null) && (!dsXML.Tables.Contains("ConsumerRehabilitationOrder") || dsXML.Tables["ConsumerRehabilitationOrder"] == null))
            //    {
            //        dsXML.Tables["ConsumerDebtSummary"].Rows[0]["TotalCourtNoticeAmt"] = 0;
            //    }

            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalJudgmentAmt", System.Type.GetType(" System.Single"), "Iif(TotalJudgmentAmt = '', 0, TotalJudgmentAmt)");
            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalCourtNoticeAmt", System.Type.GetType(" System.Single"), "Iif(TotalCourtNoticeAmt = '', 0, TotalCourtNoticeAmt)");
            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FDefaultListingAmt", System.Type.GetType(" System.Single"), "Iif(DefaultListingAmt = '', 0, DefaultListingAmt)");
            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalAdverseAmount", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmount = '', 0, TotalAdverseAmount)");



            //    this.lblJudgBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalJudgmentAmt", "{0:c0}") });
            //    this.lblAOBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalCourtNoticeAmt", "{0:c0}") });
            //    this.lblADBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalAdverseAmount", "{0:c0}") });
            //    this.lblDLBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FDefaultListingAmt", "{0:c0}") });

            //    this.lblJudgCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.JudgementCount") });
            //    this.lblAOCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.CourtNoticeCount") });
            //    this.lblAccDefaultCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoofAccountdefaults") });
            //    this.lblDLCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.DefaultListingCount") });
            //    this.lblEnq24Count.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfEnqinLast24Months") });
            //    this.lblDebtReviewStatus.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.DebtReviewStatus") });
            //    this.lblDisputeMsg.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.DisputeMessage") });
            //    this.lblEnq24Date.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.MostRecentEnqDateLast24Months") });
            //    this.lblDLDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.RecentDefaultListingDate") });
            //    this.lblADDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.MostRecentAdverseDate") });
            //    this.lblAODate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.MostRecentCourtNoticeDate") });
            //    this.lblJudgDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.MostRecentJudgmentDate") });


            //    lblDSNLR.Text = "CPA";
            //    lblDSCPA.Text = string.Empty;
            //    lblDSTotal.Text = string.Empty;

            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalMonthlyInstallment", System.Type.GetType(" System.Single"), "Iif(TotalMonthlyInstallment = '', 0, TotalMonthlyInstallment)");
            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalOutStandingDebt", System.Type.GetType(" System.Single"), "Iif(TotalOutStandingDebt = '', 0, TotalOutStandingDebt)");
            //    dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalArrearAmount", System.Type.GetType(" System.Single"), "Iif(TotalArrearAmount = '', 0, TotalArrearAmount)");
            //    //dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalAdverseAmount", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmount = '', 0, TotalAdverseAmount)");

            //    this.lblMonthlyinstValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalMonthlyInstallment", "{0:c0}") });
            //    this.lblOutstandingDebtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalOutStandingDebt", "{0:c0}") });
            //    this.lblArrearamtvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalArrearAmount", "{0:c0}") });
            //    this.lblAdverseAmtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalAdverseAmount", "{0:c0}") });

            //    this.lblActiveAccValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOFActiveAccounts") });
            //    this.lblAccGoodvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfAccountInGoodStanding") });
            //    this.lblDelinquentAccValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfAccountInBadStanding") });
            //    this.lblDelinquentratingValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.HighestMonthsinArrears") });
            //    this.lblenq90Value.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfEnquiriesLast90DaysOWN") });
            //    this.lblenq90othervalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfEnquiriesLast90DaysOTH") });
            //    this.lblAcc30Value.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfAccountsOpenedinLast45Days") });
            //    this.lblPaiduporcloseValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.NoOfPaidUpOrClosedAccounts") });

            //    if (dsXML.Tables["ConsumerDebtSummary"].Rows.Count > 0)
            //    {
            //        DataRow dr = dsXML.Tables["ConsumerDebtSummary"].Rows[0];

            //        if (!dsXML.Tables.Contains("ConsumerAdverseInfo") || dsXML.Tables["ConsumerAdverseInfo"] == null)
            //        {
            //            dr["NoofAccountdefaults"] = "0";
            //            dr["TotalAdverseAmt"] = "0";
            //            dr["MostRecentAdverseDate"] = "";
            //        }

            //        if (!dsXML.Tables.Contains("ConsumerDefaultAlert") || dsXML.Tables["ConsumerDefaultAlert"] == null)
            //        {
            //            dr["DefaultListingCount"] = "0";
            //            dr["DefaultListingAmt"] = "0";
            //            dr["RecentDefaultListingDate"] = "";
            //        }

            //        if (!dsXML.Tables.Contains("ConsumerJudgement") || dsXML.Tables["ConsumerJudgement"] == null)
            //        {
            //            dr["JudgementCount"] = "0";
            //            dr["TotalJudgmentAmt"] = "0";
            //            dr["MostRecentJudgmentDate"] = "";
            //        }

            //        if ((!dsXML.Tables.Contains("ConsumerAdminOrder") || dsXML.Tables["ConsumerAdminOrder"] == null) && (!dsXML.Tables.Contains("ConsumerSequestration") || dsXML.Tables["ConsumerSequestration"] == null) && (!dsXML.Tables.Contains("ConsumerRehabilitationOrder") || dsXML.Tables["ConsumerRehabilitationOrder"] == null))
            //        {
            //            dr["CourtnoticeCount"] = "0";
            //            dr["TotalCourtNoticeAmt"] = "0";
            //            dr["MostRecentCourtnoticeDate"] = "";
            //        }

            //        if (!dsXML.Tables.Contains("ConsumerDebtReviewStatus") || dsXML.Tables["ConsumerDebtReviewStatus"] == null)
            //        {
            //            dr["DebtReviewStatus"] = "";
            //        }

            //        if (!String.IsNullOrEmpty(dr["FTotalArrearAmount"].ToString()) && Single.Parse(dr["FTotalArrearAmount"].ToString()) > 0)
            //        {
            //            this.lblArrearamtvalue.BackColor = Color.White;
            //            this.lblArrearamtCPAvalue.BackColor = Color.White;
            //            this.lblArrearamtTotvalue.BackColor = Color.White;
            //            this.lblArrearamtvalue.ForeColor = Color.Red;
            //            this.lblArrearamtCPAvalue.ForeColor = Color.Red;
            //            this.lblArrearamtTotvalue.ForeColor = Color.Red;

            //            this.lblArrearamtvalue.Font = new Font(lblArrearamtvalue.Font.FontFamily, lblArrearamtvalue.Font.Size, FontStyle.Bold);
            //            this.lblArrearamtCPAvalue.Font = new Font(lblArrearamtCPAvalue.Font.FontFamily, lblArrearamtCPAvalue.Font.Size, FontStyle.Bold);
            //            this.lblArrearamtTotvalue.Font = new Font(lblArrearamtTotvalue.Font.FontFamily, lblArrearamtTotvalue.Font.Size, FontStyle.Bold);
            //        }
            //        //if (!String.IsNullOrEmpty(dr["HighestMonthsinArrears"].ToString()) && Int32.Parse(dr["HighestMonthsinArrears"].ToString()) > 2)
            //        //{
            //        //    this.lblDelinquentratingValue.BackColor = Color.Red;
            //        //    this.lblDelinquentratingCPAValue.BackColor = Color.Red;
            //        //    this.lblDelinquentratingTotValue.BackColor = Color.Red;
            //        //    this.lblDelinquentratingValue.ForeColor = Color.White;
            //        //    this.lblDelinquentratingCPAValue.ForeColor = Color.White;
            //        //    this.lblDelinquentratingTotValue.ForeColor = Color.White;
            //        //}
            //        //if (!String.IsNullOrEmpty(dr["FTotalAdverseAmount"].ToString()) && Single.Parse(dr["FTotalAdverseAmount"].ToString()) > 0)
            //        //{
            //        //    this.lblAdverseAmtValue.BackColor = Color.Red;
            //        //    this.lblAdverseAmtCPAValue.BackColor = Color.Red;
            //        //    this.lblAdverseAmtTotValue.BackColor = Color.Red;
            //        //    this.lblAdverseAmtValue.ForeColor = Color.White;
            //        //    this.lblAdverseAmtCPAValue.ForeColor = Color.White;
            //        //    this.lblAdverseAmtTotValue.ForeColor = Color.White;
            //        //}
            //        //if (!String.IsNullOrEmpty(dr["JudgementCount"].ToString()) && Single.Parse(dr["JudgementCount"].ToString()) > 0)
            //        //{
            //        //    this.lblJudgCount.BackColor = Color.Red;
            //        //    this.lblJudgBalance.BackColor = Color.Red;
            //        //    this.lblJudgDate.BackColor = Color.Red;
            //        //    this.lblJudgCount.ForeColor = Color.White;
            //        //    this.lblJudgBalance.ForeColor = Color.White;
            //        //    this.lblJudgDate.ForeColor = Color.White;
            //        //}
            //        //if (!String.IsNullOrEmpty(dr["CourtNoticeCount"].ToString()) && Single.Parse(dr["CourtNoticeCount"].ToString()) > 0)
            //        //{
            //        //    this.lblAOCount.BackColor = Color.Red;
            //        //    this.lblAOBalance.BackColor = Color.Red;
            //        //    this.lblAODate.BackColor = Color.Red;
            //        //    this.lblAOCount.ForeColor = Color.White;
            //        //    this.lblAOBalance.ForeColor = Color.White;
            //        //    this.lblAODate.ForeColor = Color.White;
            //        //}
            //        //if (!String.IsNullOrEmpty(dr["NoofAccountdefaults"].ToString()) && Single.Parse(dr["NoofAccountdefaults"].ToString()) > 0)
            //        //{
            //        //    this.lblAccDefaultCount.BackColor = Color.Red;
            //        //    this.lblADBalance.BackColor = Color.Red;
            //        //    this.lblADDate.BackColor = Color.Red;
            //        //    this.lblAccDefaultCount.ForeColor = Color.White;
            //        //    this.lblADBalance.ForeColor = Color.White;
            //        //    this.lblADDate.ForeColor = Color.White;
            //        //}
            //        //if (!String.IsNullOrEmpty(dr["DefaultListingCount"].ToString()) && Single.Parse(dr["DefaultListingCount"].ToString()) > 0)
            //        //{
            //        //    this.lblDLCount.BackColor = Color.Red;
            //        //    this.lblDLBalance.BackColor = Color.Red;
            //        //    this.lblDLDate.BackColor = Color.Red;
            //        //    this.lblDLCount.ForeColor = Color.White;
            //        //    this.lblDLBalance.ForeColor = Color.White;
            //        //    this.lblDLDate.ForeColor = Color.White;
            //        //}


            //        if (!String.IsNullOrEmpty(dr["HighestMonthsinArrears"].ToString()) && Int32.Parse(dr["HighestMonthsinArrears"].ToString()) > 2)
            //        {
            //            this.lblDelinquentratingValue.BackColor = Color.White;
            //            this.lblDelinquentratingCPAValue.BackColor = Color.White;
            //            this.lblDelinquentratingTotValue.BackColor = Color.White;
            //            this.lblDelinquentratingValue.ForeColor = Color.Red;
            //            this.lblDelinquentratingCPAValue.ForeColor = Color.Red;
            //            this.lblDelinquentratingTotValue.ForeColor = Color.Red;

            //            this.lblDelinquentratingValue.Font = new Font(lblDelinquentratingValue.Font.FontFamily, lblDelinquentratingValue.Font.Size, FontStyle.Bold);
            //            this.lblDelinquentratingCPAValue.Font = new Font(lblDelinquentratingCPAValue.Font.FontFamily, lblDelinquentratingCPAValue.Font.Size, FontStyle.Bold);
            //            this.lblDelinquentratingTotValue.Font = new Font(lblDelinquentratingTotValue.Font.FontFamily, lblDelinquentratingTotValue.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["FTotalAdverseAmount"].ToString()) && Single.Parse(dr["FTotalAdverseAmount"].ToString()) > 0)
            //        {
            //            this.lblAdverseAmtValue.BackColor = Color.White;
            //            this.lblAdverseAmtCPAValue.BackColor = Color.White;
            //            this.lblAdverseAmtTotValue.BackColor = Color.White;
            //            this.lblAdverseAmtValue.ForeColor = Color.Red;
            //            this.lblAdverseAmtCPAValue.ForeColor = Color.Red;
            //            this.lblAdverseAmtTotValue.ForeColor = Color.Red;

            //            this.lblAdverseAmtValue.Font = new Font(lblAdverseAmtValue.Font.FontFamily, lblAdverseAmtValue.Font.Size, FontStyle.Bold);
            //            this.lblAdverseAmtCPAValue.Font = new Font(lblAdverseAmtCPAValue.Font.FontFamily, lblAdverseAmtCPAValue.Font.Size, FontStyle.Bold);
            //            this.lblAdverseAmtTotValue.Font = new Font(lblAdverseAmtTotValue.Font.FontFamily, lblAdverseAmtTotValue.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["NoofAccountdefaults"].ToString()) && Single.Parse(dr["NoofAccountdefaults"].ToString()) > 0)
            //        {
            //            this.lblAccDefaultCount.BackColor = Color.White;
            //            this.lblADBalance.BackColor = Color.White;
            //            this.lblADDate.BackColor = Color.White;
            //            this.lblAccDefaultCount.ForeColor = Color.Red;
            //            this.lblADBalance.ForeColor = Color.Red;
            //            this.lblADDate.ForeColor = Color.Red;

            //            this.lblAccDefaultCount.Font = new Font(lblAccDefaultCount.Font.FontFamily, lblAccDefaultCount.Font.Size, FontStyle.Bold);
            //            this.lblADBalance.Font = new Font(lblADBalance.Font.FontFamily, lblADBalance.Font.Size, FontStyle.Bold);
            //            this.lblADDate.Font = new Font(lblADDate.Font.FontFamily, lblADDate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["DefaultListingCount"].ToString()) && Single.Parse(dr["DefaultListingCount"].ToString()) > 0)
            //        {
            //            this.lblDLCount.BackColor = Color.White;
            //            this.lblDLBalance.BackColor = Color.White;
            //            this.lblDLDate.BackColor = Color.White;
            //            this.lblDLCount.ForeColor = Color.Red;
            //            this.lblDLBalance.ForeColor = Color.Red;
            //            this.lblDLDate.ForeColor = Color.Red;

            //            this.lblDLCount.Font = new Font(lblDLCount.Font.FontFamily, lblDLCount.Font.Size, FontStyle.Bold);
            //            this.lblDLBalance.Font = new Font(lblDLBalance.Font.FontFamily, lblDLBalance.Font.Size, FontStyle.Bold);
            //            this.lblDLDate.Font = new Font(lblDLDate.Font.FontFamily, lblDLDate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["JudgementCount"].ToString()) && Single.Parse(dr["JudgementCount"].ToString()) > 0)
            //        {
            //            this.lblJudgCount.BackColor = Color.White;
            //            this.lblJudgBalance.BackColor = Color.White;
            //            this.lblJudgDate.BackColor = Color.White;
            //            this.lblJudgCount.ForeColor = Color.Red;
            //            this.lblJudgBalance.ForeColor = Color.Red;
            //            this.lblJudgDate.ForeColor = Color.Red;

            //            this.lblJudgCount.Font = new Font(lblJudgCount.Font.FontFamily, lblJudgCount.Font.Size, FontStyle.Bold);
            //            this.lblJudgBalance.Font = new Font(lblJudgBalance.Font.FontFamily, lblJudgBalance.Font.Size, FontStyle.Bold);
            //            this.lblJudgDate.Font = new Font(lblJudgDate.Font.FontFamily, lblJudgDate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["CourtNoticeCount"].ToString()) && Single.Parse(dr["CourtNoticeCount"].ToString()) > 0)
            //        {
            //            this.lblAOCount.BackColor = Color.White;
            //            this.lblAOBalance.BackColor = Color.White;
            //            this.lblAODate.BackColor = Color.White;
            //            this.lblAOCount.ForeColor = Color.Red;
            //            this.lblAOBalance.ForeColor = Color.Red;
            //            this.lblAODate.ForeColor = Color.Red;

            //            this.lblAOCount.Font = new Font(lblAOCount.Font.FontFamily, lblAOCount.Font.Size, FontStyle.Bold);
            //            this.lblAOBalance.Font = new Font(lblAOBalance.Font.FontFamily, lblAOBalance.Font.Size, FontStyle.Bold);
            //            this.lblAODate.Font = new Font(lblAODate.Font.FontFamily, lblAODate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["DebtReviewStatus"].ToString()))
            //        {
            //            this.lblDebtReviewStatus.BackColor = Color.White;
            //            this.lblDebtReviewStatus.ForeColor = Color.Red;

            //            this.lblDebtReviewStatus.Font = new Font(lblDebtReviewStatus.Font.FontFamily, lblDebtReviewStatus.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["DisputeMessage"].ToString()))
            //        {
            //            this.lblDisputeMsg.BackColor = Color.White;
            //            this.lblDisputeMsg.ForeColor = Color.Red;
            //            this.lblDisputeMsg.Font = new Font(lblDisputeMsg.Font.FontFamily, lblDisputeMsg.Font.Size, FontStyle.Bold);
            //        }
            //    }
            //}
            //else if (dsXML.Tables.Contains("ConsumerNLRDebtSummary"))
            //{
            //    Scoring.Visible = false;
            //    AccountStatus.Visible = false;
            //    MonthlyPaymentHeader.Visible = false;
            //    MonthlyPayment.Visible = false;
            //    ConsumerDefinition.Visible = false;

            //    lblDSNLR.Text = "NLR";
            //    lblDSCPA.Text = string.Empty;
            //    lblDSTotal.Text = string.Empty;

            //    if (dsXML.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerAdverseInfo") || dsXML.Tables["ConsumerAdverseInfo"] == null))
            //    {
            //        dsXML.Tables["ConsumerNLRDebtSummary"].Rows[0]["TotalAdverseAmt"] = 0;
            //    }
            //    if (dsXML.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerDefaultAlert") || dsXML.Tables["ConsumerDefaultAlert"] == null))
            //    {
            //        dsXML.Tables["ConsumerNLRDebtSummary"].Rows[0]["DefaultListingAmt"] = 0;
            //    }
            //    if (dsXML.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerJudgement") || dsXML.Tables["ConsumerJudgement"] == null))
            //    {
            //        dsXML.Tables["ConsumerNLRDebtSummary"].Rows[0]["TotalJudgmentAmt"] = 0;
            //    }
            //    if (dsXML.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerAdminOrder") || dsXML.Tables["ConsumerAdminOrder"] == null) && (!dsXML.Tables.Contains("ConsumerSequestration") || dsXML.Tables["ConsumerSequestration"] == null) && (!dsXML.Tables.Contains("ConsumerRehabilitationOrder") || dsXML.Tables["ConsumerRehabilitationOrder"] == null))
            //    {
            //        dsXML.Tables["ConsumerNLRDebtSummary"].Rows[0]["TotalCourtNoticeAmt"] = 0;
            //    }

            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalJudgmentAmt", System.Type.GetType(" System.Single"), "Iif(TotalJudgmentAmt = '', 0, TotalJudgmentAmt)");
            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalCourtNoticeAmt", System.Type.GetType(" System.Single"), "Iif(TotalCourtNoticeAmt = '', 0, TotalCourtNoticeAmt)");
            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FDefaultListingAmt", System.Type.GetType(" System.Single"), "Iif(DefaultListingAmt = '', 0, DefaultListingAmt)");
            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalAdverseAmount", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmount = '', 0, TotalAdverseAmount)");

            //    this.lblJudgBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalJudgmentAmt", "{0:c0}") });
            //    this.lblAOBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalCourtNoticeAmt", "{0:c0}") });
            //    this.lblADBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalAdverseAmount", "{0:c0}") });
            //    this.lblDLBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FDefaultListingAmt", "{0:c0}") });

            //    this.lblJudgCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.JudgementCount") });
            //    this.lblAOCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.CourtNoticeCount") });
            //    this.lblAccDefaultCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoofAccountdefaults") });
            //    this.lblDLCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.DefaultListingCount") });

            //    this.lblEnq24Count.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfEnqinLast24Months") });
            //    this.lblDebtReviewStatus.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.DebtReviewStatus") });
            //    this.lblDisputeMsg.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.DisputeMessage") });
            //    this.lblEnq24Date.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.MostRecentEnqDateLast24Months") });

            //    this.lblDLDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.RecentDefaultListingDate") });
            //    this.lblADDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.MostRecentAdverseDate") });
            //    this.lblAODate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.MostRecentCourtNoticeDate") });
            //    this.lblJudgDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.MostRecentJudgmentDate") });

            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalMonthlyInstallment", System.Type.GetType(" System.Single"), "Iif(TotalMonthlyInstallment = '', 0, TotalMonthlyInstallment)");
            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalOutStandingDebt", System.Type.GetType(" System.Single"), "Iif(TotalOutStandingDebt = '', 0, TotalOutStandingDebt)");
            //    dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalArrearAmount", System.Type.GetType(" System.Single"), "Iif(TotalArrearAmount = '', 0, TotalArrearAmount)");
            //    //dsXML.Tables["ConsumerNLRDebtSummary"].Columns.Add("FTotalAdverseAmount", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmount = '', 0, TotalAdverseAmount)");

            //    this.lblMonthlyinstValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalMonthlyInstallment", "{0:c0}") });
            //    this.lblOutstandingDebtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalOutStandingDebt", "{0:c0}") });
            //    this.lblArrearamtvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalArrearAmount", "{0:c0}") });
            //    this.lblAdverseAmtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.FTotalAdverseAmount", "{0:c0}") });


            //    this.lblActiveAccValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOFActiveAccounts") });
            //    this.lblAccGoodvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfAccountInGoodStanding") });
            //    this.lblDelinquentAccValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfAccountInBadStanding") });
            //    this.lblDelinquentratingValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.HighestMonthsinArrears") });
            //    this.lblenq90Value.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfEnquiriesLast90DaysOWN") });
            //    this.lblenq90othervalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfEnquiriesLast90DaysOTH") });
            //    this.lblAcc30Value.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfAccountsOpenedinLast45Days") });
            //    this.lblPaiduporcloseValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRDebtSummary.NoOfPaidUpOrClosedAccounts") });

            //    if (dsXML.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0)
            //    {
            //        DataRow dr = dsXML.Tables["ConsumerNLRDebtSummary"].Rows[0];

            //        if (!dsXML.Tables.Contains("ConsumerAdverseInfo") || dsXML.Tables["ConsumerAdverseInfo"] == null)
            //        {
            //            dr["NoofAccountdefaults"] = "0";
            //            dr["TotalAdverseAmt"] = "0";
            //            dr["MostRecentAdverseDate"] = "";
            //        }

            //        if (!dsXML.Tables.Contains("ConsumerDefaultAlert") || dsXML.Tables["ConsumerDefaultAlert"] == null)
            //        {
            //            dr["DefaultListingCount"] = "0";
            //            dr["DefaultListingAmt"] = "0";
            //            dr["RecentDefaultListingDate"] = "";
            //        }

            //        if (!dsXML.Tables.Contains("ConsumerJudgement") || dsXML.Tables["ConsumerJudgement"] == null)
            //        {
            //            dr["JudgementCount"] = "0";
            //            dr["TotalJudgmentAmt"] = "0";
            //            dr["MostRecentJudgmentDate"] = "";
            //        }

            //        if ((!dsXML.Tables.Contains("ConsumerAdminOrder") || dsXML.Tables["ConsumerAdminOrder"] == null) && (!dsXML.Tables.Contains("ConsumerSequestration") || dsXML.Tables["ConsumerSequestration"] == null) && (!dsXML.Tables.Contains("ConsumerRehabilitationOrder") || dsXML.Tables["ConsumerRehabilitationOrder"] == null))
            //        {
            //            dr["CourtnoticeCount"] = "0";
            //            dr["TotalCourtNoticeAmt"] = "0";
            //            dr["MostRecentCourtnoticeDate"] = "";
            //        }

            //        if (!dsXML.Tables.Contains("ConsumerDebtReviewStatus") || dsXML.Tables["ConsumerDebtReviewStatus"] == null)
            //        {
            //            dr["DebtReviewStatus"] = "";
            //        }

            //        if (!String.IsNullOrEmpty(dr["FTotalArrearAmount"].ToString()) && Single.Parse(dr["FTotalArrearAmount"].ToString()) > 0)
            //        {
            //            this.lblArrearamtvalue.BackColor = Color.White;
            //            // this.lblArrearamtCPAvalue.BackColor = Color.White;
            //            this.lblArrearamtTotvalue.BackColor = Color.White;
            //            this.lblArrearamtvalue.ForeColor = Color.Red;
            //            //  this.lblArrearamtCPAvalue.ForeColor = Color.Red;
            //            this.lblArrearamtTotvalue.ForeColor = Color.Red;

            //            this.lblArrearamtvalue.Font = new Font(lblArrearamtvalue.Font.FontFamily, lblArrearamtvalue.Font.Size, FontStyle.Bold);
            //            this.lblArrearamtCPAvalue.Font = new Font(lblArrearamtCPAvalue.Font.FontFamily, lblArrearamtCPAvalue.Font.Size, FontStyle.Bold);
            //            this.lblArrearamtTotvalue.Font = new Font(lblArrearamtTotvalue.Font.FontFamily, lblArrearamtTotvalue.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["HighestMonthsinArrears"].ToString()) && Int32.Parse(dr["HighestMonthsinArrears"].ToString()) > 2)
            //        {
            //            this.lblDelinquentratingValue.BackColor = Color.White;
            //            this.lblDelinquentratingCPAValue.BackColor = Color.White;
            //            this.lblDelinquentratingTotValue.BackColor = Color.White;
            //            this.lblDelinquentratingValue.ForeColor = Color.Red;
            //            this.lblDelinquentratingCPAValue.ForeColor = Color.Red;
            //            this.lblDelinquentratingTotValue.ForeColor = Color.Red;

            //            this.lblDelinquentratingValue.Font = new Font(lblDelinquentratingValue.Font.FontFamily, lblDelinquentratingValue.Font.Size, FontStyle.Bold);
            //            this.lblDelinquentratingCPAValue.Font = new Font(lblDelinquentratingCPAValue.Font.FontFamily, lblDelinquentratingCPAValue.Font.Size, FontStyle.Bold);
            //            this.lblDelinquentratingTotValue.Font = new Font(lblDelinquentratingTotValue.Font.FontFamily, lblDelinquentratingTotValue.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["FTotalAdverseAmount"].ToString()) && Single.Parse(dr["FTotalAdverseAmount"].ToString()) > 0)
            //        {
            //            this.lblAdverseAmtValue.BackColor = Color.White;
            //            this.lblAdverseAmtCPAValue.BackColor = Color.White;
            //            this.lblAdverseAmtTotValue.BackColor = Color.White;
            //            this.lblAdverseAmtValue.ForeColor = Color.Red;
            //            this.lblAdverseAmtCPAValue.ForeColor = Color.Red;
            //            this.lblAdverseAmtTotValue.ForeColor = Color.Red;

            //            this.lblAdverseAmtValue.Font = new Font(lblAdverseAmtValue.Font.FontFamily, lblAdverseAmtValue.Font.Size, FontStyle.Bold);
            //            this.lblAdverseAmtCPAValue.Font = new Font(lblAdverseAmtCPAValue.Font.FontFamily, lblAdverseAmtCPAValue.Font.Size, FontStyle.Bold);
            //            this.lblAdverseAmtTotValue.Font = new Font(lblAdverseAmtTotValue.Font.FontFamily, lblAdverseAmtTotValue.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["NoofAccountdefaults"].ToString()) && Single.Parse(dr["NoofAccountdefaults"].ToString()) > 0)
            //        {
            //            this.lblAccDefaultCount.BackColor = Color.White;
            //            this.lblADBalance.BackColor = Color.White;
            //            this.lblADDate.BackColor = Color.White;
            //            this.lblAccDefaultCount.ForeColor = Color.Red;
            //            this.lblADBalance.ForeColor = Color.Red;
            //            this.lblADDate.ForeColor = Color.Red;

            //            this.lblAccDefaultCount.Font = new Font(lblAccDefaultCount.Font.FontFamily, lblAccDefaultCount.Font.Size, FontStyle.Bold);
            //            this.lblADBalance.Font = new Font(lblADBalance.Font.FontFamily, lblADBalance.Font.Size, FontStyle.Bold);
            //            this.lblADDate.Font = new Font(lblADDate.Font.FontFamily, lblADDate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["DefaultListingCount"].ToString()) && Single.Parse(dr["DefaultListingCount"].ToString()) > 0)
            //        {
            //            this.lblDLCount.BackColor = Color.White;
            //            this.lblDLBalance.BackColor = Color.White;
            //            this.lblDLDate.BackColor = Color.White;
            //            this.lblDLCount.ForeColor = Color.Red;
            //            this.lblDLBalance.ForeColor = Color.Red;
            //            this.lblDLDate.ForeColor = Color.Red;

            //            this.lblDLCount.Font = new Font(lblDLCount.Font.FontFamily, lblDLCount.Font.Size, FontStyle.Bold);
            //            this.lblDLBalance.Font = new Font(lblDLBalance.Font.FontFamily, lblDLBalance.Font.Size, FontStyle.Bold);
            //            this.lblDLDate.Font = new Font(lblDLDate.Font.FontFamily, lblDLDate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["JudgementCount"].ToString()) && Single.Parse(dr["JudgementCount"].ToString()) > 0)
            //        {
            //            this.lblJudgCount.BackColor = Color.White;
            //            this.lblJudgBalance.BackColor = Color.White;
            //            this.lblJudgDate.BackColor = Color.White;
            //            this.lblJudgCount.ForeColor = Color.Red;
            //            this.lblJudgBalance.ForeColor = Color.Red;
            //            this.lblJudgDate.ForeColor = Color.Red;

            //            this.lblJudgCount.Font = new Font(lblJudgCount.Font.FontFamily, lblJudgCount.Font.Size, FontStyle.Bold);
            //            this.lblJudgBalance.Font = new Font(lblJudgBalance.Font.FontFamily, lblJudgBalance.Font.Size, FontStyle.Bold);
            //            this.lblJudgDate.Font = new Font(lblJudgDate.Font.FontFamily, lblJudgDate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["CourtNoticeCount"].ToString()) && Single.Parse(dr["CourtNoticeCount"].ToString()) > 0)
            //        {
            //            this.lblAOCount.BackColor = Color.White;
            //            this.lblAOBalance.BackColor = Color.White;
            //            this.lblAODate.BackColor = Color.White;
            //            this.lblAOCount.ForeColor = Color.Red;
            //            this.lblAOBalance.ForeColor = Color.Red;
            //            this.lblAODate.ForeColor = Color.Red;

            //            this.lblAOCount.Font = new Font(lblAOCount.Font.FontFamily, lblAOCount.Font.Size, FontStyle.Bold);
            //            this.lblAOBalance.Font = new Font(lblAOBalance.Font.FontFamily, lblAOBalance.Font.Size, FontStyle.Bold);
            //            this.lblAODate.Font = new Font(lblAODate.Font.FontFamily, lblAODate.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["DebtReviewStatus"].ToString()))
            //        {
            //            this.lblDebtReviewStatus.BackColor = Color.White;
            //            this.lblDebtReviewStatus.ForeColor = Color.Red;

            //            this.lblDebtReviewStatus.Font = new Font(lblDebtReviewStatus.Font.FontFamily, lblDebtReviewStatus.Font.Size, FontStyle.Bold);
            //        }
            //        if (!String.IsNullOrEmpty(dr["DisputeMessage"].ToString()))
            //        {
            //            this.lblDisputeMsg.BackColor = Color.White;
            //            this.lblDisputeMsg.ForeColor = Color.Red;
            //            this.lblDisputeMsg.Font = new Font(lblDisputeMsg.Font.FontFamily, lblDisputeMsg.Font.Size, FontStyle.Bold);
            //        }
            //    }

            //}
            //else if (dsXML.Tables.Contains("ConsumerCPANLRDebtSummary"))
            if (dsXML.Tables.Contains("ConsumerCPANLRDebtSummary"))
            {
                if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerAdverseInfo") || dsXML.Tables["ConsumerAdverseInfo"] == null))
                {
                    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["TotalAdverseAmt"] = 0;
                }
                if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerDefaultAlert") || dsXML.Tables["ConsumerDefaultAlert"] == null))
                {
                    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["DefaultListingAmt"] = 0;
                }
                //if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerJudgement") || dsXML.Tables["ConsumerJudgement"] == null))
                //{
                //    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["TotalJudgmentAmt"] = 0;
                //}
                if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0 && (!dsXML.Tables.Contains("ConsumerAdminOrder") || dsXML.Tables["ConsumerAdminOrder"] == null) && (!dsXML.Tables.Contains("ConsumerSequestration") || dsXML.Tables["ConsumerSequestration"] == null) && (!dsXML.Tables.Contains("ConsumerRehabilitationOrder") || dsXML.Tables["ConsumerRehabilitationOrder"] == null))
                {
                    dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0]["TotalCourtNoticeAmt"] = 0;
                }

                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalJudgmentAmt", System.Type.GetType(" System.Single"), "Iif(TotalJudgmentAmt = '', 0, TotalJudgmentAmt)");
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalCourtNoticeAmt", System.Type.GetType(" System.Single"), "Iif(TotalCourtNoticeAmt = '', 0, TotalCourtNoticeAmt)");
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FDefaultListingAmt", System.Type.GetType(" System.Single"), "Iif(DefaultListingAmt = '', 0, DefaultListingAmt)");
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalAdverseAmount", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmt = '', 0, TotalAdverseAmt)");
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FRehabilitationOrdersAmt", System.Type.GetType(" System.Single"), "Iif(RehabilitationOrdersAmt = '', 0, RehabilitationOrdersAmt)");
                
                //this.lblJudgBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalJudgmentAmt", "{0:c0}") });
                this.lblAOBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalCourtNoticeAmt", "{0:c0}") });
                this.lblADBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalAdverseAmount", "{0:c0}") });
                this.lblDLBalance.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FDefaultListingAmt", "{0:c0}") });
                this.lblROAmt.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FRehabilitationOrdersAmt", "{0:c0}") });

                //this.lblJudgCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.JudgementCount") });
                this.lblAOCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.CourtNoticeCount") });
                this.lblAccDefaultCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoofAccountdefaults") });
                this.lblDLCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.DefaultListingCount") });
                this.lblROCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.RehabilitationOrdersCount") });

                this.lblEnq24Count.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnqinLast24Months") });
                this.lblDebtReviewStatus.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.DebtReviewStatus") });
                this.lblDisputeMsg.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.DisputeMessage") });
                //this.lblEnq24Date.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.MostRecentEnqDateLast24Months") });

                //this.lblDLDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.RecentDefaultListingDate") });
                //this.lblADDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.MostRecentAdverseDate") });
                //this.lblAODate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.MostRecentCourtNoticeDate") });
                //this.lblJudgDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.MostRecentJudgmentDate") });

                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalMonthlyInstallmentNLR", System.Type.GetType(" System.Single"), "Iif(TotalMonthlyInstallmentNLR = '', 0, TotalMonthlyInstallmentNLR)");
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalOutStandingDebtNLR", System.Type.GetType(" System.Single"), "Iif(TotalOutStandingDebtNLR = '', 0, TotalOutStandingDebtNLR)");
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalArrearAmountNLR", System.Type.GetType(" System.Single"), "Iif(TotalArrearAmountNLR = '', 0, TotalArrearAmountNLR)");
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalAdverseAmountNLR", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmountNLR = '', 0, TotalAdverseAmountNLR)");

                //this.lblActiveAccValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOFActiveAccountsNLR") });
                //this.lblAccGoodvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountInGoodStandingNLR") });
                //this.lblDelinquentAccValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountInBadStandingNLR") });
                //this.lblMonthlyinstValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalMonthlyInstallmentNLR", "{0:c0}") });
                //this.lblOutstandingDebtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalOutStandingDebtNLR", "{0:c0}") });
                //this.lblArrearamtvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalArrearAmountNLR", "{0:c0}") });
                //this.lblDelinquentratingValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.HighestMonthsinArrearsNLR") });
                //this.lblAdverseAmtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalAdverseAmountNLR", "{0:c0}") });
                //this.lblenq90Value.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOWNNLR") });
                //this.lblenq90othervalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOTHNLR") });
                //this.lblAcc30Value.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountsOpenedinLast45DaysNLR") });
                //this.lblPaiduporcloseValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfPaidUpOrClosedAccountsNLR") });


                //CPA
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalMonthlyInstallmentCPA", System.Type.GetType(" System.Single"), "Iif(TotalMonthlyInstallmentCPA = '', 0, TotalMonthlyInstallmentCPA)");
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalOutStandingDebtCPA", System.Type.GetType(" System.Single"), "Iif(TotalOutStandingDebtCPA = '', 0, TotalOutStandingDebtCPA)");
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalArrearAmountCPA", System.Type.GetType(" System.Single"), "Iif(TotalArrearAmountCPA = '', 0, TotalArrearAmountCPA)");
                //dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalAdverseAmountCPA", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmountCPA = '', 0, TotalAdverseAmountCPA)");

                //this.lblActiveAccCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOFActiveAccountsCPA") });
                //this.lblAccGoodCPAvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountInGoodStandingCPA") });
                //this.lblDelinquentAccCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountInBadStandingCPA") });
                //this.lblMonthlyinstCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalMonthlyInstallmentCPA", "{0:c0}") });
                //this.lblOutstandingDebtCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalOutStandingDebtCPA", "{0:c0}") });
                //this.lblArrearamtCPAvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalArrearAmountCPA", "{0:c0}") });
                //this.lblDelinquentratingCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.HighestMonthsinArrearsCPA") });
                //this.lblAdverseAmtCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalAdverseAmountCPA", "{0:c0}") });
                //this.lblenq90CPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOWNCPA") });
                //this.lblenq90otherCPAvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOTHCPA") });
                //this.lblAcc30CPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountsOpenedinLast45DaysCPA") });
                //this.lblPaiduporcloseCPAValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfPaidUpOrClosedAccountsCPA") });

                // CPANLR

                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalMonthlyInstallment", System.Type.GetType(" System.Single"), "Iif(TotalMonthlyInstallment = '', 0, TotalMonthlyInstallment)");
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalOutStandingDebt", System.Type.GetType(" System.Single"), "Iif(TotalOutStandingDebt = '', 0, TotalOutStandingDebt)");
                dsXML.Tables["ConsumerCPANLRDebtSummary"].Columns.Add("FTotalArrearAmount", System.Type.GetType(" System.Single"), "Iif(TotalArrearAmount = '', 0, TotalArrearAmount)");
               
                this.lblActiveAccTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOFActiveAccounts") });
                this.lblActiveAccTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountInGoodStanding") });
                this.lblDelinquentAccTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountInBadStanding") });
                this.lblMonthlyinstTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalMonthlyInstallment", "{0:c0}") });
                this.lblOutstandingDebtTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalOutStandingDebt", "{0:c0}") });
                this.lblArrearamtTotvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalArrearAmount", "{0:c0}") });
                this.lblDelinquentratingTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.HighestMonthsinArrears") });
                //this.lblAdverseAmtTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.FTotalAdverseAmount", "{0:c0}") });
                //this.lblenq90TotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOWN") });
                this.lblenq90otherTotvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOTH") });
                this.lblAcc30TotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfAccountsOpenedinLast45Days") });
                //this.lblPaiduporcloseTotValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerCPANLRDebtSummary.NoOfPaidUpOrClosedAccounts") });

                if (dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0)
                {
                    DataRow dr = dsXML.Tables["ConsumerCPANLRDebtSummary"].Rows[0];

                    if (!dsXML.Tables.Contains("ConsumerAdverseInfo") || dsXML.Tables["ConsumerAdverseInfo"] == null)
                    {
                        dr["NoofAccountdefaults"] = "0";
                        dr["TotalAdverseAmt"] = "0";
                        //dr["MostRecentAdverseDate"] = "";
                    }

                    if (!dsXML.Tables.Contains("ConsumerDefaultAlert") || dsXML.Tables["ConsumerDefaultAlert"] == null)
                    {
                        dr["DefaultListingCount"] = "0";
                        dr["DefaultListingAmt"] = "0";
                        //dr["RecentDefaultListingDate"] = "";
                    }

                    //if (!dsXML.Tables.Contains("ConsumerJudgement") || dsXML.Tables["ConsumerJudgement"] == null)
                    //{
                        //dr["JudgementCount"] = "0";
                        //dr["TotalJudgmentAmt"] = "0";
                        //dr["MostRecentJudgmentDate"] = "";
                    //}

                    //if ((!dsXML.Tables.Contains("ConsumerAdminOrder") || dsXML.Tables["ConsumerAdminOrder"] == null) && (!dsXML.Tables.Contains("ConsumerSequestration") || dsXML.Tables["ConsumerSequestration"] == null) && (!dsXML.Tables.Contains("ConsumerRehabilitationOrder") || dsXML.Tables["ConsumerRehabilitationOrder"] == null))
                    //{
                    //    dr["CourtnoticeCount"] = "0";
                    //    dr["TotalCourtNoticeAmt"] = "0";
                        //dr["MostRecentCourtnoticeDate"] = "";
                    //}

                    if (!dsXML.Tables.Contains("ConsumerDebtReviewStatus") || dsXML.Tables["ConsumerDebtReviewStatus"] == null)
                    {
                        dr["DebtReviewStatus"] = "";
                    }

                    //if (!String.IsNullOrEmpty(dr["FTotalArrearAmount"].ToString()) && Single.Parse(dr["FTotalArrearAmount"].ToString()) > 0)
                    //{
                    //    // this.lblArrearamtvalue.BackColor = Color.White;
                    //    this.lblArrearamtCPAvalue.BackColor = Color.White;
                    //    this.lblArrearamtTotvalue.BackColor = Color.White;
                    //    // this.lblArrearamtvalue.ForeColor = Color.Red;
                    //    this.lblArrearamtCPAvalue.ForeColor = Color.Red;
                    //    this.lblArrearamtTotvalue.ForeColor = Color.Red;

                    //    //  this.lblArrearamtvalue.Font = new Font(lblArrearamtvalue.Font.FontFamily, lblArrearamtvalue.Font.Size, FontStyle.Bold);
                    //    this.lblArrearamtCPAvalue.Font = new Font(lblArrearamtCPAvalue.Font.FontFamily, lblArrearamtCPAvalue.Font.Size, FontStyle.Bold);
                    //    this.lblArrearamtTotvalue.Font = new Font(lblArrearamtTotvalue.Font.FontFamily, lblArrearamtTotvalue.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["HighestMonthsinArrears"].ToString()) && Int32.Parse(dr["HighestMonthsinArrears"].ToString()) > 2)
                    //{
                    //    this.lblDelinquentratingTotValue.BackColor = Color.White;
                    //    this.lblDelinquentratingCPAValue.BackColor = Color.White;
                    //    this.lblDelinquentratingTotValue.BackColor = Color.White;
                    //    this.lblDelinquentratingTotValue.ForeColor = Color.Red;
                    //    this.lblDelinquentratingCPAValue.ForeColor = Color.Red;
                    //    this.lblDelinquentratingTotValue.ForeColor = Color.Red;

                    //    this.lblDelinquentratingTotValue.Font = new Font(lblDelinquentratingTotValue.Font.FontFamily, lblDelinquentratingTotValue.Font.Size, FontStyle.Bold);
                    //    this.lblDelinquentratingCPAValue.Font = new Font(lblDelinquentratingCPAValue.Font.FontFamily, lblDelinquentratingCPAValue.Font.Size, FontStyle.Bold);
                    //    this.lblDelinquentratingTotValue.Font = new Font(lblDelinquentratingTotValue.Font.FontFamily, lblDelinquentratingTotValue.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["FTotalAdverseAmount"].ToString()) && Single.Parse(dr["FTotalAdverseAmount"].ToString()) > 0)
                    //{
                    //    this.lblAdverseAmtValue.BackColor = Color.White;
                    //    this.lblAdverseAmtCPAValue.BackColor = Color.White;
                    //    this.lblAdverseAmtTotValue.BackColor = Color.White;
                    //    this.lblAdverseAmtValue.ForeColor = Color.Red;
                    //    this.lblAdverseAmtCPAValue.ForeColor = Color.Red;
                    //    this.lblAdverseAmtTotValue.ForeColor = Color.Red;

                    //    this.lblAdverseAmtValue.Font = new Font(lblAdverseAmtValue.Font.FontFamily, lblAdverseAmtValue.Font.Size, FontStyle.Bold);
                    //    this.lblAdverseAmtCPAValue.Font = new Font(lblAdverseAmtCPAValue.Font.FontFamily, lblAdverseAmtCPAValue.Font.Size, FontStyle.Bold);
                    //    this.lblAdverseAmtTotValue.Font = new Font(lblAdverseAmtTotValue.Font.FontFamily, lblAdverseAmtTotValue.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["NoofAccountdefaults"].ToString()) && Single.Parse(dr["NoofAccountdefaults"].ToString()) > 0)
                    //{
                    //    this.lblAccDefaultCount.BackColor = Color.White;
                    //    this.lblADBalance.BackColor = Color.White;
                    //    this.lblADDate.BackColor = Color.White;
                    //    this.lblAccDefaultCount.ForeColor = Color.Red;
                    //    this.lblADBalance.ForeColor = Color.Red;
                    //    this.lblADDate.ForeColor = Color.Red;

                    //    this.lblAccDefaultCount.Font = new Font(lblAccDefaultCount.Font.FontFamily, lblAccDefaultCount.Font.Size, FontStyle.Bold);
                    //    this.lblADBalance.Font = new Font(lblADBalance.Font.FontFamily, lblADBalance.Font.Size, FontStyle.Bold);
                    //    this.lblADDate.Font = new Font(lblADDate.Font.FontFamily, lblADDate.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["DefaultListingCount"].ToString()) && Single.Parse(dr["DefaultListingCount"].ToString()) > 0)
                    //{
                    //    this.lblDLCount.BackColor = Color.White;
                    //    this.lblDLBalance.BackColor = Color.White;
                    //    this.lblDLDate.BackColor = Color.White;
                    //    this.lblDLCount.ForeColor = Color.Red;
                    //    this.lblDLBalance.ForeColor = Color.Red;
                    //    this.lblDLDate.ForeColor = Color.Red;

                    //    this.lblDLCount.Font = new Font(lblDLCount.Font.FontFamily, lblDLCount.Font.Size, FontStyle.Bold);
                    //    this.lblDLBalance.Font = new Font(lblDLBalance.Font.FontFamily, lblDLBalance.Font.Size, FontStyle.Bold);
                    //    this.lblDLDate.Font = new Font(lblDLDate.Font.FontFamily, lblDLDate.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["JudgementCount"].ToString()) && Single.Parse(dr["JudgementCount"].ToString()) > 0)
                    //{
                    //    this.lblJudgCount.BackColor = Color.White;
                    //    this.lblJudgBalance.BackColor = Color.White;
                    //    this.lblJudgDate.BackColor = Color.White;
                    //    this.lblJudgCount.ForeColor = Color.Red;
                    //    this.lblJudgBalance.ForeColor = Color.Red;
                    //    this.lblJudgDate.ForeColor = Color.Red;

                    //    this.lblJudgCount.Font = new Font(lblJudgCount.Font.FontFamily, lblJudgCount.Font.Size, FontStyle.Bold);
                    //    this.lblJudgBalance.Font = new Font(lblJudgBalance.Font.FontFamily, lblJudgBalance.Font.Size, FontStyle.Bold);
                    //    this.lblJudgDate.Font = new Font(lblJudgDate.Font.FontFamily, lblJudgDate.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["CourtNoticeCount"].ToString()) && Single.Parse(dr["CourtNoticeCount"].ToString()) > 0)
                    //{
                    //    this.lblAOCount.BackColor = Color.White;
                    //    this.lblAOBalance.BackColor = Color.White;
                    //    this.lblAODate.BackColor = Color.White;
                    //    this.lblAOCount.ForeColor = Color.Red;
                    //    this.lblAOBalance.ForeColor = Color.Red;
                    //    this.lblAODate.ForeColor = Color.Red;

                    //    this.lblAOCount.Font = new Font(lblAOCount.Font.FontFamily, lblAOCount.Font.Size, FontStyle.Bold);
                    //    this.lblAOBalance.Font = new Font(lblAOBalance.Font.FontFamily, lblAOBalance.Font.Size, FontStyle.Bold);
                    //    this.lblAODate.Font = new Font(lblAODate.Font.FontFamily, lblAODate.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["DebtReviewStatus"].ToString()))
                    //{
                    //    this.lblDebtReviewStatus.BackColor = Color.White;
                    //    this.lblDebtReviewStatus.ForeColor = Color.Red;
                    //    this.lblDebtReviewStatus.Font = new Font(lblDebtReviewStatus.Font.FontFamily, lblDebtReviewStatus.Font.Size, FontStyle.Bold);
                    //}
                    //if (!String.IsNullOrEmpty(dr["DisputeMessage"].ToString()))
                    //{
                    //    this.lblDisputeMsg.BackColor = Color.White;
                    //    this.lblDisputeMsg.ForeColor = Color.Red;
                    //    this.lblDisputeMsg.Font = new Font(lblDisputeMsg.Font.FontFamily, lblDisputeMsg.Font.Size, FontStyle.Bold);
                    //}
                }
            }

            //if (!(dsXML.Tables.Contains("ConsumerAccountGoodBadSummary")))
            //{
            //    DetailAccountGoodBadSummary.Visible = false;
            //    //lblCrAccStatus.Text += " - (No Data Available)";
            //    lblGBSumNoInfo.Visible = true;
            //}

            if (!(dsXML.Tables.Contains("ConsumerAccountStatus")))
            {
                //DetailAccountStatus.Visible = false;
                ////lblAccStatusH.Text += " - (No Data Available)";
                //CASLegend.Visible = false;
                //GroupHeader7.Visible = false;
                //tblAccStatusC.Visible = false;
                //MonthlyPaymentHeader.Visible = false;
                //MonthlyPayment.Visible = false;
                //ConsumerDefinition.Visible = false;
                //lblCPANoInfo.Visible = true;
            }
            else
            {
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FDateAccountOpened", System.Type.GetType("System.DateTime"), "Iif(AccountOpenedDate='','1900-01-01T00:00:00',AccountOpenedDate)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FCreditLimitAmt", System.Type.GetType("System.Double"), "Iif(CreditLimitAmt='',0,CreditLimitAmt)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FCurrentBalanceAmt", System.Type.GetType("System.Double"), "Iif(CurrentBalanceAmt='',0,CurrentBalanceAmt)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FMonthlyInstalmentAmt", System.Type.GetType("System.Double"), "Iif(MonthlyInstalmentAmt='',0,MonthlyInstalmentAmt)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FArrearsAmt", System.Type.GetType("System.Double"), "Iif(ArrearsAmt='',0,ArrearsAmt)");

                //this.lblAccOpenDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FDateAccountOpened", "{0:dd/MM/yyyy}") });
                this.lblCreditLimitD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FCreditLimitAmt", "{0:c0}") });
                this.lblCurrBalanceD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FCurrentBalanceAmt", "{0:c0}") });
                this.lblInstallementD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FMonthlyInstalmentAmt", "{0:c0}") });
                this.lblArrearsAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FArrearsAmt", "{0:c0}") });
            }

            //if ((!(dsXML.Tables.Contains("Consumer24MonthlyPaymentHeader"))) || (!(dsXML.Tables.Contains("Consumer24MonthlyPayment"))))
            //{
            //    //PageinfoMonthlyPaymentH.Format += " - (No Data Available)";
            //    lblmonthlynoinfo.Visible = true;
            //    MonthlyPaymentHeader.Visible = false;
            //    MonthlyPayment.Visible = false;
            //    ConsumerDefinition.Visible = false;
            //}

            //if (!(dsXML.Tables.Contains("ConsumerNLRAccountStatus")))
            //{
            //    DetailNLRAccountStatus.Visible = false;
            //    //lblNLRAccStatusH.Text += " - (No Data Available)";
            //    NLRLegend.Visible = false;
            //    GroupHeader26.Visible = false;
            //    tblNLRAccStatusC.Visible = false;
            //    NLRMonthlyPaymentHeader.Visible = false;
            //    NLRMonthlyPayment.Visible = false;
            //    NLRConsumerDefinition.Visible = false;
            //    lblNLRNoInfo.Visible = true;
            //}
            //else
            //{

            //    dsXML.Tables["ConsumerNLRAccountStatus"].Columns.Add("FCreditLimitAmt", System.Type.GetType("System.Double"), "Iif(CreditLimitAmt='',0,CreditLimitAmt)");
            //    dsXML.Tables["ConsumerNLRAccountStatus"].Columns.Add("FCurrentBalanceAmt", System.Type.GetType("System.Double"), "Iif(CurrentBalanceAmt='',0,CurrentBalanceAmt)");
            //    dsXML.Tables["ConsumerNLRAccountStatus"].Columns.Add("FMonthlyInstalmentAmt", System.Type.GetType("System.Double"), "Iif(MonthlyInstalmentAmt='',0,MonthlyInstalmentAmt)");
            //    dsXML.Tables["ConsumerNLRAccountStatus"].Columns.Add("FArrearsAmt", System.Type.GetType("System.Double"), "Iif(ArrearsAmt='',0,ArrearsAmt)");

            //    this.lblNLRCreditLimitD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRAccountStatus.FCreditLimitAmt", "{0:c0}") });
            //    this.lblNLRCurrBalanceD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRAccountStatus.FCurrentBalanceAmt", "{0:c0}") });
            //    this.lblNLRInstallementD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRAccountStatus.FMonthlyInstalmentAmt", "{0:c0}") });
            //    this.lblNLRArrearsAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNLRAccountStatus.FArrearsAmt", "{0:c0}") });
            //}

            //if ((!(dsXML.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))) || (!(dsXML.Tables.Contains("ConsumerNLR24MonthlyPayment"))))
            //{
            //    //PageinfoNLRMonthlyPaymentH.Format += " - (No Data Available)";
            //    lblnlrmonthlynoinfo.Visible = true;
            //    NLRMonthlyPaymentHeader.Visible = false;
            //    NLRMonthlyPayment.Visible = false;
            //    NLRConsumerDefinition.Visible = false;
            //}

            //if (!(dsXML.Tables.Contains("ConsumerNameHistory")))
            //{
            //    DetailNameHistory.Visible = false;
            //    lblNameHistH.Text += " - (No Data Available)";
            //    tblNameHisC.Visible = false;
            //    lblNameHistH.Visible = false;
            //}
            //else
            //{
            //    dsXML.Tables["ConsumerNameHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
            //    dsXML.Tables["ConsumerNameHistory"].Columns.Add("FBirthDate", System.Type.GetType("System.DateTime"), "Iif(BirthDate='','1900-01-01T00:00:00',BirthDate)");
            //    this.lblDUpdatedDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNameHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            //    this.lblHDBirthDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNameHistory.FBirthDate", "{0:dd/MM/yyyy}") });
            //}

            if (!(dsXML.Tables.Contains("ConsumerAddressHistory")))
            {
                DetailAddressHistory.Visible = false;
                //lblAddHistoryH.Text += " - (No Data Available)";
                //tblAddHC.Visible = false;
                //lblAHNoInfo.Visible = true;
            }
            //else
            //{
            //    dsXML.Tables["ConsumerAddressHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
            //    this.lblDAddUpdatedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAddressHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            //}

            if (!(dsXML.Tables.Contains("ConsumerTelephoneHistory")))
            {
                DetailTelephoneHistory.Visible = false;
                //lblContactNoH.Text += " - (No Data Available)";
                //tblContactNoC.Visible = false;
                //lblTelNoInfo.Visible = true;
            }
            //else
            //{
            //    dsXML.Tables["ConsumerTelephoneHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
            //    this.lblDNoUpdatedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerTelephoneHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            //}

            if (!(dsXML.Tables.Contains("ConsumerEmailHistory")))
            {
                DetailEmailAddressHistory.Visible = false;
                //tblEmailC.Visible = false;
                //lblEmailInfo.Visible = true;
            }

            if (!(dsXML.Tables.Contains("ConsumerEmploymentHistory")))
            {
                DetailEmploymentHistory.Visible = false;
                //lblEmploymentH.Text += " - (No Data Available)";
                //tblEmploymentC.Visible = false;
                //lblempNoinfo.Visible = true;
            }
            //else
            //{
            //    dsXML.Tables["ConsumerEmploymentHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
            //    this.lblDEmpUpdatedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerEmploymentHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            //}


            if (!(dsXML.Tables.Contains("ConsumerAdverseInfo")))
            {
                DetailAdverseInfo.Visible = false;
                //lblAdverseH.Text += " - (No Data Available)";
                //tblAdverseColumns.Visible = false;
                //lbladverseNoInfo.Visible = true;
            }
            else
            {
                //dsXML.Tables["ConsumerAdverseInfo"].Columns.Add("FActionDate", System.Type.GetType("System.DateTime"), "Iif(ActionDate='','1900-01-01T00:00:00',ActionDate)");
                dsXML.Tables["ConsumerAdverseInfo"].Columns.Add("FCurrentBalanceAmt", System.Type.GetType("System.Double"), "Iif(CurrentBalanceAmt='',0,CurrentBalanceAmt)");

                // this.lblDActionDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdverseInfo.FActionDate", "{0:dd/MM/yyyy}") });
                this.lblDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdverseInfo.FCurrentBalanceAmt", "{0:c0}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerJudgement")))
            {
                //DetailJudgements.Visible = false;
                ////lblJudgmentsH.Text += " - (No Data Available)";
                //tblCJud.Visible = false;
                //lbljudgNoInfo.Visible = true;
            }
            else
            {
                // dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["ConsumerJudgement"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.lblDAAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FDisputeAmt", "{0:c0}") });
            }

            //if (!(dsXML.Tables.Contains("ConsumerAdminOrder")))
            //{
            //    DetailAdminOrders.Visible = false;
            //    //lblAOH.Text += " - (No Data Available)";
            //    tblCAO.Visible = false;
            //    lblAonoInfo.Visible = true;
            //}
            //else
            //{
            //    // dsXML.Tables["ConsumerAdminOrder"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
            //    // dsXML.Tables["ConsumerAdminOrder"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
            //    dsXML.Tables["ConsumerAdminOrder"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

            //    // this.lblAODIssuedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdminOrder.FCaseFilingDate", "{0:dd/MM/yyyy}") });
            //    // this.lblAODLoadeddate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdminOrder.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            //    this.lblAODAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdminOrder.FDisputeAmt", "{0:c0}") });
            //}

            //if (!(dsXML.Tables.Contains("ConsumerSequestration")))
            //{
            //    DetailSequestration.Visible = false;
            //    //lblSEQH.Text += " - (No Data Available)";
            //    tblSEQC.Visible = false;
            //    lblSEQNoInfo.Visible = true;
            //}
            //else
            //{
            //    // dsXML.Tables["ConsumerSequestration"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
            //    //dsXML.Tables["ConsumerSequestration"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
            //    dsXML.Tables["ConsumerSequestration"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

            //    // this.lblSEQDIssuedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerSequestration.FCaseFilingDate", "{0:dd/MM/yyyy}") });
            //    // this.lblSEQDLoadedDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerSequestration.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            //    this.lblSEQDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerSequestration.FDisputeAmt", "{0:c0}") });
            //}
            if (!(dsXML.Tables.Contains("ConsumerRehabilitationOrder")))
            {
                //DetailRehabilitation.Visible = false;
                ////lblREHH.Text += " - (No Data Available)";
                //tblREHC.Visible = false;
                //lblREHNoInfo.Visible = true;
            }
            else
            {
                //dsXML.Tables["ConsumerRehabilitation"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                //dsXML.Tables["ConsumerRehabilitation"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                dsXML.Tables["ConsumerRehabilitationOrder"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Double"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblREHDIssuedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerRehabilitation.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                //this.lblREHDLoadedDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerRehabilitation.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
                this.lblREHDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerRehabilitationOrder.FDisputeAmt", "{0:c0}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerDefaultAlert")))
            {
                //DetailDefaultAlert.Visible = false;
                ////lblDefaultAlertH.Text += " - (No Data Available)";
                //tblDefaultAlertC.Visible = false;
                //lblDLNoInfo.Visible = true;
            }
            else
            {
                // dsXML.Tables["ConsumerDefaultAlert"].Columns.Add("FDateLoaded", System.Type.GetType("System.DateTime"), "Iif(DateLoaded='','1900-01-01T00:00:00',DateLoaded)");
                dsXML.Tables["ConsumerDefaultAlert"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                // this.lblDADLoadeddateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDefaultAlert.FDateLoaded", "{0:dd/MM/yyyy}") });
                this.lblDAAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDefaultAlert.FAmount", "{0:c0}") });

            }

            //if (!(dsXML.Tables.Contains("ConsumerDebtReviewStatus")))
            //{
            //    DetailDebtReviewStatus.Visible = false;
            //    //lbldbtReviewH.Text += " - (No Data Available)";
            //    lblDRNoInfo.Visible = true;
            //}
            ////else
            ////{
            ////    dsXML.Tables["ConsumerDebtReviewStatus"].Columns.Add("FDebtReviewStatusDate", System.Type.GetType("System.DateTime"), "Iif(DebtReviewStatusDate='','1900-01-01T00:00:00',DebtReviewStatusDate)");
            ////    this.lblDbtReviewDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtReviewStatus.FDebtReviewStatusDate", "{0:dd/MM/yyyy}") });
            ////}

            //if (!(dsXML.Tables.Contains("ConsumerEnquiryHistory")))
            //{
            //    DetailEnquiryHistory.Visible = false;
            //    //lblEnquiryH.Text += " - (No Data Available)";
            //    tblEnquiryC.Visible = false;
            //    lblEnqHisNoInfo.Visible = true;
            //}

            if (!(dsXML.Tables.Contains("ConsumerPropertyInformation")))
            {
                //DetailpropertyInfo.Visible = false;
                ////lblPropInterestsH.Text += " - (No Data Available)";
                //lblPropNoInfo.Visible = true;
            }
            else
            {
                // dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FPurchaseDate", System.Type.GetType("System.DateTime"), "Iif(PurchaseDate='','1900-01-01T00:00:00',PurchaseDate)");
                dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FPurchasePriceAmt", System.Type.GetType("System.Double"), "Iif(PurchasePriceAmt='',0,PurchasePriceAmt)");
                dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FBondAmt", System.Type.GetType("System.Double"), "Iif(BondAmt='',0,BondAmt)");

                // this.lblDPurchasedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FPurchaseDate", "{0:dd/MM/yyyy}") });
                this.lblDPurchaseprice.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FPurchasePriceAmt", "{0:c0}") });
                this.lblDBondAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FBondAmt", "{0:c0}") });
            }


            if (!(dsXML.Tables.Contains("ConsumerDirectorShipLink")))
            {
                //DetailDirectorshipLink.Visible = false;
                ////lblDirectorlinksH.Text += " - (No Data Available)";
                //lblDirNoInfo.Visible = true;
            }
            //else
            //{
            //    dsXML.Tables["ConsumerDirectorShipLink"].Columns.Add("FAppointmentDate", System.Type.GetType("System.DateTime"), "Iif(AppointmentDate='','1900-01-01T00:00:00',AppointmentDate)");
            //    this.lblDAppDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDirectorShipLink.FAppointmentDate", "{0:dd/MM/yyyy}") });
            //}

            if (!(dsXML.Tables.Contains("ConsumerOtherContactInfoAddress")))
            {
                DetailOtherContactAddress.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            }
            if (!(dsXML.Tables.Contains("ConsumerOtherContactInfoTelephone")))
            {
                DetailOtherContactTelephone.Visible = false;
                //tblTelephoneInfoC.Visible = false;
                //lblTelephoneInfoH.Text += " - (No Data Available)";
            }

            if (!(dsXML.Tables.Contains("ConsumerAddressConfirmation")))
            {
                Detail1.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            }
            if (!(dsXML.Tables.Contains("ConsumerEmploymentConfirmation")))
            {
                Detail3.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            } 
            if (!(dsXML.Tables.Contains("ConsumerOtherContactInfoAddress")))
            {
                Detail2.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            }

            if (!(dsXML.Tables.Contains("ConsumerTelephoneLinkageHome")))
            {
                Detail4.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            }
            if (!(dsXML.Tables.Contains("ConsumerTelephoneLinkageWork")))
            {
                Detail8.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            }
            if (!(dsXML.Tables.Contains("ConsumerTelephoneLinkageCellular")))
            {
                Detail9.Visible = false;
                //tblAddressc.Visible = false;
                //lblAddressH.Text += " - (No Data Available)";
            }

            if (!(dsXML.Tables.Contains("XDSPaymentNotification")))
            {
                //DetailPaymentNotification.Visible = false;
                ////lblPaymentNotificationH.Text += " - (No Data Available)";
                //tblPmtNotificationC.Visible = false;
                //lblPNNoInfo.Visible = true;
            }
            else
            {
                dsXML.Tables["XDSPaymentNotification"].Columns.Add("FAmount", System.Type.GetType("System.Double"), "Iif(Amount='',0,Amount)");

                this.lblPmtAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "XDSPaymentNotification.FAmount", "{0:c0}") });

            }


            this.DataSource = dsXML;
            dsXML.Dispose();
        }

        public string GetImageUrl(string sValue)
        {
            string imgurl = @"~\images\cr_nodata.png";

            if (sValue == "#")
            {
                imgurl = @"~\images\cr_nodata.png";
            }
            else if (sValue == "*")
            {
                imgurl = @"~\images\cr_repeat.png";
            }
            else if (sValue == "0")
            {
                imgurl = @"~\images\0.png";
            }
            else if (sValue == "1")
            {
                imgurl = @"~\images\cr_arrears1m.png";
            }
            else if (sValue == "2")
            {
                imgurl = @"~\images\cr_arrears2m.png";
            }
            else if (sValue == "3")
            {
                imgurl = @"~\images\cr_arrears3m.png";
            }
            else if (sValue == "4")
            {
                imgurl = @"~\images\cr_arrears4m.png";
            }
            else if (sValue == "5")
            {
                imgurl = @"~\images\cr_arrears5m.png";
            }
            else if (sValue == "6")
            {
                imgurl = @"~\images\cr_arrears6m.png";
            }
            else if (sValue == "7")
            {
                imgurl = @"~\images\cr_arrears7m.png";
            }
            else if (sValue == "8")
            {
                imgurl = @"~\images\cr_arrears8m.png";
            }
            else if (sValue == "9")
            {
                imgurl = @"~\images\cr_arrears9m.png";
            }
            else if (sValue == "1-2")
            {
                imgurl = @"~\images\cr_arrears2.png";
            }
            else if (sValue == "3-9")
            {
                imgurl = @"~\images\cr_arrears.png";
            }
            else if (sValue == "A")
            {
                imgurl = @"~\images\A.png";
            }
            else if (sValue == "AA")
            {
                imgurl = @"~\images\AA.png";
            }
            else if (sValue == "AB")
            {
                imgurl = @"~\images\AB.png";
            }
            else if (sValue == "AC")
            {
                imgurl = @"~\images\AC.png";
            }
            else if (sValue == "C")
            {
                imgurl = @"~\images\C.png";
            }
            else if (sValue == "D")
            {
                imgurl = @"~\images\D.png";
            }
            else if (sValue == "E")
            {
                imgurl = @"~\images\E.png";
            }
            else if (sValue == "F")
            {
                imgurl = @"~\images\F.png";
            }
            else if (sValue == "G")
            {
                imgurl = @"~\images\G.png";
            }
            else if (sValue == "H")
            {
                imgurl = @"~\images\H.png";
            }
            else if (sValue == "I")
            {
                imgurl = @"~\images\I.png";
            }
            else if (sValue == "J")
            {
                imgurl = @"~\images\J.png";
            }
            else if (sValue == "K")
            {
                imgurl = @"~\images\K.png";
            }
            else if (sValue == "L")
            {
                imgurl = @"~\images\L.png";
            }
            else if (sValue == "M")
            {
                imgurl = @"~\images\M.png";
            }
            else if (sValue == "N")
            {
                imgurl = @"~\images\N.png";
            }
            else if (sValue == "O")
            {
                imgurl = @"~\images\O.png";
            }
            else if (sValue == "P")
            {
                imgurl = @"~\images\P.png";
            }
            else if (sValue == "Q")
            {
                imgurl = @"~\images\Q.png";
            }
            else if (sValue == "R")
            {
                imgurl = @"~\images\R.png";
            }
            else if (sValue == "S")
            {
                imgurl = @"~\images\S.png";
            }
            else if (sValue == "T")
            {
                imgurl = @"~\images\T.png";
            }
            else if (sValue == "U")
            {
                imgurl = @"~\images\U.png";
            }
            else if (sValue == "V")
            {
                imgurl = @"~\images\V.png";
            }
            else if (sValue == "W")
            {
                imgurl = @"~\images\W.png";
            }
            else if (sValue == "X")
            {
                imgurl = @"~\images\X.png";
            }
            else if (sValue == "Y")
            {
                imgurl = @"~\images\Y.png";
            }
            else if (sValue == "Z")
            {
                imgurl = @"~\images\Z.png";
            }
            else
            {
                imgurl = @"~\images\cr_nodata.png";
            }

            return imgurl;
        }

        private void pbm01_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm01.ImageUrl = GetImageUrl(lblmonth01.Text);
        }

        private void pbm02_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm02.ImageUrl = GetImageUrl(lblmonth02.Text);
        }

        private void pbm03_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm03.ImageUrl = GetImageUrl(lblmonth03.Text);
        }

        private void pbm04_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm04.ImageUrl = GetImageUrl(lblmonth04.Text);
        }

        private void pbm05_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm05.ImageUrl = GetImageUrl(lblmonth05.Text);
        }

        private void pbm06_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm06.ImageUrl = GetImageUrl(lblmonth06.Text);
        }

        private void pbm07_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm07.ImageUrl = GetImageUrl(lblmonth07.Text);
        }

        private void pbm08_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm08.ImageUrl = GetImageUrl(lblmonth08.Text);
        }

        private void pbm09_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm09.ImageUrl = GetImageUrl(lblmonth09.Text);
        }

        private void pbm10_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm10.ImageUrl = GetImageUrl(lblmonth10.Text);
        }

        private void pbm11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm11.ImageUrl = GetImageUrl(lblmonth11.Text);
        }

        private void pbm12_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm12.ImageUrl = GetImageUrl(lblmonth12.Text);
        }

        private void pbm13_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm13.ImageUrl = GetImageUrl(lblmonth13.Text);
        }

        private void pbm14_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm14.ImageUrl = GetImageUrl(lblmonth14.Text);
        }

        private void pbm15_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm15.ImageUrl = GetImageUrl(lblmonth15.Text);
        }

        private void pbm16_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm16.ImageUrl = GetImageUrl(lblmonth16.Text);
        }

        private void pbm17_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm17.ImageUrl = GetImageUrl(lblmonth17.Text);
        }

        private void pbm18_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm18.ImageUrl = GetImageUrl(lblmonth18.Text);
        }

        private void pbm19_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm19.ImageUrl = GetImageUrl(lblmonth19.Text);
        }

        private void pbm20_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm20.ImageUrl = GetImageUrl(lblmonth20.Text);
        }

        private void pbm21_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm21.ImageUrl = GetImageUrl(lblmonth21.Text);
        }

        private void pbm22_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm22.ImageUrl = GetImageUrl(lblmonth22.Text);
        }

        private void pbm23_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm23.ImageUrl = GetImageUrl(lblmonth23.Text);
        }

        private void pbm24_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbm24.ImageUrl = GetImageUrl(lblmonth24.Text);
        }

        private void pbDef_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pbDef.ImageUrl = GetImageUrl(lblDef.Text);
        }

        private void lblScore_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!String.IsNullOrEmpty(lblScore.Text))
            {
                if (Convert.ToDouble(lblScore.Text) <= 659)
                    lblScore.ForeColor = System.Drawing.Color.Red;
                if (Convert.ToDouble(lblScore.Text) >= 660 && Convert.ToDouble(lblScore.Text) <= 739)
                    lblScore.ForeColor = System.Drawing.Color.Orange;
                if (Convert.ToDouble(lblScore.Text) >= 740 && Convert.ToDouble(lblScore.Text) <= 839)
                    lblScore.ForeColor = System.Drawing.Color.Blue;
                if (Convert.ToDouble(lblScore.Text) >= 840 && Convert.ToDouble(lblScore.Text) <= 1000)
                    lblScore.ForeColor = System.Drawing.Color.Green;
            }
        }
    }
}
