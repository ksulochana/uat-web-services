﻿namespace XDSConsumerDirectReport
{
    partial class CreditReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.PersonalDetails = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailPersonalDetails = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBirthDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderPersonalDetails = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.FraudIndicators = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailFraudIndicators = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderFraudIndicators = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PresageScore = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailPresageScore = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblScore = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.lblM01 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupHeaderPresageScore = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DebtSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDebtSummary = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActiveAccTotValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow125 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccGoodTotvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow126 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentAccTotValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow127 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMonthlyinstTotValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow128 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOutstandingDebtTotValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow129 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearamtTotvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow130 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentratingTotValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow131 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOBalance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow132 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblenq90otherTotvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow133 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAcc30TotValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow134 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccDefaultCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblADBalance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow135 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDLCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDLBalance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow136 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblROCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblROAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow137 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEnq24Count = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow138 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPurchPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow139 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow140 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDebtReviewStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow141 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDisputeMsg = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderDebtSummary = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDishonouredChequeHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditLimitD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrBalanceD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInstallementD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearsAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell433 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblmonth01 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm01 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth02 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm02 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth03 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm03 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth04 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm04 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth05 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm05 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth06 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm06 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth07 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm07 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth08 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm08 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth09 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm09 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm10 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm11 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm12 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm13 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm14 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm15 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm16 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm17 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm18 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm19 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm20 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm21 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm22 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm23 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblmonth24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbm24 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblMonthlyPaymentHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM02 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM03 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM04 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM05 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM06 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM07 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM08 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM09 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AdverseDomainRecords = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Adverse = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAdverseInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderAdverse = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Judgment = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailJudgment = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable57 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderJudgment = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport8 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable69 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow143 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable67 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow142 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable68 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow144 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Rehabilitation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailRehabilitation = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblREHDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderRehabilitation = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DebtReview = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDebtReview = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderDebtReview = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PaymentNotifications = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailPaymentNotifications = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPmtAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderPaymentNotifications = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.EnquiryHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailEnquiryHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderEnquiryHistory = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ContactHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeaderContactHistory = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow80 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AddressHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAddressHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable45 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderAddressHistory = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport9 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailTelephoneHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable72 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow149 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport10 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailEmailAddressHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable46 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable70 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow145 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow146 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport11 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailOtherContactAddress = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable73 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow151 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell410 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable71 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow147 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow148 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport12 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailOtherContactTelephone = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable74 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow152 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell412 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader14 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable56 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow92 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ConfirmedInformation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailConfirmedInformation = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeaderConfirmedInformation = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable47 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable58 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow114 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable60 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow116 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable59 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow115 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow111 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow112 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TelephoneLinkages = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailTelephoneLinkages = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeaderTelephoneLinkages = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable48 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable62 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow118 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable61 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow113 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow117 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport6 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable65 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow123 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable63 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow119 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow120 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport7 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable66 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow124 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable64 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow121 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow122 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.EmploymentHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailEmploymentHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable53 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow99 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderEmploymentHistory = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable52 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow100 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable49 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PropertyInterest = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailPropertyInterest = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable54 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow101 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow102 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow103 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow104 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow105 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow106 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderPropertyInterest = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable50 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DirectorshipLinks = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDirectorshipLinks = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable55 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow107 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow108 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow109 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow110 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeaderDirectorshipLinks = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable51 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow97 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow98 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport14 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail17 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDef = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbDef = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDishonouredChequeHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 15F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 25F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.PageHeader.HeightF = 129.375F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable8
            // 
            this.xrTable8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable8.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.ForeColor = System.Drawing.Color.Gray;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(12.50013F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47});
            this.xrTable8.SizeF = new System.Drawing.SizeF(877.4999F, 114.375F);
            this.xrTable8.StylePriority.UseBackColor = false;
            this.xrTable8.StylePriority.UseBorderColor = false;
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseForeColor = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell181});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.BackColor = System.Drawing.Color.White;
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell109.BorderWidth = 0;
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox2});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBackColor = false;
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.StylePriority.UseBorderWidth = false;
            this.xrTableCell109.Weight = 1.0917721518987342D;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 10.00001F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(150.5F, 89.99999F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.BackColor = System.Drawing.Color.White;
            this.xrTableCell181.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell181.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell181.BorderWidth = 0;
            this.xrTableCell181.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17,
            this.xrLabel18});
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.StylePriority.UseBackColor = false;
            this.xrTableCell181.StylePriority.UseBorderColor = false;
            this.xrTableCell181.StylePriority.UseBorders = false;
            this.xrTableCell181.StylePriority.UseBorderWidth = false;
            this.xrTableCell181.Weight = 1.9082278481012658D;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.White;
            this.xrLabel17.BorderColor = System.Drawing.Color.Gray;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(292.4228F, 55.20837F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(237.3333F, 59.1666F);
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorderColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseForeColor = false;
            this.xrLabel17.StylePriority.UsePadding = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "11-13 St. Andrews Street, \r\nOakhurst Building,Parktown, Johannesburg\r\nTel No:+27 " +
    "11 645 9100, Fax No:+27 11 484 6588\r\nwebsite:www.xds.co.za\r\nEmail : info@xds.co." +
    "za";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.White;
            this.xrLabel18.BorderColor = System.Drawing.Color.Gray;
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(231.6503F, 10.00001F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(292.8451F, 45.20835F);
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseBorderColor = false;
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseForeColor = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "My Credit Report";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PersonalDetails
            // 
            this.PersonalDetails.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailPersonalDetails,
            this.GroupHeaderPersonalDetails});
            this.PersonalDetails.DataMember = "ConsumerDetail";
            this.PersonalDetails.Level = 0;
            this.PersonalDetails.Name = "PersonalDetails";
            // 
            // DetailPersonalDetails
            // 
            this.DetailPersonalDetails.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.DetailPersonalDetails.HeightF = 180F;
            this.DetailPersonalDetails.Name = "DetailPersonalDetails";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable1.ForeColor = System.Drawing.Color.Gray;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow11,
            this.xrTableRow10});
            this.xrTable1.SizeF = new System.Drawing.SizeF(880F, 180F);
            this.xrTable1.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.79999999999999993D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell5.BorderColor = System.Drawing.Color.White;
            this.xrTableCell5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseBorderColor = false;
            this.xrTableCell5.StylePriority.UseForeColor = false;
            this.xrTableCell5.Text = "Reference No.";
            this.xrTableCell5.Weight = 0.64556962991062583D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.White;
            this.xrTableCell6.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ReferenceNo")});
            this.xrTableCell6.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseBorderColor = false;
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseForeColor = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 0.8544303314595284D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell7.BorderColor = System.Drawing.Color.White;
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseBorderColor = false;
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseForeColor = false;
            this.xrTableCell7.Text = "External Reference No.";
            this.xrTableCell7.Weight = 0.64556963956808744D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.White;
            this.xrTableCell8.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ExternalReference")});
            this.xrTableCell8.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseBorderColor = false;
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseForeColor = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 0.85443039906175833D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.79999999999999993D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell9.BorderColor = System.Drawing.Color.White;
            this.xrTableCell9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBackColor = false;
            this.xrTableCell9.StylePriority.UseBorderColor = false;
            this.xrTableCell9.StylePriority.UseForeColor = false;
            this.xrTableCell9.Text = "ID No.";
            this.xrTableCell9.Weight = 0.64556962991062583D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BackColor = System.Drawing.Color.White;
            this.xrTableCell10.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.IDNo")});
            this.xrTableCell10.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBackColor = false;
            this.xrTableCell10.StylePriority.UseBorderColor = false;
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseForeColor = false;
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.Weight = 0.8544303314595284D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell11.BorderColor = System.Drawing.Color.White;
            this.xrTableCell11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseBorderColor = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.Text = "Passport or 2nd ID No.";
            this.xrTableCell11.Weight = 0.64556963956808744D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.White;
            this.xrTableCell12.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PassportNo")});
            this.xrTableCell12.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseBorderColor = false;
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseForeColor = false;
            this.xrTableCell12.Text = "xrTableCell12";
            this.xrTableCell12.Weight = 0.85443039906175833D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.lblBirthDate,
            this.xrTableCell15,
            this.xrTableCell16});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.8D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell13.BorderColor = System.Drawing.Color.White;
            this.xrTableCell13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBackColor = false;
            this.xrTableCell13.StylePriority.UseBorderColor = false;
            this.xrTableCell13.StylePriority.UseForeColor = false;
            this.xrTableCell13.Text = "Surname";
            this.xrTableCell13.Weight = 0.64556962991062583D;
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.BackColor = System.Drawing.Color.White;
            this.lblBirthDate.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblBirthDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblBirthDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Surname")});
            this.lblBirthDate.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.lblBirthDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.StylePriority.UseBackColor = false;
            this.lblBirthDate.StylePriority.UseBorderColor = false;
            this.lblBirthDate.StylePriority.UseBorders = false;
            this.lblBirthDate.StylePriority.UseFont = false;
            this.lblBirthDate.StylePriority.UseForeColor = false;
            this.lblBirthDate.Text = "lblBirthDate";
            this.lblBirthDate.Weight = 0.8544303314595284D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell15.BorderColor = System.Drawing.Color.White;
            this.xrTableCell15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBackColor = false;
            this.xrTableCell15.StylePriority.UseBorderColor = false;
            this.xrTableCell15.StylePriority.UseForeColor = false;
            this.xrTableCell15.Text = "Residential Address";
            this.xrTableCell15.Weight = 0.64556963956808744D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.BackColor = System.Drawing.Color.White;
            this.xrTableCell16.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ResidentialAddress")});
            this.xrTableCell16.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBackColor = false;
            this.xrTableCell16.StylePriority.UseBorderColor = false;
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseForeColor = false;
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.Weight = 0.85443039906175833D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.8D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell29.BorderColor = System.Drawing.Color.White;
            this.xrTableCell29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBackColor = false;
            this.xrTableCell29.StylePriority.UseBorderColor = false;
            this.xrTableCell29.StylePriority.UseForeColor = false;
            this.xrTableCell29.Text = "First Name";
            this.xrTableCell29.Weight = 0.64556962991062583D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.BackColor = System.Drawing.Color.White;
            this.xrTableCell30.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.FirstName")});
            this.xrTableCell30.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBackColor = false;
            this.xrTableCell30.StylePriority.UseBorderColor = false;
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseForeColor = false;
            this.xrTableCell30.Text = "xrTableCell30";
            this.xrTableCell30.Weight = 0.8544303314595284D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell31.BorderColor = System.Drawing.Color.White;
            this.xrTableCell31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBackColor = false;
            this.xrTableCell31.StylePriority.UseBorderColor = false;
            this.xrTableCell31.StylePriority.UseForeColor = false;
            this.xrTableCell31.Text = "Postal Address";
            this.xrTableCell31.Weight = 0.64556963956808744D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.BackColor = System.Drawing.Color.White;
            this.xrTableCell32.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PostalAddress")});
            this.xrTableCell32.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBackColor = false;
            this.xrTableCell32.StylePriority.UseBorderColor = false;
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseForeColor = false;
            this.xrTableCell32.Text = "xrTableCell32";
            this.xrTableCell32.Weight = 0.85443039906175833D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.8D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell25.BorderColor = System.Drawing.Color.White;
            this.xrTableCell25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBackColor = false;
            this.xrTableCell25.StylePriority.UseBorderColor = false;
            this.xrTableCell25.StylePriority.UseForeColor = false;
            this.xrTableCell25.Text = "Second Name";
            this.xrTableCell25.Weight = 0.64556962991062583D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.White;
            this.xrTableCell26.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.SecondName")});
            this.xrTableCell26.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseBorderColor = false;
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseForeColor = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.Weight = 0.8544303314595284D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell27.BorderColor = System.Drawing.Color.White;
            this.xrTableCell27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBackColor = false;
            this.xrTableCell27.StylePriority.UseBorderColor = false;
            this.xrTableCell27.StylePriority.UseForeColor = false;
            this.xrTableCell27.Text = "Telephone No. (H)";
            this.xrTableCell27.Weight = 0.64556963956808744D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.BackColor = System.Drawing.Color.White;
            this.xrTableCell28.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo")});
            this.xrTableCell28.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBackColor = false;
            this.xrTableCell28.StylePriority.UseBorderColor = false;
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseForeColor = false;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.Weight = 0.85443039906175833D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.80000000000000027D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell17.BorderColor = System.Drawing.Color.White;
            this.xrTableCell17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseBorderColor = false;
            this.xrTableCell17.StylePriority.UseForeColor = false;
            this.xrTableCell17.Text = "Title";
            this.xrTableCell17.Weight = 0.64556962991062583D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.White;
            this.xrTableCell18.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.TitleDesc")});
            this.xrTableCell18.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseBorderColor = false;
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseForeColor = false;
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.Weight = 0.8544303314595284D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell19.BorderColor = System.Drawing.Color.White;
            this.xrTableCell19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorderColor = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.Text = "Telephone No. (W)";
            this.xrTableCell19.Weight = 0.64556963956808744D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.BackColor = System.Drawing.Color.White;
            this.xrTableCell20.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.WorkTelephoneNo")});
            this.xrTableCell20.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBackColor = false;
            this.xrTableCell20.StylePriority.UseBorderColor = false;
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseForeColor = false;
            this.xrTableCell20.Text = "xrTableCell20";
            this.xrTableCell20.Weight = 0.85443039906175833D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.79999999999999982D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell21.BorderColor = System.Drawing.Color.White;
            this.xrTableCell21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBackColor = false;
            this.xrTableCell21.StylePriority.UseBorderColor = false;
            this.xrTableCell21.StylePriority.UseForeColor = false;
            this.xrTableCell21.Text = "Gender";
            this.xrTableCell21.Weight = 0.64556962991062583D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.BackColor = System.Drawing.Color.White;
            this.xrTableCell22.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Gender")});
            this.xrTableCell22.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBackColor = false;
            this.xrTableCell22.StylePriority.UseBorderColor = false;
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseForeColor = false;
            this.xrTableCell22.Text = "xrTableCell22";
            this.xrTableCell22.Weight = 0.8544303314595284D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell23.BorderColor = System.Drawing.Color.White;
            this.xrTableCell23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseForeColor = false;
            this.xrTableCell23.Text = "Cellular/Mobile";
            this.xrTableCell23.Weight = 0.64556963956808744D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.BackColor = System.Drawing.Color.White;
            this.xrTableCell24.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo")});
            this.xrTableCell24.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBackColor = false;
            this.xrTableCell24.StylePriority.UseBorderColor = false;
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseForeColor = false;
            this.xrTableCell24.Text = "xrTableCell24";
            this.xrTableCell24.Weight = 0.85443039906175833D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.8D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell41.BorderColor = System.Drawing.Color.White;
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBackColor = false;
            this.xrTableCell41.StylePriority.UseBorderColor = false;
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.StylePriority.UseForeColor = false;
            this.xrTableCell41.Text = "Date of Birth";
            this.xrTableCell41.Weight = 0.64556962991062583D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.BackColor = System.Drawing.Color.White;
            this.xrTableCell42.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.BirthDate")});
            this.xrTableCell42.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBackColor = false;
            this.xrTableCell42.StylePriority.UseBorderColor = false;
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.StylePriority.UseForeColor = false;
            this.xrTableCell42.Text = "xrTableCell42";
            this.xrTableCell42.Weight = 0.8544303314595284D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell43.BorderColor = System.Drawing.Color.White;
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBackColor = false;
            this.xrTableCell43.StylePriority.UseBorderColor = false;
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.StylePriority.UseForeColor = false;
            this.xrTableCell43.Text = "E-mail Address";
            this.xrTableCell43.Weight = 0.64556963956808744D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.BackColor = System.Drawing.Color.White;
            this.xrTableCell44.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmailAddress")});
            this.xrTableCell44.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBackColor = false;
            this.xrTableCell44.StylePriority.UseBorderColor = false;
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseForeColor = false;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.Weight = 0.85443039906175833D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.79999999999999982D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell37.BorderColor = System.Drawing.Color.White;
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBackColor = false;
            this.xrTableCell37.StylePriority.UseBorderColor = false;
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.StylePriority.UseForeColor = false;
            this.xrTableCell37.Text = "Marital Status";
            this.xrTableCell37.Weight = 0.64556962991062583D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.BackColor = System.Drawing.Color.White;
            this.xrTableCell38.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.MaritalStatusDesc")});
            this.xrTableCell38.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBackColor = false;
            this.xrTableCell38.StylePriority.UseBorderColor = false;
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseForeColor = false;
            this.xrTableCell38.Text = "xrTableCell38";
            this.xrTableCell38.Weight = 0.8544303314595284D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell39.BorderColor = System.Drawing.Color.White;
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBackColor = false;
            this.xrTableCell39.StylePriority.UseBorderColor = false;
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.StylePriority.UseForeColor = false;
            this.xrTableCell39.Text = "Current Employer";
            this.xrTableCell39.Weight = 0.64556963956808744D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.BackColor = System.Drawing.Color.White;
            this.xrTableCell40.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmployerDetail")});
            this.xrTableCell40.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBackColor = false;
            this.xrTableCell40.StylePriority.UseBorderColor = false;
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UseForeColor = false;
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.Weight = 0.85443039906175833D;
            // 
            // GroupHeaderPersonalDetails
            // 
            this.GroupHeaderPersonalDetails.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupHeaderPersonalDetails.HeightF = 72.91667F;
            this.GroupHeaderPersonalDetails.Name = "GroupHeaderPersonalDetails";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable2.ForeColor = System.Drawing.Color.Gray;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 9.999974F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow68,
            this.xrTableRow69});
            this.xrTable2.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseForeColor = false;
            this.xrTable2.StylePriority.UsePadding = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 0.8D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.BackColor = System.Drawing.Color.White;
            this.xrTableCell61.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell61.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell61.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell61.StylePriority.UseBackColor = false;
            this.xrTableCell61.StylePriority.UseBorderColor = false;
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.StylePriority.UseForeColor = false;
            this.xrTableCell61.StylePriority.UsePadding = false;
            this.xrTableCell61.Text = "Your Personal Details";
            this.xrTableCell61.Weight = 3D;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell96});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 0.53333333333333344D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell96.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell96.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell96.StylePriority.UseBorderColor = false;
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.StylePriority.UseFont = false;
            this.xrTableCell96.StylePriority.UseForeColor = false;
            this.xrTableCell96.StylePriority.UsePadding = false;
            this.xrTableCell96.Text = "This section displays the Personal information for the consumer. Included here is" +
    " ID or Passport, Name, Gender, Marital Status.";
            this.xrTableCell96.Weight = 3D;
            // 
            // ReportHeader
            // 
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrLabel16,
            this.xrLabel95,
            this.xrPageInfo1});
            this.PageFooter.HeightF = 40.625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo2.BorderColor = System.Drawing.Color.White;
            this.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo2.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo2.Format = "{0:dddd, dd MMMM yyyy H:mm}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(12.50013F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(253.5834F, 25F);
            this.xrPageInfo2.StylePriority.UseBackColor = false;
            this.xrPageInfo2.StylePriority.UseBorderColor = false;
            this.xrPageInfo2.StylePriority.UseBorders = false;
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.StylePriority.UsePadding = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel16.BorderColor = System.Drawing.Color.White;
            this.xrLabel16.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.ForeColor = System.Drawing.Color.DimGray;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(266.0836F, 0F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(473.2655F, 25F);
            this.xrLabel16.StylePriority.UseBackColor = false;
            this.xrLabel16.StylePriority.UseBorderColor = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseForeColor = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Registered with the National Credit Regulator – Reg# NCR-CB5";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel95
            // 
            this.xrLabel95.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel95.BorderColor = System.Drawing.Color.White;
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel95.ForeColor = System.Drawing.Color.DimGray;
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(739.349F, 0F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(103.2358F, 25F);
            this.xrLabel95.StylePriority.UseBackColor = false;
            this.xrLabel95.StylePriority.UseBorderColor = false;
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.StylePriority.UseFont = false;
            this.xrLabel95.StylePriority.UseForeColor = false;
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.Text = "Page:";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo1.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(842.5848F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(49.00146F, 25F);
            this.xrPageInfo1.StylePriority.UseBackColor = false;
            this.xrPageInfo1.StylePriority.UseBorderColor = false;
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseForeColor = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // FraudIndicators
            // 
            this.FraudIndicators.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailFraudIndicators,
            this.GroupHeaderFraudIndicators});
            this.FraudIndicators.DataMember = "ConsumerFraudIndicatorsSummary";
            this.FraudIndicators.Level = 1;
            this.FraudIndicators.Name = "FraudIndicators";
            // 
            // DetailFraudIndicators
            // 
            this.DetailFraudIndicators.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.DetailFraudIndicators.Name = "DetailFraudIndicators";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable4.ForeColor = System.Drawing.Color.Gray;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable4.SizeF = new System.Drawing.SizeF(880F, 100F);
            this.xrTable4.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseForeColor = false;
            this.xrTable4.StylePriority.UsePadding = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell49});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.79999999999999993D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell3.BorderColor = System.Drawing.Color.White;
            this.xrTableCell3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseBorderColor = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.Text = "ID No. Verified Status At Home Affairs";
            this.xrTableCell3.Weight = 1.4914771533259312D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.BackColor = System.Drawing.Color.White;
            this.xrTableCell49.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.HomeAffairsVerificationYN")});
            this.xrTableCell49.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBackColor = false;
            this.xrTableCell49.StylePriority.UseBorderColor = false;
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseForeColor = false;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.Weight = 1.5085228466740686D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell53});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.79999999999999993D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell50.BorderColor = System.Drawing.Color.White;
            this.xrTableCell50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBackColor = false;
            this.xrTableCell50.StylePriority.UseBorderColor = false;
            this.xrTableCell50.StylePriority.UseForeColor = false;
            this.xrTableCell50.Text = "ID No. Deceased Status At Home Affairs";
            this.xrTableCell50.Weight = 1.4914771533259312D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.BackColor = System.Drawing.Color.White;
            this.xrTableCell53.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.HomeAffairsDeceasedStatus")});
            this.xrTableCell53.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBackColor = false;
            this.xrTableCell53.StylePriority.UseBorderColor = false;
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UseForeColor = false;
            this.xrTableCell53.Text = "xrTableCell53";
            this.xrTableCell53.Weight = 1.5085228466740686D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell57});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.8D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell54.BorderColor = System.Drawing.Color.White;
            this.xrTableCell54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBackColor = false;
            this.xrTableCell54.StylePriority.UseBorderColor = false;
            this.xrTableCell54.StylePriority.UseForeColor = false;
            this.xrTableCell54.Text = "ID No. Found on Fraud Database";
            this.xrTableCell54.Weight = 1.4914771533259312D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.BackColor = System.Drawing.Color.White;
            this.xrTableCell55.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell55.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBackColor = false;
            this.xrTableCell55.StylePriority.UseBorderColor = false;
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UseForeColor = false;
            this.xrTableCell55.Weight = 0.008522808044222896D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.BackColor = System.Drawing.Color.White;
            this.xrTableCell57.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.SAFPSListingYN")});
            this.xrTableCell57.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBackColor = false;
            this.xrTableCell57.StylePriority.UseBorderColor = false;
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UseForeColor = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "[SAFPSListingYN]";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 1.5000000386298458D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell62});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.8D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell58.BorderColor = System.Drawing.Color.White;
            this.xrTableCell58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBackColor = false;
            this.xrTableCell58.StylePriority.UseBorderColor = false;
            this.xrTableCell58.StylePriority.UseForeColor = false;
            this.xrTableCell58.Text = "ID No. Found on Employer Fraud Database ";
            this.xrTableCell58.Weight = 1.4914771533259312D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.BackColor = System.Drawing.Color.White;
            this.xrTableCell62.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.EmployerFraudVerificationYN")});
            this.xrTableCell62.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell62.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBackColor = false;
            this.xrTableCell62.StylePriority.UseBorderColor = false;
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseForeColor = false;
            this.xrTableCell62.Text = "xrTableCell62";
            this.xrTableCell62.Weight = 1.5085228466740686D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell66});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.8D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell63.BorderColor = System.Drawing.Color.White;
            this.xrTableCell63.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBackColor = false;
            this.xrTableCell63.StylePriority.UseBorderColor = false;
            this.xrTableCell63.StylePriority.UseForeColor = false;
            this.xrTableCell63.Text = "ID No. Found on Protective Register";
            this.xrTableCell63.Weight = 1.4914771533259312D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.BackColor = System.Drawing.Color.White;
            this.xrTableCell66.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.ProtectiveVerificationYN")});
            this.xrTableCell66.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBackColor = false;
            this.xrTableCell66.StylePriority.UseBorderColor = false;
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.StylePriority.UseForeColor = false;
            this.xrTableCell66.Text = "xrTableCell66";
            this.xrTableCell66.Weight = 1.5085228466740686D;
            // 
            // GroupHeaderFraudIndicators
            // 
            this.GroupHeaderFraudIndicators.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeaderFraudIndicators.HeightF = 71.875F;
            this.GroupHeaderFraudIndicators.Name = "GroupHeaderFraudIndicators";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable3.ForeColor = System.Drawing.Color.Gray;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow13});
            this.xrTable3.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseForeColor = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.8D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BackColor = System.Drawing.Color.White;
            this.xrTableCell1.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell1.StylePriority.UseBackColor = false;
            this.xrTableCell1.StylePriority.UseBorderColor = false;
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseForeColor = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.Text = "Potential Fraud Indicators";
            this.xrTableCell1.Weight = 3D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.53333333333333344D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell2.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell2.StylePriority.UseBorderColor = false;
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseForeColor = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Text = "This section displays information verified by Home Affairs and the SAFPS.";
            this.xrTableCell2.Weight = 3D;
            // 
            // PresageScore
            // 
            this.PresageScore.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailPresageScore,
            this.GroupHeaderPresageScore});
            this.PresageScore.DataMember = "ConsumerScoring";
            this.PresageScore.Level = 2;
            this.PresageScore.Name = "PresageScore";
            // 
            // DetailPresageScore
            // 
            this.DetailPresageScore.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14,
            this.xrTable13,
            this.xrTable15,
            this.xrPictureBox1});
            this.DetailPresageScore.HeightF = 424.3749F;
            this.DetailPresageScore.Name = "DetailPresageScore";
            // 
            // xrTable14
            // 
            this.xrTable14.BorderColor = System.Drawing.Color.Transparent;
            this.xrTable14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable14.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable14.ForeColor = System.Drawing.Color.Gray;
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow37});
            this.xrTable14.SizeF = new System.Drawing.SizeF(357.2867F, 79.99999F);
            this.xrTable14.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable14.StylePriority.UseBorderColor = false;
            this.xrTable14.StylePriority.UseBorders = false;
            this.xrTable14.StylePriority.UseFont = false;
            this.xrTable14.StylePriority.UseForeColor = false;
            this.xrTable14.StylePriority.UsePadding = false;
            this.xrTable14.StylePriority.UseTextAlignment = false;
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.79999999999999993D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.BackColor = System.Drawing.Color.White;
            this.xrTableCell99.BorderColor = System.Drawing.Color.White;
            this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell99.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell99.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBackColor = false;
            this.xrTableCell99.StylePriority.UseBorderColor = false;
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseFont = false;
            this.xrTableCell99.StylePriority.UseForeColor = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.Text = "Your Credit Score is:";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell99.Weight = 3D;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblScore});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.79999999999999993D;
            // 
            // lblScore
            // 
            this.lblScore.BackColor = System.Drawing.Color.White;
            this.lblScore.BorderColor = System.Drawing.Color.White;
            this.lblScore.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblScore.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.FinalScore", "{0}")});
            this.lblScore.Font = new System.Drawing.Font("Trebuchet MS", 14F);
            this.lblScore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblScore.Name = "lblScore";
            this.lblScore.StylePriority.UseBackColor = false;
            this.lblScore.StylePriority.UseBorderColor = false;
            this.lblScore.StylePriority.UseBorders = false;
            this.lblScore.StylePriority.UseFont = false;
            this.lblScore.StylePriority.UseForeColor = false;
            this.lblScore.StylePriority.UseTextAlignment = false;
            this.lblScore.Text = "[FinalScore]";
            this.lblScore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblScore.Weight = 3D;
            this.lblScore.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblScore_BeforePrint);
            // 
            // xrTable13
            // 
            this.xrTable13.BookmarkParent = this.lblM01;
            this.xrTable13.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable13.ForeColor = System.Drawing.Color.Gray;
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(371.7867F, 0F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35});
            this.xrTable13.SizeF = new System.Drawing.SizeF(518.2133F, 80F);
            this.xrTable13.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable13.StylePriority.UseBorderColor = false;
            this.xrTable13.StylePriority.UseBorders = false;
            this.xrTable13.StylePriority.UseFont = false;
            this.xrTable13.StylePriority.UseForeColor = false;
            this.xrTable13.StylePriority.UsePadding = false;
            this.xrTable13.StylePriority.UseTextAlignment = false;
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblM01
            // 
            this.lblM01.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM01.BorderColor = System.Drawing.Color.White;
            this.lblM01.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM01.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M01")});
            this.lblM01.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM01.Name = "lblM01";
            this.lblM01.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblM01.StylePriority.UseBackColor = false;
            this.lblM01.StylePriority.UseBorderColor = false;
            this.lblM01.StylePriority.UseBorders = false;
            this.lblM01.StylePriority.UseFont = false;
            this.lblM01.StylePriority.UseForeColor = false;
            this.lblM01.StylePriority.UsePadding = false;
            this.lblM01.StylePriority.UseTextAlignment = false;
            this.lblM01.Text = "lblM01";
            this.lblM01.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM01.Weight = 0.030594379352799655D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.79999999999999993D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell89.BorderColor = System.Drawing.Color.White;
            this.xrTableCell89.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell89.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBackColor = false;
            this.xrTableCell89.StylePriority.UseBorderColor = false;
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.StylePriority.UseForeColor = false;
            this.xrTableCell89.Text = "Unique Score Ref";
            this.xrTableCell89.Weight = 1.4914771533259312D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell90.BorderColor = System.Drawing.Color.White;
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Unique_Identifier")});
            this.xrTableCell90.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell90.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBackColor = false;
            this.xrTableCell90.StylePriority.UseBorderColor = false;
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.StylePriority.UseFont = false;
            this.xrTableCell90.StylePriority.UseForeColor = false;
            this.xrTableCell90.Text = "xrTableCell90";
            this.xrTableCell90.Weight = 1.5085228466740686D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.79999999999999993D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.BackColor = System.Drawing.Color.White;
            this.xrTableCell91.BorderColor = System.Drawing.Color.White;
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell91.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBackColor = false;
            this.xrTableCell91.StylePriority.UseBorderColor = false;
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.StylePriority.UseForeColor = false;
            this.xrTableCell91.Text = "Score Date";
            this.xrTableCell91.Weight = 1.4914771533259312D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.BackColor = System.Drawing.Color.White;
            this.xrTableCell92.BorderColor = System.Drawing.Color.White;
            this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.ScoreDate")});
            this.xrTableCell92.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell92.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBackColor = false;
            this.xrTableCell92.StylePriority.UseBorderColor = false;
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.StylePriority.UseFont = false;
            this.xrTableCell92.StylePriority.UseForeColor = false;
            this.xrTableCell92.Text = "xrTableCell92";
            this.xrTableCell92.Weight = 1.5085228466740686D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.8D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell93.BorderColor = System.Drawing.Color.White;
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell93.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBackColor = false;
            this.xrTableCell93.StylePriority.UseBorderColor = false;
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.StylePriority.UseForeColor = false;
            this.xrTableCell93.Text = "Risk Category";
            this.xrTableCell93.Weight = 1.4914771533259312D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.BackColor = System.Drawing.Color.White;
            this.xrTableCell94.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell94.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell94.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell94.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBackColor = false;
            this.xrTableCell94.StylePriority.UseBorderColor = false;
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.StylePriority.UseFont = false;
            this.xrTableCell94.StylePriority.UseForeColor = false;
            this.xrTableCell94.Weight = 0.008522808044222896D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell95.BorderColor = System.Drawing.Color.White;
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.RiskCategory")});
            this.xrTableCell95.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell95.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBackColor = false;
            this.xrTableCell95.StylePriority.UseBorderColor = false;
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.StylePriority.UseFont = false;
            this.xrTableCell95.StylePriority.UseForeColor = false;
            this.xrTableCell95.Text = "xrTableCell95";
            this.xrTableCell95.Weight = 1.5000000386298458D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTableCell98});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.8D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.BackColor = System.Drawing.Color.White;
            this.xrTableCell97.BorderColor = System.Drawing.Color.White;
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell97.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBackColor = false;
            this.xrTableCell97.StylePriority.UseBorderColor = false;
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.StylePriority.UseForeColor = false;
            this.xrTableCell97.Text = "0 score Reason";
            this.xrTableCell97.Weight = 1.4914771533259312D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.BackColor = System.Drawing.Color.White;
            this.xrTableCell98.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell98.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Exception_Code")});
            this.xrTableCell98.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell98.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBackColor = false;
            this.xrTableCell98.StylePriority.UseBorderColor = false;
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.StylePriority.UseFont = false;
            this.xrTableCell98.StylePriority.UseForeColor = false;
            this.xrTableCell98.Text = "xrTableCell98";
            this.xrTableCell98.Weight = 1.5085228466740686D;
            // 
            // xrTable15
            // 
            this.xrTable15.BorderColor = System.Drawing.Color.Transparent;
            this.xrTable15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable15.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable15.ForeColor = System.Drawing.Color.Gray;
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(371.7867F, 95.2083F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable15.SizeF = new System.Drawing.SizeF(516.2133F, 75.4167F);
            this.xrTable15.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable15.StylePriority.UseBorderColor = false;
            this.xrTable15.StylePriority.UseBorders = false;
            this.xrTable15.StylePriority.UseFont = false;
            this.xrTable15.StylePriority.UseForeColor = false;
            this.xrTable15.StylePriority.UsePadding = false;
            this.xrTable15.StylePriority.UseTextAlignment = false;
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1.5083332737286828D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.BackColor = System.Drawing.Color.White;
            this.xrTableCell101.BorderColor = System.Drawing.Color.White;
            this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell101.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell101.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBackColor = false;
            this.xrTableCell101.StylePriority.UseBorderColor = false;
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.StylePriority.UseForeColor = false;
            this.xrTableCell101.StylePriority.UseTextAlignment = false;
            this.xrTableCell101.Text = resources.GetString("xrTableCell101.Text");
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell101.Weight = 3D;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.BorderColor = System.Drawing.Color.Transparent;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(12.50013F, 87.29156F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(223.9583F, 334.375F);
            this.xrPictureBox1.StylePriority.UseBorderColor = false;
            // 
            // GroupHeaderPresageScore
            // 
            this.GroupHeaderPresageScore.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.GroupHeaderPresageScore.HeightF = 89.58334F;
            this.GroupHeaderPresageScore.Name = "GroupHeaderPresageScore";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable5.ForeColor = System.Drawing.Color.Gray;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21});
            this.xrTable5.SizeF = new System.Drawing.SizeF(880F, 70F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseForeColor = false;
            this.xrTable5.StylePriority.UsePadding = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.8D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.White;
            this.xrTableCell4.BorderColor = System.Drawing.Color.White;
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell4.StylePriority.UseBackColor = false;
            this.xrTableCell4.StylePriority.UseBorderColor = false;
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseForeColor = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.Text = "XDS Presage Score";
            this.xrTableCell4.Weight = 3D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.53333333333333344D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.White;
            this.xrTableCell14.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell14.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell14.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell14.Multiline = true;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseBorderColor = false;
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseForeColor = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.Text = "The XDS Presage score is a generic bureau scorecard which indicates the credit ri" +
    "sk associated with a consumer by taking into consideration the consumer’s ";
            this.xrTableCell14.Weight = 3D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.53333333333333344D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.BackColor = System.Drawing.Color.White;
            this.xrTableCell51.BorderColor = System.Drawing.Color.White;
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell51.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell51.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBackColor = false;
            this.xrTableCell51.StylePriority.UseBorderColor = false;
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseForeColor = false;
            this.xrTableCell51.Text = " historical credit profile and repayment behaviour. The higher the score, the low" +
    "er the associated risk of future payment delinquency or default.";
            this.xrTableCell51.Weight = 3D;
            // 
            // DebtSummary
            // 
            this.DebtSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDebtSummary,
            this.GroupHeaderDebtSummary});
            this.DebtSummary.DataMember = "ConsumerCPANLRDebtSummary";
            this.DebtSummary.Level = 3;
            this.DebtSummary.Name = "DebtSummary";
            // 
            // DetailDebtSummary
            // 
            this.DetailDebtSummary.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.DetailDebtSummary.HeightF = 379.375F;
            this.DetailDebtSummary.Name = "DetailDebtSummary";
            // 
            // xrTable7
            // 
            this.xrTable7.BackColor = System.Drawing.Color.White;
            this.xrTable7.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable7.ForeColor = System.Drawing.Color.Gray;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25,
            this.xrTableRow125,
            this.xrTableRow126,
            this.xrTableRow127,
            this.xrTableRow128,
            this.xrTableRow129,
            this.xrTableRow130,
            this.xrTableRow131,
            this.xrTableRow132,
            this.xrTableRow133,
            this.xrTableRow134,
            this.xrTableRow135,
            this.xrTableRow136,
            this.xrTableRow137,
            this.xrTableRow138,
            this.xrTableRow139,
            this.xrTableRow140,
            this.xrTableRow141});
            this.xrTable7.SizeF = new System.Drawing.SizeF(880F, 360.0002F);
            this.xrTable7.StylePriority.UseBackColor = false;
            this.xrTable7.StylePriority.UseBorderColor = false;
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseForeColor = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.lblActiveAccTotValue,
            this.xrTableCell65});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.400000244140774D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.BackColor = System.Drawing.Color.White;
            this.xrTableCell60.BorderColor = System.Drawing.Color.White;
            this.xrTableCell60.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBackColor = false;
            this.xrTableCell60.StylePriority.UseBorderColor = false;
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.StylePriority.UseForeColor = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "Active Accounts";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 1.1582138725517057D;
            // 
            // lblActiveAccTotValue
            // 
            this.lblActiveAccTotValue.BackColor = System.Drawing.Color.White;
            this.lblActiveAccTotValue.BorderColor = System.Drawing.Color.White;
            this.lblActiveAccTotValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoOFActiveAccounts")});
            this.lblActiveAccTotValue.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblActiveAccTotValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblActiveAccTotValue.Name = "lblActiveAccTotValue";
            this.lblActiveAccTotValue.StylePriority.UseBackColor = false;
            this.lblActiveAccTotValue.StylePriority.UseBorderColor = false;
            this.lblActiveAccTotValue.StylePriority.UseFont = false;
            this.lblActiveAccTotValue.StylePriority.UseForeColor = false;
            this.lblActiveAccTotValue.StylePriority.UseTextAlignment = false;
            this.lblActiveAccTotValue.Text = "lblActiveAccTotValue";
            this.lblActiveAccTotValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblActiveAccTotValue.Weight = 0.94759088822440773D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.BackColor = System.Drawing.Color.White;
            this.xrTableCell65.BorderColor = System.Drawing.Color.White;
            this.xrTableCell65.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell65.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBackColor = false;
            this.xrTableCell65.StylePriority.UseBorderColor = false;
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.StylePriority.UseForeColor = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "-";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell65.Weight = 0.89799746231631616D;
            // 
            // xrTableRow125
            // 
            this.xrTableRow125.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell330,
            this.lblAccGoodTotvalue,
            this.xrTableCell332});
            this.xrTableRow125.Name = "xrTableRow125";
            this.xrTableRow125.Weight = 0.400000244140774D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell330.BorderColor = System.Drawing.Color.White;
            this.xrTableCell330.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.StylePriority.UseBackColor = false;
            this.xrTableCell330.StylePriority.UseBorderColor = false;
            this.xrTableCell330.StylePriority.UseForeColor = false;
            this.xrTableCell330.StylePriority.UseTextAlignment = false;
            this.xrTableCell330.Text = "Accounts in Good Standing";
            this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell330.Weight = 1.1582138725517057D;
            // 
            // lblAccGoodTotvalue
            // 
            this.lblAccGoodTotvalue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAccGoodTotvalue.BorderColor = System.Drawing.Color.White;
            this.lblAccGoodTotvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoOfAccountInGoodStanding")});
            this.lblAccGoodTotvalue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblAccGoodTotvalue.Name = "lblAccGoodTotvalue";
            this.lblAccGoodTotvalue.StylePriority.UseBackColor = false;
            this.lblAccGoodTotvalue.StylePriority.UseBorderColor = false;
            this.lblAccGoodTotvalue.StylePriority.UseForeColor = false;
            this.lblAccGoodTotvalue.StylePriority.UseTextAlignment = false;
            this.lblAccGoodTotvalue.Text = "lblAccGoodTotvalue";
            this.lblAccGoodTotvalue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccGoodTotvalue.Weight = 0.94759088822440773D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell332.BorderColor = System.Drawing.Color.White;
            this.xrTableCell332.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.StylePriority.UseBackColor = false;
            this.xrTableCell332.StylePriority.UseBorderColor = false;
            this.xrTableCell332.StylePriority.UseForeColor = false;
            this.xrTableCell332.StylePriority.UseTextAlignment = false;
            this.xrTableCell332.Text = "-";
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell332.Weight = 0.89799746231631616D;
            // 
            // xrTableRow126
            // 
            this.xrTableRow126.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell333,
            this.lblDelinquentAccTotValue,
            this.xrTableCell335});
            this.xrTableRow126.Name = "xrTableRow126";
            this.xrTableRow126.Weight = 0.400000244140774D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.BorderColor = System.Drawing.Color.White;
            this.xrTableCell333.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseBackColor = false;
            this.xrTableCell333.StylePriority.UseBorderColor = false;
            this.xrTableCell333.StylePriority.UseForeColor = false;
            this.xrTableCell333.StylePriority.UseTextAlignment = false;
            this.xrTableCell333.Text = "Accounts In Arrears";
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell333.Weight = 1.1582138725517057D;
            // 
            // lblDelinquentAccTotValue
            // 
            this.lblDelinquentAccTotValue.BorderColor = System.Drawing.Color.White;
            this.lblDelinquentAccTotValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoOfAccountInBadStanding")});
            this.lblDelinquentAccTotValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDelinquentAccTotValue.Name = "lblDelinquentAccTotValue";
            this.lblDelinquentAccTotValue.StylePriority.UseBackColor = false;
            this.lblDelinquentAccTotValue.StylePriority.UseBorderColor = false;
            this.lblDelinquentAccTotValue.StylePriority.UseForeColor = false;
            this.lblDelinquentAccTotValue.StylePriority.UseTextAlignment = false;
            this.lblDelinquentAccTotValue.Text = "lblDelinquentAccTotValue";
            this.lblDelinquentAccTotValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDelinquentAccTotValue.Weight = 0.94759088822440773D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.BorderColor = System.Drawing.Color.White;
            this.xrTableCell335.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.StylePriority.UseBackColor = false;
            this.xrTableCell335.StylePriority.UseBorderColor = false;
            this.xrTableCell335.StylePriority.UseForeColor = false;
            this.xrTableCell335.StylePriority.UseTextAlignment = false;
            this.xrTableCell335.Text = "-";
            this.xrTableCell335.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell335.Weight = 0.89799746231631616D;
            // 
            // xrTableRow127
            // 
            this.xrTableRow127.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell336,
            this.xrTableCell337,
            this.lblMonthlyinstTotValue});
            this.xrTableRow127.Name = "xrTableRow127";
            this.xrTableRow127.Weight = 0.400000244140774D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell336.BorderColor = System.Drawing.Color.White;
            this.xrTableCell336.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.StylePriority.UseBackColor = false;
            this.xrTableCell336.StylePriority.UseBorderColor = false;
            this.xrTableCell336.StylePriority.UseForeColor = false;
            this.xrTableCell336.StylePriority.UseTextAlignment = false;
            this.xrTableCell336.Text = "Total Monthly Instalment";
            this.xrTableCell336.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell336.Weight = 1.1582138725517057D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell337.BorderColor = System.Drawing.Color.White;
            this.xrTableCell337.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseBackColor = false;
            this.xrTableCell337.StylePriority.UseBorderColor = false;
            this.xrTableCell337.StylePriority.UseForeColor = false;
            this.xrTableCell337.StylePriority.UseTextAlignment = false;
            this.xrTableCell337.Text = "-";
            this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell337.Weight = 0.94759088822440773D;
            // 
            // lblMonthlyinstTotValue
            // 
            this.lblMonthlyinstTotValue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblMonthlyinstTotValue.BorderColor = System.Drawing.Color.White;
            this.lblMonthlyinstTotValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalMonthlyInstallment")});
            this.lblMonthlyinstTotValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblMonthlyinstTotValue.Name = "lblMonthlyinstTotValue";
            this.lblMonthlyinstTotValue.StylePriority.UseBackColor = false;
            this.lblMonthlyinstTotValue.StylePriority.UseBorderColor = false;
            this.lblMonthlyinstTotValue.StylePriority.UseForeColor = false;
            this.lblMonthlyinstTotValue.StylePriority.UseTextAlignment = false;
            this.lblMonthlyinstTotValue.Text = "[TotalMonthlyInstallment]";
            this.lblMonthlyinstTotValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMonthlyinstTotValue.Weight = 0.89799746231631616D;
            // 
            // xrTableRow128
            // 
            this.xrTableRow128.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell339,
            this.xrTableCell340,
            this.lblOutstandingDebtTotValue});
            this.xrTableRow128.Name = "xrTableRow128";
            this.xrTableRow128.Weight = 0.400000244140774D;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.BorderColor = System.Drawing.Color.White;
            this.xrTableCell339.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBackColor = false;
            this.xrTableCell339.StylePriority.UseBorderColor = false;
            this.xrTableCell339.StylePriority.UseForeColor = false;
            this.xrTableCell339.StylePriority.UseTextAlignment = false;
            this.xrTableCell339.Text = "Total Outstanding Debt";
            this.xrTableCell339.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell339.Weight = 1.1582138725517057D;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.BorderColor = System.Drawing.Color.White;
            this.xrTableCell340.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBackColor = false;
            this.xrTableCell340.StylePriority.UseBorderColor = false;
            this.xrTableCell340.StylePriority.UseForeColor = false;
            this.xrTableCell340.StylePriority.UseTextAlignment = false;
            this.xrTableCell340.Text = "-";
            this.xrTableCell340.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell340.Weight = 0.94759088822440773D;
            // 
            // lblOutstandingDebtTotValue
            // 
            this.lblOutstandingDebtTotValue.BorderColor = System.Drawing.Color.White;
            this.lblOutstandingDebtTotValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalOutStandingDebt")});
            this.lblOutstandingDebtTotValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblOutstandingDebtTotValue.Name = "lblOutstandingDebtTotValue";
            this.lblOutstandingDebtTotValue.StylePriority.UseBackColor = false;
            this.lblOutstandingDebtTotValue.StylePriority.UseBorderColor = false;
            this.lblOutstandingDebtTotValue.StylePriority.UseForeColor = false;
            this.lblOutstandingDebtTotValue.StylePriority.UseTextAlignment = false;
            this.lblOutstandingDebtTotValue.Text = "[TotalOutStandingDebt]";
            this.lblOutstandingDebtTotValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblOutstandingDebtTotValue.Weight = 0.89799746231631616D;
            // 
            // xrTableRow129
            // 
            this.xrTableRow129.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell342,
            this.xrTableCell343,
            this.lblArrearamtTotvalue});
            this.xrTableRow129.Name = "xrTableRow129";
            this.xrTableRow129.Weight = 0.400000244140774D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell342.BorderColor = System.Drawing.Color.White;
            this.xrTableCell342.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBackColor = false;
            this.xrTableCell342.StylePriority.UseBorderColor = false;
            this.xrTableCell342.StylePriority.UseForeColor = false;
            this.xrTableCell342.StylePriority.UseTextAlignment = false;
            this.xrTableCell342.Text = "Total Arrear Amount";
            this.xrTableCell342.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell342.Weight = 1.1582138725517057D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell343.BorderColor = System.Drawing.Color.White;
            this.xrTableCell343.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseBackColor = false;
            this.xrTableCell343.StylePriority.UseBorderColor = false;
            this.xrTableCell343.StylePriority.UseForeColor = false;
            this.xrTableCell343.StylePriority.UseTextAlignment = false;
            this.xrTableCell343.Text = "-";
            this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell343.Weight = 0.94759088822440773D;
            // 
            // lblArrearamtTotvalue
            // 
            this.lblArrearamtTotvalue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblArrearamtTotvalue.BorderColor = System.Drawing.Color.White;
            this.lblArrearamtTotvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalArrearAmount")});
            this.lblArrearamtTotvalue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblArrearamtTotvalue.Name = "lblArrearamtTotvalue";
            this.lblArrearamtTotvalue.StylePriority.UseBackColor = false;
            this.lblArrearamtTotvalue.StylePriority.UseBorderColor = false;
            this.lblArrearamtTotvalue.StylePriority.UseForeColor = false;
            this.lblArrearamtTotvalue.StylePriority.UseTextAlignment = false;
            this.lblArrearamtTotvalue.Text = "[TotalArrearAmount]";
            this.lblArrearamtTotvalue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblArrearamtTotvalue.Weight = 0.89799746231631616D;
            // 
            // xrTableRow130
            // 
            this.xrTableRow130.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell345,
            this.lblDelinquentratingTotValue,
            this.xrTableCell347});
            this.xrTableRow130.Name = "xrTableRow130";
            this.xrTableRow130.Weight = 0.400000244140774D;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.BorderColor = System.Drawing.Color.White;
            this.xrTableCell345.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBackColor = false;
            this.xrTableCell345.StylePriority.UseBorderColor = false;
            this.xrTableCell345.StylePriority.UseForeColor = false;
            this.xrTableCell345.StylePriority.UseTextAlignment = false;
            this.xrTableCell345.Text = "Highest Months in Arrears (Last 24 Months)";
            this.xrTableCell345.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell345.Weight = 1.1582138725517057D;
            // 
            // lblDelinquentratingTotValue
            // 
            this.lblDelinquentratingTotValue.BorderColor = System.Drawing.Color.White;
            this.lblDelinquentratingTotValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.HighestMonthsInArrears")});
            this.lblDelinquentratingTotValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDelinquentratingTotValue.Name = "lblDelinquentratingTotValue";
            this.lblDelinquentratingTotValue.StylePriority.UseBackColor = false;
            this.lblDelinquentratingTotValue.StylePriority.UseBorderColor = false;
            this.lblDelinquentratingTotValue.StylePriority.UseForeColor = false;
            this.lblDelinquentratingTotValue.StylePriority.UseTextAlignment = false;
            this.lblDelinquentratingTotValue.Text = "lblDelinquentratingTotValue";
            this.lblDelinquentratingTotValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDelinquentratingTotValue.Weight = 0.94759088822440773D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.BorderColor = System.Drawing.Color.White;
            this.xrTableCell347.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBackColor = false;
            this.xrTableCell347.StylePriority.UseBorderColor = false;
            this.xrTableCell347.StylePriority.UseForeColor = false;
            this.xrTableCell347.StylePriority.UseTextAlignment = false;
            this.xrTableCell347.Text = "-";
            this.xrTableCell347.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell347.Weight = 0.89799746231631616D;
            // 
            // xrTableRow131
            // 
            this.xrTableRow131.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell348,
            this.lblAOCount,
            this.lblAOBalance});
            this.xrTableRow131.Name = "xrTableRow131";
            this.xrTableRow131.Weight = 0.400000244140774D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell348.BorderColor = System.Drawing.Color.White;
            this.xrTableCell348.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBackColor = false;
            this.xrTableCell348.StylePriority.UseBorderColor = false;
            this.xrTableCell348.StylePriority.UseForeColor = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.Text = "Court Notices [Judgements / Admin Orders / Sequestrations]";
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell348.Weight = 1.1582138725517057D;
            // 
            // lblAOCount
            // 
            this.lblAOCount.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAOCount.BorderColor = System.Drawing.Color.White;
            this.lblAOCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.CourtNoticeCount")});
            this.lblAOCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblAOCount.Name = "lblAOCount";
            this.lblAOCount.StylePriority.UseBackColor = false;
            this.lblAOCount.StylePriority.UseBorderColor = false;
            this.lblAOCount.StylePriority.UseForeColor = false;
            this.lblAOCount.StylePriority.UseTextAlignment = false;
            this.lblAOCount.Text = "lblAOCount";
            this.lblAOCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAOCount.Weight = 0.94759088822440773D;
            // 
            // lblAOBalance
            // 
            this.lblAOBalance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAOBalance.BorderColor = System.Drawing.Color.White;
            this.lblAOBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalCourtNoticeAmt")});
            this.lblAOBalance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblAOBalance.Name = "lblAOBalance";
            this.lblAOBalance.StylePriority.UseBackColor = false;
            this.lblAOBalance.StylePriority.UseBorderColor = false;
            this.lblAOBalance.StylePriority.UseForeColor = false;
            this.lblAOBalance.StylePriority.UseTextAlignment = false;
            this.lblAOBalance.Text = "lblAOBalance";
            this.lblAOBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAOBalance.Weight = 0.89799746231631616D;
            // 
            // xrTableRow132
            // 
            this.xrTableRow132.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell351,
            this.lblenq90otherTotvalue,
            this.xrTableCell353});
            this.xrTableRow132.Name = "xrTableRow132";
            this.xrTableRow132.Weight = 0.400000244140774D;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.BorderColor = System.Drawing.Color.White;
            this.xrTableCell351.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.StylePriority.UseBackColor = false;
            this.xrTableCell351.StylePriority.UseBorderColor = false;
            this.xrTableCell351.StylePriority.UseForeColor = false;
            this.xrTableCell351.StylePriority.UseTextAlignment = false;
            this.xrTableCell351.Text = "Enquiries Done in the last 90 days";
            this.xrTableCell351.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell351.Weight = 1.1582138725517057D;
            // 
            // lblenq90otherTotvalue
            // 
            this.lblenq90otherTotvalue.BorderColor = System.Drawing.Color.White;
            this.lblenq90otherTotvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoOfEnquiriesLast90DaysOTH")});
            this.lblenq90otherTotvalue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblenq90otherTotvalue.Name = "lblenq90otherTotvalue";
            this.lblenq90otherTotvalue.StylePriority.UseBackColor = false;
            this.lblenq90otherTotvalue.StylePriority.UseBorderColor = false;
            this.lblenq90otherTotvalue.StylePriority.UseForeColor = false;
            this.lblenq90otherTotvalue.StylePriority.UseTextAlignment = false;
            this.lblenq90otherTotvalue.Text = "lblenq90otherTotvalue";
            this.lblenq90otherTotvalue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblenq90otherTotvalue.Weight = 0.94759088822440773D;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.BorderColor = System.Drawing.Color.White;
            this.xrTableCell353.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseBackColor = false;
            this.xrTableCell353.StylePriority.UseBorderColor = false;
            this.xrTableCell353.StylePriority.UseForeColor = false;
            this.xrTableCell353.StylePriority.UseTextAlignment = false;
            this.xrTableCell353.Text = "-";
            this.xrTableCell353.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell353.Weight = 0.89799746231631616D;
            // 
            // xrTableRow133
            // 
            this.xrTableRow133.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell354,
            this.lblAcc30TotValue,
            this.xrTableCell356});
            this.xrTableRow133.Name = "xrTableRow133";
            this.xrTableRow133.Weight = 0.400000244140774D;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell354.BorderColor = System.Drawing.Color.White;
            this.xrTableCell354.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseBackColor = false;
            this.xrTableCell354.StylePriority.UseBorderColor = false;
            this.xrTableCell354.StylePriority.UseForeColor = false;
            this.xrTableCell354.StylePriority.UseTextAlignment = false;
            this.xrTableCell354.Text = "Accounts opened within the last 45 days";
            this.xrTableCell354.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell354.Weight = 1.1582138725517057D;
            // 
            // lblAcc30TotValue
            // 
            this.lblAcc30TotValue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAcc30TotValue.BorderColor = System.Drawing.Color.White;
            this.lblAcc30TotValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoOfAccountsOpenedinLast45Days")});
            this.lblAcc30TotValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblAcc30TotValue.Name = "lblAcc30TotValue";
            this.lblAcc30TotValue.StylePriority.UseBackColor = false;
            this.lblAcc30TotValue.StylePriority.UseBorderColor = false;
            this.lblAcc30TotValue.StylePriority.UseForeColor = false;
            this.lblAcc30TotValue.StylePriority.UseTextAlignment = false;
            this.lblAcc30TotValue.Text = "lblAcc30Value";
            this.lblAcc30TotValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAcc30TotValue.Weight = 0.94759088822440773D;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell356.BorderColor = System.Drawing.Color.White;
            this.xrTableCell356.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseBackColor = false;
            this.xrTableCell356.StylePriority.UseBorderColor = false;
            this.xrTableCell356.StylePriority.UseForeColor = false;
            this.xrTableCell356.StylePriority.UseTextAlignment = false;
            this.xrTableCell356.Text = "-";
            this.xrTableCell356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell356.Weight = 0.89799746231631616D;
            // 
            // xrTableRow134
            // 
            this.xrTableRow134.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell357,
            this.lblAccDefaultCount,
            this.lblADBalance});
            this.xrTableRow134.Name = "xrTableRow134";
            this.xrTableRow134.Weight = 0.400000244140774D;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.BorderColor = System.Drawing.Color.White;
            this.xrTableCell357.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseBackColor = false;
            this.xrTableCell357.StylePriority.UseBorderColor = false;
            this.xrTableCell357.StylePriority.UseForeColor = false;
            this.xrTableCell357.StylePriority.UseTextAlignment = false;
            this.xrTableCell357.Text = "Public Domain - Adverse / Defaults";
            this.xrTableCell357.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell357.Weight = 1.1582138725517057D;
            // 
            // lblAccDefaultCount
            // 
            this.lblAccDefaultCount.BorderColor = System.Drawing.Color.White;
            this.lblAccDefaultCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoofAccountdefaults")});
            this.lblAccDefaultCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblAccDefaultCount.Name = "lblAccDefaultCount";
            this.lblAccDefaultCount.StylePriority.UseBackColor = false;
            this.lblAccDefaultCount.StylePriority.UseBorderColor = false;
            this.lblAccDefaultCount.StylePriority.UseForeColor = false;
            this.lblAccDefaultCount.StylePriority.UseTextAlignment = false;
            this.lblAccDefaultCount.Text = "lblAccDefaultCount";
            this.lblAccDefaultCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccDefaultCount.Weight = 0.94759088822440773D;
            // 
            // lblADBalance
            // 
            this.lblADBalance.BorderColor = System.Drawing.Color.White;
            this.lblADBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalAdverseAmt")});
            this.lblADBalance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblADBalance.Name = "lblADBalance";
            this.lblADBalance.StylePriority.UseBackColor = false;
            this.lblADBalance.StylePriority.UseBorderColor = false;
            this.lblADBalance.StylePriority.UseForeColor = false;
            this.lblADBalance.StylePriority.UseTextAlignment = false;
            this.lblADBalance.Text = "[TotalAdverseAmt]";
            this.lblADBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblADBalance.Weight = 0.89799746231631616D;
            // 
            // xrTableRow135
            // 
            this.xrTableRow135.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell360,
            this.lblDLCount,
            this.lblDLBalance});
            this.xrTableRow135.Name = "xrTableRow135";
            this.xrTableRow135.Weight = 0.400000244140774D;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell360.BorderColor = System.Drawing.Color.White;
            this.xrTableCell360.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseBackColor = false;
            this.xrTableCell360.StylePriority.UseBorderColor = false;
            this.xrTableCell360.StylePriority.UseForeColor = false;
            this.xrTableCell360.StylePriority.UseTextAlignment = false;
            this.xrTableCell360.Text = "XDS Default listing";
            this.xrTableCell360.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell360.Weight = 1.1582138725517057D;
            // 
            // lblDLCount
            // 
            this.lblDLCount.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDLCount.BorderColor = System.Drawing.Color.White;
            this.lblDLCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.DefaultListingCount")});
            this.lblDLCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDLCount.Name = "lblDLCount";
            this.lblDLCount.StylePriority.UseBackColor = false;
            this.lblDLCount.StylePriority.UseBorderColor = false;
            this.lblDLCount.StylePriority.UseForeColor = false;
            this.lblDLCount.StylePriority.UseTextAlignment = false;
            this.lblDLCount.Text = "lblDLCount";
            this.lblDLCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDLCount.Weight = 0.94759088822440773D;
            // 
            // lblDLBalance
            // 
            this.lblDLBalance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDLBalance.BorderColor = System.Drawing.Color.White;
            this.lblDLBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.DefaultListingAmt")});
            this.lblDLBalance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDLBalance.Name = "lblDLBalance";
            this.lblDLBalance.StylePriority.UseBackColor = false;
            this.lblDLBalance.StylePriority.UseBorderColor = false;
            this.lblDLBalance.StylePriority.UseForeColor = false;
            this.lblDLBalance.StylePriority.UseTextAlignment = false;
            this.lblDLBalance.Text = "lblDLBalance";
            this.lblDLBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDLBalance.Weight = 0.89799746231631616D;
            // 
            // xrTableRow136
            // 
            this.xrTableRow136.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell363,
            this.lblROCount,
            this.lblROAmt});
            this.xrTableRow136.Name = "xrTableRow136";
            this.xrTableRow136.Weight = 0.400000244140774D;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.BorderColor = System.Drawing.Color.White;
            this.xrTableCell363.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseBackColor = false;
            this.xrTableCell363.StylePriority.UseBorderColor = false;
            this.xrTableCell363.StylePriority.UseForeColor = false;
            this.xrTableCell363.StylePriority.UseTextAlignment = false;
            this.xrTableCell363.Text = "Rehabilitation Orders";
            this.xrTableCell363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell363.Weight = 1.1582138725517057D;
            // 
            // lblROCount
            // 
            this.lblROCount.BorderColor = System.Drawing.Color.White;
            this.lblROCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.RehabilitationOrdersCount")});
            this.lblROCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblROCount.Name = "lblROCount";
            this.lblROCount.StylePriority.UseBackColor = false;
            this.lblROCount.StylePriority.UseBorderColor = false;
            this.lblROCount.StylePriority.UseForeColor = false;
            this.lblROCount.StylePriority.UseTextAlignment = false;
            this.lblROCount.Text = "lblROCount";
            this.lblROCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblROCount.Weight = 0.94759088822440773D;
            // 
            // lblROAmt
            // 
            this.lblROAmt.BorderColor = System.Drawing.Color.White;
            this.lblROAmt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.RehabilitationOrdersAmt")});
            this.lblROAmt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblROAmt.Name = "lblROAmt";
            this.lblROAmt.StylePriority.UseBackColor = false;
            this.lblROAmt.StylePriority.UseBorderColor = false;
            this.lblROAmt.StylePriority.UseForeColor = false;
            this.lblROAmt.StylePriority.UseTextAlignment = false;
            this.lblROAmt.Text = "lblROAmt";
            this.lblROAmt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblROAmt.Weight = 0.89799746231631616D;
            // 
            // xrTableRow137
            // 
            this.xrTableRow137.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell366,
            this.lblEnq24Count,
            this.xrTableCell368});
            this.xrTableRow137.Name = "xrTableRow137";
            this.xrTableRow137.Weight = 0.400000244140774D;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell366.BorderColor = System.Drawing.Color.White;
            this.xrTableCell366.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.StylePriority.UseBackColor = false;
            this.xrTableCell366.StylePriority.UseBorderColor = false;
            this.xrTableCell366.StylePriority.UseForeColor = false;
            this.xrTableCell366.StylePriority.UseTextAlignment = false;
            this.xrTableCell366.Text = " Enquiries (last 24 months)";
            this.xrTableCell366.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell366.Weight = 1.1582138725517057D;
            // 
            // lblEnq24Count
            // 
            this.lblEnq24Count.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblEnq24Count.BorderColor = System.Drawing.Color.White;
            this.lblEnq24Count.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NoOfEnqinLast24Months")});
            this.lblEnq24Count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblEnq24Count.Name = "lblEnq24Count";
            this.lblEnq24Count.StylePriority.UseBackColor = false;
            this.lblEnq24Count.StylePriority.UseBorderColor = false;
            this.lblEnq24Count.StylePriority.UseForeColor = false;
            this.lblEnq24Count.StylePriority.UseTextAlignment = false;
            this.lblEnq24Count.Text = "lblEnq24Count";
            this.lblEnq24Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblEnq24Count.Weight = 0.94759088822440773D;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell368.BorderColor = System.Drawing.Color.White;
            this.xrTableCell368.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseBackColor = false;
            this.xrTableCell368.StylePriority.UseBorderColor = false;
            this.xrTableCell368.StylePriority.UseForeColor = false;
            this.xrTableCell368.StylePriority.UseTextAlignment = false;
            this.xrTableCell368.Text = "-";
            this.xrTableCell368.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell368.Weight = 0.89799746231631616D;
            // 
            // xrTableRow138
            // 
            this.xrTableRow138.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell369,
            this.xrTableCell370,
            this.lblPurchPrice});
            this.xrTableRow138.Name = "xrTableRow138";
            this.xrTableRow138.Weight = 0.400000244140774D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.BorderColor = System.Drawing.Color.White;
            this.xrTableCell369.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBackColor = false;
            this.xrTableCell369.StylePriority.UseBorderColor = false;
            this.xrTableCell369.StylePriority.UseForeColor = false;
            this.xrTableCell369.StylePriority.UseTextAlignment = false;
            this.xrTableCell369.Text = "Current Property Interests";
            this.xrTableCell369.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell369.Weight = 1.1582138725517057D;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.BorderColor = System.Drawing.Color.White;
            this.xrTableCell370.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalProperty")});
            this.xrTableCell370.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.StylePriority.UseBackColor = false;
            this.xrTableCell370.StylePriority.UseBorderColor = false;
            this.xrTableCell370.StylePriority.UseForeColor = false;
            this.xrTableCell370.StylePriority.UseTextAlignment = false;
            this.xrTableCell370.Text = "xrTableCell370";
            this.xrTableCell370.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell370.Weight = 0.94759088822440773D;
            // 
            // lblPurchPrice
            // 
            this.lblPurchPrice.BorderColor = System.Drawing.Color.White;
            this.lblPurchPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.PurchasePrice")});
            this.lblPurchPrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblPurchPrice.Name = "lblPurchPrice";
            this.lblPurchPrice.StylePriority.UseBackColor = false;
            this.lblPurchPrice.StylePriority.UseBorderColor = false;
            this.lblPurchPrice.StylePriority.UseForeColor = false;
            this.lblPurchPrice.StylePriority.UseTextAlignment = false;
            this.lblPurchPrice.Text = "lblPurchPrice";
            this.lblPurchPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPurchPrice.Weight = 0.89799746231631616D;
            // 
            // xrTableRow139
            // 
            this.xrTableRow139.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell372,
            this.xrTableCell373,
            this.xrTableCell374});
            this.xrTableRow139.Name = "xrTableRow139";
            this.xrTableRow139.Weight = 0.400000244140774D;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell372.BorderColor = System.Drawing.Color.White;
            this.xrTableCell372.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.StylePriority.UseBackColor = false;
            this.xrTableCell372.StylePriority.UseBorderColor = false;
            this.xrTableCell372.StylePriority.UseForeColor = false;
            this.xrTableCell372.StylePriority.UseTextAlignment = false;
            this.xrTableCell372.Text = "Directorships";
            this.xrTableCell372.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell372.Weight = 1.1582138725517057D;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell373.BorderColor = System.Drawing.Color.White;
            this.xrTableCell373.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.NumberOfCompanyDirector")});
            this.xrTableCell373.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.StylePriority.UseBackColor = false;
            this.xrTableCell373.StylePriority.UseBorderColor = false;
            this.xrTableCell373.StylePriority.UseForeColor = false;
            this.xrTableCell373.StylePriority.UseTextAlignment = false;
            this.xrTableCell373.Text = "xrTableCell373";
            this.xrTableCell373.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell373.Weight = 0.94759088822440773D;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell374.BorderColor = System.Drawing.Color.White;
            this.xrTableCell374.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.StylePriority.UseBackColor = false;
            this.xrTableCell374.StylePriority.UseBorderColor = false;
            this.xrTableCell374.StylePriority.UseForeColor = false;
            this.xrTableCell374.StylePriority.UseTextAlignment = false;
            this.xrTableCell374.Text = "-";
            this.xrTableCell374.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell374.Weight = 0.89799746231631616D;
            // 
            // xrTableRow140
            // 
            this.xrTableRow140.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell375,
            this.lblDebtReviewStatus});
            this.xrTableRow140.Name = "xrTableRow140";
            this.xrTableRow140.Weight = 0.400000244140774D;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.BorderColor = System.Drawing.Color.White;
            this.xrTableCell375.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.StylePriority.UseBackColor = false;
            this.xrTableCell375.StylePriority.UseBorderColor = false;
            this.xrTableCell375.StylePriority.UseForeColor = false;
            this.xrTableCell375.StylePriority.UseTextAlignment = false;
            this.xrTableCell375.Text = "Debt Review Status";
            this.xrTableCell375.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell375.Weight = 1.1582138725517057D;
            // 
            // lblDebtReviewStatus
            // 
            this.lblDebtReviewStatus.BorderColor = System.Drawing.Color.White;
            this.lblDebtReviewStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.DebtReviewStatus")});
            this.lblDebtReviewStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDebtReviewStatus.Name = "lblDebtReviewStatus";
            this.lblDebtReviewStatus.StylePriority.UseBackColor = false;
            this.lblDebtReviewStatus.StylePriority.UseBorderColor = false;
            this.lblDebtReviewStatus.StylePriority.UseForeColor = false;
            this.lblDebtReviewStatus.StylePriority.UseTextAlignment = false;
            this.lblDebtReviewStatus.Text = "lblDebtReviewStatus";
            this.lblDebtReviewStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDebtReviewStatus.Weight = 1.8455883505407236D;
            // 
            // xrTableRow141
            // 
            this.xrTableRow141.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell378,
            this.lblDisputeMsg});
            this.xrTableRow141.Name = "xrTableRow141";
            this.xrTableRow141.Weight = 0.400000244140774D;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell378.BorderColor = System.Drawing.Color.White;
            this.xrTableCell378.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.StylePriority.UseBackColor = false;
            this.xrTableCell378.StylePriority.UseBorderColor = false;
            this.xrTableCell378.StylePriority.UseForeColor = false;
            this.xrTableCell378.StylePriority.UseTextAlignment = false;
            this.xrTableCell378.Text = "Dispute Information";
            this.xrTableCell378.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell378.Weight = 1.1582138725517057D;
            // 
            // lblDisputeMsg
            // 
            this.lblDisputeMsg.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDisputeMsg.BorderColor = System.Drawing.Color.White;
            this.lblDisputeMsg.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.DisputeMessage")});
            this.lblDisputeMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDisputeMsg.Name = "lblDisputeMsg";
            this.lblDisputeMsg.StylePriority.UseBackColor = false;
            this.lblDisputeMsg.StylePriority.UseBorderColor = false;
            this.lblDisputeMsg.StylePriority.UseForeColor = false;
            this.lblDisputeMsg.StylePriority.UseTextAlignment = false;
            this.lblDisputeMsg.Text = "lblDisputeMsg";
            this.lblDisputeMsg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDisputeMsg.Weight = 1.8455883505407236D;
            // 
            // GroupHeaderDebtSummary
            // 
            this.GroupHeaderDebtSummary.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDishonouredChequeHeader,
            this.xrTable6});
            this.GroupHeaderDebtSummary.HeightF = 110.4167F;
            this.GroupHeaderDebtSummary.Name = "GroupHeaderDebtSummary";
            // 
            // tblDishonouredChequeHeader
            // 
            this.tblDishonouredChequeHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.tblDishonouredChequeHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDishonouredChequeHeader.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.tblDishonouredChequeHeader.ForeColor = System.Drawing.Color.Gray;
            this.tblDishonouredChequeHeader.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 90.41672F);
            this.tblDishonouredChequeHeader.Name = "tblDishonouredChequeHeader";
            this.tblDishonouredChequeHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow78});
            this.tblDishonouredChequeHeader.SizeF = new System.Drawing.SizeF(880F, 20.00001F);
            this.tblDishonouredChequeHeader.StylePriority.UseBackColor = false;
            this.tblDishonouredChequeHeader.StylePriority.UseBorders = false;
            this.tblDishonouredChequeHeader.StylePriority.UseFont = false;
            this.tblDishonouredChequeHeader.StylePriority.UseForeColor = false;
            this.tblDishonouredChequeHeader.StylePriority.UseTextAlignment = false;
            this.tblDishonouredChequeHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell257,
            this.xrTableCell258});
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.Weight = 0.400000244140774D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell255.BorderColor = System.Drawing.Color.White;
            this.xrTableCell255.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell255.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBackColor = false;
            this.xrTableCell255.StylePriority.UseBorderColor = false;
            this.xrTableCell255.StylePriority.UseFont = false;
            this.xrTableCell255.StylePriority.UseForeColor = false;
            this.xrTableCell255.StylePriority.UseTextAlignment = false;
            this.xrTableCell255.Text = "Description";
            this.xrTableCell255.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell255.Weight = 1.1582138725517057D;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell257.BorderColor = System.Drawing.Color.White;
            this.xrTableCell257.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell257.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBackColor = false;
            this.xrTableCell257.StylePriority.UseBorderColor = false;
            this.xrTableCell257.StylePriority.UseFont = false;
            this.xrTableCell257.StylePriority.UseForeColor = false;
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "Count";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell257.Weight = 0.94759088822440773D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell258.BorderColor = System.Drawing.Color.White;
            this.xrTableCell258.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell258.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBackColor = false;
            this.xrTableCell258.StylePriority.UseBorderColor = false;
            this.xrTableCell258.StylePriority.UseFont = false;
            this.xrTableCell258.StylePriority.UseForeColor = false;
            this.xrTableCell258.StylePriority.UseTextAlignment = false;
            this.xrTableCell258.Text = "Amount";
            this.xrTableCell258.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell258.Weight = 0.89799746231631616D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable6.ForeColor = System.Drawing.Color.Gray;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24});
            this.xrTable6.SizeF = new System.Drawing.SizeF(880F, 70F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseForeColor = false;
            this.xrTable6.StylePriority.UsePadding = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.8D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.BackColor = System.Drawing.Color.White;
            this.xrTableCell52.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell52.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell52.StylePriority.UseBackColor = false;
            this.xrTableCell52.StylePriority.UseBorderColor = false;
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseForeColor = false;
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.Text = "Debt Summary";
            this.xrTableCell52.Weight = 3D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.53333333333333344D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.BackColor = System.Drawing.Color.White;
            this.xrTableCell56.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell56.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell56.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell56.Multiline = true;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell56.StylePriority.UseBackColor = false;
            this.xrTableCell56.StylePriority.UseBorderColor = false;
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.StylePriority.UseForeColor = false;
            this.xrTableCell56.StylePriority.UsePadding = false;
            this.xrTableCell56.Text = "This section displays a summary of all the consumer’s current debt obligations, l" +
    "egal enforcement action taken, Court Notices, enquiries done on this consumer ";
            this.xrTableCell56.Weight = 3D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.53333333333333344D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.BackColor = System.Drawing.Color.White;
            this.xrTableCell59.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell59.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell59.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBackColor = false;
            this.xrTableCell59.StylePriority.UseBorderColor = false;
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseForeColor = false;
            this.xrTableCell59.Text = "by credit providers and debt counselling information.";
            this.xrTableCell59.Weight = 3D;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5});
            this.DetailReport4.DataMember = "ConsumerAccountStatus";
            this.DetailReport4.Level = 4;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.Detail5.HeightF = 23.75006F;
            this.Detail5.Name = "Detail5";
            // 
            // xrTable11
            // 
            this.xrTable11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable11.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable11.ForeColor = System.Drawing.Color.Gray;
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable11.SizeF = new System.Drawing.SizeF(880F, 23.75006F);
            this.xrTable11.StylePriority.UseBackColor = false;
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.StylePriority.UseFont = false;
            this.xrTable11.StylePriority.UseForeColor = false;
            this.xrTable11.StylePriority.UseTextAlignment = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.lblCreditLimitD,
            this.lblCurrBalanceD,
            this.lblInstallementD,
            this.lblArrearsAmountD,
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.400000244140774D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.BackColor = System.Drawing.Color.White;
            this.xrTableCell79.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountOpenedDate")});
            this.xrTableCell79.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBackColor = false;
            this.xrTableCell79.StylePriority.UseBorderColor = false;
            this.xrTableCell79.StylePriority.UseFont = false;
            this.xrTableCell79.StylePriority.UseForeColor = false;
            this.xrTableCell79.Text = "xrTableCell79";
            this.xrTableCell79.Weight = 0.31197225114723515D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.BackColor = System.Drawing.Color.White;
            this.xrTableCell80.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.SubscriberName")});
            this.xrTableCell80.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell80.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBackColor = false;
            this.xrTableCell80.StylePriority.UseBorderColor = false;
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.StylePriority.UseForeColor = false;
            this.xrTableCell80.Text = "xrTableCell80";
            this.xrTableCell80.Weight = 0.276102988195966D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.BackColor = System.Drawing.Color.White;
            this.xrTableCell81.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountType")});
            this.xrTableCell81.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell81.Multiline = true;
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBackColor = false;
            this.xrTableCell81.StylePriority.UseBorderColor = false;
            this.xrTableCell81.StylePriority.UseFont = false;
            this.xrTableCell81.StylePriority.UseForeColor = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "xrTableCell81";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell81.Weight = 0.31413261728027475D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.BackColor = System.Drawing.Color.White;
            this.xrTableCell82.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountNo")});
            this.xrTableCell82.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseBackColor = false;
            this.xrTableCell82.StylePriority.UseBorderColor = false;
            this.xrTableCell82.StylePriority.UseFont = false;
            this.xrTableCell82.StylePriority.UseForeColor = false;
            this.xrTableCell82.Text = "xrTableCell82";
            this.xrTableCell82.Weight = 0.33245027884541006D;
            // 
            // lblCreditLimitD
            // 
            this.lblCreditLimitD.BackColor = System.Drawing.Color.White;
            this.lblCreditLimitD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblCreditLimitD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CreditLimitAmt")});
            this.lblCreditLimitD.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCreditLimitD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblCreditLimitD.Multiline = true;
            this.lblCreditLimitD.Name = "lblCreditLimitD";
            this.lblCreditLimitD.StylePriority.UseBackColor = false;
            this.lblCreditLimitD.StylePriority.UseBorderColor = false;
            this.lblCreditLimitD.StylePriority.UseFont = false;
            this.lblCreditLimitD.StylePriority.UseForeColor = false;
            this.lblCreditLimitD.Text = "lblCreditLimitD";
            this.lblCreditLimitD.Weight = 0.35818154699500671D;
            // 
            // lblCurrBalanceD
            // 
            this.lblCurrBalanceD.BackColor = System.Drawing.Color.White;
            this.lblCurrBalanceD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblCurrBalanceD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CurrentBalanceAmt")});
            this.lblCurrBalanceD.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCurrBalanceD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblCurrBalanceD.Multiline = true;
            this.lblCurrBalanceD.Name = "lblCurrBalanceD";
            this.lblCurrBalanceD.StylePriority.UseBackColor = false;
            this.lblCurrBalanceD.StylePriority.UseBorderColor = false;
            this.lblCurrBalanceD.StylePriority.UseFont = false;
            this.lblCurrBalanceD.StylePriority.UseForeColor = false;
            this.lblCurrBalanceD.Text = "lblCurrBalanceD";
            this.lblCurrBalanceD.Weight = 0.31645184042843749D;
            // 
            // lblInstallementD
            // 
            this.lblInstallementD.BackColor = System.Drawing.Color.White;
            this.lblInstallementD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblInstallementD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.MonthlyInstalmentAmt")});
            this.lblInstallementD.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblInstallementD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblInstallementD.Multiline = true;
            this.lblInstallementD.Name = "lblInstallementD";
            this.lblInstallementD.StylePriority.UseBackColor = false;
            this.lblInstallementD.StylePriority.UseBorderColor = false;
            this.lblInstallementD.StylePriority.UseFont = false;
            this.lblInstallementD.StylePriority.UseForeColor = false;
            this.lblInstallementD.Text = "lblInstallementD";
            this.lblInstallementD.Weight = 0.27847846977523222D;
            // 
            // lblArrearsAmountD
            // 
            this.lblArrearsAmountD.BackColor = System.Drawing.Color.White;
            this.lblArrearsAmountD.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblArrearsAmountD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.ArrearsAmt")});
            this.lblArrearsAmountD.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblArrearsAmountD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblArrearsAmountD.Multiline = true;
            this.lblArrearsAmountD.Name = "lblArrearsAmountD";
            this.lblArrearsAmountD.StylePriority.UseBackColor = false;
            this.lblArrearsAmountD.StylePriority.UseBorderColor = false;
            this.lblArrearsAmountD.StylePriority.UseFont = false;
            this.lblArrearsAmountD.StylePriority.UseForeColor = false;
            this.lblArrearsAmountD.Text = "lblArrearsAmountD";
            this.lblArrearsAmountD.Weight = 0.27467984110768029D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.BackColor = System.Drawing.Color.White;
            this.xrTableCell87.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell87.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.StatusCodeDesc")});
            this.xrTableCell87.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell87.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell87.Multiline = true;
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBackColor = false;
            this.xrTableCell87.StylePriority.UseBorderColor = false;
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.StylePriority.UseForeColor = false;
            this.xrTableCell87.Text = "xrTableCell87";
            this.xrTableCell87.Weight = 0.23156428203371185D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.BackColor = System.Drawing.Color.White;
            this.xrTableCell88.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell88.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.LastPaymentDate")});
            this.xrTableCell88.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell88.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell88.Multiline = true;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBackColor = false;
            this.xrTableCell88.StylePriority.UseBorderColor = false;
            this.xrTableCell88.StylePriority.UseFont = false;
            this.xrTableCell88.StylePriority.UseForeColor = false;
            this.xrTableCell88.Text = "xrTableCell88";
            this.xrTableCell88.Weight = 0.30978810728347506D;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10,
            this.xrTable9});
            this.GroupHeader5.HeightF = 112.9167F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable10.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable10.ForeColor = System.Drawing.Color.Gray;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 65.20831F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable10.SizeF = new System.Drawing.SizeF(880F, 47.70839F);
            this.xrTable10.StylePriority.UseBackColor = false;
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UseForeColor = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.400000244140774D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell69.BorderColor = System.Drawing.Color.White;
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell69.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBackColor = false;
            this.xrTableCell69.StylePriority.UseBorderColor = false;
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.StylePriority.UseForeColor = false;
            this.xrTableCell69.Text = "Date Account Opened";
            this.xrTableCell69.Weight = 0.31197225114723515D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell70.BorderColor = System.Drawing.Color.White;
            this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell70.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBackColor = false;
            this.xrTableCell70.StylePriority.UseBorderColor = false;
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.StylePriority.UseForeColor = false;
            this.xrTableCell70.Text = "Company";
            this.xrTableCell70.Weight = 0.276102988195966D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell71.BorderColor = System.Drawing.Color.White;
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell71.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell71.Multiline = true;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBackColor = false;
            this.xrTableCell71.StylePriority.UseBorderColor = false;
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UseForeColor = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = "Type of Account";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 0.31413261728027475D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell72.BorderColor = System.Drawing.Color.White;
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell72.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBackColor = false;
            this.xrTableCell72.StylePriority.UseBorderColor = false;
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.StylePriority.UseForeColor = false;
            this.xrTableCell72.Text = "Account No.";
            this.xrTableCell72.Weight = 0.33245027884541006D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell73.BorderColor = System.Drawing.Color.White;
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell73.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell73.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell73.Multiline = true;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBackColor = false;
            this.xrTableCell73.StylePriority.UseBorderColor = false;
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.StylePriority.UseForeColor = false;
            this.xrTableCell73.Text = "Open Balance /Credit Limit";
            this.xrTableCell73.Weight = 0.35818154699500671D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell74.BorderColor = System.Drawing.Color.White;
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell74.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell74.Multiline = true;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBackColor = false;
            this.xrTableCell74.StylePriority.UseBorderColor = false;
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.StylePriority.UseForeColor = false;
            this.xrTableCell74.Text = "Current\r\nBalance";
            this.xrTableCell74.Weight = 0.31645184042843749D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell75.BorderColor = System.Drawing.Color.White;
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell75.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell75.Multiline = true;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBackColor = false;
            this.xrTableCell75.StylePriority.UseBorderColor = false;
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.StylePriority.UseForeColor = false;
            this.xrTableCell75.Text = "Instalment\r\nAmount";
            this.xrTableCell75.Weight = 0.27847846977523222D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell76.BorderColor = System.Drawing.Color.White;
            this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell76.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell76.Multiline = true;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBackColor = false;
            this.xrTableCell76.StylePriority.UseBorderColor = false;
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.StylePriority.UseForeColor = false;
            this.xrTableCell76.Text = "Arrears\r\nAmount";
            this.xrTableCell76.Weight = 0.27467984110768029D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell77.BorderColor = System.Drawing.Color.White;
            this.xrTableCell77.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell77.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell77.Multiline = true;
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBackColor = false;
            this.xrTableCell77.StylePriority.UseBorderColor = false;
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.StylePriority.UseFont = false;
            this.xrTableCell77.StylePriority.UseForeColor = false;
            this.xrTableCell77.Text = "Current\r\nStatus of\r\nAccount";
            this.xrTableCell77.Weight = 0.23156428203371185D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell78.BorderColor = System.Drawing.Color.White;
            this.xrTableCell78.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell78.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell78.Multiline = true;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBackColor = false;
            this.xrTableCell78.StylePriority.UseBorderColor = false;
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.StylePriority.UseFont = false;
            this.xrTableCell78.StylePriority.UseForeColor = false;
            this.xrTableCell78.Text = "Last Paid\r\nDate";
            this.xrTableCell78.Weight = 0.30978810728347506D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable9.ForeColor = System.Drawing.Color.Gray;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow27});
            this.xrTable9.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseForeColor = false;
            this.xrTable9.StylePriority.UsePadding = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.8D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.BackColor = System.Drawing.Color.White;
            this.xrTableCell67.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell67.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell67.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell67.StylePriority.UseBackColor = false;
            this.xrTableCell67.StylePriority.UseBorderColor = false;
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.StylePriority.UseForeColor = false;
            this.xrTableCell67.StylePriority.UsePadding = false;
            this.xrTableCell67.Text = "Credit Account Status";
            this.xrTableCell67.Weight = 3D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.53333333333333344D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell68.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell68.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell68.Multiline = true;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell68.StylePriority.UseBorderColor = false;
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UseForeColor = false;
            this.xrTableCell68.StylePriority.UsePadding = false;
            this.xrTableCell68.Text = "This section includes 24 month payment behaviour of all credit agreements as repo" +
    "rted by the Credit Grantors belonging to Credit Providers Association (CPA).";
            this.xrTableCell68.Weight = 3D;
            // 
            // DetailReport5
            // 
            this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6});
            this.DetailReport5.DataMember = "Consumer24MonthlyPayment";
            this.DetailReport5.Level = 5;
            this.DetailReport5.Name = "DetailReport5";
            // 
            // Detail6
            // 
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
            this.Detail6.HeightF = 33.90636F;
            this.Detail6.Name = "Detail6";
            // 
            // xrTable12
            // 
            this.xrTable12.BackColor = System.Drawing.Color.White;
            this.xrTable12.BorderColor = System.Drawing.Color.White;
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable12.SizeF = new System.Drawing.SizeF(880.0001F, 33.90636F);
            this.xrTable12.StylePriority.UseBackColor = false;
            this.xrTable12.StylePriority.UseBorderColor = false;
            this.xrTable12.StylePriority.UseBorders = false;
            this.xrTable12.StylePriority.UseFont = false;
            this.xrTable12.StylePriority.UsePadding = false;
            this.xrTable12.StylePriority.UseTextAlignment = false;
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow30.BorderColor = System.Drawing.Color.White;
            this.xrTableRow30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell433,
            this.lblmonth01,
            this.lblmonth02,
            this.lblmonth03,
            this.lblmonth04,
            this.lblmonth05,
            this.lblmonth06,
            this.lblmonth07,
            this.lblmonth08,
            this.lblmonth09,
            this.lblmonth10,
            this.lblmonth11,
            this.lblmonth12,
            this.lblmonth13,
            this.lblmonth14,
            this.lblmonth15,
            this.lblmonth16,
            this.lblmonth17,
            this.lblmonth18,
            this.lblmonth19,
            this.lblmonth20,
            this.lblmonth21,
            this.lblmonth22,
            this.lblmonth23,
            this.lblmonth24});
            this.xrTableRow30.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow30.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableRow30.StylePriority.UseBackColor = false;
            this.xrTableRow30.StylePriority.UseBorderColor = false;
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.StylePriority.UseFont = false;
            this.xrTableRow30.StylePriority.UseForeColor = false;
            this.xrTableRow30.StylePriority.UsePadding = false;
            this.xrTableRow30.StylePriority.UseTextAlignment = false;
            this.xrTableRow30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow30.Weight = 0.48905657212263165D;
            // 
            // xrTableCell433
            // 
            this.xrTableCell433.BackColor = System.Drawing.Color.White;
            this.xrTableCell433.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.SubscriberName")});
            this.xrTableCell433.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.xrTableCell433.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell433.Name = "xrTableCell433";
            this.xrTableCell433.StylePriority.UseBackColor = false;
            this.xrTableCell433.StylePriority.UseFont = false;
            this.xrTableCell433.StylePriority.UseForeColor = false;
            this.xrTableCell433.Text = "xrTableCell433";
            this.xrTableCell433.Weight = 0.17972280852009512D;
            // 
            // lblmonth01
            // 
            this.lblmonth01.BackColor = System.Drawing.Color.White;
            this.lblmonth01.BorderColor = System.Drawing.Color.White;
            this.lblmonth01.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth01.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm01});
            this.lblmonth01.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M01")});
            this.lblmonth01.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth01.Name = "lblmonth01";
            this.lblmonth01.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblmonth01.StylePriority.UseBackColor = false;
            this.lblmonth01.StylePriority.UseBorderColor = false;
            this.lblmonth01.StylePriority.UseBorders = false;
            this.lblmonth01.StylePriority.UseFont = false;
            this.lblmonth01.StylePriority.UseForeColor = false;
            this.lblmonth01.StylePriority.UsePadding = false;
            this.lblmonth01.StylePriority.UseTextAlignment = false;
            this.lblmonth01.Text = "lblmonth01";
            this.lblmonth01.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth01.Weight = 0.037997794864054107D;
            // 
            // pbm01
            // 
            this.pbm01.ImageUrl = "~\\images\\0.png";
            this.pbm01.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm01.Name = "pbm01";
            this.pbm01.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm01.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm01.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm01_BeforePrint);
            // 
            // lblmonth02
            // 
            this.lblmonth02.BackColor = System.Drawing.Color.White;
            this.lblmonth02.BorderColor = System.Drawing.Color.White;
            this.lblmonth02.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth02.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm02});
            this.lblmonth02.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M02")});
            this.lblmonth02.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth02.Name = "lblmonth02";
            this.lblmonth02.StylePriority.UseBackColor = false;
            this.lblmonth02.StylePriority.UseBorderColor = false;
            this.lblmonth02.StylePriority.UseBorders = false;
            this.lblmonth02.StylePriority.UseFont = false;
            this.lblmonth02.StylePriority.UseForeColor = false;
            this.lblmonth02.StylePriority.UseTextAlignment = false;
            this.lblmonth02.Text = "lblmonth02";
            this.lblmonth02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth02.Weight = 0.037997795695782113D;
            // 
            // pbm02
            // 
            this.pbm02.ImageUrl = "~\\images\\0.png";
            this.pbm02.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm02.Name = "pbm02";
            this.pbm02.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm02.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm02.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm02_BeforePrint);
            // 
            // lblmonth03
            // 
            this.lblmonth03.BackColor = System.Drawing.Color.White;
            this.lblmonth03.BorderColor = System.Drawing.Color.White;
            this.lblmonth03.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth03.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm03});
            this.lblmonth03.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M03")});
            this.lblmonth03.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth03.Name = "lblmonth03";
            this.lblmonth03.StylePriority.UseBackColor = false;
            this.lblmonth03.StylePriority.UseBorderColor = false;
            this.lblmonth03.StylePriority.UseBorders = false;
            this.lblmonth03.StylePriority.UseFont = false;
            this.lblmonth03.StylePriority.UseForeColor = false;
            this.lblmonth03.StylePriority.UseTextAlignment = false;
            this.lblmonth03.Text = "lblmonth03";
            this.lblmonth03.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth03.Weight = 0.037997795058831309D;
            // 
            // pbm03
            // 
            this.pbm03.ImageUrl = "~\\images\\0.png";
            this.pbm03.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm03.Name = "pbm03";
            this.pbm03.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm03.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm03.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm03_BeforePrint);
            // 
            // lblmonth04
            // 
            this.lblmonth04.BackColor = System.Drawing.Color.White;
            this.lblmonth04.BorderColor = System.Drawing.Color.White;
            this.lblmonth04.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth04.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm04});
            this.lblmonth04.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M04")});
            this.lblmonth04.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth04.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth04.Name = "lblmonth04";
            this.lblmonth04.StylePriority.UseBackColor = false;
            this.lblmonth04.StylePriority.UseBorderColor = false;
            this.lblmonth04.StylePriority.UseBorders = false;
            this.lblmonth04.StylePriority.UseFont = false;
            this.lblmonth04.StylePriority.UseForeColor = false;
            this.lblmonth04.StylePriority.UseTextAlignment = false;
            this.lblmonth04.Text = "lblmonth04";
            this.lblmonth04.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth04.Weight = 0.037997795845440316D;
            // 
            // pbm04
            // 
            this.pbm04.ImageUrl = "~\\images\\0.png";
            this.pbm04.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm04.Name = "pbm04";
            this.pbm04.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm04.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm04.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm04_BeforePrint);
            // 
            // lblmonth05
            // 
            this.lblmonth05.BackColor = System.Drawing.Color.White;
            this.lblmonth05.BorderColor = System.Drawing.Color.White;
            this.lblmonth05.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth05.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm05});
            this.lblmonth05.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M05")});
            this.lblmonth05.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth05.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth05.Name = "lblmonth05";
            this.lblmonth05.StylePriority.UseBackColor = false;
            this.lblmonth05.StylePriority.UseBorderColor = false;
            this.lblmonth05.StylePriority.UseBorders = false;
            this.lblmonth05.StylePriority.UseFont = false;
            this.lblmonth05.StylePriority.UseForeColor = false;
            this.lblmonth05.StylePriority.UseTextAlignment = false;
            this.lblmonth05.Text = "lblmonth05";
            this.lblmonth05.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth05.Weight = 0.037997795293450529D;
            // 
            // pbm05
            // 
            this.pbm05.ImageUrl = "~\\images\\0.png";
            this.pbm05.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm05.Name = "pbm05";
            this.pbm05.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm05.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm05.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm05_BeforePrint);
            // 
            // lblmonth06
            // 
            this.lblmonth06.BackColor = System.Drawing.Color.White;
            this.lblmonth06.BorderColor = System.Drawing.Color.White;
            this.lblmonth06.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth06.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm06});
            this.lblmonth06.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M06")});
            this.lblmonth06.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth06.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth06.Name = "lblmonth06";
            this.lblmonth06.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
            this.lblmonth06.StylePriority.UseBackColor = false;
            this.lblmonth06.StylePriority.UseBorderColor = false;
            this.lblmonth06.StylePriority.UseBorders = false;
            this.lblmonth06.StylePriority.UseFont = false;
            this.lblmonth06.StylePriority.UseForeColor = false;
            this.lblmonth06.StylePriority.UsePadding = false;
            this.lblmonth06.StylePriority.UseTextAlignment = false;
            this.lblmonth06.Text = "lblmonth06";
            this.lblmonth06.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth06.Weight = 0.03799779354973877D;
            // 
            // pbm06
            // 
            this.pbm06.ImageUrl = "~\\images\\0.png";
            this.pbm06.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm06.Name = "pbm06";
            this.pbm06.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm06.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm06.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm06_BeforePrint);
            // 
            // lblmonth07
            // 
            this.lblmonth07.BackColor = System.Drawing.Color.White;
            this.lblmonth07.BorderColor = System.Drawing.Color.White;
            this.lblmonth07.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth07.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm07});
            this.lblmonth07.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M07")});
            this.lblmonth07.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth07.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth07.Name = "lblmonth07";
            this.lblmonth07.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
            this.lblmonth07.StylePriority.UseBackColor = false;
            this.lblmonth07.StylePriority.UseBorderColor = false;
            this.lblmonth07.StylePriority.UseBorders = false;
            this.lblmonth07.StylePriority.UseFont = false;
            this.lblmonth07.StylePriority.UseForeColor = false;
            this.lblmonth07.StylePriority.UsePadding = false;
            this.lblmonth07.StylePriority.UseTextAlignment = false;
            this.lblmonth07.Text = "lblmonth07";
            this.lblmonth07.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth07.Weight = 0.037997794511818332D;
            // 
            // pbm07
            // 
            this.pbm07.ImageUrl = "~\\images\\0.png";
            this.pbm07.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm07.Name = "pbm07";
            this.pbm07.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm07.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm07.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm07_BeforePrint);
            // 
            // lblmonth08
            // 
            this.lblmonth08.BackColor = System.Drawing.Color.White;
            this.lblmonth08.BorderColor = System.Drawing.Color.White;
            this.lblmonth08.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth08.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm08});
            this.lblmonth08.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M08")});
            this.lblmonth08.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth08.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth08.Name = "lblmonth08";
            this.lblmonth08.StylePriority.UseBackColor = false;
            this.lblmonth08.StylePriority.UseBorderColor = false;
            this.lblmonth08.StylePriority.UseBorders = false;
            this.lblmonth08.StylePriority.UseFont = false;
            this.lblmonth08.StylePriority.UseForeColor = false;
            this.lblmonth08.StylePriority.UseTextAlignment = false;
            this.lblmonth08.Text = "lblmonth08";
            this.lblmonth08.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth08.Weight = 0.037997794062394084D;
            // 
            // pbm08
            // 
            this.pbm08.ImageUrl = "~\\images\\0.png";
            this.pbm08.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm08.Name = "pbm08";
            this.pbm08.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm08.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm08.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm08_BeforePrint);
            // 
            // lblmonth09
            // 
            this.lblmonth09.BackColor = System.Drawing.Color.White;
            this.lblmonth09.BorderColor = System.Drawing.Color.White;
            this.lblmonth09.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth09.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm09});
            this.lblmonth09.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M09")});
            this.lblmonth09.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth09.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth09.Name = "lblmonth09";
            this.lblmonth09.StylePriority.UseBackColor = false;
            this.lblmonth09.StylePriority.UseBorderColor = false;
            this.lblmonth09.StylePriority.UseBorders = false;
            this.lblmonth09.StylePriority.UseFont = false;
            this.lblmonth09.StylePriority.UseForeColor = false;
            this.lblmonth09.StylePriority.UseTextAlignment = false;
            this.lblmonth09.Text = "lblmonth09";
            this.lblmonth09.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth09.Weight = 0.037997795111124492D;
            // 
            // pbm09
            // 
            this.pbm09.ImageUrl = "~\\images\\0.png";
            this.pbm09.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm09.Name = "pbm09";
            this.pbm09.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm09.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm09.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm09_BeforePrint);
            // 
            // lblmonth10
            // 
            this.lblmonth10.BackColor = System.Drawing.Color.White;
            this.lblmonth10.BorderColor = System.Drawing.Color.White;
            this.lblmonth10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm10});
            this.lblmonth10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M10")});
            this.lblmonth10.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth10.Name = "lblmonth10";
            this.lblmonth10.StylePriority.UseBackColor = false;
            this.lblmonth10.StylePriority.UseBorderColor = false;
            this.lblmonth10.StylePriority.UseBorders = false;
            this.lblmonth10.StylePriority.UseFont = false;
            this.lblmonth10.StylePriority.UseForeColor = false;
            this.lblmonth10.StylePriority.UseTextAlignment = false;
            this.lblmonth10.Text = "lblmonth10";
            this.lblmonth10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth10.Weight = 0.037997793600199496D;
            // 
            // pbm10
            // 
            this.pbm10.ImageUrl = "~\\images\\0.png";
            this.pbm10.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm10.Name = "pbm10";
            this.pbm10.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm10.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm10_BeforePrint);
            // 
            // lblmonth11
            // 
            this.lblmonth11.BackColor = System.Drawing.Color.White;
            this.lblmonth11.BorderColor = System.Drawing.Color.White;
            this.lblmonth11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm11});
            this.lblmonth11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M11")});
            this.lblmonth11.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth11.Name = "lblmonth11";
            this.lblmonth11.StylePriority.UseBackColor = false;
            this.lblmonth11.StylePriority.UseBorderColor = false;
            this.lblmonth11.StylePriority.UseBorders = false;
            this.lblmonth11.StylePriority.UseFont = false;
            this.lblmonth11.StylePriority.UseForeColor = false;
            this.lblmonth11.StylePriority.UseTextAlignment = false;
            this.lblmonth11.Text = "lblmonth11";
            this.lblmonth11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth11.Weight = 0.037997795030432983D;
            // 
            // pbm11
            // 
            this.pbm11.ImageUrl = "~\\images\\0.png";
            this.pbm11.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm11.Name = "pbm11";
            this.pbm11.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm11.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm11_BeforePrint);
            // 
            // lblmonth12
            // 
            this.lblmonth12.BackColor = System.Drawing.Color.White;
            this.lblmonth12.BorderColor = System.Drawing.Color.White;
            this.lblmonth12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm12});
            this.lblmonth12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M12")});
            this.lblmonth12.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth12.Name = "lblmonth12";
            this.lblmonth12.StylePriority.UseBackColor = false;
            this.lblmonth12.StylePriority.UseBorderColor = false;
            this.lblmonth12.StylePriority.UseBorders = false;
            this.lblmonth12.StylePriority.UseFont = false;
            this.lblmonth12.StylePriority.UseForeColor = false;
            this.lblmonth12.StylePriority.UseTextAlignment = false;
            this.lblmonth12.Text = "lblmonth12";
            this.lblmonth12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth12.Weight = 0.03799779530965907D;
            // 
            // pbm12
            // 
            this.pbm12.ImageUrl = "~\\images\\0.png";
            this.pbm12.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm12.Name = "pbm12";
            this.pbm12.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm12.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm12_BeforePrint);
            // 
            // lblmonth13
            // 
            this.lblmonth13.BackColor = System.Drawing.Color.White;
            this.lblmonth13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm13});
            this.lblmonth13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M13")});
            this.lblmonth13.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth13.Name = "lblmonth13";
            this.lblmonth13.StylePriority.UseBackColor = false;
            this.lblmonth13.StylePriority.UseBorders = false;
            this.lblmonth13.StylePriority.UseFont = false;
            this.lblmonth13.StylePriority.UseForeColor = false;
            this.lblmonth13.StylePriority.UseTextAlignment = false;
            this.lblmonth13.Text = "lblmonth13";
            this.lblmonth13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth13.Weight = 0.037997794375367148D;
            // 
            // pbm13
            // 
            this.pbm13.ImageUrl = "~\\images\\0.png";
            this.pbm13.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm13.Name = "pbm13";
            this.pbm13.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm13.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm13_BeforePrint);
            // 
            // lblmonth14
            // 
            this.lblmonth14.BackColor = System.Drawing.Color.White;
            this.lblmonth14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm14});
            this.lblmonth14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M14")});
            this.lblmonth14.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth14.Name = "lblmonth14";
            this.lblmonth14.StylePriority.UseBackColor = false;
            this.lblmonth14.StylePriority.UseBorders = false;
            this.lblmonth14.StylePriority.UseFont = false;
            this.lblmonth14.StylePriority.UseForeColor = false;
            this.lblmonth14.StylePriority.UseTextAlignment = false;
            this.lblmonth14.Text = "lblmonth14";
            this.lblmonth14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth14.Weight = 0.037997793379079235D;
            // 
            // pbm14
            // 
            this.pbm14.ImageUrl = "~\\images\\0.png";
            this.pbm14.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm14.Name = "pbm14";
            this.pbm14.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm14.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm14_BeforePrint);
            // 
            // lblmonth15
            // 
            this.lblmonth15.BackColor = System.Drawing.Color.White;
            this.lblmonth15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm15});
            this.lblmonth15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M15")});
            this.lblmonth15.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth15.Name = "lblmonth15";
            this.lblmonth15.StylePriority.UseBackColor = false;
            this.lblmonth15.StylePriority.UseBorders = false;
            this.lblmonth15.StylePriority.UseFont = false;
            this.lblmonth15.StylePriority.UseForeColor = false;
            this.lblmonth15.StylePriority.UseTextAlignment = false;
            this.lblmonth15.Text = "lblmonth15";
            this.lblmonth15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth15.Weight = 0.037997796490361821D;
            // 
            // pbm15
            // 
            this.pbm15.ImageUrl = "~\\images\\0.png";
            this.pbm15.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm15.Name = "pbm15";
            this.pbm15.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm15.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm15_BeforePrint);
            // 
            // lblmonth16
            // 
            this.lblmonth16.BackColor = System.Drawing.Color.White;
            this.lblmonth16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm16});
            this.lblmonth16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M16")});
            this.lblmonth16.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth16.Name = "lblmonth16";
            this.lblmonth16.StylePriority.UseBackColor = false;
            this.lblmonth16.StylePriority.UseBorders = false;
            this.lblmonth16.StylePriority.UseFont = false;
            this.lblmonth16.StylePriority.UseForeColor = false;
            this.lblmonth16.StylePriority.UseTextAlignment = false;
            this.lblmonth16.Text = "lblmonth16";
            this.lblmonth16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth16.Weight = 0.037997794164270376D;
            // 
            // pbm16
            // 
            this.pbm16.ImageUrl = "~\\images\\0.png";
            this.pbm16.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm16.Name = "pbm16";
            this.pbm16.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm16.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm16_BeforePrint);
            // 
            // lblmonth17
            // 
            this.lblmonth17.BackColor = System.Drawing.Color.White;
            this.lblmonth17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm17});
            this.lblmonth17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M17")});
            this.lblmonth17.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth17.Name = "lblmonth17";
            this.lblmonth17.StylePriority.UseBackColor = false;
            this.lblmonth17.StylePriority.UseBorders = false;
            this.lblmonth17.StylePriority.UseFont = false;
            this.lblmonth17.StylePriority.UseForeColor = false;
            this.lblmonth17.StylePriority.UseTextAlignment = false;
            this.lblmonth17.Text = "lblmonth17";
            this.lblmonth17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth17.Weight = 0.037997792920644688D;
            // 
            // pbm17
            // 
            this.pbm17.ImageUrl = "~\\images\\0.png";
            this.pbm17.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm17.Name = "pbm17";
            this.pbm17.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm17.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm17_BeforePrint);
            // 
            // lblmonth18
            // 
            this.lblmonth18.BackColor = System.Drawing.Color.White;
            this.lblmonth18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm18});
            this.lblmonth18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M18")});
            this.lblmonth18.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth18.Name = "lblmonth18";
            this.lblmonth18.StylePriority.UseBackColor = false;
            this.lblmonth18.StylePriority.UseBorders = false;
            this.lblmonth18.StylePriority.UseFont = false;
            this.lblmonth18.StylePriority.UseForeColor = false;
            this.lblmonth18.StylePriority.UseTextAlignment = false;
            this.lblmonth18.Text = "lblmonth18";
            this.lblmonth18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth18.Weight = 0.037997794688394995D;
            // 
            // pbm18
            // 
            this.pbm18.ImageUrl = "~\\images\\0.png";
            this.pbm18.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm18.Name = "pbm18";
            this.pbm18.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm18.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm18_BeforePrint);
            // 
            // lblmonth19
            // 
            this.lblmonth19.BackColor = System.Drawing.Color.White;
            this.lblmonth19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm19});
            this.lblmonth19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M19")});
            this.lblmonth19.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth19.Name = "lblmonth19";
            this.lblmonth19.StylePriority.UseBackColor = false;
            this.lblmonth19.StylePriority.UseBorders = false;
            this.lblmonth19.StylePriority.UseFont = false;
            this.lblmonth19.StylePriority.UseForeColor = false;
            this.lblmonth19.StylePriority.UseTextAlignment = false;
            this.lblmonth19.Text = "lblmonth19";
            this.lblmonth19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth19.Weight = 0.037997793470829669D;
            // 
            // pbm19
            // 
            this.pbm19.ImageUrl = "~\\images\\0.png";
            this.pbm19.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm19.Name = "pbm19";
            this.pbm19.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm19.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm19_BeforePrint);
            // 
            // lblmonth20
            // 
            this.lblmonth20.BackColor = System.Drawing.Color.White;
            this.lblmonth20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm20});
            this.lblmonth20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M20")});
            this.lblmonth20.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth20.Name = "lblmonth20";
            this.lblmonth20.StylePriority.UseBackColor = false;
            this.lblmonth20.StylePriority.UseBorders = false;
            this.lblmonth20.StylePriority.UseFont = false;
            this.lblmonth20.StylePriority.UseForeColor = false;
            this.lblmonth20.StylePriority.UseTextAlignment = false;
            this.lblmonth20.Text = "lblmonth20";
            this.lblmonth20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth20.Weight = 0.037997796423939349D;
            // 
            // pbm20
            // 
            this.pbm20.ImageUrl = "~\\images\\0.png";
            this.pbm20.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm20.Name = "pbm20";
            this.pbm20.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm20.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm20_BeforePrint);
            // 
            // lblmonth21
            // 
            this.lblmonth21.BackColor = System.Drawing.Color.White;
            this.lblmonth21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm21});
            this.lblmonth21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M21")});
            this.lblmonth21.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth21.Name = "lblmonth21";
            this.lblmonth21.StylePriority.UseBackColor = false;
            this.lblmonth21.StylePriority.UseBorders = false;
            this.lblmonth21.StylePriority.UseFont = false;
            this.lblmonth21.StylePriority.UseForeColor = false;
            this.lblmonth21.StylePriority.UseTextAlignment = false;
            this.lblmonth21.Text = "lblmonth21";
            this.lblmonth21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth21.Weight = 0.037997793062359779D;
            // 
            // pbm21
            // 
            this.pbm21.ImageUrl = "~\\images\\0.png";
            this.pbm21.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm21.Name = "pbm21";
            this.pbm21.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm21.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm21_BeforePrint);
            // 
            // lblmonth22
            // 
            this.lblmonth22.BackColor = System.Drawing.Color.White;
            this.lblmonth22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm22});
            this.lblmonth22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M22")});
            this.lblmonth22.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth22.Name = "lblmonth22";
            this.lblmonth22.StylePriority.UseBackColor = false;
            this.lblmonth22.StylePriority.UseBorders = false;
            this.lblmonth22.StylePriority.UseFont = false;
            this.lblmonth22.StylePriority.UseForeColor = false;
            this.lblmonth22.StylePriority.UseTextAlignment = false;
            this.lblmonth22.Text = "lblmonth22";
            this.lblmonth22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth22.Weight = 0.03799779496576907D;
            // 
            // pbm22
            // 
            this.pbm22.ImageUrl = "~\\images\\0.png";
            this.pbm22.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm22.Name = "pbm22";
            this.pbm22.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm22.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm22.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm22_BeforePrint);
            // 
            // lblmonth23
            // 
            this.lblmonth23.BackColor = System.Drawing.Color.White;
            this.lblmonth23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm23});
            this.lblmonth23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M23")});
            this.lblmonth23.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth23.Name = "lblmonth23";
            this.lblmonth23.StylePriority.UseBackColor = false;
            this.lblmonth23.StylePriority.UseBorders = false;
            this.lblmonth23.StylePriority.UseFont = false;
            this.lblmonth23.StylePriority.UseForeColor = false;
            this.lblmonth23.StylePriority.UseTextAlignment = false;
            this.lblmonth23.Text = "lblmonth23";
            this.lblmonth23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth23.Weight = 0.03799779489312078D;
            // 
            // pbm23
            // 
            this.pbm23.ImageUrl = "~\\images\\0.png";
            this.pbm23.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm23.Name = "pbm23";
            this.pbm23.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm23.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm23.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm23_BeforePrint);
            // 
            // lblmonth24
            // 
            this.lblmonth24.BackColor = System.Drawing.Color.White;
            this.lblmonth24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmonth24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbm24});
            this.lblmonth24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M24")});
            this.lblmonth24.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblmonth24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblmonth24.Name = "lblmonth24";
            this.lblmonth24.StylePriority.UseBackColor = false;
            this.lblmonth24.StylePriority.UseBorders = false;
            this.lblmonth24.StylePriority.UseFont = false;
            this.lblmonth24.StylePriority.UseForeColor = false;
            this.lblmonth24.StylePriority.UseTextAlignment = false;
            this.lblmonth24.Text = "lblmonth24";
            this.lblmonth24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblmonth24.Weight = 0.037997784146586076D;
            // 
            // pbm24
            // 
            this.pbm24.ImageUrl = "~\\images\\0.png";
            this.pbm24.LocationFloat = new DevExpress.Utils.PointFloat(1.3F, 0F);
            this.pbm24.Name = "pbm24";
            this.pbm24.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbm24.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbm24.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbm24_BeforePrint);
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMonthlyPaymentHeader});
            this.GroupHeader6.HeightF = 44.79167F;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // tblMonthlyPaymentHeader
            // 
            this.tblMonthlyPaymentHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.tblMonthlyPaymentHeader.BorderColor = System.Drawing.Color.White;
            this.tblMonthlyPaymentHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblMonthlyPaymentHeader.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.tblMonthlyPaymentHeader.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 16F);
            this.tblMonthlyPaymentHeader.Name = "tblMonthlyPaymentHeader";
            this.tblMonthlyPaymentHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblMonthlyPaymentHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.tblMonthlyPaymentHeader.SizeF = new System.Drawing.SizeF(880.0001F, 26.74992F);
            this.tblMonthlyPaymentHeader.StylePriority.UseBackColor = false;
            this.tblMonthlyPaymentHeader.StylePriority.UseBorderColor = false;
            this.tblMonthlyPaymentHeader.StylePriority.UseBorders = false;
            this.tblMonthlyPaymentHeader.StylePriority.UseFont = false;
            this.tblMonthlyPaymentHeader.StylePriority.UsePadding = false;
            this.tblMonthlyPaymentHeader.StylePriority.UseTextAlignment = false;
            this.tblMonthlyPaymentHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.BackColor = System.Drawing.Color.White;
            this.xrTableRow31.BorderColor = System.Drawing.Color.White;
            this.xrTableRow31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell430,
            this.lblM01,
            this.lblM02,
            this.lblM03,
            this.lblM04,
            this.lblM05,
            this.lblM06,
            this.lblM07,
            this.lblM08,
            this.lblM09,
            this.lblM10,
            this.lblM11,
            this.lblM12,
            this.lblM13,
            this.lblM14,
            this.lblM15,
            this.lblM16,
            this.lblM17,
            this.lblM18,
            this.lblM19,
            this.lblM20,
            this.lblM21,
            this.lblM22,
            this.lblM23,
            this.lblM24});
            this.xrTableRow31.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableRow31.StylePriority.UseBackColor = false;
            this.xrTableRow31.StylePriority.UseBorderColor = false;
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.StylePriority.UseFont = false;
            this.xrTableRow31.StylePriority.UseForeColor = false;
            this.xrTableRow31.StylePriority.UsePadding = false;
            this.xrTableRow31.StylePriority.UseTextAlignment = false;
            this.xrTableRow31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow31.Weight = 1.32D;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell430.BorderColor = System.Drawing.Color.White;
            this.xrTableCell430.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell430.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.StylePriority.UseBackColor = false;
            this.xrTableCell430.StylePriority.UseBorderColor = false;
            this.xrTableCell430.StylePriority.UseBorders = false;
            this.xrTableCell430.StylePriority.UseFont = false;
            this.xrTableCell430.Text = "Subscriber Name";
            this.xrTableCell430.Weight = 0.14470597495711846D;
            // 
            // lblM02
            // 
            this.lblM02.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM02.BorderColor = System.Drawing.Color.White;
            this.lblM02.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM02.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M02")});
            this.lblM02.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM02.Name = "lblM02";
            this.lblM02.StylePriority.UseBackColor = false;
            this.lblM02.StylePriority.UseBorderColor = false;
            this.lblM02.StylePriority.UseBorders = false;
            this.lblM02.StylePriority.UseFont = false;
            this.lblM02.StylePriority.UseForeColor = false;
            this.lblM02.StylePriority.UseTextAlignment = false;
            this.lblM02.Text = "lblM02";
            this.lblM02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM02.Weight = 0.030594379796802976D;
            // 
            // lblM03
            // 
            this.lblM03.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM03.BorderColor = System.Drawing.Color.White;
            this.lblM03.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM03.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M03")});
            this.lblM03.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM03.Name = "lblM03";
            this.lblM03.StylePriority.UseBackColor = false;
            this.lblM03.StylePriority.UseBorderColor = false;
            this.lblM03.StylePriority.UseBorders = false;
            this.lblM03.StylePriority.UseFont = false;
            this.lblM03.StylePriority.UseForeColor = false;
            this.lblM03.StylePriority.UseTextAlignment = false;
            this.lblM03.Text = "lblM03";
            this.lblM03.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM03.Weight = 0.03059437862403841D;
            // 
            // lblM04
            // 
            this.lblM04.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM04.BorderColor = System.Drawing.Color.White;
            this.lblM04.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM04.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M04")});
            this.lblM04.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM04.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM04.Name = "lblM04";
            this.lblM04.StylePriority.UseBackColor = false;
            this.lblM04.StylePriority.UseBorderColor = false;
            this.lblM04.StylePriority.UseBorders = false;
            this.lblM04.StylePriority.UseFont = false;
            this.lblM04.StylePriority.UseForeColor = false;
            this.lblM04.StylePriority.UseTextAlignment = false;
            this.lblM04.Text = "lblM04";
            this.lblM04.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM04.Weight = 0.030594372734078057D;
            // 
            // lblM05
            // 
            this.lblM05.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM05.BorderColor = System.Drawing.Color.White;
            this.lblM05.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM05.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M05")});
            this.lblM05.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM05.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM05.Name = "lblM05";
            this.lblM05.StylePriority.UseBackColor = false;
            this.lblM05.StylePriority.UseBorderColor = false;
            this.lblM05.StylePriority.UseBorders = false;
            this.lblM05.StylePriority.UseFont = false;
            this.lblM05.StylePriority.UseForeColor = false;
            this.lblM05.StylePriority.UseTextAlignment = false;
            this.lblM05.Text = "lblM05";
            this.lblM05.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM05.Weight = 0.030594378510270151D;
            // 
            // lblM06
            // 
            this.lblM06.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM06.BorderColor = System.Drawing.Color.White;
            this.lblM06.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM06.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M06")});
            this.lblM06.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM06.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM06.Name = "lblM06";
            this.lblM06.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
            this.lblM06.StylePriority.UseBackColor = false;
            this.lblM06.StylePriority.UseBorderColor = false;
            this.lblM06.StylePriority.UseBorders = false;
            this.lblM06.StylePriority.UseFont = false;
            this.lblM06.StylePriority.UseForeColor = false;
            this.lblM06.StylePriority.UsePadding = false;
            this.lblM06.StylePriority.UseTextAlignment = false;
            this.lblM06.Text = "lblM06";
            this.lblM06.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM06.Weight = 0.03059437998193186D;
            // 
            // lblM07
            // 
            this.lblM07.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM07.BorderColor = System.Drawing.Color.White;
            this.lblM07.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM07.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M07")});
            this.lblM07.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM07.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM07.Name = "lblM07";
            this.lblM07.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
            this.lblM07.StylePriority.UseBackColor = false;
            this.lblM07.StylePriority.UseBorderColor = false;
            this.lblM07.StylePriority.UseBorders = false;
            this.lblM07.StylePriority.UseFont = false;
            this.lblM07.StylePriority.UseForeColor = false;
            this.lblM07.StylePriority.UsePadding = false;
            this.lblM07.StylePriority.UseTextAlignment = false;
            this.lblM07.Text = "lblM07";
            this.lblM07.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM07.Weight = 0.03059437398187817D;
            // 
            // lblM08
            // 
            this.lblM08.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM08.BorderColor = System.Drawing.Color.White;
            this.lblM08.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM08.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M08")});
            this.lblM08.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM08.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM08.Name = "lblM08";
            this.lblM08.StylePriority.UseBackColor = false;
            this.lblM08.StylePriority.UseBorderColor = false;
            this.lblM08.StylePriority.UseBorders = false;
            this.lblM08.StylePriority.UseFont = false;
            this.lblM08.StylePriority.UseForeColor = false;
            this.lblM08.StylePriority.UseTextAlignment = false;
            this.lblM08.Text = "lblM08";
            this.lblM08.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM08.Weight = 0.030594376232330425D;
            // 
            // lblM09
            // 
            this.lblM09.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM09.BorderColor = System.Drawing.Color.White;
            this.lblM09.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM09.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M09")});
            this.lblM09.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM09.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM09.Name = "lblM09";
            this.lblM09.StylePriority.UseBackColor = false;
            this.lblM09.StylePriority.UseBorderColor = false;
            this.lblM09.StylePriority.UseBorders = false;
            this.lblM09.StylePriority.UseFont = false;
            this.lblM09.StylePriority.UseForeColor = false;
            this.lblM09.StylePriority.UseTextAlignment = false;
            this.lblM09.Text = "lblM09";
            this.lblM09.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM09.Weight = 0.030594375869835891D;
            // 
            // lblM10
            // 
            this.lblM10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM10.BorderColor = System.Drawing.Color.White;
            this.lblM10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M10")});
            this.lblM10.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM10.Name = "lblM10";
            this.lblM10.StylePriority.UseBackColor = false;
            this.lblM10.StylePriority.UseBorderColor = false;
            this.lblM10.StylePriority.UseBorders = false;
            this.lblM10.StylePriority.UseFont = false;
            this.lblM10.StylePriority.UseForeColor = false;
            this.lblM10.StylePriority.UseTextAlignment = false;
            this.lblM10.Text = "lblM10";
            this.lblM10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM10.Weight = 0.03059437646766304D;
            // 
            // lblM11
            // 
            this.lblM11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM11.BorderColor = System.Drawing.Color.White;
            this.lblM11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M11")});
            this.lblM11.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM11.Name = "lblM11";
            this.lblM11.StylePriority.UseBackColor = false;
            this.lblM11.StylePriority.UseBorderColor = false;
            this.lblM11.StylePriority.UseBorders = false;
            this.lblM11.StylePriority.UseFont = false;
            this.lblM11.StylePriority.UseForeColor = false;
            this.lblM11.StylePriority.UseTextAlignment = false;
            this.lblM11.Text = "lblM11";
            this.lblM11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM11.Weight = 0.030594379247710635D;
            // 
            // lblM12
            // 
            this.lblM12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM12.BorderColor = System.Drawing.Color.White;
            this.lblM12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M12")});
            this.lblM12.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblM12.Name = "lblM12";
            this.lblM12.StylePriority.UseBackColor = false;
            this.lblM12.StylePriority.UseBorderColor = false;
            this.lblM12.StylePriority.UseBorders = false;
            this.lblM12.StylePriority.UseFont = false;
            this.lblM12.StylePriority.UseForeColor = false;
            this.lblM12.StylePriority.UseTextAlignment = false;
            this.lblM12.Text = "lblM12";
            this.lblM12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM12.Weight = 0.030594380318512568D;
            // 
            // lblM13
            // 
            this.lblM13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M13")});
            this.lblM13.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM13.Name = "lblM13";
            this.lblM13.StylePriority.UseBackColor = false;
            this.lblM13.StylePriority.UseBorderColor = false;
            this.lblM13.StylePriority.UseFont = false;
            this.lblM13.StylePriority.UseTextAlignment = false;
            this.lblM13.Text = "lblM13";
            this.lblM13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM13.Weight = 0.030594379517813269D;
            // 
            // lblM14
            // 
            this.lblM14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M14")});
            this.lblM14.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM14.Name = "lblM14";
            this.lblM14.StylePriority.UseBackColor = false;
            this.lblM14.StylePriority.UseBorderColor = false;
            this.lblM14.StylePriority.UseFont = false;
            this.lblM14.StylePriority.UseTextAlignment = false;
            this.lblM14.Text = "lblM14";
            this.lblM14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM14.Weight = 0.030594378403082309D;
            // 
            // lblM15
            // 
            this.lblM15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M15")});
            this.lblM15.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM15.Name = "lblM15";
            this.lblM15.StylePriority.UseBackColor = false;
            this.lblM15.StylePriority.UseBorderColor = false;
            this.lblM15.StylePriority.UseFont = false;
            this.lblM15.StylePriority.UseTextAlignment = false;
            this.lblM15.Text = "lblM15";
            this.lblM15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM15.Weight = 0.030594378048186932D;
            // 
            // lblM16
            // 
            this.lblM16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M16")});
            this.lblM16.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM16.Name = "lblM16";
            this.lblM16.StylePriority.UseBackColor = false;
            this.lblM16.StylePriority.UseBorderColor = false;
            this.lblM16.StylePriority.UseFont = false;
            this.lblM16.StylePriority.UseTextAlignment = false;
            this.lblM16.Text = "lblM16";
            this.lblM16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM16.Weight = 0.030594378006007034D;
            // 
            // lblM17
            // 
            this.lblM17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M17")});
            this.lblM17.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM17.Name = "lblM17";
            this.lblM17.StylePriority.UseBackColor = false;
            this.lblM17.StylePriority.UseBorderColor = false;
            this.lblM17.StylePriority.UseFont = false;
            this.lblM17.StylePriority.UseTextAlignment = false;
            this.lblM17.Text = "lblM17";
            this.lblM17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM17.Weight = 0.030594379181032069D;
            // 
            // lblM18
            // 
            this.lblM18.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M18")});
            this.lblM18.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM18.Name = "lblM18";
            this.lblM18.StylePriority.UseBackColor = false;
            this.lblM18.StylePriority.UseBorderColor = false;
            this.lblM18.StylePriority.UseFont = false;
            this.lblM18.StylePriority.UseTextAlignment = false;
            this.lblM18.Text = "lblM18";
            this.lblM18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM18.Weight = 0.030594372711482493D;
            // 
            // lblM19
            // 
            this.lblM19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M19")});
            this.lblM19.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM19.Name = "lblM19";
            this.lblM19.StylePriority.UseBackColor = false;
            this.lblM19.StylePriority.UseBorderColor = false;
            this.lblM19.StylePriority.UseFont = false;
            this.lblM19.StylePriority.UseTextAlignment = false;
            this.lblM19.Text = "lblM19";
            this.lblM19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM19.Weight = 0.030594374734036464D;
            // 
            // lblM20
            // 
            this.lblM20.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M20")});
            this.lblM20.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM20.Name = "lblM20";
            this.lblM20.StylePriority.UseBackColor = false;
            this.lblM20.StylePriority.UseBorderColor = false;
            this.lblM20.StylePriority.UseFont = false;
            this.lblM20.StylePriority.UseTextAlignment = false;
            this.lblM20.Text = "lblM20";
            this.lblM20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM20.Weight = 0.030594373656016013D;
            // 
            // lblM21
            // 
            this.lblM21.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M21")});
            this.lblM21.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM21.Name = "lblM21";
            this.lblM21.StylePriority.UseBackColor = false;
            this.lblM21.StylePriority.UseBorderColor = false;
            this.lblM21.StylePriority.UseFont = false;
            this.lblM21.StylePriority.UseTextAlignment = false;
            this.lblM21.Text = "lblM21";
            this.lblM21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM21.Weight = 0.03059437560556403D;
            // 
            // lblM22
            // 
            this.lblM22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M22")});
            this.lblM22.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM22.Name = "lblM22";
            this.lblM22.StylePriority.UseBackColor = false;
            this.lblM22.StylePriority.UseBorderColor = false;
            this.lblM22.StylePriority.UseFont = false;
            this.lblM22.StylePriority.UseTextAlignment = false;
            this.lblM22.Text = "lblM22";
            this.lblM22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM22.Weight = 0.030594376620949135D;
            // 
            // lblM23
            // 
            this.lblM23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M23")});
            this.lblM23.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM23.Name = "lblM23";
            this.lblM23.StylePriority.UseBackColor = false;
            this.lblM23.StylePriority.UseBorderColor = false;
            this.lblM23.StylePriority.UseFont = false;
            this.lblM23.StylePriority.UseTextAlignment = false;
            this.lblM23.Text = "lblM23";
            this.lblM23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM23.Weight = 0.030594375828907821D;
            // 
            // lblM24
            // 
            this.lblM24.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblM24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M24")});
            this.lblM24.Font = new System.Drawing.Font("Trebuchet MS", 7.5F);
            this.lblM24.Name = "lblM24";
            this.lblM24.StylePriority.UseBackColor = false;
            this.lblM24.StylePriority.UseBorderColor = false;
            this.lblM24.StylePriority.UseFont = false;
            this.lblM24.StylePriority.UseTextAlignment = false;
            this.lblM24.Text = "lblM24";
            this.lblM24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM24.Weight = 0.030594396596588514D;
            // 
            // AdverseDomainRecords
            // 
            this.AdverseDomainRecords.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7,
            this.Adverse,
            this.Judgment,
            this.DetailReport8});
            this.AdverseDomainRecords.Level = 7;
            this.AdverseDomainRecords.Name = "AdverseDomainRecords";
            // 
            // Detail7
            // 
            this.Detail7.Expanded = false;
            this.Detail7.HeightF = 22.70839F;
            this.Detail7.Name = "Detail7";
            this.Detail7.Visible = false;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable26});
            this.GroupHeader7.HeightF = 68.75F;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable26.ForeColor = System.Drawing.Color.Gray;
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow50,
            this.xrTableRow51});
            this.xrTable26.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable26.StylePriority.UseBorders = false;
            this.xrTable26.StylePriority.UseFont = false;
            this.xrTable26.StylePriority.UseForeColor = false;
            this.xrTable26.StylePriority.UsePadding = false;
            this.xrTable26.StylePriority.UseTextAlignment = false;
            this.xrTable26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.8D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.BackColor = System.Drawing.Color.White;
            this.xrTableCell117.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell117.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell117.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell117.StylePriority.UseBackColor = false;
            this.xrTableCell117.StylePriority.UseBorderColor = false;
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.StylePriority.UseFont = false;
            this.xrTableCell117.StylePriority.UseForeColor = false;
            this.xrTableCell117.StylePriority.UsePadding = false;
            this.xrTableCell117.Text = "Adverse Records";
            this.xrTableCell117.Weight = 3D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell118});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.53333333333333344D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.BackColor = System.Drawing.Color.White;
            this.xrTableCell118.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell118.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell118.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell118.Multiline = true;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell118.StylePriority.UseBackColor = false;
            this.xrTableCell118.StylePriority.UseBorderColor = false;
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.StylePriority.UseFont = false;
            this.xrTableCell118.StylePriority.UseForeColor = false;
            this.xrTableCell118.StylePriority.UsePadding = false;
            this.xrTableCell118.Text = "This section details all Legal enforcement action taken on you by the your Credit" +
    " or Service Providers.";
            this.xrTableCell118.Weight = 3D;
            // 
            // Adverse
            // 
            this.Adverse.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAdverseInfo,
            this.GroupHeaderAdverse});
            this.Adverse.DataMember = "ConsumerAdverseInfo";
            this.Adverse.Level = 0;
            this.Adverse.Name = "Adverse";
            // 
            // DetailAdverseInfo
            // 
            this.DetailAdverseInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable28});
            this.DetailAdverseInfo.HeightF = 22.70839F;
            this.DetailAdverseInfo.Name = "DetailAdverseInfo";
            // 
            // xrTable28
            // 
            this.xrTable28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable28.ForeColor = System.Drawing.Color.Gray;
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
            this.xrTable28.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable28.StylePriority.UseBackColor = false;
            this.xrTable28.StylePriority.UseBorders = false;
            this.xrTable28.StylePriority.UseFont = false;
            this.xrTable28.StylePriority.UseForeColor = false;
            this.xrTable28.StylePriority.UseTextAlignment = false;
            this.xrTable28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128,
            this.lblDAmount,
            this.xrTableCell131,
            this.xrTableCell132});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 0.400000244140774D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.BackColor = System.Drawing.Color.White;
            this.xrTableCell126.BorderColor = System.Drawing.Color.White;
            this.xrTableCell126.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.SubscriberName")});
            this.xrTableCell126.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell126.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBackColor = false;
            this.xrTableCell126.StylePriority.UseBorderColor = false;
            this.xrTableCell126.StylePriority.UseFont = false;
            this.xrTableCell126.StylePriority.UseForeColor = false;
            this.xrTableCell126.Text = "xrTableCell126";
            this.xrTableCell126.Weight = 0.3901962326505819D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.BackColor = System.Drawing.Color.White;
            this.xrTableCell127.BorderColor = System.Drawing.Color.White;
            this.xrTableCell127.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.AccountNo")});
            this.xrTableCell127.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell127.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBackColor = false;
            this.xrTableCell127.StylePriority.UseBorderColor = false;
            this.xrTableCell127.StylePriority.UseFont = false;
            this.xrTableCell127.StylePriority.UseForeColor = false;
            this.xrTableCell127.Text = "xrTableCell127";
            this.xrTableCell127.Weight = 0.41798348169293664D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.BackColor = System.Drawing.Color.White;
            this.xrTableCell128.BorderColor = System.Drawing.Color.White;
            this.xrTableCell128.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.ActionDate")});
            this.xrTableCell128.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell128.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell128.Multiline = true;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBackColor = false;
            this.xrTableCell128.StylePriority.UseBorderColor = false;
            this.xrTableCell128.StylePriority.UseFont = false;
            this.xrTableCell128.StylePriority.UseForeColor = false;
            this.xrTableCell128.StylePriority.UseTextAlignment = false;
            this.xrTableCell128.Text = "xrTableCell128";
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell128.Weight = 0.52276708964208407D;
            // 
            // lblDAmount
            // 
            this.lblDAmount.BackColor = System.Drawing.Color.White;
            this.lblDAmount.BorderColor = System.Drawing.Color.White;
            this.lblDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.CurrentBalanceAmt")});
            this.lblDAmount.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblDAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDAmount.Name = "lblDAmount";
            this.lblDAmount.StylePriority.UseBackColor = false;
            this.lblDAmount.StylePriority.UseBorderColor = false;
            this.lblDAmount.StylePriority.UseFont = false;
            this.lblDAmount.StylePriority.UseForeColor = false;
            this.lblDAmount.Text = "lblDAmount";
            this.lblDAmount.Weight = 0.47035109773844952D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.BackColor = System.Drawing.Color.White;
            this.xrTableCell131.BorderColor = System.Drawing.Color.White;
            this.xrTableCell131.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.DataStatus")});
            this.xrTableCell131.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell131.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell131.Multiline = true;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBackColor = false;
            this.xrTableCell131.StylePriority.UseBorderColor = false;
            this.xrTableCell131.StylePriority.UseFont = false;
            this.xrTableCell131.StylePriority.UseForeColor = false;
            this.xrTableCell131.Text = "xrTableCell131";
            this.xrTableCell131.Weight = 0.57518360709396343D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.BackColor = System.Drawing.Color.White;
            this.xrTableCell132.BorderColor = System.Drawing.Color.White;
            this.xrTableCell132.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.Comments")});
            this.xrTableCell132.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell132.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell132.Multiline = true;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBackColor = false;
            this.xrTableCell132.StylePriority.UseBorderColor = false;
            this.xrTableCell132.StylePriority.UseFont = false;
            this.xrTableCell132.StylePriority.UseForeColor = false;
            this.xrTableCell132.Text = "xrTableCell132";
            this.xrTableCell132.Weight = 0.62732071427441394D;
            // 
            // GroupHeaderAdverse
            // 
            this.GroupHeaderAdverse.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable27});
            this.GroupHeaderAdverse.HeightF = 54.79177F;
            this.GroupHeaderAdverse.Name = "GroupHeaderAdverse";
            // 
            // xrTable27
            // 
            this.xrTable27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable27.ForeColor = System.Drawing.Color.Gray;
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54,
            this.xrTableRow53});
            this.xrTable27.SizeF = new System.Drawing.SizeF(880F, 54.79177F);
            this.xrTable27.StylePriority.UseBackColor = false;
            this.xrTable27.StylePriority.UseBorders = false;
            this.xrTable27.StylePriority.UseFont = false;
            this.xrTable27.StylePriority.UseForeColor = false;
            this.xrTable27.StylePriority.UseTextAlignment = false;
            this.xrTable27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 0.400000244140774D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.BackColor = System.Drawing.Color.White;
            this.xrTableCell130.BorderColor = System.Drawing.Color.White;
            this.xrTableCell130.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell130.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell130.Multiline = true;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBackColor = false;
            this.xrTableCell130.StylePriority.UseBorderColor = false;
            this.xrTableCell130.StylePriority.UseFont = false;
            this.xrTableCell130.StylePriority.UseForeColor = false;
            this.xrTableCell130.StylePriority.UseTextAlignment = false;
            this.xrTableCell130.Text = "Adverse/Defaults\r\n";
            this.xrTableCell130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell130.Weight = 3.0038022230924293D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 0.400000244140774D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell119.BorderColor = System.Drawing.Color.White;
            this.xrTableCell119.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell119.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBackColor = false;
            this.xrTableCell119.StylePriority.UseBorderColor = false;
            this.xrTableCell119.StylePriority.UseFont = false;
            this.xrTableCell119.StylePriority.UseForeColor = false;
            this.xrTableCell119.Text = "Subscriber";
            this.xrTableCell119.Weight = 0.3901962326505819D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell120.BorderColor = System.Drawing.Color.White;
            this.xrTableCell120.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell120.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBackColor = false;
            this.xrTableCell120.StylePriority.UseBorderColor = false;
            this.xrTableCell120.StylePriority.UseFont = false;
            this.xrTableCell120.StylePriority.UseForeColor = false;
            this.xrTableCell120.Text = "Account No.";
            this.xrTableCell120.Weight = 0.41798348169293664D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell121.BorderColor = System.Drawing.Color.White;
            this.xrTableCell121.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell121.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell121.Multiline = true;
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBackColor = false;
            this.xrTableCell121.StylePriority.UseBorderColor = false;
            this.xrTableCell121.StylePriority.UseFont = false;
            this.xrTableCell121.StylePriority.UseForeColor = false;
            this.xrTableCell121.StylePriority.UseTextAlignment = false;
            this.xrTableCell121.Text = "Action date";
            this.xrTableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell121.Weight = 0.52276708964208407D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell122.BorderColor = System.Drawing.Color.White;
            this.xrTableCell122.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell122.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBackColor = false;
            this.xrTableCell122.StylePriority.UseBorderColor = false;
            this.xrTableCell122.StylePriority.UseFont = false;
            this.xrTableCell122.StylePriority.UseForeColor = false;
            this.xrTableCell122.Text = "Amount";
            this.xrTableCell122.Weight = 0.47035109773844952D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell123.BorderColor = System.Drawing.Color.White;
            this.xrTableCell123.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell123.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell123.Multiline = true;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBackColor = false;
            this.xrTableCell123.StylePriority.UseBorderColor = false;
            this.xrTableCell123.StylePriority.UseFont = false;
            this.xrTableCell123.StylePriority.UseForeColor = false;
            this.xrTableCell123.Text = "Account Status";
            this.xrTableCell123.Weight = 0.57518360709396343D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell124.BorderColor = System.Drawing.Color.White;
            this.xrTableCell124.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell124.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell124.Multiline = true;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBackColor = false;
            this.xrTableCell124.StylePriority.UseBorderColor = false;
            this.xrTableCell124.StylePriority.UseFont = false;
            this.xrTableCell124.StylePriority.UseForeColor = false;
            this.xrTableCell124.Text = "Comment";
            this.xrTableCell124.Weight = 0.62732071427441394D;
            // 
            // Judgment
            // 
            this.Judgment.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailJudgment,
            this.GroupHeaderJudgment});
            this.Judgment.DataMember = "ConsumerJudgement";
            this.Judgment.Level = 2;
            this.Judgment.Name = "Judgment";
            // 
            // DetailJudgment
            // 
            this.DetailJudgment.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable57});
            this.DetailJudgment.HeightF = 22.70839F;
            this.DetailJudgment.Name = "DetailJudgment";
            // 
            // xrTable57
            // 
            this.xrTable57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable57.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable57.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable57.ForeColor = System.Drawing.Color.Gray;
            this.xrTable57.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable57.Name = "xrTable57";
            this.xrTable57.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable57.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable57.StylePriority.UseBackColor = false;
            this.xrTable57.StylePriority.UseBorders = false;
            this.xrTable57.StylePriority.UseFont = false;
            this.xrTable57.StylePriority.UseForeColor = false;
            this.xrTable57.StylePriority.UseTextAlignment = false;
            this.xrTable57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.lblJDAmount,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell286,
            this.xrTableCell287});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.400000244140774D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.BackColor = System.Drawing.Color.White;
            this.xrTableCell34.BorderColor = System.Drawing.Color.White;
            this.xrTableCell34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseNumber")});
            this.xrTableCell34.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBackColor = false;
            this.xrTableCell34.StylePriority.UseBorderColor = false;
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseForeColor = false;
            this.xrTableCell34.Text = "xrTableCell34";
            this.xrTableCell34.Weight = 0.27286026039556177D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.BackColor = System.Drawing.Color.White;
            this.xrTableCell35.BorderColor = System.Drawing.Color.White;
            this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseFilingDate")});
            this.xrTableCell35.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBackColor = false;
            this.xrTableCell35.StylePriority.UseBorderColor = false;
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseForeColor = false;
            this.xrTableCell35.Text = "xrTableCell35";
            this.xrTableCell35.Weight = 0.326212470749496D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.BackColor = System.Drawing.Color.White;
            this.xrTableCell36.BorderColor = System.Drawing.Color.White;
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseType")});
            this.xrTableCell36.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBackColor = false;
            this.xrTableCell36.StylePriority.UseBorderColor = false;
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseForeColor = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell36.Weight = 0.34224727248367504D;
            // 
            // lblJDAmount
            // 
            this.lblJDAmount.BackColor = System.Drawing.Color.White;
            this.lblJDAmount.BorderColor = System.Drawing.Color.White;
            this.lblJDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.DisputeAmt")});
            this.lblJDAmount.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblJDAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblJDAmount.Name = "lblJDAmount";
            this.lblJDAmount.StylePriority.UseBackColor = false;
            this.lblJDAmount.StylePriority.UseBorderColor = false;
            this.lblJDAmount.StylePriority.UseFont = false;
            this.lblJDAmount.StylePriority.UseForeColor = false;
            this.lblJDAmount.Text = "lblJDAmount";
            this.lblJDAmount.Weight = 0.38962705889832266D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.BackColor = System.Drawing.Color.White;
            this.xrTableCell46.BorderColor = System.Drawing.Color.White;
            this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.PlaintiffName")});
            this.xrTableCell46.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell46.Multiline = true;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBackColor = false;
            this.xrTableCell46.StylePriority.UseBorderColor = false;
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseForeColor = false;
            this.xrTableCell46.Text = "xrTableCell46";
            this.xrTableCell46.Weight = 0.41089098500973215D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.BackColor = System.Drawing.Color.White;
            this.xrTableCell47.BorderColor = System.Drawing.Color.White;
            this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CourtName")});
            this.xrTableCell47.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell47.Multiline = true;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBackColor = false;
            this.xrTableCell47.StylePriority.UseBorderColor = false;
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseForeColor = false;
            this.xrTableCell47.Text = "xrTableCell47";
            this.xrTableCell47.Weight = 0.37079453204633855D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.BackColor = System.Drawing.Color.White;
            this.xrTableCell48.BorderColor = System.Drawing.Color.White;
            this.xrTableCell48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.AttorneyName")});
            this.xrTableCell48.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBackColor = false;
            this.xrTableCell48.StylePriority.UseBorderColor = false;
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UseForeColor = false;
            this.xrTableCell48.Text = "xrTableCell48";
            this.xrTableCell48.Weight = 0.34981739103157627D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.BackColor = System.Drawing.Color.White;
            this.xrTableCell286.BorderColor = System.Drawing.Color.White;
            this.xrTableCell286.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.TelephoneNo")});
            this.xrTableCell286.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell286.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBackColor = false;
            this.xrTableCell286.StylePriority.UseBorderColor = false;
            this.xrTableCell286.StylePriority.UseFont = false;
            this.xrTableCell286.StylePriority.UseForeColor = false;
            this.xrTableCell286.Text = "xrTableCell286";
            this.xrTableCell286.Weight = 0.2824662664277991D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.BackColor = System.Drawing.Color.White;
            this.xrTableCell287.BorderColor = System.Drawing.Color.White;
            this.xrTableCell287.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.Comments")});
            this.xrTableCell287.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell287.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBackColor = false;
            this.xrTableCell287.StylePriority.UseBorderColor = false;
            this.xrTableCell287.StylePriority.UseFont = false;
            this.xrTableCell287.StylePriority.UseForeColor = false;
            this.xrTableCell287.Text = "xrTableCell287";
            this.xrTableCell287.Weight = 0.25888598604992807D;
            // 
            // GroupHeaderJudgment
            // 
            this.GroupHeaderJudgment.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable29});
            this.GroupHeaderJudgment.HeightF = 63.75011F;
            this.GroupHeaderJudgment.Name = "GroupHeaderJudgment";
            // 
            // xrTable29
            // 
            this.xrTable29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable29.ForeColor = System.Drawing.Color.Gray;
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10.41667F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55,
            this.xrTableRow57});
            this.xrTable29.SizeF = new System.Drawing.SizeF(880F, 53.33344F);
            this.xrTable29.StylePriority.UseBackColor = false;
            this.xrTable29.StylePriority.UseBorders = false;
            this.xrTable29.StylePriority.UseFont = false;
            this.xrTable29.StylePriority.UseForeColor = false;
            this.xrTable29.StylePriority.UseTextAlignment = false;
            this.xrTable29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 0.400000244140774D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.BackColor = System.Drawing.Color.White;
            this.xrTableCell125.BorderColor = System.Drawing.Color.White;
            this.xrTableCell125.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell125.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell125.Multiline = true;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseBackColor = false;
            this.xrTableCell125.StylePriority.UseBorderColor = false;
            this.xrTableCell125.StylePriority.UseFont = false;
            this.xrTableCell125.StylePriority.UseForeColor = false;
            this.xrTableCell125.StylePriority.UseTextAlignment = false;
            this.xrTableCell125.Text = "Court Notice\'s Judgments, Administration Orders, Provision Sequestration, Final S" +
    "equestration, Rehabilitation Order, Debt Review Order\r\n";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell125.Weight = 3.0038022230924293D;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell143});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 0.400000244140774D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell133.BorderColor = System.Drawing.Color.White;
            this.xrTableCell133.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell133.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBackColor = false;
            this.xrTableCell133.StylePriority.UseBorderColor = false;
            this.xrTableCell133.StylePriority.UseFont = false;
            this.xrTableCell133.StylePriority.UseForeColor = false;
            this.xrTableCell133.Text = "Case No.";
            this.xrTableCell133.Weight = 0.27286026039556177D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell134.BorderColor = System.Drawing.Color.White;
            this.xrTableCell134.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell134.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBackColor = false;
            this.xrTableCell134.StylePriority.UseBorderColor = false;
            this.xrTableCell134.StylePriority.UseFont = false;
            this.xrTableCell134.StylePriority.UseForeColor = false;
            this.xrTableCell134.Text = "Issue Date";
            this.xrTableCell134.Weight = 0.326212470749496D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell135.BorderColor = System.Drawing.Color.White;
            this.xrTableCell135.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell135.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell135.Multiline = true;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBackColor = false;
            this.xrTableCell135.StylePriority.UseBorderColor = false;
            this.xrTableCell135.StylePriority.UseFont = false;
            this.xrTableCell135.StylePriority.UseForeColor = false;
            this.xrTableCell135.StylePriority.UseTextAlignment = false;
            this.xrTableCell135.Text = "Judgement Type";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell135.Weight = 0.34224727248367504D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell136.BorderColor = System.Drawing.Color.White;
            this.xrTableCell136.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell136.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBackColor = false;
            this.xrTableCell136.StylePriority.UseBorderColor = false;
            this.xrTableCell136.StylePriority.UseFont = false;
            this.xrTableCell136.StylePriority.UseForeColor = false;
            this.xrTableCell136.Text = "Amount";
            this.xrTableCell136.Weight = 0.38962705889832266D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell137.BorderColor = System.Drawing.Color.White;
            this.xrTableCell137.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell137.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell137.Multiline = true;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBackColor = false;
            this.xrTableCell137.StylePriority.UseBorderColor = false;
            this.xrTableCell137.StylePriority.UseFont = false;
            this.xrTableCell137.StylePriority.UseForeColor = false;
            this.xrTableCell137.Text = "Plaintiff";
            this.xrTableCell137.Weight = 0.41089098500973215D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell138.BorderColor = System.Drawing.Color.White;
            this.xrTableCell138.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell138.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell138.Multiline = true;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBackColor = false;
            this.xrTableCell138.StylePriority.UseBorderColor = false;
            this.xrTableCell138.StylePriority.UseFont = false;
            this.xrTableCell138.StylePriority.UseForeColor = false;
            this.xrTableCell138.Text = "Court";
            this.xrTableCell138.Weight = 0.37079453204633855D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell140.BorderColor = System.Drawing.Color.White;
            this.xrTableCell140.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell140.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBackColor = false;
            this.xrTableCell140.StylePriority.UseBorderColor = false;
            this.xrTableCell140.StylePriority.UseFont = false;
            this.xrTableCell140.StylePriority.UseForeColor = false;
            this.xrTableCell140.Text = "Attorney";
            this.xrTableCell140.Weight = 0.34981739103157627D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell141.BorderColor = System.Drawing.Color.White;
            this.xrTableCell141.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell141.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBackColor = false;
            this.xrTableCell141.StylePriority.UseBorderColor = false;
            this.xrTableCell141.StylePriority.UseFont = false;
            this.xrTableCell141.StylePriority.UseForeColor = false;
            this.xrTableCell141.Text = "Phone No";
            this.xrTableCell141.Weight = 0.2824662664277991D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell143.BorderColor = System.Drawing.Color.White;
            this.xrTableCell143.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell143.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBackColor = false;
            this.xrTableCell143.StylePriority.UseBorderColor = false;
            this.xrTableCell143.StylePriority.UseFont = false;
            this.xrTableCell143.StylePriority.UseForeColor = false;
            this.xrTableCell143.Text = "Comment";
            this.xrTableCell143.Weight = 0.25888598604992807D;
            // 
            // DetailReport8
            // 
            this.DetailReport8.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail10,
            this.GroupHeader10});
            this.DetailReport8.DataMember = "ConsumerDefaultAlert";
            this.DetailReport8.Level = 1;
            this.DetailReport8.Name = "DetailReport8";
            // 
            // Detail10
            // 
            this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable69});
            this.Detail10.HeightF = 22.70839F;
            this.Detail10.Name = "Detail10";
            // 
            // xrTable69
            // 
            this.xrTable69.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable69.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable69.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable69.ForeColor = System.Drawing.Color.Gray;
            this.xrTable69.LocationFloat = new DevExpress.Utils.PointFloat(10F, 0F);
            this.xrTable69.Name = "xrTable69";
            this.xrTable69.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow143});
            this.xrTable69.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable69.StylePriority.UseBackColor = false;
            this.xrTable69.StylePriority.UseBorders = false;
            this.xrTable69.StylePriority.UseFont = false;
            this.xrTable69.StylePriority.UseForeColor = false;
            this.xrTable69.StylePriority.UseTextAlignment = false;
            this.xrTable69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow143
            // 
            this.xrTableRow143.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell380,
            this.xrTableCell386,
            this.xrTableCell387,
            this.lblDAAmountD,
            this.xrTableCell389});
            this.xrTableRow143.Name = "xrTableRow143";
            this.xrTableRow143.Weight = 0.400000244140774D;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.BackColor = System.Drawing.Color.White;
            this.xrTableCell380.BorderColor = System.Drawing.Color.White;
            this.xrTableCell380.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Company")});
            this.xrTableCell380.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell380.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.StylePriority.UseBackColor = false;
            this.xrTableCell380.StylePriority.UseBorderColor = false;
            this.xrTableCell380.StylePriority.UseFont = false;
            this.xrTableCell380.StylePriority.UseForeColor = false;
            this.xrTableCell380.Text = "xrTableCell380";
            this.xrTableCell380.Weight = 0.45775330285033344D;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.BackColor = System.Drawing.Color.White;
            this.xrTableCell386.BorderColor = System.Drawing.Color.White;
            this.xrTableCell386.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.AccountNo")});
            this.xrTableCell386.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell386.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.StylePriority.UseBackColor = false;
            this.xrTableCell386.StylePriority.UseBorderColor = false;
            this.xrTableCell386.StylePriority.UseFont = false;
            this.xrTableCell386.StylePriority.UseForeColor = false;
            this.xrTableCell386.Text = "xrTableCell386";
            this.xrTableCell386.Weight = 0.48356670454077239D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.BackColor = System.Drawing.Color.White;
            this.xrTableCell387.BorderColor = System.Drawing.Color.White;
            this.xrTableCell387.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.DateLoaded")});
            this.xrTableCell387.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell387.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell387.Multiline = true;
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseBackColor = false;
            this.xrTableCell387.StylePriority.UseBorderColor = false;
            this.xrTableCell387.StylePriority.UseFont = false;
            this.xrTableCell387.StylePriority.UseForeColor = false;
            this.xrTableCell387.StylePriority.UseTextAlignment = false;
            this.xrTableCell387.Text = "xrTableCell387";
            this.xrTableCell387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell387.Weight = 0.55204755283234164D;
            // 
            // lblDAAmountD
            // 
            this.lblDAAmountD.BackColor = System.Drawing.Color.White;
            this.lblDAAmountD.BorderColor = System.Drawing.Color.White;
            this.lblDAAmountD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Amount")});
            this.lblDAAmountD.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblDAAmountD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDAAmountD.Name = "lblDAAmountD";
            this.lblDAAmountD.StylePriority.UseBackColor = false;
            this.lblDAAmountD.StylePriority.UseBorderColor = false;
            this.lblDAAmountD.StylePriority.UseFont = false;
            this.lblDAAmountD.StylePriority.UseForeColor = false;
            this.lblDAAmountD.Text = "lblDAAmountD";
            this.lblDAAmountD.Weight = 0.64638773916220549D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.BackColor = System.Drawing.Color.White;
            this.xrTableCell389.BorderColor = System.Drawing.Color.White;
            this.xrTableCell389.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.StatusCode")});
            this.xrTableCell389.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell389.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell389.Multiline = true;
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.StylePriority.UseBackColor = false;
            this.xrTableCell389.StylePriority.UseBorderColor = false;
            this.xrTableCell389.StylePriority.UseFont = false;
            this.xrTableCell389.StylePriority.UseForeColor = false;
            this.xrTableCell389.Text = "xrTableCell389";
            this.xrTableCell389.Weight = 0.86404692370677649D;
            // 
            // GroupHeader10
            // 
            this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable67,
            this.xrTable68});
            this.GroupHeader10.HeightF = 55.20833F;
            this.GroupHeader10.Name = "GroupHeader10";
            // 
            // xrTable67
            // 
            this.xrTable67.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable67.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable67.ForeColor = System.Drawing.Color.Gray;
            this.xrTable67.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrTable67.Name = "xrTable67";
            this.xrTable67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable67.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow142});
            this.xrTable67.SizeF = new System.Drawing.SizeF(880F, 32.49994F);
            this.xrTable67.StylePriority.UseBorders = false;
            this.xrTable67.StylePriority.UseFont = false;
            this.xrTable67.StylePriority.UseForeColor = false;
            this.xrTable67.StylePriority.UsePadding = false;
            this.xrTable67.StylePriority.UseTextAlignment = false;
            this.xrTable67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow142
            // 
            this.xrTableRow142.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell377});
            this.xrTableRow142.Name = "xrTableRow142";
            this.xrTableRow142.Weight = 0.8D;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.BackColor = System.Drawing.Color.White;
            this.xrTableCell377.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell377.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell377.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell377.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell377.StylePriority.UseBackColor = false;
            this.xrTableCell377.StylePriority.UseBorderColor = false;
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.StylePriority.UseFont = false;
            this.xrTableCell377.StylePriority.UseForeColor = false;
            this.xrTableCell377.StylePriority.UsePadding = false;
            this.xrTableCell377.Text = "XDS Default Listing";
            this.xrTableCell377.Weight = 3D;
            // 
            // xrTable68
            // 
            this.xrTable68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable68.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable68.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable68.ForeColor = System.Drawing.Color.Gray;
            this.xrTable68.LocationFloat = new DevExpress.Utils.PointFloat(10F, 32.49994F);
            this.xrTable68.Name = "xrTable68";
            this.xrTable68.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow144});
            this.xrTable68.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable68.StylePriority.UseBackColor = false;
            this.xrTable68.StylePriority.UseBorders = false;
            this.xrTable68.StylePriority.UseFont = false;
            this.xrTable68.StylePriority.UseForeColor = false;
            this.xrTable68.StylePriority.UseTextAlignment = false;
            this.xrTable68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow144
            // 
            this.xrTableRow144.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell381,
            this.xrTableCell382,
            this.xrTableCell383,
            this.xrTableCell384,
            this.xrTableCell385});
            this.xrTableRow144.Name = "xrTableRow144";
            this.xrTableRow144.Weight = 0.400000244140774D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell381.BorderColor = System.Drawing.Color.White;
            this.xrTableCell381.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell381.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.StylePriority.UseBackColor = false;
            this.xrTableCell381.StylePriority.UseBorderColor = false;
            this.xrTableCell381.StylePriority.UseFont = false;
            this.xrTableCell381.StylePriority.UseForeColor = false;
            this.xrTableCell381.Text = "Subscriber";
            this.xrTableCell381.Weight = 0.45775330285033344D;
            // 
            // xrTableCell382
            // 
            this.xrTableCell382.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell382.BorderColor = System.Drawing.Color.White;
            this.xrTableCell382.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell382.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell382.Name = "xrTableCell382";
            this.xrTableCell382.StylePriority.UseBackColor = false;
            this.xrTableCell382.StylePriority.UseBorderColor = false;
            this.xrTableCell382.StylePriority.UseFont = false;
            this.xrTableCell382.StylePriority.UseForeColor = false;
            this.xrTableCell382.Text = "Account No.";
            this.xrTableCell382.Weight = 0.48356670454077239D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell383.BorderColor = System.Drawing.Color.White;
            this.xrTableCell383.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell383.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell383.Multiline = true;
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseBackColor = false;
            this.xrTableCell383.StylePriority.UseBorderColor = false;
            this.xrTableCell383.StylePriority.UseFont = false;
            this.xrTableCell383.StylePriority.UseForeColor = false;
            this.xrTableCell383.StylePriority.UseTextAlignment = false;
            this.xrTableCell383.Text = "Date Listed";
            this.xrTableCell383.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell383.Weight = 0.55204755283234164D;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell384.BorderColor = System.Drawing.Color.White;
            this.xrTableCell384.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell384.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.StylePriority.UseBackColor = false;
            this.xrTableCell384.StylePriority.UseBorderColor = false;
            this.xrTableCell384.StylePriority.UseFont = false;
            this.xrTableCell384.StylePriority.UseForeColor = false;
            this.xrTableCell384.Text = "Amount";
            this.xrTableCell384.Weight = 0.64638773916220549D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell385.BorderColor = System.Drawing.Color.White;
            this.xrTableCell385.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell385.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell385.Multiline = true;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseBackColor = false;
            this.xrTableCell385.StylePriority.UseBorderColor = false;
            this.xrTableCell385.StylePriority.UseFont = false;
            this.xrTableCell385.StylePriority.UseForeColor = false;
            this.xrTableCell385.Text = "Account Status";
            this.xrTableCell385.Weight = 0.86404692370677649D;
            // 
            // Rehabilitation
            // 
            this.Rehabilitation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailRehabilitation,
            this.GroupHeaderRehabilitation});
            this.Rehabilitation.DataMember = "ConsumerRehabilitationOrder";
            this.Rehabilitation.Level = 8;
            this.Rehabilitation.Name = "Rehabilitation";
            // 
            // DetailRehabilitation
            // 
            this.DetailRehabilitation.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable33});
            this.DetailRehabilitation.HeightF = 22.70839F;
            this.DetailRehabilitation.Name = "DetailRehabilitation";
            // 
            // xrTable33
            // 
            this.xrTable33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable33.ForeColor = System.Drawing.Color.Gray;
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow61});
            this.xrTable33.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable33.StylePriority.UseBackColor = false;
            this.xrTable33.StylePriority.UseBorders = false;
            this.xrTable33.StylePriority.UseFont = false;
            this.xrTable33.StylePriority.UseForeColor = false;
            this.xrTable33.StylePriority.UseTextAlignment = false;
            this.xrTable33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell163,
            this.xrTableCell164,
            this.lblREHDAmount,
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 0.400000244140774D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.BackColor = System.Drawing.Color.White;
            this.xrTableCell153.BorderColor = System.Drawing.Color.White;
            this.xrTableCell153.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.CaseNumber")});
            this.xrTableCell153.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell153.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBackColor = false;
            this.xrTableCell153.StylePriority.UseBorderColor = false;
            this.xrTableCell153.StylePriority.UseFont = false;
            this.xrTableCell153.StylePriority.UseForeColor = false;
            this.xrTableCell153.Text = "xrTableCell153";
            this.xrTableCell153.Weight = 0.27286026039556177D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.BackColor = System.Drawing.Color.White;
            this.xrTableCell163.BorderColor = System.Drawing.Color.White;
            this.xrTableCell163.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.CaseFilingDate")});
            this.xrTableCell163.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell163.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.StylePriority.UseBackColor = false;
            this.xrTableCell163.StylePriority.UseBorderColor = false;
            this.xrTableCell163.StylePriority.UseFont = false;
            this.xrTableCell163.StylePriority.UseForeColor = false;
            this.xrTableCell163.Text = "xrTableCell163";
            this.xrTableCell163.Weight = 0.326212470749496D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.BackColor = System.Drawing.Color.White;
            this.xrTableCell164.BorderColor = System.Drawing.Color.White;
            this.xrTableCell164.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.CaseType")});
            this.xrTableCell164.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell164.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell164.Multiline = true;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBackColor = false;
            this.xrTableCell164.StylePriority.UseBorderColor = false;
            this.xrTableCell164.StylePriority.UseFont = false;
            this.xrTableCell164.StylePriority.UseForeColor = false;
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.Text = "xrTableCell164";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell164.Weight = 0.34224727248367504D;
            // 
            // lblREHDAmount
            // 
            this.lblREHDAmount.BackColor = System.Drawing.Color.White;
            this.lblREHDAmount.BorderColor = System.Drawing.Color.White;
            this.lblREHDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.DisputeAmt")});
            this.lblREHDAmount.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblREHDAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblREHDAmount.Name = "lblREHDAmount";
            this.lblREHDAmount.StylePriority.UseBackColor = false;
            this.lblREHDAmount.StylePriority.UseBorderColor = false;
            this.lblREHDAmount.StylePriority.UseFont = false;
            this.lblREHDAmount.StylePriority.UseForeColor = false;
            this.lblREHDAmount.Text = "lblREHDAmount";
            this.lblREHDAmount.Weight = 0.38962705889832266D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.BackColor = System.Drawing.Color.White;
            this.xrTableCell166.BorderColor = System.Drawing.Color.White;
            this.xrTableCell166.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.PlaintiffName")});
            this.xrTableCell166.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell166.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell166.Multiline = true;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBackColor = false;
            this.xrTableCell166.StylePriority.UseBorderColor = false;
            this.xrTableCell166.StylePriority.UseFont = false;
            this.xrTableCell166.StylePriority.UseForeColor = false;
            this.xrTableCell166.Text = "xrTableCell166";
            this.xrTableCell166.Weight = 0.41089098500973215D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.BackColor = System.Drawing.Color.White;
            this.xrTableCell167.BorderColor = System.Drawing.Color.White;
            this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.CourtName")});
            this.xrTableCell167.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell167.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell167.Multiline = true;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBackColor = false;
            this.xrTableCell167.StylePriority.UseBorderColor = false;
            this.xrTableCell167.StylePriority.UseFont = false;
            this.xrTableCell167.StylePriority.UseForeColor = false;
            this.xrTableCell167.Text = "xrTableCell167";
            this.xrTableCell167.Weight = 0.37079453204633855D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.BackColor = System.Drawing.Color.White;
            this.xrTableCell168.BorderColor = System.Drawing.Color.White;
            this.xrTableCell168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.AttorneyName")});
            this.xrTableCell168.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell168.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBackColor = false;
            this.xrTableCell168.StylePriority.UseBorderColor = false;
            this.xrTableCell168.StylePriority.UseFont = false;
            this.xrTableCell168.StylePriority.UseForeColor = false;
            this.xrTableCell168.Text = "xrTableCell168";
            this.xrTableCell168.Weight = 0.34981739103157627D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.BackColor = System.Drawing.Color.White;
            this.xrTableCell169.BorderColor = System.Drawing.Color.White;
            this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.TelephoneNo")});
            this.xrTableCell169.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell169.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBackColor = false;
            this.xrTableCell169.StylePriority.UseBorderColor = false;
            this.xrTableCell169.StylePriority.UseFont = false;
            this.xrTableCell169.StylePriority.UseForeColor = false;
            this.xrTableCell169.Text = "xrTableCell169";
            this.xrTableCell169.Weight = 0.2824662664277991D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.BackColor = System.Drawing.Color.White;
            this.xrTableCell170.BorderColor = System.Drawing.Color.White;
            this.xrTableCell170.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerRehabilitationOrder.Comments")});
            this.xrTableCell170.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell170.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBackColor = false;
            this.xrTableCell170.StylePriority.UseBorderColor = false;
            this.xrTableCell170.StylePriority.UseFont = false;
            this.xrTableCell170.StylePriority.UseForeColor = false;
            this.xrTableCell170.Text = "xrTableCell170";
            this.xrTableCell170.Weight = 0.25888598604992807D;
            // 
            // GroupHeaderRehabilitation
            // 
            this.GroupHeaderRehabilitation.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable32,
            this.xrTable31});
            this.GroupHeaderRehabilitation.HeightF = 86.04161F;
            this.GroupHeaderRehabilitation.Name = "GroupHeaderRehabilitation";
            // 
            // xrTable32
            // 
            this.xrTable32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable32.ForeColor = System.Drawing.Color.Gray;
            this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 63.33322F);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow62});
            this.xrTable32.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable32.StylePriority.UseBackColor = false;
            this.xrTable32.StylePriority.UseBorders = false;
            this.xrTable32.StylePriority.UseFont = false;
            this.xrTable32.StylePriority.UseForeColor = false;
            this.xrTable32.StylePriority.UseTextAlignment = false;
            this.xrTable32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154,
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 0.400000244140774D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell154.BorderColor = System.Drawing.Color.White;
            this.xrTableCell154.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell154.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBackColor = false;
            this.xrTableCell154.StylePriority.UseBorderColor = false;
            this.xrTableCell154.StylePriority.UseFont = false;
            this.xrTableCell154.StylePriority.UseForeColor = false;
            this.xrTableCell154.Text = "Case No.";
            this.xrTableCell154.Weight = 0.27286026039556177D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell155.BorderColor = System.Drawing.Color.White;
            this.xrTableCell155.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell155.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBackColor = false;
            this.xrTableCell155.StylePriority.UseBorderColor = false;
            this.xrTableCell155.StylePriority.UseFont = false;
            this.xrTableCell155.StylePriority.UseForeColor = false;
            this.xrTableCell155.Text = "Issue Date";
            this.xrTableCell155.Weight = 0.326212470749496D;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell156.BorderColor = System.Drawing.Color.White;
            this.xrTableCell156.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell156.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell156.Multiline = true;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBackColor = false;
            this.xrTableCell156.StylePriority.UseBorderColor = false;
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseForeColor = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.Text = "Judgement Type";
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell156.Weight = 0.34224727248367504D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell157.BorderColor = System.Drawing.Color.White;
            this.xrTableCell157.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell157.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBackColor = false;
            this.xrTableCell157.StylePriority.UseBorderColor = false;
            this.xrTableCell157.StylePriority.UseFont = false;
            this.xrTableCell157.StylePriority.UseForeColor = false;
            this.xrTableCell157.Text = "Amount";
            this.xrTableCell157.Weight = 0.38962705889832266D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell158.BorderColor = System.Drawing.Color.White;
            this.xrTableCell158.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell158.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell158.Multiline = true;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBackColor = false;
            this.xrTableCell158.StylePriority.UseBorderColor = false;
            this.xrTableCell158.StylePriority.UseFont = false;
            this.xrTableCell158.StylePriority.UseForeColor = false;
            this.xrTableCell158.Text = "Plaintiff";
            this.xrTableCell158.Weight = 0.41089098500973215D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell159.BorderColor = System.Drawing.Color.White;
            this.xrTableCell159.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell159.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell159.Multiline = true;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBackColor = false;
            this.xrTableCell159.StylePriority.UseBorderColor = false;
            this.xrTableCell159.StylePriority.UseFont = false;
            this.xrTableCell159.StylePriority.UseForeColor = false;
            this.xrTableCell159.Text = "Court";
            this.xrTableCell159.Weight = 0.37079453204633855D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell160.BorderColor = System.Drawing.Color.White;
            this.xrTableCell160.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell160.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBackColor = false;
            this.xrTableCell160.StylePriority.UseBorderColor = false;
            this.xrTableCell160.StylePriority.UseFont = false;
            this.xrTableCell160.StylePriority.UseForeColor = false;
            this.xrTableCell160.Text = "Attorney";
            this.xrTableCell160.Weight = 0.34981739103157627D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell161.BorderColor = System.Drawing.Color.White;
            this.xrTableCell161.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell161.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseBackColor = false;
            this.xrTableCell161.StylePriority.UseBorderColor = false;
            this.xrTableCell161.StylePriority.UseFont = false;
            this.xrTableCell161.StylePriority.UseForeColor = false;
            this.xrTableCell161.Text = "Phone No";
            this.xrTableCell161.Weight = 0.2824662664277991D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell162.BorderColor = System.Drawing.Color.White;
            this.xrTableCell162.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell162.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBackColor = false;
            this.xrTableCell162.StylePriority.UseBorderColor = false;
            this.xrTableCell162.StylePriority.UseFont = false;
            this.xrTableCell162.StylePriority.UseForeColor = false;
            this.xrTableCell162.Text = "Comment";
            this.xrTableCell162.Weight = 0.25888598604992807D;
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable31.ForeColor = System.Drawing.Color.Gray;
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 4.791769F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow58,
            this.xrTableRow60});
            this.xrTable31.SizeF = new System.Drawing.SizeF(880F, 55.20834F);
            this.xrTable31.StylePriority.UseBorders = false;
            this.xrTable31.StylePriority.UseFont = false;
            this.xrTable31.StylePriority.UseForeColor = false;
            this.xrTable31.StylePriority.UsePadding = false;
            this.xrTable31.StylePriority.UseTextAlignment = false;
            this.xrTable31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell139});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 0.8D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.BackColor = System.Drawing.Color.White;
            this.xrTableCell139.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell139.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell139.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell139.StylePriority.UseBackColor = false;
            this.xrTableCell139.StylePriority.UseBorderColor = false;
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.StylePriority.UseFont = false;
            this.xrTableCell139.StylePriority.UseForeColor = false;
            this.xrTableCell139.StylePriority.UsePadding = false;
            this.xrTableCell139.Text = "Rehabilitation";
            this.xrTableCell139.Weight = 3D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 0.53333333333333344D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.BackColor = System.Drawing.Color.White;
            this.xrTableCell152.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell152.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell152.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell152.Multiline = true;
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell152.StylePriority.UseBackColor = false;
            this.xrTableCell152.StylePriority.UseBorderColor = false;
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.StylePriority.UseFont = false;
            this.xrTableCell152.StylePriority.UseForeColor = false;
            this.xrTableCell152.StylePriority.UsePadding = false;
            this.xrTableCell152.Text = "This section details information on the Rehabilitation Order that was granted aft" +
    "er all debts as per the Sequestration Oder were paid up.\r\n";
            this.xrTableCell152.Weight = 3D;
            // 
            // DebtReview
            // 
            this.DebtReview.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDebtReview,
            this.GroupHeaderDebtReview});
            this.DebtReview.DataMember = "ConsumerDebtReviewStatus";
            this.DebtReview.Level = 9;
            this.DebtReview.Name = "DebtReview";
            // 
            // DetailDebtReview
            // 
            this.DetailDebtReview.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable35});
            this.DetailDebtReview.HeightF = 80F;
            this.DetailDebtReview.Name = "DetailDebtReview";
            // 
            // xrTable35
            // 
            this.xrTable35.BorderColor = System.Drawing.Color.Transparent;
            this.xrTable35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable35.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable35.ForeColor = System.Drawing.Color.Gray;
            this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow65,
            this.xrTableRow66,
            this.xrTableRow67,
            this.xrTableRow70});
            this.xrTable35.SizeF = new System.Drawing.SizeF(880.0001F, 80F);
            this.xrTable35.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable35.StylePriority.UseBorderColor = false;
            this.xrTable35.StylePriority.UseBorders = false;
            this.xrTable35.StylePriority.UseFont = false;
            this.xrTable35.StylePriority.UseForeColor = false;
            this.xrTable35.StylePriority.UsePadding = false;
            this.xrTable35.StylePriority.UseTextAlignment = false;
            this.xrTable35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell174});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 0.79999999999999993D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell173.BorderColor = System.Drawing.Color.White;
            this.xrTableCell173.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell173.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseBackColor = false;
            this.xrTableCell173.StylePriority.UseBorderColor = false;
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.StylePriority.UseForeColor = false;
            this.xrTableCell173.Text = "Debt Review date";
            this.xrTableCell173.Weight = 1.4914771533259312D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell174.BorderColor = System.Drawing.Color.White;
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell174.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatusDate")});
            this.xrTableCell174.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell174.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBackColor = false;
            this.xrTableCell174.StylePriority.UseBorderColor = false;
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.StylePriority.UseFont = false;
            this.xrTableCell174.StylePriority.UseForeColor = false;
            this.xrTableCell174.Text = "xrTableCell174";
            this.xrTableCell174.Weight = 1.5085228466740686D;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175,
            this.xrTableCell176});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 0.79999999999999993D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.BackColor = System.Drawing.Color.White;
            this.xrTableCell175.BorderColor = System.Drawing.Color.White;
            this.xrTableCell175.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell175.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBackColor = false;
            this.xrTableCell175.StylePriority.UseBorderColor = false;
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.StylePriority.UseForeColor = false;
            this.xrTableCell175.Text = "Debt Counsellor Name";
            this.xrTableCell175.Weight = 1.4914771533259312D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.BackColor = System.Drawing.Color.White;
            this.xrTableCell176.BorderColor = System.Drawing.Color.White;
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell176.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorName")});
            this.xrTableCell176.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell176.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBackColor = false;
            this.xrTableCell176.StylePriority.UseBorderColor = false;
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.StylePriority.UseFont = false;
            this.xrTableCell176.StylePriority.UseForeColor = false;
            this.xrTableCell176.Text = "xrTableCell176";
            this.xrTableCell176.Weight = 1.5085228466740686D;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell179});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 0.8D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell177.BorderColor = System.Drawing.Color.White;
            this.xrTableCell177.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell177.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBackColor = false;
            this.xrTableCell177.StylePriority.UseBorderColor = false;
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.StylePriority.UseForeColor = false;
            this.xrTableCell177.Text = "Debt Counsellor Contact No.";
            this.xrTableCell177.Weight = 1.49147684121438D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.BackColor = System.Drawing.Color.White;
            this.xrTableCell178.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell178.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell178.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBackColor = false;
            this.xrTableCell178.StylePriority.UseBorderColor = false;
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.StylePriority.UseFont = false;
            this.xrTableCell178.StylePriority.UseForeColor = false;
            this.xrTableCell178.Weight = 0.00852312015577406D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell179.BorderColor = System.Drawing.Color.White;
            this.xrTableCell179.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell179.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorTelephoneNo")});
            this.xrTableCell179.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell179.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBackColor = false;
            this.xrTableCell179.StylePriority.UseBorderColor = false;
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.StylePriority.UseFont = false;
            this.xrTableCell179.StylePriority.UseForeColor = false;
            this.xrTableCell179.Text = "xrTableCell179";
            this.xrTableCell179.Weight = 1.5000000386298458D;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell182});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 0.8D;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.BackColor = System.Drawing.Color.White;
            this.xrTableCell180.BorderColor = System.Drawing.Color.White;
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell180.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBackColor = false;
            this.xrTableCell180.StylePriority.UseBorderColor = false;
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.StylePriority.UseForeColor = false;
            this.xrTableCell180.Text = "Debt Review Status";
            this.xrTableCell180.Weight = 1.4914771533259312D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.BackColor = System.Drawing.Color.White;
            this.xrTableCell182.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell182.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatus")});
            this.xrTableCell182.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell182.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBackColor = false;
            this.xrTableCell182.StylePriority.UseBorderColor = false;
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseForeColor = false;
            this.xrTableCell182.Text = "xrTableCell182";
            this.xrTableCell182.Weight = 1.5085228466740686D;
            // 
            // GroupHeaderDebtReview
            // 
            this.GroupHeaderDebtReview.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable34});
            this.GroupHeaderDebtReview.HeightF = 48.95837F;
            this.GroupHeaderDebtReview.Name = "GroupHeaderDebtReview";
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable34.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable34.ForeColor = System.Drawing.Color.Gray;
            this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow63});
            this.xrTable34.SizeF = new System.Drawing.SizeF(880F, 30F);
            this.xrTable34.StylePriority.UseBorders = false;
            this.xrTable34.StylePriority.UseFont = false;
            this.xrTable34.StylePriority.UseForeColor = false;
            this.xrTable34.StylePriority.UsePadding = false;
            this.xrTable34.StylePriority.UseTextAlignment = false;
            this.xrTable34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 0.8D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.BackColor = System.Drawing.Color.White;
            this.xrTableCell171.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell171.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell171.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell171.StylePriority.UseBackColor = false;
            this.xrTableCell171.StylePriority.UseBorderColor = false;
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.StylePriority.UseFont = false;
            this.xrTableCell171.StylePriority.UseForeColor = false;
            this.xrTableCell171.StylePriority.UsePadding = false;
            this.xrTableCell171.Text = "Debt Review Record";
            this.xrTableCell171.Weight = 3D;
            // 
            // PaymentNotifications
            // 
            this.PaymentNotifications.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailPaymentNotifications,
            this.GroupHeaderPaymentNotifications});
            this.PaymentNotifications.DataMember = "XDSPaymentNotification";
            this.PaymentNotifications.Level = 10;
            this.PaymentNotifications.Name = "PaymentNotifications";
            // 
            // DetailPaymentNotifications
            // 
            this.DetailPaymentNotifications.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable38});
            this.DetailPaymentNotifications.HeightF = 22.70839F;
            this.DetailPaymentNotifications.Name = "DetailPaymentNotifications";
            // 
            // xrTable38
            // 
            this.xrTable38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable38.ForeColor = System.Drawing.Color.Gray;
            this.xrTable38.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73});
            this.xrTable38.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable38.StylePriority.UseBackColor = false;
            this.xrTable38.StylePriority.UseBorders = false;
            this.xrTable38.StylePriority.UseFont = false;
            this.xrTable38.StylePriority.UseForeColor = false;
            this.xrTable38.StylePriority.UseTextAlignment = false;
            this.xrTable38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell191,
            this.lblPmtAmountD,
            this.xrTableCell193});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 0.400000244140774D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.BackColor = System.Drawing.Color.White;
            this.xrTableCell189.BorderColor = System.Drawing.Color.White;
            this.xrTableCell189.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Company")});
            this.xrTableCell189.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell189.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBackColor = false;
            this.xrTableCell189.StylePriority.UseBorderColor = false;
            this.xrTableCell189.StylePriority.UseFont = false;
            this.xrTableCell189.StylePriority.UseForeColor = false;
            this.xrTableCell189.Text = "xrTableCell189";
            this.xrTableCell189.Weight = 0.45775330285033344D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.BackColor = System.Drawing.Color.White;
            this.xrTableCell190.BorderColor = System.Drawing.Color.White;
            this.xrTableCell190.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.AccountNo")});
            this.xrTableCell190.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell190.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBackColor = false;
            this.xrTableCell190.StylePriority.UseBorderColor = false;
            this.xrTableCell190.StylePriority.UseFont = false;
            this.xrTableCell190.StylePriority.UseForeColor = false;
            this.xrTableCell190.Text = "xrTableCell190";
            this.xrTableCell190.Weight = 0.48356670454077239D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.BackColor = System.Drawing.Color.White;
            this.xrTableCell191.BorderColor = System.Drawing.Color.White;
            this.xrTableCell191.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.DateLoaded")});
            this.xrTableCell191.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell191.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell191.Multiline = true;
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseBackColor = false;
            this.xrTableCell191.StylePriority.UseBorderColor = false;
            this.xrTableCell191.StylePriority.UseFont = false;
            this.xrTableCell191.StylePriority.UseForeColor = false;
            this.xrTableCell191.StylePriority.UseTextAlignment = false;
            this.xrTableCell191.Text = "xrTableCell191";
            this.xrTableCell191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell191.Weight = 0.55204755283234164D;
            // 
            // lblPmtAmountD
            // 
            this.lblPmtAmountD.BackColor = System.Drawing.Color.White;
            this.lblPmtAmountD.BorderColor = System.Drawing.Color.White;
            this.lblPmtAmountD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Amount")});
            this.lblPmtAmountD.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPmtAmountD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblPmtAmountD.Name = "lblPmtAmountD";
            this.lblPmtAmountD.StylePriority.UseBackColor = false;
            this.lblPmtAmountD.StylePriority.UseBorderColor = false;
            this.lblPmtAmountD.StylePriority.UseFont = false;
            this.lblPmtAmountD.StylePriority.UseForeColor = false;
            this.lblPmtAmountD.Text = "[XDSPaymentNotification.Amount]";
            this.lblPmtAmountD.Weight = 0.64638773916220549D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.BackColor = System.Drawing.Color.White;
            this.xrTableCell193.BorderColor = System.Drawing.Color.White;
            this.xrTableCell193.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.StatusCode")});
            this.xrTableCell193.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell193.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell193.Multiline = true;
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBackColor = false;
            this.xrTableCell193.StylePriority.UseBorderColor = false;
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.StylePriority.UseForeColor = false;
            this.xrTableCell193.Text = "xrTableCell193";
            this.xrTableCell193.Weight = 0.86404692370677649D;
            // 
            // GroupHeaderPaymentNotifications
            // 
            this.GroupHeaderPaymentNotifications.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable37,
            this.xrTable36});
            this.GroupHeaderPaymentNotifications.HeightF = 110.4167F;
            this.GroupHeaderPaymentNotifications.Name = "GroupHeaderPaymentNotifications";
            // 
            // xrTable37
            // 
            this.xrTable37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable37.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable37.ForeColor = System.Drawing.Color.Gray;
            this.xrTable37.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 87.70827F);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.xrTable37.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable37.StylePriority.UseBackColor = false;
            this.xrTable37.StylePriority.UseBorders = false;
            this.xrTable37.StylePriority.UseFont = false;
            this.xrTable37.StylePriority.UseForeColor = false;
            this.xrTable37.StylePriority.UseTextAlignment = false;
            this.xrTable37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 0.400000244140774D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell184.BorderColor = System.Drawing.Color.White;
            this.xrTableCell184.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell184.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBackColor = false;
            this.xrTableCell184.StylePriority.UseBorderColor = false;
            this.xrTableCell184.StylePriority.UseFont = false;
            this.xrTableCell184.StylePriority.UseForeColor = false;
            this.xrTableCell184.Text = "Subscriber";
            this.xrTableCell184.Weight = 0.45775330285033344D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell185.BorderColor = System.Drawing.Color.White;
            this.xrTableCell185.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell185.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBackColor = false;
            this.xrTableCell185.StylePriority.UseBorderColor = false;
            this.xrTableCell185.StylePriority.UseFont = false;
            this.xrTableCell185.StylePriority.UseForeColor = false;
            this.xrTableCell185.Text = "Account No.";
            this.xrTableCell185.Weight = 0.48356670454077239D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell186.BorderColor = System.Drawing.Color.White;
            this.xrTableCell186.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell186.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell186.Multiline = true;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBackColor = false;
            this.xrTableCell186.StylePriority.UseBorderColor = false;
            this.xrTableCell186.StylePriority.UseFont = false;
            this.xrTableCell186.StylePriority.UseForeColor = false;
            this.xrTableCell186.StylePriority.UseTextAlignment = false;
            this.xrTableCell186.Text = "Date Listed";
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell186.Weight = 0.55204755283234164D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell187.BorderColor = System.Drawing.Color.White;
            this.xrTableCell187.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell187.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBackColor = false;
            this.xrTableCell187.StylePriority.UseBorderColor = false;
            this.xrTableCell187.StylePriority.UseFont = false;
            this.xrTableCell187.StylePriority.UseForeColor = false;
            this.xrTableCell187.Text = "Amount";
            this.xrTableCell187.Weight = 0.64638773916220549D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell188.BorderColor = System.Drawing.Color.White;
            this.xrTableCell188.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell188.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell188.Multiline = true;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBackColor = false;
            this.xrTableCell188.StylePriority.UseBorderColor = false;
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseForeColor = false;
            this.xrTableCell188.Text = "Account Status";
            this.xrTableCell188.Weight = 0.86404692370677649D;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable36.ForeColor = System.Drawing.Color.Gray;
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 24F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64,
            this.xrTableRow71});
            this.xrTable36.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable36.StylePriority.UseBorders = false;
            this.xrTable36.StylePriority.UseFont = false;
            this.xrTable36.StylePriority.UseForeColor = false;
            this.xrTable36.StylePriority.UsePadding = false;
            this.xrTable36.StylePriority.UseTextAlignment = false;
            this.xrTable36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 0.8D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.BackColor = System.Drawing.Color.White;
            this.xrTableCell172.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell172.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell172.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell172.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell172.StylePriority.UseBackColor = false;
            this.xrTableCell172.StylePriority.UseBorderColor = false;
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.StylePriority.UseFont = false;
            this.xrTableCell172.StylePriority.UseForeColor = false;
            this.xrTableCell172.StylePriority.UsePadding = false;
            this.xrTableCell172.Text = "XDS Payment Notifications";
            this.xrTableCell172.Weight = 3D;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 0.53333333333333344D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.BackColor = System.Drawing.Color.White;
            this.xrTableCell183.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell183.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell183.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell183.Multiline = true;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell183.StylePriority.UseBackColor = false;
            this.xrTableCell183.StylePriority.UseBorderColor = false;
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.StylePriority.UseFont = false;
            this.xrTableCell183.StylePriority.UseForeColor = false;
            this.xrTableCell183.StylePriority.UsePadding = false;
            this.xrTableCell183.Text = "This section displays information of Credit or Service Providers who require that" +
    " you contact them";
            this.xrTableCell183.Weight = 3D;
            // 
            // EnquiryHistory
            // 
            this.EnquiryHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailEnquiryHistory,
            this.GroupHeaderEnquiryHistory});
            this.EnquiryHistory.DataMember = "ConsumerEnquiryHistory";
            this.EnquiryHistory.Level = 11;
            this.EnquiryHistory.Name = "EnquiryHistory";
            // 
            // DetailEnquiryHistory
            // 
            this.DetailEnquiryHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable41});
            this.DetailEnquiryHistory.HeightF = 22.70839F;
            this.DetailEnquiryHistory.Name = "DetailEnquiryHistory";
            // 
            // xrTable41
            // 
            this.xrTable41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable41.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable41.ForeColor = System.Drawing.Color.Gray;
            this.xrTable41.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable41.Name = "xrTable41";
            this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow77});
            this.xrTable41.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable41.StylePriority.UseBackColor = false;
            this.xrTable41.StylePriority.UseBorders = false;
            this.xrTable41.StylePriority.UseFont = false;
            this.xrTable41.StylePriority.UseForeColor = false;
            this.xrTable41.StylePriority.UseTextAlignment = false;
            this.xrTable41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201});
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.Weight = 0.400000244140774D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.BackColor = System.Drawing.Color.White;
            this.xrTableCell199.BorderColor = System.Drawing.Color.White;
            this.xrTableCell199.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.EnquiryDate")});
            this.xrTableCell199.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell199.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBackColor = false;
            this.xrTableCell199.StylePriority.UseBorderColor = false;
            this.xrTableCell199.StylePriority.UseFont = false;
            this.xrTableCell199.StylePriority.UseForeColor = false;
            this.xrTableCell199.Text = "xrTableCell199";
            this.xrTableCell199.Weight = 0.70362638966612234D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.BackColor = System.Drawing.Color.White;
            this.xrTableCell200.BorderColor = System.Drawing.Color.White;
            this.xrTableCell200.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberName")});
            this.xrTableCell200.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell200.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBackColor = false;
            this.xrTableCell200.StylePriority.UseBorderColor = false;
            this.xrTableCell200.StylePriority.UseFont = false;
            this.xrTableCell200.StylePriority.UseForeColor = false;
            this.xrTableCell200.Text = "xrTableCell200";
            this.xrTableCell200.Weight = 1.2546415239308193D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.BackColor = System.Drawing.Color.White;
            this.xrTableCell201.BorderColor = System.Drawing.Color.White;
            this.xrTableCell201.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberBusinessTypeDesc")});
            this.xrTableCell201.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell201.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell201.Multiline = true;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseBackColor = false;
            this.xrTableCell201.StylePriority.UseBorderColor = false;
            this.xrTableCell201.StylePriority.UseFont = false;
            this.xrTableCell201.StylePriority.UseForeColor = false;
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.Text = "xrTableCell201";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell201.Weight = 1.0455343094954881D;
            // 
            // GroupHeaderEnquiryHistory
            // 
            this.GroupHeaderEnquiryHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable40,
            this.xrTable39});
            this.GroupHeaderEnquiryHistory.Name = "GroupHeaderEnquiryHistory";
            // 
            // xrTable40
            // 
            this.xrTable40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable40.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable40.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable40.ForeColor = System.Drawing.Color.Gray;
            this.xrTable40.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 77.29161F);
            this.xrTable40.Name = "xrTable40";
            this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow76});
            this.xrTable40.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable40.StylePriority.UseBackColor = false;
            this.xrTable40.StylePriority.UseBorders = false;
            this.xrTable40.StylePriority.UseFont = false;
            this.xrTable40.StylePriority.UseForeColor = false;
            this.xrTable40.StylePriority.UseTextAlignment = false;
            this.xrTable40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198});
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.Weight = 0.400000244140774D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell196.BorderColor = System.Drawing.Color.White;
            this.xrTableCell196.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell196.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBackColor = false;
            this.xrTableCell196.StylePriority.UseBorderColor = false;
            this.xrTableCell196.StylePriority.UseFont = false;
            this.xrTableCell196.StylePriority.UseForeColor = false;
            this.xrTableCell196.Text = "Enquiry Date";
            this.xrTableCell196.Weight = 0.70362638966612234D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell197.BorderColor = System.Drawing.Color.White;
            this.xrTableCell197.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell197.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBackColor = false;
            this.xrTableCell197.StylePriority.UseBorderColor = false;
            this.xrTableCell197.StylePriority.UseFont = false;
            this.xrTableCell197.StylePriority.UseForeColor = false;
            this.xrTableCell197.Text = "Name of Credit Grantor";
            this.xrTableCell197.Weight = 1.2546415239308193D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell198.BorderColor = System.Drawing.Color.White;
            this.xrTableCell198.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell198.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell198.Multiline = true;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBackColor = false;
            this.xrTableCell198.StylePriority.UseBorderColor = false;
            this.xrTableCell198.StylePriority.UseFont = false;
            this.xrTableCell198.StylePriority.UseForeColor = false;
            this.xrTableCell198.StylePriority.UseTextAlignment = false;
            this.xrTableCell198.Text = "Type / Category of Credit Grantor";
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell198.Weight = 1.0455343094954881D;
            // 
            // xrTable39
            // 
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable39.ForeColor = System.Drawing.Color.Gray;
            this.xrTable39.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 14F);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74,
            this.xrTableRow75});
            this.xrTable39.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable39.StylePriority.UseBorders = false;
            this.xrTable39.StylePriority.UseFont = false;
            this.xrTable39.StylePriority.UseForeColor = false;
            this.xrTable39.StylePriority.UsePadding = false;
            this.xrTable39.StylePriority.UseTextAlignment = false;
            this.xrTable39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell194});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 0.8D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.BackColor = System.Drawing.Color.White;
            this.xrTableCell194.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell194.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell194.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell194.StylePriority.UseBackColor = false;
            this.xrTableCell194.StylePriority.UseBorderColor = false;
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.StylePriority.UseFont = false;
            this.xrTableCell194.StylePriority.UseForeColor = false;
            this.xrTableCell194.StylePriority.UsePadding = false;
            this.xrTableCell194.Text = "Credit Enquiry History";
            this.xrTableCell194.Weight = 3D;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell195});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 0.53333333333333344D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell195.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell195.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell195.Multiline = true;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell195.StylePriority.UseBorderColor = false;
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.StylePriority.UseFont = false;
            this.xrTableCell195.StylePriority.UseForeColor = false;
            this.xrTableCell195.StylePriority.UsePadding = false;
            this.xrTableCell195.Text = "This section includes a list of organisations or credit providers who have reques" +
    "ted your Credit Report.";
            this.xrTableCell195.Weight = 3D;
            // 
            // ContactHistory
            // 
            this.ContactHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.GroupHeaderContactHistory,
            this.AddressHistory,
            this.DetailReport9,
            this.DetailReport10,
            this.DetailReport11,
            this.DetailReport12});
            this.ContactHistory.Level = 12;
            this.ContactHistory.Name = "ContactHistory";
            // 
            // Detail13
            // 
            this.Detail13.HeightF = 0F;
            this.Detail13.Name = "Detail13";
            this.Detail13.Visible = false;
            // 
            // GroupHeaderContactHistory
            // 
            this.GroupHeaderContactHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable42});
            this.GroupHeaderContactHistory.HeightF = 79.16667F;
            this.GroupHeaderContactHistory.Name = "GroupHeaderContactHistory";
            // 
            // xrTable42
            // 
            this.xrTable42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable42.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable42.ForeColor = System.Drawing.Color.Gray;
            this.xrTable42.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 15F);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow79,
            this.xrTableRow80});
            this.xrTable42.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable42.StylePriority.UseBorders = false;
            this.xrTable42.StylePriority.UseFont = false;
            this.xrTable42.StylePriority.UseForeColor = false;
            this.xrTable42.StylePriority.UsePadding = false;
            this.xrTable42.StylePriority.UseTextAlignment = false;
            this.xrTable42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow79
            // 
            this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202});
            this.xrTableRow79.Name = "xrTableRow79";
            this.xrTableRow79.Weight = 0.8D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.BackColor = System.Drawing.Color.White;
            this.xrTableCell202.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell202.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell202.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell202.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell202.StylePriority.UseBackColor = false;
            this.xrTableCell202.StylePriority.UseBorderColor = false;
            this.xrTableCell202.StylePriority.UseBorders = false;
            this.xrTableCell202.StylePriority.UseFont = false;
            this.xrTableCell202.StylePriority.UseForeColor = false;
            this.xrTableCell202.StylePriority.UsePadding = false;
            this.xrTableCell202.Text = "Contact History";
            this.xrTableCell202.Weight = 3D;
            // 
            // xrTableRow80
            // 
            this.xrTableRow80.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203});
            this.xrTableRow80.Name = "xrTableRow80";
            this.xrTableRow80.Weight = 0.53333333333333344D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell203.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell203.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell203.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell203.Multiline = true;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell203.StylePriority.UseBorderColor = false;
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.StylePriority.UseFont = false;
            this.xrTableCell203.StylePriority.UseForeColor = false;
            this.xrTableCell203.StylePriority.UsePadding = false;
            this.xrTableCell203.Text = "This section list the last three sets of contact information provided on you by C" +
    "redit or Service Providers.";
            this.xrTableCell203.Weight = 3D;
            // 
            // AddressHistory
            // 
            this.AddressHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAddressHistory,
            this.GroupHeaderAddressHistory});
            this.AddressHistory.DataMember = "ConsumerAddressHistory";
            this.AddressHistory.Level = 0;
            this.AddressHistory.Name = "AddressHistory";
            // 
            // DetailAddressHistory
            // 
            this.DetailAddressHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable45});
            this.DetailAddressHistory.HeightF = 22.70839F;
            this.DetailAddressHistory.Name = "DetailAddressHistory";
            // 
            // xrTable45
            // 
            this.xrTable45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable45.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable45.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable45.ForeColor = System.Drawing.Color.Gray;
            this.xrTable45.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable45.Name = "xrTable45";
            this.xrTable45.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow86});
            this.xrTable45.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable45.StylePriority.UseBackColor = false;
            this.xrTable45.StylePriority.UseBorders = false;
            this.xrTable45.StylePriority.UseFont = false;
            this.xrTable45.StylePriority.UseForeColor = false;
            this.xrTable45.StylePriority.UseTextAlignment = false;
            this.xrTable45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow86
            // 
            this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell214,
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell215});
            this.xrTableRow86.Name = "xrTableRow86";
            this.xrTableRow86.Weight = 0.400000244140774D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.BackColor = System.Drawing.Color.White;
            this.xrTableCell212.BorderColor = System.Drawing.Color.White;
            this.xrTableCell212.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.LastUpdatedDate")});
            this.xrTableCell212.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell212.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseBackColor = false;
            this.xrTableCell212.StylePriority.UseBorderColor = false;
            this.xrTableCell212.StylePriority.UseFont = false;
            this.xrTableCell212.StylePriority.UseForeColor = false;
            this.xrTableCell212.Text = "xrTableCell212";
            this.xrTableCell212.Weight = 0.39019620660831805D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.BackColor = System.Drawing.Color.White;
            this.xrTableCell213.BorderColor = System.Drawing.Color.White;
            this.xrTableCell213.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.AddressType")});
            this.xrTableCell213.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell213.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseBackColor = false;
            this.xrTableCell213.StylePriority.UseBorderColor = false;
            this.xrTableCell213.StylePriority.UseFont = false;
            this.xrTableCell213.StylePriority.UseForeColor = false;
            this.xrTableCell213.Text = "xrTableCell213";
            this.xrTableCell213.Weight = 0.43272098114382451D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.BackColor = System.Drawing.Color.White;
            this.xrTableCell214.BorderColor = System.Drawing.Color.White;
            this.xrTableCell214.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address1")});
            this.xrTableCell214.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell214.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell214.Multiline = true;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseBackColor = false;
            this.xrTableCell214.StylePriority.UseBorderColor = false;
            this.xrTableCell214.StylePriority.UseFont = false;
            this.xrTableCell214.StylePriority.UseForeColor = false;
            this.xrTableCell214.StylePriority.UseTextAlignment = false;
            this.xrTableCell214.Text = "xrTableCell214";
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell214.Weight = 0.46831396768519656D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.BackColor = System.Drawing.Color.White;
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell114.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address2")});
            this.xrTableCell114.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell114.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBackColor = false;
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseFont = false;
            this.xrTableCell114.StylePriority.UseForeColor = false;
            this.xrTableCell114.Text = "xrTableCell114";
            this.xrTableCell114.Weight = 0.4473895304074969D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.BackColor = System.Drawing.Color.White;
            this.xrTableCell115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address3")});
            this.xrTableCell115.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell115.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBackColor = false;
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseFont = false;
            this.xrTableCell115.StylePriority.UseForeColor = false;
            this.xrTableCell115.Text = "xrTableCell115";
            this.xrTableCell115.Weight = 0.48624156775096961D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.BackColor = System.Drawing.Color.White;
            this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell116.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address4")});
            this.xrTableCell116.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell116.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBackColor = false;
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.StylePriority.UseFont = false;
            this.xrTableCell116.StylePriority.UseForeColor = false;
            this.xrTableCell116.Text = "xrTableCell116";
            this.xrTableCell116.Weight = 0.46527942033451392D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.BackColor = System.Drawing.Color.White;
            this.xrTableCell215.BorderColor = System.Drawing.Color.White;
            this.xrTableCell215.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.PostalCode")});
            this.xrTableCell215.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell215.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.StylePriority.UseBackColor = false;
            this.xrTableCell215.StylePriority.UseBorderColor = false;
            this.xrTableCell215.StylePriority.UseFont = false;
            this.xrTableCell215.StylePriority.UseForeColor = false;
            this.xrTableCell215.Text = "xrTableCell215";
            this.xrTableCell215.Weight = 0.31366054916211006D;
            // 
            // GroupHeaderAddressHistory
            // 
            this.GroupHeaderAddressHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable43});
            this.GroupHeaderAddressHistory.HeightF = 55.83344F;
            this.GroupHeaderAddressHistory.Name = "GroupHeaderAddressHistory";
            // 
            // xrTable43
            // 
            this.xrTable43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable43.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable43.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable43.ForeColor = System.Drawing.Color.Gray;
            this.xrTable43.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow81,
            this.xrTableRow82});
            this.xrTable43.SizeF = new System.Drawing.SizeF(880F, 55.83344F);
            this.xrTable43.StylePriority.UseBackColor = false;
            this.xrTable43.StylePriority.UseBorders = false;
            this.xrTable43.StylePriority.UseFont = false;
            this.xrTable43.StylePriority.UseForeColor = false;
            this.xrTable43.StylePriority.UseTextAlignment = false;
            this.xrTable43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow81
            // 
            this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell204});
            this.xrTableRow81.Name = "xrTableRow81";
            this.xrTableRow81.Weight = 0.400000244140774D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.BackColor = System.Drawing.Color.White;
            this.xrTableCell204.BorderColor = System.Drawing.Color.White;
            this.xrTableCell204.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell204.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell204.Multiline = true;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseBackColor = false;
            this.xrTableCell204.StylePriority.UseBorderColor = false;
            this.xrTableCell204.StylePriority.UseFont = false;
            this.xrTableCell204.StylePriority.UseForeColor = false;
            this.xrTableCell204.StylePriority.UseTextAlignment = false;
            this.xrTableCell204.Text = "Address History\r\n";
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell204.Weight = 3.0038022230924293D;
            // 
            // xrTableRow82
            // 
            this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell210});
            this.xrTableRow82.Name = "xrTableRow82";
            this.xrTableRow82.Weight = 0.400000244140774D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell205.BorderColor = System.Drawing.Color.White;
            this.xrTableCell205.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell205.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseBackColor = false;
            this.xrTableCell205.StylePriority.UseBorderColor = false;
            this.xrTableCell205.StylePriority.UseFont = false;
            this.xrTableCell205.StylePriority.UseForeColor = false;
            this.xrTableCell205.Text = "Bureau Update";
            this.xrTableCell205.Weight = 0.390196206608318D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell208.BorderColor = System.Drawing.Color.White;
            this.xrTableCell208.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell208.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseBackColor = false;
            this.xrTableCell208.StylePriority.UseBorderColor = false;
            this.xrTableCell208.StylePriority.UseFont = false;
            this.xrTableCell208.StylePriority.UseForeColor = false;
            this.xrTableCell208.Text = "Address Type";
            this.xrTableCell208.Weight = 0.43272098114382462D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell209.BorderColor = System.Drawing.Color.White;
            this.xrTableCell209.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell209.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell209.Multiline = true;
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBackColor = false;
            this.xrTableCell209.StylePriority.UseBorderColor = false;
            this.xrTableCell209.StylePriority.UseFont = false;
            this.xrTableCell209.StylePriority.UseForeColor = false;
            this.xrTableCell209.StylePriority.UseTextAlignment = false;
            this.xrTableCell209.Text = "Address Line 1";
            this.xrTableCell209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell209.Weight = 0.4683139156006686D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell111.BorderColor = System.Drawing.Color.White;
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell111.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell111.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBackColor = false;
            this.xrTableCell111.StylePriority.UseBorderColor = false;
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.StylePriority.UseFont = false;
            this.xrTableCell111.StylePriority.UseForeColor = false;
            this.xrTableCell111.Text = "Address Line 2";
            this.xrTableCell111.Weight = 0.44738953040749685D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell112.BorderColor = System.Drawing.Color.White;
            this.xrTableCell112.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell112.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell112.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBackColor = false;
            this.xrTableCell112.StylePriority.UseBorderColor = false;
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UseFont = false;
            this.xrTableCell112.StylePriority.UseForeColor = false;
            this.xrTableCell112.Text = "Address Line 3";
            this.xrTableCell112.Weight = 0.47941454919968907D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell113.BorderColor = System.Drawing.Color.White;
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell113.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell113.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBackColor = false;
            this.xrTableCell113.StylePriority.UseBorderColor = false;
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.StylePriority.UseFont = false;
            this.xrTableCell113.StylePriority.UseForeColor = false;
            this.xrTableCell113.Text = "Address Line 4";
            this.xrTableCell113.Weight = 0.47210664722390561D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell210.BorderColor = System.Drawing.Color.White;
            this.xrTableCell210.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell210.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.StylePriority.UseBackColor = false;
            this.xrTableCell210.StylePriority.UseBorderColor = false;
            this.xrTableCell210.StylePriority.UseFont = false;
            this.xrTableCell210.StylePriority.UseForeColor = false;
            this.xrTableCell210.Text = "Postal Code";
            this.xrTableCell210.Weight = 0.31366039290852665D;
            // 
            // DetailReport9
            // 
            this.DetailReport9.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailTelephoneHistory,
            this.GroupHeader11});
            this.DetailReport9.DataMember = "ConsumerTelephoneHistory";
            this.DetailReport9.Level = 1;
            this.DetailReport9.Name = "DetailReport9";
            // 
            // DetailTelephoneHistory
            // 
            this.DetailTelephoneHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable72});
            this.DetailTelephoneHistory.HeightF = 26.04167F;
            this.DetailTelephoneHistory.Name = "DetailTelephoneHistory";
            // 
            // xrTable72
            // 
            this.xrTable72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable72.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable72.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable72.ForeColor = System.Drawing.Color.Gray;
            this.xrTable72.LocationFloat = new DevExpress.Utils.PointFloat(11.58624F, 0F);
            this.xrTable72.Name = "xrTable72";
            this.xrTable72.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow149});
            this.xrTable72.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable72.StylePriority.UseBackColor = false;
            this.xrTable72.StylePriority.UseBorders = false;
            this.xrTable72.StylePriority.UseFont = false;
            this.xrTable72.StylePriority.UseForeColor = false;
            this.xrTable72.StylePriority.UseTextAlignment = false;
            this.xrTable72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow149
            // 
            this.xrTableRow149.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell403,
            this.xrTableCell404,
            this.xrTableCell405,
            this.xrTableCell407});
            this.xrTableRow149.Name = "xrTableRow149";
            this.xrTableRow149.Weight = 0.400000244140774D;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.BackColor = System.Drawing.Color.White;
            this.xrTableCell403.BorderColor = System.Drawing.Color.White;
            this.xrTableCell403.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.LastUpdatedDate")});
            this.xrTableCell403.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell403.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.StylePriority.UseBackColor = false;
            this.xrTableCell403.StylePriority.UseBorderColor = false;
            this.xrTableCell403.StylePriority.UseFont = false;
            this.xrTableCell403.StylePriority.UseForeColor = false;
            this.xrTableCell403.Text = "xrTableCell221";
            this.xrTableCell403.Weight = 0.70270397267790619D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.BackColor = System.Drawing.Color.White;
            this.xrTableCell404.BorderColor = System.Drawing.Color.White;
            this.xrTableCell404.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneType")});
            this.xrTableCell404.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell404.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseBackColor = false;
            this.xrTableCell404.StylePriority.UseBorderColor = false;
            this.xrTableCell404.StylePriority.UseFont = false;
            this.xrTableCell404.StylePriority.UseForeColor = false;
            this.xrTableCell404.Text = "xrTableCell222";
            this.xrTableCell404.Weight = 0.73279658709193907D;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.BackColor = System.Drawing.Color.White;
            this.xrTableCell405.BorderColor = System.Drawing.Color.White;
            this.xrTableCell405.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelCode")});
            this.xrTableCell405.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell405.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell405.Multiline = true;
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseBackColor = false;
            this.xrTableCell405.StylePriority.UseBorderColor = false;
            this.xrTableCell405.StylePriority.UseFont = false;
            this.xrTableCell405.StylePriority.UseForeColor = false;
            this.xrTableCell405.StylePriority.UseTextAlignment = false;
            this.xrTableCell405.Text = "xrTableCell223";
            this.xrTableCell405.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell405.Weight = 0.62639811216857055D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.BackColor = System.Drawing.Color.White;
            this.xrTableCell407.BorderColor = System.Drawing.Color.White;
            this.xrTableCell407.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneNo")});
            this.xrTableCell407.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell407.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.StylePriority.UseBackColor = false;
            this.xrTableCell407.StylePriority.UseBorderColor = false;
            this.xrTableCell407.StylePriority.UseFont = false;
            this.xrTableCell407.StylePriority.UseForeColor = false;
            this.xrTableCell407.Text = "xrTableCell224";
            this.xrTableCell407.Weight = 0.94190355115401381D;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable44});
            this.GroupHeader11.HeightF = 61.45833F;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // xrTable44
            // 
            this.xrTable44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable44.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable44.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable44.ForeColor = System.Drawing.Color.Gray;
            this.xrTable44.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 5.624898F);
            this.xrTable44.Name = "xrTable44";
            this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow83,
            this.xrTableRow84});
            this.xrTable44.SizeF = new System.Drawing.SizeF(880F, 55.83344F);
            this.xrTable44.StylePriority.UseBackColor = false;
            this.xrTable44.StylePriority.UseBorders = false;
            this.xrTable44.StylePriority.UseFont = false;
            this.xrTable44.StylePriority.UseForeColor = false;
            this.xrTable44.StylePriority.UseTextAlignment = false;
            this.xrTable44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow83
            // 
            this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell206});
            this.xrTableRow83.Name = "xrTableRow83";
            this.xrTableRow83.Weight = 0.400000244140774D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.BackColor = System.Drawing.Color.White;
            this.xrTableCell206.BorderColor = System.Drawing.Color.White;
            this.xrTableCell206.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell206.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell206.Multiline = true;
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseBackColor = false;
            this.xrTableCell206.StylePriority.UseBorderColor = false;
            this.xrTableCell206.StylePriority.UseFont = false;
            this.xrTableCell206.StylePriority.UseForeColor = false;
            this.xrTableCell206.StylePriority.UseTextAlignment = false;
            this.xrTableCell206.Text = "Contact Number History\r\n";
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell206.Weight = 3.0038022230924293D;
            // 
            // xrTableRow84
            // 
            this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell207,
            this.xrTableCell216,
            this.xrTableCell217,
            this.xrTableCell218});
            this.xrTableRow84.Name = "xrTableRow84";
            this.xrTableRow84.Weight = 0.400000244140774D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell207.BorderColor = System.Drawing.Color.White;
            this.xrTableCell207.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell207.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.StylePriority.UseBackColor = false;
            this.xrTableCell207.StylePriority.UseBorderColor = false;
            this.xrTableCell207.StylePriority.UseFont = false;
            this.xrTableCell207.StylePriority.UseForeColor = false;
            this.xrTableCell207.Text = "Bureau Update";
            this.xrTableCell207.Weight = 0.70270397267790619D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell216.BorderColor = System.Drawing.Color.White;
            this.xrTableCell216.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell216.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseBackColor = false;
            this.xrTableCell216.StylePriority.UseBorderColor = false;
            this.xrTableCell216.StylePriority.UseFont = false;
            this.xrTableCell216.StylePriority.UseForeColor = false;
            this.xrTableCell216.Text = "Telephone Type";
            this.xrTableCell216.Weight = 0.732796639176467D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell217.BorderColor = System.Drawing.Color.White;
            this.xrTableCell217.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell217.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell217.Multiline = true;
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseBackColor = false;
            this.xrTableCell217.StylePriority.UseBorderColor = false;
            this.xrTableCell217.StylePriority.UseFont = false;
            this.xrTableCell217.StylePriority.UseForeColor = false;
            this.xrTableCell217.StylePriority.UseTextAlignment = false;
            this.xrTableCell217.Text = "Telephone Code";
            this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell217.Weight = 0.62639790383045924D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell218.BorderColor = System.Drawing.Color.White;
            this.xrTableCell218.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell218.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.StylePriority.UseBackColor = false;
            this.xrTableCell218.StylePriority.UseBorderColor = false;
            this.xrTableCell218.StylePriority.UseFont = false;
            this.xrTableCell218.StylePriority.UseForeColor = false;
            this.xrTableCell218.Text = "Telephone Number";
            this.xrTableCell218.Weight = 0.94190370740759732D;
            // 
            // DetailReport10
            // 
            this.DetailReport10.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailEmailAddressHistory,
            this.GroupHeader12});
            this.DetailReport10.DataMember = "ConsumerEmailHistory";
            this.DetailReport10.Level = 2;
            this.DetailReport10.Name = "DetailReport10";
            // 
            // DetailEmailAddressHistory
            // 
            this.DetailEmailAddressHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable46});
            this.DetailEmailAddressHistory.HeightF = 26.04167F;
            this.DetailEmailAddressHistory.Name = "DetailEmailAddressHistory";
            // 
            // xrTable46
            // 
            this.xrTable46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable46.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable46.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable46.ForeColor = System.Drawing.Color.Gray;
            this.xrTable46.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable46.Name = "xrTable46";
            this.xrTable46.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow87});
            this.xrTable46.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable46.StylePriority.UseBackColor = false;
            this.xrTable46.StylePriority.UseBorders = false;
            this.xrTable46.StylePriority.UseFont = false;
            this.xrTable46.StylePriority.UseForeColor = false;
            this.xrTable46.StylePriority.UseTextAlignment = false;
            this.xrTable46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow87
            // 
            this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell221,
            this.xrTableCell222});
            this.xrTableRow87.Name = "xrTableRow87";
            this.xrTableRow87.Weight = 0.400000244140774D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.BackColor = System.Drawing.Color.White;
            this.xrTableCell221.BorderColor = System.Drawing.Color.White;
            this.xrTableCell221.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmailHistory.LastUpdatedDate")});
            this.xrTableCell221.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell221.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBackColor = false;
            this.xrTableCell221.StylePriority.UseBorderColor = false;
            this.xrTableCell221.StylePriority.UseFont = false;
            this.xrTableCell221.StylePriority.UseForeColor = false;
            this.xrTableCell221.Text = "xrTableCell221";
            this.xrTableCell221.Weight = 0.500811831676476D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.BackColor = System.Drawing.Color.White;
            this.xrTableCell222.BorderColor = System.Drawing.Color.White;
            this.xrTableCell222.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmailHistory.EmailAddress")});
            this.xrTableCell222.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell222.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBackColor = false;
            this.xrTableCell222.StylePriority.UseBorderColor = false;
            this.xrTableCell222.StylePriority.UseFont = false;
            this.xrTableCell222.StylePriority.UseForeColor = false;
            this.xrTableCell222.Text = "xrTableCell222";
            this.xrTableCell222.Weight = 1.0920278052870414D;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable70});
            this.GroupHeader12.HeightF = 64.58334F;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // xrTable70
            // 
            this.xrTable70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable70.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable70.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable70.ForeColor = System.Drawing.Color.Gray;
            this.xrTable70.LocationFloat = new DevExpress.Utils.PointFloat(11.58624F, 10.0001F);
            this.xrTable70.Name = "xrTable70";
            this.xrTable70.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow145,
            this.xrTableRow146});
            this.xrTable70.SizeF = new System.Drawing.SizeF(880F, 54.58324F);
            this.xrTable70.StylePriority.UseBackColor = false;
            this.xrTable70.StylePriority.UseBorders = false;
            this.xrTable70.StylePriority.UseFont = false;
            this.xrTable70.StylePriority.UseForeColor = false;
            this.xrTable70.StylePriority.UseTextAlignment = false;
            this.xrTable70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow145
            // 
            this.xrTableRow145.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell393});
            this.xrTableRow145.Name = "xrTableRow145";
            this.xrTableRow145.Weight = 0.400000244140774D;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.BackColor = System.Drawing.Color.White;
            this.xrTableCell393.BorderColor = System.Drawing.Color.White;
            this.xrTableCell393.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell393.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell393.Multiline = true;
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.StylePriority.UseBackColor = false;
            this.xrTableCell393.StylePriority.UseBorderColor = false;
            this.xrTableCell393.StylePriority.UseFont = false;
            this.xrTableCell393.StylePriority.UseForeColor = false;
            this.xrTableCell393.StylePriority.UseTextAlignment = false;
            this.xrTableCell393.Text = "Email Address History\r\n";
            this.xrTableCell393.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell393.Weight = 3.0038022230924293D;
            // 
            // xrTableRow146
            // 
            this.xrTableRow146.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell394,
            this.xrTableCell395});
            this.xrTableRow146.Name = "xrTableRow146";
            this.xrTableRow146.Weight = 0.400000244140774D;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell394.BorderColor = System.Drawing.Color.White;
            this.xrTableCell394.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell394.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.StylePriority.UseBackColor = false;
            this.xrTableCell394.StylePriority.UseBorderColor = false;
            this.xrTableCell394.StylePriority.UseFont = false;
            this.xrTableCell394.StylePriority.UseForeColor = false;
            this.xrTableCell394.Text = "Bureau Update";
            this.xrTableCell394.Weight = 0.50081194215285807D;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell395.BorderColor = System.Drawing.Color.White;
            this.xrTableCell395.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell395.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.StylePriority.UseBackColor = false;
            this.xrTableCell395.StylePriority.UseBorderColor = false;
            this.xrTableCell395.StylePriority.UseFont = false;
            this.xrTableCell395.StylePriority.UseForeColor = false;
            this.xrTableCell395.Text = "Email Address";
            this.xrTableCell395.Weight = 1.0920276948106593D;
            // 
            // DetailReport11
            // 
            this.DetailReport11.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailOtherContactAddress,
            this.GroupHeader13});
            this.DetailReport11.DataMember = "ConsumerOtherContactInfoAddress";
            this.DetailReport11.Level = 3;
            this.DetailReport11.Name = "DetailReport11";
            // 
            // DetailOtherContactAddress
            // 
            this.DetailOtherContactAddress.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable73});
            this.DetailOtherContactAddress.HeightF = 22.70839F;
            this.DetailOtherContactAddress.Name = "DetailOtherContactAddress";
            // 
            // xrTable73
            // 
            this.xrTable73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable73.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable73.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable73.ForeColor = System.Drawing.Color.Gray;
            this.xrTable73.LocationFloat = new DevExpress.Utils.PointFloat(11.58624F, 0F);
            this.xrTable73.Name = "xrTable73";
            this.xrTable73.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow151});
            this.xrTable73.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable73.StylePriority.UseBackColor = false;
            this.xrTable73.StylePriority.UseBorders = false;
            this.xrTable73.StylePriority.UseFont = false;
            this.xrTable73.StylePriority.UseForeColor = false;
            this.xrTable73.StylePriority.UseTextAlignment = false;
            this.xrTable73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow151
            // 
            this.xrTableRow151.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell408,
            this.xrTableCell409,
            this.xrTableCell410});
            this.xrTableRow151.Name = "xrTableRow151";
            this.xrTableRow151.Weight = 0.400000244140774D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.BackColor = System.Drawing.Color.White;
            this.xrTableCell408.BorderColor = System.Drawing.Color.White;
            this.xrTableCell408.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOtherContactInfoAddress.LastUpdatedDate")});
            this.xrTableCell408.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell408.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.StylePriority.UseBackColor = false;
            this.xrTableCell408.StylePriority.UseBorderColor = false;
            this.xrTableCell408.StylePriority.UseFont = false;
            this.xrTableCell408.StylePriority.UseForeColor = false;
            this.xrTableCell408.Text = "xrTableCell408";
            this.xrTableCell408.Weight = 0.706745419531867D;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.BackColor = System.Drawing.Color.White;
            this.xrTableCell409.BorderColor = System.Drawing.Color.White;
            this.xrTableCell409.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOtherContactInfoAddress.AddressTypeInd")});
            this.xrTableCell409.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell409.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.StylePriority.UseBackColor = false;
            this.xrTableCell409.StylePriority.UseBorderColor = false;
            this.xrTableCell409.StylePriority.UseFont = false;
            this.xrTableCell409.StylePriority.UseForeColor = false;
            this.xrTableCell409.Text = "xrTableCell409";
            this.xrTableCell409.Weight = 0.79827480753163D;
            // 
            // xrTableCell410
            // 
            this.xrTableCell410.BackColor = System.Drawing.Color.White;
            this.xrTableCell410.BorderColor = System.Drawing.Color.White;
            this.xrTableCell410.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOtherContactInfoAddress.Address")});
            this.xrTableCell410.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell410.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell410.Multiline = true;
            this.xrTableCell410.Name = "xrTableCell410";
            this.xrTableCell410.StylePriority.UseBackColor = false;
            this.xrTableCell410.StylePriority.UseBorderColor = false;
            this.xrTableCell410.StylePriority.UseFont = false;
            this.xrTableCell410.StylePriority.UseForeColor = false;
            this.xrTableCell410.StylePriority.UseTextAlignment = false;
            this.xrTableCell410.Text = "xrTableCell410";
            this.xrTableCell410.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell410.Weight = 1.4987819960289326D;
            // 
            // GroupHeader13
            // 
            this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable71});
            this.GroupHeader13.HeightF = 60.41667F;
            this.GroupHeader13.Name = "GroupHeader13";
            // 
            // xrTable71
            // 
            this.xrTable71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable71.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable71.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable71.ForeColor = System.Drawing.Color.Gray;
            this.xrTable71.LocationFloat = new DevExpress.Utils.PointFloat(11.58624F, 14.99989F);
            this.xrTable71.Name = "xrTable71";
            this.xrTable71.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow147,
            this.xrTableRow148});
            this.xrTable71.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable71.StylePriority.UseBackColor = false;
            this.xrTable71.StylePriority.UseBorders = false;
            this.xrTable71.StylePriority.UseFont = false;
            this.xrTable71.StylePriority.UseForeColor = false;
            this.xrTable71.StylePriority.UseTextAlignment = false;
            this.xrTable71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow147
            // 
            this.xrTableRow147.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell398});
            this.xrTableRow147.Name = "xrTableRow147";
            this.xrTableRow147.Weight = 0.400000244140774D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.BackColor = System.Drawing.Color.White;
            this.xrTableCell398.BorderColor = System.Drawing.Color.White;
            this.xrTableCell398.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell398.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell398.Multiline = true;
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.StylePriority.UseBackColor = false;
            this.xrTableCell398.StylePriority.UseBorderColor = false;
            this.xrTableCell398.StylePriority.UseFont = false;
            this.xrTableCell398.StylePriority.UseForeColor = false;
            this.xrTableCell398.StylePriority.UseTextAlignment = false;
            this.xrTableCell398.Text = "Trace More - Address History";
            this.xrTableCell398.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell398.Weight = 3.0038022230924293D;
            // 
            // xrTableRow148
            // 
            this.xrTableRow148.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell399,
            this.xrTableCell400,
            this.xrTableCell401});
            this.xrTableRow148.Name = "xrTableRow148";
            this.xrTableRow148.Weight = 0.400000244140774D;
            // 
            // xrTableCell399
            // 
            this.xrTableCell399.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell399.BorderColor = System.Drawing.Color.White;
            this.xrTableCell399.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell399.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell399.Name = "xrTableCell399";
            this.xrTableCell399.StylePriority.UseBackColor = false;
            this.xrTableCell399.StylePriority.UseBorderColor = false;
            this.xrTableCell399.StylePriority.UseFont = false;
            this.xrTableCell399.StylePriority.UseForeColor = false;
            this.xrTableCell399.Text = "Bureau Update";
            this.xrTableCell399.Weight = 0.706745419531867D;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell400.BorderColor = System.Drawing.Color.White;
            this.xrTableCell400.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell400.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.StylePriority.UseBackColor = false;
            this.xrTableCell400.StylePriority.UseBorderColor = false;
            this.xrTableCell400.StylePriority.UseFont = false;
            this.xrTableCell400.StylePriority.UseForeColor = false;
            this.xrTableCell400.Text = " Type";
            this.xrTableCell400.Weight = 0.79827480753163D;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell401.BorderColor = System.Drawing.Color.White;
            this.xrTableCell401.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell401.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell401.Multiline = true;
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.StylePriority.UseBackColor = false;
            this.xrTableCell401.StylePriority.UseBorderColor = false;
            this.xrTableCell401.StylePriority.UseFont = false;
            this.xrTableCell401.StylePriority.UseForeColor = false;
            this.xrTableCell401.StylePriority.UseTextAlignment = false;
            this.xrTableCell401.Text = "Address";
            this.xrTableCell401.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell401.Weight = 1.4987819960289326D;
            // 
            // DetailReport12
            // 
            this.DetailReport12.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailOtherContactTelephone,
            this.GroupHeader14});
            this.DetailReport12.DataMember = "ConsumerOtherContactInfoTelephone";
            this.DetailReport12.Level = 4;
            this.DetailReport12.Name = "DetailReport12";
            // 
            // DetailOtherContactTelephone
            // 
            this.DetailOtherContactTelephone.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable74});
            this.DetailOtherContactTelephone.HeightF = 22.70839F;
            this.DetailOtherContactTelephone.Name = "DetailOtherContactTelephone";
            // 
            // xrTable74
            // 
            this.xrTable74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable74.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable74.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable74.ForeColor = System.Drawing.Color.Gray;
            this.xrTable74.LocationFloat = new DevExpress.Utils.PointFloat(11.58624F, 0F);
            this.xrTable74.Name = "xrTable74";
            this.xrTable74.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow152});
            this.xrTable74.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable74.StylePriority.UseBackColor = false;
            this.xrTable74.StylePriority.UseBorders = false;
            this.xrTable74.StylePriority.UseFont = false;
            this.xrTable74.StylePriority.UseForeColor = false;
            this.xrTable74.StylePriority.UseTextAlignment = false;
            this.xrTable74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow152
            // 
            this.xrTableRow152.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell412,
            this.xrTableCell413,
            this.xrTableCell415});
            this.xrTableRow152.Name = "xrTableRow152";
            this.xrTableRow152.Weight = 0.400000244140774D;
            // 
            // xrTableCell412
            // 
            this.xrTableCell412.BackColor = System.Drawing.Color.White;
            this.xrTableCell412.BorderColor = System.Drawing.Color.White;
            this.xrTableCell412.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOtherContactInfoTelephone.LastUpdatedDate")});
            this.xrTableCell412.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell412.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell412.Name = "xrTableCell412";
            this.xrTableCell412.StylePriority.UseBackColor = false;
            this.xrTableCell412.StylePriority.UseBorderColor = false;
            this.xrTableCell412.StylePriority.UseFont = false;
            this.xrTableCell412.StylePriority.UseForeColor = false;
            this.xrTableCell412.Text = "xrTableCell412";
            this.xrTableCell412.Weight = 0.706745419531867D;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.BackColor = System.Drawing.Color.White;
            this.xrTableCell413.BorderColor = System.Drawing.Color.White;
            this.xrTableCell413.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOtherContactInfoTelephone.TelephoneTypeInd")});
            this.xrTableCell413.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell413.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.StylePriority.UseBackColor = false;
            this.xrTableCell413.StylePriority.UseBorderColor = false;
            this.xrTableCell413.StylePriority.UseFont = false;
            this.xrTableCell413.StylePriority.UseForeColor = false;
            this.xrTableCell413.Text = "xrTableCell413";
            this.xrTableCell413.Weight = 0.79827496378521356D;
            // 
            // xrTableCell415
            // 
            this.xrTableCell415.BackColor = System.Drawing.Color.White;
            this.xrTableCell415.BorderColor = System.Drawing.Color.White;
            this.xrTableCell415.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOtherContactInfoTelephone.TelephoneNumber")});
            this.xrTableCell415.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell415.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell415.Name = "xrTableCell415";
            this.xrTableCell415.StylePriority.UseBackColor = false;
            this.xrTableCell415.StylePriority.UseBorderColor = false;
            this.xrTableCell415.StylePriority.UseFont = false;
            this.xrTableCell415.StylePriority.UseForeColor = false;
            this.xrTableCell415.Text = "xrTableCell415";
            this.xrTableCell415.Weight = 1.498781839775349D;
            // 
            // GroupHeader14
            // 
            this.GroupHeader14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable56});
            this.GroupHeader14.HeightF = 57.75013F;
            this.GroupHeader14.Name = "GroupHeader14";
            // 
            // xrTable56
            // 
            this.xrTable56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable56.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable56.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable56.ForeColor = System.Drawing.Color.Gray;
            this.xrTable56.LocationFloat = new DevExpress.Utils.PointFloat(11.58624F, 12.33335F);
            this.xrTable56.Name = "xrTable56";
            this.xrTable56.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9,
            this.xrTableRow92});
            this.xrTable56.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable56.StylePriority.UseBackColor = false;
            this.xrTable56.StylePriority.UseBorders = false;
            this.xrTable56.StylePriority.UseFont = false;
            this.xrTable56.StylePriority.UseForeColor = false;
            this.xrTable56.StylePriority.UseTextAlignment = false;
            this.xrTable56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.400000244140774D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.BackColor = System.Drawing.Color.White;
            this.xrTableCell33.BorderColor = System.Drawing.Color.White;
            this.xrTableCell33.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell33.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell33.Multiline = true;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBackColor = false;
            this.xrTableCell33.StylePriority.UseBorderColor = false;
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseForeColor = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Trace More - Telephone History";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell33.Weight = 3.0038022230924293D;
            // 
            // xrTableRow92
            // 
            this.xrTableRow92.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell231,
            this.xrTableCell390,
            this.xrTableCell392});
            this.xrTableRow92.Name = "xrTableRow92";
            this.xrTableRow92.Weight = 0.400000244140774D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell231.BorderColor = System.Drawing.Color.White;
            this.xrTableCell231.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell231.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBackColor = false;
            this.xrTableCell231.StylePriority.UseBorderColor = false;
            this.xrTableCell231.StylePriority.UseFont = false;
            this.xrTableCell231.StylePriority.UseForeColor = false;
            this.xrTableCell231.Text = "Bureau Update";
            this.xrTableCell231.Weight = 0.706745419531867D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell390.BorderColor = System.Drawing.Color.White;
            this.xrTableCell390.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell390.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBackColor = false;
            this.xrTableCell390.StylePriority.UseBorderColor = false;
            this.xrTableCell390.StylePriority.UseFont = false;
            this.xrTableCell390.StylePriority.UseForeColor = false;
            this.xrTableCell390.Text = "Type";
            this.xrTableCell390.Weight = 0.79827496378521356D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell392.BorderColor = System.Drawing.Color.White;
            this.xrTableCell392.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell392.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.StylePriority.UseBackColor = false;
            this.xrTableCell392.StylePriority.UseBorderColor = false;
            this.xrTableCell392.StylePriority.UseFont = false;
            this.xrTableCell392.StylePriority.UseForeColor = false;
            this.xrTableCell392.Text = "Telephone Number";
            this.xrTableCell392.Weight = 1.498781839775349D;
            // 
            // ConfirmedInformation
            // 
            this.ConfirmedInformation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailConfirmedInformation,
            this.GroupHeaderConfirmedInformation,
            this.DetailReport,
            this.DetailReport1});
            this.ConfirmedInformation.Level = 13;
            this.ConfirmedInformation.Name = "ConfirmedInformation";
            // 
            // DetailConfirmedInformation
            // 
            this.DetailConfirmedInformation.HeightF = 0F;
            this.DetailConfirmedInformation.Name = "DetailConfirmedInformation";
            this.DetailConfirmedInformation.Visible = false;
            // 
            // GroupHeaderConfirmedInformation
            // 
            this.GroupHeaderConfirmedInformation.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable47});
            this.GroupHeaderConfirmedInformation.HeightF = 85.20832F;
            this.GroupHeaderConfirmedInformation.Name = "GroupHeaderConfirmedInformation";
            // 
            // xrTable47
            // 
            this.xrTable47.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable47.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable47.ForeColor = System.Drawing.Color.Gray;
            this.xrTable47.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable47.Name = "xrTable47";
            this.xrTable47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable47.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow85,
            this.xrTableRow88,
            this.xrTableRow89});
            this.xrTable47.SizeF = new System.Drawing.SizeF(880F, 70F);
            this.xrTable47.StylePriority.UseBorders = false;
            this.xrTable47.StylePriority.UseFont = false;
            this.xrTable47.StylePriority.UseForeColor = false;
            this.xrTable47.StylePriority.UsePadding = false;
            this.xrTable47.StylePriority.UseTextAlignment = false;
            this.xrTable47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow85
            // 
            this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell211});
            this.xrTableRow85.Name = "xrTableRow85";
            this.xrTableRow85.Weight = 0.8D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.BackColor = System.Drawing.Color.White;
            this.xrTableCell211.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell211.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell211.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell211.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell211.StylePriority.UseBackColor = false;
            this.xrTableCell211.StylePriority.UseBorderColor = false;
            this.xrTableCell211.StylePriority.UseBorders = false;
            this.xrTableCell211.StylePriority.UseFont = false;
            this.xrTableCell211.StylePriority.UseForeColor = false;
            this.xrTableCell211.StylePriority.UsePadding = false;
            this.xrTableCell211.Text = "Confirmed Information";
            this.xrTableCell211.Weight = 3D;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell227});
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Weight = 0.53333333333333344D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell227.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell227.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell227.Multiline = true;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell227.StylePriority.UseBorderColor = false;
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.StylePriority.UseFont = false;
            this.xrTableCell227.StylePriority.UseForeColor = false;
            this.xrTableCell227.StylePriority.UsePadding = false;
            this.xrTableCell227.Text = "This section displays a list of address, contact and employment information recei" +
    "ved from various XDS sources and has been confirmed with you";
            this.xrTableCell227.Weight = 3D;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell228});
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Weight = 0.53333333333333344D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell228.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell228.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell228.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorderColor = false;
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.StylePriority.UseFont = false;
            this.xrTableCell228.StylePriority.UseForeColor = false;
            this.xrTableCell228.Text = " at a point in time. ";
            this.xrTableCell228.Weight = 3D;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.DetailReport2,
            this.GroupHeader1});
            this.DetailReport.DataMember = "ConsumerAddressConfirmation";
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable58});
            this.Detail1.HeightF = 22.70839F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable58
            // 
            this.xrTable58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable58.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable58.ForeColor = System.Drawing.Color.Gray;
            this.xrTable58.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable58.Name = "xrTable58";
            this.xrTable58.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow114});
            this.xrTable58.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable58.StylePriority.UseBackColor = false;
            this.xrTable58.StylePriority.UseBorders = false;
            this.xrTable58.StylePriority.UseFont = false;
            this.xrTable58.StylePriority.UseForeColor = false;
            this.xrTable58.StylePriority.UseTextAlignment = false;
            this.xrTable58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow114
            // 
            this.xrTableRow114.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290,
            this.xrTableCell291,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTableCell294,
            this.xrTableCell295});
            this.xrTableRow114.Name = "xrTableRow114";
            this.xrTableRow114.Weight = 0.400000244140774D;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.BackColor = System.Drawing.Color.White;
            this.xrTableCell290.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell290.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressConfirmation.AddressType")});
            this.xrTableCell290.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell290.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.StylePriority.UseBackColor = false;
            this.xrTableCell290.StylePriority.UseBorderColor = false;
            this.xrTableCell290.StylePriority.UseFont = false;
            this.xrTableCell290.StylePriority.UseForeColor = false;
            this.xrTableCell290.Text = "xrTableCell290";
            this.xrTableCell290.Weight = 0.39019578993209519D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.BackColor = System.Drawing.Color.White;
            this.xrTableCell291.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell291.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressConfirmation.Address1")});
            this.xrTableCell291.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell291.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.StylePriority.UseBackColor = false;
            this.xrTableCell291.StylePriority.UseBorderColor = false;
            this.xrTableCell291.StylePriority.UseFont = false;
            this.xrTableCell291.StylePriority.UseForeColor = false;
            this.xrTableCell291.Text = "xrTableCell291";
            this.xrTableCell291.Weight = 0.5852585419686267D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.BackColor = System.Drawing.Color.White;
            this.xrTableCell292.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell292.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressConfirmation.Address2")});
            this.xrTableCell292.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell292.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell292.Multiline = true;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.StylePriority.UseBackColor = false;
            this.xrTableCell292.StylePriority.UseBorderColor = false;
            this.xrTableCell292.StylePriority.UseFont = false;
            this.xrTableCell292.StylePriority.UseForeColor = false;
            this.xrTableCell292.StylePriority.UseTextAlignment = false;
            this.xrTableCell292.Text = "xrTableCell292";
            this.xrTableCell292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell292.Weight = 0.56459914277617385D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.BackColor = System.Drawing.Color.White;
            this.xrTableCell293.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell293.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressConfirmation.Address3")});
            this.xrTableCell293.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell293.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseBackColor = false;
            this.xrTableCell293.StylePriority.UseBorderColor = false;
            this.xrTableCell293.StylePriority.UseFont = false;
            this.xrTableCell293.StylePriority.UseForeColor = false;
            this.xrTableCell293.Text = "xrTableCell293";
            this.xrTableCell293.Weight = 0.56575160598395691D;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.BackColor = System.Drawing.Color.White;
            this.xrTableCell294.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell294.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressConfirmation.Address4")});
            this.xrTableCell294.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell294.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.StylePriority.UseBackColor = false;
            this.xrTableCell294.StylePriority.UseBorderColor = false;
            this.xrTableCell294.StylePriority.UseFont = false;
            this.xrTableCell294.StylePriority.UseForeColor = false;
            this.xrTableCell294.Text = "xrTableCell294";
            this.xrTableCell294.Weight = 0.5187573064922697D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.BackColor = System.Drawing.Color.White;
            this.xrTableCell295.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell295.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressConfirmation.DateConfirmed")});
            this.xrTableCell295.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell295.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBackColor = false;
            this.xrTableCell295.StylePriority.UseBorderColor = false;
            this.xrTableCell295.StylePriority.UseFont = false;
            this.xrTableCell295.StylePriority.UseForeColor = false;
            this.xrTableCell295.Text = "xrTableCell295";
            this.xrTableCell295.Weight = 0.37923983593930694D;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader2});
            this.DetailReport2.DataMember = "ConsumerContactConfirmation";
            this.DetailReport2.Level = 0;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable60});
            this.Detail3.HeightF = 22.70839F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable60
            // 
            this.xrTable60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable60.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable60.ForeColor = System.Drawing.Color.Gray;
            this.xrTable60.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable60.Name = "xrTable60";
            this.xrTable60.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow116});
            this.xrTable60.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable60.StylePriority.UseBackColor = false;
            this.xrTable60.StylePriority.UseBorders = false;
            this.xrTable60.StylePriority.UseFont = false;
            this.xrTable60.StylePriority.UseForeColor = false;
            this.xrTable60.StylePriority.UseTextAlignment = false;
            this.xrTable60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow116
            // 
            this.xrTableRow116.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell220,
            this.xrTableCell296,
            this.xrTableCell297});
            this.xrTableRow116.Name = "xrTableRow116";
            this.xrTableRow116.Weight = 0.400000244140774D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.BackColor = System.Drawing.Color.White;
            this.xrTableCell148.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell148.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerContactConfirmation.LandLineNumber")});
            this.xrTableCell148.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell148.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBackColor = false;
            this.xrTableCell148.StylePriority.UseBorderColor = false;
            this.xrTableCell148.StylePriority.UseFont = false;
            this.xrTableCell148.StylePriority.UseForeColor = false;
            this.xrTableCell148.Text = "xrTableCell148";
            this.xrTableCell148.Weight = 0.58807525001145167D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.BackColor = System.Drawing.Color.White;
            this.xrTableCell220.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell220.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerContactConfirmation.LandLineNumberConfirmDate")});
            this.xrTableCell220.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell220.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseBackColor = false;
            this.xrTableCell220.StylePriority.UseBorderColor = false;
            this.xrTableCell220.StylePriority.UseFont = false;
            this.xrTableCell220.StylePriority.UseForeColor = false;
            this.xrTableCell220.Text = "xrTableCell220";
            this.xrTableCell220.Weight = 0.8967586807624367D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.BackColor = System.Drawing.Color.White;
            this.xrTableCell296.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell296.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerContactConfirmation.CellNumber")});
            this.xrTableCell296.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell296.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell296.Multiline = true;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBackColor = false;
            this.xrTableCell296.StylePriority.UseBorderColor = false;
            this.xrTableCell296.StylePriority.UseFont = false;
            this.xrTableCell296.StylePriority.UseForeColor = false;
            this.xrTableCell296.StylePriority.UseTextAlignment = false;
            this.xrTableCell296.Text = "xrTableCell296";
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell296.Weight = 0.62097106062022667D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.BackColor = System.Drawing.Color.White;
            this.xrTableCell297.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell297.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerContactConfirmation.CellNoConfirmDate")});
            this.xrTableCell297.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell297.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseBackColor = false;
            this.xrTableCell297.StylePriority.UseBorderColor = false;
            this.xrTableCell297.StylePriority.UseFont = false;
            this.xrTableCell297.StylePriority.UseForeColor = false;
            this.xrTableCell297.Text = "xrTableCell297";
            this.xrTableCell297.Weight = 0.89799723169831425D;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17});
            this.GroupHeader2.HeightF = 55.41675F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrTable17
            // 
            this.xrTable17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable17.ForeColor = System.Drawing.Color.Gray;
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 9.999974F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41,
            this.xrTableRow59});
            this.xrTable17.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable17.StylePriority.UseBackColor = false;
            this.xrTable17.StylePriority.UseBorders = false;
            this.xrTable17.StylePriority.UseFont = false;
            this.xrTable17.StylePriority.UseForeColor = false;
            this.xrTable17.StylePriority.UseTextAlignment = false;
            this.xrTable17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell107});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.400000244140774D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.BackColor = System.Drawing.Color.White;
            this.xrTableCell107.BorderColor = System.Drawing.Color.White;
            this.xrTableCell107.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell107.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell107.Multiline = true;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBackColor = false;
            this.xrTableCell107.StylePriority.UseBorderColor = false;
            this.xrTableCell107.StylePriority.UseFont = false;
            this.xrTableCell107.StylePriority.UseForeColor = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "Confirmed Contact No. History";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell107.Weight = 3.0038022230924293D;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144,
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell149});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 0.400000244140774D;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell144.BorderColor = System.Drawing.Color.White;
            this.xrTableCell144.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell144.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBackColor = false;
            this.xrTableCell144.StylePriority.UseBorderColor = false;
            this.xrTableCell144.StylePriority.UseFont = false;
            this.xrTableCell144.StylePriority.UseForeColor = false;
            this.xrTableCell144.Text = "Land Line Number";
            this.xrTableCell144.Weight = 0.58807525001145167D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell145.BorderColor = System.Drawing.Color.White;
            this.xrTableCell145.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell145.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBackColor = false;
            this.xrTableCell145.StylePriority.UseBorderColor = false;
            this.xrTableCell145.StylePriority.UseFont = false;
            this.xrTableCell145.StylePriority.UseForeColor = false;
            this.xrTableCell145.Text = "Date Confirmed";
            this.xrTableCell145.Weight = 0.8967586807624367D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell146.BorderColor = System.Drawing.Color.White;
            this.xrTableCell146.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell146.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBackColor = false;
            this.xrTableCell146.StylePriority.UseBorderColor = false;
            this.xrTableCell146.StylePriority.UseFont = false;
            this.xrTableCell146.StylePriority.UseForeColor = false;
            this.xrTableCell146.StylePriority.UseTextAlignment = false;
            this.xrTableCell146.Text = "Cell Number";
            this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell146.Weight = 0.62097106062022667D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell149.BorderColor = System.Drawing.Color.White;
            this.xrTableCell149.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell149.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBackColor = false;
            this.xrTableCell149.StylePriority.UseBorderColor = false;
            this.xrTableCell149.StylePriority.UseFont = false;
            this.xrTableCell149.StylePriority.UseForeColor = false;
            this.xrTableCell149.Text = "Date Confirmed";
            this.xrTableCell149.Weight = 0.89799723169831425D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
            this.GroupHeader1.HeightF = 55.41675F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable16
            // 
            this.xrTable16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable16.ForeColor = System.Drawing.Color.Gray;
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(12.50013F, 9.999974F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38,
            this.xrTableRow40});
            this.xrTable16.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable16.StylePriority.UseBackColor = false;
            this.xrTable16.StylePriority.UseBorders = false;
            this.xrTable16.StylePriority.UseFont = false;
            this.xrTable16.StylePriority.UseForeColor = false;
            this.xrTable16.StylePriority.UseTextAlignment = false;
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.400000244140774D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.BackColor = System.Drawing.Color.White;
            this.xrTableCell100.BorderColor = System.Drawing.Color.White;
            this.xrTableCell100.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell100.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell100.Multiline = true;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBackColor = false;
            this.xrTableCell100.StylePriority.UseBorderColor = false;
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.StylePriority.UseForeColor = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "Confirmed Address History\r\n";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell100.Weight = 3.0038022230924293D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell108,
            this.xrTableCell142});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.400000244140774D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell103.BorderColor = System.Drawing.Color.White;
            this.xrTableCell103.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell103.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBackColor = false;
            this.xrTableCell103.StylePriority.UseBorderColor = false;
            this.xrTableCell103.StylePriority.UseFont = false;
            this.xrTableCell103.StylePriority.UseForeColor = false;
            this.xrTableCell103.Text = "Type";
            this.xrTableCell103.Weight = 0.39019578993209519D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell104.BorderColor = System.Drawing.Color.White;
            this.xrTableCell104.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell104.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBackColor = false;
            this.xrTableCell104.StylePriority.UseBorderColor = false;
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.StylePriority.UseForeColor = false;
            this.xrTableCell104.Text = "Line 1";
            this.xrTableCell104.Weight = 0.5852585419686267D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell105.BorderColor = System.Drawing.Color.White;
            this.xrTableCell105.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell105.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell105.Multiline = true;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBackColor = false;
            this.xrTableCell105.StylePriority.UseBorderColor = false;
            this.xrTableCell105.StylePriority.UseFont = false;
            this.xrTableCell105.StylePriority.UseForeColor = false;
            this.xrTableCell105.StylePriority.UseTextAlignment = false;
            this.xrTableCell105.Text = "Line 2";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell105.Weight = 0.56459914277617385D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell106.BorderColor = System.Drawing.Color.White;
            this.xrTableCell106.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell106.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBackColor = false;
            this.xrTableCell106.StylePriority.UseBorderColor = false;
            this.xrTableCell106.StylePriority.UseFont = false;
            this.xrTableCell106.StylePriority.UseForeColor = false;
            this.xrTableCell106.Text = "Line 3";
            this.xrTableCell106.Weight = 0.56575160598395691D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell108.BorderColor = System.Drawing.Color.White;
            this.xrTableCell108.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell108.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBackColor = false;
            this.xrTableCell108.StylePriority.UseBorderColor = false;
            this.xrTableCell108.StylePriority.UseFont = false;
            this.xrTableCell108.StylePriority.UseForeColor = false;
            this.xrTableCell108.Text = "Line 4";
            this.xrTableCell108.Weight = 0.5187573064922697D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell142.BorderColor = System.Drawing.Color.White;
            this.xrTableCell142.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell142.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBackColor = false;
            this.xrTableCell142.StylePriority.UseBorderColor = false;
            this.xrTableCell142.StylePriority.UseFont = false;
            this.xrTableCell142.StylePriority.UseForeColor = false;
            this.xrTableCell142.Text = "Date Confirmed";
            this.xrTableCell142.Weight = 0.37923983593930694D;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader3});
            this.DetailReport1.DataMember = "ConsumerEmploymentConfirmation";
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable59});
            this.Detail2.HeightF = 22.70839F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable59
            // 
            this.xrTable59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable59.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable59.ForeColor = System.Drawing.Color.Gray;
            this.xrTable59.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable59.Name = "xrTable59";
            this.xrTable59.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow115});
            this.xrTable59.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable59.StylePriority.UseBackColor = false;
            this.xrTable59.StylePriority.UseBorders = false;
            this.xrTable59.StylePriority.UseFont = false;
            this.xrTable59.StylePriority.UseForeColor = false;
            this.xrTable59.StylePriority.UseTextAlignment = false;
            this.xrTable59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow115
            // 
            this.xrTableRow115.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell226,
            this.xrTableCell289});
            this.xrTableRow115.Name = "xrTableRow115";
            this.xrTableRow115.Weight = 0.400000244140774D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.BackColor = System.Drawing.Color.White;
            this.xrTableCell225.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell225.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentConfirmation.EmployerDetail")});
            this.xrTableCell225.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell225.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBackColor = false;
            this.xrTableCell225.StylePriority.UseBorderColor = false;
            this.xrTableCell225.StylePriority.UseFont = false;
            this.xrTableCell225.StylePriority.UseForeColor = false;
            this.xrTableCell225.Text = "xrTableCell225";
            this.xrTableCell225.Weight = 1.0163640047571105D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.BackColor = System.Drawing.Color.White;
            this.xrTableCell226.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell226.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentConfirmation.Designation")});
            this.xrTableCell226.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell226.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBackColor = false;
            this.xrTableCell226.StylePriority.UseBorderColor = false;
            this.xrTableCell226.StylePriority.UseFont = false;
            this.xrTableCell226.StylePriority.UseForeColor = false;
            this.xrTableCell226.Text = "xrTableCell226";
            this.xrTableCell226.Weight = 1.2555636544541322D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.BackColor = System.Drawing.Color.White;
            this.xrTableCell289.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell289.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentConfirmation.DateConfirmed")});
            this.xrTableCell289.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell289.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseBackColor = false;
            this.xrTableCell289.StylePriority.UseBorderColor = false;
            this.xrTableCell289.StylePriority.UseFont = false;
            this.xrTableCell289.StylePriority.UseForeColor = false;
            this.xrTableCell289.Text = "xrTableCell289";
            this.xrTableCell289.Weight = 0.73187456388118677D;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable30});
            this.GroupHeader3.HeightF = 55.41675F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrTable30
            // 
            this.xrTable30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable30.ForeColor = System.Drawing.Color.Gray;
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(12.50013F, 9.999974F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow111,
            this.xrTableRow112});
            this.xrTable30.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable30.StylePriority.UseBackColor = false;
            this.xrTable30.StylePriority.UseBorders = false;
            this.xrTable30.StylePriority.UseFont = false;
            this.xrTable30.StylePriority.UseForeColor = false;
            this.xrTable30.StylePriority.UseTextAlignment = false;
            this.xrTable30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow111
            // 
            this.xrTableRow111.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150});
            this.xrTableRow111.Name = "xrTableRow111";
            this.xrTableRow111.Weight = 0.400000244140774D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.BackColor = System.Drawing.Color.White;
            this.xrTableCell150.BorderColor = System.Drawing.Color.White;
            this.xrTableCell150.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell150.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell150.Multiline = true;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBackColor = false;
            this.xrTableCell150.StylePriority.UseBorderColor = false;
            this.xrTableCell150.StylePriority.UseFont = false;
            this.xrTableCell150.StylePriority.UseForeColor = false;
            this.xrTableCell150.StylePriority.UseTextAlignment = false;
            this.xrTableCell150.Text = "Confirmed Employment History";
            this.xrTableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell150.Weight = 3.0038022230924293D;
            // 
            // xrTableRow112
            // 
            this.xrTableRow112.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell219,
            this.xrTableCell288});
            this.xrTableRow112.Name = "xrTableRow112";
            this.xrTableRow112.Weight = 0.400000244140774D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell151.BorderColor = System.Drawing.Color.White;
            this.xrTableCell151.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell151.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBackColor = false;
            this.xrTableCell151.StylePriority.UseBorderColor = false;
            this.xrTableCell151.StylePriority.UseFont = false;
            this.xrTableCell151.StylePriority.UseForeColor = false;
            this.xrTableCell151.Text = "Employer";
            this.xrTableCell151.Weight = 1.0163640047571105D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell219.BorderColor = System.Drawing.Color.White;
            this.xrTableCell219.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell219.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseBackColor = false;
            this.xrTableCell219.StylePriority.UseBorderColor = false;
            this.xrTableCell219.StylePriority.UseFont = false;
            this.xrTableCell219.StylePriority.UseForeColor = false;
            this.xrTableCell219.Text = "Designation";
            this.xrTableCell219.Weight = 1.2555636544541322D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell288.BorderColor = System.Drawing.Color.White;
            this.xrTableCell288.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell288.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBackColor = false;
            this.xrTableCell288.StylePriority.UseBorderColor = false;
            this.xrTableCell288.StylePriority.UseFont = false;
            this.xrTableCell288.StylePriority.UseForeColor = false;
            this.xrTableCell288.Text = "Date Confirmed";
            this.xrTableCell288.Weight = 0.73187456388118677D;
            // 
            // TelephoneLinkages
            // 
            this.TelephoneLinkages.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailTelephoneLinkages,
            this.GroupHeaderTelephoneLinkages,
            this.DetailReport3,
            this.DetailReport6,
            this.DetailReport7});
            this.TelephoneLinkages.Level = 14;
            this.TelephoneLinkages.Name = "TelephoneLinkages";
            // 
            // DetailTelephoneLinkages
            // 
            this.DetailTelephoneLinkages.HeightF = 0.8333842F;
            this.DetailTelephoneLinkages.Name = "DetailTelephoneLinkages";
            this.DetailTelephoneLinkages.Visible = false;
            // 
            // GroupHeaderTelephoneLinkages
            // 
            this.GroupHeaderTelephoneLinkages.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable48});
            this.GroupHeaderTelephoneLinkages.HeightF = 65.41678F;
            this.GroupHeaderTelephoneLinkages.Name = "GroupHeaderTelephoneLinkages";
            // 
            // xrTable48
            // 
            this.xrTable48.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable48.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable48.ForeColor = System.Drawing.Color.Gray;
            this.xrTable48.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 12.5F);
            this.xrTable48.Name = "xrTable48";
            this.xrTable48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable48.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow90,
            this.xrTableRow91});
            this.xrTable48.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable48.StylePriority.UseBorders = false;
            this.xrTable48.StylePriority.UseFont = false;
            this.xrTable48.StylePriority.UseForeColor = false;
            this.xrTable48.StylePriority.UsePadding = false;
            this.xrTable48.StylePriority.UseTextAlignment = false;
            this.xrTable48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell229});
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.Weight = 0.8D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.BackColor = System.Drawing.Color.White;
            this.xrTableCell229.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell229.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell229.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell229.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell229.StylePriority.UseBackColor = false;
            this.xrTableCell229.StylePriority.UseBorderColor = false;
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.StylePriority.UseFont = false;
            this.xrTableCell229.StylePriority.UseForeColor = false;
            this.xrTableCell229.StylePriority.UsePadding = false;
            this.xrTableCell229.Text = "Telephone Linkages";
            this.xrTableCell229.Weight = 3D;
            // 
            // xrTableRow91
            // 
            this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230});
            this.xrTableRow91.Name = "xrTableRow91";
            this.xrTableRow91.Weight = 0.53333333333333344D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell230.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell230.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell230.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell230.Multiline = true;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell230.StylePriority.UseBorderColor = false;
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.StylePriority.UseFont = false;
            this.xrTableCell230.StylePriority.UseForeColor = false;
            this.xrTableCell230.StylePriority.UsePadding = false;
            this.xrTableCell230.Text = "This section displays additional contact information, whereby XDS match that your" +
    " contact numbers are linked to other individuals. ";
            this.xrTableCell230.Weight = 3D;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader4});
            this.DetailReport3.DataMember = "ConsumerTelephoneLinkageHome";
            this.DetailReport3.Level = 0;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable62});
            this.Detail4.HeightF = 22.70839F;
            this.Detail4.Name = "Detail4";
            // 
            // xrTable62
            // 
            this.xrTable62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable62.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable62.ForeColor = System.Drawing.Color.Gray;
            this.xrTable62.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable62.Name = "xrTable62";
            this.xrTable62.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow118});
            this.xrTable62.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable62.StylePriority.UseBackColor = false;
            this.xrTable62.StylePriority.UseBorders = false;
            this.xrTable62.StylePriority.UseFont = false;
            this.xrTable62.StylePriority.UseForeColor = false;
            this.xrTable62.StylePriority.UseTextAlignment = false;
            this.xrTable62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow118
            // 
            this.xrTableRow118.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306,
            this.xrTableCell307,
            this.xrTableCell308});
            this.xrTableRow118.Name = "xrTableRow118";
            this.xrTableRow118.Weight = 0.400000244140774D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.BackColor = System.Drawing.Color.White;
            this.xrTableCell304.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell304.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.ConsumerName")});
            this.xrTableCell304.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell304.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBackColor = false;
            this.xrTableCell304.StylePriority.UseBorderColor = false;
            this.xrTableCell304.StylePriority.UseFont = false;
            this.xrTableCell304.StylePriority.UseForeColor = false;
            this.xrTableCell304.Text = "xrTableCell304";
            this.xrTableCell304.Weight = 0.52744380739533181D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.BackColor = System.Drawing.Color.White;
            this.xrTableCell305.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell305.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.Surname")});
            this.xrTableCell305.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell305.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBackColor = false;
            this.xrTableCell305.StylePriority.UseBorderColor = false;
            this.xrTableCell305.StylePriority.UseFont = false;
            this.xrTableCell305.StylePriority.UseForeColor = false;
            this.xrTableCell305.Text = "xrTableCell305";
            this.xrTableCell305.Weight = 0.49510441706548469D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.BackColor = System.Drawing.Color.White;
            this.xrTableCell306.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell306.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.HomeTelephone")});
            this.xrTableCell306.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell306.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell306.Multiline = true;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBackColor = false;
            this.xrTableCell306.StylePriority.UseBorderColor = false;
            this.xrTableCell306.StylePriority.UseFont = false;
            this.xrTableCell306.StylePriority.UseForeColor = false;
            this.xrTableCell306.StylePriority.UseTextAlignment = false;
            this.xrTableCell306.Text = "xrTableCell306";
            this.xrTableCell306.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell306.Weight = 0.6028405927293693D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.BackColor = System.Drawing.Color.White;
            this.xrTableCell307.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell307.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.WorkTelephone")});
            this.xrTableCell307.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell307.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBackColor = false;
            this.xrTableCell307.StylePriority.UseBorderColor = false;
            this.xrTableCell307.StylePriority.UseFont = false;
            this.xrTableCell307.StylePriority.UseForeColor = false;
            this.xrTableCell307.Text = "xrTableCell307";
            this.xrTableCell307.Weight = 0.646539292117116D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.BackColor = System.Drawing.Color.White;
            this.xrTableCell308.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell308.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.CellularNo")});
            this.xrTableCell308.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell308.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBackColor = false;
            this.xrTableCell308.StylePriority.UseBorderColor = false;
            this.xrTableCell308.StylePriority.UseFont = false;
            this.xrTableCell308.StylePriority.UseForeColor = false;
            this.xrTableCell308.Text = "xrTableCell308";
            this.xrTableCell308.Weight = 0.73187411378512757D;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable61});
            this.GroupHeader4.HeightF = 56.25F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // xrTable61
            // 
            this.xrTable61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable61.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable61.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable61.ForeColor = System.Drawing.Color.Gray;
            this.xrTable61.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable61.Name = "xrTable61";
            this.xrTable61.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow113,
            this.xrTableRow117});
            this.xrTable61.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable61.StylePriority.UseBackColor = false;
            this.xrTable61.StylePriority.UseBorders = false;
            this.xrTable61.StylePriority.UseFont = false;
            this.xrTable61.StylePriority.UseForeColor = false;
            this.xrTable61.StylePriority.UseTextAlignment = false;
            this.xrTable61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow113
            // 
            this.xrTableRow113.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147});
            this.xrTableRow113.Name = "xrTableRow113";
            this.xrTableRow113.Weight = 0.400000244140774D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.BackColor = System.Drawing.Color.White;
            this.xrTableCell147.BorderColor = System.Drawing.Color.White;
            this.xrTableCell147.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell147.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell147.Multiline = true;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBackColor = false;
            this.xrTableCell147.StylePriority.UseBorderColor = false;
            this.xrTableCell147.StylePriority.UseFont = false;
            this.xrTableCell147.StylePriority.UseForeColor = false;
            this.xrTableCell147.StylePriority.UseTextAlignment = false;
            this.xrTableCell147.Text = "Home Linkage Information";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell147.Weight = 3.0038022230924293D;
            // 
            // xrTableRow117
            // 
            this.xrTableRow117.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell298,
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTableCell302});
            this.xrTableRow117.Name = "xrTableRow117";
            this.xrTableRow117.Weight = 0.400000244140774D;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell298.BorderColor = System.Drawing.Color.White;
            this.xrTableCell298.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell298.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.StylePriority.UseBackColor = false;
            this.xrTableCell298.StylePriority.UseBorderColor = false;
            this.xrTableCell298.StylePriority.UseFont = false;
            this.xrTableCell298.StylePriority.UseForeColor = false;
            this.xrTableCell298.Text = "Name";
            this.xrTableCell298.Weight = 0.52744380739533181D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell299.BorderColor = System.Drawing.Color.White;
            this.xrTableCell299.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell299.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBackColor = false;
            this.xrTableCell299.StylePriority.UseBorderColor = false;
            this.xrTableCell299.StylePriority.UseFont = false;
            this.xrTableCell299.StylePriority.UseForeColor = false;
            this.xrTableCell299.Text = "Surname";
            this.xrTableCell299.Weight = 0.49510441706548469D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell300.BorderColor = System.Drawing.Color.White;
            this.xrTableCell300.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell300.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell300.Multiline = true;
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseBackColor = false;
            this.xrTableCell300.StylePriority.UseBorderColor = false;
            this.xrTableCell300.StylePriority.UseFont = false;
            this.xrTableCell300.StylePriority.UseForeColor = false;
            this.xrTableCell300.StylePriority.UseTextAlignment = false;
            this.xrTableCell300.Text = "Home Number";
            this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell300.Weight = 0.6028405927293693D;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell301.BorderColor = System.Drawing.Color.White;
            this.xrTableCell301.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell301.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseBackColor = false;
            this.xrTableCell301.StylePriority.UseBorderColor = false;
            this.xrTableCell301.StylePriority.UseFont = false;
            this.xrTableCell301.StylePriority.UseForeColor = false;
            this.xrTableCell301.Text = "Work Number";
            this.xrTableCell301.Weight = 0.646539292117116D;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell302.BorderColor = System.Drawing.Color.White;
            this.xrTableCell302.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell302.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBackColor = false;
            this.xrTableCell302.StylePriority.UseBorderColor = false;
            this.xrTableCell302.StylePriority.UseFont = false;
            this.xrTableCell302.StylePriority.UseForeColor = false;
            this.xrTableCell302.Text = "Cell Number";
            this.xrTableCell302.Weight = 0.73187411378512757D;
            // 
            // DetailReport6
            // 
            this.DetailReport6.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8,
            this.GroupHeader8});
            this.DetailReport6.DataMember = "ConsumerTelephoneLinkageWork";
            this.DetailReport6.Level = 1;
            this.DetailReport6.Name = "DetailReport6";
            // 
            // Detail8
            // 
            this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable65});
            this.Detail8.HeightF = 22.70839F;
            this.Detail8.Name = "Detail8";
            // 
            // xrTable65
            // 
            this.xrTable65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable65.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable65.ForeColor = System.Drawing.Color.Gray;
            this.xrTable65.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable65.Name = "xrTable65";
            this.xrTable65.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow123});
            this.xrTable65.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable65.StylePriority.UseBackColor = false;
            this.xrTable65.StylePriority.UseBorders = false;
            this.xrTable65.StylePriority.UseFont = false;
            this.xrTable65.StylePriority.UseForeColor = false;
            this.xrTable65.StylePriority.UseTextAlignment = false;
            this.xrTable65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow123
            // 
            this.xrTableRow123.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell320,
            this.xrTableCell321,
            this.xrTableCell322,
            this.xrTableCell323,
            this.xrTableCell324});
            this.xrTableRow123.Name = "xrTableRow123";
            this.xrTableRow123.Weight = 0.400000244140774D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.BackColor = System.Drawing.Color.White;
            this.xrTableCell320.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell320.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.ConsumerName")});
            this.xrTableCell320.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell320.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.StylePriority.UseBackColor = false;
            this.xrTableCell320.StylePriority.UseBorderColor = false;
            this.xrTableCell320.StylePriority.UseFont = false;
            this.xrTableCell320.StylePriority.UseForeColor = false;
            this.xrTableCell320.Text = "xrTableCell320";
            this.xrTableCell320.Weight = 0.52744380739533181D;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.BackColor = System.Drawing.Color.White;
            this.xrTableCell321.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell321.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.Surname")});
            this.xrTableCell321.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell321.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.StylePriority.UseBackColor = false;
            this.xrTableCell321.StylePriority.UseBorderColor = false;
            this.xrTableCell321.StylePriority.UseFont = false;
            this.xrTableCell321.StylePriority.UseForeColor = false;
            this.xrTableCell321.StylePriority.UseTextAlignment = false;
            this.xrTableCell321.Text = "xrTableCell321";
            this.xrTableCell321.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell321.Weight = 0.48984294223117519D;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.BackColor = System.Drawing.Color.White;
            this.xrTableCell322.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell322.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.HomeTelephone")});
            this.xrTableCell322.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell322.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell322.Multiline = true;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBackColor = false;
            this.xrTableCell322.StylePriority.UseBorderColor = false;
            this.xrTableCell322.StylePriority.UseFont = false;
            this.xrTableCell322.StylePriority.UseForeColor = false;
            this.xrTableCell322.StylePriority.UseTextAlignment = false;
            this.xrTableCell322.Text = "xrTableCell322";
            this.xrTableCell322.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell322.Weight = 0.60810206756367879D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.BackColor = System.Drawing.Color.White;
            this.xrTableCell323.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell323.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.WorkTelephone")});
            this.xrTableCell323.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell323.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseBackColor = false;
            this.xrTableCell323.StylePriority.UseBorderColor = false;
            this.xrTableCell323.StylePriority.UseFont = false;
            this.xrTableCell323.StylePriority.UseForeColor = false;
            this.xrTableCell323.Text = "xrTableCell323";
            this.xrTableCell323.Weight = 0.64653950045522746D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.BackColor = System.Drawing.Color.White;
            this.xrTableCell324.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell324.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.CellularNo")});
            this.xrTableCell324.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell324.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.StylePriority.UseBackColor = false;
            this.xrTableCell324.StylePriority.UseBorderColor = false;
            this.xrTableCell324.StylePriority.UseFont = false;
            this.xrTableCell324.StylePriority.UseForeColor = false;
            this.xrTableCell324.Text = "xrTableCell324";
            this.xrTableCell324.Weight = 0.73187390544701614D;
            // 
            // GroupHeader8
            // 
            this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable63});
            this.GroupHeader8.HeightF = 55.41678F;
            this.GroupHeader8.Name = "GroupHeader8";
            // 
            // xrTable63
            // 
            this.xrTable63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable63.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable63.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable63.ForeColor = System.Drawing.Color.Gray;
            this.xrTable63.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable63.Name = "xrTable63";
            this.xrTable63.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow119,
            this.xrTableRow120});
            this.xrTable63.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable63.StylePriority.UseBackColor = false;
            this.xrTable63.StylePriority.UseBorders = false;
            this.xrTable63.StylePriority.UseFont = false;
            this.xrTable63.StylePriority.UseForeColor = false;
            this.xrTable63.StylePriority.UseTextAlignment = false;
            this.xrTable63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow119
            // 
            this.xrTableRow119.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell303});
            this.xrTableRow119.Name = "xrTableRow119";
            this.xrTableRow119.Weight = 0.400000244140774D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.BackColor = System.Drawing.Color.White;
            this.xrTableCell303.BorderColor = System.Drawing.Color.White;
            this.xrTableCell303.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell303.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell303.Multiline = true;
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBackColor = false;
            this.xrTableCell303.StylePriority.UseBorderColor = false;
            this.xrTableCell303.StylePriority.UseFont = false;
            this.xrTableCell303.StylePriority.UseForeColor = false;
            this.xrTableCell303.StylePriority.UseTextAlignment = false;
            this.xrTableCell303.Text = "Work Linkage Information";
            this.xrTableCell303.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell303.Weight = 3.0038022230924293D;
            // 
            // xrTableRow120
            // 
            this.xrTableRow120.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell309,
            this.xrTableCell310,
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTableCell313});
            this.xrTableRow120.Name = "xrTableRow120";
            this.xrTableRow120.Weight = 0.400000244140774D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell309.BorderColor = System.Drawing.Color.White;
            this.xrTableCell309.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell309.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBackColor = false;
            this.xrTableCell309.StylePriority.UseBorderColor = false;
            this.xrTableCell309.StylePriority.UseFont = false;
            this.xrTableCell309.StylePriority.UseForeColor = false;
            this.xrTableCell309.Text = "Name";
            this.xrTableCell309.Weight = 0.52744380739533181D;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell310.BorderColor = System.Drawing.Color.White;
            this.xrTableCell310.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell310.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBackColor = false;
            this.xrTableCell310.StylePriority.UseBorderColor = false;
            this.xrTableCell310.StylePriority.UseFont = false;
            this.xrTableCell310.StylePriority.UseForeColor = false;
            this.xrTableCell310.StylePriority.UseTextAlignment = false;
            this.xrTableCell310.Text = "Surname";
            this.xrTableCell310.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell310.Weight = 0.48984294223117519D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell311.BorderColor = System.Drawing.Color.White;
            this.xrTableCell311.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell311.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell311.Multiline = true;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBackColor = false;
            this.xrTableCell311.StylePriority.UseBorderColor = false;
            this.xrTableCell311.StylePriority.UseFont = false;
            this.xrTableCell311.StylePriority.UseForeColor = false;
            this.xrTableCell311.StylePriority.UseTextAlignment = false;
            this.xrTableCell311.Text = "Home Number";
            this.xrTableCell311.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell311.Weight = 0.60810206756367879D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell312.BorderColor = System.Drawing.Color.White;
            this.xrTableCell312.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell312.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBackColor = false;
            this.xrTableCell312.StylePriority.UseBorderColor = false;
            this.xrTableCell312.StylePriority.UseFont = false;
            this.xrTableCell312.StylePriority.UseForeColor = false;
            this.xrTableCell312.Text = "Work Number";
            this.xrTableCell312.Weight = 0.64653950045522746D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell313.BorderColor = System.Drawing.Color.White;
            this.xrTableCell313.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell313.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.StylePriority.UseBackColor = false;
            this.xrTableCell313.StylePriority.UseBorderColor = false;
            this.xrTableCell313.StylePriority.UseFont = false;
            this.xrTableCell313.StylePriority.UseForeColor = false;
            this.xrTableCell313.Text = "Cell Number";
            this.xrTableCell313.Weight = 0.73187390544701614D;
            // 
            // DetailReport7
            // 
            this.DetailReport7.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader9});
            this.DetailReport7.DataMember = "ConsumerTelephoneLinkageCellular";
            this.DetailReport7.Level = 2;
            this.DetailReport7.Name = "DetailReport7";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable66});
            this.Detail9.HeightF = 23.95833F;
            this.Detail9.Name = "Detail9";
            // 
            // xrTable66
            // 
            this.xrTable66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable66.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable66.ForeColor = System.Drawing.Color.Gray;
            this.xrTable66.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable66.Name = "xrTable66";
            this.xrTable66.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow124});
            this.xrTable66.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable66.StylePriority.UseBackColor = false;
            this.xrTable66.StylePriority.UseBorders = false;
            this.xrTable66.StylePriority.UseFont = false;
            this.xrTable66.StylePriority.UseForeColor = false;
            this.xrTable66.StylePriority.UseTextAlignment = false;
            this.xrTable66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow124
            // 
            this.xrTableRow124.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell325,
            this.xrTableCell326,
            this.xrTableCell327,
            this.xrTableCell328,
            this.xrTableCell329});
            this.xrTableRow124.Name = "xrTableRow124";
            this.xrTableRow124.Weight = 0.400000244140774D;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.BackColor = System.Drawing.Color.White;
            this.xrTableCell325.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell325.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.ConsumerName")});
            this.xrTableCell325.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell325.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.StylePriority.UseBackColor = false;
            this.xrTableCell325.StylePriority.UseBorderColor = false;
            this.xrTableCell325.StylePriority.UseFont = false;
            this.xrTableCell325.StylePriority.UseForeColor = false;
            this.xrTableCell325.Text = "xrTableCell325";
            this.xrTableCell325.Weight = 0.52744385947985961D;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.BackColor = System.Drawing.Color.White;
            this.xrTableCell326.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell326.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.Surname")});
            this.xrTableCell326.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell326.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.StylePriority.UseBackColor = false;
            this.xrTableCell326.StylePriority.UseBorderColor = false;
            this.xrTableCell326.StylePriority.UseFont = false;
            this.xrTableCell326.StylePriority.UseForeColor = false;
            this.xrTableCell326.Text = "xrTableCell326";
            this.xrTableCell326.Weight = 0.49510436498095689D;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.BackColor = System.Drawing.Color.White;
            this.xrTableCell327.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell327.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.HomeTelephone")});
            this.xrTableCell327.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell327.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell327.Multiline = true;
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.StylePriority.UseBackColor = false;
            this.xrTableCell327.StylePriority.UseBorderColor = false;
            this.xrTableCell327.StylePriority.UseFont = false;
            this.xrTableCell327.StylePriority.UseForeColor = false;
            this.xrTableCell327.StylePriority.UseTextAlignment = false;
            this.xrTableCell327.Text = "xrTableCell327";
            this.xrTableCell327.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell327.Weight = 0.6028405927293693D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.BackColor = System.Drawing.Color.White;
            this.xrTableCell328.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell328.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.WorkTelephone")});
            this.xrTableCell328.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell328.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.StylePriority.UseBackColor = false;
            this.xrTableCell328.StylePriority.UseBorderColor = false;
            this.xrTableCell328.StylePriority.UseFont = false;
            this.xrTableCell328.StylePriority.UseForeColor = false;
            this.xrTableCell328.Text = "xrTableCell328";
            this.xrTableCell328.Weight = 0.64653950045522746D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.BackColor = System.Drawing.Color.White;
            this.xrTableCell329.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell329.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.CellularNo")});
            this.xrTableCell329.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell329.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBackColor = false;
            this.xrTableCell329.StylePriority.UseBorderColor = false;
            this.xrTableCell329.StylePriority.UseFont = false;
            this.xrTableCell329.StylePriority.UseForeColor = false;
            this.xrTableCell329.Text = "xrTableCell329";
            this.xrTableCell329.Weight = 0.73187390544701614D;
            // 
            // GroupHeader9
            // 
            this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable64});
            this.GroupHeader9.HeightF = 55.41678F;
            this.GroupHeader9.Name = "GroupHeader9";
            // 
            // xrTable64
            // 
            this.xrTable64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable64.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable64.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable64.ForeColor = System.Drawing.Color.Gray;
            this.xrTable64.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable64.Name = "xrTable64";
            this.xrTable64.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow121,
            this.xrTableRow122});
            this.xrTable64.SizeF = new System.Drawing.SizeF(880F, 45.41678F);
            this.xrTable64.StylePriority.UseBackColor = false;
            this.xrTable64.StylePriority.UseBorders = false;
            this.xrTable64.StylePriority.UseFont = false;
            this.xrTable64.StylePriority.UseForeColor = false;
            this.xrTable64.StylePriority.UseTextAlignment = false;
            this.xrTable64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow121
            // 
            this.xrTableRow121.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell314});
            this.xrTableRow121.Name = "xrTableRow121";
            this.xrTableRow121.Weight = 0.400000244140774D;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.BackColor = System.Drawing.Color.White;
            this.xrTableCell314.BorderColor = System.Drawing.Color.White;
            this.xrTableCell314.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrTableCell314.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell314.Multiline = true;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.StylePriority.UseBackColor = false;
            this.xrTableCell314.StylePriority.UseBorderColor = false;
            this.xrTableCell314.StylePriority.UseFont = false;
            this.xrTableCell314.StylePriority.UseForeColor = false;
            this.xrTableCell314.StylePriority.UseTextAlignment = false;
            this.xrTableCell314.Text = "Cellular Linkage Information";
            this.xrTableCell314.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell314.Weight = 3.0038022230924293D;
            // 
            // xrTableRow122
            // 
            this.xrTableRow122.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell315,
            this.xrTableCell316,
            this.xrTableCell317,
            this.xrTableCell318,
            this.xrTableCell319});
            this.xrTableRow122.Name = "xrTableRow122";
            this.xrTableRow122.Weight = 0.400000244140774D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell315.BorderColor = System.Drawing.Color.White;
            this.xrTableCell315.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell315.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.StylePriority.UseBackColor = false;
            this.xrTableCell315.StylePriority.UseBorderColor = false;
            this.xrTableCell315.StylePriority.UseFont = false;
            this.xrTableCell315.StylePriority.UseForeColor = false;
            this.xrTableCell315.Text = "Name";
            this.xrTableCell315.Weight = 0.52744385947985961D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell316.BorderColor = System.Drawing.Color.White;
            this.xrTableCell316.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell316.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.StylePriority.UseBackColor = false;
            this.xrTableCell316.StylePriority.UseBorderColor = false;
            this.xrTableCell316.StylePriority.UseFont = false;
            this.xrTableCell316.StylePriority.UseForeColor = false;
            this.xrTableCell316.Text = "Surname";
            this.xrTableCell316.Weight = 0.49510436498095689D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell317.BorderColor = System.Drawing.Color.White;
            this.xrTableCell317.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell317.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell317.Multiline = true;
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.StylePriority.UseBackColor = false;
            this.xrTableCell317.StylePriority.UseBorderColor = false;
            this.xrTableCell317.StylePriority.UseFont = false;
            this.xrTableCell317.StylePriority.UseForeColor = false;
            this.xrTableCell317.StylePriority.UseTextAlignment = false;
            this.xrTableCell317.Text = "Home Number";
            this.xrTableCell317.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell317.Weight = 0.6028405927293693D;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell318.BorderColor = System.Drawing.Color.White;
            this.xrTableCell318.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell318.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBackColor = false;
            this.xrTableCell318.StylePriority.UseBorderColor = false;
            this.xrTableCell318.StylePriority.UseFont = false;
            this.xrTableCell318.StylePriority.UseForeColor = false;
            this.xrTableCell318.Text = "Work Number";
            this.xrTableCell318.Weight = 0.64653950045522746D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell319.BorderColor = System.Drawing.Color.White;
            this.xrTableCell319.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell319.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBackColor = false;
            this.xrTableCell319.StylePriority.UseBorderColor = false;
            this.xrTableCell319.StylePriority.UseFont = false;
            this.xrTableCell319.StylePriority.UseForeColor = false;
            this.xrTableCell319.Text = "Cell Number";
            this.xrTableCell319.Weight = 0.73187390544701614D;
            // 
            // EmploymentHistory
            // 
            this.EmploymentHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailEmploymentHistory,
            this.GroupHeaderEmploymentHistory});
            this.EmploymentHistory.DataMember = "ConsumerEmploymentHistory";
            this.EmploymentHistory.Level = 15;
            this.EmploymentHistory.Name = "EmploymentHistory";
            // 
            // DetailEmploymentHistory
            // 
            this.DetailEmploymentHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable53});
            this.DetailEmploymentHistory.HeightF = 22.70839F;
            this.DetailEmploymentHistory.Name = "DetailEmploymentHistory";
            // 
            // xrTable53
            // 
            this.xrTable53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable53.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable53.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable53.ForeColor = System.Drawing.Color.Gray;
            this.xrTable53.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable53.Name = "xrTable53";
            this.xrTable53.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow99});
            this.xrTable53.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable53.StylePriority.UseBackColor = false;
            this.xrTable53.StylePriority.UseBorders = false;
            this.xrTable53.StylePriority.UseFont = false;
            this.xrTable53.StylePriority.UseForeColor = false;
            this.xrTable53.StylePriority.UseTextAlignment = false;
            this.xrTable53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow99
            // 
            this.xrTableRow99.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238,
            this.xrTableCell242,
            this.xrTableCell243});
            this.xrTableRow99.Name = "xrTableRow99";
            this.xrTableRow99.Weight = 0.400000244140774D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.BackColor = System.Drawing.Color.White;
            this.xrTableCell238.BorderColor = System.Drawing.Color.White;
            this.xrTableCell238.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.LastUpdatedDate")});
            this.xrTableCell238.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell238.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBackColor = false;
            this.xrTableCell238.StylePriority.UseBorderColor = false;
            this.xrTableCell238.StylePriority.UseFont = false;
            this.xrTableCell238.StylePriority.UseForeColor = false;
            this.xrTableCell238.Text = "xrTableCell238";
            this.xrTableCell238.Weight = 0.76446236822310754D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.BackColor = System.Drawing.Color.White;
            this.xrTableCell242.BorderColor = System.Drawing.Color.White;
            this.xrTableCell242.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.EmployerDetail")});
            this.xrTableCell242.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell242.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBackColor = false;
            this.xrTableCell242.StylePriority.UseBorderColor = false;
            this.xrTableCell242.StylePriority.UseFont = false;
            this.xrTableCell242.StylePriority.UseForeColor = false;
            this.xrTableCell242.Text = "xrTableCell242";
            this.xrTableCell242.Weight = 1.1928829200475064D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.BackColor = System.Drawing.Color.White;
            this.xrTableCell243.BorderColor = System.Drawing.Color.White;
            this.xrTableCell243.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.Designation")});
            this.xrTableCell243.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell243.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell243.Multiline = true;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBackColor = false;
            this.xrTableCell243.StylePriority.UseBorderColor = false;
            this.xrTableCell243.StylePriority.UseFont = false;
            this.xrTableCell243.StylePriority.UseForeColor = false;
            this.xrTableCell243.StylePriority.UseTextAlignment = false;
            this.xrTableCell243.Text = "xrTableCell243";
            this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell243.Weight = 1.0464569348218156D;
            // 
            // GroupHeaderEmploymentHistory
            // 
            this.GroupHeaderEmploymentHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable52,
            this.xrTable49});
            this.GroupHeaderEmploymentHistory.HeightF = 94.79166F;
            this.GroupHeaderEmploymentHistory.Name = "GroupHeaderEmploymentHistory";
            // 
            // xrTable52
            // 
            this.xrTable52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTable52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable52.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTable52.ForeColor = System.Drawing.Color.Gray;
            this.xrTable52.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 68.20827F);
            this.xrTable52.Name = "xrTable52";
            this.xrTable52.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow100});
            this.xrTable52.SizeF = new System.Drawing.SizeF(880F, 22.70839F);
            this.xrTable52.StylePriority.UseBackColor = false;
            this.xrTable52.StylePriority.UseBorders = false;
            this.xrTable52.StylePriority.UseFont = false;
            this.xrTable52.StylePriority.UseForeColor = false;
            this.xrTable52.StylePriority.UseTextAlignment = false;
            this.xrTable52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow100
            // 
            this.xrTableRow100.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTableCell241});
            this.xrTableRow100.Name = "xrTableRow100";
            this.xrTableRow100.Weight = 0.400000244140774D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell239.BorderColor = System.Drawing.Color.White;
            this.xrTableCell239.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell239.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBackColor = false;
            this.xrTableCell239.StylePriority.UseBorderColor = false;
            this.xrTableCell239.StylePriority.UseFont = false;
            this.xrTableCell239.StylePriority.UseForeColor = false;
            this.xrTableCell239.Text = "Bureau Update";
            this.xrTableCell239.Weight = 0.76446236822310754D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell240.BorderColor = System.Drawing.Color.White;
            this.xrTableCell240.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell240.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseBackColor = false;
            this.xrTableCell240.StylePriority.UseBorderColor = false;
            this.xrTableCell240.StylePriority.UseFont = false;
            this.xrTableCell240.StylePriority.UseForeColor = false;
            this.xrTableCell240.Text = "Employer ";
            this.xrTableCell240.Weight = 1.1928829200475064D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell241.BorderColor = System.Drawing.Color.White;
            this.xrTableCell241.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell241.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell241.Multiline = true;
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBackColor = false;
            this.xrTableCell241.StylePriority.UseBorderColor = false;
            this.xrTableCell241.StylePriority.UseFont = false;
            this.xrTableCell241.StylePriority.UseForeColor = false;
            this.xrTableCell241.StylePriority.UseTextAlignment = false;
            this.xrTableCell241.Text = "Designation ";
            this.xrTableCell241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell241.Weight = 1.0464569348218156D;
            // 
            // xrTable49
            // 
            this.xrTable49.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable49.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable49.ForeColor = System.Drawing.Color.Gray;
            this.xrTable49.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 12.5F);
            this.xrTable49.Name = "xrTable49";
            this.xrTable49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable49.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow93,
            this.xrTableRow94});
            this.xrTable49.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable49.StylePriority.UseBorders = false;
            this.xrTable49.StylePriority.UseFont = false;
            this.xrTable49.StylePriority.UseForeColor = false;
            this.xrTable49.StylePriority.UsePadding = false;
            this.xrTable49.StylePriority.UseTextAlignment = false;
            this.xrTable49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow93
            // 
            this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell232});
            this.xrTableRow93.Name = "xrTableRow93";
            this.xrTableRow93.Weight = 0.8D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.BackColor = System.Drawing.Color.White;
            this.xrTableCell232.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell232.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell232.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell232.StylePriority.UseBackColor = false;
            this.xrTableCell232.StylePriority.UseBorderColor = false;
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.StylePriority.UseFont = false;
            this.xrTableCell232.StylePriority.UseForeColor = false;
            this.xrTableCell232.StylePriority.UsePadding = false;
            this.xrTableCell232.Text = "Employment History";
            this.xrTableCell232.Weight = 3D;
            // 
            // xrTableRow94
            // 
            this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233});
            this.xrTableRow94.Name = "xrTableRow94";
            this.xrTableRow94.Weight = 0.53333333333333344D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell233.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell233.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell233.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell233.Multiline = true;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell233.StylePriority.UseBorderColor = false;
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.StylePriority.UseFont = false;
            this.xrTableCell233.StylePriority.UseForeColor = false;
            this.xrTableCell233.StylePriority.UsePadding = false;
            this.xrTableCell233.Text = "This section displays a list of your Employers.";
            this.xrTableCell233.Weight = 3D;
            // 
            // PropertyInterest
            // 
            this.PropertyInterest.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailPropertyInterest,
            this.GroupHeaderPropertyInterest});
            this.PropertyInterest.DataMember = "ConsumerPropertyInformation";
            this.PropertyInterest.Level = 16;
            this.PropertyInterest.Name = "PropertyInterest";
            // 
            // DetailPropertyInterest
            // 
            this.DetailPropertyInterest.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable54});
            this.DetailPropertyInterest.HeightF = 120F;
            this.DetailPropertyInterest.Name = "DetailPropertyInterest";
            // 
            // xrTable54
            // 
            this.xrTable54.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable54.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable54.ForeColor = System.Drawing.Color.Gray;
            this.xrTable54.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable54.Name = "xrTable54";
            this.xrTable54.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable54.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow101,
            this.xrTableRow102,
            this.xrTableRow103,
            this.xrTableRow104,
            this.xrTableRow105,
            this.xrTableRow106});
            this.xrTable54.SizeF = new System.Drawing.SizeF(880F, 120F);
            this.xrTable54.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable54.StylePriority.UseBorders = false;
            this.xrTable54.StylePriority.UseFont = false;
            this.xrTable54.StylePriority.UseForeColor = false;
            this.xrTable54.StylePriority.UsePadding = false;
            this.xrTable54.StylePriority.UseTextAlignment = false;
            this.xrTable54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow101
            // 
            this.xrTableRow101.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell244,
            this.xrTableCell245,
            this.xrTableCell246,
            this.xrTableCell247});
            this.xrTableRow101.Name = "xrTableRow101";
            this.xrTableRow101.Weight = 0.79999999999999993D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell244.BorderColor = System.Drawing.Color.White;
            this.xrTableCell244.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBackColor = false;
            this.xrTableCell244.StylePriority.UseBorderColor = false;
            this.xrTableCell244.StylePriority.UseForeColor = false;
            this.xrTableCell244.Text = "Title Deed number:";
            this.xrTableCell244.Weight = 0.64556962991062583D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.BackColor = System.Drawing.Color.White;
            this.xrTableCell245.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell245.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.TitleDeedNo")});
            this.xrTableCell245.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell245.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBackColor = false;
            this.xrTableCell245.StylePriority.UseBorderColor = false;
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.StylePriority.UseFont = false;
            this.xrTableCell245.StylePriority.UseForeColor = false;
            this.xrTableCell245.Text = "xrTableCell245";
            this.xrTableCell245.Weight = 0.8544303314595284D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell246.BorderColor = System.Drawing.Color.White;
            this.xrTableCell246.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell246.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBackColor = false;
            this.xrTableCell246.StylePriority.UseBorderColor = false;
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.StylePriority.UseForeColor = false;
            this.xrTableCell246.Text = "Erf/Site No.:";
            this.xrTableCell246.Weight = 0.64556963956808744D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.BackColor = System.Drawing.Color.White;
            this.xrTableCell247.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell247.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfNo")});
            this.xrTableCell247.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell247.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBackColor = false;
            this.xrTableCell247.StylePriority.UseBorderColor = false;
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.StylePriority.UseFont = false;
            this.xrTableCell247.StylePriority.UseForeColor = false;
            this.xrTableCell247.Text = "xrTableCell247";
            this.xrTableCell247.Weight = 0.85443039906175833D;
            // 
            // xrTableRow102
            // 
            this.xrTableRow102.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250,
            this.xrTableCell251});
            this.xrTableRow102.Name = "xrTableRow102";
            this.xrTableRow102.Weight = 0.79999999999999993D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell248.BorderColor = System.Drawing.Color.White;
            this.xrTableCell248.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBackColor = false;
            this.xrTableCell248.StylePriority.UseBorderColor = false;
            this.xrTableCell248.StylePriority.UseForeColor = false;
            this.xrTableCell248.Text = "Deeds Office: ";
            this.xrTableCell248.Weight = 0.64556962991062583D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.BackColor = System.Drawing.Color.White;
            this.xrTableCell249.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell249.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.DeedsOffice")});
            this.xrTableCell249.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell249.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBackColor = false;
            this.xrTableCell249.StylePriority.UseBorderColor = false;
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.StylePriority.UseFont = false;
            this.xrTableCell249.StylePriority.UseForeColor = false;
            this.xrTableCell249.Text = "xrTableCell249";
            this.xrTableCell249.Weight = 0.8544303314595284D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell250.BorderColor = System.Drawing.Color.White;
            this.xrTableCell250.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBackColor = false;
            this.xrTableCell250.StylePriority.UseBorderColor = false;
            this.xrTableCell250.StylePriority.UseForeColor = false;
            this.xrTableCell250.Text = "Physical Address:";
            this.xrTableCell250.Weight = 0.64556963956808744D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.BackColor = System.Drawing.Color.White;
            this.xrTableCell251.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell251.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell251.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PhysicalAddress")});
            this.xrTableCell251.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell251.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBackColor = false;
            this.xrTableCell251.StylePriority.UseBorderColor = false;
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.StylePriority.UseFont = false;
            this.xrTableCell251.StylePriority.UseForeColor = false;
            this.xrTableCell251.Text = "xrTableCell251";
            this.xrTableCell251.Weight = 0.85443039906175833D;
            // 
            // xrTableRow103
            // 
            this.xrTableRow103.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell252,
            this.xrTableCell253,
            this.xrTableCell254,
            this.xrTableCell256});
            this.xrTableRow103.Name = "xrTableRow103";
            this.xrTableRow103.Weight = 0.8D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell252.BorderColor = System.Drawing.Color.White;
            this.xrTableCell252.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.StylePriority.UseBackColor = false;
            this.xrTableCell252.StylePriority.UseBorderColor = false;
            this.xrTableCell252.StylePriority.UseForeColor = false;
            this.xrTableCell252.Text = "Property Type:";
            this.xrTableCell252.Weight = 0.64556962991062583D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.BackColor = System.Drawing.Color.White;
            this.xrTableCell253.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell253.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell253.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PropertyTypeDesc")});
            this.xrTableCell253.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell253.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBackColor = false;
            this.xrTableCell253.StylePriority.UseBorderColor = false;
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.StylePriority.UseFont = false;
            this.xrTableCell253.StylePriority.UseForeColor = false;
            this.xrTableCell253.Text = "xrTableCell253";
            this.xrTableCell253.Weight = 0.8544303314595284D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell254.BorderColor = System.Drawing.Color.White;
            this.xrTableCell254.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBackColor = false;
            this.xrTableCell254.StylePriority.UseBorderColor = false;
            this.xrTableCell254.StylePriority.UseForeColor = false;
            this.xrTableCell254.Text = "Extent / Size:";
            this.xrTableCell254.Weight = 0.64556963956808744D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.BackColor = System.Drawing.Color.White;
            this.xrTableCell256.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell256.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfSize")});
            this.xrTableCell256.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell256.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseBackColor = false;
            this.xrTableCell256.StylePriority.UseBorderColor = false;
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.StylePriority.UseFont = false;
            this.xrTableCell256.StylePriority.UseForeColor = false;
            this.xrTableCell256.Text = "xrTableCell256";
            this.xrTableCell256.Weight = 0.85443039906175833D;
            // 
            // xrTableRow104
            // 
            this.xrTableRow104.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell261,
            this.lblDPurchaseprice});
            this.xrTableRow104.Name = "xrTableRow104";
            this.xrTableRow104.Weight = 0.8D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell259.BorderColor = System.Drawing.Color.White;
            this.xrTableCell259.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBackColor = false;
            this.xrTableCell259.StylePriority.UseBorderColor = false;
            this.xrTableCell259.StylePriority.UseForeColor = false;
            this.xrTableCell259.Text = "Purchase Date:";
            this.xrTableCell259.Weight = 0.64556962991062583D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.BackColor = System.Drawing.Color.White;
            this.xrTableCell260.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell260.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchaseDate")});
            this.xrTableCell260.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell260.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBackColor = false;
            this.xrTableCell260.StylePriority.UseBorderColor = false;
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.StylePriority.UseFont = false;
            this.xrTableCell260.StylePriority.UseForeColor = false;
            this.xrTableCell260.Text = "xrTableCell260";
            this.xrTableCell260.Weight = 0.8544303314595284D;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell261.BorderColor = System.Drawing.Color.White;
            this.xrTableCell261.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBackColor = false;
            this.xrTableCell261.StylePriority.UseBorderColor = false;
            this.xrTableCell261.StylePriority.UseForeColor = false;
            this.xrTableCell261.Text = "Purchase price:";
            this.xrTableCell261.Weight = 0.64556963956808744D;
            // 
            // lblDPurchaseprice
            // 
            this.lblDPurchaseprice.BackColor = System.Drawing.Color.White;
            this.lblDPurchaseprice.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDPurchaseprice.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPurchaseprice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchasePriceAmt")});
            this.lblDPurchaseprice.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.lblDPurchaseprice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDPurchaseprice.Name = "lblDPurchaseprice";
            this.lblDPurchaseprice.StylePriority.UseBackColor = false;
            this.lblDPurchaseprice.StylePriority.UseBorderColor = false;
            this.lblDPurchaseprice.StylePriority.UseBorders = false;
            this.lblDPurchaseprice.StylePriority.UseFont = false;
            this.lblDPurchaseprice.StylePriority.UseForeColor = false;
            this.lblDPurchaseprice.Text = "lblDPurchaseprice";
            this.lblDPurchaseprice.Weight = 0.85443039906175833D;
            // 
            // xrTableRow105
            // 
            this.xrTableRow105.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265,
            this.lblDBondAmount});
            this.xrTableRow105.Name = "xrTableRow105";
            this.xrTableRow105.Weight = 0.8D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell263.BorderColor = System.Drawing.Color.White;
            this.xrTableCell263.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBackColor = false;
            this.xrTableCell263.StylePriority.UseBorderColor = false;
            this.xrTableCell263.StylePriority.UseForeColor = false;
            this.xrTableCell263.Text = "Bond Holder:";
            this.xrTableCell263.Weight = 0.64556962991062583D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.BackColor = System.Drawing.Color.White;
            this.xrTableCell264.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell264.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell264.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondHolderName")});
            this.xrTableCell264.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell264.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBackColor = false;
            this.xrTableCell264.StylePriority.UseBorderColor = false;
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.StylePriority.UseFont = false;
            this.xrTableCell264.StylePriority.UseForeColor = false;
            this.xrTableCell264.Text = "xrTableCell264";
            this.xrTableCell264.Weight = 0.8544303314595284D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell265.BorderColor = System.Drawing.Color.White;
            this.xrTableCell265.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBackColor = false;
            this.xrTableCell265.StylePriority.UseBorderColor = false;
            this.xrTableCell265.StylePriority.UseForeColor = false;
            this.xrTableCell265.Text = "Bond Amount:";
            this.xrTableCell265.Weight = 0.64556963956808744D;
            // 
            // lblDBondAmount
            // 
            this.lblDBondAmount.BackColor = System.Drawing.Color.White;
            this.lblDBondAmount.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDBondAmount.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDBondAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAmt")});
            this.lblDBondAmount.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.lblDBondAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.lblDBondAmount.Name = "lblDBondAmount";
            this.lblDBondAmount.StylePriority.UseBackColor = false;
            this.lblDBondAmount.StylePriority.UseBorderColor = false;
            this.lblDBondAmount.StylePriority.UseBorders = false;
            this.lblDBondAmount.StylePriority.UseFont = false;
            this.lblDBondAmount.StylePriority.UseForeColor = false;
            this.lblDBondAmount.Text = "lblDBondAmount";
            this.lblDBondAmount.Weight = 0.85443039906175833D;
            // 
            // xrTableRow106
            // 
            this.xrTableRow106.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267,
            this.xrTableCell268,
            this.xrTableCell270});
            this.xrTableRow106.Name = "xrTableRow106";
            this.xrTableRow106.Weight = 0.80000000000000027D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell267.BorderColor = System.Drawing.Color.White;
            this.xrTableCell267.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBackColor = false;
            this.xrTableCell267.StylePriority.UseBorderColor = false;
            this.xrTableCell267.StylePriority.UseForeColor = false;
            this.xrTableCell267.Text = "Bond Number: ";
            this.xrTableCell267.Weight = 0.64556962991062583D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.BackColor = System.Drawing.Color.White;
            this.xrTableCell268.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell268.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAccountNo")});
            this.xrTableCell268.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell268.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBackColor = false;
            this.xrTableCell268.StylePriority.UseBorderColor = false;
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.StylePriority.UseFont = false;
            this.xrTableCell268.StylePriority.UseForeColor = false;
            this.xrTableCell268.Text = "xrTableCell268";
            this.xrTableCell268.Weight = 0.8544303314595284D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.BackColor = System.Drawing.Color.White;
            this.xrTableCell270.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell270.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell270.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBackColor = false;
            this.xrTableCell270.StylePriority.UseBorderColor = false;
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.StylePriority.UseFont = false;
            this.xrTableCell270.StylePriority.UseForeColor = false;
            this.xrTableCell270.Weight = 1.5000000386298458D;
            // 
            // GroupHeaderPropertyInterest
            // 
            this.GroupHeaderPropertyInterest.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable50});
            this.GroupHeaderPropertyInterest.HeightF = 70.83334F;
            this.GroupHeaderPropertyInterest.Name = "GroupHeaderPropertyInterest";
            // 
            // xrTable50
            // 
            this.xrTable50.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable50.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable50.ForeColor = System.Drawing.Color.Gray;
            this.xrTable50.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable50.Name = "xrTable50";
            this.xrTable50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable50.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow95,
            this.xrTableRow96});
            this.xrTable50.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable50.StylePriority.UseBorders = false;
            this.xrTable50.StylePriority.UseFont = false;
            this.xrTable50.StylePriority.UseForeColor = false;
            this.xrTable50.StylePriority.UsePadding = false;
            this.xrTable50.StylePriority.UseTextAlignment = false;
            this.xrTable50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow95
            // 
            this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell234});
            this.xrTableRow95.Name = "xrTableRow95";
            this.xrTableRow95.Weight = 0.8D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.BackColor = System.Drawing.Color.White;
            this.xrTableCell234.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell234.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell234.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell234.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell234.StylePriority.UseBackColor = false;
            this.xrTableCell234.StylePriority.UseBorderColor = false;
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.StylePriority.UseFont = false;
            this.xrTableCell234.StylePriority.UseForeColor = false;
            this.xrTableCell234.StylePriority.UsePadding = false;
            this.xrTableCell234.Text = "Property Interest";
            this.xrTableCell234.Weight = 3D;
            // 
            // xrTableRow96
            // 
            this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235});
            this.xrTableRow96.Name = "xrTableRow96";
            this.xrTableRow96.Weight = 0.53333333333333344D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell235.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell235.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell235.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell235.Multiline = true;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell235.StylePriority.UseBorderColor = false;
            this.xrTableCell235.StylePriority.UseBorders = false;
            this.xrTableCell235.StylePriority.UseFont = false;
            this.xrTableCell235.StylePriority.UseForeColor = false;
            this.xrTableCell235.StylePriority.UsePadding = false;
            this.xrTableCell235.Text = "This section displays a list of properties registered in the name of the consumer" +
    " at the Deeds offices around SA.";
            this.xrTableCell235.Weight = 3D;
            // 
            // DirectorshipLinks
            // 
            this.DirectorshipLinks.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDirectorshipLinks,
            this.GroupHeaderDirectorshipLinks});
            this.DirectorshipLinks.DataMember = "ConsumerDirectorShipLink";
            this.DirectorshipLinks.Level = 17;
            this.DirectorshipLinks.Name = "DirectorshipLinks";
            // 
            // DetailDirectorshipLinks
            // 
            this.DetailDirectorshipLinks.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable55});
            this.DetailDirectorshipLinks.HeightF = 80.41668F;
            this.DetailDirectorshipLinks.Name = "DetailDirectorshipLinks";
            // 
            // xrTable55
            // 
            this.xrTable55.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable55.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable55.ForeColor = System.Drawing.Color.Gray;
            this.xrTable55.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable55.Name = "xrTable55";
            this.xrTable55.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable55.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow107,
            this.xrTableRow108,
            this.xrTableRow109,
            this.xrTableRow110});
            this.xrTable55.SizeF = new System.Drawing.SizeF(880F, 80F);
            this.xrTable55.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable55.StylePriority.UseBorders = false;
            this.xrTable55.StylePriority.UseFont = false;
            this.xrTable55.StylePriority.UseForeColor = false;
            this.xrTable55.StylePriority.UsePadding = false;
            this.xrTable55.StylePriority.UseTextAlignment = false;
            this.xrTable55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow107
            // 
            this.xrTableRow107.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell269,
            this.xrTableCell271,
            this.xrTableCell272,
            this.xrTableCell273});
            this.xrTableRow107.Name = "xrTableRow107";
            this.xrTableRow107.Weight = 0.79999999999999993D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell269.BorderColor = System.Drawing.Color.White;
            this.xrTableCell269.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseBackColor = false;
            this.xrTableCell269.StylePriority.UseBorderColor = false;
            this.xrTableCell269.StylePriority.UseForeColor = false;
            this.xrTableCell269.Text = "Current Post:";
            this.xrTableCell269.Weight = 0.64556962991062583D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.BackColor = System.Drawing.Color.White;
            this.xrTableCell271.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell271.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell271.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorDesignationDesc")});
            this.xrTableCell271.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell271.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBackColor = false;
            this.xrTableCell271.StylePriority.UseBorderColor = false;
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.StylePriority.UseFont = false;
            this.xrTableCell271.StylePriority.UseForeColor = false;
            this.xrTableCell271.Text = "xrTableCell271";
            this.xrTableCell271.Weight = 0.8544303314595284D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell272.BorderColor = System.Drawing.Color.White;
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell272.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBackColor = false;
            this.xrTableCell272.StylePriority.UseBorderColor = false;
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.StylePriority.UseForeColor = false;
            this.xrTableCell272.Text = "Date of Inception:";
            this.xrTableCell272.Weight = 0.64556963956808744D;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.BackColor = System.Drawing.Color.White;
            this.xrTableCell273.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell273.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell273.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.AppointmentDate")});
            this.xrTableCell273.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell273.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBackColor = false;
            this.xrTableCell273.StylePriority.UseBorderColor = false;
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.StylePriority.UseFont = false;
            this.xrTableCell273.StylePriority.UseForeColor = false;
            this.xrTableCell273.Text = "xrTableCell273";
            this.xrTableCell273.Weight = 0.85443039906175833D;
            // 
            // xrTableRow108
            // 
            this.xrTableRow108.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell274,
            this.xrTableCell275,
            this.xrTableCell276,
            this.xrTableCell277});
            this.xrTableRow108.Name = "xrTableRow108";
            this.xrTableRow108.Weight = 0.79999999999999993D;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell274.BorderColor = System.Drawing.Color.White;
            this.xrTableCell274.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.StylePriority.UseBackColor = false;
            this.xrTableCell274.StylePriority.UseBorderColor = false;
            this.xrTableCell274.StylePriority.UseForeColor = false;
            this.xrTableCell274.Text = "Company Name:";
            this.xrTableCell274.Weight = 0.64556962991062583D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.BackColor = System.Drawing.Color.White;
            this.xrTableCell275.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell275.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.CommercialName")});
            this.xrTableCell275.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell275.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBackColor = false;
            this.xrTableCell275.StylePriority.UseBorderColor = false;
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.StylePriority.UseFont = false;
            this.xrTableCell275.StylePriority.UseForeColor = false;
            this.xrTableCell275.Text = "xrTableCell275";
            this.xrTableCell275.Weight = 0.8544303314595284D;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell276.BorderColor = System.Drawing.Color.White;
            this.xrTableCell276.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.StylePriority.UseBackColor = false;
            this.xrTableCell276.StylePriority.UseBorderColor = false;
            this.xrTableCell276.StylePriority.UseForeColor = false;
            this.xrTableCell276.Text = "Company Reg No.: ";
            this.xrTableCell276.Weight = 0.64556963956808744D;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.BackColor = System.Drawing.Color.White;
            this.xrTableCell277.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell277.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.RegistrationNo")});
            this.xrTableCell277.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell277.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseBackColor = false;
            this.xrTableCell277.StylePriority.UseBorderColor = false;
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.StylePriority.UseFont = false;
            this.xrTableCell277.StylePriority.UseForeColor = false;
            this.xrTableCell277.Text = "xrTableCell277";
            this.xrTableCell277.Weight = 0.85443039906175833D;
            // 
            // xrTableRow109
            // 
            this.xrTableRow109.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell278,
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTableCell281});
            this.xrTableRow109.Name = "xrTableRow109";
            this.xrTableRow109.Weight = 0.8D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell278.BorderColor = System.Drawing.Color.White;
            this.xrTableCell278.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBackColor = false;
            this.xrTableCell278.StylePriority.UseBorderColor = false;
            this.xrTableCell278.StylePriority.UseForeColor = false;
            this.xrTableCell278.Text = "Company Address:";
            this.xrTableCell278.Weight = 0.64556962991062583D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.BackColor = System.Drawing.Color.White;
            this.xrTableCell279.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell279.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell279.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.PhysicalAddress")});
            this.xrTableCell279.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell279.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBackColor = false;
            this.xrTableCell279.StylePriority.UseBorderColor = false;
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.StylePriority.UseFont = false;
            this.xrTableCell279.StylePriority.UseForeColor = false;
            this.xrTableCell279.Text = "xrTableCell279";
            this.xrTableCell279.Weight = 0.8544303314595284D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell280.BorderColor = System.Drawing.Color.White;
            this.xrTableCell280.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBackColor = false;
            this.xrTableCell280.StylePriority.UseBorderColor = false;
            this.xrTableCell280.StylePriority.UseForeColor = false;
            this.xrTableCell280.Text = "Company Phone No.: ";
            this.xrTableCell280.Weight = 0.64556963956808744D;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.BackColor = System.Drawing.Color.White;
            this.xrTableCell281.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell281.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.TelephoneNo")});
            this.xrTableCell281.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell281.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBackColor = false;
            this.xrTableCell281.StylePriority.UseBorderColor = false;
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.StylePriority.UseFont = false;
            this.xrTableCell281.StylePriority.UseForeColor = false;
            this.xrTableCell281.Text = "xrTableCell281";
            this.xrTableCell281.Weight = 0.85443039906175833D;
            // 
            // xrTableRow110
            // 
            this.xrTableRow110.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell282,
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell285});
            this.xrTableRow110.Name = "xrTableRow110";
            this.xrTableRow110.Weight = 0.8D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell282.BorderColor = System.Drawing.Color.White;
            this.xrTableCell282.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseBackColor = false;
            this.xrTableCell282.StylePriority.UseBorderColor = false;
            this.xrTableCell282.StylePriority.UseForeColor = false;
            this.xrTableCell282.Text = "Industry category:";
            this.xrTableCell282.Weight = 0.64556962991062583D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.BackColor = System.Drawing.Color.White;
            this.xrTableCell283.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell283.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell283.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.SICDesc")});
            this.xrTableCell283.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell283.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseBackColor = false;
            this.xrTableCell283.StylePriority.UseBorderColor = false;
            this.xrTableCell283.StylePriority.UseBorders = false;
            this.xrTableCell283.StylePriority.UseFont = false;
            this.xrTableCell283.StylePriority.UseForeColor = false;
            this.xrTableCell283.Text = "xrTableCell283";
            this.xrTableCell283.Weight = 0.8544303314595284D;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell284.BorderColor = System.Drawing.Color.White;
            this.xrTableCell284.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.StylePriority.UseBackColor = false;
            this.xrTableCell284.StylePriority.UseBorderColor = false;
            this.xrTableCell284.StylePriority.UseForeColor = false;
            this.xrTableCell284.Text = "Director Status";
            this.xrTableCell284.Weight = 0.64556963956808744D;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.BackColor = System.Drawing.Color.White;
            this.xrTableCell285.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell285.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell285.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorStatus")});
            this.xrTableCell285.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.xrTableCell285.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(76)))), ((int)(((byte)(101)))));
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseBackColor = false;
            this.xrTableCell285.StylePriority.UseBorderColor = false;
            this.xrTableCell285.StylePriority.UseBorders = false;
            this.xrTableCell285.StylePriority.UseFont = false;
            this.xrTableCell285.StylePriority.UseForeColor = false;
            this.xrTableCell285.Text = "xrTableCell285";
            this.xrTableCell285.Weight = 0.85443039906175833D;
            // 
            // GroupHeaderDirectorshipLinks
            // 
            this.GroupHeaderDirectorshipLinks.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable51});
            this.GroupHeaderDirectorshipLinks.HeightF = 71.875F;
            this.GroupHeaderDirectorshipLinks.Name = "GroupHeaderDirectorshipLinks";
            // 
            // xrTable51
            // 
            this.xrTable51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable51.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTable51.ForeColor = System.Drawing.Color.Gray;
            this.xrTable51.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10F);
            this.xrTable51.Name = "xrTable51";
            this.xrTable51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable51.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow97,
            this.xrTableRow98});
            this.xrTable51.SizeF = new System.Drawing.SizeF(880F, 50F);
            this.xrTable51.StylePriority.UseBorders = false;
            this.xrTable51.StylePriority.UseFont = false;
            this.xrTable51.StylePriority.UseForeColor = false;
            this.xrTable51.StylePriority.UsePadding = false;
            this.xrTable51.StylePriority.UseTextAlignment = false;
            this.xrTable51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow97
            // 
            this.xrTableRow97.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236});
            this.xrTableRow97.Name = "xrTableRow97";
            this.xrTableRow97.Weight = 0.8D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.BackColor = System.Drawing.Color.White;
            this.xrTableCell236.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell236.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.xrTableCell236.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell236.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell236.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(211)))), ((int)(((byte)(51)))));
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell236.StylePriority.UseBackColor = false;
            this.xrTableCell236.StylePriority.UseBorderColor = false;
            this.xrTableCell236.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell236.StylePriority.UseBorders = false;
            this.xrTableCell236.StylePriority.UseFont = false;
            this.xrTableCell236.StylePriority.UseForeColor = false;
            this.xrTableCell236.StylePriority.UsePadding = false;
            this.xrTableCell236.Text = "Directorship Links";
            this.xrTableCell236.Weight = 3D;
            // 
            // xrTableRow98
            // 
            this.xrTableRow98.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell237});
            this.xrTableRow98.Name = "xrTableRow98";
            this.xrTableRow98.Weight = 0.53333333333333344D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell237.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell237.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell237.ForeColor = System.Drawing.Color.Navy;
            this.xrTableCell237.Multiline = true;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell237.StylePriority.UseBorderColor = false;
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.StylePriority.UseFont = false;
            this.xrTableCell237.StylePriority.UseForeColor = false;
            this.xrTableCell237.StylePriority.UsePadding = false;
            this.xrTableCell237.Text = "This section displays a list of companies where the Consumer is linked as a Princ" +
    "ipal at CIPC.";
            this.xrTableCell237.Weight = 3D;
            // 
            // DetailReport14
            // 
            this.DetailReport14.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail17});
            this.DetailReport14.DataMember = "ConsumerDefinition";
            this.DetailReport14.Level = 6;
            this.DetailReport14.Name = "DetailReport14";
            // 
            // Detail17
            // 
            this.Detail17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable18});
            this.Detail17.HeightF = 33.54177F;
            this.Detail17.MultiColumn.ColumnCount = 3;
            this.Detail17.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail17.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail17.Name = "Detail17";
            this.Detail17.StylePriority.UseTextAlignment = false;
            this.Detail17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrTable18
            // 
            this.xrTable18.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable18.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.xrTable18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(22.50013F, 0F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.xrTable18.SizeF = new System.Drawing.SizeF(275.7711F, 33.54177F);
            this.xrTable18.StylePriority.UseBackColor = false;
            this.xrTable18.StylePriority.UseBorderColor = false;
            this.xrTable18.StylePriority.UseBorders = false;
            this.xrTable18.StylePriority.UseFont = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDef,
            this.xrTableCell110});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.StylePriority.UseBorders = false;
            this.xrTableRow42.Weight = 1D;
            // 
            // lblDef
            // 
            this.lblDef.BackColor = System.Drawing.Color.White;
            this.lblDef.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.lblDef.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblDef.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pbDef});
            this.lblDef.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefinition.DefinitionCode")});
            this.lblDef.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.lblDef.ForeColor = System.Drawing.Color.DimGray;
            this.lblDef.Name = "lblDef";
            this.lblDef.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDef.StylePriority.UseBackColor = false;
            this.lblDef.StylePriority.UseBorderColor = false;
            this.lblDef.StylePriority.UseBorders = false;
            this.lblDef.StylePriority.UseFont = false;
            this.lblDef.StylePriority.UseForeColor = false;
            this.lblDef.StylePriority.UsePadding = false;
            this.lblDef.StylePriority.UseTextAlignment = false;
            this.lblDef.Text = "lblDef";
            this.lblDef.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDef.Weight = 0.19794084795996253D;
            // 
            // pbDef
            // 
            this.pbDef.ImageUrl = "~\\images\\0.png";
            this.pbDef.LocationFloat = new DevExpress.Utils.PointFloat(12.49987F, 0F);
            this.pbDef.Name = "pbDef";
            this.pbDef.SizeF = new System.Drawing.SizeF(29.06F, 31.04F);
            this.pbDef.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pbDef.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pbDef_BeforePrint);
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.BackColor = System.Drawing.Color.White;
            this.xrTableCell110.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell110.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell110.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefinition.DefinitionDesc")});
            this.xrTableCell110.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell110.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell110.StylePriority.UseBackColor = false;
            this.xrTableCell110.StylePriority.UseBorderColor = false;
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.StylePriority.UseFont = false;
            this.xrTableCell110.StylePriority.UseForeColor = false;
            this.xrTableCell110.StylePriority.UsePadding = false;
            this.xrTableCell110.StylePriority.UseTextAlignment = false;
            this.xrTableCell110.Text = "xrTableCell110";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell110.Weight = 0.82157037086657525D;
            // 
            // CreditReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PersonalDetails,
            this.ReportHeader,
            this.ReportFooter,
            this.PageFooter,
            this.FraudIndicators,
            this.PresageScore,
            this.DebtSummary,
            this.DetailReport4,
            this.DetailReport5,
            this.AdverseDomainRecords,
            this.Rehabilitation,
            this.DebtReview,
            this.PaymentNotifications,
            this.EnquiryHistory,
            this.ContactHistory,
            this.ConfirmedInformation,
            this.TelephoneLinkages,
            this.EmploymentHistory,
            this.PropertyInterest,
            this.DirectorshipLinks,
            this.DetailReport14});
            this.Margins = new System.Drawing.Printing.Margins(15, 10, 15, 25);
            this.PageHeight = 1200;
            this.PageWidth = 927;
            this.PaperKind = System.Drawing.Printing.PaperKind.LetterExtra;
            this.Version = "12.1";
            this.XmlDataPath = "C:\\Users\\pgummala\\Desktop\\Credit2.xml";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDishonouredChequeHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.DetailReportBand PersonalDetails;
        private DevExpress.XtraReports.UI.DetailBand DetailPersonalDetails;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell lblBirthDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderPersonalDetails;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.DetailReportBand FraudIndicators;
        private DevExpress.XtraReports.UI.DetailBand DetailFraudIndicators;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderFraudIndicators;
        private DevExpress.XtraReports.UI.DetailReportBand PresageScore;
        private DevExpress.XtraReports.UI.DetailBand DetailPresageScore;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderPresageScore;
        private DevExpress.XtraReports.UI.DetailReportBand DebtSummary;
        private DevExpress.XtraReports.UI.DetailBand DetailDebtSummary;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderDebtSummary;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAccTotValue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTable tblDishonouredChequeHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditLimitD;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrBalanceD;
        private DevExpress.XtraReports.UI.XRTableCell lblInstallementD;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearsAmountD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport5;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable tblMonthlyPaymentHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRTableCell lblM01;
        private DevExpress.XtraReports.UI.XRTableCell lblM02;
        private DevExpress.XtraReports.UI.XRTableCell lblM03;
        private DevExpress.XtraReports.UI.XRTableCell lblM04;
        private DevExpress.XtraReports.UI.XRTableCell lblM05;
        private DevExpress.XtraReports.UI.XRTableCell lblM06;
        private DevExpress.XtraReports.UI.XRTableCell lblM07;
        private DevExpress.XtraReports.UI.XRTableCell lblM08;
        private DevExpress.XtraReports.UI.XRTableCell lblM09;
        private DevExpress.XtraReports.UI.XRTableCell lblM10;
        private DevExpress.XtraReports.UI.XRTableCell lblM11;
        private DevExpress.XtraReports.UI.XRTableCell lblM12;
        private DevExpress.XtraReports.UI.XRTableCell lblM13;
        private DevExpress.XtraReports.UI.XRTableCell lblM14;
        private DevExpress.XtraReports.UI.XRTableCell lblM15;
        private DevExpress.XtraReports.UI.XRTableCell lblM16;
        private DevExpress.XtraReports.UI.XRTableCell lblM17;
        private DevExpress.XtraReports.UI.XRTableCell lblM18;
        private DevExpress.XtraReports.UI.XRTableCell lblM19;
        private DevExpress.XtraReports.UI.XRTableCell lblM20;
        private DevExpress.XtraReports.UI.XRTableCell lblM21;
        private DevExpress.XtraReports.UI.XRTableCell lblM22;
        private DevExpress.XtraReports.UI.XRTableCell lblM23;
        private DevExpress.XtraReports.UI.XRTableCell lblM24;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell433;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth01;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth02;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth03;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth04;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth05;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth06;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth07;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth08;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth09;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth10;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth11;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth12;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth13;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth14;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth15;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth16;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth17;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth18;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth19;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth20;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth21;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth22;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth23;
        private DevExpress.XtraReports.UI.XRTableCell lblmonth24;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblScore;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.DetailReportBand AdverseDomainRecords;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.DetailReportBand Adverse;
        private DevExpress.XtraReports.UI.DetailBand DetailAdverseInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderAdverse;
        private DevExpress.XtraReports.UI.DetailReportBand Rehabilitation;
        private DevExpress.XtraReports.UI.DetailBand DetailRehabilitation;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell lblREHDAmount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderRehabilitation;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.DetailReportBand DebtReview;
        private DevExpress.XtraReports.UI.DetailBand DetailDebtReview;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderDebtReview;
        private DevExpress.XtraReports.UI.DetailReportBand PaymentNotifications;
        private DevExpress.XtraReports.UI.DetailBand DetailPaymentNotifications;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell lblPmtAmountD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderPaymentNotifications;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.DetailReportBand EnquiryHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailEnquiryHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderEnquiryHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.DetailReportBand ContactHistory;
        private DevExpress.XtraReports.UI.DetailBand Detail13;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderContactHistory;
        private DevExpress.XtraReports.UI.DetailReportBand AddressHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailAddressHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderAddressHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.DetailReportBand ConfirmedInformation;
        private DevExpress.XtraReports.UI.DetailBand DetailConfirmedInformation;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderConfirmedInformation;
        private DevExpress.XtraReports.UI.XRTable xrTable47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.DetailReportBand TelephoneLinkages;
        private DevExpress.XtraReports.UI.DetailBand DetailTelephoneLinkages;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderTelephoneLinkages;
        private DevExpress.XtraReports.UI.XRTable xrTable48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.DetailReportBand EmploymentHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailEmploymentHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderEmploymentHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTable xrTable49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.DetailReportBand PropertyInterest;
        private DevExpress.XtraReports.UI.DetailBand DetailPropertyInterest;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderPropertyInterest;
        private DevExpress.XtraReports.UI.XRTable xrTable50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.DetailReportBand DirectorshipLinks;
        private DevExpress.XtraReports.UI.DetailBand DetailDirectorshipLinks;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderDirectorshipLinks;
        private DevExpress.XtraReports.UI.XRTable xrTable51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTable xrTable54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell lblDPurchaseprice;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell lblDBondAmount;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTable xrTable55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell lblDAmount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.DetailReportBand Judgment;
        private DevExpress.XtraReports.UI.DetailBand DetailJudgment;
        private DevExpress.XtraReports.UI.XRTable xrTable57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblJDAmount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderJudgment;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTable xrTable60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTable xrTable59;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable xrTable62;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable xrTable61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport6;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.XRTable xrTable65;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
        private DevExpress.XtraReports.UI.XRTable xrTable63;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport7;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.XRTable xrTable66;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
        private DevExpress.XtraReports.UI.XRTable xrTable64;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGoodTotvalue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAccTotValue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinstTotValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebtTotValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamtTotvalue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentratingTotValue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell lblAOCount;
        private DevExpress.XtraReports.UI.XRTableCell lblAOBalance;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell lblenq90otherTotvalue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableCell lblAcc30TotValue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell lblAccDefaultCount;
        private DevExpress.XtraReports.UI.XRTableCell lblADBalance;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell lblDLCount;
        private DevExpress.XtraReports.UI.XRTableCell lblDLBalance;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRTableCell lblROCount;
        private DevExpress.XtraReports.UI.XRTableCell lblROAmt;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell lblEnq24Count;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell lblPurchPrice;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableCell lblDebtReviewStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell lblDisputeMsg;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport8;
        private DevExpress.XtraReports.UI.DetailBand Detail10;
        private DevExpress.XtraReports.UI.XRTable xrTable69;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAmountD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
        private DevExpress.XtraReports.UI.XRTable xrTable68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport9;
        private DevExpress.XtraReports.UI.DetailBand DetailTelephoneHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable72;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.XRTable xrTable44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport10;
        private DevExpress.XtraReports.UI.DetailBand DetailEmailAddressHistory;
        private DevExpress.XtraReports.UI.XRTable xrTable46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.XRTable xrTable70;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport11;
        private DevExpress.XtraReports.UI.DetailBand DetailOtherContactAddress;
        private DevExpress.XtraReports.UI.XRTable xrTable73;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell410;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
        private DevExpress.XtraReports.UI.XRTable xrTable71;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport12;
        private DevExpress.XtraReports.UI.DetailBand DetailOtherContactTelephone;
        private DevExpress.XtraReports.UI.XRTable xrTable74;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell412;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader14;
        private DevExpress.XtraReports.UI.XRTable xrTable56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRPictureBox pbm24;
        private DevExpress.XtraReports.UI.XRPictureBox pbm01;
        private DevExpress.XtraReports.UI.XRPictureBox pbm02;
        private DevExpress.XtraReports.UI.XRPictureBox pbm03;
        private DevExpress.XtraReports.UI.XRPictureBox pbm04;
        private DevExpress.XtraReports.UI.XRPictureBox pbm05;
        private DevExpress.XtraReports.UI.XRPictureBox pbm06;
        private DevExpress.XtraReports.UI.XRPictureBox pbm07;
        private DevExpress.XtraReports.UI.XRPictureBox pbm08;
        private DevExpress.XtraReports.UI.XRPictureBox pbm09;
        private DevExpress.XtraReports.UI.XRPictureBox pbm10;
        private DevExpress.XtraReports.UI.XRPictureBox pbm11;
        private DevExpress.XtraReports.UI.XRPictureBox pbm12;
        private DevExpress.XtraReports.UI.XRPictureBox pbm13;
        private DevExpress.XtraReports.UI.XRPictureBox pbm14;
        private DevExpress.XtraReports.UI.XRPictureBox pbm15;
        private DevExpress.XtraReports.UI.XRPictureBox pbm16;
        private DevExpress.XtraReports.UI.XRPictureBox pbm17;
        private DevExpress.XtraReports.UI.XRPictureBox pbm18;
        private DevExpress.XtraReports.UI.XRPictureBox pbm19;
        private DevExpress.XtraReports.UI.XRPictureBox pbm20;
        private DevExpress.XtraReports.UI.XRPictureBox pbm21;
        private DevExpress.XtraReports.UI.XRPictureBox pbm22;
        private DevExpress.XtraReports.UI.XRPictureBox pbm23;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport14;
        private DevExpress.XtraReports.UI.DetailBand Detail17;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell lblDef;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRPictureBox pbDef;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTable xrTable67;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
    }
}
