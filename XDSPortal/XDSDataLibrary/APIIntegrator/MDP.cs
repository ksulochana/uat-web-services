﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using XDSPortalLibrary.Entity_Layer.MDP;

namespace XDSDataLibrary.APIIntegrator
{
    internal class Mdp
    {
        readonly string _connection;
        readonly string _Enquiryconnection;
        readonly string _productname;
        readonly int _SubscriberEnquiryID;

        string error = string.Empty;

        TokenForQualification token = new TokenForQualification();
        public Mdp(string Admincn, string Enquirycn, string ProductName, int SubscriberEnquiryID)
        {
            _connection = Admincn;
            _productname = ProductName;
            _SubscriberEnquiryID = SubscriberEnquiryID;
            _Enquiryconnection = Enquirycn;

            if (string.IsNullOrEmpty(_productname)) { throw new System.ArgumentException("Inavlid Productname Supplied."); }
            GetMDPIntegrationinfo();

        }

        public void Login()
        {
            var action = "Login";
            var querystring = string.Empty;
            var loginResponse = string.Empty;
            var url = string.Empty;

            Constructquerystring(action, out url, out querystring);
            try
            {
                loginResponse = InvokeAPI("Post", querystring, url, action);

                if (!string.IsNullOrEmpty(loginResponse))
                {
                    token = JsonConvert.DeserializeObject<TokenForQualification>(loginResponse);
                }
            }
            catch (WebException ex)
            {
                error = ex.Message + ex.StackTrace + ex.InnerException;
            }

            Logmdprequestresponse(querystring, loginResponse, error);
            if (!string.IsNullOrEmpty(error))
            {
                throw new HttpException("Error while generating the token");
            }

        }

        public DataTable GetXAC(string idno, Int64 consumerId, out string request, out string response)
        {
            var action = "XAC";
            var querystring = string.Empty;
            var url = string.Empty;
            response = string.Empty;
            DataTable qualificationResponse = new DataTable();

            Constructquerystring(action, out url, out querystring);

            string body = JsonConvert.SerializeObject(new { idNo = idno, consumerId = consumerId });
            request = body;
            response = InvokeAPI("Post", body, url, action);


            if (!string.IsNullOrEmpty(response))
            {
                var result = JsonConvert.DeserializeObject<QualificationResponse>(response);

                qualificationResponse = ConvertToDataTable(result.xac);

            }
            return qualificationResponse;
        }

        public DataTable GetXACWithLogin(string idno, Int64 consumerId)
        {
            var error = string.Empty;
            var response = string.Empty;
            var qualificationResponse = new DataTable();
            string request = string.Empty;
            try
            {
                Login();
                qualificationResponse = GetXAC(idno, consumerId, out request, out response);
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (HttpWebResponse webResponse = (HttpWebResponse)ex.Response)
                    {
                        if ((int)webResponse.StatusCode == 401)
                        {
                            Login();
                            qualificationResponse = GetXAC(idno, consumerId, out request, out response);
                        }
                    }
                }
                else
                {
                    error = ex.Message + ex.StackTrace + ex.InnerException;
                }
            }

            Logmdprequestresponse(request, response, error);
            if (!string.IsNullOrEmpty(error))
            {
                throw new HttpException("Error while fetching XAC");
            }
            return qualificationResponse;
        }

        private static void Constructquerystring(string action, out string url, out string querystring)
        {

            querystring = string.Empty;

            XDSPortalLibrary.Entity_Layer.URLInfo uRLInfo = XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo.Where(t => t.Key == action).Select(t => t.Value).First();
            if (action == "Login")
            {
                url = uRLInfo.MDPBaseURL + uRLInfo.URL;
            }
            else
            {
                url = uRLInfo.URL;
            }

            foreach (KeyValuePair<string, string> item in uRLInfo.Parameter)
            {
                querystring = $"{querystring}{item.Key}={item.Value}&";
            }
            querystring = querystring.Trim('=', '&');

        }


        public void GetMDPIntegrationinfo()
        {
            SqlConnection sqlConnection = new SqlConnection(_connection);

            if (sqlConnection.State == ConnectionState.Closed)
                sqlConnection.Open();

            string strSQL = @"Select mp.ProductName,mpu.FunctionName,mp.MDPBaseURL,mpu.url,mpup.ParameterName,mpup.ParameterValue from MDPProducts Mp inner join MDPProductURLS Mpu left outer join MDPProductURLParameters mpup on mpu.URLID = mpup.URLID
on mp.MDPProductID = Mpu.ProductID where mp.StatusInd = 'A' and mpu.StatusInd = 'A' and isnull(mpup.StatusInd,'A')= 'A' and mp.ProductName = '" + _productname.ToString() + "' order by FunctionName";

            System.IO.File.AppendAllText(@"C:\Log\Qualifications.txt", "start getting the info" + strSQL + "\n");
            SqlCommand sqlcmd = new SqlCommand(strSQL, sqlConnection);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            string functionname = string.Empty;
            Dictionary<string, string> param = new Dictionary<string, string>();

            XDSPortalLibrary.Entity_Layer.URLInfo oUinfo = new XDSPortalLibrary.Entity_Layer.URLInfo();
            XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo = new Dictionary<string, XDSPortalLibrary.Entity_Layer.URLInfo>();
            int i = 0;

            foreach (DataRow r in ds.Tables[0].Rows)
            {

                if (!string.IsNullOrEmpty(functionname) && functionname != r["FunctionName"].ToString())
                {

                    XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo.Add(functionname, oUinfo);

                    functionname = r["FunctionName"].ToString();

                    oUinfo = new XDSPortalLibrary.Entity_Layer.URLInfo();
                    oUinfo.MDPBaseURL = r["MDPBaseURL"].ToString();
                    oUinfo.URL = r["url"].ToString();

                    param.Add(r["ParameterName"].ToString(), r["ParameterValue"].ToString());
                    oUinfo.Parameter = param;

                }
                else
                {
                    functionname = r["FunctionName"].ToString();
                    oUinfo.MDPBaseURL = r["MDPBaseURL"].ToString();
                    oUinfo.URL = r["url"].ToString();

                    param.Add(r["ParameterName"].ToString(), r["ParameterValue"].ToString());
                    oUinfo.Parameter = param;
                }


                if (i == (ds.Tables[0].Rows.Count) - 1)
                {
                    XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo.Add(functionname, oUinfo);
                }
                i++;

            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();
            sqlConnection.Close();


        }

        private string InvokeAPI(string httpactionverb, string body, string url, string action)
        {
            string response = string.Empty;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc00); // tls2

                using (var client = new System.Net.WebClient())
                {
                    if (action != "Login")
                    {
                        client.Headers.Add("Authorization", String.Format("Bearer {0}", token.access_token));
                        client.Headers.Add("content-type", "application/json");
                    }
                    else
                    {
                        client.Headers.Add("content-type", "application/x-www-form-urlencoded");//set your header here, you can add multiple headers
                    }

                    response = Encoding.ASCII.GetString(client.UploadData(url, httpactionverb, Encoding.Default.GetBytes(body)));

                }
            }
            catch (Exception ex) { error = ex.Message + ex.StackTrace + ex.InnerException; }

            Logmdprequestresponse(url + ":" + body, response, error);

            return response;
        }

        private void Logmdprequestresponse(string request, string response, string error)
        {
            SqlConnection con = new SqlConnection(_Enquiryconnection);

            SqlCommand cmd = new SqlCommand("INSERT INTO RPCResponse (SubscriberEnquiryID,Response,SubmissionDate,ResponseDate,CreatedByUser,CreatedOnDate,Request,error) values(@SubscriberEnquiryID,@Response,@SubmissionDate,getdate(),'Admin',getdate(),@Request,@Error)", con);

            cmd.Parameters.AddWithValue("@SubscriberEnquiryID", _SubscriberEnquiryID);
            cmd.Parameters.AddWithValue("@Response", response);
            cmd.Parameters.AddWithValue("@Request", request);
            cmd.Parameters.AddWithValue("@Error", error);
            cmd.Parameters.AddWithValue("@SubmissionDate", System.DateTime.Now);

            if (con.State == ConnectionState.Closed)
                con.Open();

            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static DataTable ConvertToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            // Get properties of the class
            var properties = typeof(T).GetProperties();
            foreach (var prop in properties)
            {
                // Create a column in the DataTable for each property
                dataTable.Columns.Add(prop.Name, prop.PropertyType);
            }

            // Add rows to the DataTable
            foreach (var item in items)
            {
                var values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

    }

    public class TokenForQualification
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public int refresh_expires_in { get; set; }
        public string refresh_token { get; set; }
        public string token_type { get; set; }
        public string id_token { get; set; }
        public int notbeforepolicy { get; set; }
        public string session_state { get; set; }
        public string scope { get; set; }
    }




}