﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Data;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Reflection;
using System.ServiceModel;
using FreeImageAPI;
using XDSPortalLibrary.Entity_Layer;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Web;
using XDSDataLibrary.LexisXGServices;
using System.Xml;

namespace XDSDataLibrary
{
    public class ReportAccess
    {
        public DataSet ds;
        public DataSet dsTracePlus = new DataSet();
        public string strCon;
        SqlConnection constring = null;
        DataTable dtError;
        public bool displayUnusableInfo = false;
        object outputparameters = null;

        public string FnSubstring(string input, int startIndex, int numberOfCharacters)
        {

            if (string.IsNullOrEmpty(input) || startIndex < 0)
            {
                return string.Empty;
            }
            else if (numberOfCharacters + startIndex > input.Length)
            {
                numberOfCharacters = input.Length - startIndex;
                return input.Substring(startIndex, numberOfCharacters).ToUpper();
            }
            else
            {
                return input.Substring(startIndex, numberOfCharacters).ToUpper();
            }
        }

        public static bool FnIsAlphaString(string input)
        {
            Regex rg = new Regex("^[a-zA-Z]+$");

            return rg.IsMatch(input);
        }

        public ReportAccess(string AdminConnection, string ReportConnectionString)
        {
            strCon = ReportConnectionString;
            constring = new SqlConnection(AdminConnection);
            dtError = new DataTable("Error");
            dtError.Columns.Add("ErrorDescription");

        }

        private void ProcessReport(object strSql)
        {
           // File.AppendAllText(@"C:\Log\Response.txt", strSql + "\n");
            SqlConnection con = new SqlConnection(strCon);
            try
            {
                DataSet dsLive = new DataSet();
                SqlCommand ObjReportProcCMd = new SqlCommand(strSql.ToString(), con);
                con.Open();

                ObjReportProcCMd.CommandTimeout = 0;
                ObjReportProcCMd.CommandType = CommandType.Text;

                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);
                ObjDAReport.Fill(dsLive);

                con.Close();

                if (dsLive.Tables.Count > 0)
                {
                    for (int i = 0; i < dsLive.Tables.Count; i++)
                    {
                      //  File.AppendAllText(@"C:\Log\Response.txt", strSql + ":" + dsLive.Tables.Count.ToString() + "\n");
                        
                        if (dsLive.Tables[i].Rows.Count > 0)
                        {
                            if (dsLive.Tables[i].Columns.Contains("TableName"))
                            {
                             //   File.AppendAllText(@"C:\Log\Response.txt", strSql + ":" + dsLive.Tables[i].Rows[0]["TableName"].ToString() + "\n");

                                dsLive.Tables[i].TableName = dsLive.Tables[i].Rows[0]["TableName"].ToString();
                                dsLive.Tables[i].Columns.Remove("TableName");
                            }

                            //File.AppendAllText(@"C:\Log\Response.txt", "Copy Done\n");
                            ds.Tables.Add(dsLive.Tables[i].Copy());
                            //File.AppendAllText(@"C:\Log\Response.txt", "Copy Done1\n");

                            //foreach (DataTable dt in ds.Tables)
                            //{
                            //    File.AppendAllText(@"C:\Log\Response.txt", dt.TableName + " report :" + dt.Rows.Count.ToString() + "\n");
                            //}
                        }
                    }
                }
                con.Close();
                con.Dispose();
                ObjReportProcCMd.Dispose();
                dsLive.Clear();
                dsLive.Dispose();
                SqlConnection.ClearPool(con);

            }
            catch (Exception ex)
            {
                con.Close();
                SqlConnection.ClearPool(con);
                DataRow drError = dtError.NewRow();
                File.AppendAllText(@"C:\Log\Response.txt", strSql + " " + ex.Message);
                drError["ErrorDescription"] =   ex.Message;
                dtError.Rows.Add(drError);
            }
        }

        private void ProcessReportoutputparameters(object Parameters)
        {
            object[] inputparameters = (object[])Parameters;

            string strSql = string.Empty;
            SqlParameterCollection oparamcollection = null;

            strSql = inputparameters[0].ToString();

            oparamcollection = (SqlParameterCollection)inputparameters[1];

            SqlConnection con = new SqlConnection(strCon);
            try
            {

                DataSet dsLive = new DataSet();

                SqlCommand ObjReportProcCMd = new SqlCommand(strSql.ToString(), con);

                con.Open();

                ObjReportProcCMd.CommandTimeout = 0;
                ObjReportProcCMd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);

                foreach (SqlParameter oparam in oparamcollection)
                {
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = oparam.ParameterName;
                    param.TypeName = oparam.TypeName;
                    param.SqlDbType = oparam.SqlDbType;
                    param.Value = oparam.Value;
                    param.Direction = oparam.Direction;
                    param.Size = oparam.Size;

                  
                    //ObjReportProcCMd.Parameters.Add(param);
                    ObjDAReport.SelectCommand.Parameters.Add(param);
                }                
                
                ObjDAReport.Fill(dsLive);

                con.Close();

                foreach (SqlParameter oparam in oparamcollection)
                {
                    if (oparam.Direction == ParameterDirection.Output)
                    {
                        foreach (SqlParameter op in ObjReportProcCMd.Parameters)
                        {
                            if (oparam.ParameterName == op.ParameterName)
                            {
                                oparam.Value = Convert.ToString(op.Value);

                             
                            }
                        }
                    }
                }

                outputparameters = oparamcollection;

                if (dsLive.Tables.Count > 0)
                {

                    for (int i = 0; i < dsLive.Tables.Count; i++)
                    {
                        if (dsLive.Tables[i].Rows.Count > 0)
                        {
                            if (dsLive.Tables[i].Columns.Contains("TableName"))
                            {
                                dsLive.Tables[i].TableName = dsLive.Tables[i].Rows[0]["TableName"].ToString();
                                dsLive.Tables[i].Columns.Remove("TableName");
                            }
                            ds.Tables.Add(dsLive.Tables[i].Copy());


                        }
                    }
                }
                con.Close();
                con.Dispose();
                ObjReportProcCMd.Dispose();
                dsLive.Clear();
                dsLive.Dispose();
                SqlConnection.ClearPool(con);

            }
            catch (Exception ex)
            {
                con.Close();
                SqlConnection.ClearPool(con);
                DataRow drError = dtError.NewRow();
                drError["ErrorDescription"] = inputparameters[0].ToString() + ex.Message;
                dtError.Rows.Add(drError);
            }
        }

        private void ProcessACFEReport(object strSql)
        {
            SqlConnection con = new SqlConnection(strCon);
            try
            {
                DataSet dsLive = new DataSet();
                SqlCommand ObjReportProcCMd = new SqlCommand(strSql.ToString(), con);
                con.Open();

                ObjReportProcCMd.CommandTimeout = 0;
                ObjReportProcCMd.CommandType = CommandType.Text;

                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);
                ObjDAReport.Fill(dsLive);

                con.Close();

                if (dsLive.Tables.Count > 0)
                {
                    for (int i = 0; i < dsLive.Tables.Count; i++)
                    {
                        if (dsLive.Tables[i].Rows.Count > 0)
                        {
                            if (dsLive.Tables[i].Columns.Contains("TableName"))
                            {
                                dsLive.Tables[i].TableName = dsLive.Tables[i].Rows[0]["TableName"].ToString();
                                dsLive.Tables[i].Columns.Remove("TableName");
                            }

                            if (!ds.Tables.Contains(dsLive.Tables[i].TableName))
                            {
                                if (dsLive.Tables[i].TableName == "ConsumerDetail" && dsLive.Tables[i].Columns.Contains("XDSDeceasedStatus"))
                                { }
                                else
                                    ds.Tables.Add(dsLive.Tables[i].Copy());
                            }
                        }
                    }
                }
                con.Close();
                con.Dispose();
                ObjReportProcCMd.Dispose();
                dsLive.Clear();
                dsLive.Dispose();
                SqlConnection.ClearPool(con);

            }
            catch (Exception ex)
            {
                con.Close();
                SqlConnection.ClearPool(con);
                DataRow drError = dtError.NewRow();
                drError["ErrorDescription"] = ex.Message;
                dtError.Rows.Add(drError);
            }
        }

        private void ProcessParameterizedReport(object Parameters)
        {
            object[] inputparameters = (object[])Parameters;

            string strSql = string.Empty;
            SqlParameterCollection oparamcollection = null;
            
            strSql = inputparameters[0].ToString();
            
            oparamcollection = (SqlParameterCollection)inputparameters[1];
            
            SqlConnection con = new SqlConnection(strCon);
            try
            {
             
                DataSet dsLive = new DataSet();
            
                SqlCommand ObjReportProcCMd = new SqlCommand(strSql.ToString(), con);
            
                con.Open();

                ObjReportProcCMd.CommandTimeout = 0;
                ObjReportProcCMd.CommandType = CommandType.Text;

                foreach (SqlParameter oparam in oparamcollection)
                {
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = oparam.ParameterName;
                    param.TypeName = oparam.TypeName;
                    param.SqlDbType = oparam.SqlDbType;
                    param.Value = oparam.Value;

                    ObjReportProcCMd.Parameters.Add(param);
                }

                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);
                ObjDAReport.Fill(dsLive);

                con.Close();

                if (dsLive.Tables.Count > 0)
                {
                    
                    for (int i = 0; i < dsLive.Tables.Count; i++)
                    {
                        if (dsLive.Tables[i].Rows.Count > 0)
                        {
                            if (dsLive.Tables[i].Columns.Contains("TableName"))
                            {
                                dsLive.Tables[i].TableName = dsLive.Tables[i].Rows[0]["TableName"].ToString();
                                dsLive.Tables[i].Columns.Remove("TableName");
                            }
                            ds.Tables.Add(dsLive.Tables[i].Copy());

                            
                        }
                    }
                }
                con.Close();
                con.Dispose();
                ObjReportProcCMd.Dispose();
                dsLive.Clear();
                dsLive.Dispose();
                SqlConnection.ClearPool(con);

            }
            catch (Exception ex)
            {
                con.Close();
                SqlConnection.ClearPool(con);
                DataRow drError = dtError.NewRow();
                drError["ErrorDescription"] = inputparameters[0].ToString() + ex.Message;
                dtError.Rows.Add(drError);
            }
        }

        private void FootPrintSection(object strSql)
        {
            SqlConnection con = new SqlConnection(strCon);
            try
            {
                DataSet dsLive = new DataSet();
                SqlCommand ObjReportProcCMd = new SqlCommand(strSql.ToString(), con);
                con.Open();

                ObjReportProcCMd.CommandTimeout = 0;
                ObjReportProcCMd.CommandType = CommandType.Text;

                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);
                ObjDAReport.Fill(dsLive);

                con.Close();

                if (dsLive.Tables.Count > 0)
                {
                    for (int i = 0; i < dsLive.Tables.Count; i++)
                    {
                        if (dsLive.Tables[i].Rows.Count > 0)
                        {
                            if (dsLive.Tables[i].Columns.Contains("TableName"))
                            {
                                dsLive.Tables[i].TableName = dsLive.Tables[i].Rows[0]["TableName"].ToString();
                                dsLive.Tables[i].Columns.Remove("TableName");
                            }
                            ds.Tables.Add(dsLive.Tables[i].Copy());
                        }
                    }
                }
                con.Close();
                con.Dispose();
                ObjReportProcCMd.Dispose();
                dsLive.Clear();
                dsLive.Dispose();
                SqlConnection.ClearPool(con);
            }
            catch (Exception ex)
            {
                con.Close();
                SqlConnection.ClearPool(con);
                DataRow drError = dtError.NewRow();
                drError["ErrorDescription"] = ex.Message;
                dtError.Rows.Add(drError);
            }
        }

        private void ProcessTracePLus(object strSql)
        {
            SqlConnection con = new SqlConnection(strCon);
            try
            {
                string tname = Guid.NewGuid().ToString();
                DataTable dt = new DataTable(tname);

                SqlCommand ObjReportProcCMd = new SqlCommand(strSql.ToString(), con);
                con.Open();

                ObjReportProcCMd.CommandTimeout = 0;
                ObjReportProcCMd.CommandType = CommandType.Text;

                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);
                ObjDAReport.Fill(dsTracePlus, "Segments");

                con.Close();
                con.Dispose();
                ObjReportProcCMd.Dispose();
                SqlConnection.ClearPool(con);
            }
            catch (Exception ex)
            {
                con.Close();
                SqlConnection.ClearPool(con);
                DataRow drError = dtError.NewRow();
                drError["ErrorDescription"] = ex.Message;
                dtError.Rows.Add(drError);
            }
        }

        public string GetTracePlusResult(string strConsumerID, string strDataSegment)
        {
            string strBonusXML = string.Empty;
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(strDataSegment));
                string AssociationCode = dsDataSegment.Tables[0].Rows[0]["AssociationCode"].ToString();
                string ReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                dsTracePlus = new DataSet("BonusSegments");

                DataTable dtTracePlus = new DataTable("Segments");

                DataColumn dcDataSegmentID = new DataColumn("DataSegmentID");
                DataColumn dcDataSegmentName = new DataColumn("DataSegmentName");
                DataColumn dcDataSegmentDisplayText = new DataColumn("DataSegmentDisplayText");
                DataColumn dcBonusViewed = new DataColumn("BonusViewed");
                DataColumn dcBonusPrice = new DataColumn("BonusPrice");

                dtTracePlus.Columns.Add(dcDataSegmentID);
                dtTracePlus.Columns.Add(dcDataSegmentName);
                dtTracePlus.Columns.Add(dcDataSegmentDisplayText);
                dtTracePlus.Columns.Add(dcBonusViewed);
                dtTracePlus.Columns.Add(dcBonusPrice);

                dsTracePlus.Tables.Add(dtTracePlus);

                Thread thPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thConfirmation = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thSMSnotify = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thEmailnotify = new Thread(new ParameterizedThreadStart(ProcessTracePLus));

                string strSQLPropertyInformation = string.Empty;
                string strSQLDirectorShipLink = string.Empty;
                string strSQLTelephoneLinkage = string.Empty;
                string strSQLConfirmation = string.Empty;
                string strSMSNotify = string.Empty;
                string strEmailNotify = string.Empty;


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    int intPayAsYouGo = 0;
                    if (dr["PayAsYouGo"].ToString().ToLower() == "true")
                        intPayAsYouGo = 1;

                    if ((dr["DataSegmentID"].ToString() == "20") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLPropertyInformation = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "20", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thPropertyInformation.Start(strSQLPropertyInformation);
                    }
                    if ((dr["DataSegmentID"].ToString() == "19") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLDirectorShipLink = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "19", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thDirectorShipLink.Start(strSQLDirectorShipLink);
                    }
                    if ((dr["DataSegmentID"].ToString() == "44") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLTelephoneLinkage = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "44", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thTelephoneLinkage.Start(strSQLTelephoneLinkage);
                    }
                    if ((dr["DataSegmentID"].ToString() == "37") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLConfirmation = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "37", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thConfirmation.Start(strSQLConfirmation);
                    }
                    if ((dr["DataSegmentID"].ToString() == "69") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSMSNotify = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "69", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thSMSnotify.Start(strSMSNotify);
                    }
                    if ((dr["DataSegmentID"].ToString() == "70") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strEmailNotify = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "70", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thEmailnotify.Start(strEmailNotify);
                    }
                }

                if (!string.IsNullOrEmpty(strSQLPropertyInformation))
                    thPropertyInformation.Join();
                if (!string.IsNullOrEmpty(strSQLDirectorShipLink))
                    thDirectorShipLink.Join();
                if (!string.IsNullOrEmpty(strSQLTelephoneLinkage))
                    thTelephoneLinkage.Join();
                if (!string.IsNullOrEmpty(strSQLConfirmation))
                    thConfirmation.Join();
                if (!string.IsNullOrEmpty(strSMSNotify))
                    thSMSnotify.Join();
                if (!string.IsNullOrEmpty(strEmailNotify))
                    thEmailnotify.Join();


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }
                else
                {
                    strBonusXML = dsTracePlus.GetXml();
                }

                dsTracePlus.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return strBonusXML;
        }


        public string GetTraceLiteResult(string strConsumerID, string strDataSegment)
        {
            string strBonusXML = string.Empty;
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(strDataSegment));
                string AssociationCode = dsDataSegment.Tables[0].Rows[0]["AssociationCode"].ToString();
                string ReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                dsTracePlus = new DataSet("BonusSegments");

                DataTable dtTracePlus = new DataTable("Segments");

                DataColumn dcDataSegmentID = new DataColumn("DataSegmentID");
                DataColumn dcDataSegmentName = new DataColumn("DataSegmentName");
                DataColumn dcDataSegmentDisplayText = new DataColumn("DataSegmentDisplayText");
                DataColumn dcBonusViewed = new DataColumn("BonusViewed");
                DataColumn dcBonusPrice = new DataColumn("BonusPrice");

                dtTracePlus.Columns.Add(dcDataSegmentID);
                dtTracePlus.Columns.Add(dcDataSegmentName);
                dtTracePlus.Columns.Add(dcDataSegmentDisplayText);
                dtTracePlus.Columns.Add(dcBonusViewed);
                dtTracePlus.Columns.Add(dcBonusPrice);

                dsTracePlus.Tables.Add(dtTracePlus);

                Thread thPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thConfirmation = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thSMSnotify = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thEmailnotify = new Thread(new ParameterizedThreadStart(ProcessTracePLus));

                string strSQLPropertyInformation = string.Empty;
                string strSQLDirectorShipLink = string.Empty;
                string strSQLTelephoneLinkage = string.Empty;
                string strSQLConfirmation = string.Empty;
                string strSMSNotify = string.Empty;
                string strEmailNotify = string.Empty;


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    int intPayAsYouGo = 0;
                    if (dr["PayAsYouGo"].ToString().ToLower() == "true")
                        intPayAsYouGo = 1;

                    if ((dr["DataSegmentID"].ToString() == "20") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLPropertyInformation = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, 
                            "20", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thPropertyInformation.Start(strSQLPropertyInformation);
                    }
                    if ((dr["DataSegmentID"].ToString() == "19") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLDirectorShipLink = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID,
                            "19", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thDirectorShipLink.Start(strSQLDirectorShipLink);
                    }
                    if ((dr["DataSegmentID"].ToString() == "44") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLTelephoneLinkage = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID,
                            "44", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thTelephoneLinkage.Start(strSQLTelephoneLinkage);
                    }
                    if ((dr["DataSegmentID"].ToString() == "37") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLConfirmation = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, 
                            "37", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thConfirmation.Start(strSQLConfirmation);
                    }
                    if ((dr["DataSegmentID"].ToString() == "69") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSMSNotify = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, 
                            "69", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thSMSnotify.Start(strSMSNotify);
                    }
                    if ((dr["DataSegmentID"].ToString() == "70") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strEmailNotify = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID,
                            "70", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thEmailnotify.Start(strEmailNotify);
                    }
                }

                if (!string.IsNullOrEmpty(strSQLPropertyInformation))
                    thPropertyInformation.Join();
                if (!string.IsNullOrEmpty(strSQLDirectorShipLink))
                    thDirectorShipLink.Join();
                if (!string.IsNullOrEmpty(strSQLTelephoneLinkage))
                    thTelephoneLinkage.Join();
                if (!string.IsNullOrEmpty(strSQLConfirmation))
                    thConfirmation.Join();
                if (!string.IsNullOrEmpty(strSMSNotify))
                    thSMSnotify.Join();
                if (!string.IsNullOrEmpty(strEmailNotify))
                    thEmailnotify.Join();


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }
                else
                {
                    strBonusXML = dsTracePlus.GetXml();
                }

                dsTracePlus.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return strBonusXML;
        }


        public string GetBiometricPlusResult(string strConsumerID, string strDataSegment)
        {
            string strBonusXML = string.Empty;
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(strDataSegment));
                string AssociationCode = dsDataSegment.Tables[0].Rows[0]["AssociationCode"].ToString();
                string ReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                dsTracePlus = new DataSet("BonusSegments");

                DataTable dtTracePlus = new DataTable("Segments");

                DataColumn dcDataSegmentID = new DataColumn("DataSegmentID");
                DataColumn dcDataSegmentName = new DataColumn("DataSegmentName");
                DataColumn dcDataSegmentDisplayText = new DataColumn("DataSegmentDisplayText");
                DataColumn dcBonusViewed = new DataColumn("BonusViewed");
                DataColumn dcBonusPrice = new DataColumn("BonusPrice");

                dtTracePlus.Columns.Add(dcDataSegmentID);
                dtTracePlus.Columns.Add(dcDataSegmentName);
                dtTracePlus.Columns.Add(dcDataSegmentDisplayText);
                dtTracePlus.Columns.Add(dcBonusViewed);
                dtTracePlus.Columns.Add(dcBonusPrice);

                dsTracePlus.Tables.Add(dtTracePlus);

                Thread thPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thConfirmation = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thSMSnotify = new Thread(new ParameterizedThreadStart(ProcessTracePLus));
                Thread thEmailnotify = new Thread(new ParameterizedThreadStart(ProcessTracePLus));

                string strSQLPropertyInformation = string.Empty;
                string strSQLDirectorShipLink = string.Empty;
                string strSQLTelephoneLinkage = string.Empty;
                string strSQLConfirmation = string.Empty;
                string strSMSNotify = string.Empty;
                string strEmailNotify = string.Empty;


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    int intPayAsYouGo = 0;
                    if (dr["PayAsYouGo"].ToString().ToLower() == "true")
                        intPayAsYouGo = 1;

                    if ((dr["DataSegmentID"].ToString() == "20") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLPropertyInformation = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "20", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thPropertyInformation.Start(strSQLPropertyInformation);
                    }
                    if ((dr["DataSegmentID"].ToString() == "19") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLDirectorShipLink = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "19", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thDirectorShipLink.Start(strSQLDirectorShipLink);
                    }
                    if ((dr["DataSegmentID"].ToString() == "44") && (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLTelephoneLinkage = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "44", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thTelephoneLinkage.Start(strSQLTelephoneLinkage);
                    }
                    if ((dr["DataSegmentID"].ToString() == "37") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSQLConfirmation = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "37", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thConfirmation.Start(strSQLConfirmation);
                    }
                    if ((dr["DataSegmentID"].ToString() == "69") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strSMSNotify = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "69", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thSMSnotify.Start(strSMSNotify);
                    }
                    if ((dr["DataSegmentID"].ToString() == "70") & (dr["BonusViewed"].ToString().ToLower() == "true"))
                    {
                        strEmailNotify = string.Format("execute spCLR_TracePlus {0},{1},{2},{3},{4},{5}", strConsumerID, "70", dr["BonusPrice"].ToString(), intPayAsYouGo.ToString(), AssociationCode, ReportID);
                        thEmailnotify.Start(strEmailNotify);
                    }
                }

                if (!string.IsNullOrEmpty(strSQLPropertyInformation))
                    thPropertyInformation.Join();
                if (!string.IsNullOrEmpty(strSQLDirectorShipLink))
                    thDirectorShipLink.Join();
                if (!string.IsNullOrEmpty(strSQLTelephoneLinkage))
                    thTelephoneLinkage.Join();
                if (!string.IsNullOrEmpty(strSQLConfirmation))
                    thConfirmation.Join();
                if (!string.IsNullOrEmpty(strSMSNotify))
                    thSMSnotify.Join();
                if (!string.IsNullOrEmpty(strEmailNotify))
                    thEmailnotify.Join();


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }
                else
                {
                    strBonusXML = dsTracePlus.GetXml();
                }

                dsTracePlus.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return strBonusXML;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerMatch(XDSPortalLibrary.Entity_Layer.Response ObjResponse, bool bBonusCheck, string strDataSegments)
        {
            try
            {
                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "ConsumerDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);


                    if (bBonusCheck)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["BonusXML"] = "";
                            string strBonusXML = GetTracePlusResult(dr["ConsumerID"].ToString(), strDataSegments);
                            if (strBonusXML != "<BonusSegments />")
                                dr["BonusXML"] = strBonusXML;
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["BonusXML"] = "";
                            dr["TempReference"] = "";
                        }

                        
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseKeyType = "C";
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseKeyType = "C";
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseKeyType = "C";
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                if (dsTracePlus != null)
                {
                    dsTracePlus.Clear();
                    dsTracePlus.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerIDBiometricMatch(XDSPortalLibrary.Entity_Layer.Response ObjResponse, bool bBonusCheck, string strDataSegments)
        {
            try
            {
                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "ConsumerDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);


                    if (bBonusCheck)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["BonusXML"] = "";
                            string strBonusXML = GetBiometricPlusResult(dr["ConsumerID"].ToString(), strDataSegments);
                            if (strBonusXML != "<BonusSegments />")
                                dr["BonusXML"] = strBonusXML;
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["BonusXML"] = "";
                            dr["TempReference"] = "";
                        }
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseKeyType = "C";
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseKeyType = "C";
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseKeyType = "C";
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                if (dsTracePlus != null)
                {
                    dsTracePlus.Clear();
                    dsTracePlus.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerTraceMatch(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDno) && String.IsNullOrEmpty(ObjSearch.Passportno) && (String.IsNullOrEmpty(ObjSearch.Surname) || (ObjSearch.DOB.Year <= 1900) || String.IsNullOrEmpty(ObjSearch.FirstName)))
                {
                    throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");

                string strSQL = string.Empty;

                if (ObjSearch.ProductID == 146)
                {
                    XDSData.Common.Functions oFunctions = new XDSData.Common.Functions();
                    if (!oFunctions.FnValidateIDNo(ObjSearch.IDno))
                    {
                        if (!FnIsAlphaString(ObjSearch.IDno))
                        {
                            ObjSearch.Passportno = ObjSearch.IDno;
                            ObjSearch.IDno = string.Empty;
                        }
                        else
                        {
                            throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                        }
                    }
                    strSQL = string.Format("execute spConsumer_S_Match_Custom_Jd_ShortCode '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}',{9}", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate,ObjSearch.subscriberID.ToString());
                }
                else
                {
                    strSQL = string.Format("execute spConsumer_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate);
                }


                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerTraceMatchTraceLite(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDno) && String.IsNullOrEmpty(ObjSearch.Passportno) && (String.IsNullOrEmpty(ObjSearch.Surname) || (ObjSearch.DOB.Year <= 1900) || String.IsNullOrEmpty(ObjSearch.FirstName)))
                {
                    throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");

                string strSQL = string.Empty;

                if (ObjSearch.ProductID == 146)
                {
                    XDSData.Common.Functions oFunctions = new XDSData.Common.Functions();
                    if (!oFunctions.FnValidateIDNo(ObjSearch.IDno))
                    {
                        if (!FnIsAlphaString(ObjSearch.IDno))
                        {
                            ObjSearch.Passportno = ObjSearch.IDno;
                            ObjSearch.IDno = string.Empty;
                        }
                        else
                        {
                            throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                        }
                    }
                    strSQL = string.Format("execute spConsumer_S_Match_Custom_Jd_ShortCode '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}',{9}", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate, ObjSearch.subscriberID.ToString());
                }
                else
                {
                    strSQL = string.Format("execute spConsumer_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate);
                }


                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }


        public XDSPortalLibrary.Entity_Layer.Response ConsumerMatchCustomC4L(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (ObjSearch.IsCustomVetting)
                {
                    if (String.IsNullOrEmpty(ObjSearch.IDno))
                    {
                        throw new Exception("ID Number is mandatory.");
                    }
                    //else if (String.IsNullOrEmpty(ObjSearch.RequestNo.ToString()))
                    //{
                    //    throw new Exception("RequestNo is mandatory.");
                    //}
                    else if (String.IsNullOrEmpty(ObjSearch.FirstName))
                    {
                        throw new Exception("Customer Name is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.Surname))
                    {
                        throw new Exception("Customer Surname is mandatory.");
                    }
                }

                if ((String.IsNullOrEmpty(ObjSearch.IDno) || String.IsNullOrEmpty(ObjSearch.Passportno)) && (String.IsNullOrEmpty(ObjSearch.Surname)))
                {
                    throw new Exception("Your search input must have an Id Number and Surname or a Passport Number and Surname");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");

                string strSQL = string.Format("execute spConsumer_S_Match_Custom_C4L '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate, ObjSearch.subscriberID);
                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerTraceMatch(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, string ConsumerIDs)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                //if (String.IsNullOrEmpty(ObjSearch.IDno) && String.IsNullOrEmpty(ObjSearch.Passportno) && (String.IsNullOrEmpty(ObjSearch.Surname) || (ObjSearch.DOB.Year <= 1900) || String.IsNullOrEmpty(ObjSearch.FirstName)))
                //{
                //    throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                //}

                //string strFirstInitial = "";
                //string strSecondInitial = "";
                //if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                //    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                //if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                //    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                //string strBirthDate = "";
                //if (ObjSearch.DOB.Year > 1753)
                //{
                //    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                //}

                ds = new DataSet("ListOfConsumers");

                string strSQL = string.Format("Select a.ConsumerID,a.FirstName, a.SecondName, a.ThirdName, a.Surname, a.IDNo, a.PassportNo, a.BirthDate,a.GenderInd from Consumer a Where a.ConsumerID in (" + ConsumerIDs + ")");
                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerCreditEnquiryMatch(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDno) && String.IsNullOrEmpty(ObjSearch.Passportno) && (String.IsNullOrEmpty(ObjSearch.Surname) || (ObjSearch.DOB.Year <= 1900) || String.IsNullOrEmpty(ObjSearch.FirstName)))
                {
                    throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spConsumer_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate);
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerCreditApplicationMatch(XDSPortalLibrary.Entity_Layer.ConsumerCreditApplication ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDNo) && String.IsNullOrEmpty(ObjSearch.PassportNo) && (String.IsNullOrEmpty(ObjSearch.SurName) || (ObjSearch.DOB.Year <= 1900) || String.IsNullOrEmpty(ObjSearch.FirstName)))
                {
                    throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spConsumer_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}'", ObjSearch.IDNo.Replace("'", "''"), ObjSearch.PassportNo.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.SurName.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate);
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerAccountMatch(XDSPortalLibrary.Entity_Layer.ConsumerAccountTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.Accountno))
                {
                    throw new Exception("Accountno is mandatory");
                }

                ds = new DataSet("ListOfConsumers");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spConsumer_S_Match_AccountNo '{0}', '{1}','{2}'", ObjSearch.Accountno.Replace("'", "''"), ObjSearch.SubAccountno.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerAddressMatch(XDSPortalLibrary.Entity_Layer.ConsumerAddress ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.Province) || String.IsNullOrEmpty(ObjSearch.City) || String.IsNullOrEmpty(ObjSearch.Suburb) || String.IsNullOrEmpty(ObjSearch.PostalCode) || String.IsNullOrEmpty(ObjSearch.StreetName))
                {
                    if (ObjSearch.Country == "South Africa")
                    {
                        throw new Exception("The combination of Province, City, Suburb, PostalCode, StreetName or Province, City, Suburb, PostalCode, Postal Number is Mandatory");
                    }
                }

                ds = new DataSet("ListOfConsumers");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute Search '{0}', '{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}'", "", ObjSearch.Country.Replace("'", "''"), ObjSearch.Province.Replace("'", "''"), ObjSearch.City.Replace("'", "''"), ObjSearch.Suburb.Replace("'", "''"), ObjSearch.PostalCode.Replace("'", "''"), ObjSearch.StreetName.Replace("'", "''"), ObjSearch.StreetNo.Replace("'", "''"), ObjSearch.BuildingName.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerTelephoneMatch(XDSPortalLibrary.Entity_Layer.ConsumerTelephone ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.TelePhoneNo))
                {
                    throw new Exception("Telephone no is mandatory");
                }

                ds = new DataSet("ListOfConsumers");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spConsumer_S_Match_Telephone '{0}', '{1}','{2}'", ObjSearch.TelephoneCode.Replace("'", "''"), ObjSearch.TelePhoneNo.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerEasyMatch(XDSPortalLibrary.Entity_Layer.ConsumerEasyTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.TypeOfSearch) || String.IsNullOrEmpty(ObjSearch.Surname) || ObjSearch.Age == 0 || ObjSearch.Year == 0)
                {
                    throw new Exception("Please enter data to all mandatory fileds");
                }
                if (ObjSearch.Age >= DateTime.Now.Year)
                {
                    throw new Exception("Please enter valid Age");
                }
                if (ObjSearch.Year > DateTime.Now.Year)
                {
                    throw new Exception("Please enter valid Age and Year");
                }

                ds = new DataSet("ListOfConsumers");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spConsumer_S_Match_Easy '{0}', '{1}','{2}', '{3}', {4}, {5}, {6}, '{7}','{8}','{9}'", ObjSearch.Firstname.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.Gender.Replace("'", "''"), ObjSearch.Age.ToString(), ObjSearch.Year.ToString(), ObjSearch.Deviation.ToString(), ObjSearch.Firstinitial.Replace("'", "''"), ObjSearch.SecondInitial.Replace("'", "''"), ObjSearch.TypeOfSearch.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerIDVerificationMatch(XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                DateTime dtissueddate = new DateTime();
                if (String.IsNullOrEmpty(ObjSearch.IDno))
                {
                    throw new Exception("IDno is mandatory");
                }

                if (!string.IsNullOrEmpty(ObjSearch.IDIssuedDate))
                {
                    if (!DateTime.TryParseExact(ObjSearch.IDIssuedDate, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtissueddate))
                    {
                        throw new Exception("Please enter the Date of Issue as shouwn on the ID Book/Card for Date o Issue Verification to Work");
                    }
                }

               
                ds = new DataSet("ListOfConsumers");

                string strSQL = string.Empty;

                if (ObjSearch.SubscriberAssociationCode.ToUpper() == "STDIDV13" )
                {
                     strSQL = string.Format("execute spConsumer_S_Match_IDVerification_Custom '{0}'", ObjSearch.IDno.Replace("'", "''"));
                }
                else if ( ObjSearch.SubscriberAssociationCode.ToUpper() == "JDCUSTOM")
                {
                    strSQL = string.Format("execute spConsumer_S_Match_IDVerification_Custom_Name '{0}','{1}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"));
                }
                else
                {
                     strSQL = string.Format("execute spConsumer_S_Match_IDVerification '{0}'", ObjSearch.IDno.Replace("'", "''"));
                }
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "ConsumerDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.ConsumerID = Convert.ToInt32(dr["ConsumerID"]);
                        ObjSearch.HomeAffairsID = Convert.ToInt32(dr["HomeAffairsID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ObjSearch.HomeAffairsID > 0)
                    {
                        ObjResponse.ResponseKeyType = "H";
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.HomeAffairsID);

                    }
                    else
                    {
                        ObjResponse.ResponseKeyType = "C";
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    }


                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerPublicDomainMatch(XDSPortalLibrary.Entity_Layer.PublicDomainEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDno) && String.IsNullOrEmpty(ObjSearch.Passportno) && (String.IsNullOrEmpty(ObjSearch.Surname) || (ObjSearch.DOB.Year <= 1900) || String.IsNullOrEmpty(ObjSearch.FirstName)))
                {
                    throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");


                string strSQL = string.Format("execute spConsumer_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate);

                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        private bool ValidateRegNo(string s)
        {
            bool invalid = false;

            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"\d{4}/\d{6}/\d{2}");

            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"[a-zA-Z]\d{4}/\d{6}/\d{2}");

            if (s.Length < 14 || s.Length > 15)
            {
                throw new Exception("Registrationno must be 12 or 13 digits");
            }
            else if (s.Length == 14 && !reg.IsMatch(s))
            {
                throw new Exception("Invalid Registrationno Supplied");
            }
            else if (s.Length == 15 && !reg1.IsMatch(s))
            {
                throw new Exception("Registrationno must be 13 digits  starts with Alphabet");
            }


            return invalid;

        }

        public XDSPortalLibrary.Entity_Layer.Response BusinessEnquiryMatch(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.RegistrationNo))
                    ObjSearch.RegistrationNo = "//";

                if (ObjSearch.RegistrationNo == "//" && String.IsNullOrEmpty(ObjSearch.BusinessName) && String.IsNullOrEmpty(ObjSearch.VATNumber) && String.IsNullOrEmpty(ObjSearch.SolePropIDNo))
                {
                    throw new Exception("Either Registrationno or busniessname or VATNumber or Sole Proprieter ID must be supplied");
                }
                else if (ObjSearch.RegistrationNo != "//")
                {
                    ValidateRegNo(ObjSearch.RegistrationNo);
                }

                ds = new DataSet("DsCommercialMatch");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spCommercial_S_Match_Standard '{0}', '{1}', '{2}', '{3}', '{4}'", ObjSearch.RegistrationNo.Replace("'", "''"), ObjSearch.BusinessName.Replace("'", "''"), ObjSearch.VATNumber.Replace("'", "''"), ObjSearch.SolePropIDNo.Replace("'", "''"), ObjSearch.TrustNo.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "CommercialDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.CommercialID = Convert.ToInt32(dr["CommercialID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response BusinessEnquiryMatchMIE(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.RegistrationNo))
                    ObjSearch.RegistrationNo = "//";

                if (ObjSearch.RegistrationNo == "//" && String.IsNullOrEmpty(ObjSearch.BusinessName) && String.IsNullOrEmpty(ObjSearch.VATNumber) && String.IsNullOrEmpty(ObjSearch.SolePropIDNo))
                {
                    throw new Exception("Either Registrationno or busniessname or VATNumber or Sole Proprieter ID must be supplied");
                }
                else if (ObjSearch.RegistrationNo != "//")
                {
                    ValidateRegNo(ObjSearch.RegistrationNo);
                }

                ds = new DataSet("DsCommercialMatch");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spCommercial_S_Match_Standard_CustomMIE '{0}', '{1}', '{2}', '{3}', '{4}'", ObjSearch.RegistrationNo.Replace("'", "''"), ObjSearch.BusinessName.Replace("'", "''"), ObjSearch.VATNumber.Replace("'", "''"), ObjSearch.SolePropIDNo.Replace("'", "''"), ObjSearch.TrustNo.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "CommercialDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.CommercialID = Convert.ToInt32(dr["CommercialID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        ObjResponse.ResponseKey = ObjSearch.CommercialID;
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count > 1)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        ObjResponse.ResponseKey = ObjSearch.CommercialID;
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response BusinessEnquiryMatchPlus(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.RegistrationNo))
                    ObjSearch.RegistrationNo = "//";

                if (ObjSearch.RegistrationNo == "//" && String.IsNullOrEmpty(ObjSearch.BusinessName))
                {
                    throw new Exception("Either Registrationno or busniessname must be supplied");
                }
                else if (ObjSearch.RegistrationNo != "//")
                {
                    ValidateRegNo(ObjSearch.RegistrationNo);
                }
                else if (!String.IsNullOrEmpty(ObjSearch.BusinessName) && (ObjSearch.BusinessName.Length < 3 || ObjSearch.BusinessName.Length > 120))
                {
                    throw new Exception("Length of businessname must be between 3 and 120");
                }

                ds = new DataSet("ListofEntities");

                
                string strSQL = string.Format("execute spCommercial_S_Match_Standard_Plus '{0}', '{1}'", ObjSearch.RegistrationNo.Replace("'", "''"), ObjSearch.BusinessName.Replace("'", "''"));                

                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "EntityDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.CommercialID = Convert.ToInt32(dr["EntityID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response BusinessEnquiryCustom(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();


            
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;


                if (String.IsNullOrEmpty(ObjSearch.RegistrationNo))
                    ObjSearch.RegistrationNo = "//";

                if (ObjSearch.RegistrationNo == "//" && String.IsNullOrEmpty(ObjSearch.BusinessName) && String.IsNullOrEmpty(ObjSearch.VATNumber) && String.IsNullOrEmpty(ObjSearch.SolePropIDNo))
                {
                    throw new Exception("Either Registrationno or busniessname or VATNumber or Sole Proprieter ID must be supplied");
                }
                else if (ObjSearch.RegistrationNo != "//")
                {
                    ValidateRegNo(ObjSearch.RegistrationNo);
                }

                ds = new DataSet("DsCommercialMatch");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spCommercial_S_Match_Standard_Custom '{0}', '{1}', '{2}', '{3}', '{4}',{5},{6}", ObjSearch.RegistrationNo.Replace("'", "''"), ObjSearch.BusinessName.Replace("'", "''"), ObjSearch.VATNumber.Replace("'", "''"), ObjSearch.SolePropIDNo.Replace("'", "''"), ObjSearch.TrustNo.Replace("'", "''"), ObjSearch.subscriberID, ObjSearch.ReportID);
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {

                    

                    ds.Tables[0].TableName = "CommercialDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    
                   

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.CommercialID = Convert.ToInt32(dr["CommercialID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";


                      

                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response BusinessEnquiryCustomNR(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch, string IDs)
        {
            string[] theIds = IDs.Split(',');

            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;


                if (String.IsNullOrEmpty(ObjSearch.RegistrationNo))
                    ObjSearch.RegistrationNo = "//";

                if (ObjSearch.RegistrationNo == "//" && String.IsNullOrEmpty(ObjSearch.BusinessName) && String.IsNullOrEmpty(ObjSearch.VATNumber) && String.IsNullOrEmpty(ObjSearch.SolePropIDNo))
                {
                    throw new Exception("Either Registrationno or Businessname or VATNumber or Sole Proprieter ID must be supplied");
                }
                else if (ObjSearch.RegistrationNo != "//")
                {
                    ValidateRegNo(ObjSearch.RegistrationNo);
                }

                ds = new DataSet("DsCommercialMatch");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spConsumer_S_Match_multiple '{0}', '{1}', '{2}', '{3}', '{4}',{5},{6},{7}", theIds[0],theIds[1],theIds[2],theIds[3],theIds[4],theIds[5],theIds[6],theIds[7]);
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {



                    ds.Tables[0].TableName = "CommercialDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);




                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.CommercialID = Convert.ToInt32(dr["CommercialID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";




                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }


        public XDSPortalLibrary.Entity_Layer.Response DirectorEnquiryMatch(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDno))
                {
                    throw new Exception("Your search input must have an Id Number");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.Firstname)))
                    strFirstInitial = ObjSearch.Firstname.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfDirectors");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spDirector_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Firstname.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), strBirthDate, strFirstInitial, strSecondInitial);
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "DirectorDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.DirectorID = Convert.ToInt32(dr["DirectorID"]);
                        ObjSearch.ConsumerID = Convert.ToInt32(dr["ConsumerID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        //Display user info
                        ObjResponse.ResponseKeyType = "D";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        ObjResponse.ResponseData = xmlstring;

                    }
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "D";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response DirectorEnquiryMatchLinkages(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.IDno.Trim()) && (string.IsNullOrEmpty(ObjSearch.Firstname.Trim()) || string.IsNullOrEmpty(ObjSearch.Surname.Trim())))
                {
                    throw new Exception("At least ID Number or both of First Name and Surname are required");
                }
                else if (!string.IsNullOrEmpty(ObjSearch.IDno.Trim()) && (ObjSearch.IDno.Trim().Length < 4 || ObjSearch.IDno.Trim().Length > 20))
                {                    
                        throw new Exception("Length of IDNumber must be between 4 and 20");
                }
                else if (!string.IsNullOrEmpty(ObjSearch.Firstname.Trim()) && (ObjSearch.Firstname.Trim().Length < 2 || ObjSearch.Firstname.Trim().Length > 120))
                {
                    throw new Exception("First Name must be at least length 2 and not more than 120 characters");                    
                }
                else if (!string.IsNullOrEmpty(ObjSearch.Surname.Trim()) && (ObjSearch.Surname.Trim().Length < 2 || ObjSearch.Surname.Trim().Length > 120))
                {
                    throw new Exception("Surname must be at least length 2 and not more than 120 characters");
                }


                ds = new DataSet("ListofEntities");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spDirector_S_Match_Standard_Linkage '{0}', '{1}','{2}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Firstname.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "EntityDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.DirectorID = Convert.ToInt32(dr["EntityID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        //Display user info
                        ObjResponse.ResponseKeyType = "D";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        ObjResponse.ResponseData = xmlstring;

                    }
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "D";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response DeedsEnquiryMatch(XDSPortalLibrary.Entity_Layer.DeedsEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "P";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("ListOfProperties");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spProperty_S_Match_Standard '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}','{8}','{9}'", ObjSearch.DeedsOffice.Replace("'", "''"), ObjSearch.IDno.Replace("'", "''"), ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SurName.Replace("'", "''"), ObjSearch.BusinessName.Replace("'", "''"), ObjSearch.TownshipName.Replace("'", "''"), ObjSearch.ErfNo.Replace("'", "''"), ObjSearch.TitleDeedNo.Replace("'", "''"), ObjSearch.BondAccNo.Replace("'", "''"), ObjSearch.PortionNo.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "PropertyDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.PropertyDeedID = Convert.ToInt32(dr["PropertyDeedID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }


                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "P";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response CustomVettingCMatch(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ConnectCustomVettingC");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spCustomVettingC {0}, '{1}','{2}','{3}', {4},'{5}','{6}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.FirstName.Replace("'", "''"), ObjSearch.GrossMonthlyIncome.ToString(), ObjSearch.ReferenceNo.Replace("'", "''"), ObjSearch.ExternalReference.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ds.Tables[0].TableName = "CustomVettingC";
                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response BusinessEnquiryTracePlusMatch(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (String.IsNullOrEmpty(ObjSearch.RegistrationNo) && String.IsNullOrEmpty(ObjSearch.BusinessName))
                {
                    throw new Exception("Registration No or Business Name are mandatory");
                }

                ds = new DataSet("DsCommercialMatch");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spCommercial_S_Match_Standard '{0}', '{1}', '{2}', '{3}', '{4}'", ObjSearch.RegistrationNo.Replace("'", "''"), ObjSearch.BusinessName.Replace("'", "''"), ObjSearch.VATNumber.Replace("'", "''"), ObjSearch.SolePropIDNo.Replace("'", "''"), ObjSearch.TrustNo.Replace("'", "''"));
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xmlstring = "";

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "CommercialDetails";
                    DataColumn dcBonusSeg = new DataColumn("BonusXML");
                    DataColumn dcTempReference = new DataColumn("TempReference");

                    ds.Tables[0].Columns.Add(dcBonusSeg);
                    ds.Tables[0].Columns.Add(dcTempReference);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ObjSearch.CommercialID = Convert.ToInt32(dr["CommercialID"]);
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";
                    }

                    if (ObjSearch.BonusCheck)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["BonusXML"] = "";
                            string strBonusXML = GetTracePlusResult("0", ObjSearch.DataSegments.ToString());
                            if (strBonusXML != "<BonusSegments />")
                                dr["BonusXML"] = strBonusXML;
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["BonusXML"] = "";
                            dr["TempReference"] = "";
                        }
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Return User Info XML
                        ObjResponse.ResponseKeyType = "B";
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                        ObjResponse.ResponseData = xmlstring;
                    }

                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        // No match
                        xmlstring = ds.GetXml();
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        ObjResponse.ResponseData = xmlstring;

                    }
                }
                else
                {
                    // No match
                    xmlstring = ds.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                }

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();


            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public DataSet GetBureauData(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {

            try
            {

                ds = new DataSet("ConnectBureauData");

                //Thread thConsumerMatch = new Thread(new ParameterizedThreadStart(ProcessReport));

                string strSQL = string.Format("execute spBureauData_V1 {0},'{1}','{2}','{3}','{4}','{5}'", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.subscriberID.ToString());
                //thConsumerMatch.Start(strSQL);
                //thConsumerMatch.Join();
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                dtError.Clear();
                dtError.Dispose();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            return ds;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetCustomVettingBReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);


                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));


                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCustomVettingB] {0} ,{1},'{2}','{3}',{4},'{5}',{6},{7},{8},'{9}',{10}", ObjSearch.ConsumerID.ToString(), ObjSearch.GrossMonthlyIncome.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), ObjSearch.Username.ToString(), strReportID.ToString()));
                //thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));



                thConsumerScoringSummary.Join();
                //thConsumerDetails.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {

                        if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                        {
                            if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                            {
                                dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                            }
                            strReportID = dr["ReportID"].ToString();
                            strReportName = dr["ReportName"].ToString();
                        }

                    }

                }

                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetConsumerTraceReport(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string strReportID = "";

            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //    if (dtError.Rows.Count > 0)
                //    {
                //        throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //    }


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0}, '{1}', '{2}', '{3}', {4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                }
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerCreditAccountSummary.Join();
                thConsumerScoringSummary.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();
                thAkaNames.Join();

                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {

                    thNLRConsumerAccountLoad.Join();
                }

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());

                                    }

                                }
                                break;
                        }

                    }

                }

                if (dsTracPlusDataSegment != null)
                {
                    if (dsTracPlusDataSegment.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsTracPlusDataSegment.Tables[0].Rows)
                        {
                            if ((dr["BonusViewed"].ToString().ToLower() == "true"))
                            {
                                switch (dr["DataSegmentName"].ToString())
                                {
                                    case "ConsumerTelephoneLinkage":
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                            }
                                        }
                                        break;
                                    case "ConsumerDetailConfirmation":
                                        if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                            }
                                        }
                                        break;
                                    default:
                                        if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                        {
                                            if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                            {
                                                dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }

                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 2 || ObjSearch.ProductID == 3 || ObjSearch.ProductID == 4 || ObjSearch.ProductID == 5 || ObjSearch.ProductID == 7) && !ObjSearch.AccountHolderName.Trim().Contains("Fides"))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetConsumerTraceReportBusinessStatus(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string strReportID = "";

            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //    if (dtError.Rows.Count > 0)
                //    {
                //        throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //    }


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0}, '{1}', '{2}', '{3}', {4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink_KZ] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                }
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerCreditAccountSummary.Join();
                thConsumerScoringSummary.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();
                thAkaNames.Join();

                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {

                    thNLRConsumerAccountLoad.Join();
                }

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());

                                    }

                                }
                                break;
                        }

                    }

                }

                if (dsTracPlusDataSegment != null)
                {
                    if (dsTracPlusDataSegment.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsTracPlusDataSegment.Tables[0].Rows)
                        {
                            if ((dr["BonusViewed"].ToString().ToLower() == "true"))
                            {
                                switch (dr["DataSegmentName"].ToString())
                                {
                                    case "ConsumerTelephoneLinkage":
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                            }
                                        }
                                        break;
                                    case "ConsumerDetailConfirmation":
                                        if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                            }
                                        }
                                        break;
                                    default:
                                        if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                        {
                                            if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                            {
                                                dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }

                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 2 || ObjSearch.ProductID == 3 || ObjSearch.ProductID == 4 || ObjSearch.ProductID == 5 || ObjSearch.ProductID == 7) && !ObjSearch.AccountHolderName.Trim().Contains("Fides"))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetContactTraceReport(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string strReportID = "";

            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //    if (dtError.Rows.Count > 0)
                //    {
                //        throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //    }


                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport)); ;
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                //thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                ////thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                //thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0}, '{1}', '{2}', '{3}', {4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                //thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                // thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                //thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                // thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                //{
                //    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //}

                //thConsumerAccountLoad.Join();
                //thConsumerCreditAccountSummary.Join();
                //thConsumerScoringSummary.Join();
                thConsumerDetails.Join();
                //thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                //thConsumerPotentialFraudIndicator.Join();
                //thPublicDomain.Join();
                //thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();

                //if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                //{

                //    thNLRConsumerAccountLoad.Join();
                //}

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());

                                    }

                                }
                                break;
                        }

                    }

                }

                if (dsTracPlusDataSegment != null)
                {
                    if (dsTracPlusDataSegment.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsTracPlusDataSegment.Tables[0].Rows)
                        {
                            if ((dr["BonusViewed"].ToString().ToLower() == "true"))
                            {
                                switch (dr["DataSegmentName"].ToString())
                                {
                                    case "ConsumerTelephoneLinkage":
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                            }
                                        }
                                        break;
                                    case "ConsumerDetailConfirmation":
                                        if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                            }
                                        }
                                        break;
                                    default:
                                        if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                        {
                                            if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                            {
                                                dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }

                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 2 || ObjSearch.ProductID == 3 || ObjSearch.ProductID == 4 || ObjSearch.ProductID == 5 || ObjSearch.ProductID == 7) && !ObjSearch.AccountHolderName.Trim().Contains("Fides"))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetACFECreditEnquiryReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //if (dtError.Rows.Count > 0)
                //{
                //    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //}

                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                //Thread thNLRConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thIdentityVerification = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thPmtGAP = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability] '{0}','{1}',{2},{3},{4},{5},'{6}','{7}',{8}", "", "", ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ConsumerID.ToString(), ObjSearch.Username, ObjSearch.SubscriberAssociationCode, strReportID.ToString()));
                thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.Username, ObjSearch.GrossMonthlyIncome));
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                }
                thIdentityVerification.Start(string.Format("execute [dbo].[spCLR_ConsumerIdentityVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.FirstInitial.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(), ObjSearch.IDno.ToString(), ObjSearch.FirstName.ToString(), ObjSearch.Surname.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));
                thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerCreditAccountSummary.Join();
                thConsumerScoringSummary.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();
                thAffordability.Join();
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Join();
                }
                thIdentityVerification.Join();
                thPmtGAP.Join();
                thAkaNames.Join();

                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerAccountLoad.Join();
                //}
                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerCreditAccountSummary.Join();
                //}

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                DataTable dtCPA24MonthlyPayment = new DataTable();
                DataTable dtNLR24MonthlyPayment = new DataTable();
                DataTable dtNLRConsumerAccount = new DataTable();
                DataTable dtCPAConsumerAccount = new DataTable();

                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                {
                    dtCPA24MonthlyPayment = ds.Tables["Consumer24MonthlyPayment"];
                }

                if (ds.Tables.Contains("ConsumerAccountStatus"))
                {
                    dtCPAConsumerAccount = ds.Tables["ConsumerAccountStatus"];
                }
                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                {
                    dtNLR24MonthlyPayment = ds.Tables["ConsumerNLR24MonthlyPayment"];
                }
                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                {
                    dtNLRConsumerAccount = ds.Tables["ConsumerNLRAccountStatus"];
                }

                float fAvailableInst = 0;

                if (ds.Tables.Contains("ConsumerAffordability"))
                {
                    fAvailableInst = float.Parse(ds.Tables["ConsumerAffordability"].Rows[0]["PredAvailableinstalment"].ToString());
                }

                Thread thSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                thSummary.Start(string.Format("execute [dbo].[SpClr_CollectionsConsumerSummary] {0},{1},{2},{3},{4},'{5}','{6}',{7},{8},'{9}',{10}", ObjSearch.ConsumerID, "0", ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.SubscriberAssociationCode, ObjSearch.AssociationTypeCode, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username, fAvailableInst.ToString()));
                thSummary.Join();

                Thread thIndustryPmt = new Thread(new ParameterizedThreadStart(ProcessParameterizedReport));
                string strindustryPmt = "execute [dbo].[spCLR_CreditIndustry] @ConsumerID, @SubscriberAssociationCode, @AssociationTypeCode, @SubscriberID, @ReportID, @ProductID";

                SqlParameter oConsumerID = new SqlParameter("@ConsumerID", ObjSearch.ConsumerID);
                SqlParameter oAssociationCode = new SqlParameter("@SubscriberAssociationCode", ObjSearch.SubscriberAssociationCode);
                SqlParameter oAssociationTypeCode = new SqlParameter("@AssociationTypeCode", ObjSearch.AssociationTypeCode);
                SqlParameter oSubscriberID = new SqlParameter("@SubscriberID", ObjSearch.subscriberID);
                SqlParameter oReportID = new SqlParameter("@ReportID", 60);
                SqlParameter oProductID = new SqlParameter("@ProductID", 81);

                SqlParameterCollection oparamcoll = new SqlCommand().Parameters;
                oparamcoll.Add(oConsumerID);
                oparamcoll.Add(oAssociationCode);
                oparamcoll.Add(oAssociationTypeCode);
                oparamcoll.Add(oSubscriberID);
                oparamcoll.Add(oReportID);
                oparamcoll.Add(oProductID);

                if (dtCPA24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPA24MonthlyPayment";
                    SqlParameter oCPA24MonthlyPayment = new SqlParameter("@CPA24MonthlyPayment", dtCPA24MonthlyPayment);
                    oCPA24MonthlyPayment.TypeName = "dbo.CPA24MonthlyPayment";
                    oCPA24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPA24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLR24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLR24MonthlyPayment";
                    SqlParameter oNLR24MonthlyPayment = new SqlParameter("@NLR24MonthlyPayment", dtNLR24MonthlyPayment);
                    oNLR24MonthlyPayment.TypeName = "dbo.NLR24MonthlyPayment";
                    oNLR24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLR24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLRConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLRConsumerAccount";
                    SqlParameter oNLRConsumerAccount = new SqlParameter("@NLRConsumerAccount", dtNLRConsumerAccount);
                    oNLRConsumerAccount.TypeName = "dbo.NLRConsumerAccount";
                    oNLRConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLRConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtCPAConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPAConsumerAccount";
                    SqlParameter oCPAConsumerAccount = new SqlParameter("@CPAConsumerAccount", dtCPAConsumerAccount);
                    oCPAConsumerAccount.TypeName = "dbo.CPAConsumerAccount";
                    oCPAConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPAConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                dtCPAConsumerAccount.Dispose();
                dtNLRConsumerAccount.Dispose();
                dtNLR24MonthlyPayment.Dispose();
                dtNLR24MonthlyPayment.Dispose();

                object[] Values = new object[2];
                Values[0] = strindustryPmt;
                Values[1] = oparamcoll;

                thIndustryPmt.Start(Values);
                thIndustryPmt.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            //case "ConsumerNLRDebtSummary":

                            //        if (ds.Tables.Contains("NLRSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("NLRSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["NLRSummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("CPASummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("CPASummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["CPASummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("ConsumerNLRDebtSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("ConsumerNLRDebtSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["ConsumerNLRDebtSummary"].Copy());
                            //            }
                            //        }


                            //    break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                //strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 87))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetACFECreditEnquiryReportBusinessStatus(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //if (dtError.Rows.Count > 0)
                //{
                //    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //}

                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                //Thread thNLRConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thIdentityVerification = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thPmtGAP = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink_KZ] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                //thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability] '{0}','{1}',{2},{3},{4},{5},'{6}','{7}',{8}", "", "", ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ConsumerID.ToString(), ObjSearch.Username, ObjSearch.SubscriberAssociationCode, strReportID.ToString()));
                thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.Username, ObjSearch.GrossMonthlyIncome));
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                }
                thIdentityVerification.Start(string.Format("execute [dbo].[spCLR_ConsumerIdentityVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.FirstInitial.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(), ObjSearch.IDno.ToString(), ObjSearch.FirstName.ToString(), ObjSearch.Surname.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));
                thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerCreditAccountSummary.Join();
                thConsumerScoringSummary.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();
                thAffordability.Join();
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Join();
                }
                thIdentityVerification.Join();
                thPmtGAP.Join();
                thAkaNames.Join();

                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerAccountLoad.Join();
                //}
                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerCreditAccountSummary.Join();
                //}

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                DataTable dtCPA24MonthlyPayment = new DataTable();
                DataTable dtNLR24MonthlyPayment = new DataTable();
                DataTable dtNLRConsumerAccount = new DataTable();
                DataTable dtCPAConsumerAccount = new DataTable();

                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                {
                    dtCPA24MonthlyPayment = ds.Tables["Consumer24MonthlyPayment"];
                }

                if (ds.Tables.Contains("ConsumerAccountStatus"))
                {
                    dtCPAConsumerAccount = ds.Tables["ConsumerAccountStatus"];
                }
                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                {
                    dtNLR24MonthlyPayment = ds.Tables["ConsumerNLR24MonthlyPayment"];
                }
                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                {
                    dtNLRConsumerAccount = ds.Tables["ConsumerNLRAccountStatus"];
                }

                float fAvailableInst = 0;

                if (ds.Tables.Contains("ConsumerAffordability"))
                {
                    fAvailableInst = float.Parse(ds.Tables["ConsumerAffordability"].Rows[0]["PredAvailableinstalment"].ToString());
                }

                Thread thSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                thSummary.Start(string.Format("execute [dbo].[SpClr_CollectionsConsumerSummary] {0},{1},{2},{3},{4},'{5}','{6}',{7},{8},'{9}',{10}", ObjSearch.ConsumerID, "0", ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.SubscriberAssociationCode, ObjSearch.AssociationTypeCode, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username, fAvailableInst.ToString()));
                thSummary.Join();

                Thread thIndustryPmt = new Thread(new ParameterizedThreadStart(ProcessParameterizedReport));
                string strindustryPmt = "execute [dbo].[spCLR_CreditIndustry] @ConsumerID, @SubscriberAssociationCode, @AssociationTypeCode, @SubscriberID, @ReportID, @ProductID";

                SqlParameter oConsumerID = new SqlParameter("@ConsumerID", ObjSearch.ConsumerID);
                SqlParameter oAssociationCode = new SqlParameter("@SubscriberAssociationCode", ObjSearch.SubscriberAssociationCode);
                SqlParameter oAssociationTypeCode = new SqlParameter("@AssociationTypeCode", ObjSearch.AssociationTypeCode);
                SqlParameter oSubscriberID = new SqlParameter("@SubscriberID", ObjSearch.subscriberID);
                SqlParameter oReportID = new SqlParameter("@ReportID", 60);
                SqlParameter oProductID = new SqlParameter("@ProductID", 81);

                SqlParameterCollection oparamcoll = new SqlCommand().Parameters;
                oparamcoll.Add(oConsumerID);
                oparamcoll.Add(oAssociationCode);
                oparamcoll.Add(oAssociationTypeCode);
                oparamcoll.Add(oSubscriberID);
                oparamcoll.Add(oReportID);
                oparamcoll.Add(oProductID);

                if (dtCPA24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPA24MonthlyPayment";
                    SqlParameter oCPA24MonthlyPayment = new SqlParameter("@CPA24MonthlyPayment", dtCPA24MonthlyPayment);
                    oCPA24MonthlyPayment.TypeName = "dbo.CPA24MonthlyPayment";
                    oCPA24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPA24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLR24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLR24MonthlyPayment";
                    SqlParameter oNLR24MonthlyPayment = new SqlParameter("@NLR24MonthlyPayment", dtNLR24MonthlyPayment);
                    oNLR24MonthlyPayment.TypeName = "dbo.NLR24MonthlyPayment";
                    oNLR24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLR24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLRConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLRConsumerAccount";
                    SqlParameter oNLRConsumerAccount = new SqlParameter("@NLRConsumerAccount", dtNLRConsumerAccount);
                    oNLRConsumerAccount.TypeName = "dbo.NLRConsumerAccount";
                    oNLRConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLRConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtCPAConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPAConsumerAccount";
                    SqlParameter oCPAConsumerAccount = new SqlParameter("@CPAConsumerAccount", dtCPAConsumerAccount);
                    oCPAConsumerAccount.TypeName = "dbo.CPAConsumerAccount";
                    oCPAConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPAConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                dtCPAConsumerAccount.Dispose();
                dtNLRConsumerAccount.Dispose();
                dtNLR24MonthlyPayment.Dispose();
                dtNLR24MonthlyPayment.Dispose();

                object[] Values = new object[2];
                Values[0] = strindustryPmt;
                Values[1] = oparamcoll;

                thIndustryPmt.Start(Values);
                thIndustryPmt.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            //case "ConsumerNLRDebtSummary":

                            //        if (ds.Tables.Contains("NLRSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("NLRSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["NLRSummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("CPASummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("CPASummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["CPASummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("ConsumerNLRDebtSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("ConsumerNLRDebtSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["ConsumerNLRDebtSummary"].Copy());
                            //            }
                            //        }


                            //    break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                //strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 87))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetCreditEnquiryReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //if (dtError.Rows.Count > 0)
                //{
                //    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //}

                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                //Thread thNLRConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPmtGAP = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thNLRScore = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (ObjSearch.ProductID == 132)
                {
                    if (ObjSearch.AssociationTypeCode.ToLower() == "cca" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }
                    if (strReportID == "161")
                    {
                        thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCustomVettingCashConverters] {0} , {1},{2}, {3},{4}, {5},'{6}','{7}','{8}',{9},{10},'{11}','{12}'", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.Username, ObjSearch.AssociationTypeCode.ToString(), ObjSearch.SubscriberAssociationCode, "0",ObjSearch.GrossMonthlyIncome,ObjSearch.IDno,ObjSearch.Passportno));
                    }
                    else
                    {
                        thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }
                    thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                    thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                    thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.ProductID, ObjSearch.GrossMonthlyIncome));
                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }
                   
                    thConsumerScoringSummary.Join();
                    thConsumerDetails.Join();
                    thConsumerPotentialFraudIndicator.Join();
                    thAffordability.Join();
                    if (ObjSearch.AssociationTypeCode.ToLower() == "cca" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thConsumerAccountLoad.Join();
                    }
                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Join();
                    }
                }
                else if (ObjSearch.ProductID == 31 && strReportID == "159")
                {
                    thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                    thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCustomVettingBayport] {0} , {1},{2}, {3},{4}, {5},'{6}','{7}','{8}',{9}",ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.Username, ObjSearch.AssociationTypeCode.ToString(), ObjSearch.SubscriberAssociationCode, "0"));
                    thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                    thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                    thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                    thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                    //thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability] '{0}','{1}',{2},{3},{4},{5},'{6}','{7}',{8},{9},{10},'{11}',{12}", "", "", ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ConsumerID.ToString(), ObjSearch.Username, ObjSearch.SubscriberAssociationCode, strReportID.ToString(), ObjSearch.ProductID, 0, ObjSearch.AssociationTypeCode, ObjSearch.GrossMonthlyIncome));
                    //thNLRScore.Start(string.Format("Execute dbo.[spCLR_NLRConsumerScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}'", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 0, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode));
                    //thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }

                    //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                    //{
                    //    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));                    
                    //}
                    //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                    //{
                    //    thNLRConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_NLRConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID,ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    //} 
                  //  thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username));

                    thConsumerAccountLoad.Join();
                    thConsumerCreditAccountSummary.Join();
                    thConsumerScoringSummary.Join();
                    thConsumerDetails.Join();
                    thConsumerPropertyInformation.Join();
                    thConsumerPotentialFraudIndicator.Join();
                    thPublicDomain.Join();
                    thConsumerDirectorShipLink.Join();
                   // thAffordability.Join();
                   // thNLRScore.Join();
                   // thAkaNames.Join();

                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Join();
                    }
                   // thPmtGAP.Join();
                }
                else
                {

                    thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    //              thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                    if (strReportID == "162")
                    {
                        thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCustomVettingCashConverters] {0} , {1},{2}, {3},{4}, {5},'{6}','{7}','{8}',{9},{10},'{11}','{12}'", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.Username, ObjSearch.AssociationTypeCode.ToString(), ObjSearch.SubscriberAssociationCode, "0", ObjSearch.GrossMonthlyIncome, ObjSearch.IDno, ObjSearch.Passportno));
                    }
                    else
                    {
                        thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }
                    thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                    thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                    thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                    thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                    thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability] '{0}','{1}',{2},{3},{4},{5},'{6}','{7}',{8},{9},{10},'{11}',{12}", "", "", ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ConsumerID.ToString(), ObjSearch.Username, ObjSearch.SubscriberAssociationCode, strReportID.ToString(), ObjSearch.ProductID, 0, ObjSearch.AssociationTypeCode, ObjSearch.GrossMonthlyIncome));
                    thNLRScore.Start(string.Format("Execute dbo.[spCLR_NLRConsumerScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}'", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 0, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode));
                    thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }

                    //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                    //{
                    //    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));                    
                    //}
                    //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                    //{
                    //    thNLRConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_NLRConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID,ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    //} 
                    thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username));

                    thConsumerAccountLoad.Join();
                    thConsumerCreditAccountSummary.Join();
                    thConsumerScoringSummary.Join();
                    thConsumerDetails.Join();
                    thConsumerPropertyInformation.Join();
                    thConsumerPotentialFraudIndicator.Join();
                    thPublicDomain.Join();
                    thConsumerDirectorShipLink.Join();
                    thAffordability.Join();
                    thNLRScore.Join();
                    thAkaNames.Join();

                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Join();
                    }
                    thPmtGAP.Join();
                }

                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerAccountLoad.Join();
                //}
                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerCreditAccountSummary.Join();
                //}


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            //case "ConsumerNLRDebtSummary":

                            //        if (ds.Tables.Contains("NLRSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("NLRSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["NLRSummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("CPASummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("CPASummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["CPASummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("ConsumerNLRDebtSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("ConsumerNLRDebtSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["ConsumerNLRDebtSummary"].Copy());
                            //            }
                            //        }


                            //    break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }
                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                //strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 15 || ObjSearch.ProductID == 31))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetBiometricsCreditEnquiryReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                
                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), "1"));
                thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), "1", ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.ProductID, ObjSearch.GrossMonthlyIncome));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), "1"));
                    
                thConsumerScoringSummary.Join();
                thAffordability.Join();
                thPublicDomain.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtError.Clear();
                ds.Clear();

                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetBiometricsCreditReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            
            try
            {
                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), "1"));
                thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), "1", ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.ProductID, ObjSearch.GrossMonthlyIncome));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), "1", ObjSearch.subscriberID));

                thConsumerScoringSummary.Join();
                thAffordability.Join();
                thConsumerDetails.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtError.Clear();
                ds.Clear();

                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetCreditEnquiryReportBusinessStatus(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //if (dtError.Rows.Count > 0)
                //{
                //    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //}

                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                //Thread thNLRConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPmtGAP = new Thread(new ParameterizedThreadStart(ProcessACFEReport));
                Thread thNLRScore = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (ObjSearch.ProductID == 132)
                {
                    if (ObjSearch.AssociationTypeCode.ToLower() == "cca" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }
                    thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                    thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                    thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.ProductID, ObjSearch.GrossMonthlyIncome));
                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }

                    thConsumerScoringSummary.Join();
                    thConsumerDetails.Join();
                    thConsumerPotentialFraudIndicator.Join();
                    thAffordability.Join();
                    if (ObjSearch.AssociationTypeCode.ToLower() == "cca" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thConsumerAccountLoad.Join();
                    }
                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Join();
                    }
                }
                else
                {

                    thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    //              thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                    thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                    thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                    thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                    thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink_KZ] {0} , 0", ObjSearch.ConsumerID.ToString()));
                    thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability] '{0}','{1}',{2},{3},{4},{5},'{6}','{7}',{8},{9},{10},'{11}',{12}", "", "", ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ConsumerID.ToString(), ObjSearch.Username, ObjSearch.SubscriberAssociationCode, strReportID.ToString(), ObjSearch.ProductID, 0, ObjSearch.AssociationTypeCode, ObjSearch.GrossMonthlyIncome));
                    thNLRScore.Start(string.Format("Execute dbo.[spCLR_NLRConsumerScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}'", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 0, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode));
                    thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    }

                    //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                    //{
                    //    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));                    
                    //}
                    //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                    //{
                    //    thNLRConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_NLRConsumerCreditAccountSummary] {0},{1},'{2}',{3} ", ObjSearch.ConsumerID.ToString(),ObjSearch.subscriberID,ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    //} 
                    thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), 81, 60, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.Username));

                    thConsumerAccountLoad.Join();
                    thConsumerCreditAccountSummary.Join();
                    thConsumerScoringSummary.Join();
                    thConsumerDetails.Join();
                    thConsumerPropertyInformation.Join();
                    thConsumerPotentialFraudIndicator.Join();
                    thPublicDomain.Join();
                    thConsumerDirectorShipLink.Join();
                    thAffordability.Join();
                    thNLRScore.Join();
                    thAkaNames.Join();

                    if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                    {
                        thNLRConsumerAccountLoad.Join();
                    }
                    thPmtGAP.Join();
                }

                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerAccountLoad.Join();
                //}
                //if (ObjSearch.ProductID == 57 || ObjSearch.ProductID == 58)
                //{
                //    thNLRConsumerCreditAccountSummary.Join();
                //}


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            //case "ConsumerNLRDebtSummary":

                            //        if (ds.Tables.Contains("NLRSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("NLRSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["NLRSummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("CPASummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("CPASummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["CPASummary"].Copy());
                            //            }
                            //        }
                            //        if (ds.Tables.Contains("ConsumerNLRDebtSummary"))
                            //        {
                            //            if (!dsReport.Tables.Contains("ConsumerNLRDebtSummary"))
                            //            {
                            //                dsReport.Tables.Add(ds.Tables["ConsumerNLRDebtSummary"].Copy());
                            //            }
                            //        }


                            //    break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }
                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                //strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 15 || ObjSearch.ProductID == 31))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }


        public XDSPortalLibrary.Entity_Layer.Response GetConsumerICheckReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));

                thConsumerScoringSummary.Join();
                thConsumerDetails.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }
                    }
                }
                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                //strCount.Add("ConsumerScoring");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }

                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 15 || ObjSearch.ProductID == 31))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    xml = xml.Replace("ConsumerScoring", "ConsumeriCheck");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetConsumerScoreIndicatorReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));

                thConsumerScoringSummary.Join();
                thConsumerDetails.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }
                    }
                }
                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                //strCount.Add("ConsumerScoring");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }

                if (i == 0 && !ObjSearch.DisplayUnusableInformation && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 15 || ObjSearch.ProductID == 31))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();

                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetBusinessEnquiryReport(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "B";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Commercial");

                DataSet dsReport = new DataSet("Commercial");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
                drReportInformation["ReportID"] = ObjSearch.ReportID.ToString();
                drReportInformation["ReportName"] = ObjSearch.ReportName.ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                Thread thDirectorCurrentBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thDirectorPreviousBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialBusinessInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialPrincipalInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialScore = new Thread(new ParameterizedThreadStart(ProcessReport));

                thDirectorCurrentBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorCurrentBusinessInterests] {0} , {1}", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thDirectorPreviousBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorPreviousBusinessInterests] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialInformation.Start(string.Format("execute [dbo].[spCLR_CommercialInformation] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString(), String.IsNullOrEmpty(ObjSearch.TmpReference) ? "0" : ObjSearch.TmpReference.ToString()));
                thCommercialBusinessInformation.Start(string.Format("execute [dbo].[spCLR_CommercialBusinessInformation] {0} , '{1}','{2}', {3}", ObjSearch.CommercialID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialPrincipalInformation.Start(string.Format("execute [dbo].[spCLR_CommercialPrincipalInformation] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialScore.Start(string.Format("execute [dbo].[spCommercialScoring] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString()));

                thDirectorCurrentBusinessInterests.Join();
                thDirectorPreviousBusinessInterests.Join();
                thCommercialInformation.Join();
                thCommercialBusinessInformation.Join();
                thCommercialPrincipalInformation.Join();
                thCommercialScore.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "CommercialPrincipalInformation":
                                if (ds.Tables.Contains("CommercialPrincipalInformation"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialPrincipalInformation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialPrincipalInformation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("DirectorCurrentBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("DirectorCurrentBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["DirectorCurrentBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("DirectorPreviousBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("DirectorPreviousBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["DirectorPreviousBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("DirectorPropertyInterests"))
                                {
                                    if (!dsReport.Tables.Contains("DirectorPropertyInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["DirectorPropertyInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialActivePrincipalInfoSummary"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialActivePrincipalInfoSummary"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialActivePrincipalInfoSummary"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialInActivePrincipalInfoSummary"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialInActivePrincipalInfoSummary"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialInActivePrincipalInfoSummary"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialActivePrincipalInformation"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialActivePrincipalInformation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialActivePrincipalInformation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialInActivePrincipalInformation"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialInActivePrincipalInformation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialInActivePrincipalInformation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("BusinessEnquiryHistory"))
                                {
                                    if (!dsReport.Tables.Contains("BusinessEnquiryHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["BusinessEnquiryHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorCurrentBusinessinterests"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorCurrentBusinessinterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorCurrentBusinessinterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorCurrentBusinessinterests"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorCurrentBusinessinterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorCurrentBusinessinterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorPreviousBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorPreviousBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPreviousBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorPreviousBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorPreviousBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPreviousBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorPropertyInterests"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorPropertyInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPropertyInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorPropertyInterests"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorPropertyInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPropertyInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorJudgments"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorJudgments"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorJudgments"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorJudgments"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorJudgments"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorJudgments"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorDebtReview"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorDebtReview"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorDebtReview"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorDebtReview"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorDebtReview"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorDebtReview"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorAddressHistory"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorAddressHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorAddressHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorAddressHistory"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorAddressHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorAddressHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorContactHistory"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorContactHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorContactHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorAddressHistory"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorAddressHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorAddressHistory"].Copy());
                                    }
                                }

                                if (ds.Tables.Contains("ActiveDirectorPaymentNotification"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorPaymentNotification"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPaymentNotification"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorPaymentNotification"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorPaymentNotification"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPaymentNotification"].Copy());
                                    }
                                }

                                if (ds.Tables.Contains("ActiveDirectorDefaultAlert"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorDefaultAlert"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorDefaultAlert"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorDefaultAlert"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorDefaultAlert"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorDefaultAlert"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetBusinessEnquiryReportWithStatus(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "B";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Commercial");

                DataSet dsReport = new DataSet("Commercial");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
                drReportInformation["ReportID"] = ObjSearch.ReportID.ToString();
                drReportInformation["ReportName"] = ObjSearch.ReportName.ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                Thread thDirectorCurrentBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thDirectorPreviousBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialBusinessInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialPrincipalInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialScore = new Thread(new ParameterizedThreadStart(ProcessReport));

                thDirectorCurrentBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorCurrentBusinessInterests_StatusDate] {0} , {1}", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thDirectorPreviousBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorPreviousBusinessInterests_StatusDate] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialInformation.Start(string.Format("execute [dbo].[spCLR_CommercialInformation] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString(), String.IsNullOrEmpty(ObjSearch.TmpReference) ? "0" : ObjSearch.TmpReference.ToString()));
                thCommercialBusinessInformation.Start(string.Format("execute [dbo].[spCLR_CommercialBusinessInformation] {0} , '{1}','{2}', {3}", ObjSearch.CommercialID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialPrincipalInformation.Start(string.Format("execute [dbo].[spCLR_CommercialPrincipalInformation] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialScore.Start(string.Format("execute [dbo].[spCommercialScoring] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString()));

                thDirectorCurrentBusinessInterests.Join();
                thDirectorPreviousBusinessInterests.Join();
                thCommercialInformation.Join();
                thCommercialBusinessInformation.Join();
                thCommercialPrincipalInformation.Join();
                thCommercialScore.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "CommercialPrincipalInformation":
                                if (ds.Tables.Contains("CommercialPrincipalInformation"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialPrincipalInformation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialPrincipalInformation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("DirectorCurrentBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("DirectorCurrentBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["DirectorCurrentBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("DirectorPreviousBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("DirectorPreviousBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["DirectorPreviousBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("DirectorPropertyInterests"))
                                {
                                    if (!dsReport.Tables.Contains("DirectorPropertyInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["DirectorPropertyInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialActivePrincipalInfoSummary"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialActivePrincipalInfoSummary"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialActivePrincipalInfoSummary"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialInActivePrincipalInfoSummary"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialInActivePrincipalInfoSummary"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialInActivePrincipalInfoSummary"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialActivePrincipalInformation"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialActivePrincipalInformation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialActivePrincipalInformation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("CommercialInActivePrincipalInformation"))
                                {
                                    if (!dsReport.Tables.Contains("CommercialInActivePrincipalInformation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["CommercialInActivePrincipalInformation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("BusinessEnquiryHistory"))
                                {
                                    if (!dsReport.Tables.Contains("BusinessEnquiryHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["BusinessEnquiryHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorCurrentBusinessinterests"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorCurrentBusinessinterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorCurrentBusinessinterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorCurrentBusinessinterests"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorCurrentBusinessinterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorCurrentBusinessinterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorPreviousBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorPreviousBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPreviousBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorPreviousBusinessInterests"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorPreviousBusinessInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPreviousBusinessInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorPropertyInterests"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorPropertyInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPropertyInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorPropertyInterests"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorPropertyInterests"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPropertyInterests"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorJudgments"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorJudgments"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorJudgments"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorJudgments"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorJudgments"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorJudgments"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorDebtReview"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorDebtReview"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorDebtReview"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorDebtReview"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorDebtReview"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorDebtReview"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorAddressHistory"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorAddressHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorAddressHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorAddressHistory"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorAddressHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorAddressHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ActiveDirectorContactHistory"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorContactHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorContactHistory"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorAddressHistory"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorAddressHistory"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorAddressHistory"].Copy());
                                    }
                                }

                                if (ds.Tables.Contains("ActiveDirectorPaymentNotification"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorPaymentNotification"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPaymentNotification"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorPaymentNotification"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorPaymentNotification"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPaymentNotification"].Copy());
                                    }
                                }

                                if (ds.Tables.Contains("ActiveDirectorDefaultAlert"))
                                {
                                    if (!dsReport.Tables.Contains("ActiveDirectorDefaultAlert"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ActiveDirectorDefaultAlert"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("InactiveDirectorDefaultAlert"))
                                {
                                    if (!dsReport.Tables.Contains("InactiveDirectorDefaultAlert"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["InactiveDirectorDefaultAlert"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        //public XDSPortalLibrary.Entity_Layer.Response GetBusinessEnquiryReportCustomVettingR(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch, XDSPortalLibrary.Entity_Layer.ModuleRequestResponse oinputResponse, out XDSPortalLibrary.Entity_Layer.ModuleRequestResponse oReportResponse)
        //{
        //    oReportResponse = oinputResponse;
        //    XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
        //    try
        //    {

        //        DataSet dsDataSegment = new DataSet();
        //        dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

        //        DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


        //        ObjResponse.ResponseKeyType = "B";
        //        ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
        //        ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

        //        ds = new DataSet("Commercial");

        //        DataSet dsReport = new DataSet("Commercial");

        //        DataTable dtReportInformation = new DataTable("ReportInformation");
        //        DataColumn dcReportID = new DataColumn("ReportID");
        //        DataColumn dcReportName = new DataColumn("ReportName");
        //        string strReportID = "";
        //        string strReportName = "";

        //        dtReportInformation.Columns.Add(dcReportID);
        //        dtReportInformation.Columns.Add(dcReportName);

        //        DataRow drReportInformation = dtReportInformation.NewRow();
        //        //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
        //        //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
        //        drReportInformation["ReportID"] = ObjSearch.ReportID.ToString();
        //        drReportInformation["ReportName"] = ObjSearch.ReportName.ToString();

        //        dtReportInformation.Rows.Add(drReportInformation);
        //        dsReport.Tables.Add(dtReportInformation);

        //        if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
        //            ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


        //        Thread thDirectorCurrentBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thDirectorPreviousBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialBusinessInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialPrincipalInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialScore = new Thread(new ParameterizedThreadStart(ProcessReport));

        //        thDirectorCurrentBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorCurrentBusinessInterests] {0} , {1}", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
        //        thDirectorPreviousBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorPreviousBusinessInterests] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
        //        thCommercialInformation.Start(string.Format("execute [dbo].[spCLR_CommercialInformation] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString(), String.IsNullOrEmpty(ObjSearch.TmpReference) ? "0" : ObjSearch.TmpReference.ToString()));
        //        thCommercialBusinessInformation.Start(string.Format("execute [dbo].[spCLR_CommercialBusinessInformation] {0} , '{1}','{2}', {3}", ObjSearch.CommercialID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.ProductID.ToString()));
        //        thCommercialPrincipalInformation.Start(string.Format("execute [dbo].[spCLR_CommercialPrincipalInformation] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
        //        thCommercialScore.Start(string.Format("execute [dbo].[spCommercialScoring] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString()));

        //        thDirectorCurrentBusinessInterests.Join();
        //        thDirectorPreviousBusinessInterests.Join();
        //        thCommercialInformation.Join();
        //        thCommercialBusinessInformation.Join();
        //        thCommercialPrincipalInformation.Join();
        //        thCommercialScore.Join();

        //        if (dtError.Rows.Count > 0)
        //        {
        //            throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
        //        }


        //        //foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
        //        //{
        //        //    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
        //        //    {
        //        //        switch (dr["DataSegmentName"].ToString())
        //        //        {
        //        //            case "CommercialPrincipalInformation":
        //        //                if (ds.Tables.Contains("CommercialPrincipalInformation"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("CommercialPrincipalInformation"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["CommercialPrincipalInformation"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("DirectorCurrentBusinessInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("DirectorCurrentBusinessInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["DirectorCurrentBusinessInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("DirectorPreviousBusinessInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("DirectorPreviousBusinessInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["DirectorPreviousBusinessInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("DirectorPropertyInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("DirectorPropertyInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["DirectorPropertyInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("CommercialActivePrincipalInfoSummary"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("CommercialActivePrincipalInfoSummary"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["CommercialActivePrincipalInfoSummary"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("CommercialInActivePrincipalInfoSummary"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("CommercialInActivePrincipalInfoSummary"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["CommercialInActivePrincipalInfoSummary"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("CommercialActivePrincipalInformation"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("CommercialActivePrincipalInformation"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["CommercialActivePrincipalInformation"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("CommercialInActivePrincipalInformation"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("CommercialInActivePrincipalInformation"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["CommercialInActivePrincipalInformation"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("BusinessEnquiryHistory"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("BusinessEnquiryHistory"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["BusinessEnquiryHistory"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorCurrentBusinessinterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorCurrentBusinessinterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorCurrentBusinessinterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorCurrentBusinessinterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorCurrentBusinessinterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorCurrentBusinessinterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorPreviousBusinessInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorPreviousBusinessInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPreviousBusinessInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorPreviousBusinessInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorPreviousBusinessInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPreviousBusinessInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorPropertyInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorPropertyInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPropertyInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorPropertyInterests"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorPropertyInterests"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPropertyInterests"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorJudgments"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorJudgments"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorJudgments"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorJudgments"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorJudgments"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorJudgments"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorDebtReview"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorDebtReview"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorDebtReview"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorDebtReview"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorDebtReview"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorDebtReview"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorAddressHistory"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorAddressHistory"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorAddressHistory"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorAddressHistory"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorAddressHistory"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorAddressHistory"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("ActiveDirectorContactHistory"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorContactHistory"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorContactHistory"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorAddressHistory"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorAddressHistory"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorAddressHistory"].Copy());
        //        //                    }
        //        //                }

        //        //                if (ds.Tables.Contains("ActiveDirectorPaymentNotification"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorPaymentNotification"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorPaymentNotification"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorPaymentNotification"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorPaymentNotification"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorPaymentNotification"].Copy());
        //        //                    }
        //        //                }

        //        //                if (ds.Tables.Contains("ActiveDirectorDefaultAlert"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("ActiveDirectorDefaultAlert"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["ActiveDirectorDefaultAlert"].Copy());
        //        //                    }
        //        //                }
        //        //                if (ds.Tables.Contains("InactiveDirectorDefaultAlert"))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains("InactiveDirectorDefaultAlert"))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables["InactiveDirectorDefaultAlert"].Copy());
        //        //                    }
        //        //                }
        //        //                break;
        //        //            default:
        //        //                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
        //        //                {
        //        //                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
        //        //                    {
        //        //                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
        //        //                    }
        //        //                    strReportID = dr["ReportID"].ToString();
        //        //                    strReportName = dr["ReportName"].ToString();
        //        //                }
        //        //                break;
        //        //        }

        //        //    }

        //        //}

        //        dsReport = ds;

        //        File.AppendAllText(@"C:\Log\mtnvet.txt", dsReport.GetXml());


        //        try
        //        {
        //            int count = 0, i = 0; string FinancialYearEnd = string.Empty;

        //            if (dsReport.Tables.Count > 0)
        //            {

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialBusinessInformation");

        //                if (dsReport.Tables.Contains("CommercialBusinessInformation"))
        //                {
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step1");

        //                    oReportResponse.ModuleRequestResult.RegistrationDetails = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultRegistrationDetails();
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step2");
        //                    oReportResponse.ModuleRequestResult.Header = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultHeader();
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step3");
        //                    oReportResponse.ModuleRequestResult.Names = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultNames();
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step4");
        //                    foreach (DataRow r in dsReport.Tables["CommercialBusinessInformation"].Rows)
        //                    {
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step5");
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultRegistrationDetails o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultRegistrationDetails();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step6");

        //                        o.MajorProduct = ObjSearch.ReportID.ToString();

        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step7");
        //                        o.RegistrationDate = r["RegistrationDate"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step8");
        //                        // o.RegistrationStatusCode = r[""].ToString();
        //                        o.RegistrationStatusDesc = r["CommercialStatus"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step9");
        //                        //o.LiquidationDate
        //                        o.RegistrationNo = r["RegistrationNo12"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step10");
        //                        // o.CompanyTypeCode = 
        //                        o.CompanyTypeDesc = r["CommercialType"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step11");
        //                        //o.RegistrationCountry
        //                        o.PreviousRegistrationNo = new string[1];
        //                        o.PreviousRegistrationNo[0] = r["RegistrationNoConverted"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step12");
        //                        //o.Auditors
        //                        o.RegisteredAddress = r["PhysicalAddress"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step13");
        //                        //o.Suburb
        //                        //o.City
        //                        //o.Country
        //                        //o.PostCode
        //                        o.AuthorisedCapital = r["AuthorisedCapitalAmt"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step14");
        //                        o.IssuedCapital = r["IssuedCapitalAmt"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step15");
        //                        //o.StatedCapital
        //                        //o.Reg_Info_Date
        //                        o.PostalAddress = new string[1];
        //                        o.PostalAddress[0] = r["PostalAddress"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step16");
        //                        //o.PostalPostCode
        //                        o.FinancialYearEnd = r["FinancialYearEnd"].ToString();

        //                        FinancialYearEnd = o.FinancialYearEnd;

        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step8");
        //                        oReportResponse.ModuleRequestResult.RegistrationDetails = o;
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step9");
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultHeader oHeader = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultHeader();
        //                        oHeader.MajorProduct = ObjSearch.ReportID.ToString();
        //                        oHeader.DateOfHeader = DateTime.Now.ToString("yyyyMMdd");
        //                        oHeader.StartDate = r["BusinessStartDate"].ToString();
        //                        oHeader.BusinessType = r["CommercialType"].ToString();
        //                        //oHeader.DunsNumber
        //                        oHeader.StartingCapital = r["IssuedCapitalAmt"].ToString().Substring(0, r["IssuedCapitalAmt"].ToString().Length > 10 ? 10 : r["IssuedCapitalAmt"].ToString().Length);
        //                        //oHeader.PurchasePrice
        //                        oHeader.Industry = r["SIC"].ToString();
        //                        //oHeader.BusinessFunction
        //                        // oHeader.BusinessCategory = r
        //                        oHeader.PhysicalAddress = r["PhysicalAddress"].ToString();
        //                        //oHeader.Suburb
        //                        //oHeader.City
        //                        //oHeader.Country
        //                        //oHeader.PostCode
        //                        oHeader.PostalAddress = r["PostalAddress"].ToString();
        //                        //oHeader.PostalSuburb 
        //                        //oHeader.PostalCity
        //                        //oHeader.PostalCountry
        //                        //oHeader.PostalPostCode
        //                        oHeader.Phone = r["TelephoneNo"].ToString();
        //                        oHeader.Fax = r["FaxNo"].ToString();
        //                        //oHeader.VATNumbers = 
        //                        oHeader.Website = r["BussWebsite"].ToString();
        //                        oHeader.Email = r["BussEmail"].ToString();
        //                        oHeader.TaxNumber = r["TaxNo"].ToString();

        //                        oReportResponse.ModuleRequestResult.Header = oHeader;



        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultNames oNames = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultNames();

        //                        oNames.PreviousNames = new string[1];
        //                        oNames.PreviousNames[0] = r["PreviousBussName"].ToString();
        //                        oNames.PreviousNameDates = new string[1]; ;

        //                        oNames.PreviousNameDates[0] = r["NameChangeDate"].ToString();
        //                        oNames.AKAName = r["TradeName"].ToString();
        //                        oNames.BusinessName = r["CommercialName"].ToString();

        //                        oReportResponse.ModuleRequestResult.Names = oNames;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialBankCodeHistory");

        //                if (dsReport.Tables.Contains("CommercialBankCodeHistory"))
        //                {
        //                    count = dsReport.Tables["CommercialBankCodeHistory"].Rows.Count;
        //                    oReportResponse.ModuleRequestResult.BankCodes = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankCodes();
        //                    oReportResponse.ModuleRequestResult.BankCodes.BankCodes = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankCodesBankCodes();

        //                    oReportResponse.ModuleRequestResult.BankHistory = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankHistory();
        //                    oReportResponse.ModuleRequestResult.BankHistory.BankHistory = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankHistoryBankHistory[count];

        //                    oReportResponse.ModuleRequestResult.GeneralBankingInfo = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultGeneralBankingInfo();
        //                    oReportResponse.ModuleRequestResult.GeneralBankingInfo.GeneralBankingInfo = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultGeneralBankingInfoGeneralBankingInfo();

        //                    i = 0;
        //                    foreach (DataRow r in dsReport.Tables["CommercialBankCodeHistory"].Rows)
        //                    {

        //                        DateTime dtrequesteddate = DateTime.Now;
        //                        DateTime requesteddate = DateTime.Now;
        //                        string bankCode = string.Empty;

        //                        bankCode = r["BankCode"].ToString();



        //                        if (DateTime.TryParseExact(r["DateRequested"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtrequesteddate))
        //                        {
        //                            if (i == 0)
        //                            {
        //                                requesteddate = dtrequesteddate;
        //                            }

        //                            if (dtrequesteddate >= requesteddate && !string.IsNullOrEmpty(bankCode))
        //                            {
        //                                XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankCodesBankCodes o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankCodesBankCodes();

        //                                o.MajorProduct = ObjSearch.ReportID.ToString();
        //                                o.AccountNo = r["AccountNumber"].ToString();
        //                                o.BankName = r["Bank"].ToString();
        //                                o.Branch = r["BranchName"].ToString();
        //                                o.BankCode = r["BankCode"].ToString();
        //                                o.BankCodeDesc = r["Bank_Comments"].ToString();
        //                                o.Comment = r["Comment"].ToString();
        //                                o.Amount = r["EnquiryAmount"].ToString();
        //                                o.StartDate = r["DateOpened"].ToString();
        //                                o.Terms = r["terms"].ToString();
        //                                o.Date = r["DateRequested"].ToString();


        //                                oReportResponse.ModuleRequestResult.BankCodes.BankCodes = o;
        //                                requesteddate = dtrequesteddate;


        //                                XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultGeneralBankingInfoGeneralBankingInfo oBankInfo = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultGeneralBankingInfoGeneralBankingInfo();
        //                                oBankInfo.MajorProduct = ObjSearch.ReportID.ToString();
        //                                oBankInfo.AccountNo = r["AccountNumber"].ToString();
        //                                oBankInfo.BankName = r["Bank"].ToString();
        //                                oBankInfo.Branch = r["BranchName"].ToString();
        //                                oBankInfo.Comment = r["Comment"].ToString();
        //                                oBankInfo.StartDate = r["DateOpened"].ToString();
        //                                oBankInfo.InfoDate = r["DateRequested"].ToString();

        //                                oReportResponse.ModuleRequestResult.GeneralBankingInfo.GeneralBankingInfo = oBankInfo;
        //                            }


        //                        }



        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankHistoryBankHistory oHis = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBankHistoryBankHistory();

        //                        oHis.MajorProduct = ObjSearch.ReportID.ToString();
        //                        oHis.AccountNo = r["AccountNumber"].ToString();
        //                        oHis.BankName = r["Bank"].ToString();
        //                        oHis.Branch = r["BranchName"].ToString();
        //                        oHis.BankCode = r["BankCode"].ToString();
        //                        oHis.BankCodeDesc = r["Bank_Comments"].ToString();
        //                        oHis.Comment = r["Comment"].ToString();
        //                        oHis.Amount = r["EnquiryAmount"].ToString();
        //                        oHis.StartDate = r["DateOpened"].ToString();
        //                        oHis.Terms = r["terms"].ToString();
        //                        oHis.Date = r["DateRequested"].ToString();

        //                        oReportResponse.ModuleRequestResult.BankHistory.BankHistory[i] = oHis;

        //                        i++;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialTradeReferencesInformation");

        //                if (dsReport.Tables.Contains("CommercialTradeReferencesInformation"))
        //                {
        //                    count = dsReport.Tables["CommercialTradeReferencesInformation"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.TradeHistory = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultTradeHistory();
        //                    oReportResponse.ModuleRequestResult.TradeHistory.TradeHistory = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultTradeHistoryTradeHistory[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialTradeReferencesInformation"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultTradeHistoryTradeHistory o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultTradeHistoryTradeHistory();

        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        o.DateOfRef = r["Createdondate"].ToString();
        //                        // o.YearsKnown = 
        //                        //o.MonthsKnown
        //                        o.CreditLimit = r["CreditLimit"].ToString();
        //                        o.Unlimited = "N";// r["MaxLimit"].ToString();
        //                        o.Purchases = r["AveragePurchases"].ToString();
        //                        o.TermsTaken = r["TermsTaken"].ToString();
        //                        o.TermsGiven = r["Terms"].ToString();
        //                        // o.Discount = 
        //                        // o.ReferenceName = 
        //                        //o.Insurance
        //                        //o.InsuranceDesc
        //                        o.Comment = r["Comment"].ToString();
        //                        o.AssuredValue = r["SuretyValue"].ToString();
        //                        // o.Obtained = r[""]

        //                        oReportResponse.ModuleRequestResult.TradeHistory.TradeHistory[1] = o;

        //                        i++;
        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPropertyInformation");

        //                if (dsReport.Tables.Contains("CommercialPropertyInformation"))
        //                {
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDA();
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDABusinessDeedsSummaryDA();

        //                    int noofProperties = dsReport.Tables["CommercialPropertyInformation"].Rows.Count;
        //                    double totalValue = 0;
        //                    double totalBond = 0;
        //                    string OldestPropertyDate = string.Empty, OldestPropertyValue = string.Empty;
        //                    DateTime purchasedate = DateTime.Now;

        //                    List<string> Deedsoffice = new List<string>();

        //                    foreach (DataRow r in dsReport.Tables["CommercialPropertyInformation"].Rows)
        //                    {
        //                        if (!Deedsoffice.Contains(r["DeedsOffice"]))
        //                        {
        //                            Deedsoffice.Add(r["DeedsOffice"].ToString());
        //                        }

        //                        totalValue = totalValue + double.Parse(r["PurchasePriceAmt"].ToString());
        //                        totalBond = totalBond + double.Parse(r["BondAmt"].ToString());

        //                        DateTime dtpurchasedate = DateTime.Now;

        //                        if (DateTime.TryParseExact(r["PurchaseDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtpurchasedate))
        //                        {
        //                            if (i == 0)
        //                            {
        //                                purchasedate = dtpurchasedate;
        //                            }
        //                            if (purchasedate >= dtpurchasedate)
        //                            {
        //                                OldestPropertyDate = r["PurchaseDate"].ToString();
        //                                OldestPropertyValue = r["PurchasePriceAmt"].ToString();

        //                                purchasedate = dtpurchasedate;

        //                            }

        //                        }

        //                    }
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.Comment = string.Empty;
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.DeedsOffice = String.Join(",", Deedsoffice).ToString();
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.MajorProduct = ObjSearch.ReportID.ToString();
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.NumberProperties = noofProperties.ToString();
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.TotalBond = totalBond.ToString();
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.TotalOwned = noofProperties.ToString();
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA.BusinessDeedsSummaryDA.TotalValue = totalValue.ToString();

        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialJudgment");

        //                if (dsReport.Tables.Contains("CommercialJudgment"))
        //                {
        //                    count = dsReport.Tables["CommercialJudgment"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.CourtRecords = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCourtRecords();
        //                    oReportResponse.ModuleRequestResult.CourtRecords.CourtRecord = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCourtRecordsCourtRecord[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialJudgment"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCourtRecordsCourtRecord o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCourtRecordsCourtRecord();
        //                        o.ActionDate = r["CaseFilingDate"].ToString();
        //                        o.Attorney = r["AttorneyName"].ToString();
        //                        //o.AbandonDate
        //                        o.CaseNumber = r["CaseNumber"].ToString();
        //                        //o.City 
        //                        o.ClaimAmount = r["DisputeAmt"].ToString();
        //                        //o.Comment 
        //                        //o.Country
        //                        //o.CourtDistrict = r[]
        //                        //o.CourtRecordAddress =
        //                        //  o.CourtType = 
        //                        //o.DefendantDistrict
        //                        //o.DefendantName
        //                        //o.DefendantTradeStyle
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.Message
        //                        //o.NatureOfDebt
        //                        //o.NumberFound
        //                        //o.PlaintiffName = r[""]
        //                        //o.PostCode
        //                        //o.ReturnDate
        //                        //o.SerialNumber
        //                        //o.Suburb
        //                        //o.TypeCode
        //                        o.TypeDesc = r["CaseType"].ToString();

        //                        oReportResponse.ModuleRequestResult.CourtRecords.CourtRecord[i] = o;

        //                        i++;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialDefaultAlert");

        //                if (dsReport.Tables.Contains("CommercialDefaultAlert"))
        //                {
        //                    count = dsReport.Tables["CommercialDefaultAlert"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.Defaults = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultDefaults();
        //                    oReportResponse.ModuleRequestResult.Defaults.Default = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultDefaultsDefault[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialDefaultAlert"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultDefaultsDefault o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultDefaultsDefault();

        //                        o.Amount = r["Amount"].ToString();
        //                        //o.City
        //                        o.Comment = r["Comments"].ToString();
        //                        //o.Country 
        //                        //o.DefaultAddress 
        //                        o.DefaultDate = r["StatusDate"].ToString();
        //                        o.DefaultName = r["CommercialName"].ToString();
        //                        //o.DefaultTradeStyle
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.Message 
        //                        //o.NumberFound
        //                        //o.OnBehalfOf 
        //                        //o.PostCode
        //                        //o.SerialNo
        //                        o.Status = r["Statuscode"].ToString();
        //                        o.SubscriberName = r["Company"].ToString();
        //                        //o.Suburb 
        //                        o.SupplierName = r["Company"].ToString();

        //                        oReportResponse.ModuleRequestResult.Defaults.Default[i] = o;

        //                        i++;

        //                    }
        //                }

        //                if (dsReport.Tables.Contains("CommercialFinancialEstimates"))
        //                {
        //                    count = dsReport.Tables["CommercialFinancialEstimates"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.FinanceHeader = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultFinanceHeader();


        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialFinancialEstimates"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultFinanceHeader o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultFinanceHeader();

        //                        o.MajorProduct = strReportID;
        //                        o.Date = r["Createdondate"].ToString();
        //                        o.Source = r["CommercialName"].ToString();
        //                        o.TurnOverAmount1 = r["CurrentTurnOverPerAnnum"].ToString();
        //                        o.TurnOverAmount2 = r["PreviousYearTurnover"].ToString();
        //                        o.FinYearMonth = FinancialYearEnd;

        //                        oReportResponse.ModuleRequestResult.FinanceHeader = o;


        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCurrentAsset oAsset = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCurrentAsset();
        //                        oAsset.MajorProduct = strReportID;
        //                        oAsset.CurrentAssTotal = r["CurrentAssets"].ToString();

        //                        oReportResponse.ModuleRequestResult.CurrentAsset = oAsset;

        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCurrentLiabilities oLiability = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultCurrentLiabilities();
        //                        oLiability.MajorProduct = strReportID;
        //                        oLiability.CurrentLiabilityTotal = r["CurrentLiability"].ToString();

        //                        oReportResponse.ModuleRequestResult.CurrentLiabilities = oLiability;

        //                        i++;

        //                    }
        //                }

        //                if (dsReport.Tables.Contains("CommercialOperationAssessment"))
        //                {
        //                    count = dsReport.Tables["CommercialOperationAssessment"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.Operations = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperations();
        //                    oReportResponse.ModuleRequestResult.Operations.Operation = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperationsOperation();


        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialOperationAssessment"].Rows)
        //                    {
        //                        string importcompany = string.Empty, exportCompany = string.Empty;

        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperationsOperation o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperationsOperation();

        //                        o.MajorProduct = strReportID;
        //                        o.Date = r["Createdondate"].ToString();
        //                        o.SicCode = r["SICCID"].ToString();
        //                        o.SicDescription = r["DefinitionOfCode"].ToString();
        //                        o.ImportComment = r["ImportCountryID"].ToString();
        //                        if (!string.IsNullOrEmpty(o.ImportComment))
        //                        {
        //                            o.Import = "Y";
        //                        }
        //                        else
        //                        {
        //                            o.Import = "N";
        //                        }

        //                        o.ExportComment = r["ExportCountryID"].ToString();
        //                        if (!string.IsNullOrEmpty(o.ExportComment))
        //                        {
        //                            o.Export = "Y";
        //                        }
        //                        else
        //                        {
        //                            o.Export = "N";
        //                        }

        //                        oReportResponse.ModuleRequestResult.Operations.Operation = o;

        //                        i++;

        //                    }
        //                }

        //                if (dsReport.Tables.Contains("CommercialNumberOfEmployee"))
        //                {
        //                    count = dsReport.Tables["CommercialNumberOfEmployee"].Rows.Count;

        //                    if (oReportResponse.ModuleRequestResult.Operations == null)
        //                    {
        //                        oReportResponse.ModuleRequestResult.Operations = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperations();
        //                    }

        //                    if (oReportResponse.ModuleRequestResult.Operations.Operation == null)
        //                    {
        //                        oReportResponse.ModuleRequestResult.Operations.Operation = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperationsOperation();
        //                    }



        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialNumberOfEmployee"].Rows)
        //                    {
        //                        string importcompany = string.Empty, exportCompany = string.Empty;

        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperationsOperation o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultOperationsOperation();
        //                        o = oReportResponse.ModuleRequestResult.Operations.Operation;

        //                        o.NumberSalesStaff = (int.Parse(r["Salaried"].ToString()) + int.Parse(r["Contracted"].ToString())).ToString().Substring(0, 6);
        //                        o.NumberWageStaff = (int.Parse(r["Casual"].ToString()) + int.Parse(r["Waged"].ToString())).ToString().Substring(0, 6);
        //                        o.TotalNumberStaff = (int.Parse(r["Salaried"].ToString()) + int.Parse(r["Contracted"].ToString()) + int.Parse(r["Casual"].ToString()) + int.Parse(r["Waged"].ToString())).ToString().Substring(0, 6);


        //                        oReportResponse.ModuleRequestResult.Operations.Operation = o;

        //                        i++;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation");

        //                //Notarial Bond

        //                if (dsReport.Tables.Contains("CommercialPrincipalInformation"))
        //                {
        //                    count = dsReport.Tables["CommercialPrincipalInformation"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.Principals = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipals();
        //                    oReportResponse.ModuleRequestResult.Principals.Principals = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalsPrincipals[count];

        //                    oReportResponse.ModuleRequestResult.PrincipalLinks = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalLinks();
        //                    oReportResponse.ModuleRequestResult.PrincipalLinks.PrincipalLink = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalLinksPrincipalLink[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialPrincipalInformation"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalsPrincipals o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalsPrincipals();

        //                        //o.AssetNotarialBond
        //                        //o.CivilCourtRecord
        //                        //o.Comment
        //                        //o.ConfirmedByRegistrar
        //                        //o.DateDisputed
        //                        o.DateOfBirth = r["BirthDate"].ToString();
        //                        //o.DateStarted = 
        //                        //o.DebtCouncilDate
        //                        //o.DebtCouncilDesc
        //                        //o.Deeds
        //                        //o.DefaultData
        //                        //o.EmpiricaExclusionCode
        //                        //o.EmpiricaExclusionDescription
        //                        //o.EmpiricaReasonCode
        //                        //o.EmpiricaReasonDescription
        //                        //o.EmpiricaScore
        //                        o.Forename1 = r["FirstName"].ToString();
        //                        o.Forename2 = r["SecondName"].ToString();
        //                        // o.Forename3 = r[""]
        //                        o.Surname = r["Surname"].ToString();
        //                        o.IDNumber = r["IDNo"].ToString();
        //                        //o.InfoDate 
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.NoticesOrNotarialBonds 
        //                        o.Position = r["Designation"].ToString();

        //                        oReportResponse.ModuleRequestResult.Principals.Principals[i] = o;


        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalLinksPrincipalLink olinks = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultPrincipalLinksPrincipalLink();
        //                        olinks.ConsumerNo = r["DirectorID"].ToString();
        //                        olinks.DateOfBirth = r["BirthDate"].ToString();
        //                        //olinks.Established
        //                        olinks.Forename1 = r["FirstName"].ToString();
        //                        olinks.Forename2 = r["SecondName"].ToString();
        //                        //olinks.Forename3
        //                        olinks.IDNumber = r["IDNo"].ToString();
        //                        //olinks.LinkedBusinessDefaults
        //                        //olinks.LinkedCompanies 
        //                        olinks.MajorProduct = ObjSearch.ReportID.ToString();
        //                        olinks.Surname = r["Surname"].ToString();

        //                        int countDirLinks = 0;

        //                        if (dsReport.Tables.Contains("DirectorCurrentBusinessinterests"))
        //                        {
        //                            foreach (DataRow rcurrbusint in dsReport.Tables["DirectorCurrentBusinessinterests"].Rows)
        //                            {
        //                                if (rcurrbusint["DirectorID"].ToString() == olinks.ConsumerNo)
        //                                {
        //                                    countDirLinks++;
        //                                }
        //                            }

        //                            int iDirLinks = 0;
        //                            foreach (DataRow rcurrbusint in dsReport.Tables["DirectorCurrentBusinessinterests"].Rows)
        //                            {
        //                                if (rcurrbusint["DirectorID"].ToString() == olinks.ConsumerNo)
        //                                {
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:start LinkedCompanies");
        //                                    if (olinks.LinkedCompanies == null)
        //                                    {
        //                                        olinks.LinkedCompanies = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedCompanies();
        //                                        olinks.LinkedCompanies.LinkedCompanies = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedCompany[countDirLinks];
        //                                    }
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:end LinkedCompanies");


        //                                    XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedCompany oLinkedCompanies = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedCompany();
        //                                    oLinkedCompanies.BusinessName = rcurrbusint["CommercialName"].ToString();
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:CommercialName LinkedCompanies");
        //                                    oLinkedCompanies.BusinessStatus = rcurrbusint["CommercialStatus"].ToString();
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:CommercialStatus LinkedCompanies");
        //                                    oLinkedCompanies.RegisteredDate = rcurrbusint["RegistrationDate"].ToString();
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:RegistrationDate LinkedCompanies");
        //                                    if (olinks.LinkedBusinessDefaults == null)
        //                                    {
        //                                        olinks.LinkedBusinessDefaults = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedBusinessDefaults();
        //                                        olinks.LinkedBusinessDefaults.LinkedBusinessDefaults = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedBusinessDefault[countDirLinks];
        //                                    }

        //                                    XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedBusinessDefault oLinkedBusinessDefaults = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultLinkedBusinessDefault();
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:CommercialID LinkedCompanies");
        //                                    oLinkedBusinessDefaults.ITNumber = int.Parse(rcurrbusint["CommercialID"].ToString());
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:strReportID LinkedCompanies");
        //                                    oLinkedBusinessDefaults.MajorProduct = strReportID.ToString();
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:RegistrationNo LinkedCompanies");
        //                                    oLinkedBusinessDefaults.RegistrationNo = rcurrbusint["RegistrationNo"].ToString();
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation" + iDirLinks.ToString());

        //                                    olinks.LinkedCompanies.LinkedCompanies[iDirLinks] = oLinkedCompanies;
        //                                    olinks.LinkedBusinessDefaults.LinkedBusinessDefaults[iDirLinks] = oLinkedBusinessDefaults;

        //                                    iDirLinks++;
        //                                }
        //                            }
        //                        }

        //                        //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation assign" );

        //                        oReportResponse.ModuleRequestResult.PrincipalLinks.PrincipalLink[i] = olinks;

        //                        //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation end");

        //                        i++;

        //                    }
        //                }



        //                // Principal Clearances

        //                if (dsReport.Tables.Contains("DirectorJudgments"))
        //                {
        //                    i = 0;
        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "JUDG")
        //                        {
        //                            count = count + 1;
        //                        }
        //                    }

        //                    oReportResponse.ModuleRequestResult.ConsumerJudgements = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerJudgements();
        //                    oReportResponse.ModuleRequestResult.ConsumerJudgements.ConsumerJudgement = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerJudgementsConsumerJudgement[count];


        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "JUDG")
        //                        {
        //                            XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerJudgementsConsumerJudgement o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerJudgementsConsumerJudgement();

        //                            //o.Address1 = r[""]
        //                            o.AdminMonthlyAmount = r["DisputeAmt"].ToString();
        //                            // o.AdminNoMonths = r[""]
        //                            //o.AdminStartDate
        //                            o.Amount = r["DisputeAmt"].ToString();
        //                            o.AttorneyName = r["AttorneyName"].ToString();
        //                            o.AttorneyPhone = r["TelephoneNo"].ToString();
        //                            //o.AttorneyReference 
        //                            o.CaptureDate = r["DateLoaded"].ToString();
        //                            o.CaseNumber = r["CaseNumber"].ToString();
        //                            o.ConsumerNumber = r["DirectorID"].ToString();
        //                            o.CourtName = r["CourtName"].ToString();
        //                            //o.CourtNameCode
        //                            //o.CourtType = r[""]
        //                            //o.CourtTypeCode
        //                            //o.DateOfBirth=
        //                            //o.DebtCode
        //                            o.DebtDescription = r["CaseType"].ToString();
        //                            //o.DefendantName
        //                            //o.DefNo
        //                            o.IDNo = r["IDNo"].ToString();
        //                            //o.JudgementCode=r[""]
        //                            o.JudgementDate = r["JudgmentDate"].ToString();
        //                            o.JudgementDescription = r["CaseReason"].ToString();
        //                            o.MajorProduct = ObjSearch.ReportID.ToString();
        //                            //o.MasterNumber
        //                            //o.Message
        //                            o.PlaintiffName = r["PlaintiffName"].ToString();
        //                            //o.Remarks
        //                            //o.TradeStyle 
        //                            //o.TransType

        //                            oReportResponse.ModuleRequestResult.ConsumerJudgements.ConsumerJudgement[i] = o;
        //                            i++;
        //                        }

        //                    }
        //                }

                        
        //                if (dsReport.Tables.Contains("DirectorDefaultAlert"))
        //                {
        //                    count = dsReport.Tables["DirectorDefaultAlert"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.ConsumerDefaults = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerDefaults();
        //                    oReportResponse.ModuleRequestResult.ConsumerDefaults.ConsumerDefault = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerDefaultsConsumerDefault[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["DirectorDefaultAlert"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerDefaultsConsumerDefault o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerDefaultsConsumerDefault();

        //                        o.Amount = r["Amount"].ToString();
        //                        //o.City 
        //                        o.Remarks = r["Comments"].ToString();
        //                        //o.Country 
        //                        //o.DefaultAddress 
        //                        o.DefaultDate = r["StatusDate"].ToString();
        //                        o.Name = r["Surname"].ToString();
        //                        //o.DefaultTradeStyle
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.Message 
        //                        //o.NumberFound
        //                        //o.OnBehalfOf
        //                        //o.PostCode
        //                        //o.SerialNo
        //                        o.DebtDesc = r["Statuscode"].ToString();
        //                        o.SubscriberName = r["Company"].ToString();
        //                        //o.Suburb                              
        //                        o.Account = r["AccountNo"].ToString();


        //                        oReportResponse.ModuleRequestResult.ConsumerDefaults.ConsumerDefault[i] = o;

        //                        i++;
        //                    }
        //                }

        //                if (dsReport.Tables.Contains("DirectorJudgments"))
        //                {

        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
        //                        {
        //                            count = count + 1;
        //                        }
        //                    }

        //                    oReportResponse.ModuleRequestResult.ConsumerNotices = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerNotices();
        //                    oReportResponse.ModuleRequestResult.ConsumerNotices.ConsumerNotice = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
        //                        {
        //                            XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice();
        //                            //o.Address1 = r[""]
        //                            // o.AdminMonthlyAmount = r["DisputeAmt"].ToString();
        //                            // o.AdminNoMonths = r[""]
        //                            //o.AdminStartDate
        //                            o.Amount = r["DisputeAmt"].ToString();
        //                            o.AttorneyName = r["AttorneyName"].ToString();
        //                            o.AttorneyPhone = r["TelephoneNo"].ToString();
        //                            //o.AttorneyReference 
        //                            o.CaptureDate = r["DateLoaded"].ToString();
        //                            o.CaseNumber = r["CaseNumber"].ToString();
        //                            o.ConsumerNumber = r["DirectorID"].ToString();
        //                            o.CourtName = r["CourtName"].ToString();
        //                            //o.Cou1``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````-rtNameCode
        //                            //o.CourtType = r[""]
        //                            //o.CourtTypeCode
        //                            //o.DateOfBirth=
        //                            //o.DebtCode
        //                            // o.DebtDescription = r["CaseType"].ToString();
        //                            //o.DefendantName
        //                            //o.DefNo
        //                            o.IDNo = r["IDNo"].ToString();
        //                            //o.JudgementCode=r[""]
        //                            //o.JudgementDate = r["JudgmentDate"].ToString();
        //                            //o.JudgementDescription = r["CaseReason"].ToString();
        //                            o.MajorProduct = ObjSearch.ReportID.ToString();
        //                            //o.MasterNumber
        //                            //o.Message
        //                            o.PlaintiffName = r["PlaintiffName"].ToString();
        //                            //o.Remarks
        //                            //o.TradeStyle 
        //                            //o.TransType

        //                            oReportResponse.ModuleRequestResult.ConsumerNotices.ConsumerNotice[i] = o;

        //                            i++;
        //                        }

        //                    }
        //                }

        //                // Consumer NotarialBonds

        //                //if (dsReport.Tables.Contains("DirectorDetail"))
        //                //{

        //                //    oReportResponse.ModuleRequestResult.ConsumerInfoNO04 = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerInfoNO04();                           

        //                //    i = 0;

        //                //    foreach (DataRow r in dsReport.Tables["DirectorDetail"].Rows)
        //                //    {
        //                //        if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
        //                //        {
        //                //            XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice();
        //                //            //o.Address1 = r[""]
        //                //            // o.AdminMonthlyAmount = r["DisputeAmt"].ToString();
        //                //            // o.AdminNoMonths = r[""]
        //                //            //o.AdminStartDate
        //                //            o.Amount = r["DisputeAmt"].ToString();
        //                //            o.AttorneyName = r["AttorneyName"].ToString();
        //                //            o.AttorneyPhone = r["TelephoneNo"].ToString();
        //                //            //o.AttorneyReference 
        //                //            o.CaptureDate = r["DateLoaded"].ToString();
        //                //            o.CaseNumber = r["CaseNumber"].ToString();
        //                //            o.ConsumerNumber = r["DirectorID"].ToString();
        //                //            o.CourtName = r["CourtName"].ToString();
        //                //            //o.CourtNameCode
        //                //            //o.CourtType = r[""]
        //                //            //o.CourtTypeCode
        //                //            //o.DateOfBirth=
        //                //            //o.DebtCode
        //                //            // o.DebtDescription = r["CaseType"].ToString();
        //                //            //o.DefendantName
        //                //            //o.DefNo
        //                //            o.IDNo = r["IDNo"].ToString();
        //                //            //o.JudgementCode=r[""]
        //                //            //o.JudgementDate = r["JudgmentDate"].ToString();
        //                //            //o.JudgementDescription = r["CaseReason"].ToString();
        //                //            o.MajorProduct = ObjSearch.ReportID.ToString();
        //                //            //o.MasterNumber
        //                //            //o.Message
        //                //            o.PlaintiffName = r["PlaintiffName"].ToString();
        //                //            //o.Remarks
        //                //            //o.TradeStyle 
        //                //            //o.TransType

        //                //            oReportResponse.ModuleRequestResult.ConsumerNotices.ConsumerNotice[i] = o;

        //                //            i++;
        //                //        }

        //                //    }
        //                //}

        //                if (dsReport.Tables.Contains("BusinessEnquiryHistory"))
        //                {
        //                    count = ds.Tables["BusinessEnquiryHistory"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.EnquiryHistory = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultEnquiryHistoryEnquiryHistory();

        //                    oReportResponse.ModuleRequestResult.EnquiryHistory.EnquiryHistory = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultEnquiryHistory[count];

        //                    i = 0;

        //                    foreach (DataRow dr in ds.Tables["BusinessEnquiryHistory"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultEnquiryHistory o = new XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResultEnquiryHistory();
        //                        o.MajorProduct = strReportID.ToString();
        //                        o.DateOfEnquiry = dr["EnquiryDate"].ToString();
        //                        o.ContactPhone = dr["SubscriberContact"].ToString();
        //                        o.Subscriber = dr["SubscriberName"].ToString();

        //                        oReportResponse.ModuleRequestResult.EnquiryHistory.EnquiryHistory[i] = o;

        //                        i++;
        //                    }
        //                }

        //            }



        //            string xml = dsReport.GetXml();
        //            xml = xml.Replace("_x0036_", "");
        //            ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
        //            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
        //            ObjResponse.ResponseData = xml;

        //            dtReportInformation.Clear();
        //            dtError.Clear();
        //            ds.Clear();
        //            dsReport.Clear();

        //            dtReportInformation.Dispose();
        //            dtError.Dispose();
        //            ds.Dispose();
        //            dsReport.Dispose();
        //            if (dsTracPlusDataSegment != null)
        //            {
        //                dsTracPlusDataSegment.Dispose();
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //            ObjResponse.ResponseData = e.Message;


        //            oReportResponse.ModuleRequestResult.ResponseStatus = XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResult.enumRResultResponseStatus.Failure.ToString();
        //            oReportResponse.ModuleRequestResult.ErrorMessage = "Error While Processing your request, Please Contact XDS";
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        ObjResponse.ResponseData = ex.Message;
        //    }

        //    return ObjResponse;

        //}


        //public XDSPortalLibrary.Entity_Layer.Response GetBusinessEnquiryReportCustomVettingR(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch, XDSPortalLibrary.Entity_Layer.MTN.ModuleRequestResponse oinputResponse, out XDSPortalLibrary.Entity_Layer.MTN.ModuleRequestResponse oReportResponse)
        //{
        //    oReportResponse = oinputResponse;
        //    XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
        //    try
        //    {

        //        DataSet dsDataSegment = new DataSet();
        //        dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

        //        DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


        //        ObjResponse.ResponseKeyType = "B";
        //        ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
        //        ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

        //        ds = new DataSet("Commercial");

        //        DataSet dsReport = new DataSet("Commercial");

        //        DataTable dtReportInformation = new DataTable("ReportInformation");
        //        DataColumn dcReportID = new DataColumn("ReportID");
        //        DataColumn dcReportName = new DataColumn("ReportName");
        //        string strReportID = ObjSearch.ReportID.ToString();
        //        string strReportName = "";

        //        dtReportInformation.Columns.Add(dcReportID);
        //        dtReportInformation.Columns.Add(dcReportName);

        //        DataRow drReportInformation = dtReportInformation.NewRow();
        //        //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
        //        //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
        //        drReportInformation["ReportID"] = ObjSearch.ReportID.ToString();
        //        drReportInformation["ReportName"] = ObjSearch.ReportName.ToString();

        //        dtReportInformation.Rows.Add(drReportInformation);
        //        dsReport.Tables.Add(dtReportInformation);

        //        if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
        //            ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


        //        Thread thDirectorCurrentBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thDirectorPreviousBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialBusinessInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialPrincipalInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
        //        Thread thCommercialScore = new Thread(new ParameterizedThreadStart(ProcessReport));

        //        thDirectorCurrentBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorCurrentBusinessInterests] {0} , {1}", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
        //        thDirectorPreviousBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorPreviousBusinessInterests] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
        //        thCommercialInformation.Start(string.Format("execute [dbo].[spCLR_CommercialInformation] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString(), String.IsNullOrEmpty(ObjSearch.TmpReference) ? "0" : ObjSearch.TmpReference.ToString()));
        //        thCommercialBusinessInformation.Start(string.Format("execute [dbo].[spCLR_CommercialBusinessInformation] {0} , '{1}','{2}', {3}", ObjSearch.CommercialID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.ProductID.ToString()));
        //        thCommercialPrincipalInformation.Start(string.Format("execute [dbo].[spCLR_CommercialPrincipalInformation] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
        //        thCommercialScore.Start(string.Format("execute [dbo].[spCommercialScoring] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString()));

        //        thDirectorCurrentBusinessInterests.Join();
        //        thDirectorPreviousBusinessInterests.Join();
        //        thCommercialInformation.Join();
        //        thCommercialBusinessInformation.Join();
        //        thCommercialPrincipalInformation.Join();
        //        thCommercialScore.Join();

        //        if (dtError.Rows.Count > 0)
        //        {
        //            throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
        //        }



        //        dsReport = ds;

        //        File.AppendAllText(@"C:\Log\mtnvet.txt", dsReport.GetXml());


        //        try
        //        {
        //            string COM003 = string.Empty;
        //            double totalPropertySize = 0;
        //            double com026 = 0;
        //            int com024 = 0;

        //            List<Thread> threads = new List<Thread>();
        //            List<Thread> Pressage = new List<Thread>();

        //            if (dsReport.Tables.Contains("CommercialPrincipalInformation"))
        //            {
        //                foreach (DataRow r in dsReport.Tables["CommercialPrincipalInformation"].Rows)
        //                {
        //                    Thread thread = new Thread(ProcessReport)
        //                    {
        //                        Name = string.Format("{0}", r["DirectorID"].ToString())
        //                    };
        //                    thread.Start(string.Format("execute [dbo].[spCLR_MTN_ProcessDirectorVariables] {0}, {1}, {2},{3},{4},{5},{6},'{7}','{8}','{9}' ", r["ConsumerID"].ToString(), ObjSearch.subscriberID.ToString(), r["DirectorID"].ToString(), ObjSearch.SubscriberEnquiryID.ToString(), ObjSearch.SubscriberEnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.Username.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.AssociationCode.ToString()));
        //                    threads.Add(thread);


        //                    Thread Th = new Thread(ProcessReport)
        //                    {
        //                        Name = string.Format("Pressage {0}", r["DirectorID"].ToString())
        //                    };
        //                    Th.Start(string.Format("execute [dbo].[spclr_MTN_PrincipalPressage] {0}, {1}, {2},{3},{4},{5},{6},{7},'{8}','{9}','{10}',{11} ", ObjSearch.CommercialID.ToString(), r["ConsumerID"].ToString(), r["DirectorID"].ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberEnquiryID.ToString(), ObjSearch.SubscriberEnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.Username.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.AssociationCode.ToString(),"1"));
        //                    Pressage.Add(Th);
        //                }
        //            }
        //            int count = 0, i = 0; string FinancialYearEnd = string.Empty;

        //            if (dsReport.Tables.Count > 0)
        //            {

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialBusinessInformation");

        //                if (dsReport.Tables.Contains("CommercialBusinessInformation"))
        //                {
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step1");

        //                    oReportResponse.ModuleRequestResult.RegistrationDetails = new XDSPortalLibrary.Entity_Layer.MTN.RegistrationDetails();
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step2");
        //                    oReportResponse.ModuleRequestResult.Header = new XDSPortalLibrary.Entity_Layer.MTN.Header();
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step3");
        //                    oReportResponse.ModuleRequestResult.Names = new XDSPortalLibrary.Entity_Layer.MTN.Names();
        //                    File.AppendAllText(@"C:\Log\mtnvet.txt", "step4");
        //                    foreach (DataRow r in dsReport.Tables["CommercialBusinessInformation"].Rows)
        //                    {
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step5");
        //                        XDSPortalLibrary.Entity_Layer.MTN.RegistrationDetails o = new XDSPortalLibrary.Entity_Layer.MTN.RegistrationDetails();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step6");

        //                        COM003 = r["CommercialStatusCode"].ToString();

        //                        o.MajorProduct = ObjSearch.ReportID.ToString();

        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step7");
        //                        o.RegistrationDate = r["RegistrationDate"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step8");
        //                        // o.RegistrationStatusCode = r[""].ToString();
        //                        o.RegistrationStatusDesc = r["CommercialStatus"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step9");
        //                        //o.LiquidationDate
        //                        o.RegistrationNo = r["RegistrationNo12"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step10");
        //                        // o.CompanyTypeCode = 
        //                        o.CompanyTypeDesc = r["CommercialType"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step11");
        //                        //o.RegistrationCountry
        //                        o.PreviousRegistrationNo = new string[1];
        //                        o.PreviousRegistrationNo[0] = r["RegistrationNoConverted"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step12");
        //                        //o.Auditors
        //                        o.RegisteredAddress = r["PhysicalAddress"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step13");
        //                        //o.Suburb
        //                        //o.City
        //                        //o.Country
        //                        //o.PostCode
        //                        o.AuthorisedCapital = r["AuthorisedCapitalAmt"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step14");
        //                        o.IssuedCapital = r["IssuedCapitalAmt"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step15");
        //                        //o.StatedCapital
        //                        //o.Reg_Info_Date
        //                        o.PostalAddress = new string[1];
        //                        o.PostalAddress[0] = r["PostalAddress"].ToString();
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step16");
        //                        //o.PostalPostCode
        //                        o.FinancialYearEnd = r["FinancialYearEnd"].ToString();

        //                        FinancialYearEnd = o.FinancialYearEnd;

        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step8");
        //                        oReportResponse.ModuleRequestResult.RegistrationDetails = o;
        //                        File.AppendAllText(@"C:\Log\mtnvet.txt", "step9");
        //                        XDSPortalLibrary.Entity_Layer.MTN.Header oHeader = new XDSPortalLibrary.Entity_Layer.MTN.Header();
        //                        oHeader.MajorProduct = ObjSearch.ReportID.ToString();
        //                        oHeader.DateOfHeader = DateTime.Now.ToString("yyyyMMdd");
        //                        oHeader.StartDate = r["BusinessStartDate"].ToString();
        //                        oHeader.BusinessType = r["CommercialType"].ToString();
        //                        //oHeader.DunsNumber
        //                        oHeader.StartingCapital = r["IssuedCapitalAmt"].ToString().Substring(0, r["IssuedCapitalAmt"].ToString().Length > 10 ? 10 : r["IssuedCapitalAmt"].ToString().Length);
        //                        //oHeader.PurchasePrice
        //                        oHeader.Industry = r["SIC"].ToString();
        //                        //oHeader.BusinessFunction
        //                        // oHeader.BusinessCategory = r
        //                        oHeader.PhysicalAddress = r["PhysicalAddress"].ToString();
        //                        //oHeader.Suburb
        //                        //oHeader.City
        //                        //oHeader.Country
        //                        //oHeader.PostCode
        //                        oHeader.PostalAddress = r["PostalAddress"].ToString();
        //                        //oHeader.PostalSuburb 
        //                        //oHeader.PostalCity
        //                        //oHeader.PostalCountry
        //                        //oHeader.PostalPostCode
        //                        oHeader.Phone = r["TelephoneNo"].ToString();
        //                        oHeader.Fax = r["FaxNo"].ToString();
        //                        //oHeader.VATNumbers = 
        //                        oHeader.Website = r["BussWebsite"].ToString();
        //                        oHeader.Email = r["BussEmail"].ToString();
        //                        oHeader.TaxNumber = r["TaxNo"].ToString();

        //                        oReportResponse.ModuleRequestResult.Header = oHeader;



        //                        XDSPortalLibrary.Entity_Layer.MTN.Names oNames = new XDSPortalLibrary.Entity_Layer.MTN.Names();

        //                        oNames.PreviousNames = new string[1];
        //                        oNames.PreviousNames[0] = r["PreviousBussName"].ToString();
        //                        oNames.PreviousNameDates = new string[1]; ;

        //                        oNames.PreviousNameDates[0] = r["NameChangeDate"].ToString();
        //                        oNames.AKAName = r["TradeName"].ToString();
        //                        oNames.BusinessName = r["CommercialName"].ToString();

        //                        oReportResponse.ModuleRequestResult.Names = oNames;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialBankCodeHistory");

        //                if (dsReport.Tables.Contains("CommercialBankCodeHistory"))
        //                {
        //                    count = dsReport.Tables["CommercialBankCodeHistory"].Rows.Count;
        //                    oReportResponse.ModuleRequestResult.BankCodes = new XDSPortalLibrary.Entity_Layer.MTN.BankCodes[count];


        //                    oReportResponse.ModuleRequestResult.BankHistory = new XDSPortalLibrary.Entity_Layer.MTN.BankHistory[count];

        //                    oReportResponse.ModuleRequestResult.GeneralBankingInfo = new XDSPortalLibrary.Entity_Layer.MTN.GeneralBankingInfo[1];


        //                    i = 0;
        //                    foreach (DataRow r in dsReport.Tables["CommercialBankCodeHistory"].Rows)
        //                    {

        //                        DateTime dtrequesteddate = DateTime.Now;
        //                        DateTime requesteddate = DateTime.Now;
        //                        string bankCode = string.Empty;

        //                        bankCode = r["BankCode"].ToString();



        //                        if (DateTime.TryParseExact(r["DateRequested"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtrequesteddate))
        //                        {
        //                            if (i == 0)
        //                            {
        //                                requesteddate = dtrequesteddate;
        //                            }

        //                            if (dtrequesteddate >= requesteddate && !string.IsNullOrEmpty(bankCode))
        //                            {
        //                                XDSPortalLibrary.Entity_Layer.MTN.BankCodes o = new XDSPortalLibrary.Entity_Layer.MTN.BankCodes();

        //                                o.MajorProduct = ObjSearch.ReportID.ToString();
        //                                o.AccountNo = r["AccountNumber"].ToString();
        //                                o.BankName = r["Bank"].ToString();
        //                                o.Branch = r["BranchName"].ToString();
        //                                o.BankCode = r["BankCode"].ToString();
        //                                o.BankCodeDesc = r["Bank_Comments"].ToString();
        //                                if (!string.IsNullOrEmpty(r["Comment"].ToString()))
        //                                {
        //                                    o.Comment = new string[1];
        //                                    o.Comment[0] = r["Comment"].ToString();
        //                                }
        //                                o.Amount = r["EnquiryAmount"].ToString();
        //                                o.StartDate = r["DateOpened"].ToString();
        //                                o.Terms = r["terms"].ToString();
        //                                o.Date = r["DateRequested"].ToString();


        //                                oReportResponse.ModuleRequestResult.BankCodes[i] = o;
        //                                requesteddate = dtrequesteddate;


        //                                XDSPortalLibrary.Entity_Layer.MTN.GeneralBankingInfo oBankInfo = new XDSPortalLibrary.Entity_Layer.MTN.GeneralBankingInfo();
        //                                oBankInfo.MajorProduct = ObjSearch.ReportID.ToString();
        //                                oBankInfo.AccountNo = r["AccountNumber"].ToString();
        //                                oBankInfo.BankName = r["Bank"].ToString();
        //                                oBankInfo.Branch = r["BranchName"].ToString();
        //                                if (!string.IsNullOrEmpty(r["Comment"].ToString()))
        //                                {
        //                                    oBankInfo.Comment = new string[1];
        //                                    oBankInfo.Comment[0] = r["Comment"].ToString();
        //                                }
        //                                oBankInfo.StartDate = r["DateOpened"].ToString();
        //                                oBankInfo.InfoDate = r["DateRequested"].ToString();

        //                                oReportResponse.ModuleRequestResult.GeneralBankingInfo[i] = oBankInfo;
        //                            }


        //                        }



        //                        XDSPortalLibrary.Entity_Layer.MTN.BankHistory oHis = new XDSPortalLibrary.Entity_Layer.MTN.BankHistory();

        //                        oHis.MajorProduct = ObjSearch.ReportID.ToString();
        //                        oHis.AccountNo = r["AccountNumber"].ToString();
        //                        oHis.BankName = r["Bank"].ToString();
        //                        oHis.Branch = r["BranchName"].ToString();
        //                        oHis.BankCode = r["BankCode"].ToString();
        //                        oHis.BankCodeDesc = r["Bank_Comments"].ToString();
        //                        if (!string.IsNullOrEmpty(r["Comment"].ToString()))
        //                        {
        //                            oHis.Comment = new string[1];
        //                            oHis.Comment[0] = r["Comment"].ToString();
        //                        }
        //                        oHis.Amount = r["EnquiryAmount"].ToString();
        //                        oHis.StartDate = r["DateOpened"].ToString();
        //                        oHis.Terms = r["terms"].ToString();
        //                        oHis.Date = r["DateRequested"].ToString();

        //                        oReportResponse.ModuleRequestResult.BankHistory[i] = oHis;

        //                        i++;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialTradeReferencesInformation");

        //                if (dsReport.Tables.Contains("CommercialTradeReferencesInformation"))
        //                {
        //                    count = dsReport.Tables["CommercialTradeReferencesInformation"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.TradeHistory = new XDSPortalLibrary.Entity_Layer.MTN.TradeHistory[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialTradeReferencesInformation"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.TradeHistory o = new XDSPortalLibrary.Entity_Layer.MTN.TradeHistory();

        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        o.DateOfRef = r["Createdondate"].ToString();
        //                        // o.YearsKnown = 
        //                        //o.MonthsKnown
        //                        o.CreditLimit = r["CreditLimit"].ToString();
        //                        o.Unlimited = "N";// r["MaxLimit"].ToString();
        //                        o.Purchases = r["AveragePurchases"].ToString();
        //                        o.TermsTaken = r["TermsTaken"].ToString();
        //                        o.TermsGiven = r["Terms"].ToString();
        //                        // o.Discount = 
        //                        // o.ReferenceName = 
        //                        //o.Insurance
        //                        //o.InsuranceDesc
        //                        o.Comment = r["Comment"].ToString();
        //                        o.AssuredValue = r["SuretyValue"].ToString();
        //                        // o.Obtained = r[""]

        //                        oReportResponse.ModuleRequestResult.TradeHistory[i] = o;

        //                        i++;
        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPropertyInformation");

        //                if (dsReport.Tables.Contains("CommercialPropertyInformation"))
        //                {
        //                    int noofProperties = dsReport.Tables["CommercialPropertyInformation"].Rows.Count;
        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA = new XDSPortalLibrary.Entity_Layer.MTN.BusinessDeedsSummaryDA[1];

        //                    double totalValue = 0;
        //                    double totalBond = 0;
                           

        //                    string OldestPropertyDate = string.Empty, OldestPropertyValue = string.Empty;
        //                    DateTime purchasedate = DateTime.Now;

        //                    List<string> Deedsoffice = new List<string>();

        //                    foreach (DataRow r in dsReport.Tables["CommercialPropertyInformation"].Rows)
        //                    {
        //                        if (!Deedsoffice.Contains(r["DeedsOffice"]))
        //                        {
        //                            Deedsoffice.Add(r["DeedsOffice"].ToString());
        //                        }

        //                        totalValue = totalValue + double.Parse(r["PurchasePriceAmt"].ToString());
        //                        totalBond = totalBond + double.Parse(r["BondAmt"].ToString());

        //                        string resultString = Regex.Match(r["Extent"].ToString(), @"\d+").Value;



        //                        if ( double.TryParse(resultString,out totalPropertySize))
        //                        {
        //                            com026 = com026 + totalPropertySize;
        //                        }

        //                        DateTime dtpurchasedate = DateTime.Now;

        //                        if (DateTime.TryParseExact(r["PurchaseDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtpurchasedate))
        //                        {
        //                            if (i == 0)
        //                            {
        //                                purchasedate = dtpurchasedate;
        //                            }
        //                            if (purchasedate >= dtpurchasedate)
        //                            {
        //                                OldestPropertyDate = r["PurchaseDate"].ToString();
        //                                OldestPropertyValue = r["PurchasePriceAmt"].ToString();

        //                                purchasedate = dtpurchasedate;

        //                            }
        //                            else
        //                            {
        //                                com024 = int.Parse( (DateTime.Now.Subtract(dtpurchasedate).Days / (365.25 / 12)).ToString());
        //                            }

        //                        }

        //                    }

        //                    XDSPortalLibrary.Entity_Layer.MTN.BusinessDeedsSummaryDA o = new XDSPortalLibrary.Entity_Layer.MTN.BusinessDeedsSummaryDA();
        //                    o.Comment = string.Empty;
        //                    o.DeedsOffice = String.Join(",", Deedsoffice).ToString();
        //                    o.MajorProduct = ObjSearch.ReportID.ToString();
        //                    o.NumberProperties = noofProperties.ToString();
        //                    o.TotalBond = totalBond.ToString();
        //                    o.TotalOwned = noofProperties.ToString();
        //                    o.TotalValue = totalValue.ToString();

        //                    oReportResponse.ModuleRequestResult.BusinessDeedsSummaryDA[0] = o;
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialJudgment");

        //                if (dsReport.Tables.Contains("CommercialJudgment"))
        //                {
        //                    count = dsReport.Tables["CommercialJudgment"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.CourtRecords = new XDSPortalLibrary.Entity_Layer.MTN.CourtRecord[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialJudgment"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.CourtRecord o = new XDSPortalLibrary.Entity_Layer.MTN.CourtRecord();
        //                        o.ActionDate = r["CaseFilingDate"].ToString();
        //                        o.Attorney = r["AttorneyName"].ToString();
        //                        //o.AbandonDate
        //                        o.CaseNumber = r["CaseNumber"].ToString();
        //                        //o.City 
        //                        o.ClaimAmount = r["DisputeAmt"].ToString();
        //                        //o.Comment 
        //                        //o.Country
        //                        //o.CourtDistrict = r[]
        //                        //o.CourtRecordAddress =
        //                        //  o.CourtType = 
        //                        //o.DefendantDistrict
        //                        //o.DefendantName
        //                        //o.DefendantTradeStyle
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.Message
        //                        //o.NatureOfDebt
        //                        //o.NumberFound
        //                        //o.PlaintiffName = r[""]
        //                        //o.PostCode
        //                        //o.ReturnDate
        //                        //o.SerialNumber
        //                        //o.Suburb
        //                        //o.TypeCode
        //                        o.TypeDesc = r["CaseType"].ToString();

        //                        oReportResponse.ModuleRequestResult.CourtRecords[i] = o;

        //                        i++;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialDefaultAlert");

        //                if (dsReport.Tables.Contains("CommercialDefaultAlert"))
        //                {
        //                    count = dsReport.Tables["CommercialDefaultAlert"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.Defaults = new XDSPortalLibrary.Entity_Layer.MTN.Default[count];

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialDefaultAlert"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.Default o = new XDSPortalLibrary.Entity_Layer.MTN.Default();

        //                        o.Amount = r["Amount"].ToString();
        //                        //o.City
        //                        o.Comment = r["Comments"].ToString();
        //                        //o.Country 
        //                        //o.DefaultAddress 
        //                        o.DefaultDate = r["StatusDate"].ToString();
        //                        o.DefaultName = r["CommercialName"].ToString();
        //                        //o.DefaultTradeStyle
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.Message 
        //                        //o.NumberFound
        //                        //o.OnBehalfOf 
        //                        //o.PostCode
        //                        //o.SerialNo
        //                        o.Status = r["Statuscode"].ToString();
        //                        o.SubscriberName = r["Company"].ToString();
        //                        //o.Suburb 
        //                        o.SupplierName = r["Company"].ToString();

        //                        oReportResponse.ModuleRequestResult.Defaults[i] = o;

        //                        i++;

        //                    }
        //                }

        //                if (dsReport.Tables.Contains("CommercialFinancialEstimates"))
        //                {
        //                    count = dsReport.Tables["CommercialFinancialEstimates"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.FinanceHeader = new XDSPortalLibrary.Entity_Layer.MTN.FinanceHeader();


        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialFinancialEstimates"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.FinanceHeader o = new XDSPortalLibrary.Entity_Layer.MTN.FinanceHeader();

        //                        o.MajorProduct = strReportID;
        //                        o.Date = r["Createdondate"].ToString();
        //                        o.Source = r["CommercialName"].ToString();
        //                        o.TurnOverAmount1 = r["CurrentTurnOverPerAnnum"].ToString();
        //                        o.TurnOverAmount2 = r["PreviousYearTurnover"].ToString();
        //                        o.FinYearMonth = FinancialYearEnd;

        //                        oReportResponse.ModuleRequestResult.FinanceHeader = o;


        //                        XDSPortalLibrary.Entity_Layer.MTN.CurrentAsset oAsset = new XDSPortalLibrary.Entity_Layer.MTN.CurrentAsset();
        //                        oAsset.MajorProduct = strReportID;
        //                        oAsset.CurrentAssTotal = r["CurrentAssets"].ToString();

        //                        oReportResponse.ModuleRequestResult.CurrentAsset = oAsset;

        //                        XDSPortalLibrary.Entity_Layer.MTN.CurrentLiabilities oLiability = new XDSPortalLibrary.Entity_Layer.MTN.CurrentLiabilities();
        //                        oLiability.MajorProduct = strReportID;
        //                        oLiability.CurrentLiabilityTotal = r["CurrentLiability"].ToString();

        //                        oReportResponse.ModuleRequestResult.CurrentLiabilities = oLiability;

        //                        i++;

        //                    }
        //                }

        //                if (dsReport.Tables.Contains("CommercialOperationAssessment"))
        //                {
        //                    count = dsReport.Tables["CommercialOperationAssessment"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.Operations = new XDSPortalLibrary.Entity_Layer.MTN.Operation[count];



        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialOperationAssessment"].Rows)
        //                    {
        //                        string importcompany = string.Empty, exportCompany = string.Empty;

        //                        XDSPortalLibrary.Entity_Layer.MTN.Operation o = new XDSPortalLibrary.Entity_Layer.MTN.Operation();

        //                        o.MajorProduct = strReportID;
        //                        o.Date = r["Createdondate"].ToString();
        //                        o.SicCode = r["SICCID"].ToString();
        //                        o.SicDescription = r["DefinitionOfCode"].ToString();
        //                        o.ImportComment = r["ImportCountryID"].ToString();
        //                        if (!string.IsNullOrEmpty(o.ImportComment))
        //                        {
        //                            o.Import = "Y";
        //                        }
        //                        else
        //                        {
        //                            o.Import = "N";
        //                        }

        //                        o.ExportComment = r["ExportCountryID"].ToString();
        //                        if (!string.IsNullOrEmpty(o.ExportComment))
        //                        {
        //                            o.Export = "Y";
        //                        }
        //                        else
        //                        {
        //                            o.Export = "N";
        //                        }

        //                        oReportResponse.ModuleRequestResult.Operations[i] = o;

        //                        i++;

        //                    }
        //                }

        //                if (dsReport.Tables.Contains("CommercialNumberOfEmployee"))
        //                {
        //                    count = dsReport.Tables["CommercialNumberOfEmployee"].Rows.Count;

        //                    if (oReportResponse.ModuleRequestResult.Operations == null)
        //                    {
        //                        oReportResponse.ModuleRequestResult.Operations = new XDSPortalLibrary.Entity_Layer.MTN.Operation[count];
        //                    }




        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialNumberOfEmployee"].Rows)
        //                    {
        //                        string importcompany = string.Empty, exportCompany = string.Empty;

        //                        XDSPortalLibrary.Entity_Layer.MTN.Operation o = new XDSPortalLibrary.Entity_Layer.MTN.Operation();


        //                        o.NumberSalesStaff = (int.Parse(r["Salaried"].ToString()) + int.Parse(r["Contracted"].ToString())).ToString().Substring(0, 6);
        //                        o.NumberWageStaff = (int.Parse(r["Casual"].ToString()) + int.Parse(r["Waged"].ToString())).ToString().Substring(0, 6);
        //                        o.TotalNumberStaff = (int.Parse(r["Salaried"].ToString()) + int.Parse(r["Contracted"].ToString()) + int.Parse(r["Casual"].ToString()) + int.Parse(r["Waged"].ToString())).ToString().Substring(0, 6);


        //                        oReportResponse.ModuleRequestResult.Operations[i] = o;

        //                        i++;

        //                    }
        //                }

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation");

        //                //Notarial Bond

        //                if (dsReport.Tables.Contains("CommercialPrincipalInformation"))
        //                {
        //                    count = dsReport.Tables["CommercialPrincipalInformation"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.Principals = new XDSPortalLibrary.Entity_Layer.MTN.Principal[count];


        //                    oReportResponse.ModuleRequestResult.PrincipalLinks = new XDSPortalLibrary.Entity_Layer.MTN.PrincipalLink[count];


        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["CommercialPrincipalInformation"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.Principal o = new XDSPortalLibrary.Entity_Layer.MTN.Principal();

        //                        //o.AssetNotarialBond
        //                        //o.CivilCourtRecord
        //                        //o.Comment
        //                        //o.ConfirmedByRegistrar
        //                        //o.DateDisputed
        //                        o.DateOfBirth = r["BirthDate"].ToString();
        //                        //o.DateStarted = 
        //                        //o.DebtCouncilDate
        //                        //o.DebtCouncilDesc
        //                        //o.Deeds
        //                        //o.DefaultData
        //                        //o.EmpiricaExclusionCode
        //                        o.EmpiricaExclusionDescription = r["DirectorID"].ToString();
        //                        //o.EmpiricaReasonCode
        //                        //o.EmpiricaReasonDescription
        //                        //o.EmpiricaScore
        //                        o.Forename1 = r["FirstName"].ToString();
        //                        o.Forename2 = r["SecondName"].ToString();
        //                        // o.Forename3 = r[""]
        //                        o.Surname = r["Surname"].ToString();
        //                        o.IDNumber = r["IDNo"].ToString();
        //                        //o.InfoDate 
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.NoticesOrNotarialBonds 
        //                        o.Position = r["Designation"].ToString();

        //                        oReportResponse.ModuleRequestResult.Principals[i] = o;


        //                        XDSPortalLibrary.Entity_Layer.MTN.PrincipalLink olinks = new XDSPortalLibrary.Entity_Layer.MTN.PrincipalLink();
        //                        olinks.ConsumerNo = r["DirectorID"].ToString();
        //                        olinks.DateOfBirth = r["BirthDate"].ToString();
        //                        //olinks.Established
        //                        olinks.Forename1 = r["FirstName"].ToString();
        //                        olinks.Forename2 = r["SecondName"].ToString();
        //                        //olinks.Forename3
        //                        olinks.IDNumber = r["IDNo"].ToString();
        //                        //olinks.LinkedBusinessDefaults
        //                        //olinks.LinkedCompanies 
        //                        olinks.MajorProduct = ObjSearch.ReportID.ToString();
        //                        olinks.Surname = r["Surname"].ToString();

        //                        int countDirLinks = 0;

        //                        XDSPortalLibrary.Entity_Layer.MTN.LinkedCompany oLinkedCompanies = new XDSPortalLibrary.Entity_Layer.MTN.LinkedCompany();
        //                        XDSPortalLibrary.Entity_Layer.MTN.LinkedBusinessDefault oLinkedBusinessDefaults = new XDSPortalLibrary.Entity_Layer.MTN.LinkedBusinessDefault();

        //                        if (dsReport.Tables.Contains("DirectorCurrentBusinessinterests"))
        //                        {

        //                            if (olinks.LinkedCompanies == null)
        //                            {
        //                                olinks.LinkedCompanies = new XDSPortalLibrary.Entity_Layer.MTN.LinkedCompany[1];

        //                            }
        //                            if (olinks.LinkedBusinessDefaults == null)
        //                            {
        //                                olinks.LinkedBusinessDefaults = new XDSPortalLibrary.Entity_Layer.MTN.LinkedBusinessDefault[1];

        //                            }

        //                            foreach (DataRow rcurrbusint in dsReport.Tables["DirectorCurrentBusinessinterests"].Rows)
        //                            {
        //                                if (rcurrbusint["DirectorID"].ToString() == olinks.ConsumerNo)
        //                                {
        //                                    countDirLinks++;
        //                                }
        //                            }

        //                            int iDirLinks = 0;
        //                            foreach (DataRow rcurrbusint in dsReport.Tables["DirectorCurrentBusinessinterests"].Rows)
        //                            {
        //                                if (rcurrbusint["DirectorID"].ToString() == olinks.ConsumerNo)
        //                                {
        //                                     File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:start LinkedCompanies");
                                           

        //                                    if (oLinkedCompanies.BusinessName == null)
        //                                    {
        //                                        oLinkedCompanies.BusinessName = new string[countDirLinks];
        //                                    }

        //                                    if (oLinkedCompanies.BusinessStatus == null)
        //                                    {
        //                                        oLinkedCompanies.BusinessStatus = new string[countDirLinks];
        //                                    }

        //                                    if (oLinkedCompanies.RegisteredDate == null)
        //                                    {
        //                                        oLinkedCompanies.RegisteredDate = new string[countDirLinks];
        //                                    }

        //                                    if (oLinkedBusinessDefaults.ITNumber == null)
        //                                    {
        //                                        oLinkedBusinessDefaults.ITNumber = new string[countDirLinks];
        //                                    }

        //                                    if (oLinkedBusinessDefaults.RegistrationNumber == null)
        //                                    {
        //                                        oLinkedBusinessDefaults.RegistrationNumber = new string[countDirLinks];
        //                                    }

        //                                    if (oLinkedBusinessDefaults.JudgementCount == null)
        //                                    {
        //                                        oLinkedBusinessDefaults.JudgementCount = new string[countDirLinks];
        //                                    }

        //                                    if (oLinkedBusinessDefaults.AdverseCount == null)
        //                                    {
        //                                        oLinkedBusinessDefaults.AdverseCount = new string[countDirLinks];
        //                                    }
        //                                      File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:end LinkedCompanies");



        //                                      oLinkedCompanies.BusinessName[iDirLinks] = rcurrbusint["CommercialName"].ToString();
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:CommercialName LinkedCompanies");
        //                                      oLinkedCompanies.BusinessStatus[iDirLinks] = rcurrbusint["CommercialStatus"].ToString();
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:CommercialStatus LinkedCompanies");
        //                                      oLinkedCompanies.RegisteredDate[iDirLinks] = rcurrbusint["RegistrationDate"].ToString();
        //                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:RegistrationDate LinkedCompanies");
                                           

                                            
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:CommercialID LinkedCompanies");
        //                                    oLinkedBusinessDefaults.ITNumber[iDirLinks] = rcurrbusint["CommercialID"].ToString();
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:strReportID LinkedCompanies");
                                            
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:RegistrationNo LinkedCompanies");
        //                                    oLinkedBusinessDefaults.RegistrationNumber[iDirLinks] = rcurrbusint["RegistrationNo"].ToString();
        //                                    oLinkedBusinessDefaults.JudgementCount[iDirLinks] = rcurrbusint["JudgmentsCount"].ToString();
        //                                    oLinkedBusinessDefaults.AdverseCount[iDirLinks] = rcurrbusint["DefaultCount"].ToString();
        //                                    //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation" + iDirLinks.ToString());

                                          
        //                                    iDirLinks++;
        //                                }
        //                            }
        //                        }

        //                        olinks.LinkedCompanies[0] = oLinkedCompanies;
        //                        olinks.LinkedBusinessDefaults[0] = oLinkedBusinessDefaults;

        //                        //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation assign" );

        //                        oReportResponse.ModuleRequestResult.PrincipalLinks[i] = olinks;

        //                        //  File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation end");

        //                        i++;

        //                    }
        //                }



        //                // Principal Clearances

        //                if (dsReport.Tables.Contains("DirectorJudgments"))
        //                {
        //                    i = 0;
        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "JUDG")
        //                        {
        //                            count = count + 1;
        //                        }
        //                    }

        //                    oReportResponse.ModuleRequestResult.ConsumerJudgements = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerJudgement[count];
                            


        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "JUDG")
        //                        {
        //                            XDSPortalLibrary.Entity_Layer.MTN.ConsumerJudgement o = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerJudgement();

        //                            //o.Address1 = r[""]
        //                            o.AdminMonthlyAmount = r["DisputeAmt"].ToString();
        //                            // o.AdminNoMonths = r[""]
        //                            //o.AdminStartDate
        //                            o.Amount = r["DisputeAmt"].ToString();
        //                            o.AttorneyName = r["AttorneyName"].ToString();
        //                            o.AttorneyPhone = r["TelephoneNo"].ToString();
        //                            //o.AttorneyReference 
        //                            o.CaptureDate = r["DateLoaded"].ToString();
        //                            o.CaseNumber = r["CaseNumber"].ToString();
        //                            o.ConsumerNumber = r["DirectorID"].ToString();
        //                            o.CourtName = r["CourtName"].ToString();
        //                            //o.CourtNameCode
        //                            //o.CourtType = r[""]
        //                            //o.CourtTypeCode
        //                            //o.DateOfBirth=
        //                            //o.DebtCode
        //                            o.DebtDescription = r["CaseType"].ToString();
        //                            //o.DefendantName
        //                            //o.DefNo
        //                            o.IDNo = r["IDNo"].ToString();
        //                            //o.JudgementCode=r[""]
        //                            o.JudgementDate = r["JudgmentDate"].ToString();
        //                            o.JudgementDescription = r["CaseReason"].ToString();
        //                            o.MajorProduct = ObjSearch.ReportID.ToString();
        //                            //o.MasterNumber
        //                            //o.Message
        //                            o.PlaintiffName = r["PlaintiffName"].ToString();
        //                            //o.Remarks
        //                            //o.TradeStyle 
        //                            //o.TransType

        //                            oReportResponse.ModuleRequestResult.ConsumerJudgements[i] = o;
        //                            i++;
        //                        }

        //                    }
        //                }


        //                if (dsReport.Tables.Contains("DirectorDefaultAlert"))
        //                {
        //                    count = dsReport.Tables["DirectorDefaultAlert"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.ConsumerDefaults = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerDefault[count];
                            

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["DirectorDefaultAlert"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.ConsumerDefault o = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerDefault();

        //                        o.Amount = r["Amount"].ToString();
        //                        //o.City 
        //                        o.Remarks = r["Comments"].ToString();
        //                        //o.Country 
        //                        //o.DefaultAddress 
        //                        o.DefaultDate = r["StatusDate"].ToString();
        //                        o.Name = r["Surname"].ToString();
        //                        //o.DefaultTradeStyle
        //                        o.MajorProduct = ObjSearch.ReportID.ToString();
        //                        //o.Message 
        //                        //o.NumberFound
        //                        //o.OnBehalfOf
        //                        //o.PostCode
        //                        //o.SerialNo
        //                        o.DebtDesc = r["Statuscode"].ToString();
        //                        o.SubscriberName = r["Company"].ToString();
        //                        //o.Suburb                              
        //                        o.Account = r["AccountNo"].ToString();


        //                        oReportResponse.ModuleRequestResult.ConsumerDefaults[i] = o;

        //                        i++;
        //                    }
        //                }

        //                if (dsReport.Tables.Contains("DirectorJudgments"))
        //                {

        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
        //                        {
        //                            count = count + 1;
        //                        }
        //                    }

        //                    oReportResponse.ModuleRequestResult.ConsumerNotices = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerNotice[count];
                            

        //                    i = 0;

        //                    foreach (DataRow r in dsReport.Tables["DirectorJudgments"].Rows)
        //                    {
        //                        if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
        //                        {
        //                            XDSPortalLibrary.Entity_Layer.MTN.ConsumerNotice o = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerNotice();
        //                            //o.Address1 = r[""]
        //                            // o.AdminMonthlyAmount = r["DisputeAmt"].ToString();
        //                            // o.AdminNoMonths = r[""]
        //                            //o.AdminStartDate
        //                            o.Amount = r["DisputeAmt"].ToString();
        //                            o.AttorneyName = r["AttorneyName"].ToString();
        //                            o.AttorneyPhone = r["TelephoneNo"].ToString();
        //                            //o.AttorneyReference 
        //                            o.CaptureDate = r["DateLoaded"].ToString();
        //                            o.CaseNumber = r["CaseNumber"].ToString();
        //                            o.ConsumerNumber = r["DirectorID"].ToString();
        //                            o.CourtName = r["CourtName"].ToString();
        //                            //o.CourtNameCode
        //                            //o.CourtType = r[""]
        //                            //o.CourtTypeCode
        //                            //o.DateOfBirth=
        //                            //o.DebtCode
        //                            // o.DebtDescription = r["CaseType"].ToString();
        //                            //o.DefendantName
        //                            //o.DefNo
        //                            o.IDNo = r["IDNo"].ToString();
        //                            //o.JudgementCode=r[""]
        //                            //o.JudgementDate = r["JudgmentDate"].ToString();
        //                            //o.JudgementDescription = r["CaseReason"].ToString();
        //                            o.MajorProduct = ObjSearch.ReportID.ToString();
        //                            //o.MasterNumber
        //                            //o.Message
        //                            o.PlaintiffName = r["PlaintiffName"].ToString();
        //                            //o.Remarks
        //                            //o.TradeStyle 
        //                            //o.TransType

        //                            oReportResponse.ModuleRequestResult.ConsumerNotices[i] = o;

        //                            i++;
        //                        }

        //                    }
        //                }

        //                // Consumer NotarialBonds

        //                if (dsReport.Tables.Contains("DirectorDetail"))
        //                {
        //                    count = dsReport.Tables["DirectorDetail"].Rows.Count;
        //                    oReportResponse.ModuleRequestResult.ConsumerInfoNO04 = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerInfoNO04[count];

        //                    i = 0;

        //                    string HomeTelephoneno = string.Empty, WorkTelephoneno = string.Empty, cellularno = string.Empty;

        //                    foreach (DataRow r in dsReport.Tables["DirectorDetail"].Rows)
        //                    {

        //                        XDSPortalLibrary.Entity_Layer.MTN.ConsumerInfoNO04 o = new XDSPortalLibrary.Entity_Layer.MTN.ConsumerInfoNO04();

        //                        o.ConsumerNo = r["DirectorID"].ToString();
        //                        o.DateOfBirth = r["BirthDate"].ToString();
        //                        o.DeceasedDate = r["Deceaseddate"].ToString();
        //                        o.Forename1 = r["FirstName"].ToString();
        //                        o.Forename2 = r["SecondName"].ToString();
        //                        o.Gender = r["Gender"].ToString();
        //                        o.IdentityNo1 = r["IDNo"].ToString();
        //                        o.IdentityNo2 = r["PassportNo"].ToString();
        //                        o.MaritalStatusDesc = r["MaritalStatusDesc"].ToString();
        //                        o.NameInfoDate = r["LastUpdatedDate"].ToString();
        //                        o.Part = string.Empty;
        //                        o.PartSeq = string.Empty;
        //                        o.RecordSeq = "1";
        //                        o.SpouseName1 = r["SpouseFirstName"].ToString();
        //                        o.SpouseName2 = r["SpouseSurName"].ToString();
        //                        o.Surname = r["Surname"].ToString();
        //                        HomeTelephoneno = r["HomeTelephoneNo"].ToString();
        //                        WorkTelephoneno = r["WorkTelephoneNo"].ToString();
        //                        cellularno = r["CellularNo"].ToString();
        //                        o.TelephoneNumbers = (string.IsNullOrEmpty(HomeTelephoneno) ? string.Empty : "H" + HomeTelephoneno + " ") +
        //                            (string.IsNullOrEmpty(WorkTelephoneno) ? string.Empty : "B" + WorkTelephoneno + " ") +
        //                            (string.IsNullOrEmpty(cellularno) ? string.Empty : "C" + cellularno);
        //                        o.Title = r["TitleDesc"].ToString();


        //                        oReportResponse.ModuleRequestResult.ConsumerInfoNO04[i] = o;

        //                        i++;


        //                    }
        //                }

        //                if (dsReport.Tables.Contains("BusinessEnquiryHistory"))
        //                {
        //                    count = ds.Tables["BusinessEnquiryHistory"].Rows.Count;

        //                    oReportResponse.ModuleRequestResult.EnquiryHistory = new XDSPortalLibrary.Entity_Layer.MTN.EnquiryHistory[count];

        //                    i = 0;

        //                    foreach (DataRow dr in ds.Tables["BusinessEnquiryHistory"].Rows)
        //                    {
        //                        XDSPortalLibrary.Entity_Layer.MTN.EnquiryHistory o = new XDSPortalLibrary.Entity_Layer.MTN.EnquiryHistory();
        //                        o.MajorProduct = strReportID.ToString();
        //                        o.DateOfEnquiry = dr["EnquiryDate"].ToString();
        //                        o.ContactPhone = dr["SubscriberContact"].ToString();
        //                        o.Subscriber = dr["SubscriberName"].ToString();

        //                        oReportResponse.ModuleRequestResult.EnquiryHistory[i] = o;

        //                        i++;
        //                    }
        //                }

        //            }

        //            threads.ForEach(t => t.Join());
        //            Pressage.ForEach(t => t.Join());

        //            if (dsReport.Tables.Contains("CommercialPrincipalInformation"))
        //            {
        //                string DirectorID = string.Empty;

        //                string tablename = "PrincipalScoring";

                        
        //                foreach (DataRow r in dsReport.Tables["CommercialPrincipalInformation"].Rows)
        //                {
        //                    DirectorID = r["DirectorID"].ToString();
        //                    tablename = tablename + DirectorID;

        //                    if (dsReport.Tables.Contains(tablename))
        //                    {
        //                        foreach (DataRow rScore in dsReport.Tables[tablename].Rows)
        //                        {

        //                            if (oReportResponse.ModuleRequestResult.Principals != null)
        //                            {

        //                                i = 0;

        //                                while (oReportResponse.ModuleRequestResult.Principals.Count() < i)                                        
        //                                {

        //                                    if (oReportResponse.ModuleRequestResult.Principals[i].EmpiricaExclusionDescription == DirectorID && rScore["DirectorID"].ToString() == DirectorID)
        //                                    {
        //                                        XDSPortalLibrary.Entity_Layer.MTN.Principal o = new XDSPortalLibrary.Entity_Layer.MTN.Principal();
        //                                        o = oReportResponse.ModuleRequestResult.Principals[i];
        //                                        o.EmpiricaExclusionDescription = rScore["Exception_Code"].ToString();
        //                                        o.EmpiricaReasonCode = new string[3];
        //                                        o.EmpiricaReasonCode[0] = rScore["ReasonCode1"].ToString();
        //                                        o.EmpiricaReasonCode[1] = rScore["ReasonCode2"].ToString();
        //                                        o.EmpiricaReasonCode[2] = rScore["ReasonCode3"].ToString();
        //                                        o.EmpiricaScore = r["FinalScore"].ToString();

        //                                        oReportResponse.ModuleRequestResult.Principals[i] = o;

        //                                        break;
        //                                    }

        //                                    i++;
        //                                }
        //                            }

        //                        }
        //                    }                     

                           
                          
        //                }
        //            }

        //            if (dtError.Rows.Count > 0)
        //            {

        //                File.AppendAllText(@"C:\Log\mtnvet.txt", dtError.Rows[0]["ErrorDescription"].ToString());
        //                throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
        //            }

        //            string xml = dsReport.GetXml();
        //            xml = xml.Replace("_x0036_", "");
        //            ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
        //            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
        //            ObjResponse.ResponseData = xml;

        //            dtReportInformation.Clear();
        //            dtError.Clear();
        //            ds.Clear();
        //            dsReport.Clear();

        //            dtReportInformation.Dispose();
        //            dtError.Dispose();
        //            ds.Dispose();
        //            dsReport.Dispose();
        //            if (dsTracPlusDataSegment != null)
        //            {
        //                dsTracPlusDataSegment.Dispose();
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //            ObjResponse.ResponseData = e.Message;


        //            oReportResponse.ModuleRequestResult.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTN.ResponseStatus.Failure;
        //            oReportResponse.ModuleRequestResult.ErrorMessage = "Error While Processing your request, Please Contact XDS";
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        ObjResponse.ResponseData = ex.Message;
        //    }

        //    return ObjResponse;

        //}

        public XDSPortalLibrary.Entity_Layer.Response GetBusinessEnquiryReportCustomVettingR(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch, XDSPortalLibrary.Entity_Layer.MTNSOA.MTNCommercialFinalOutput oinputResponse, out XDSPortalLibrary.Entity_Layer.MTNSOA.MTNCommercialFinalOutput oReportResponse)
        {
            oReportResponse = oinputResponse;
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "B";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Commercial");

                DataSet dsReport = new DataSet("Commercial");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = ObjSearch.ReportID.ToString();
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
                drReportInformation["ReportID"] = ObjSearch.ReportID.ToString();
                drReportInformation["ReportName"] = ObjSearch.ReportName.ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                Thread thDirectorCurrentBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thDirectorPreviousBusinessInterests = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialBusinessInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialPrincipalInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCommercialScore = new Thread(new ParameterizedThreadStart(ProcessReport));


                thDirectorCurrentBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorCurrentBusinessInterests] {0} , {1}", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thDirectorPreviousBusinessInterests.Start(string.Format("execute [dbo].[spCLR_DirectorPreviousBusinessInterests] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialInformation.Start(string.Format("execute [dbo].[spCLR_CommercialInformation] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString(), String.IsNullOrEmpty(ObjSearch.TmpReference) ? "0" : ObjSearch.TmpReference.ToString()));
                thCommercialBusinessInformation.Start(string.Format("execute [dbo].[spCLR_CommercialBusinessInformation] {0} , '{1}','{2}', {3}", ObjSearch.CommercialID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialPrincipalInformation.Start(string.Format("execute [dbo].[spCLR_CommercialPrincipalInformation] {0}, {1} ", ObjSearch.CommercialID.ToString(), ObjSearch.ProductID.ToString()));
                thCommercialScore.Start(string.Format("execute [dbo].[spCommercialScoring] {0}, {1}, {2} ", ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString()));


               // File.AppendAllText(@"C:\Log\mtnvet.txt", "1");
                thDirectorCurrentBusinessInterests.Join();
               // File.AppendAllText(@"C:\Log\mtnvet.txt", "2");
                thDirectorPreviousBusinessInterests.Join();
               // File.AppendAllText(@"C:\Log\mtnvet.txt", "3");
                thCommercialInformation.Join();
               // File.AppendAllText(@"C:\Log\mtnvet.txt", "4");
                thCommercialBusinessInformation.Join();
               // File.AppendAllText(@"C:\Log\mtnvet.txt", "5");
                thCommercialPrincipalInformation.Join();
               // File.AppendAllText(@"C:\Log\mtnvet.txt", "6");
                thCommercialScore.Join();
               // File.AppendAllText(@"C:\Log\mtnvet.txt", "7");
                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }



                dsReport = ds;

               // File.AppendAllText(@"C:\Log\mtnvet.txt", dsReport.GetXml());


                try
                {
                    string COM003 = string.Empty;
                    double totalPropertySize = 0;
                    double com026 = 0;
                    int com024 = 0;

                    List<Thread> threads = new List<Thread>();
                    List<Thread> Pressage = new List<Thread>();

                    if (dsReport.Tables.Contains("CommercialActivePrincipalInformation"))
                    {
                        foreach (DataRow r in dsReport.Tables["CommercialActivePrincipalInformation"].Rows)
                        {
                            Thread thread = new Thread(ProcessReport)
                            {
                                Name = string.Format("{0}", r["DirectorID"].ToString())
                            };
                            thread.Start(string.Format("execute [dbo].[spCLR_MTN_ProcessDirectorVariables] {0}, {1}, {2},{3},{4},{5},{6},'{7}','{8}','{9}' ", r["ConsumerID"].ToString(), ObjSearch.subscriberID.ToString(), r["DirectorID"].ToString(), ObjSearch.SubscriberEnquiryID.ToString(), ObjSearch.SubscriberEnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.Username.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.AssociationCode.ToString()));
                            threads.Add(thread);


                            Thread Th = new Thread(ProcessReport)
                            {
                                Name = string.Format("Pressage {0}", r["DirectorID"].ToString())
                            };
                            Th.Start(string.Format("execute [dbo].[spclr_MTN_PrincipalPressage] {0}, {1}, {2},{3},{4},{5},{6},{7},'{8}','{9}','{10}',{11} ", ObjSearch.CommercialID.ToString(), r["ConsumerID"].ToString(), r["DirectorID"].ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberEnquiryID.ToString(), ObjSearch.SubscriberEnquiryResultID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.Username.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.AssociationCode.ToString(), "1"));
                            Pressage.Add(Th);
                        }
                    }
                    int count = 0, i = 0; string FinancialYearEnd = string.Empty;

                    if (dsReport.Tables.Count > 0)
                    {

                       // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialBusinessInformation");

                        if (dsReport.Tables.Contains("CommercialBusinessInformation"))
                        {
                            //File.AppendAllText(@"C:\Log\mtnvet.txt", "step1");

                            oReportResponse.BureauData.registrationDetailsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.RegistrationDetails();
                            //File.AppendAllText(@"C:\Log\mtnvet.txt", "step2");
                            oReportResponse.BureauData.headerField = new XDSPortalLibrary.Entity_Layer.MTNSOA.Header();
                            //File.AppendAllText(@"C:\Log\mtnvet.txt", "step3");
                            oReportResponse.BureauData.namesField = new XDSPortalLibrary.Entity_Layer.MTNSOA.Names();
                            //File.AppendAllText(@"C:\Log\mtnvet.txt", "step4");
                            foreach (DataRow r in dsReport.Tables["CommercialBusinessInformation"].Rows)
                            {
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step5");
                                XDSPortalLibrary.Entity_Layer.MTNSOA.RegistrationDetails o = new XDSPortalLibrary.Entity_Layer.MTNSOA.RegistrationDetails();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step6");

                                COM003 = r["CommercialStatusCode"].ToString();

                                o.majorProductField = ObjSearch.ReportID.ToString();
                                o.reg_Info_DateField = DateTime.Now.ToString("yyyyMMdd");
                                o.reg_Status_ReasonField = string.Empty;

                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step7");
                                o.registrationDateField = r["RegistrationDate"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step8");
                                // o.RegistrationStatusCode = r[""].ToString();
                                o.registrationStatusDescField = r["CommercialStatus"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step9");
                                //o.LiquidationDate
                                o.registrationNoField = r["RegistrationNo12"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step10");
                                // o.CompanyTypeCode = 
                                o.companyTypeDescField = r["CommercialType"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step11");
                                //o.RegistrationCountry
                                o.previousRegistrationNoField = new string[1];
                                o.previousRegistrationNoField[0] = r["RegistrationNoConverted"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step12");
                                //o.Auditors
                                o.registeredAddressField = r["PhysicalAddress"].ToString();
                                o.registrationStatusCodeField = r["CommercialStatusCode"].ToString();
                                string postcode = string.Empty;
                                int code = 0;
                                if (!string.IsNullOrEmpty(r["PhysicalAddress"].ToString()))
                                {
                                     postcode = r["PhysicalAddress"].ToString().Substring(r["PhysicalAddress"].ToString().LastIndexOf(",")+1);

                                    
                                }

                                if (int.TryParse(postcode, out code))
                                { }
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step13");
                                //o.Suburb
                                //o.City
                                //o.Country
                                o.postCodeField = code.ToString();
                                if (!string.IsNullOrEmpty(r["PostalAddress"].ToString()))
                                {
                                    postcode = r["PostalAddress"].ToString().Substring(r["PostalAddress"].ToString().LastIndexOf(",")+1);
//                                    File.AppendAllText(@"C:\Log\mtnvet.txt", postcode);
                                }

                                if (int.TryParse(postcode, out code))
                                { }
                                o.authorisedCapitalField = r["AuthorisedCapitalAmt"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step14");
                                o.issuedCapitalField = r["IssuedCapitalAmt"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step15");
                                //o.StatedCapital
                                //o.Reg_Info_Date
                                o.postalAddressField = new string[1];
                                o.postalAddressField[0] = r["PostalAddress"].ToString();
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step16");
                                o.postalPostCodeField = code.ToString();
                                o.financialYearEndField = r["FinancialYearEnd"].ToString();

                                FinancialYearEnd = o.financialYearEndField;

                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step8");
                                oReportResponse.BureauData.registrationDetailsField = o;
                                //File.AppendAllText(@"C:\Log\mtnvet.txt", "step9");
                                XDSPortalLibrary.Entity_Layer.MTNSOA.Header oHeader = new XDSPortalLibrary.Entity_Layer.MTNSOA.Header();
                                oHeader.majorProductField = ObjSearch.ReportID.ToString();
                                oHeader.dateOfHeaderField = DateTime.Now.ToString("yyyyMMdd");
                                oHeader.startDateField = r["BusinessStartDate"].ToString();
                                oHeader.businessTypeField = r["CommercialType"].ToString();
                                //oHeader.DunsNumber
                                oHeader.startingCapitalField = r["IssuedCapitalAmt"].ToString().Substring(0, r["IssuedCapitalAmt"].ToString().Length > 10 ? 10 : r["IssuedCapitalAmt"].ToString().Length);
                                //oHeader.PurchasePrice
                                oHeader.industryField = r["SIC"].ToString();
                                //oHeader.BusinessFunction
                                // oHeader.BusinessCategory = r
                                oHeader.physicalAddressField = r["PhysicalAddress"].ToString();
                                //oHeader.Suburb
                                //oHeader.City
                                //oHeader.Country
                                //oHeader.PostCode
                                oHeader.postalAddressField = r["PostalAddress"].ToString();
                                //oHeader.PostalSuburb 
                                //oHeader.PostalCity
                                //oHeader.PostalCountry
                                //oHeader.PostalPostCode
                                oHeader.phoneField = r["TelephoneNo"].ToString();
                                oHeader.faxField = r["FaxNo"].ToString();
                                //oHeader.VATNumbers = 
                                oHeader.websiteField = r["BussWebsite"].ToString();
                                oHeader.emailField = r["BussEmail"].ToString();
                                oHeader.taxNumberField = r["TaxNo"].ToString();

                                oReportResponse.BureauData.headerField = oHeader;



                                XDSPortalLibrary.Entity_Layer.MTNSOA.Names oNames = new XDSPortalLibrary.Entity_Layer.MTNSOA.Names();

                                oNames.previousNamesField = new string[1];
                                oNames.previousNamesField[0] = r["PreviousBussName"].ToString();
                                oNames.previousNameDatesField = new string[1]; ;

                                oNames.previousNameDatesField[0] = r["NameChangeDate"].ToString();
                                oNames.aKANameField = r["TradeName"].ToString();
                                oNames.businessNameField = r["CommercialName"].ToString();

                                oReportResponse.BureauData.namesField = oNames;

                            }
                        }

                       // File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialBankCodeHistory");

                        if (dsReport.Tables.Contains("CommercialBankCodeHistory"))
                        {
                            count = dsReport.Tables["CommercialBankCodeHistory"].Rows.Count;
                            oReportResponse.BureauData.bankCodesField = new XDSPortalLibrary.Entity_Layer.MTNSOA.BankCodes[count];


                            oReportResponse.BureauData.bankHistoryField = new XDSPortalLibrary.Entity_Layer.MTNSOA.BankHistory[count];

                            oReportResponse.BureauData.generalBankingInfoField = new XDSPortalLibrary.Entity_Layer.MTNSOA.GeneralBankingInfo[1];


                            i = 0;
                            foreach (DataRow r in dsReport.Tables["CommercialBankCodeHistory"].Rows)
                            {

                                DateTime dtrequesteddate = DateTime.Now;
                                DateTime requesteddate = DateTime.Now;
                                string bankCode = string.Empty;

                                bankCode = r["BankCode"].ToString();



                                if (DateTime.TryParseExact(r["DateRequested"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtrequesteddate))
                                {
                                    if (i == 0)
                                    {
                                        requesteddate = dtrequesteddate;
                                    }

                                    if (dtrequesteddate >= requesteddate && !string.IsNullOrEmpty(bankCode))
                                    {
                                        XDSPortalLibrary.Entity_Layer.MTNSOA.BankCodes o = new XDSPortalLibrary.Entity_Layer.MTNSOA.BankCodes();

                                        o.majorProductField = ObjSearch.ReportID.ToString();
                                        o.accountNoField = r["AccountNumber"].ToString();
                                        o.bankNameField = r["Bank"].ToString();
                                        o.branchField = r["BranchName"].ToString();
                                        o.bankCodeField = r["BankCode"].ToString();
                                        o.bankCodeDescField = r["Bank_Comments"].ToString();
                                        if (!string.IsNullOrEmpty(r["Comment"].ToString()))
                                        {
                                            o.commentField = new string[1];
                                            o.commentField[0] = r["Comment"].ToString();
                                        }
                                        o.amountField = r["EnquiryAmount"].ToString();
                                        o.startDateField = r["DateOpened"].ToString();
                                        o.termsField = r["terms"].ToString();
                                        o.dateField = r["DateRequested"].ToString();


                                        oReportResponse.BureauData.bankCodesField[i] = o;
                                        requesteddate = dtrequesteddate;


                                        XDSPortalLibrary.Entity_Layer.MTNSOA.GeneralBankingInfo oBankInfo = new XDSPortalLibrary.Entity_Layer.MTNSOA.GeneralBankingInfo();
                                        oBankInfo.majorProductField = ObjSearch.ReportID.ToString();
                                        oBankInfo.accountNoField = r["AccountNumber"].ToString();
                                        oBankInfo.bankNameField = r["Bank"].ToString();
                                        oBankInfo.branchField = r["BranchName"].ToString();
                                        if (!string.IsNullOrEmpty(r["Comment"].ToString()))
                                        {
                                            oBankInfo.commentField = new string[1];
                                            oBankInfo.commentField[0] = r["Comment"].ToString();
                                        }
                                        oBankInfo.startDateField = r["DateOpened"].ToString();
                                        oBankInfo.infoDateField = r["DateRequested"].ToString();

                                        oReportResponse.BureauData.generalBankingInfoField[i] = oBankInfo;
                                    }


                                }



                                XDSPortalLibrary.Entity_Layer.MTNSOA.BankHistory oHis = new XDSPortalLibrary.Entity_Layer.MTNSOA.BankHistory();

                                oHis.majorProductField = ObjSearch.ReportID.ToString();
                                oHis.accountNoField = r["AccountNumber"].ToString();
                                oHis.bankNameField = r["Bank"].ToString();
                                oHis.branchField = r["BranchName"].ToString();
                                oHis.bankCodeField = r["BankCode"].ToString();
                                oHis.bankCodeDescField = r["Bank_Comments"].ToString();
                                if (!string.IsNullOrEmpty(r["Comment"].ToString()))
                                {
                                    oHis.commentField = new string[1];
                                    oHis.commentField[0] = r["Comment"].ToString();
                                }
                                oHis.amountField = r["EnquiryAmount"].ToString();
                                oHis.startDateField = r["DateOpened"].ToString();
                                oHis.termsField = r["terms"].ToString();
                                oHis.dateField = r["DateRequested"].ToString();

                                oReportResponse.BureauData.bankHistoryField[i] = oHis;

                                i++;

                            }
                        }

                        //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialTradeReferencesInformation");

                        if (dsReport.Tables.Contains("CommercialTradeReferencesInformation"))
                        {
                            count = dsReport.Tables["CommercialTradeReferencesInformation"].Rows.Count;

                            oReportResponse.BureauData.tradeHistoryField = new XDSPortalLibrary.Entity_Layer.MTNSOA.TradeHistory[count];

                            i = 0;

                            foreach (DataRow r in dsReport.Tables["CommercialTradeReferencesInformation"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.MTNSOA.TradeHistory o = new XDSPortalLibrary.Entity_Layer.MTNSOA.TradeHistory();

                                o.majorProductField = ObjSearch.ReportID.ToString();
                                o.dateOfRefField = r["Createdondate"].ToString();
                                // o.YearsKnown = 
                                //o.MonthsKnown

                               
                                o.creditLimitField = r["CreditLimit"].ToString();                                    
                                o.unlimitedField = "N";// r["MaxLimit"].ToString();
                                o.purchasesField = r["AveragePurchases"].ToString();
                                o.termsTakenField = r["TermsTaken"].ToString();
                                o.termsGivenField = r["Terms"].ToString();
                                // o.Discount = 
                                // o.ReferenceName = 
                                //o.Insurance
                                //o.InsuranceDesc
                                o.commentField = r["Comment"].ToString();
                                o.assuredValueField = r["SuretyValue"].ToString();
                                // o.Obtained = r[""]

                                oReportResponse.BureauData.tradeHistoryField[i] = o;

                                i++;
                            }
                        }

                        //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPropertyInformation");

                        if (dsReport.Tables.Contains("CommercialPropertyInformation"))
                        {
                            int noofProperties = dsReport.Tables["CommercialPropertyInformation"].Rows.Count;
                            oReportResponse.BureauData.businessDeedsSummaryDAField = new XDSPortalLibrary.Entity_Layer.MTNSOA.BusinessDeedsSummaryDA[1];

                            double totalValue = 0;
                            double totalBond = 0;


                            string OldestPropertyDate = string.Empty, OldestPropertyValue = string.Empty;
                            DateTime purchasedate = DateTime.Now;

                            List<string> Deedsoffice = new List<string>();

                            foreach (DataRow r in dsReport.Tables["CommercialPropertyInformation"].Rows)
                            {
                                if (!Deedsoffice.Contains(r["DeedsOffice"]))
                                {
                                    Deedsoffice.Add(r["DeedsOffice"].ToString());
                                }

                                totalValue = totalValue + double.Parse(r["PurchasePriceAmt"].ToString());
                                totalBond = totalBond + double.Parse(r["BondAmt"].ToString());

                                string resultString = Regex.Match(r["Extent"].ToString(), @"\d+").Value;



                                if (double.TryParse(resultString, out totalPropertySize))
                                {
                                    com026 = com026 + totalPropertySize;
                                }

                                DateTime dtpurchasedate = DateTime.Now;

                                if (DateTime.TryParseExact(r["PurchaseDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtpurchasedate))
                                {
                                    if (i == 0)
                                    {
                                        purchasedate = dtpurchasedate;
                                    }
                                    if (purchasedate >= dtpurchasedate)
                                    {
                                        OldestPropertyDate = r["PurchaseDate"].ToString();
                                        OldestPropertyValue = r["PurchasePriceAmt"].ToString();

                                        purchasedate = dtpurchasedate;

                                    }
                                    else
                                    {
                                        com024 = int.Parse(Math.Round((DateTime.Now.Subtract(dtpurchasedate).Days / (365.25 / 12)),0).ToString());
                                    }

                                }

                            }

                            XDSPortalLibrary.Entity_Layer.MTNSOA.BusinessDeedsSummaryDA o = new XDSPortalLibrary.Entity_Layer.MTNSOA.BusinessDeedsSummaryDA();
                            o.commentField = string.Empty;
                            o.deedsOfficeField = String.Join(",", Deedsoffice).ToString();
                            o.majorProductField = ObjSearch.ReportID.ToString();
                            o.numberPropertiesField = noofProperties.ToString();
                            o.totalBondField = totalBond.ToString();
                            o.totalOwnedField = noofProperties.ToString();
                            o.totalValueField = totalValue.ToString();

                            oReportResponse.BureauData.businessDeedsSummaryDAField[0] = o;
                        }

                        //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialJudgment");

                        int countcourtrecord = 0;

                        if (dsReport.Tables.Contains("CommercialJudgment"))
                        {
                            countcourtrecord = dsReport.Tables["CommercialJudgment"].Rows.Count;
                        }

                        if (dsReport.Tables.Contains("ActiveDirectorJudgments"))
                        {

                            foreach (DataRow r in dsReport.Tables["ActiveDirectorJudgments"].Rows)
                            {
                               // if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
                                {
                                    countcourtrecord = countcourtrecord + 1;
                                }
                            }
                        }

                        if (countcourtrecord > 0)
                        {
                            count = countcourtrecord;

                            oReportResponse.BureauData.courtRecordsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord[count];
                            i = 0;

                            if (dsReport.Tables.Contains("CommercialJudgment"))
                            {
                                foreach (DataRow r in dsReport.Tables["CommercialJudgment"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord o = new XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord();
                                    o.actionDateField = r["CaseFilingDate"].ToString();
                                    o.attorneyField = r["AttorneyName"].ToString();
                                    //o.AbandonDate
                                    o.caseNumberField = r["CaseNumber"].ToString();
                                    //o.City 
                                    o.claimAmountField = r["DisputeAmt"].ToString();
                                    //o.Comment 
                                    //o.Country
                                    //o.CourtDistrict = r[]
                                    //o.CourtRecordAddress =
                                    //  o.CourtType = 
                                    //o.DefendantDistrict
                                    //o.DefendantName
                                    //o.DefendantTradeStyle
                                    o.majorProductField = ObjSearch.ReportID.ToString();
                                    //o.Message
                                    //o.NatureOfDebt
                                    //o.NumberFound
                                    //o.PlaintiffName = r[""]
                                    //o.PostCode
                                    //o.ReturnDate
                                    //o.SerialNumber
                                    //o.Suburb
                                    //o.TypeCode
                                    o.typeDescField = r["CaseType"].ToString();

                                    oReportResponse.BureauData.courtRecordsField[i] = o;

                                    i++;

                                }
                            }

                            if (dsReport.Tables.Contains("ActiveDirectorJudgments"))
                            {
                                foreach (DataRow r in dsReport.Tables["ActiveDirectorJudgments"].Rows)
                                {
                                   // if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
                                    {
                                        XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord o = new XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord();


                                        o.actionDateField = r["JudgmentDate"].ToString();
                                        o.attorneyField = r["AttorneyName"].ToString();
                                        //o.AbandonDate
                                        o.caseNumberField = r["CaseNumber"].ToString();
                                        //o.City 
                                        o.claimAmountField = r["DisputeAmt"].ToString();
                                        //o.Comment 
                                        //o.Country
                                        //o.CourtDistrict = r[]
                                        //o.CourtRecordAddress =
                                        //  o.CourtType = 
                                        //o.DefendantDistrict
                                        //o.DefendantName
                                        //o.DefendantTradeStyle
                                        o.majorProductField = ObjSearch.ReportID.ToString();
                                        //o.Message
                                        //o.NatureOfDebt
                                        //o.NumberFound
                                        //o.PlaintiffName = r[""]
                                        //o.PostCode
                                        //o.ReturnDate
                                        //o.SerialNumber
                                        //o.Suburb
                                        //o.TypeCode
                                        o.typeDescField = r["CaseType"].ToString();

                                        oReportResponse.BureauData.courtRecordsField[i] = o;

                                        i++;
                                    }
                                }

                            }
                        }
                        else
                        {
                            i=0;
                            oReportResponse.BureauData.courtRecordsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord[1];
                            XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord o = new XDSPortalLibrary.Entity_Layer.MTNSOA.CourtRecord();


                            o.actionDateField = string.Empty;
                            o.attorneyField = string.Empty;
                            //o.AbandonDate
                            o.caseNumberField =string.Empty;
                            //o.City 
                            o.claimAmountField = string.Empty;
                            //o.Comment 
                            //o.Country
                            //o.CourtDistrict = r[]
                            //o.CourtRecordAddress =
                            //  o.CourtType = 
                            //o.DefendantDistrict
                            //o.DefendantName
                            //o.DefendantTradeStyle
                            o.majorProductField = ObjSearch.ReportID.ToString();
                            //o.Message
                            //o.NatureOfDebt
                            //o.NumberFound
                            //o.PlaintiffName = r[""]
                            //o.PostCode
                            //o.ReturnDate
                            //o.SerialNumber
                            //o.Suburb
                            //o.TypeCode
                            o.typeDescField = string.Empty;

                            oReportResponse.BureauData.courtRecordsField[i] = o;
                        }
                        //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialDefaultAlert");

                        if (dsReport.Tables.Contains("CommercialDefaultAlert"))
                        {
                            count = dsReport.Tables["CommercialDefaultAlert"].Rows.Count;

                            oReportResponse.BureauData.defaultsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.Default[count];

                            i = 0;

                            foreach (DataRow r in dsReport.Tables["CommercialDefaultAlert"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.MTNSOA.Default o = new XDSPortalLibrary.Entity_Layer.MTNSOA.Default();

                                string amount = r["Amount"].ToString();
                                decimal damount = 0;
                                

                                if (!string.IsNullOrEmpty(amount))
                                {
                                    if (decimal.TryParse(amount,out damount))
                                    {
                                       o.amountField= Math.Round(damount,0).ToString();
                                    }
                                    else
                                    {
                                        o.amountField = "0";
                                    }
                                }
                                else
                                    {
                                        o.amountField = "0";
                                    }

                               // o.amountField  = Math.Round(,0);
                                //o.City
                                o.commentField = r["Comments"].ToString();
                                //o.Country 
                                //o.DefaultAddress 
                                o.defaultDateField = r["StatusDate"].ToString();
                                o.defaultNameField = r["CommercialName"].ToString();
                                //o.DefaultTradeStyle
                                o.majorProductField = ObjSearch.ReportID.ToString();
                                //o.Message 
                                o.numberFoundField = "1";
                                //o.OnBehalfOf 
                                //o.PostCode
                                //o.SerialNo
                                o.statusField = r["Statuscode"].ToString();
                                o.subscriberNameField = r["Company"].ToString();
                                //o.Suburb 
                                o.supplierNameField = r["Company"].ToString();

                                oReportResponse.BureauData.defaultsField[i] = o;

                                i++;

                            }
                        }

                        if (dsReport.Tables.Contains("CommercialFinancialEstimates"))
                        {
                            count = dsReport.Tables["CommercialFinancialEstimates"].Rows.Count;

                            oReportResponse.BureauData.financeHeaderField = new XDSPortalLibrary.Entity_Layer.MTNSOA.FinanceHeader();


                            i = 0;

                            foreach (DataRow r in dsReport.Tables["CommercialFinancialEstimates"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.MTNSOA.FinanceHeader o = new XDSPortalLibrary.Entity_Layer.MTNSOA.FinanceHeader();

                                o.majorProductField = strReportID;
                                o.dateField = r["Createdondate"].ToString();
                                o.sourceField = r["CommercialName"].ToString();
                                o.turnOverAmount1Field = r["CurrentTurnOverPerAnnum"].ToString();
                                o.turnOverAmount2Field = r["PreviousYearTurnover"].ToString();
                                o.finYearMonthField = FinancialYearEnd;

                                oReportResponse.BureauData.financeHeaderField = o;


                                XDSPortalLibrary.Entity_Layer.MTNSOA.CurrentAsset oAsset = new XDSPortalLibrary.Entity_Layer.MTNSOA.CurrentAsset();
                                oAsset.majorProductField = strReportID;
                                oAsset.currentAssTotalField = r["CurrentAssets"].ToString();

                                oReportResponse.BureauData.currentAssetField = oAsset;

                                XDSPortalLibrary.Entity_Layer.MTNSOA.CurrentLiabilities oLiability = new XDSPortalLibrary.Entity_Layer.MTNSOA.CurrentLiabilities();
                                oLiability.majorProductField = strReportID;
                                oLiability.currentLiabilityTotalField = r["CurrentLiability"].ToString();

                                oReportResponse.BureauData.currentLiabilitiesField = oLiability;

                                i++;

                            }
                        }

                        if (dsReport.Tables.Contains("CommercialOperationAssessment"))
                        {
                            count = dsReport.Tables["CommercialOperationAssessment"].Rows.Count;

                            oReportResponse.BureauData.operationsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.Operation[count];



                            i = 0;

                            foreach (DataRow r in dsReport.Tables["CommercialOperationAssessment"].Rows)
                            {
                                string importcompany = string.Empty, exportCompany = string.Empty;

                                XDSPortalLibrary.Entity_Layer.MTNSOA.Operation o = new XDSPortalLibrary.Entity_Layer.MTNSOA.Operation();

                                o.majorProductField = strReportID;
                                o.dateField = r["Createdondate"].ToString();
                                o.sicCodeField = r["SICCID"].ToString();
                                o.sicDescriptionField = r["DefinitionOfCode"].ToString();
                                o.importCommentField = r["ImportCountryID"].ToString();
                                if (!string.IsNullOrEmpty(o.importCommentField))
                                {
                                    o.importField = "Y";
                                }
                                else
                                {
                                    o.importField = "N";
                                }

                                o.exportCommentField = r["ExportCountryID"].ToString();
                                if (!string.IsNullOrEmpty(o.exportCommentField))
                                {
                                    o.exportField = "Y";
                                }
                                else
                                {
                                    o.exportField = "N";
                                }

                                oReportResponse.BureauData.operationsField[i] = o;

                                i++;

                            }
                        }

                        if (dsReport.Tables.Contains("CommercialNumberOfEmployee"))
                        {
                            count = dsReport.Tables["CommercialNumberOfEmployee"].Rows.Count;

                            if (oReportResponse.BureauData.operationsField == null)
                            {
                                oReportResponse.BureauData.operationsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.Operation[count];
                            }




                            i = 0;

                            foreach (DataRow r in dsReport.Tables["CommercialNumberOfEmployee"].Rows)
                            {
                                string importcompany = string.Empty, exportCompany = string.Empty;

                                XDSPortalLibrary.Entity_Layer.MTNSOA.Operation o = new XDSPortalLibrary.Entity_Layer.MTNSOA.Operation();


                                o.numberSalesStaffField = FnSubstring((int.Parse(r["Salaried"].ToString()) + int.Parse(r["Contracted"].ToString())).ToString(), 0, 6);
                                o.numberWageStaffField = FnSubstring((int.Parse(r["Casual"].ToString()) + int.Parse(r["Waged"].ToString())).ToString(), 0, 6);
                                o.totalNumberStaffField = FnSubstring((int.Parse(r["Salaried"].ToString()) + int.Parse(r["Contracted"].ToString()) + int.Parse(r["Casual"].ToString()) + int.Parse(r["Waged"].ToString())).ToString(), 0, 6);



                                oReportResponse.BureauData.operationsField[i] = o;

                                i++;

                            }
                        }

                        //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation");

                        //Notarial Bond

                        if (dsReport.Tables.Contains("CommercialActivePrincipalInformation"))
                        {
                            count = dsReport.Tables["CommercialActivePrincipalInformation"].Rows.Count;

                            oReportResponse.BureauData.principalsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.Principal1[count];


                            //oReportResponse.BureauData.principalLinksField = new XDSPortalLibrary.Entity_Layer.MTNSOA.PrincipalLink[count];

                            oReportResponse.BureauData.principalLinksField = new XDSPortalLibrary.Entity_Layer.MTNSOA.PrincipalLinks();
                            oReportResponse.BureauData.principalLinksField.PrincipalLink = new XDSPortalLibrary.Entity_Layer.MTNSOA.PrincipalLink[count];


                            i = 0;

                            foreach (DataRow r in dsReport.Tables["CommercialActivePrincipalInformation"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.MTNSOA.Principal1 o = new XDSPortalLibrary.Entity_Layer.MTNSOA.Principal1();

                                //o.AssetNotarialBond
                                //o.CivilCourtRecord
                                //o.Comment
                                //o.ConfirmedByRegistrar
                                //o.DateDisputed
                                o.dateOfBirthField = r["BirthDate"].ToString();
                                //o.DateStarted = 
                                //o.DebtCouncilDate
                                //o.DebtCouncilDesc
                                //o.Deeds
                                //o.DefaultData
                                //o.EmpiricaExclusionCode
                                o.empiricaExclusionDescriptionField = r["DirectorID"].ToString();
                                //o.EmpiricaReasonCode
                                //o.EmpiricaReasonDescription
                                //o.EmpiricaScore
                                o.forename1Field = r["FirstName"].ToString();
                                o.forename2Field = r["SecondName"].ToString();
                                // o.Forename3 = r[""]
                                o.surnameField = r["Surname"].ToString();
                                o.iDNumberField = r["IDNo"].ToString();
                                //o.InfoDate 
                                o.majorProductField = ObjSearch.ReportID.ToString();
                                //o.NoticesOrNotarialBonds 
                                o.positionField = r["Designation"].ToString();

                                oReportResponse.BureauData.principalsField[i] = o;


                                XDSPortalLibrary.Entity_Layer.MTNSOA.PrincipalLink olinks = new XDSPortalLibrary.Entity_Layer.MTNSOA.PrincipalLink();
                                olinks.consumerNoField = r["DirectorID"].ToString();
                                olinks.dateOfBirthField = r["BirthDate"].ToString();
                                //olinks.Established
                                olinks.forename1Field = r["FirstName"].ToString();
                                olinks.forename2Field = r["SecondName"].ToString();
                                //olinks.Forename3
                                olinks.iDNumberField = r["IDNo"].ToString();
                                //olinks.LinkedBusinessDefaults
                                //olinks.LinkedCompanies 
                                olinks.majorProductField = ObjSearch.ReportID.ToString();
                                olinks.surnameField = r["Surname"].ToString();

                                int countDirLinks = 0;

                                XDSPortalLibrary.Entity_Layer.MTNSOA.LinkedCompany oLinkedCompanies = new XDSPortalLibrary.Entity_Layer.MTNSOA.LinkedCompany();
                                XDSPortalLibrary.Entity_Layer.MTNSOA.LinkedBusinessDefault oLinkedBusinessDefaults = new XDSPortalLibrary.Entity_Layer.MTNSOA.LinkedBusinessDefault();

                                if (dsReport.Tables.Contains("DirectorCurrentBusinessinterests"))
                                {

                                    if (olinks.linkedCompaniesField == null)
                                    {
                                        olinks.linkedCompaniesField = new XDSPortalLibrary.Entity_Layer.MTNSOA.LinkedCompany[1];

                                    }
                                    if (olinks.linkedBusinessDefaultsField == null)
                                    {
                                        olinks.linkedBusinessDefaultsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.LinkedBusinessDefault[1];

                                    }

                                    foreach (DataRow rcurrbusint in dsReport.Tables["DirectorCurrentBusinessinterests"].Rows)
                                    {
                                        if (rcurrbusint["DirectorID"].ToString() == olinks.consumerNoField)
                                        {
                                            countDirLinks++;
                                        }
                                    }

                                    int iDirLinks = 0;
                                    foreach (DataRow rcurrbusint in dsReport.Tables["DirectorCurrentBusinessinterests"].Rows)
                                    {
                                        if (rcurrbusint["DirectorID"].ToString() == olinks.consumerNoField)
                                        {
                                            //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:start LinkedCompanies");


                                            if (oLinkedCompanies.businessNameField == null)
                                            {
                                                oLinkedCompanies.businessNameField = new string[countDirLinks];
                                            }

                                            if (oLinkedCompanies.businessStatusField == null)
                                            {
                                                oLinkedCompanies.businessStatusField = new string[countDirLinks];
                                            }

                                            if (oLinkedCompanies.registeredDateField == null)
                                            {
                                                oLinkedCompanies.registeredDateField = new string[countDirLinks];
                                            }

                                            if (oLinkedBusinessDefaults.iTNumberField == null)
                                            {
                                                oLinkedBusinessDefaults.iTNumberField = new string[countDirLinks];
                                            }

                                            if (oLinkedBusinessDefaults.registrationNumberField == null)
                                            {
                                                oLinkedBusinessDefaults.registrationNumberField = new string[countDirLinks];
                                            }

                                            if (oLinkedBusinessDefaults.judgementCountField == null)
                                            {
                                                oLinkedBusinessDefaults.judgementCountField = new string[countDirLinks];
                                            }

                                            if (oLinkedBusinessDefaults.adverseCountField == null)
                                            {
                                                oLinkedBusinessDefaults.adverseCountField = new string[countDirLinks];
                                            }
                                            //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:end LinkedCompanies");



                                            oLinkedCompanies.businessNameField[iDirLinks] = rcurrbusint["CommercialName"].ToString();
                                            // //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:CommercialName LinkedCompanies");
                                            oLinkedCompanies.businessStatusField[iDirLinks] = rcurrbusint["CommercialStatus"].ToString();
                                            // //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:CommercialStatus LinkedCompanies");
                                            oLinkedCompanies.registeredDateField[iDirLinks] = rcurrbusint["RegistrationDate"].ToString();
                                            // //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:RegistrationDate LinkedCompanies");



                                            //  //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:CommercialID LinkedCompanies");
                                            oLinkedBusinessDefaults.iTNumberField[iDirLinks] = rcurrbusint["CommercialID"].ToString();
                                            //  //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialActivePrincipalInformation:strReportID LinkedCompanies");

                                            //  //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation:RegistrationNo LinkedCompanies");
                                            oLinkedBusinessDefaults.registrationNumberField[iDirLinks] = rcurrbusint["RegistrationNo"].ToString();
                                            oLinkedBusinessDefaults.judgementCountField[iDirLinks] = rcurrbusint["JudgmentsCount"].ToString();
                                            oLinkedBusinessDefaults.adverseCountField[iDirLinks] = rcurrbusint["DefaultCount"].ToString();
                                            //  //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation" + iDirLinks.ToString());


                                            iDirLinks++;
                                        }
                                    }
                                }

                                olinks.linkedCompaniesField[0] = oLinkedCompanies;
                                olinks.linkedBusinessDefaultsField[0] = oLinkedBusinessDefaults;

                                //  //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation assign" );

                                oReportResponse.BureauData.principalLinksField.PrincipalLink[i] = olinks;

                                //  //File.AppendAllText(@"C:\Log\mtnvet.txt", "CommercialPrincipalInformation end");

                                i++;

                            }
                        }



                        // Principal Clearances

                        if (dsReport.Tables.Contains("ActiveDirectorJudgments"))
                        {
                            i = 0;
                            count = 0;
                            foreach (DataRow r in dsReport.Tables["ActiveDirectorJudgments"].Rows)
                            {
                                if (r["JudgementTypeCode"].ToString().ToUpper() == "JUDG")
                                {
                                    count = count + 1;
                                }
                            }

                            oReportResponse.BureauData.consumerJudgementsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerJudgement[count];



                            foreach (DataRow r in dsReport.Tables["ActiveDirectorJudgments"].Rows)
                            {
                                if (r["JudgementTypeCode"].ToString().ToUpper() == "JUDG")
                                {
                                    XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerJudgement o = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerJudgement();

                                    //o.Address1 = r[""]
                                    o.adminMonthlyAmountField = r["DisputeAmt"].ToString();
                                    // o.AdminNoMonths = r[""]
                                    //o.AdminStartDate
                                    o.amountField = r["DisputeAmt"].ToString();
                                    o.attorneyNameField = r["AttorneyName"].ToString();
                                    o.attorneyPhoneField = r["TelephoneNo"].ToString();
                                    //o.AttorneyReference 
                                    o.captureDateField = r["DateLoaded"].ToString();
                                    o.caseNumberField = r["CaseNumber"].ToString();
                                    o.consumerNumberField = r["DirectorID"].ToString();
                                    o.courtNameField = r["CourtName"].ToString();
                                    //o.CourtNameCode
                                    //o.CourtType = r[""]
                                    //o.CourtTypeCode
                                    //o.DateOfBirth=
                                    //o.DebtCode
                                    o.debtDescriptionField = r["CaseType"].ToString();
                                    //o.DefendantName
                                    //o.DefNo
                                    o.iDNoField = r["IDNo"].ToString();
                                    //o.JudgementCode=r[""]
                                    o.judgementDateField = r["JudgmentDate"].ToString();
                                    o.judgementDescriptionField = r["CaseReason"].ToString();
                                    o.majorProductField = ObjSearch.ReportID.ToString();
                                    //o.MasterNumber
                                    //o.Message
                                    o.plaintiffNameField = r["PlaintiffName"].ToString();
                                    //o.Remarks
                                    //o.TradeStyle 
                                    //o.TransType

                                    oReportResponse.BureauData.consumerJudgementsField[i] = o;
                                    i++;
                                }

                            }
                        }


                        if (dsReport.Tables.Contains("ActiveDirectorDefaultAlert"))
                        {
                            count = dsReport.Tables["ActiveDirectorDefaultAlert"].Rows.Count;

                            oReportResponse.BureauData.consumerDefaultsField = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerDefault[count];


                            i = 0;

                            foreach (DataRow r in dsReport.Tables["ActiveDirectorDefaultAlert"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerDefault o = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerDefault();

                                o.amountField = r["Amount"].ToString();
                                //o.City 
                                o.remarksField = r["Comments"].ToString();
                                //o.Country 
                                //o.DefaultAddress 
                                o.defaultDateField = r["StatusDate"].ToString();
                                o.nameField = r["Surname"].ToString();
                                //o.DefaultTradeStyle
                                o.majorProductField = ObjSearch.ReportID.ToString();
                                //o.Message 
                                //o.NumberFound
                                //o.OnBehalfOf
                                //o.PostCode
                                //o.SerialNo
                                o.debtDescField = r["Statuscode"].ToString();
                                o.subscriberNameField = r["Company"].ToString();
                                //o.Suburb                              
                                o.accountField = r["AccountNo"].ToString();


                                oReportResponse.BureauData.consumerDefaultsField[i] = o;

                                i++;
                            }
                        }

                        if (dsReport.Tables.Contains("ActiveDirectorJudgments"))
                        {
                            count = 0;
                            foreach (DataRow r in dsReport.Tables["ActiveDirectorJudgments"].Rows)
                            {
                                if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
                                {
                                    count = count + 1;
                                }
                            }

                           
                                    oReportResponse.BureauData.consumerNoticesField = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerNotice[count];

                                  

                            i = 0;

                            foreach (DataRow r in dsReport.Tables["ActiveDirectorJudgments"].Rows)
                            {
                                if (r["JudgementTypeCode"].ToString().ToUpper() == "ADM")
                                {
                                    XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerNotice o = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerNotice();
                                    //o.Address1 = r[""]
                                    // o.AdminMonthlyAmount = r["DisputeAmt"].ToString();
                                    // o.AdminNoMonths = r[""]
                                    //o.AdminStartDate
                                    o.amountField = r["DisputeAmt"].ToString();
                                    o.attorneyNameField = r["AttorneyName"].ToString();
                                    o.attorneyPhoneField = r["TelephoneNo"].ToString();
                                    //o.AttorneyReference 
                                    o.captureDateField = r["DateLoaded"].ToString();
                                    o.caseNumberField = r["CaseNumber"].ToString();
                                    o.consumerNumberField = r["DirectorID"].ToString();
                                    o.courtNameField = r["CourtName"].ToString();
                                    //o.CourtNameCode
                                    //o.CourtType = r[""]
                                    //o.CourtTypeCode
                                    //o.DateOfBirth=
                                    //o.DebtCode
                                    // o.DebtDescription = r["CaseType"].ToString();
                                    //o.DefendantName
                                    //o.DefNo
                                    o.iDNoField = r["IDNo"].ToString();
                                    //o.JudgementCode=r[""]
                                    //o.JudgementDate = r["JudgmentDate"].ToString();
                                    //o.JudgementDescription = r["CaseReason"].ToString();
                                    o.majorProductField = ObjSearch.ReportID.ToString();
                                    //o.MasterNumber
                                    //o.Message
                                    o.plaintiffNameField = r["PlaintiffName"].ToString();
                                    //o.Remarks
                                    //o.TradeStyle 
                                    //o.TransType

                                    oReportResponse.BureauData.consumerNoticesField[i] = o;

                                    i++;
                                }

                            }
                        }

                        // Consumer NotarialBonds

                        if (dsReport.Tables.Contains("ActiveDirectorDetail"))
                        {
                            count = dsReport.Tables["ActiveDirectorDetail"].Rows.Count;
                            oReportResponse.BureauData.consumerInfoNO04Field = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerInfoNO04[count];

                            i = 0;

                            string HomeTelephoneno = string.Empty, WorkTelephoneno = string.Empty, cellularno = string.Empty;

                            foreach (DataRow r in dsReport.Tables["ActiveDirectorDetail"].Rows)
                            {

                                XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerInfoNO04 o = new XDSPortalLibrary.Entity_Layer.MTNSOA.ConsumerInfoNO04();

                                o.consumerNoField = r["DirectorID"].ToString();
                                o.dateOfBirthField = r["BirthDate"].ToString();
                                o.deceasedDateField = r["Deceaseddate"].ToString();
                                o.forename1Field = r["FirstName"].ToString();
                                o.forename2Field = r["SecondName"].ToString();
                                o.genderField = r["Gender"].ToString();
                                o.identityNo1Field = r["IDNo"].ToString();
                                o.identityNo2Field = r["PassportNo"].ToString();
                                o.maritalStatusDescField = r["MaritalStatusDesc"].ToString();
                                o.nameInfoDateField = r["LastUpdatedDate"].ToString();
                                o.partField = string.Empty;
                                o.partSeqField = string.Empty;
                                o.recordSeqField = "1";
                                o.spouseName1Field = r["SpouseFirstName"].ToString();
                                o.spouseName2Field = r["SpouseSurName"].ToString();
                                o.surnameField = r["Surname"].ToString();
                                HomeTelephoneno = r["HomeTelephoneNo"].ToString();
                                WorkTelephoneno = r["WorkTelephoneNo"].ToString();
                                cellularno = r["CellularNo"].ToString();
                                o.telephoneNumbersField = (string.IsNullOrEmpty(HomeTelephoneno) ? string.Empty : "H" + HomeTelephoneno + " ") +
                                    (string.IsNullOrEmpty(WorkTelephoneno) ? string.Empty : "B" + WorkTelephoneno + " ") +
                                    (string.IsNullOrEmpty(cellularno) ? string.Empty : "C" + cellularno);
                                o.titleField = r["TitleDesc"].ToString();
                                

                                oReportResponse.BureauData.consumerInfoNO04Field[i] = o;

                                i++;


                            }
                        }

                        if (dsReport.Tables.Contains("BusinessEnquiryHistory"))
                        {
                            count = ds.Tables["BusinessEnquiryHistory"].Rows.Count;

                            oReportResponse.BureauData.enquiryHistoryField = new XDSPortalLibrary.Entity_Layer.MTNSOA.EnquiryHistory[count];

                            i = 0;

                            foreach (DataRow dr in ds.Tables["BusinessEnquiryHistory"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.MTNSOA.EnquiryHistory o = new XDSPortalLibrary.Entity_Layer.MTNSOA.EnquiryHistory();
                                o.majorProductField = strReportID.ToString();
                                o.dateOfEnquiryField = dr["EnquiryDate"].ToString();
                                o.contactPhoneField = dr["SubscriberContact"].ToString();
                                o.subscriberField = dr["SubscriberName"].ToString();

                                oReportResponse.BureauData.enquiryHistoryField[i] = o;

                                i++;
                            }
                        }

                    }
                   // File.AppendAllText(@"C:\Log\mtnvet.txt", "threads");
                    threads.ForEach(t => t.Join());
                   // File.AppendAllText(@"C:\Log\mtnvet.txt", "pressage");
                    Pressage.ForEach(t => t.Join());

                    if (dsReport.Tables.Contains("CommercialPrincipalInformation"))
                    {
                        string DirectorID = string.Empty;

                        string tablename = "PrincipalScoring";


                        foreach (DataRow r in dsReport.Tables["CommercialPrincipalInformation"].Rows)
                        {
                            DirectorID = r["DirectorID"].ToString();
                            tablename = tablename + DirectorID;

                            if (dsReport.Tables.Contains(tablename))
                            {
                                foreach (DataRow rScore in dsReport.Tables[tablename].Rows)
                                {

                                    if (oReportResponse.BureauData.principalsField != null)
                                    {

                                        i = 0;

                                        while (oReportResponse.BureauData.principalsField.Count() < i)
                                        {

                                            if (oReportResponse.BureauData.principalsField[i].empiricaExclusionDescriptionField == DirectorID && rScore["DirectorID"].ToString() == DirectorID)
                                            {
                                                XDSPortalLibrary.Entity_Layer.MTNSOA.Principal1 o = new XDSPortalLibrary.Entity_Layer.MTNSOA.Principal1();
                                                o = oReportResponse.BureauData.principalsField[i];
                                                o.empiricaExclusionDescriptionField = rScore["Exception_Code"].ToString();
                                                o.empiricaReasonCodeField = new string[3];
                                                o.empiricaReasonCodeField[0] = rScore["ReasonCode1"].ToString();
                                                o.empiricaReasonCodeField[1] = rScore["ReasonCode2"].ToString();
                                                o.empiricaReasonCodeField[2] = rScore["ReasonCode3"].ToString();
                                                o.empiricaScoreField = r["FinalScore"].ToString();

                                                oReportResponse.BureauData.principalsField[i] = o;

                                                break;
                                            }

                                            i++;
                                        }
                                    }

                                }
                            }



                        }
                    }
                   // File.AppendAllText(@"C:\Log\mtnvet.txt", "isexisting");
                    ObjSearch.IsExistingClient = ObjSearch.IsExistingClient.ToUpper();
                    if (ObjSearch.IsExistingClient.ToUpper() == "2" || ObjSearch.IsExistingClient.ToUpper() == "1" || ObjSearch.IsExistingClient.ToUpper() == "Y")
                    {
                        Thread thMTNcomScore = new Thread(new ParameterizedThreadStart(ProcessReport));
                        thMTNcomScore.Start(string.Format("Execute dbo.[spCLR_MTN_CommercialScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}','{11}',{12},{13},{14},{15},'{16}'",
                            ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberEnquiryID, ObjSearch.SubscriberEnquiryResultID,
                            ObjSearch.ProductID, strReportID, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.AssociationCode, ObjSearch.IsExistingClient,
                            string.Empty, string.Empty, ObjSearch.ProductIndicator, COM003, com024, com026, COM003));
                        

                        string tname = "MTNScore" + ObjSearch.IsExistingClient;

                        if ( ObjSearch.IsExistingClient.ToUpper() == "Y")
                            ObjSearch.IsExistingClient = "N"; 
                        else
                        ObjSearch.IsExistingClient = "3";                        

                        Thread thMTNcomScore1 = new Thread(new ParameterizedThreadStart(ProcessReport));

                        thMTNcomScore1.Start(string.Format("Execute dbo.[spCLR_MTN_CommercialScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}','{11}',{12},{13},{14},{15},'{16}'",
                           ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberEnquiryID, ObjSearch.SubscriberEnquiryResultID,
                           ObjSearch.ProductID, strReportID, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.AssociationCode, ObjSearch.IsExistingClient,
                           string.Empty, string.Empty, ObjSearch.ProductIndicator, COM003, com024, com026, COM003));

                        thMTNcomScore.Join();

                        thMTNcomScore1.Join();

                        if (dsReport.Tables.Contains(tname))
                        {

                            foreach (DataRow dr in ds.Tables[tname].Rows)
                            {
                                //oReportResponse.BehaviouralScore = Convert.ToDecimal(dr["XDSPresageScore"].ToString());
                                //oReportResponse.BehaviouralRiskBand = dr["SMEBand"].ToString();
                                oReportResponse.BehaviouralScore = Convert.ToDecimal(dr["BehaviouralScore"].ToString());
                                oReportResponse.BehaviouralRiskBand = dr["BehaviouralBand"].ToString();


                            }
                        }

                        tname = "MTNScore" + ObjSearch.IsExistingClient;

                        if (dsReport.Tables.Contains(tname))
                        {

                            foreach (DataRow dr in ds.Tables[tname].Rows)
                            {
                                oReportResponse.FinalOutcome = dr["Result"].ToString();
                                oReportResponse.FinalOutcomeReason = dr["ReferralReason"].ToString();
                                oReportResponse.MatrixBand = dr["MatrixBand"].ToString();
                                oReportResponse.CompanyBand = dr["SMEBand"].ToString();
                                oReportResponse.CompanyScore = dr["FinalScore"].ToString();

                                //File.AppendAllText(@"C:\Log\mtnvet1.txt", dr["FinalScore"].ToString());
                                oReportResponse.FinalPrincipalScore = dr["XDSPresageScore"].ToString();

                                //File.AppendAllText(@"C:\Log\mtnvet1.txt", dr["XDSPresageScore"].ToString() + ":" + oReportResponse.FinalPrincipalScore.ToString());
                                oReportResponse.FinalPrincipalBand = dr["PrincipalBand"].ToString();
                              //  oReportResponse.CompanyBand = dr["Scoreband"].ToString();


                            }
                        }
                    }

                    else if (ObjSearch.IsExistingClient.ToUpper() == "3" || ObjSearch.IsExistingClient.ToUpper() == "N")
                    {
                        Thread thMTNcomScore = new Thread(new ParameterizedThreadStart(ProcessReport));
                        thMTNcomScore.Start(string.Format("Execute dbo.[spCLR_MTN_CommercialScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}','{11}',{12},{13},{14},{15},'{16}'",
                            ObjSearch.CommercialID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberEnquiryID, ObjSearch.SubscriberEnquiryResultID,
                            ObjSearch.ProductID, strReportID, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.AssociationCode, ObjSearch.IsExistingClient,
                            string.Empty, string.Empty, ObjSearch.ProductIndicator, COM003, com024, com026, COM003));


                        thMTNcomScore.Join();
                        string tname = "MTNScore" + ObjSearch.IsExistingClient;

                        if (dsReport.Tables.Contains(tname))
                        {

                            foreach (DataRow dr in ds.Tables[tname].Rows)
                            {
                                oReportResponse.FinalOutcome = dr["Result"].ToString();
                                oReportResponse.FinalOutcomeReason = dr["ReferralReason"].ToString();
                                oReportResponse.MatrixBand = dr["MatrixBand"].ToString();
                                oReportResponse.CompanyBand = dr["SMEBand"].ToString();
                                oReportResponse.CompanyScore = dr["FinalScore"].ToString();

                                //File.AppendAllText(@"C:\Log\mtnvet1.txt", dr["FinalScore"].ToString());
                                oReportResponse.FinalPrincipalScore = dr["XDSPresageScore"].ToString();

                                //File.AppendAllText(@"C:\Log\mtnvet1.txt", dr["XDSPresageScore"].ToString() + ":" + oReportResponse.FinalPrincipalScore.ToString());
                                oReportResponse.FinalPrincipalBand = dr["PrincipalBand"].ToString();
                                //  oReportResponse.CompanyBand = dr["Scoreband"].ToString();


                            }
                        }

                        tname = "MTNScoreDetails" + ObjSearch.IsExistingClient;

                        if (ds.Tables.Contains(tname))
                        {

                            foreach (DataRow dr in ds.Tables[tname].Rows)
                            {


                                if (dr["Code"].ToString() == "AffordabilityOverride")
                                {
                                    int iAfford = 0;
                                    int.TryParse(dr["Att"].ToString(), out iAfford);
                                    oReportResponse.BureauData.AffordabilityOverride = iAfford;
                                }
                                else if (dr["Code"].ToString() == "AffordabilityOverrideEndDate")
                                {
                                    DateTime odateTime = new DateTime();
                                    DateTime.TryParseExact(dr["Att"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out odateTime);
                                    oReportResponse.BureauData.AffordabilityOverrideEndDate = odateTime;
                                }
                                else if (dr["Code"].ToString() == "CreditLimit")
                                {
                                    decimal dcreditlimit = 0;
                                    decimal.TryParse(dr["Att"].ToString(), out dcreditlimit);
                                    oReportResponse.BureauData.CreditLimit = dcreditlimit;
                                }
                                else if (dr["Code"].ToString() == "Recommendation")
                                {
                                    if (oReportResponse.BureauData.ScorecardRecommendation == null)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendation();
                                    }
                                    oReportResponse.BureauData.ScorecardRecommendation.Recommendation = dr["Att"].ToString() == "1" ? "ACCEPT" : "DECLINE";
                                }
                                else if (dr["Code"].ToString() == "InternalReason")
                                {
                                    if (oReportResponse.BureauData.ScorecardRecommendation == null)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendation();
                                    }
                                    if (oReportResponse.BureauData.ScorecardRecommendation.Reasons == null || oReportResponse.BureauData.ScorecardRecommendation.Reasons.Length == 0)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation.Reasons = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendationReasonsReason[1];
                                        oReportResponse.BureauData.ScorecardRecommendation.Reasons[0] = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendationReasonsReason();
                                    }
                                    oReportResponse.BureauData.ScorecardRecommendation.Reasons[0].Internal = dr["Att"].ToString();
                                }
                                else if (dr["Code"].ToString() == "ExternalReason")
                                {
                                    if (oReportResponse.BureauData.ScorecardRecommendation == null)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendation();
                                    }
                                    if (oReportResponse.BureauData.ScorecardRecommendation.Reasons == null || oReportResponse.BureauData.ScorecardRecommendation.Reasons.Length == 0)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation.Reasons = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendationReasonsReason[1];
                                        oReportResponse.BureauData.ScorecardRecommendation.Reasons[0] = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendationReasonsReason();
                                    }
                                    oReportResponse.BureauData.ScorecardRecommendation.Reasons[0].External = dr["Att"].ToString();
                                }
                                else if (dr["Code"].ToString() == "Code")
                                {
                                    if (oReportResponse.BureauData.ScorecardRecommendation == null)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendation();
                                    }
                                    if (oReportResponse.BureauData.ScorecardRecommendation.Reasons == null || oReportResponse.BureauData.ScorecardRecommendation.Reasons.Length == 0)
                                    {
                                        oReportResponse.BureauData.ScorecardRecommendation.Reasons = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendationReasonsReason[1];
                                        oReportResponse.BureauData.ScorecardRecommendation.Reasons[0] = new XDSPortalLibrary.Entity_Layer.MTNSOA.ScorecardRecommendationReasonsReason();
                                    }
                                    oReportResponse.BureauData.ScorecardRecommendation.Reasons[0].Code = dr["Code"].ToString();
                                }
                                else if (dr["Code"].ToString() == "CampaignFileName" && dr["Att"].ToString() != null)
                                {
                                    if (oReportResponse.BureauData.CampaignInformation == null)
                                    {
                                        oReportResponse.BureauData.CampaignInformation = new XDSPortalLibrary.Entity_Layer.MTNSOA.CampaignInformation();

                                    }
                                    oReportResponse.BureauData.CampaignInformation.CampaignFileName = dr["Att"].ToString();

                                }
                                else if (dr["Code"].ToString() == "CampaignName" && dr["Att"].ToString() != null)
                                {
                                    if (oReportResponse.BureauData.CampaignInformation == null)
                                    {
                                        oReportResponse.BureauData.CampaignInformation = new XDSPortalLibrary.Entity_Layer.MTNSOA.CampaignInformation();

                                    }
                                    oReportResponse.BureauData.CampaignInformation.CampaignName = dr["Att"].ToString();

                                }
                                else if (dr["Code"].ToString() == "CampaignValidity" && dr["Att"].ToString() != null)
                                {
                                    DateTime odateTime = new DateTime();
                                    DateTime.TryParseExact(dr["Att"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out odateTime);

                                    if (oReportResponse.BureauData.CampaignInformation == null)
                                    {
                                        oReportResponse.BureauData.CampaignInformation = new XDSPortalLibrary.Entity_Layer.MTNSOA.CampaignInformation();

                                    }
                                    oReportResponse.BureauData.CampaignInformation.CampaignValidity = odateTime;

                                }
                                else if (dr["Code"].ToString() == "PreApproved" && dr["Att"].ToString() != null)
                                {
                                    Boolean oBoolean = new Boolean();
                                    Boolean.TryParse(dr["Att"].ToString(), out oBoolean);

                                    if (oReportResponse.BureauData.CampaignInformation == null)
                                    {
                                        oReportResponse.BureauData.CampaignInformation = new XDSPortalLibrary.Entity_Layer.MTNSOA.CampaignInformation();

                                    }
                                    oReportResponse.BureauData.CampaignInformation.PreApproved = oBoolean;

                                }



                            }
                        }
                    }

                    if (oReportResponse.BehaviouralScore == 0)
                    {
                        oReportResponse.ScoreFlag = "1";
                    }
                    else if (oReportResponse.BehaviouralScore > 0)
                    {
                        oReportResponse.ScoreFlag = "0";
                    }

                      		


                    if (dtError.Rows.Count > 0)
                    {

                        //File.AppendAllText(@"C:\Log\mtnvet.txt", dtError.Rows[0]["ErrorDescription"].ToString());
                        throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                    }

                    //XmlSerializer serializer1 = new XmlSerializer(oReportResponse.GetType());
                    //System.IO.StringWriter sw1 = new System.IO.StringWriter();
                    //serializer1.Serialize(sw1, oReportResponse);
                    //System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());


                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;
                    ObjResponse.ResponseExternalReferenceNo = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }

                }
                catch (Exception e)
                {
                   // File.AppendAllText(@"C:\Log\mtnvet.txt", e.Message);

                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = e.Message;


                    oReportResponse.BureauData.responseStatusField = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure;
                    oReportResponse.BureauData.errorMessageField = "Error While Processing your request, Please Contact XDS";

                    oReportResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                    oReportResponse.ErrorDescription = "Error While Processing your request, Please Contact XDS";
                }



            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetPublicDomainReport(XDSPortalLibrary.Entity_Layer.PublicDomainEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                ProcessReport(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                ProcessReport(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                ProcessReport(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }



                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                dsDataSegment.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }

                dsTracePlus.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetDirectorCreditEnquiryReport(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "D";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Director");

                DataSet dsReport = new DataSet("Director");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thAkaNames.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation)
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.DirectorID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    dsDataSegment.Dispose();
                    dsTracePlus.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetDirectorCreditEnquiryReportPDR(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "D";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Director");

                DataSet dsReport = new DataSet("Director");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformationPDR] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLinkPDR] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thAkaNames.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation)
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.DirectorID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    dsDataSegment.Dispose();
                    dsTracePlus.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetDirectorCreditEnquiryReportBusinessStatus(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "D";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Director");

                DataSet dsReport = new DataSet("Director");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformationPDR] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLinkPDR] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerPotentialFraudIndicator.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thAkaNames.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation)
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.DirectorID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    dsDataSegment.Dispose();
                    dsTracePlus.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetIDVerificationReport(XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("HomeAffairs");

                DataSet dsReport = new DataSet("HomeAffairs");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                ProcessReport(string.Format("execute [dbo].[spCLR_ConsumerIdentityVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.HomeAffairsID.ToString(),
                    ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(),
                    ObjSearch.IDno.ToString(), ObjSearch.Firstname.ToString(), ObjSearch.Surname.ToString(),
                    ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }



                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                dsDataSegment.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }
                dsTracePlus.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }


        public XDSPortalLibrary.Entity_Layer.Response GetIDBioMetricVerificationReport(XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string LinkFailureError = "";
            string LinkFailureXML = "";
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("HomeAffairs");

                DataSet dsReport = new DataSet("HomeAffairs");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");




                ProcessReport(string.Format("execute [dbo].[spCLR_ConsumerIdentityVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.HomeAffairsID.ToString(),
                    ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(),
                    ObjSearch.IDno.ToString(), ObjSearch.Firstname.ToString(), ObjSearch.Surname.ToString(),
                    ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }

                ////BiometricsAXControl.Model.VerifyRequest oRequest = new BiometricsAXControl.Model.VerifyRequest();
                ////oRequest.ClientIdNumber = ObjSearch.IDno;
                ////oRequest.UseProxy = false;
                ////oRequest.VendorId = Guid.Parse(ObjSearch.VendorID);

                ////BiometricsAXControl.Model.CaptureBiometricsModel ocapture = new BiometricsAXControl.Model.CaptureBiometricsModel();
                ////ocapture.DateTime = ObjSearch.LastUsedTimeStamp;
                ////ocapture.Errors = new List<string>() { ObjSearch.Errors };
                ////ocapture.Finger1Index = ((BiometricsAXControl.FingerIndex)ObjSearch.Finger1Index);
                ////ocapture.Finger2Index = ((BiometricsAXControl.FingerIndex)ObjSearch.Finger2Index);
                ////ocapture.Finger1Wsq = ObjSearch.Finger1WsqImage;
                ////ocapture.Finger2Wsq = ObjSearch.Finger2WsqImage;
                ////ocapture.IdNumber = ObjSearch.IDno;
                ////ocapture.SensorFirmwareVersion = ObjSearch.DeviceFirmwareVersion;
                ////ocapture.SensorModel = ObjSearch.DeviceModel;
                ////ocapture.SensorSerialNumber = ObjSearch.DeviceSerialNumber;
                ////ocapture.WorkstationName = ObjSearch.WorkStationName;
                ////ocapture.LoggedInUserName = ObjSearch.WorkstationLoggedInUserName;

                ////BiometricsAXControl.Model.VerifyResponse oResponse = new BiometricsAXControl.Model.VerifyResponse();

                ////oResponse = BiometricsAXControl.VerificationService.SubmitVerification(oRequest, ocapture);

                //IVSBio.Service1Asmx oIvsclient = new IVSBio.Service1Asmx();
                //oIvsclient.Url = ObjSearch.IVSURL;

                //IVSBio.IvsHanisPublicAccessRequestModel or = new IVSBio.IvsHanisPublicAccessRequestModel();
                //or.IdNumber = ObjSearch.IDno;
                //or.FromProxyUrl = string.Empty;
                //or.VedorId = Guid.Parse(ObjSearch.VendorID);
                //or.DateTime = DateTime.Now;
                //or.FingerIndex1 = (int)ObjSearch.Finger1Index;
                //or.FingerIndex2 = (int)ObjSearch.Finger2Index;
                //or.FingerPrint1 = ObjSearch.Finger1WsqImage;
                //or.FingerPrint2 = ObjSearch.Finger2WsqImage;
                //or.LoggedInUserName = ObjSearch.WorkstationLoggedInUserName;
                //or.VedorId =  Guid.Parse(ObjSearch.VendorID);
                //or.SensorSerialNumber = ObjSearch.DeviceSerialNumber;
                //or.SensorModel = ObjSearch.DeviceModel;
                //or.SensorFirmwareVersion = ObjSearch.DeviceFirmwareVersion;
                //or.WorkstationName = ObjSearch.WorkStationName;


                //IVSBio.IvsHanisPublicAccessResponseModel oResponse = new IVSBio.IvsHanisPublicAccessResponseModel();

                

                //oResponse = oIvsclient.SubmitVerification(or);

                //DataTable dt = new DataTable();
                //dt.TableName = "BioMetricVerificationResult";

                

                ////foreach (PropertyInfo info in typeof(BiometricsAXControl.Model.VerifyResponse).GetProperties() )
                ////{
                ////    File.AppendAllText(@"C:\Log\IDVLog.txt", info.Name);
                //dt.Columns.Add(new DataColumn("ApplicationErrors", typeof(string)));
                //dt.Columns.Add(new DataColumn("Base64StringJpeg2000Image", typeof(string)));
                //dt.Columns.Add(new DataColumn("FingerColor", typeof(string)));
                //dt.Columns.Add(new DataColumn("HasImage", typeof(string)));
                //dt.Columns.Add(new DataColumn("TmStamp", typeof(string)));
                //dt.Columns.Add(new DataColumn("TransactionNumber", typeof(string)));
                //dt.Columns.Add(new DataColumn("VerificationColorIndicator", typeof(string)));
                //dt.Columns.Add(new DataColumn("VerificationErrors", typeof(string)));
                //dt.Columns.Add(new DataColumn("VerificationResult", typeof(string)));
                //dt.Columns.Add(new DataColumn("VerificationResultDescription", typeof(string)));
 
                ////}
                //dt.Columns.Add(new DataColumn("SubscriberEnquiryID", typeof(string)));
                //dt.Columns.Add(new DataColumn("SubscriberEnquiryResultID", typeof(string)));    

                //dt.AcceptChanges();

                //DataRow drbio = dt.NewRow();

                //if (dt.Columns.Count > 0)
                //{

                //    for (int i = 0; i < dt.Columns.Count; i++)
                //    {
                //        File.AppendAllText(@"C:\Log\IDVLog.txt", dt.Columns[i].ColumnName);
                //    }
                //}

                //List<string> ApplicationErrors = null;
                //List<string> VerificationErrors = null;
                //string VerificationResult = string.Empty;

                //try
                //{


                //    if (oResponse != null)
                //    {
                //        // drbio["ApplicationErrors"] = oResponse.ApplicationErrors == null ? string.Empty : oResponse.ApplicationErrors.ToString();
                //        drbio["Base64StringJpeg2000Image"] = oResponse.JpegImage == null ? string.Empty : oResponse.JpegImage.ToString();
                //        drbio["FingerColor"] = oResponse.VerificationColor.ToString();
                //        drbio["HasImage"] = oResponse.HasImage;
                //        drbio["TmStamp"] = DateTime.Now;
                //        drbio["TransactionNumber"] = oResponse.TransactionId;
                //        drbio["VerificationColorIndicator"] = string.Empty;
                //        //drbio["VerificationErrors"] = oResponse.VerificationErrors == null ? string.Empty : oResponse.VerificationErrors.ToString();
                //        //drbio["VerificationResult"] = oResponse.Body.SubmitVerificationResult.VerificationResult;
                //        drbio["VerificationResultDescription"] = oResponse.ResultDescription == null ? string.Empty : oResponse.ResultDescription.ToString();
                //        drbio["SubscriberEnquiryID"] = ObjSearch.EnquiryID;
                //        drbio["SubscriberEnquiryResultID"] = ObjSearch.EnquiryResultID;




                //        if (oResponse.WebServiceErrors != null && oResponse.WebServiceErrors.Count() > 0)
                //        {
                //            if (ApplicationErrors == null)
                //                ApplicationErrors = new List<string>();
                //            ApplicationErrors.AddRange((IEnumerable<string>)oResponse.WebServiceErrors);
                //        }
                //        if (!string.IsNullOrWhiteSpace(oResponse.HanisError) || oResponse.VerificationResult == null)
                //        {
                //            VerificationErrors = new List<string>()
                //              {
                //                oResponse.HanisError
                //              };
                //            VerificationResult = oResponse.VerificationResult == null || !oResponse.VerificationResult.Equals("verified", StringComparison.InvariantCultureIgnoreCase) ? "Unverified_Errors" : "Verified_Errors";
                //        }
                //        else
                //            VerificationResult = oResponse.VerificationResult.Equals("verified", StringComparison.InvariantCultureIgnoreCase) ? "Verified" : "Unverified_Finger_Prints";
                //    }


                //}
                //catch (Exception ex)
                //{
                //    ApplicationErrors = new List<string>()
                //    {
                //      ex.Message
                //    };
                //    if (ex.InnerException != null)
                //        ApplicationErrors.Add(ex.InnerException.Message);
                //    VerificationResult = "Failed_Application_Erors";
                //}

                //if (ApplicationErrors != null)
                //{
                //    drbio["ApplicationErrors"] = String.Join(", ", ApplicationErrors);
                //    LinkFailureError = drbio["ApplicationErrors"].ToString();
                //}
                //else
                //{
                //    drbio["ApplicationErrors"] = string.Empty;
                //}
                //if (VerificationErrors != null)
                //{
                //    drbio["VerificationErrors"] = String.Join(", ", VerificationErrors);
                //}
                //else
                //{
                //    drbio["VerificationErrors"] = string.Empty;
                //}
                //drbio["VerificationResult"] = VerificationResult;

                //File.AppendAllText(@"C:\Log\IDVLog.txt", "stepx");

                //dt.Rows.Add(drbio);
                //dsReport.Tables.Add(dt);


                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
             //   File.AppendAllText(@"C:\Log\IDVLog.txt", ObjResponse.ResponseData);
                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                dsDataSegment.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }
                dsTracePlus.Dispose();

                if (LinkFailureError.Contains("There was no endpoint listening") ||
                    LinkFailureError.Contains("The request channel timed out") ||
                    LinkFailureError.Contains("The remote name could not be resolved"))
                {
                  
                       LinkFailureXML = xml;
                       throw new Exception(LinkFailureXML);
                  
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                if (LinkFailureXML != "")
                {
                    ObjResponse.ResponseData = LinkFailureXML;
                }
                else
                {
                    if (ex.Message.Contains("ivs") || ex.Message.Contains("hanis"))
                    {
                        ObjResponse.ResponseData = "DHA link is down.";
                    }
                    else
                    {
                        ObjResponse.ResponseData = ex.Message;
                    }
                }
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetIDPhotoVerificationReport(XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("HomeAffairs");

                DataSet dsReport = new DataSet("HomeAffairs");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                ProcessReport(string.Format("execute [dbo].[spCLR_ConsumerIdentityPhotoVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.HomeAffairsID.ToString(),
                    ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(),
                    ObjSearch.IDno.ToString(), ObjSearch.Firstname.ToString(), ObjSearch.Surname.ToString(),
                    ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }



                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                dsDataSegment.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }
                dsTracePlus.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetDeedsEnquiryReport(XDSPortalLibrary.Entity_Layer.DeedsEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Property");

                DataSet dsReport = new DataSet("Property");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                ProcessReport(string.Format("execute [dbo].[spCLR_PropertyInformation] {0} , '{1}', '{2}','{3}',{4},'{5}','{6}' ", ObjSearch.PropertyDeedID.ToString(), ObjSearch.ReferenceNo, ObjSearch.ExternalReference, ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.IDno, ObjSearch.BusinessName));



                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }


                string xml = dsReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.PropertyDeedID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                dsReport.Clear();

                dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                dsReport.Dispose();
                dsDataSegment.Dispose();
                dsTracePlus.Dispose();
                if (dsTracPlusDataSegment != null)
                {
                    dsTracPlusDataSegment.Dispose();
                }


            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response LoadConsumerDefaultAlert(XDSPortalLibrary.Entity_Layer.ConsumerDefaultAlertListing ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DateTime dBirthDate = DateTime.Now;
                try
                {
                    if (!string.IsNullOrEmpty(ObjSearch.BirthDate)) dBirthDate = DateTime.Parse(ObjSearch.BirthDate);
                }
                catch (Exception a)
                {
                    throw new Exception("Invalid Birth date supplied");

                }
                DateTime dEffectiveDate = DateTime.Now;
                try
                {
                    if (!string.IsNullOrEmpty(ObjSearch.Effectivedate)) dEffectiveDate = DateTime.Parse(ObjSearch.Effectivedate);
                }
                catch (Exception b)
                {
                    throw new Exception("Invalid Effective date supplied");
                }

                DateTime dtAmnestyDate = new DateTime(2014, 4, 2);

                dtAmnestyDate = Convert.ToDateTime(dtAmnestyDate.ToString("MM/dd/yyyy"));
                dEffectiveDate = Convert.ToDateTime(dEffectiveDate.ToString("MM/dd/yyyy"));

                if (dEffectiveDate < dtAmnestyDate)
                    throw new Exception("Please provide an effective date after 01 April 2014");

                if (String.IsNullOrEmpty(ObjSearch.Surname) && String.IsNullOrEmpty(ObjSearch.FirstName) && String.IsNullOrEmpty(ObjSearch.AccountNo) && ObjSearch.Amount == 0 && String.IsNullOrEmpty(ObjSearch.StatusCode))
                {
                    throw new Exception("Your input must have a SurName, FirstName, Account Number, Amount and Reason Code");
                }
                else if (String.IsNullOrEmpty(ObjSearch.idno) && (String.IsNullOrEmpty(ObjSearch.PassportNo) || (dBirthDate.Year <= 1900)))
                {
                    throw new Exception("Either ID Number or the combination of passportno and date of Birth is mandatory");
                }
                else if (ObjSearch.SMSNotification == false && ObjSearch.EmailNotification == false && ObjSearch.ClientResponsibleToNotify == "0")
                {
                    throw new Exception("Please select the option I am Responsible for notifying the Client");
                }
                else if ((ObjSearch.SMSNotification == true) && (ObjSearch.Mobile == string.Empty) && string.IsNullOrEmpty(ObjSearch.ClientContactDetails))
                {
                    throw new Exception("Please enter a valid Mobile number of the consumer and Client Contact Details to send the SMS Notification");
                }
                else if ((ObjSearch.EmailNotification == true) && (ObjSearch.EmailId == string.Empty) && string.IsNullOrEmpty(ObjSearch.ClientContactDetails))
                {
                    throw new Exception("Please enter a valid Email address of the consumer and Client Contact Details to send the Email Notification");
                }
                else if (ObjSearch.Amount < 100)
                {
                    throw new Exception("Amount must be Greaterthan 100");
                }
                else if (ObjSearch.AlertType == "D" && ObjSearch.ConsumerNotified == "0")
                {
                    throw new Exception("Consumer must be notified 20 days prior to the default alert date");
                }
                else if ((ObjSearch.EmailNotification == true) && (Regex.IsMatch(ObjSearch.EmailId, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false))
                {
                    throw new Exception("Invalid Email address supplied");
                }
                else if ((ObjSearch.SMSNotification == true) && (ObjSearch.Mobile.Length != 10 || (!Regex.IsMatch(ObjSearch.Mobile, "^[0-9]*$"))))
                {
                    throw new Exception("Invalid phone number supplied");
                }
                else if (!String.IsNullOrEmpty(ObjSearch.ClientContactDetails) && (Regex.IsMatch(ObjSearch.ClientContactDetails, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false) && (ObjSearch.ClientContactDetails.Length != 10 || Regex.IsMatch(ObjSearch.ClientContactDetails, "^[0-9]*$") == false))
                {
                    throw new Exception("Invalid Client Contact Details supplied! Please Enter a 10 digit Phone Number or a valid Email Address.");
                }

                ds = new DataSet("ConsumerDefaultALert");

                string strSQL = string.Format("execute sp_InsertOnlineConsumerDefaultAlert '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}'", ObjSearch.idno.Replace("'", "''"), ObjSearch.PassportNo.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), dBirthDate.ToShortDateString(), ObjSearch.Gender.Replace("'", "''"), ObjSearch.AccountNo.Replace("'", "''"), ObjSearch.SubAccountNo.Replace("'", "''"), ObjSearch.Accountype.Replace("'", "''"), ObjSearch.StatusCode.Replace("'", "''"), dEffectiveDate.ToShortDateString(), ObjSearch.Amount, ObjSearch.comments.Replace("'", "''"), ObjSearch.Hometelephone.Replace("'", "''"), ObjSearch.WorkTelephone.Replace("'", "''"), ObjSearch.Mobile.Replace("'", "''"), ObjSearch.Address1.Replace("'", "''"), ObjSearch.Address2.Replace("'", "''"), ObjSearch.Address3.Replace("'", "''"), ObjSearch.Address4.Replace("'", "''"), ObjSearch.EmailId.Replace("'", "''"), ObjSearch.Postalcode, ObjSearch.Createdbyuser, ObjSearch.SystemUserID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.ThirdName, ObjSearch.AlertType, ObjSearch.ConsumerNotified, ObjSearch.ClientContactDetails, ObjSearch.ClientResponsibleToNotify, ObjSearch.SubscriberName, ObjSearch.DefaultMoveAfter20D);



                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataTable dt in ds.Tables)
                {
                    dt.TableName = "Result";
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    DataColumn dc = new DataColumn("Status");
                    ds.Tables[0].Columns.Add(dc);
                    if (ObjSearch.AlertType == "D")
                    {
                        dr["Status"] = "Consumer Listed successfully,please wait for 15 minutes to view your Default Listing.";
                    }
                    else if (ObjSearch.AlertType == "P")
                    {
                        dr["Status"] = "Payment Notification listing was made, please wait for 15 minutes to view your Payment Notification.";
                    }
                }

                string reference = ds.Tables[0].Rows[0][0].ToString();
                ObjResponse.ResponseKey = int.Parse(reference);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = ds.GetXml();
                //ObjResponse.ResponseData = "Consumer Listed successfully";
                //"<ConsumerDefaultAlert><Status>Enquiry Submitted successfully and your reference number is: " + reference + "</Status></ConsumerDefaultAlert>";

                dtError.Clear();
                ds.Clear();


                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response LoadCommercialDefaultAlert(XDSPortalLibrary.Entity_Layer.CommercialDefaultAlertListing ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                //DateTime dBirthDate = DateTime.Now;
                //try
                //{
                //    if (!string.IsNullOrEmpty(ObjSearch.BirthDate)) dBirthDate = DateTime.Parse(ObjSearch.BirthDate);
                //}
                //catch (Exception a)
                //{
                //    throw new Exception("Invalid Birth date supplied");

                //}
                DateTime dEffectiveDate = DateTime.Now;
                try
                {
                    if (!string.IsNullOrEmpty(ObjSearch.Effectivedate)) dEffectiveDate = DateTime.Parse(ObjSearch.Effectivedate);
                }
                catch (Exception b)
                {
                    throw new Exception("Invalid Effective date supplied");
                }

                DateTime dtAmnestyDate = new DateTime(2014, 4, 2);

                dtAmnestyDate = Convert.ToDateTime(dtAmnestyDate.ToString("MM/dd/yyyy"));
                dEffectiveDate = Convert.ToDateTime(dEffectiveDate.ToString("MM/dd/yyyy"));

                if (dEffectiveDate < dtAmnestyDate)
                    throw new Exception("Please provide an effective date after 01 April 2014");

                if (String.IsNullOrEmpty(ObjSearch.RegistrationNumber) && String.IsNullOrEmpty(ObjSearch.VatNo) && String.IsNullOrEmpty(ObjSearch.AccountNo) && ObjSearch.Amount == 0 && String.IsNullOrEmpty(ObjSearch.StatusCode))
                {
                    throw new Exception("Your input must have a Registration No, Busniess Name, Account Number, Amount and Reason Code");
                }
                else if (ObjSearch.RegistrationNumber == "//")
                {
                    throw new Exception("Please enter a Valid Registration Number");
                }
                else if (ObjSearch.SMSNotification == false && ObjSearch.EmailNotification == false && ObjSearch.ClientResponsibleToNotify == "0")
                {
                    throw new Exception("Please select the option I am Responsible for notifying the Client");
                }
                else if ((ObjSearch.SMSNotification == true) && (ObjSearch.Mobile == string.Empty))
                {
                    throw new Exception("Please enter a valid Mobile number of the consumer to send the SMS Notification");
                }
                else if ((ObjSearch.EmailNotification == true) && (ObjSearch.EmailId == string.Empty))
                {
                    throw new Exception("Please enter a valid Email address of the consumer to send the Email Notification");
                }
                else if (ObjSearch.Amount < 100)
                {
                    throw new Exception("Amount must be Greaterthan 100");
                }
                else if (ObjSearch.AlertType == "D" && ObjSearch.ConsumerNotified == "0")
                {
                    throw new Exception("Consumer must be notified 20 days prior to the default alert date");
                }
                else if ((ObjSearch.EmailNotification == true) && (Regex.IsMatch(ObjSearch.EmailId, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false))
                {
                    throw new Exception("Invalid Email address supplied");
                }
                else if ((ObjSearch.SMSNotification == true) && (ObjSearch.Mobile.Length != 10 || (!Regex.IsMatch(ObjSearch.Mobile, "^[0-9]*$"))))
                {
                    throw new Exception("Invalid phone number supplied");
                }
                else if (!String.IsNullOrEmpty(ObjSearch.ClientContactDetails) && (Regex.IsMatch(ObjSearch.ClientContactDetails, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false) && (ObjSearch.ClientContactDetails.Length != 10 || Regex.IsMatch(ObjSearch.ClientContactDetails, "^[0-9]*$") == false))
                {
                    throw new Exception("Invalid Client Contact Details supplied! Please Enter a 10 digit Phone Number or a valid Email Address.");
                }

                ds = new DataSet("CommercialDefaultAlert");

                string strSQL = string.Format("execute sp_InsertOnlineCommercialDefaultAlert '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}'", ObjSearch.CommercialID.ToString(), ObjSearch.RegistrationNumber.Replace("'", "''"), ObjSearch.CommercialName.Replace("'", "''"), ObjSearch.VatNo.Replace("'", "''"), ObjSearch.idno.Replace("'", "''"), ObjSearch.PassportNo.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.ThirdName.Replace("'", "''"), ObjSearch.Gender.Replace("'", "''"), ObjSearch.AccountNo.Replace("'", "''"), ObjSearch.SubAccountNo.Replace("'", "''"), ObjSearch.Accountype.Replace("'", "''"), ObjSearch.StatusCode.Replace("'", "''"), dEffectiveDate.ToShortDateString(), ObjSearch.Amount, ObjSearch.comments.Replace("'", "''"), ObjSearch.Hometelephone.Replace("'", "''"), ObjSearch.WorkTelephone.Replace("'", "''"), ObjSearch.Mobile.Replace("'", "''"), ObjSearch.Address1.Replace("'", "''"), ObjSearch.Address2.Replace("'", "''"), ObjSearch.Address3.Replace("'", "''"), ObjSearch.Address4.Replace("'", "''"), ObjSearch.EmailId.Replace("'", "''"), ObjSearch.Postalcode, ObjSearch.Createdbyuser, ObjSearch.AlertType, ObjSearch.ConsumerNotified, ObjSearch.ClientContactDetails, ObjSearch.ClientResponsibleToNotify, ObjSearch.SubscriberName, ObjSearch.DefaultMoveAfter20D);

                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataTable dt in ds.Tables)
                {
                    dt.TableName = "Result";
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    DataColumn dc = new DataColumn("Status");
                    ds.Tables[0].Columns.Add(dc);
                    if (ObjSearch.AlertType == "D")
                    {
                        dr["Status"] = "Company Listed successfully, please wait for 15 minutes to view your Default Listing.";
                    }
                    else if (ObjSearch.AlertType == "P")
                    {
                        dr["Status"] = "Payment Notification listing was made, please wait for 15 minutes to view your Payment Notification.";
                    }
                }


                string reference = ds.Tables[0].Rows[0][0].ToString();
                ObjResponse.ResponseKey = int.Parse(reference);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = ds.GetXml();
                //ObjResponse.ResponseData = "Company Listed successfully";
                //"<CommercialDefaultAlert><Status>Enquiry Submitted successfully and your reference number is: " + reference + "</Status></CommercialDefaultAlert>";

                dtError.Clear();
                ds.Clear();


                dtError.Dispose();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerMatchCustom(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (ObjSearch.IsCustomVetting)
                {
                    if (String.IsNullOrEmpty(ObjSearch.IDno))
                    {
                        throw new Exception("ID Number is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.RequestNo.ToString()))
                    {
                        throw new Exception("RequestNo is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.FirstName))
                    {
                        throw new Exception("Customer Name is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.Surname))
                    {
                        throw new Exception("Customer Surname is mandatory.");
                    }
                }

                if ((String.IsNullOrEmpty(ObjSearch.IDno) || String.IsNullOrEmpty(ObjSearch.Passportno)) && (String.IsNullOrEmpty(ObjSearch.Surname)))
                {
                    throw new Exception("Your search input must have an Id Number and Surname or a Passport Number and Surname");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");


                string strSQL = string.Empty;

                if (ObjSearch.SubscriberAssociationCode.Trim().ToUpper() == "JDCUSTOM")
                
                {
                    
                    strSQL = string.Format("execute spConsumer_S_Match_Custom_JD '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate, ObjSearch.subscriberID);

                    //File.AppendAllText(@"C:\\Log\Response.txt", "\n" + strSQL);
                }
                else
                {
                    strSQL = string.Format("execute spConsumer_S_Match_Custom '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate, ObjSearch.subscriberID);
                }
                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerMatchCustomKYC(XDSPortalLibrary.Entity_Layer.ConsumerAddress ObjSearch, int ConsumerID, String IDNo, String PassportNo, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if ((String.IsNullOrEmpty(IDNo) && String.IsNullOrEmpty(PassportNo)))
                {
                    throw new Exception("Your search input must have an Id Number or a Passport Number");
                }

                if (String.IsNullOrEmpty(ObjSearch.StreetName) || String.IsNullOrEmpty(ObjSearch.StreetNo) || String.IsNullOrEmpty(ObjSearch.PostalCode))
                {
                    throw new Exception("Address Line 1, Address Line 2 and PostalCode are Mandatory");
                }

                ds = new DataSet("KYCOnline");

                string strSQL = string.Empty;
                strSQL = string.Format("execute spKYC_OnlineReport '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10}", IDNo.Replace("'", "''"), PassportNo.Replace("'", "''"), ObjSearch.StreetNo.Replace("'", "''"), ObjSearch.StreetName.Replace("'", "''"), ObjSearch.Suburb.Replace("'", "''"), ObjSearch.City.Replace("'", "''"), ObjSearch.PostalCode.Replace("'", "''"), ObjSearch.ExternalReference.Replace("'", "''"), ObjSearch.ReferenceNo.Replace("'", "''"), DateTime.Now.ToString("yyyyMMdd"), ConsumerID);

                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerMatchCustomMTN(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (ObjSearch.IsCustomVetting)
                {
                    if (String.IsNullOrEmpty(ObjSearch.IDno))
                    {
                        throw new Exception("ID Number is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.RequestNo.ToString()))
                    {
                        throw new Exception("RequestNo is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.FirstName))
                    {
                        throw new Exception("Customer Name is mandatory.");
                    }
                    else if (String.IsNullOrEmpty(ObjSearch.Surname))
                    {
                        throw new Exception("Customer Surname is mandatory.");
                    }
                }

                if ((String.IsNullOrEmpty(ObjSearch.IDno) || String.IsNullOrEmpty(ObjSearch.Passportno)) && (String.IsNullOrEmpty(ObjSearch.Surname)))
                {
                    throw new Exception("Your search input must have an Id Number and Surname or a Passport Number and Surname");
                }

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);

                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }

                ds = new DataSet("ListOfConsumers");


                string strSQL = string.Empty;

                    strSQL = string.Format("execute spConsumer_S_Match_Custom_MTN '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate, ObjSearch.subscriberID);
                
                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConsumerMatchCustomVettingD(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            int intAccountType = 0;
            try
            {

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                if (ObjSearch.IDType.ToUpper() != "SAID" && ObjSearch.IDType.ToUpper() != "PASSPORT")
                {
                    throw new Exception("Invalid ID Type");
                }
                else if ((String.IsNullOrEmpty(ObjSearch.IDno) || String.IsNullOrEmpty(ObjSearch.Passportno)) && (String.IsNullOrEmpty(ObjSearch.Surname)))
                {
                    throw new Exception("Your search input must have an Id Number and Lastname or a Passport Number and Lastname");
                }
                else if (String.IsNullOrEmpty(ObjSearch.CompanyName))
                {
                    throw new Exception("Company Name is mandatory.");
                }
                else if (String.IsNullOrEmpty(ObjSearch.Branch))
                {
                    throw new Exception("Branch is mandatory.");
                }
                else if (String.IsNullOrEmpty(ObjSearch.RuleSet))
                {
                    throw new Exception("RuleSet is mandatory.");
                }
                //else if (String.IsNullOrEmpty(ObjSearch.Title))
                //{
                //    throw new Exception("Title is mandatory.");
                //}
                else if (String.IsNullOrEmpty(ObjSearch.Gender))
                {
                    throw new Exception("Gender is mandatory.");
                }
                else if (ObjSearch.Gender.ToUpper() != "M" && ObjSearch.Gender.ToUpper() != "F" && ObjSearch.Gender.ToUpper() != "O")
                {
                    throw new Exception("Gender must be either M, F or O.");
                }
                //else if (String.IsNullOrEmpty(ObjSearch.AddressLine1))
                //{
                //    throw new Exception("AddressLine1 is mandatory.");
                //}
                //else if (String.IsNullOrEmpty(ObjSearch.Suburb) && String.IsNullOrEmpty(ObjSearch.City))
                //{
                //    throw new Exception("Either Suburb or City must be entered.");
                //}
                //else if (String.IsNullOrEmpty(ObjSearch.PhysicalPostalCode))
                //{
                //    throw new Exception("PhysicalPostalCode is mandatory.");
                //}
                //else if (String.IsNullOrEmpty(ObjSearch.ProvinceCode))
                //{
                //    throw new Exception("ProvinceCode is mandatory.");
                //}
                else if (String.IsNullOrEmpty(ObjSearch.OwnerTenant))
                {
                    throw new Exception("OwnerTenant is mandatory.");
                }
                else if (ObjSearch.OwnerTenant.ToUpper() != "O" && ObjSearch.OwnerTenant.ToUpper() != "T")
                {
                    throw new Exception("OwnerTenant must be either O or T.");
                }
                //else if (String.IsNullOrEmpty(ObjSearch.HomeTelephoneCode))
                //{
                //    throw new Exception("HomeTelephoneCode is mandatory.");
                //}
                //else if (String.IsNullOrEmpty(ObjSearch.HomePhoneNumber))
                //{
                //    throw new Exception("HomePhoneNumber is mandatory.");
                //}
                //else if (String.IsNullOrEmpty(ObjSearch.WorkTelephoneCode))
                //{
                //    throw new Exception("WorkTelephoneCode is mandatory.");
                //}
                else if (String.IsNullOrEmpty(ObjSearch.WorkPhoneNumber))
                {
                    throw new Exception("WorkPhoneNumber is mandatory.");
                }
                else if (String.IsNullOrEmpty(ObjSearch.YearsatCurrentEmployer))
                {
                    throw new Exception("Years at Current Employer is mandatory.");
                }
                else if (String.IsNullOrEmpty(ObjSearch.AccountType))
                {
                    throw new Exception("Bank AccountType is mandatory.");
                }
                else if (!int.TryParse(ObjSearch.AccountType, out intAccountType) || intAccountType < 0 || intAccountType > 3)
                {
                    throw new Exception("Bank AccountType must be 0,1,2 or 3.");
                }
                else if (String.IsNullOrEmpty(ObjSearch.Income))
                {
                    throw new Exception("Income is mandatory.");
                }
                //else if (String.IsNullOrEmpty(ObjSearch.SFID))
                //{
                //    throw new Exception("SFID is mandatory.");
                //}

                string strFirstInitial = "";
                string strSecondInitial = "";
                if (!(String.IsNullOrEmpty(ObjSearch.FirstName)))
                    strFirstInitial = ObjSearch.FirstName.Substring(0, 1);
                if (!(String.IsNullOrEmpty(ObjSearch.SecondName)))
                    strSecondInitial = ObjSearch.SecondName.Substring(0, 1);
                
                string strBirthDate = "";
                if (ObjSearch.DOB.Year > 1753)
                {                    
                    strBirthDate = ObjSearch.DOB.ToString("yyyy/MM/dd");
                }
                
                ds = new DataSet("ListOfConsumers");

                string strSQL = string.Format("execute spConsumer_S_Match_Custom '{0}', '{1}','{2}','{3}', '{4}','{5}','{6}','{7}', '{8}','{9}'", ObjSearch.IDno.Replace("'", "''"), ObjSearch.Passportno.Replace("'", "''"), strFirstInitial, strSecondInitial, ObjSearch.FirstName.Replace("'", "''"), ObjSearch.SecondName.Replace("'", "''"), ObjSearch.Surname.Replace("'", "''"), ObjSearch.MaidenName.Replace("'", "''"), strBirthDate, ObjSearch.subscriberID);
                ProcessReport(strSQL);


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                ObjResponse = ConsumerMatch(ObjResponse, ObjSearch.BonusCheck, ObjSearch.DataSegments);

                dtError.Clear();
                dtError.Dispose();
                ds.Clear();
                ds.Dispose();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response NLRRegistration(XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlConnection con = new SqlConnection(strCon);
                SqlCommand objcmd = new SqlCommand("sp_I_NLRDailyRegistrations", con);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;

                objcmd.Parameters.AddWithValue("@TransactionID", ObjSearch.TransactionID);
                objcmd.Parameters.AddWithValue("@TransactionType", ObjSearch.TransactionType);
                objcmd.Parameters.AddWithValue("@EnquiryReason1", ObjSearch.EnquiryReason1);
                objcmd.Parameters.AddWithValue("@SubscriberCode", ObjSearch.SubscriberCode);
                objcmd.Parameters.AddWithValue("@SubscriberPassword", ObjSearch.SubscriberPassword);
                objcmd.Parameters.AddWithValue("@SubscriberGroupID", ObjSearch.SubscriberGroupID);
                objcmd.Parameters.AddWithValue("@SubscriberOperatorID", ObjSearch.SubscriberOperatorID);
                objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", ObjSearch.SubscriberOperatorPassword);
                objcmd.Parameters.AddWithValue("@TransactionStatus", ObjSearch.TransactionStatus);
                objcmd.Parameters.AddWithValue("@ErrorCode", ObjSearch.ErrorCode);
                objcmd.Parameters.AddWithValue("@MatchedRecordsFound", ObjSearch.MatchedRecordsFound);
                objcmd.Parameters.AddWithValue("@ConsIdentificationNo", ObjSearch.ConsIdentificationNo);
                objcmd.Parameters.AddWithValue("@DateEnquirySent", ObjSearch.DateEnquirySent);
                objcmd.Parameters.AddWithValue("@TimeEnquirySent", ObjSearch.TimeEnquirySent);
                objcmd.Parameters.AddWithValue("@EchoIdentityNo", ObjSearch.EchoIdentityNo);
                objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", ObjSearch.EchoUniqueClientNo);
                objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", ObjSearch.MaxNoOfInquiries);
                objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", ObjSearch.MaxNoOfNLRAccounts);
                objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", ObjSearch.MaxNoOfAssAddresses);
                objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", ObjSearch.MaxNoOfOtherEmployers);
                objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", ObjSearch.MaxNoOfAssHomeTelNos);
                objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", ObjSearch.MaxNoOfAssBusTelNos);
                objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", ObjSearch.MaxNoOfAssCellNos);
                objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", ObjSearch.MaxNoOfAssOtherTelNos);
                objcmd.Parameters.AddWithValue("@LoanRegNo", ObjSearch.LoanRegNo);
                objcmd.Parameters.AddWithValue("@SupplierRefNo", ObjSearch.SupplierRefNo);
                objcmd.Parameters.AddWithValue("@EnquiryRefNo", ObjSearch.EnquiryRefNo);
                objcmd.Parameters.AddWithValue("@ConsSurname", ObjSearch.ConsSurname);
                objcmd.Parameters.AddWithValue("@Forename1", ObjSearch.Forename1);
                objcmd.Parameters.AddWithValue("@Forename2", ObjSearch.Forename2);
                objcmd.Parameters.AddWithValue("@Forename3", ObjSearch.Forename3);
                objcmd.Parameters.AddWithValue("@SpouseCurrSurname", ObjSearch.SpouseCurrSurname);
                objcmd.Parameters.AddWithValue("@SpouseForename1", ObjSearch.SpouseForename1);
                objcmd.Parameters.AddWithValue("@SpouseForename2", ObjSearch.SpouseForename2);
                objcmd.Parameters.AddWithValue("@ConsBirthDate", ObjSearch.ConsBirthDate);
                objcmd.Parameters.AddWithValue("@ConsIdentityNo", ObjSearch.ConsIdentityNo);
                objcmd.Parameters.AddWithValue("@CurrAddrLn1", ObjSearch.CurrAddrLn1);
                objcmd.Parameters.AddWithValue("@CurrAddrLn2", ObjSearch.CurrAddrLn2);
                objcmd.Parameters.AddWithValue("@CurrAddrLn3", ObjSearch.CurrAddrLn3);
                objcmd.Parameters.AddWithValue("@CurrAddrLn4", ObjSearch.CurrAddrLn4);
                objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", ObjSearch.CurrAddressPostalCode);
                objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", ObjSearch.PeriodAtCurrAddress);
                objcmd.Parameters.AddWithValue("@OtherAddrLn1", ObjSearch.OtherAddrLn1);
                objcmd.Parameters.AddWithValue("@OtherAddrLn2", ObjSearch.OtherAddrLn2);
                objcmd.Parameters.AddWithValue("@OtherAddrLn3", ObjSearch.OtherAddrLn3);
                objcmd.Parameters.AddWithValue("@OtherAddrLn4", ObjSearch.OtherAddrLn4);
                objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", ObjSearch.OtherAddressPostalCode);
                objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", ObjSearch.OwnerTenantAtCurrAddress);
                objcmd.Parameters.AddWithValue("@ConsOccupation", ObjSearch.ConsOccupation);
                objcmd.Parameters.AddWithValue("@ConsEmployer", ObjSearch.ConsEmployer);
                objcmd.Parameters.AddWithValue("@ConsMaidenName", ObjSearch.ConsMaidenName);
                objcmd.Parameters.AddWithValue("@ConsAliasName", ObjSearch.ConsAliasName);
                objcmd.Parameters.AddWithValue("@Gender", ObjSearch.Gender);
                objcmd.Parameters.AddWithValue("@ConsMaritalStatus", ObjSearch.ConsMaritalStatus);
                objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", ObjSearch.ConsHomeTelDiallingCode);
                objcmd.Parameters.AddWithValue("@ConsHomeTelNo", ObjSearch.ConsHomeTelNo);
                objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", ObjSearch.ConsWorkTelDiallingCode);
                objcmd.Parameters.AddWithValue("@ConsWorkTelNo", ObjSearch.ConsWorkTelNo);
                objcmd.Parameters.AddWithValue("@ConsCellularTelNo", ObjSearch.ConsCellularTelNo);
                objcmd.Parameters.AddWithValue("@LoanAmt1", ObjSearch.LoanAmt1);
                objcmd.Parameters.AddWithValue("@InstalmentAmt", ObjSearch.InstalmentAmt);
                objcmd.Parameters.AddWithValue("@MonthlySalary", ObjSearch.MonthlySalary);
                objcmd.Parameters.AddWithValue("@SalaryFrequency", ObjSearch.SalaryFrequency);
                objcmd.Parameters.AddWithValue("@RepaymentPeriod1", ObjSearch.RepaymentPeriod1);
                objcmd.Parameters.AddWithValue("@EnquiryReason2", ObjSearch.EnquiryReason2);
                objcmd.Parameters.AddWithValue("@BranchCode", ObjSearch.BranchCode);
                objcmd.Parameters.AddWithValue("@AccountNo", ObjSearch.AccountNo);
                objcmd.Parameters.AddWithValue("@SubAccountNo", ObjSearch.SubAccountNo);
                objcmd.Parameters.AddWithValue("@LoanType", ObjSearch.LoanType);
                objcmd.Parameters.AddWithValue("@DateLoanDisbursed", ObjSearch.DateLoanDisbursed);
                objcmd.Parameters.AddWithValue("@LoanAmt2", ObjSearch.LoanAmt2);
                objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", ObjSearch.LoanAmtBalanceIndicator);
                objcmd.Parameters.AddWithValue("@CurrBalance", ObjSearch.CurrBalance);
                objcmd.Parameters.AddWithValue("@CurrBalanceIndicator", ObjSearch.CurrBalanceIndicator);
                objcmd.Parameters.AddWithValue("@MonthlyInstalment", ObjSearch.MonthlyInstalment);
                objcmd.Parameters.AddWithValue("@LoadIndicator", ObjSearch.LoadIndicator);
                objcmd.Parameters.AddWithValue("@RepaymentPeriod2", ObjSearch.RepaymentPeriod2);
                objcmd.Parameters.AddWithValue("@LoanPurpose", ObjSearch.LoanPurpose);
                objcmd.Parameters.AddWithValue("@TotalAmtRepayable", ObjSearch.TotalAmtRepayable);
                objcmd.Parameters.AddWithValue("@InterestType", ObjSearch.InterestType);
                objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", ObjSearch.AnnualRateForTotalChargeOfCredit);
                objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", ObjSearch.RandValueOfInterestCharges);
                objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", ObjSearch.RandValueOfTotalChargeOfCredit);
                objcmd.Parameters.AddWithValue("@SettlementAmt", ObjSearch.SettlementAmt);
                objcmd.Parameters.AddWithValue("@ReAdvanceIndicator", ObjSearch.ReAdvanceIndicator);
                objcmd.Parameters.AddWithValue("@ConsumerID", ObjSearch.ConsumerID);
                objcmd.Parameters.AddWithValue("@SubscriberID", ObjSearch.SubscriberID);
                objcmd.Parameters.AddWithValue("@CreatedByUser", ObjSearch.CreatedByUser);
                objcmd.Parameters.AddWithValue("@PassportNo", ObjSearch.PassportNo);

                SqlDataAdapter ObjDA = new SqlDataAdapter(objcmd);
                DataSet objds = new DataSet();
                ObjDA.Fill(objds);

                foreach (DataTable dt in objds.Tables)
                {
                    dt.TableName = "Result";
                }
                foreach (DataRow dr in objds.Tables[0].Rows)
                {
                    DataColumn dc = new DataColumn("Status");
                    objds.Tables[0].Columns.Add(dc);
                    dr["Status"] = "Loan Registered Successfully";

                }

                objds.DataSetName = "NLRRegistrations";
                string reference = objds.Tables[0].Rows[0]["NLRDailyRegistrationID"].ToString();

                objds.Tables[0].Columns.Remove("NLRDailyRegistrationID");

                ObjResponse.ResponseKey = int.Parse(reference);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = objds.GetXml();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response NLRClosure(XDSPortalLibrary.Entity_Layer.NLRDailyClosures ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlConnection con = new SqlConnection(strCon);
                SqlCommand objcmd = new SqlCommand("sp_I_NLRDailyClosures", con);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;

                objcmd.Parameters.AddWithValue("@TransactionID", ObjSearch.TransactionID);
                objcmd.Parameters.AddWithValue("@TransactionType", ObjSearch.TransactionType);
                objcmd.Parameters.AddWithValue("@SubscriberDataFileCode", ObjSearch.SubscriberDataFileCode);
                objcmd.Parameters.AddWithValue("@SubscriberDataFilePassword", ObjSearch.SubscriberDataFilePassword);
                objcmd.Parameters.AddWithValue("@SubscriberDataFileGroupID", ObjSearch.SubscriberDataFileGroupID);
                objcmd.Parameters.AddWithValue("@SubscriberDataFileOperatorID", ObjSearch.SubscriberDataFileOperatorID);
                objcmd.Parameters.AddWithValue("@SubscriberDataFilesOperatorPassword", ObjSearch.SubscriberDataFilesOperatorPassword);
                objcmd.Parameters.AddWithValue("@NLRLoanRegistrationNumber", ObjSearch.NLRLoanRegistrationNumber);
                objcmd.Parameters.AddWithValue("@NLREnquiryReferenceNumber", ObjSearch.NLREnquiryReferenceNumber);
                objcmd.Parameters.AddWithValue("@SupplierRefNo", ObjSearch.SupplierRefNo);
                objcmd.Parameters.AddWithValue("@Surname", ObjSearch.Surname);
                objcmd.Parameters.AddWithValue("@Forename1", ObjSearch.Forename1);
                objcmd.Parameters.AddWithValue("@Forename2", ObjSearch.Forename2);
                objcmd.Parameters.AddWithValue("@Forename3", ObjSearch.Forename3);
                objcmd.Parameters.AddWithValue("@DateOfBirth", ObjSearch.DateOfBirth);
                objcmd.Parameters.AddWithValue("@BranchCode", ObjSearch.BranchCode);
                objcmd.Parameters.AddWithValue("@AccountNo", ObjSearch.AccountNumber);
                objcmd.Parameters.AddWithValue("@SubAccountNo", ObjSearch.SubAccountNumber);
                objcmd.Parameters.AddWithValue("@StatusCode", ObjSearch.StatusCode);
                objcmd.Parameters.AddWithValue("@DateOfClosureCancellation", ObjSearch.DateOfClosureCancellation);
                objcmd.Parameters.AddWithValue("@TransactionStatus", ObjSearch.TransactionStatus);
                objcmd.Parameters.AddWithValue("@ErrorCode", ObjSearch.ErrorCode);
                objcmd.Parameters.AddWithValue("@ClosureDate", ObjSearch.ClosureDate);
                objcmd.Parameters.AddWithValue("@ClosureTime", ObjSearch.ClosureTime);
                objcmd.Parameters.AddWithValue("@ConsumerID", ObjSearch.ConsumerID);
                objcmd.Parameters.AddWithValue("@SubscriberID", ObjSearch.SubscriberID);
                objcmd.Parameters.AddWithValue("@CreatedByUser", ObjSearch.CreatedByUser);
                objcmd.Parameters.AddWithValue("@ConsIdentityNo", ObjSearch.SAIDNumber);
                objcmd.Parameters.AddWithValue("@PassportNo", ObjSearch.PassportNo);


                SqlDataAdapter ObjDA = new SqlDataAdapter(objcmd);
                DataSet objds = new DataSet();
                ObjDA.Fill(objds);

                foreach (DataTable dt in objds.Tables)
                {
                    dt.TableName = "Result";
                }
                foreach (DataRow dr in objds.Tables[0].Rows)
                {
                    DataColumn dc = new DataColumn("Status");
                    objds.Tables[0].Columns.Add(dc);
                    dr["Status"] = "Loan Closed Successfully";
                }

                objds.DataSetName = "NLRClosures";
                string reference = objds.Tables[0].Rows[0]["NLRDailyClosureID"].ToString();

                objds.Tables[0].Columns.Remove("NLRDailyClosureID");

                ObjResponse.ResponseKey = int.Parse(reference);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = objds.GetXml();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CellcScoreCard(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");


                string strSQL = string.Format("execute spCellCScoreCard {0}, {1},{2},{3},'{4}','{5}','{6}',{7},'{8}','{9}','{10}','{11}','{12}','{13}','{14}'", ObjSearch.ConsumerID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID, ObjSearch.AccountType.ToString(), ObjSearch.BankName.ToString(), ObjSearch.PostalCode.ToString(), ObjSearch.GrossMonthlyIncome.ToString(), ObjSearch.CreatedbyUser, ObjSearch.SFID, ObjSearch.IDno, ObjSearch.Passportno, ObjSearch.IDType, string.IsNullOrEmpty(ObjSearch.Title)?string.Empty: ObjSearch.Title.Replace("'","''"), ObjSearch.DOB.ToString("yyyyMMdd"));
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                //ds.Tables[0].TableName = "CellCScoreCard";
                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public OutputData CellcFMP(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, string Username, string Password, string strFMPLoginURL, string strFMPMatchURL, out string FraudRules)
        {
            OutputData FMPResult = new OutputData();
            FraudRules = "";

            try
            {
                //FMP for all applications
                InputData testInput = new InputData();

                testInput.SubscriberID = 1;
                testInput.CustomerTypeID = 1;
                testInput.AccountTypeID = 1;
                testInput.BusinessProcessID = 2;
                testInput.TransactionTypeID = 1;
                testInput.ProductID = 1;
                testInput.ApplicationDateTime = DateTime.Now;
                testInput.FirstName = ObjSearch.FirstName;
                testInput.LastName = ObjSearch.Surname;
                if (ObjSearch.IDType.ToString().ToUpper() == "SAID")
                {
                    testInput.IdentificationTypeID = 1;
                }
                else
                {
                    testInput.IdentificationTypeID = 2;
                }

                testInput.IDNumber = ObjSearch.IDno;

                testInput.MonthlyIncome = Convert.ToDouble(ObjSearch.Income);

                testInput.SystemUsername = ObjSearch.UserName;
                testInput.DateofBirth = ObjSearch.DOB;
                testInput.HomeTelephoneISDCode = ObjSearch.HomeTelephoneCode;
                testInput.HomeTelephoneNumber = ObjSearch.HomePhoneNumber;
                testInput.WorkTelephoneISDCode = ObjSearch.WorkTelephoneCode;
                testInput.WorkTelephoneNumber = ObjSearch.WorkPhoneNumber;
                testInput.ResidentialAddress1 = ObjSearch.AddressLine1;
                testInput.ResidentialAddress2 = ObjSearch.AddressLine2;
                testInput.ResidentialAddress3 = ObjSearch.Suburb;
                testInput.ResidentialAddress4 = ObjSearch.City;
                testInput.ResidentialAddressPostalCode = ObjSearch.PhysicalPostalCode;
                testInput.PostalAddress1 = ObjSearch.PostalAddressLine1;
                testInput.PostalAddress2 = ObjSearch.PostalAddressLine2;
                testInput.PostalAddress3 = ObjSearch.PostalSuburb;
                testInput.PostalAddress4 = ObjSearch.PostalCity;
                testInput.PostalCode = ObjSearch.PostalCode;
                //testInput.BankName = ObjSearch.BankName;
                //testInput.BankBranchCode = ObjSearch.BranchCode;
                if (ObjSearch.Gender.ToString().ToUpper() == "M")
                {
                    testInput.GenderID = 1;
                }
                else
                {
                    testInput.GenderID = 2;
                }

                //testInput.BankAccountNumber = ObjSearch.BankAccountNumber;
                //testInput.FingerprintAuthenticationNeeded = false;
                //testInput.FingerprintAuthenticationPassed = false;
                //testInput.FingerprintAuthenticationSystemError = false;
                //testInput.FacialRecognitionNeeded = false;
                //testInput.FacialRecognitionPassed = false;
                //testInput.FacialRecognitionSystemError = false;
                //testInput.VoiceRecognitionNeeded = false;
                //testInput.VoiceRecognitionPassed = false;
                //testInput.VoiceRecognitionSystemError = false;
                //testInput.KBANeeded = false;
                //testInput.KBAPassed = false;
                //testInput.KBASystemError = false;
                //testInput.KBAAttempts = 0;
                //testInput.AVSNeeded = false;
                //testInput.AVSPassed = false;
                //testInput.AVSSystemError = false;
                //testInput.OtherAuthenticationNeeded = false;
                //testInput.OtherAuthenticationPassed = false;
                //testInput.OtherAuthenticationSystemError = false;
                //testInput.OtherauthenticationType = "OtherauthenticationType";
                //testInput.CreditVettingDecision = "Approved";
                //testInput.CreditVettingAuthenticationUser = "CreditVettingAuthenticationUser";
                //testInput.DeviceMasked = false;
                //testInput.DeviceMacAddress = "DeviceMacAddress";
                //testInput.GPSLocationCoordinates = "GPSLocationCoordinates";

                UriBuilder uriBuilder = new UriBuilder(strFMPLoginURL);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["Username"] = Username;
                query["Password"] = Password;
                uriBuilder.Query = query.ToString();
                string uri = uriBuilder.Uri.ToString();

                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync(uri).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;

                        ConnectTicket connectTicket = JsonConvert.DeserializeObject<ConnectTicket>(data);

                        testInput.ConnectTicket = connectTicket.Value;

                        using (WebClient client1 = new WebClient())
                        {
                            client1.Headers[HttpRequestHeader.ContentType] = "application/json";
                            var jsonObj = JsonConvert.SerializeObject(testInput);
                            var dataString = client1.UploadString(strFMPMatchURL, jsonObj);
                            FMPResult = JsonConvert.DeserializeObject<OutputData>(dataString);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            //FraudRules = string.Join(", ", FMPResult.FraudRuleTriggered);

            return FMPResult;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetVodacomCustomVetting(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsResult = new DataSet("XDSConnectResponse");

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                DataTable dtHeader = new DataTable("header");
                dtHeader.Columns.Add("ResultCode", typeof(int));
                dtHeader.Columns.Add("ErrorCode", typeof(String));
                dtHeader.Columns.Add("Message", typeof(String));
                dtHeader.Columns.Add("DateTime", typeof(DateTime));

                DataTable dtScoreCard = new DataTable("Results");
                dtScoreCard.Columns.Add("RequestNo", typeof(int));
                dtScoreCard.Columns.Add("IDNumber", typeof(String));
                //dtScoreCard.Columns.Add("PassportNo", typeof(String));
                dtScoreCard.Columns.Add("CustomerName", typeof(String));
                dtScoreCard.Columns.Add("CustomerSurname", typeof(String));
                dtScoreCard.Columns.Add("RequestMSISDN", typeof(int));
                dtScoreCard.Columns.Add("ContactNo", typeof(String));
                dtScoreCard.Columns.Add("Cell", typeof(String));
                dtScoreCard.Columns.Add("Total_Enquiries_Ever", typeof(int));
                dtScoreCard.Columns.Add("Total_Enquiries_Own", typeof(int));
                dtScoreCard.Columns.Add("Enq_30_Days_Other", typeof(int));
                dtScoreCard.Columns.Add("Enq_90_Days_Other", typeof(int));
                dtScoreCard.Columns.Add("Total_Judgements_Ever", typeof(int));
                dtScoreCard.Columns.Add("Total_Judgements_Value", typeof(int));
                dtScoreCard.Columns.Add("Total_Defaults_Ever", typeof(int));
                dtScoreCard.Columns.Add("Total_Defaults_Value", typeof(int));
                dtScoreCard.Columns.Add("PP_Total_Active", typeof(int));
                dtScoreCard.Columns.Add("PP_Total_Own", typeof(int));
                dtScoreCard.Columns.Add("PP_Most_Recent", typeof(int));
                dtScoreCard.Columns.Add("PP_Worst_Ever", typeof(int));
                dtScoreCard.Columns.Add("PP_Worst6_Months", typeof(int));
                dtScoreCard.Columns.Add("PP_Worst12_Months", typeof(int));
                dtScoreCard.Columns.Add("PP_Line_status_D", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_F", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_I", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_J", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_L", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_N", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_R", typeof(String));
                dtScoreCard.Columns.Add("PP_Line_status_W", typeof(String));
                dtScoreCard.Columns.Add("Total_Notices", typeof(int));
                dtScoreCard.Columns.Add("TotalRehabilitations", typeof(int));
                dtScoreCard.Columns.Add("TotalAdminOrders", typeof(int));
                dtScoreCard.Columns.Add("TotalIntentionToSurrender", typeof(int));
                dtScoreCard.Columns.Add("TotalSequestrations", typeof(int));
                dtScoreCard.Columns.Add("FirstAddressLine1", typeof(String));
                dtScoreCard.Columns.Add("FirstAddressLine2", typeof(String));
                dtScoreCard.Columns.Add("FirstAddressLine3", typeof(String));
                dtScoreCard.Columns.Add("FirstAddressLine4", typeof(String));
                dtScoreCard.Columns.Add("FirstAddressPostCode", typeof(String));
                dtScoreCard.Columns.Add("HomeTel", typeof(String));
                dtScoreCard.Columns.Add("WorkTel", typeof(String));
                dtScoreCard.Columns.Add("CellPhone", typeof(String));
                dtScoreCard.Columns.Add("OTH100ALL", typeof(String));
                dtScoreCard.Columns.Add("OTH101ALL", typeof(String));
                dtScoreCard.Columns.Add("OTH102ALL", typeof(int));
                dtScoreCard.Columns.Add("IDVerified", typeof(String));
                dtScoreCard.Columns.Add("OTH002ALL", typeof(String));
                dtScoreCard.Columns.Add("OTH001ALL", typeof(String));
                dtScoreCard.Columns.Add("LEG004ALL", typeof(int));
                dtScoreCard.Columns.Add("LEG001ALL", typeof(int));
                dtScoreCard.Columns.Add("LEG100ALL", typeof(int));
                dtScoreCard.Columns.Add("LEG103ALL", typeof(int));
                dtScoreCard.Columns.Add("LEG107ALL", typeof(int));
                dtScoreCard.Columns.Add("ADV009ALL", typeof(int));
                dtScoreCard.Columns.Add("ADV010ALL", typeof(int));
                dtScoreCard.Columns.Add("ADV013ALL", typeof(int));
                dtScoreCard.Columns.Add("CPA217ALL", typeof(int));
                dtScoreCard.Columns.Add("CPA211ALL", typeof(int));
                dtScoreCard.Columns.Add("NLR217ALL", typeof(int));
                dtScoreCard.Columns.Add("NLR211ALL", typeof(int));
                dtScoreCard.Columns.Add("CPA002ALL", typeof(int));
                dtScoreCard.Columns.Add("CPA100ALL", typeof(int));
                dtScoreCard.Columns.Add("XDSPresageAOScore", typeof(int));
                dtScoreCard.Columns.Add("XDSPresageAOExclusionReason", typeof(String));
                dtScoreCard.Columns.Add("DMA_IND", typeof(String));
                dtScoreCard.Columns.Add("FraudIndicator", typeof(String));

                DataRow drHeader;
                drHeader = dtHeader.NewRow();

                drHeader["ResultCode"] = 2;
                drHeader["ErrorCode"] = string.Empty;
                drHeader["Message"] = string.Empty;
                drHeader["DateTime"] = DateTime.Now;
                //drScorecard["DateTime"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                DataRow drScorecard;
                drScorecard = dtScoreCard.NewRow();

                drScorecard["RequestNo"] = ObjSearch.RequestNo;
                drScorecard["IDNumber"] = string.Empty;
                //drScorecard["PassportNo"] = string.Empty;
                drScorecard["CustomerName"] = string.Empty;
                drScorecard["CustomerSurname"] = string.Empty;
                drScorecard["RequestMSISDN"] = ObjSearch.RequestMSISDN;
                drScorecard["ContactNo"] = ObjSearch.ContactNo;
                drScorecard["Cell"] = string.Empty;
                drScorecard["Total_Enquiries_Ever"] = 0;
                drScorecard["Total_Enquiries_Own"] = 0;
                drScorecard["Enq_30_Days_Other"] = 0;
                drScorecard["Enq_90_Days_Other"] = 0;
                drScorecard["Total_Judgements_Ever"] = 0;
                drScorecard["Total_Judgements_Value"] = 0;
                drScorecard["Total_Defaults_Ever"] = 0;
                drScorecard["Total_Defaults_Value"] = 0;
                drScorecard["PP_Total_Active"] = 0;
                drScorecard["PP_Total_Own"] = 0;
                drScorecard["PP_Most_Recent"] = 0;
                drScorecard["PP_Worst_Ever"] = 0;
                drScorecard["PP_Worst6_Months"] = 0;
                drScorecard["PP_Worst12_Months"] = 0;
                drScorecard["PP_Line_status_D"] = string.Empty;
                drScorecard["PP_Line_status_F"] = string.Empty;
                drScorecard["PP_Line_status_I"] = string.Empty;
                drScorecard["PP_Line_status_J"] = string.Empty;
                drScorecard["PP_Line_status_L"] = string.Empty;
                drScorecard["PP_Line_status_N"] = string.Empty;
                drScorecard["PP_Line_status_R"] = string.Empty;
                drScorecard["PP_Line_status_W"] = string.Empty;
                drScorecard["Total_Notices"] = 0;
                drScorecard["TotalRehabilitations"] = 0;
                drScorecard["TotalAdminOrders"] = 0;
                drScorecard["TotalIntentionToSurrender"] = 0;
                drScorecard["TotalSequestrations"] = 0;
                drScorecard["FirstAddressLine1"] = string.Empty;
                drScorecard["FirstAddressLine2"] = string.Empty;
                drScorecard["FirstAddressLine3"] = string.Empty;
                drScorecard["FirstAddressLine4"] = string.Empty;
                drScorecard["FirstAddressPostCode"] = string.Empty;
                drScorecard["HomeTel"] = string.Empty;
                drScorecard["WorkTel"] = string.Empty;
                drScorecard["CellPhone"] = string.Empty;
                drScorecard["OTH100ALL"] = string.Empty;
                drScorecard["OTH101ALL"] = string.Empty;
                drScorecard["OTH102ALL"] = 0;
                drScorecard["IDVerified"] = string.Empty;
                drScorecard["OTH002ALL"] = string.Empty;
                drScorecard["OTH001ALL"] = string.Empty;
                drScorecard["LEG004ALL"] = 0;
                drScorecard["LEG001ALL"] = 0;
                drScorecard["LEG100ALL"] = 0;
                drScorecard["LEG103ALL"] = 0;
                drScorecard["LEG107ALL"] = 0;
                drScorecard["ADV009ALL"] = 0;
                drScorecard["ADV010ALL"] = 0;
                drScorecard["ADV013ALL"] = 0;
                drScorecard["CPA217ALL"] = 0;
                drScorecard["CPA211ALL"] = 0;
                drScorecard["NLR217ALL"] = 0;
                drScorecard["NLR211ALL"] = 0;
                drScorecard["CPA002ALL"] = 0;
                drScorecard["CPA100ALL"] = 0;
                drScorecard["XDSPresageAOScore"] = 0;
                drScorecard["XDSPresageAOExclusionReason"] = string.Empty;
                drScorecard["DMA_IND"] = string.Empty;
                drScorecard["FraudIndicator"] = string.Empty;

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //if (dtError.Rows.Count > 0)
                //{
                //    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //}

                Thread thVodacom = new Thread(new ParameterizedThreadStart(ProcessReport));
                thVodacom.Start(string.Format("execute [dbo].[spVodacomCustomVetting] {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID.ToString()));
                thVodacom.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];

                    drScorecard["RequestNo"] = ObjSearch.RequestNo.ToString();
                    drScorecard["IDNumber"] = dr["IDNumber"].ToString();
                    //drScorecard["PassportNo"] = dr["PassportNo"].ToString();
                    drScorecard["CustomerName"] = dr["CustomerName"].ToString();
                    drScorecard["CustomerSurname"] = dr["CustomerSurname"].ToString();
                    drScorecard["RequestMSISDN"] = ObjSearch.RequestMSISDN.ToString();
                    drScorecard["ContactNo"] = ObjSearch.ContactNo.ToString();
                    drScorecard["Cell"] = dr["Cell"].ToString();
                    drScorecard["Total_Enquiries_Ever"] = dr["Total_Enquiries_Ever"].ToString();
                    drScorecard["Total_Enquiries_Own"] = dr["Total_Enquiries_Own"].ToString();
                    drScorecard["Enq_30_Days_Other"] = dr["Enq_30_Days_Other"].ToString();
                    drScorecard["Enq_90_Days_Other"] = dr["Enq_90_Days_Other"].ToString();
                    drScorecard["Total_Judgements_Ever"] = dr["Total_Judgements_Ever"].ToString();
                    drScorecard["Total_Judgements_Value"] = dr["Total_Judgements_Value"].ToString();
                    drScorecard["Total_Defaults_Ever"] = dr["Total_Defaults_Ever"].ToString();
                    drScorecard["Total_Defaults_Value"] = dr["Total_Defaults_Value"].ToString();
                    drScorecard["PP_Total_Active"] = dr["PP_Total_Active"].ToString();
                    drScorecard["PP_Total_Own"] = dr["PP_Total_Own"].ToString();
                    drScorecard["PP_Most_Recent"] = dr["PP_Most_Recent"].ToString();
                    drScorecard["PP_Worst_Ever"] = dr["PP_Worst_Ever"].ToString();
                    drScorecard["PP_Worst6_Months"] = dr["PP_Worst6_Months"].ToString();
                    drScorecard["PP_Worst12_Months"] = dr["PP_Worst12_Months"].ToString();
                    drScorecard["PP_Line_status_D"] = dr["PP_Line_status_D"].ToString();
                    drScorecard["PP_Line_status_F"] = dr["PP_Line_status_F"].ToString();
                    drScorecard["PP_Line_status_I"] = dr["PP_Line_status_I"].ToString();
                    drScorecard["PP_Line_status_J"] = dr["PP_Line_status_J"].ToString();
                    drScorecard["PP_Line_status_L"] = dr["PP_Line_status_L"].ToString();
                    drScorecard["PP_Line_status_N"] = dr["PP_Line_status_N"].ToString();
                    drScorecard["PP_Line_status_R"] = dr["PP_Line_status_R"].ToString();
                    drScorecard["PP_Line_status_W"] = dr["PP_Line_status_W"].ToString();
                    drScorecard["Total_Notices"] = dr["Total_Notices"].ToString();
                    drScorecard["TotalRehabilitations"] = dr["TotalRehabilitations"].ToString();
                    drScorecard["TotalAdminOrders"] = dr["TotalAdminOrders"].ToString();
                    drScorecard["TotalIntentionToSurrender"] = dr["TotalIntentionToSurrender"].ToString();
                    drScorecard["TotalSequestrations"] = dr["TotalSequestrations"].ToString();
                    drScorecard["FirstAddressLine1"] = dr["FirstAddressLine1"].ToString();
                    drScorecard["FirstAddressLine2"] = dr["FirstAddressLine2"].ToString();
                    drScorecard["FirstAddressLine3"] = dr["FirstAddressLine3"].ToString();
                    drScorecard["FirstAddressLine4"] = dr["FirstAddressLine4"].ToString();
                    drScorecard["FirstAddressPostCode"] = dr["FirstAddressPostCode"].ToString();
                    drScorecard["HomeTel"] = dr["HomeTel"].ToString();
                    drScorecard["WorkTel"] = dr["WorkTel"].ToString();
                    drScorecard["CellPhone"] = dr["CellPhone"].ToString();
                    drScorecard["OTH100ALL"] = dr["OTH100ALL"].ToString();
                    drScorecard["OTH101ALL"] = dr["OTH101ALL"].ToString();
                    drScorecard["OTH102ALL"] = dr["OTH102ALL"].ToString();
                    drScorecard["IDVerified"] = dr["IDVerified"].ToString();
                    drScorecard["OTH002ALL"] = dr["OTH002ALL"].ToString();
                    drScorecard["OTH001ALL"] = dr["OTH001ALL"].ToString();
                    drScorecard["LEG004ALL"] = dr["LEG004ALL"].ToString();
                    drScorecard["LEG001ALL"] = dr["LEG001ALL"].ToString();
                    drScorecard["LEG100ALL"] = dr["LEG100ALL"].ToString();
                    drScorecard["LEG103ALL"] = dr["LEG103ALL"].ToString();
                    drScorecard["LEG107ALL"] = dr["LEG107ALL"].ToString();
                    drScorecard["ADV009ALL"] = dr["ADV009ALL"].ToString();
                    drScorecard["ADV010ALL"] = dr["ADV010ALL"].ToString();
                    drScorecard["ADV013ALL"] = dr["ADV013ALL"].ToString();
                    drScorecard["CPA217ALL"] = dr["CPA217ALL"].ToString();
                    drScorecard["CPA211ALL"] = dr["CPA211ALL"].ToString();
                    drScorecard["NLR217ALL"] = dr["NLR217ALL"].ToString();
                    drScorecard["NLR211ALL"] = dr["NLR211ALL"].ToString();
                    drScorecard["CPA002ALL"] = dr["CPA002ALL"].ToString();
                    drScorecard["CPA100ALL"] = dr["CPA100ALL"].ToString();
                    drScorecard["XDSPresageAOScore"] = dr["XDSPresageAOScore"].ToString();
                    drScorecard["XDSPresageAOExclusionReason"] = dr["XDSPresageAOExclusionReason"].ToString();
                    drScorecard["DMA_IND"] = dr["DMA_IND"].ToString();
                    drScorecard["FraudIndicator"] = dr["FraudIndicator"].ToString();
                    drHeader["ResultCode"] = 0;
                    drHeader["ErrorCode"] = "";
                    drHeader["Message"] = "Successful";
                    drHeader["DateTime"] = DateTime.Now;
                }

                dtHeader.Rows.Add(drHeader);
                dtScoreCard.Rows.Add(drScorecard);

                dsResult.Tables.Add(dtHeader);
                dsResult.Tables.Add(dtScoreCard);

                string xml = dsResult.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                //dtReportInformation.Clear();
                dtError.Clear();
                ds.Clear();
                //dsReport.Clear();

                //dtReportInformation.Dispose();
                dtError.Dispose();
                ds.Dispose();
                //dsReport.Dispose();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public bool ConsumerFootprintMatch(int KeyID, int SubscriberID, int ProductID, int SubscriberEnquiryResultID)
        {
            bool bResult;
            ds = new DataSet("Vodacom");

            Thread thVodacom = new Thread(new ParameterizedThreadStart(ProcessReport));
            thVodacom.Start(string.Format("execute spVodacomFootprintMatch {0}, {1}, {2}, {3}", KeyID, SubscriberID, ProductID, SubscriberEnquiryResultID));
            thVodacom.Join();

            if (dtError.Rows.Count > 0)
            {
                throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
            }

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                bResult = true;
            else
                bResult = false;

            dtError.Clear();
            ds.Clear();

            dtError.Dispose();
            ds.Dispose();

            return bResult;
        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingG(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");


                string strSQL = string.Format("execute spCustomVettingG {0}, {1},{2},{3},{4},{5},'{6}','{7}','{8}'", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID, ObjSearch.ConsumerID.ToString(), ObjSearch.GrossMonthlyIncome.ToString(), ObjSearch.UserName.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                //ds.Tables[0].TableName = "CellCScoreCard";
                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingH(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCustomVettingH {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}, {10}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 63, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false, ObjSearch.GrossMonthlyIncome.ToString());
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetCollectionTraceReport(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string strReportID = "";

            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //    if (dtError.Rows.Count > 0)
                //    {
                //        throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //    }


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPmtGAP = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));


                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                }
                thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.UserName));
                thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.UserName, ObjSearch.GrossMonthlyIncome));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerCreditAccountSummary.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();
                thAffordability.Join();

                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Join();
                }
                thPmtGAP.Join();
                thAkaNames.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                DataTable dtCPA24MonthlyPayment = new DataTable();
                DataTable dtNLR24MonthlyPayment = new DataTable();
                DataTable dtNLRConsumerAccount = new DataTable();
                DataTable dtCPAConsumerAccount = new DataTable();


                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                {
                    dtCPA24MonthlyPayment = ds.Tables["Consumer24MonthlyPayment"];
                }

                if (ds.Tables.Contains("ConsumerAccountStatus"))
                {
                    dtCPAConsumerAccount = ds.Tables["ConsumerAccountStatus"];
                }
                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                {
                    dtNLR24MonthlyPayment = ds.Tables["ConsumerNLR24MonthlyPayment"];
                }
                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                {
                    dtNLRConsumerAccount = ds.Tables["ConsumerNLRAccountStatus"];
                }

                float fAvailableInst = 0;

                if (ds.Tables.Contains("ConsumerAffordability"))
                {
                    fAvailableInst = float.Parse(ds.Tables["ConsumerAffordability"].Rows[0]["PredAvailableinstalment"].ToString());
                }

                Thread thSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                thSummary.Start(string.Format("execute [dbo].[SpClr_CollectionsConsumerSummary] {0},{1},{2},{3},{4},'{5}','{6}',{7},{8},'{9}',{10}", ObjSearch.ConsumerID, "0", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.SubscriberAssociationCode, ObjSearch.AssociationTypeCode, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.UserName, fAvailableInst.ToString()));
                thSummary.Join();



                Thread thIndustryPmt = new Thread(new ParameterizedThreadStart(ProcessParameterizedReport));

                string strindustryPmt = "execute [dbo].[spCLR_CreditIndustry] @ConsumerID, @SubscriberAssociationCode, @AssociationTypeCode, @SubscriberID, @ReportID, @ProductID";
                //string strindustryPmt = "[dbo].[spCLR_CreditIndustry]";


                SqlParameter oConsumerID = new SqlParameter("@ConsumerID", ObjSearch.ConsumerID);
                SqlParameter oAssociationCode = new SqlParameter("@SubscriberAssociationCode", ObjSearch.SubscriberAssociationCode);
                SqlParameter oAssociationTypeCode = new SqlParameter("@AssociationTypeCode", ObjSearch.AssociationTypeCode);
                SqlParameter oSubscriberID = new SqlParameter("@SubscriberID", ObjSearch.subscriberID);
                SqlParameter oReportID = new SqlParameter("@ReportID", strReportID);
                SqlParameter oProductID = new SqlParameter("@ProductID", ObjSearch.ProductID);


                SqlParameterCollection oparamcoll = new SqlCommand().Parameters;
                oparamcoll.Add(oConsumerID);
                oparamcoll.Add(oAssociationCode);
                oparamcoll.Add(oAssociationTypeCode);
                oparamcoll.Add(oSubscriberID);
                oparamcoll.Add(oReportID);
                oparamcoll.Add(oProductID);

                if (dtCPA24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPA24MonthlyPayment";
                    SqlParameter oCPA24MonthlyPayment = new SqlParameter("@CPA24MonthlyPayment", dtCPA24MonthlyPayment);
                    oCPA24MonthlyPayment.TypeName = "dbo.CPA24MonthlyPayment";
                    oCPA24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPA24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLR24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLR24MonthlyPayment";
                    SqlParameter oNLR24MonthlyPayment = new SqlParameter("@NLR24MonthlyPayment", dtNLR24MonthlyPayment);
                    oNLR24MonthlyPayment.TypeName = "dbo.NLR24MonthlyPayment";
                    oNLR24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLR24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLRConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLRConsumerAccount";
                    SqlParameter oNLRConsumerAccount = new SqlParameter("@NLRConsumerAccount", dtNLRConsumerAccount);
                    oNLRConsumerAccount.TypeName = "dbo.NLRConsumerAccount";
                    oNLRConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLRConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtCPAConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPAConsumerAccount";
                    SqlParameter oCPAConsumerAccount = new SqlParameter("@CPAConsumerAccount", dtCPAConsumerAccount);
                    oCPAConsumerAccount.TypeName = "dbo.CPAConsumerAccount";
                    oCPAConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPAConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                dtCPAConsumerAccount.Dispose();
                dtNLRConsumerAccount.Dispose();
                dtNLR24MonthlyPayment.Dispose();
                dtNLR24MonthlyPayment.Dispose();


                object[] Values = new object[2];
                Values[0] = strindustryPmt;
                Values[1] = oparamcoll;

                thIndustryPmt.Start(Values);
                thIndustryPmt.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerFraudIndicatorsSummary":
                                if (ds.Tables.Contains("ConsumerFraudIndicatorsSummary"))
                                {
                                    if (!ObjSearch.SAFPSIndicator)
                                    {
                                        if (ds.Tables["ConsumerFraudIndicatorsSummary"].Columns.Contains("EmployerFraudVerificationYN"))
                                        {
                                            ds.Tables["ConsumerFraudIndicatorsSummary"].Rows[0]["EmployerFraudVerificationYN"] = "Access disabled to view this information";
                                        }

                                        if (ds.Tables["ConsumerFraudIndicatorsSummary"].Columns.Contains("SAFPSListingYN"))
                                        {
                                            ds.Tables["ConsumerFraudIndicatorsSummary"].Rows[0]["SAFPSListingYN"] = "Access disabled to view this information";
                                        }

                                        if (ds.Tables["ConsumerFraudIndicatorsSummary"].Columns.Contains("ProtectiveVerificationYN"))
                                        {
                                            ds.Tables["ConsumerFraudIndicatorsSummary"].Rows[0]["ProtectiveVerificationYN"] = "Access disabled to view this information";
                                        }

                                    }

                                    if (!dsReport.Tables.Contains("ConsumerFraudIndicatorsSummary"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerFraudIndicatorsSummary"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());

                                    }

                                }
                                break;
                        }

                    }

                }

                if (dsTracPlusDataSegment != null)
                {
                    if (dsTracPlusDataSegment.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsTracPlusDataSegment.Tables[0].Rows)
                        {
                            if ((dr["BonusViewed"].ToString().ToLower() == "true"))
                            {
                                switch (dr["DataSegmentName"].ToString())
                                {
                                    case "ConsumerTelephoneLinkage":
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                            }
                                        }
                                        break;
                                    case "ConsumerDetailConfirmation":
                                        if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                            }
                                        }
                                        break;
                                    default:
                                        if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                        {
                                            if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                            {
                                                dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 2 || ObjSearch.ProductID == 3 || ObjSearch.ProductID == 4 || ObjSearch.ProductID == 5 || ObjSearch.ProductID == 7))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetCollectionTraceReportBusinessStatus(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string strReportID = "";

            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                //Thread thCheckBlockStatus = new Thread(new ParameterizedThreadStart(ProcessReport));
                //thCheckBlockStatus.Start(string.Format("Execute spCheckblockingStatus {0},{1}", ObjSearch.subscriberID, ObjSearch.ConsumerID));
                //thCheckBlockStatus.Join();

                //    if (dtError.Rows.Count > 0)
                //    {
                //        throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                //    }


                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerTelephoneLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPublicDomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDirectorShipLink = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerInformationConfirmation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thPmtGAP = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordability = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));


                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));
                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0} , '{1}','{2}', '{3}',{4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thConsumerTelephoneLinkage.Start(string.Format("execute [dbo].[spCLR_ConsumerTelephoneLinkage] {0} , 0 ", ObjSearch.ConsumerID.ToString()));
                thPublicDomain.Start(string.Format("execute [dbo].[spCLR_PublicDomain] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerDirectorShipLink.Start(string.Format("execute [dbo].[spCLR_ConsumerDirectorShipLink_KZ] {0} , 0", ObjSearch.ConsumerID.ToString()));
                thConsumerInformationConfirmation.Start(string.Format("execute [dbo].[spCLR_ConsumerInformationConfirmation] {0},'{1}',{2}", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                }
                thPmtGAP.Start(string.Format("execute [dbo].[spCLR_ProcessPmtGAP] {0},{1},'{2}','{3}',{4},{5},{6},{7},{8},'{9}'", ObjSearch.ConsumerID, "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.UserName));
                thAffordability.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.UserName, ObjSearch.GrossMonthlyIncome));
                thAkaNames.Start(string.Format("Execute dbo.[spCLR_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno, ObjSearch.Surname.Replace("'", "''")));

                thConsumerAccountLoad.Join();
                thConsumerCreditAccountSummary.Join();
                thConsumerDetails.Join();
                thConsumerPropertyInformation.Join();
                thConsumerTelephoneLinkage.Join();
                thPublicDomain.Join();
                thConsumerDirectorShipLink.Join();
                thConsumerInformationConfirmation.Join();
                thAffordability.Join();

                if (ObjSearch.AssociationTypeCode.ToLower() == "nlr" || ObjSearch.AssociationTypeCode.ToLower() == "ccanlr")
                {
                    thNLRConsumerAccountLoad.Join();
                }
                thPmtGAP.Join();
                thAkaNames.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                DataTable dtCPA24MonthlyPayment = new DataTable();
                DataTable dtNLR24MonthlyPayment = new DataTable();
                DataTable dtNLRConsumerAccount = new DataTable();
                DataTable dtCPAConsumerAccount = new DataTable();


                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                {
                    dtCPA24MonthlyPayment = ds.Tables["Consumer24MonthlyPayment"];
                }

                if (ds.Tables.Contains("ConsumerAccountStatus"))
                {
                    dtCPAConsumerAccount = ds.Tables["ConsumerAccountStatus"];
                }
                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                {
                    dtNLR24MonthlyPayment = ds.Tables["ConsumerNLR24MonthlyPayment"];
                }
                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                {
                    dtNLRConsumerAccount = ds.Tables["ConsumerNLRAccountStatus"];
                }

                float fAvailableInst = 0;

                if (ds.Tables.Contains("ConsumerAffordability"))
                {
                    fAvailableInst = float.Parse(ds.Tables["ConsumerAffordability"].Rows[0]["PredAvailableinstalment"].ToString());
                }

                Thread thSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                thSummary.Start(string.Format("execute [dbo].[SpClr_CollectionsConsumerSummary] {0},{1},{2},{3},{4},'{5}','{6}',{7},{8},'{9}',{10}", ObjSearch.ConsumerID, "0", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString(), ObjSearch.SubscriberAssociationCode, ObjSearch.AssociationTypeCode, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.UserName, fAvailableInst.ToString()));
                thSummary.Join();



                Thread thIndustryPmt = new Thread(new ParameterizedThreadStart(ProcessParameterizedReport));

                string strindustryPmt = "execute [dbo].[spCLR_CreditIndustry] @ConsumerID, @SubscriberAssociationCode, @AssociationTypeCode, @SubscriberID, @ReportID, @ProductID";
                //string strindustryPmt = "[dbo].[spCLR_CreditIndustry]";


                SqlParameter oConsumerID = new SqlParameter("@ConsumerID", ObjSearch.ConsumerID);
                SqlParameter oAssociationCode = new SqlParameter("@SubscriberAssociationCode", ObjSearch.SubscriberAssociationCode);
                SqlParameter oAssociationTypeCode = new SqlParameter("@AssociationTypeCode", ObjSearch.AssociationTypeCode);
                SqlParameter oSubscriberID = new SqlParameter("@SubscriberID", ObjSearch.subscriberID);
                SqlParameter oReportID = new SqlParameter("@ReportID", strReportID);
                SqlParameter oProductID = new SqlParameter("@ProductID", ObjSearch.ProductID);


                SqlParameterCollection oparamcoll = new SqlCommand().Parameters;
                oparamcoll.Add(oConsumerID);
                oparamcoll.Add(oAssociationCode);
                oparamcoll.Add(oAssociationTypeCode);
                oparamcoll.Add(oSubscriberID);
                oparamcoll.Add(oReportID);
                oparamcoll.Add(oProductID);

                if (dtCPA24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPA24MonthlyPayment";
                    SqlParameter oCPA24MonthlyPayment = new SqlParameter("@CPA24MonthlyPayment", dtCPA24MonthlyPayment);
                    oCPA24MonthlyPayment.TypeName = "dbo.CPA24MonthlyPayment";
                    oCPA24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPA24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLR24MonthlyPayment.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLR24MonthlyPayment";
                    SqlParameter oNLR24MonthlyPayment = new SqlParameter("@NLR24MonthlyPayment", dtNLR24MonthlyPayment);
                    oNLR24MonthlyPayment.TypeName = "dbo.NLR24MonthlyPayment";
                    oNLR24MonthlyPayment.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLR24MonthlyPayment);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtNLRConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @NLRConsumerAccount";
                    SqlParameter oNLRConsumerAccount = new SqlParameter("@NLRConsumerAccount", dtNLRConsumerAccount);
                    oNLRConsumerAccount.TypeName = "dbo.NLRConsumerAccount";
                    oNLRConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oNLRConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                if (dtCPAConsumerAccount.Rows.Count > 0)
                {
                    strindustryPmt = strindustryPmt + ", @CPAConsumerAccount";
                    SqlParameter oCPAConsumerAccount = new SqlParameter("@CPAConsumerAccount", dtCPAConsumerAccount);
                    oCPAConsumerAccount.TypeName = "dbo.CPAConsumerAccount";
                    oCPAConsumerAccount.SqlDbType = SqlDbType.Structured;
                    oparamcoll.Add(oCPAConsumerAccount);
                }
                else
                {
                    strindustryPmt = strindustryPmt + ", default";
                }

                dtCPAConsumerAccount.Dispose();
                dtNLRConsumerAccount.Dispose();
                dtNLR24MonthlyPayment.Dispose();
                dtNLR24MonthlyPayment.Dispose();


                object[] Values = new object[2];
                Values[0] = strindustryPmt;
                Values[1] = oparamcoll;

                thIndustryPmt.Start(Values);
                thIndustryPmt.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            case "Consumer24MonthlyPayment":
                                if (ds.Tables.Contains("Consumer24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer24MonthlyPayment"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerAccountStatus":
                                if (ds.Tables.Contains("ConsumerAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("AccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["AccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAccountStatus"].Copy());
                                    }
                                }
                                break;
                            case "Consumer6MonthlyPaymentWithDelinquent":
                                if (ds.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                {
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["Consumer6MonthlyPaymentWithDelinquent"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerNLR24MonthlyPayment":
                                if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLR24MonthlyPayment"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerTelephoneLinkage":
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                    }
                                }
                                break;
                            case "ConsumerDetailConfirmation":
                                if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                    }
                                }
                                break;
                            case "OtherContactInfo":
                                if (ds.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoAddress"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoAddress"].Copy());
                                    }
                                }
                                if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                {
                                    if (!dsReport.Tables.Contains("ConsumerOtherContactInfoTelephone"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerOtherContactInfoTelephone"].Copy());
                                    }
                                }

                                break;
                            case "ConsumerNLRAccountStatus":
                                if (ds.Tables.Contains("ConsumerNLRAccountStatus"))
                                {
                                    if (!dsReport.Tables.Contains("NLRAccountTypeLegend"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["NLRAccountTypeLegend"].Copy());
                                    }
                                    if (!dsReport.Tables.Contains("ConsumerNLRAccountStatus"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerNLRAccountStatus"].Copy());
                                    }
                                }
                                else { }
                                break;
                            case "ConsumerFraudIndicatorsSummary":
                                if (ds.Tables.Contains("ConsumerFraudIndicatorsSummary"))
                                {
                                    if (!ObjSearch.SAFPSIndicator)
                                    {
                                        if (ds.Tables["ConsumerFraudIndicatorsSummary"].Columns.Contains("EmployerFraudVerificationYN"))
                                        {
                                            ds.Tables["ConsumerFraudIndicatorsSummary"].Rows[0]["EmployerFraudVerificationYN"] = "Access disabled to view this information";
                                        }

                                        if (ds.Tables["ConsumerFraudIndicatorsSummary"].Columns.Contains("SAFPSListingYN"))
                                        {
                                            ds.Tables["ConsumerFraudIndicatorsSummary"].Rows[0]["SAFPSListingYN"] = "Access disabled to view this information";
                                        }

                                        if (ds.Tables["ConsumerFraudIndicatorsSummary"].Columns.Contains("ProtectiveVerificationYN"))
                                        {
                                            ds.Tables["ConsumerFraudIndicatorsSummary"].Rows[0]["ProtectiveVerificationYN"] = "Access disabled to view this information";
                                        }

                                    }

                                    if (!dsReport.Tables.Contains("ConsumerFraudIndicatorsSummary"))
                                    {
                                        dsReport.Tables.Add(ds.Tables["ConsumerFraudIndicatorsSummary"].Copy());
                                    }
                                }
                                break;
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());

                                    }

                                }
                                break;
                        }

                    }

                }

                if (dsTracPlusDataSegment != null)
                {
                    if (dsTracPlusDataSegment.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsTracPlusDataSegment.Tables[0].Rows)
                        {
                            if ((dr["BonusViewed"].ToString().ToLower() == "true"))
                            {
                                switch (dr["DataSegmentName"].ToString())
                                {
                                    case "ConsumerTelephoneLinkage":
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageHome"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageHome"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageWork"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageWork"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerTelephoneLinkageCellular"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerTelephoneLinkageCellular"].Copy());
                                            }
                                        }
                                        break;
                                    case "ConsumerDetailConfirmation":
                                        if (ds.Tables.Contains("ConsumerAddressConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerAddressConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerAddressConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerContactConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerContactConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerContactConfirmation"].Copy());
                                            }
                                        }
                                        if (ds.Tables.Contains("ConsumerEmploymentConfirmation"))
                                        {
                                            if (!dsReport.Tables.Contains("ConsumerEmploymentConfirmation"))
                                            {
                                                dsReport.Tables.Add(ds.Tables["ConsumerEmploymentConfirmation"].Copy());
                                            }
                                        }
                                        break;
                                    default:
                                        if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                        {
                                            if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                            {
                                                dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");
                strCount.Add("ConsumerCreditAccountSummary");
                strCount.Add("ConsumerPropertyInformationSummary");
                strCount.Add("ConsumerDirectorSummary");
                strCount.Add("ConsumerAccountGoodBadSummary");
                strCount.Add("ConsumerScoring");
                strCount.Add("ConsumerDebtSummary");
                strCount.Add("ConsumerNLRDebtSummary");
                strCount.Add("ConsumerCPANLRDebtSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 2 || ObjSearch.ProductID == 3 || ObjSearch.ProductID == 4 || ObjSearch.ProductID == 5 || ObjSearch.ProductID == 7))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                    if (dsTracPlusDataSegment != null)
                    {
                        dsTracPlusDataSegment.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetKYCReport(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;


                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("Consumer");

                DataSet dsReport = new DataSet("Consumer");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPotentialFraudIndicator = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0},'{1}','{2}','{3}',{4},{5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));
                thConsumerPotentialFraudIndicator.Start(string.Format("execute [dbo].[spCLR_ConsumerPotentialFraudIndicator] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.SAFPSIndicator.ToString()));

                thConsumerDetails.Join();
                thConsumerPotentialFraudIndicator.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                foreach (DataRow dr in dsDataSegment.Tables[0].Rows)
                {
                    if ((dr["Active"].ToString().ToLower() == "true") & (dr["BonusViewed"].ToString().ToLower() == "false"))
                    {
                        switch (dr["DataSegmentName"].ToString())
                        {
                            default:
                                if (ds.Tables.Contains(dr["DataSegmentName"].ToString()))
                                {
                                    if (!dsReport.Tables.Contains(dr["DataSegmentName"].ToString()))
                                    {
                                        dsReport.Tables.Add(ds.Tables[dr["DataSegmentName"].ToString()].Copy());
                                    }
                                    strReportID = dr["ReportID"].ToString();
                                    strReportName = dr["ReportName"].ToString();
                                }
                                break;
                        }

                    }

                }
                int i = 0;

                List<string> strCount = new List<string>();
                strCount.Add("ReportInformation");
                strCount.Add("ConsumerDetail");
                strCount.Add("ConsumerFraudIndicatorsSummary");

                foreach (DataTable dt in dsReport.Tables)
                {
                    if (!strCount.Contains(dt.TableName))
                    {
                        i = i + 1;
                    }
                }
                if (i == 0 && !ObjSearch.DisplayUnusableInformation && (ObjSearch.ProductID == 91))
                {
                    throw new Exception("Unable to display the report due to a lack of usable Trace/Credit Information! This enquiry will not be billed");
                }
                else
                {

                    string xml = dsReport.GetXml();
                    xml = xml.Replace("_x0036_", "");
                    ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml;

                    dtReportInformation.Clear();
                    dtError.Clear();
                    ds.Clear();
                    dsReport.Clear();

                    dtReportInformation.Dispose();
                    dtError.Dispose();
                    ds.Dispose();
                    dsReport.Dispose();
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingI(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCustomVettingI {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 73, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingJ(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCustomVettingJ {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 74, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetPOSEnquiryReport(XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("POSDetails");

                DataSet dsReport = new DataSet("POSDetails");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                Thread thConsumerHomeAffairsInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditAccountSummary = new Thread(new ParameterizedThreadStart(ProcessReport));


                thConsumerHomeAffairsInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerIdentityPhotoVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.HomeAffairsID.ToString(),
                    ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(),
                    ObjSearch.IDno.ToString(), ObjSearch.Firstname.ToString(), ObjSearch.Surname.ToString(),
                    ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));

                thConsumerCreditInformation.Start(string.Format("execute [dbo].[spCLR_CreditInformation_POS] {0}, {1}, {2}, {3}, {4},{5},{6}, '{7}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode,
                    ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.CreatedbyUser));

                thConsumerCreditAccountSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerCreditAccountSummary] {0},{1},'{2}','{3}',{4},{5} ", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.AssociationTypeCode.ToString(), ObjSearch.ProductID.ToString(), strReportID.ToString()));

                thConsumerHomeAffairsInformation.Join();
                thConsumerCreditInformation.Join();
                thConsumerCreditAccountSummary.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                string CPA309 = string.Empty, NLR309 = string.Empty, ALL309 = string.Empty;

                if (ds.Tables.Count > 0)
                {
                    if(ds.Tables.Contains("ConsumerCPANLRDebtSummary"))
                    {
                        foreach (DataRow dr in ds.Tables["ConsumerCPANLRDebtSummary"].Rows)
                        {
                            CPA309 = dr["TotalMonthlyInstallmentCPA"].ToString();
                            NLR309 = dr["TotalMonthlyInstallmentNLR"].ToString();
                            ALL309 = dr["TotalMonthlyInstallment"].ToString();
                        }
                    }

                    ds.Tables.Remove("ConsumerCPANLRDebtSummary");

                    if (ds.Tables.Contains("ConsumerCreditInfoPOS"))
                    {
                        ds.Tables["ConsumerCreditInfoPOS"].Rows[0]["CPA309ALL"] = CPA309 ;
                        ds.Tables["ConsumerCreditInfoPOS"].Rows[0]["NLR309ALL"] = NLR309;
                        ds.Tables["ConsumerCreditInfoPOS"].Rows[0]["ALL309"] = ALL309;
                    }
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingK(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, string[] ApplicantionDetails)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL =
                    string.Format("execute spCustomVettingK '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}','{10}'," +
                                                            "'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}'",
                  ObjSearch.ExternalReference, ApplicantionDetails[0], ApplicantionDetails[1], ApplicantionDetails[2],
                  ApplicantionDetails[3], ApplicantionDetails[4], ApplicantionDetails[5], ApplicantionDetails[6], ApplicantionDetails[7],
                  ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID,
                    ObjSearch.SFID, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);

                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                //if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                //{
                //    ds.Tables[0].Columns.Add("EnquiryID");
                //    ds.Tables[0].Columns.Add("EnquiryResultID");

                //    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                //    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                //}

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingL(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCustomVettingL {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 74, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingN(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, string[] ApplicantionDetails)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                Decimal decScore = 0;

                ds = new DataSet("Consumer");

                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCustomVettingN = new Thread(new ParameterizedThreadStart(ProcessReport)); 

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}",
                    ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.SFID));

                thConsumerScoringSummary.Join();

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Columns.Contains("FinalScore"))
                        Decimal.TryParse(ds.Tables[0].Rows[0]["FinalScore"].ToString(), out decScore);
                }

                ds = new DataSet("CustomVetting");

                thCustomVettingN.Start(string.Format("execute spCustomVettingN '{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', '{8}', '{9}','{10}'," +
                                                            "'{11}','{12}',{13},{14},{15},{16},{17},{18},'{19}','{20}','{21}',{22},'{23}'",
                  ApplicantionDetails[12].Replace("'","''"),
                  ApplicantionDetails[0].Replace("'", "''"), ApplicantionDetails[1].Replace("'", "''"), ApplicantionDetails[2].Replace("'", "''"), ApplicantionDetails[3].Replace("'", "''"), ApplicantionDetails[4].Replace("'", "''"), ApplicantionDetails[5].Replace("'", "''"),
                  ApplicantionDetails[6].Replace("'", "''"), ApplicantionDetails[7].Replace("'", "''"), ApplicantionDetails[8].Replace("'", "''"), ApplicantionDetails[9].Replace("'", "''"), ApplicantionDetails[10].Replace("'", "''"), ApplicantionDetails[11].Replace("'", "''"),
                  ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID,
                    ObjSearch.SFID.Replace("'", "''"), ApplicantionDetails[13].Replace("'", "''"), ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false, decScore));

                thCustomVettingN.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "CustomVettingResult";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }
        public XDSPortalLibrary.Entity_Layer.Response CustomvettingP(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, string[] ApplicantionDetails)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                Decimal decScore = 0;

                ds = new DataSet("Consumer");

                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thCustomVettingP = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}",
                    ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.SFID));

                thConsumerScoringSummary.Join();

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Columns.Contains("FinalScore"))
                        Decimal.TryParse(ds.Tables[0].Rows[0]["FinalScore"].ToString(), out decScore);
                }

                ds = new DataSet("CustomVetting");

                thCustomVettingP.Start(string.Format("execute spCustomVettingCELLC '{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', '{8}', '{9}','{10}'," +
                                                            "'{11}','{12}',{13},{14},{15},{16},{17},{18},'{19}','{20}','{21}',{22},'{23}'",
                  ApplicantionDetails[12],
                  ApplicantionDetails[0], ApplicantionDetails[1], ApplicantionDetails[2], ApplicantionDetails[3], ApplicantionDetails[4], ApplicantionDetails[5],
                  ApplicantionDetails[6], ApplicantionDetails[7], ApplicantionDetails[8], ApplicantionDetails[9], ApplicantionDetails[10], ApplicantionDetails[11],
                  ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID,
                    ObjSearch.SFID, ApplicantionDetails[13], ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false, decScore));

                thCustomVettingP.Join();

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "CustomVettingResult";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;
        }
        
        public XDSPortalLibrary.Entity_Layer.Response XDSCustomvetting(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, int ReportID)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCLR_XdsGenricCustomVetting '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}'",
                    "", "", "", ObjSearch.ConsumerID, ObjSearch.IDno, ObjSearch.Passportno, 0, ObjSearch.subscriberID.ToString(), ObjSearch.EnquiryID, ObjSearch.EnquiryResultID,
                    ObjSearch.ProductID, ReportID, ObjSearch.UserName, ObjSearch.AssociationTypeCode,
                    ObjSearch.SubscriberAssociationCode, false, ObjSearch.GrossMonthlyIncome.ToString(), ObjSearch.ReferenceNo, ObjSearch.ExternalReference);

                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response XDSGeneriCustomvetting(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, int ReportID)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCLR_XdsGenricCustomVetting '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}'",
                    "", "", "", ObjSearch.ConsumerID, ObjSearch.IDno, ObjSearch.Passportno, 0, ObjSearch.subscriberID.ToString(), ObjSearch.EnquiryID, ObjSearch.EnquiryResultID,
                    ObjSearch.ProductID, ReportID, ObjSearch.UserName, ObjSearch.AssociationTypeCode,
                    ObjSearch.SubscriberAssociationCode, false, ObjSearch.GrossMonthlyIncome.ToString(), ObjSearch.ReferenceNo, ObjSearch.ExternalReference);

                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetEIntelligenceEnquiryReport(XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch, int EnquiryID, int EnquiryResultID)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                DataSet dsTracPlusDataSegment = ObjSearch.BonusSegments;

                ObjResponse.ResponseKeyType = "C";
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                ds = new DataSet("CreditInformationDetails");

                DataSet dsReport = new DataSet("CreditInformationDetails");

                DataTable dtReportInformation = new DataTable("ReportInformation");
                DataColumn dcReportID = new DataColumn("ReportID");
                DataColumn dcReportName = new DataColumn("ReportName");
                string strReportID = "";
                string strReportName = "";

                dtReportInformation.Columns.Add(dcReportID);
                dtReportInformation.Columns.Add(dcReportName);

                DataRow drReportInformation = dtReportInformation.NewRow();
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                drReportInformation["ReportID"] = strReportID;
                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                dtReportInformation.Rows.Add(drReportInformation);
                dsReport.Tables.Add(dtReportInformation);

                if (!string.IsNullOrEmpty(ObjSearch.ExternalReference))
                    ObjSearch.ExternalReference = ObjSearch.ExternalReference.Replace("'", "''");

                Thread thConsumerHomeAffairsInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerCreditInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));

                thConsumerDetails.Start(string.Format("execute [dbo].[spCLR_ConsumerDetails] {0}, '{1}', '{2}', '{3}', {4}, {5}", ObjSearch.ConsumerID.ToString(), ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString(), ObjSearch.subscriberID));

                thConsumerHomeAffairsInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerIdentityPhotoVerification] {0}, {1},{2},'{3}',{4},'{5}','{6}','{7}','{8}','{9}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID.ToString(), ObjSearch.HomeAffairsID.ToString(),
                    ObjSearch.SubscriberAssociationCode.ToString(), ObjSearch.ProductID.ToString(),
                    ObjSearch.IDno.ToString(), ObjSearch.Firstname.ToString(), ObjSearch.Surname.ToString(),
                    ObjSearch.ReferenceNo.ToString(), ObjSearch.ExternalReference.ToString()));

                thConsumerCreditInformation.Start(string.Format("execute [dbo].[spCLR_CreditInformation_EIntelligence] {0}, {1}, {2}, {3}, {4},{5},{6}, '{7}' ",
                    ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode,
                    EnquiryID, EnquiryResultID, ObjSearch.CreatedbyUser));




                thConsumerHomeAffairsInformation.Join();
                thConsumerCreditInformation.Join();
                thConsumerDetails.Join();


                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }


                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetCreditEnquiryReportFinance27(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");


                string strSQL = string.Format("execute [dbo].[spCustomVettingFinanace27] {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 88, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }


                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;


        }

        public XDSPortalLibrary.Entity_Layer.Response GetCreditEnquiryReportBabereki(XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");


                string strSQL = string.Format("execute [dbo].[spCustomVettingBabereki] {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 88, ObjSearch.Username, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }


                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;


        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingMFin(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                ds = new DataSet("XDSConnectReponse");

                string strSQL = string.Format("execute spCustomVettingMfin {0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}", ObjSearch.subscriberID.ToString(), ObjSearch.ConsumerID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 74, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, false);
                ProcessReport(strSQL);

                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }

                ds.Tables[0].TableName = "Result";

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("EnquiryID");
                    ds.Tables[0].Columns.Add("EnquiryResultID");

                    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                }

                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");

                dtError.Clear();
                ds.Clear();
                dtError.Dispose();
                ds.Dispose();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;
                ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                ObjResponse.ResponseKeyType = "C";
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingO(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, XDSPortalLibrary.Entity_Layer.SubmitResponse oinputResponse,out XDSPortalLibrary.Entity_Layer.SubmitResponse oSResponse)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            oSResponse = oinputResponse;
            try
            {

             //   File.AppendAllText(@"C:\Log\Response.txt", "EnterCustomvettingO");
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));

                
                string strReportID = "";
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();

                
                ds = new DataSet("XDSConnectReponse");

               // Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thdemographics = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thdisputeInfo = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thpublicdomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thJDScore = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (ObjSearch.ConsumerID > 0)
                {

                    thdemographics.Start(string.Format("execute [dbo].[spCLR_JD_Demographics] {0}", ObjSearch.ConsumerID.ToString()));
                    thdisputeInfo.Start(string.Format("execute [dbo].[spCLR_JD_DisputeInfo] {0}", ObjSearch.ConsumerID.ToString()));
                    
                    thpublicdomain.Start(string.Format("execute [dbo].[spCLR_JD_PublicDomain] {0},{1}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString()));
                    thNLRAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID, "0"));                    
                    thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID, "0"));
                    

                    // thConsumerDetails.Join();
                    thdemographics.Join();
                    thdisputeInfo.Join();
                    
                    thpublicdomain.Join();
                    thNLRAccountLoad.Join();                    
                    thConsumerAccountLoad.Join();
                }

                thAkaNames.Start(string.Format("execute [dbo].[spCLR_JD_AKANames] {0},'{1}','{2}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno.ToString(), ObjSearch.Surname));

                thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                thConsumerScoringSummary.Join();

               

                    thJDScore.Start(string.Format("Execute dbo.[spCLR_JD_ScoringSummary] {0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}',{11},{12},'{13}',{14},'{15}','{16}','{17}','{18}','{19}'", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID, ObjSearch.EnquiryID, ObjSearch.EnquiryResultID, ObjSearch.ProductID, 0, ObjSearch.UserName, ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.EmploymentType, ObjSearch.EmploymentSector, ObjSearch.NoOfDependants, ObjSearch.Income, ObjSearch.ProvinceCode, ObjSearch.HomeAffairsID, ObjSearch.IDno, ObjSearch.Passportno,ObjSearch.DOB.ToString("yyyyMMdd"),ObjSearch.MaidenName,ObjSearch .Gender));
                    thJDScore.Join();
                

                thAkaNames.Join();
                
                if (dtError.Rows.Count > 0)
                {
                    throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                }

                if (ds.Tables.Count <= 0)
                {
                    throw new Exception("No Result Found");
                }
                
                //ds.Tables[0].TableName = "Result";
                
                //if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                //{
                //    ds.Tables[0].Columns.Add("EnquiryID");
                //    ds.Tables[0].Columns.Add("EnquiryResultID");

                //    ds.Tables[0].Rows[0]["EnquiryID"] = ObjSearch.EnquiryID.ToString();
                //    ds.Tables[0].Rows[0]["EnquiryResultID"] = ObjSearch.EnquiryResultID.ToString();
                //}

                
                string xml = ds.GetXml();
                xml = xml.Replace("_x0036_", "");


                string forename1 = string.Empty, forename2 = string.Empty, forename3 = string.Empty, surName = string.Empty,IDno = string.Empty;

                 string NLRNoWriteOff = string.Empty;
                            string NLRPPWorstArrears6Months = "0";
                            string NLRPPWorstArrears24Months = "0";
                            string NLRPercentage0ArrearsLast24Months = "0";
                            string NLRWorstNLRArrears = "0";
                            string NLRPercentageZeroArrearsLast3Months = "0";
                            string NLRNumberOfNLRPaymentProfilesOpenedLastYear = "0";
                            string NLRMonthsSinceHighestNegative = "0";
                            string NLRPercentage3PlusArrearsLast3Months = "0";
                            string NLRPercentage3PlusArrearsLast24Months = "0";
                            string NLRWorstArrearsLastYear = "0";
                            string NLRTotalAdverseEver = "0";
                            string NLRTotalAccountsNLR = "0";
                            string NLRNegativeEver = "";
                            string NLRTotalInstallmentNLROpenAccounts = "0";
                            string NLRRecentNegativeMonths = "0";
                            string NLRRecentNegativeValue = "";
                            string NLRWorstMPSAArrearsLast90Days = "0";
                            string NLRHighestAdverseValue = "";
                            string NLRnoof0ArrearsLast24Months = "0";
                            string NLRnoofZeroArrearsLast3Months = "0";
                            string NLRnoof3PlusArrearsLast3Months = "0";
                            string NLRnoof3PlusArrearsLast24Months = "0";
                            string NLRtotalArrearsLast24Months = "0";
                            string NLRtotalArrearsLast3Months = "0";

                            string CPANoWriteOff = "0";
                            string CPAPPWorstArrears6Months = "0";
                            string CPAPPWorstArrears24Months = "0";
                            string CPAMonthsSinceOldestCPAProfile = "0";
                            string CPAPercentage0ArrearsLast24Months = "0";
                            string CPAWorstCPAArrearsLast90Days = "0";
                            string CPAPercentageZeroArrearsLast3Months = "0";
                            string CPAMonthsSinceHighestNegative = "0";
                            string CPAPercentage3PlusArrearsLast3Months = "0";
                            string CPAPercentage3PlusArrearsLast24Months = "0";
                            string CPAWorstArrearsLastYear = "0";
                            string CPATotalAdverseEver = "0";
                            string CPATotalAccountsCPA = "0";
                            string CPANegativeEver = "";
                            string CPATotalInstallmentCPAOpenAccounts = "0";
                            string CPARecentNegativeMonths = "0";
                            string CPARecentNegativeValue = "";
                            string CPAWorstMPSAArrearsLast90Days = "0";
                            string CPAHighestAdverseValue = "";
                            string CPAnoof0ArrearsLast24Months = "0";
                            string CPAnoofZeroArrearsLast3Months = "0";
                            string CPAnoof3PlusArrearsLast3Months = "0";
                            string CPAnoof3PlusArrearsLast24Months = "0";
                            string CPAtotalArrearsLast24Months = "0";
                            string CPAtotalArrearsLast3Months = "0";


                try
                {
                   

                    if (ds.Tables.Count > 0)
                    {


                        //foreach (DataTable dt in ds.Tables)
                        //{
                        //    File.AppendAllText(@"C:\Log\Response.txt", dt.TableName + " method :" + dt.Rows.Count.ToString() + "\n");
                        //}

                        if (ds.Tables.Contains("JDScore"))
                        {

                            foreach (DataRow dr in ds.Tables["JDScore"].Rows)
                            {
                                string strScore = dr["FinalScore"].ToString();
                                string ModelID = dr["ModelID"].ToString();
                                double doubleScore = Math.Round(float.Parse(strScore), 0);

                                strScore = doubleScore.ToString();
                                if (strScore == "0")
                                {
                                    strScore = "1";
                                }

                                if (ModelID == "0")
                                {
                                    ModelID = "1";
                                }

                                while (strScore.Length < 3)
                                {                                    
                                    strScore = "0" + strScore;
                                }

                                while (ModelID.Length < 3 && dr["ModelID"].ToString() == "0")
                                {
                                    ModelID = "0" + ModelID;
                                }

                                oSResponse.SubmitResult.SBCScore = strScore;
                                oSResponse.SubmitResult.SBCScoreIndicator = ModelID;//changed by kopano

                            }
                        }



                        if (ds.Tables.Contains("JDScoreDetails"))
                        {
                            int count = ds.Tables["JDScoreDetails"].Rows.Count;
                            oSResponse.SubmitResult.BureauResponse.StandardBatchCharsSB01 = new XDSPortalLibrary.Entity_Layer.StandardBatchCharsSB01[count];
                            int i = 0;

                            foreach (DataRow dr in ds.Tables["JDScoreDetails"].Rows)
                            {

                                XDSPortalLibrary.Entity_Layer.StandardBatchCharsSB01 o = new XDSPortalLibrary.Entity_Layer.StandardBatchCharsSB01();
                                
                                    
                                    o.ConsumerNo = dr["ConsumerID"].ToString();
                                    o.Name = dr["Code"].ToString();
                                    o.Value = dr["Att"].ToString();
                                    o.Weighting = dr["Score"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.StandardBatchCharsSB01[i] = o;
                                    i++;
                               
                            }
                        }

                        if (ds.Tables.Contains("AggregateNX33CPA"))
                        {

                            foreach (DataRow dr in ds.Tables["AggregateNX33CPA"].Rows)
                            {
                                CPANoWriteOff = dr["NoWriteOff"].ToString();
                                CPAPPWorstArrears6Months = dr["PPWorstArrears6Months"].ToString();
                                CPAPPWorstArrears24Months = dr["PPWorstArrears24Months"].ToString();
                                CPAMonthsSinceOldestCPAProfile = dr["MonthsSinceOldestCPAProfile"].ToString();
                                CPAPercentage0ArrearsLast24Months = dr["Percentage0ArrearsLast24Months"].ToString();
                                CPAWorstCPAArrearsLast90Days = dr["WorstCPAArrearsLast90Days"].ToString();
                                CPAPercentageZeroArrearsLast3Months = dr["PercentageZeroArrearsLast3Months"].ToString();
                                CPAMonthsSinceHighestNegative = dr["MonthsSinceHighestNegative"].ToString();
                                CPAPercentage3PlusArrearsLast3Months = dr["Percentage3PlusArrearsLast3Months"].ToString();
                                CPAPercentage3PlusArrearsLast24Months = dr["Percentage3PlusArrearsLast24Months"].ToString();
                                CPAWorstArrearsLastYear = dr["WorstArrearsLastYear"].ToString();
                                CPATotalAdverseEver = dr["TotalAdverseEver"].ToString();
                                CPATotalAccountsCPA = dr["TotalAccountsCPA"].ToString();
                                CPANegativeEver = dr["NegativeEver"].ToString();
                                CPATotalInstallmentCPAOpenAccounts = dr["TotalInstallmentCPAOpenAccounts"].ToString();
                                CPARecentNegativeMonths = dr["RecentNegativeMonths"].ToString();
                                CPARecentNegativeValue = dr["RecentNegativeValue"].ToString();
                                CPAWorstMPSAArrearsLast90Days = dr["WorstMPSAArrearsLast90Days"].ToString();
                                CPAHighestAdverseValue = dr["HighestAdverseValue"].ToString();
                                CPAnoof0ArrearsLast24Months = dr["noof0ArrearsLast24Months"].ToString();
                                CPAnoofZeroArrearsLast3Months = dr["noofZeroArrearsLast3Months"].ToString();
                                CPAnoof3PlusArrearsLast3Months = dr["noof3PlusArrearsLast3Months"].ToString();
                                CPAnoof3PlusArrearsLast24Months = dr["noof3PlusArrearsLast24Months"].ToString();
                                CPAtotalArrearsLast24Months = dr["totalArrearsLast24Months"].ToString();
                                CPAtotalArrearsLast3Months = dr["totalArrearsLast3Months"].ToString();


                                //PaymentProfileWMP01

                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01 = new XDSPortalLibrary.Entity_Layer.PaymentProfileWMP01[1];

                                XDSPortalLibrary.Entity_Layer.PaymentProfileWMP01 o = new XDSPortalLibrary.Entity_Layer.PaymentProfileWMP01();

                                o.WorstPaymentProfileMaxPayCPACurrent = dr["WorstPaymentProfileMaxPayCPACurrent"].ToString();
                                o.WorstPaymentProfileMaxPayCPAL3M = dr["WorstPaymentProfileMaxPayCPAL3M"].ToString();
                                o.WorstPaymentProfileMaxPayCPAL6M = dr["WorstPaymentProfileMaxPayCPAL6M"].ToString();
                                o.WorstPaymentProfileMaxPayCPAL12M = dr["WorstPaymentProfileMaxPayCPAL12M"].ToString();
                                o.WorstPaymentProfileMaxPayCPAL24M = dr["WorstPaymentProfileMaxPayCPAL24M"].ToString();

                                o.WorstPaymentProfileStatusCurrent = dr["WorstPaymentProfileStatusCPACurrent"].ToString();
                                o.WorstPaymentProfileStatusL3M = dr["WorstPaymentProfileStatusCPAL3M"].ToString();
                                o.WorstPaymentProfileStatusL6M = dr["WorstPaymentProfileStatusCPAL6M"].ToString();
                                o.WorstPaymentProfileStatusL12M = dr["WorstPaymentProfileStatusCPAL12M"].ToString();
                                o.WorstPaymentProfileStatusL18M = dr["WorstPaymentProfileStatusCPAL18M"].ToString();
                                o.WorstPaymentProfileStatusL24M = dr["WorstPaymentProfileStatusCPAL24M"].ToString();



                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0] = o;


                             
                            }

                        }

                        if (ds.Tables.Contains("AggregateNX33NLR"))
                        {

                            foreach (DataRow dr in ds.Tables["AggregateNX33NLR"].Rows)
                            {
                                NLRNoWriteOff = dr["NoWriteOff"].ToString();
                                NLRPPWorstArrears6Months = dr["PPWorstArrears6Months"].ToString();
                                NLRPPWorstArrears24Months = dr["PPWorstArrears24Months"].ToString();
                                NLRPercentage0ArrearsLast24Months = dr["Percentage0ArrearsLast24Months"].ToString();
                                NLRWorstNLRArrears = dr["WorstNLRArrears"].ToString();
                                NLRPercentageZeroArrearsLast3Months = dr["PercentageZeroArrearsLast3Months"].ToString();
                                NLRNumberOfNLRPaymentProfilesOpenedLastYear = dr["NumberOfNLRPaymentProfilesOpenedLastYear"].ToString();
                                NLRMonthsSinceHighestNegative = dr["MonthsSinceHighestNegative"].ToString();
                                NLRPercentage3PlusArrearsLast3Months = dr["Percentage3PlusArrearsLast3Months"].ToString();
                                NLRPercentage3PlusArrearsLast24Months = dr["Percentage3PlusArrearsLast24Months"].ToString();
                                NLRWorstArrearsLastYear = dr["WorstArrearsLastYear"].ToString();
                                NLRTotalAdverseEver = dr["TotalAdverseEver"].ToString();
                                NLRTotalAccountsNLR = dr["TotalAccountsNLR"].ToString();
                                NLRNegativeEver = dr["NegativeEver"].ToString();
                                NLRTotalInstallmentNLROpenAccounts = dr["TotalInstallmentNLROpenAccounts"].ToString();
                                NLRRecentNegativeMonths = dr["RecentNegativeMonths"].ToString();
                                NLRRecentNegativeValue = dr["RecentNegativeValue"].ToString();
                                NLRWorstMPSAArrearsLast90Days = dr["WorstMPSAArrearsLast90Days"].ToString();
                                NLRHighestAdverseValue = dr["HighestAdverseValue"].ToString();
                                NLRnoof0ArrearsLast24Months = dr["noof0ArrearsLast24Months"].ToString();
                                NLRnoofZeroArrearsLast3Months = dr["noofZeroArrearsLast3Months"].ToString();
                                 NLRnoof3PlusArrearsLast3Months = dr["noof3PlusArrearsLast3Months"].ToString();
                                NLRnoof3PlusArrearsLast24Months = dr["noof3PlusArrearsLast24Months"].ToString();
                                NLRtotalArrearsLast24Months = dr["totalArrearsLast24Months"].ToString();
                                NLRtotalArrearsLast3Months = dr["totalArrearsLast3Months"].ToString();


                                //PaymentProfileWMP01

                                //File.AppendAllText(@"C:\Log\Response.txt","xstep1");
                                //File.AppendAllText(@"C:\Log\Response.txt", "xstep4");
                                //File.AppendAllText(@"C:\Log\Response.txt", ds.Tables["AggregateNX33NLR"].Columns.ToString());
                                //File.AppendAllText(@"C:\Log\Response.txt", dr["WorstPaymentProfileMaxPayNLRCurrent"].ToString());
                                //File.AppendAllText(@"C:\Log\Response.txt", dr["WorstPaymentProfileMaxPayNLRCurrent"].ToString());
                                if (oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01 == null)
                                {
                                   //// File.AppendAllText(@"C:\Log\Response.txt", "xstep2");
                                    oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01 = new XDSPortalLibrary.Entity_Layer.PaymentProfileWMP01[1];
                                   // File.AppendAllText(@"C:\Log\Response.txt", "xstep3");

                                    XDSPortalLibrary.Entity_Layer.PaymentProfileWMP01 o = new XDSPortalLibrary.Entity_Layer.PaymentProfileWMP01();
                                    o.WorstPaymentProfileMaxPayNLRCurrent = dr["WorstPaymentProfileMaxPayNLRCurrent"].ToString();
                                    //File.AppendAllText(@"C:\Log\Response.txt", "xstep5");
                                    o.WorstPaymentProfileMaxPayNLR3M = dr["WorstPaymentProfileMaxPayNLRL3M"].ToString();
                                    o.WorstPaymentProfileMaxPayNLR6M = dr["WorstPaymentProfileMaxPayNLRL6M"].ToString();
                                    o.WorstPaymentProfileMaxPayNLR12M = dr["WorstPaymentProfileMaxPayNLRL12M"].ToString();
                                    o.WorstPaymentProfileMaxPayNLR24M = dr["WorstPaymentProfileMaxPayNLRL24M"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0] = o;
                                }                              

                                
                                else
                                {
                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0].WorstPaymentProfileMaxPayNLRCurrent = dr["WorstPaymentProfileMaxPayNLRCurrent"].ToString();
                                //File.AppendAllText(@"C:\Log\Response.txt", "xstep5");
                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0].WorstPaymentProfileMaxPayNLR3M = dr["WorstPaymentProfileMaxPayNLRL3M"].ToString();
                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0].WorstPaymentProfileMaxPayNLR6M = dr["WorstPaymentProfileMaxPayNLRL6M"].ToString();
                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0].WorstPaymentProfileMaxPayNLR12M = dr["WorstPaymentProfileMaxPayNLRL12M"].ToString();
                                oSResponse.SubmitResult.BureauResponse.PaymentProfileWMP01[0].WorstPaymentProfileMaxPayNLR24M = dr["WorstPaymentProfileMaxPayNLRL24M"].ToString();
                                }
                            
                            }

                        }

                       


                        Thread thAggregate = new Thread(new ParameterizedThreadStart(ProcessReport));
                        thAggregate.Start(string.Format("execute [dbo].[spCLR_JD_AggregateCalc] '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}','{44}','{45}','{46}','{47}','{48}','{49}'", CPANoWriteOff,
                                            CPAPPWorstArrears6Months,
                                            CPAPPWorstArrears24Months,
                                            CPAMonthsSinceOldestCPAProfile,
                                            CPAPercentage0ArrearsLast24Months,
                                            CPAWorstCPAArrearsLast90Days,
                                            CPAPercentageZeroArrearsLast3Months,
                                            CPAMonthsSinceHighestNegative,
                                            CPAPercentage3PlusArrearsLast3Months,
                                            CPAPercentage3PlusArrearsLast24Months,
                                            CPAWorstArrearsLastYear,
                                            CPATotalAdverseEver,
                                            CPATotalAccountsCPA,
                                            CPANegativeEver,
                                            CPATotalInstallmentCPAOpenAccounts,
                                            CPARecentNegativeMonths,
                                            CPARecentNegativeValue,
                                            CPAWorstMPSAArrearsLast90Days,
                                            CPAHighestAdverseValue,
                                            NLRNoWriteOff,
                                            NLRPPWorstArrears6Months,
                                            NLRPPWorstArrears24Months,
                                            NLRPercentage0ArrearsLast24Months,
                                            NLRWorstNLRArrears,
                                            NLRPercentageZeroArrearsLast3Months,
                                            NLRNumberOfNLRPaymentProfilesOpenedLastYear,
                                            NLRMonthsSinceHighestNegative,
                                            NLRPercentage3PlusArrearsLast3Months,
                                            NLRPercentage3PlusArrearsLast24Months,
                                            NLRWorstArrearsLastYear,
                                            NLRTotalAdverseEver,
                                            NLRNegativeEver,
                                            NLRTotalInstallmentNLROpenAccounts,
                                            NLRRecentNegativeMonths,
                                            NLRRecentNegativeValue,
                                            NLRWorstMPSAArrearsLast90Days,
                                            NLRHighestAdverseValue,
                                            NLRTotalAccountsNLR,
                                            CPAnoof0ArrearsLast24Months,
                                            CPAnoofZeroArrearsLast3Months,
                                            CPAnoof3PlusArrearsLast3Months,
                                            CPAnoof3PlusArrearsLast24Months,
                                            CPAtotalArrearsLast24Months,
                                            CPAtotalArrearsLast3Months,
                                            NLRnoof0ArrearsLast24Months,
                                            NLRnoofZeroArrearsLast3Months,
                                            NLRnoof3PlusArrearsLast3Months,
                                            NLRnoof3PlusArrearsLast24Months,
                                            NLRtotalArrearsLast24Months,
                                            NLRtotalArrearsLast3Months));
                        thAggregate.Join();

                        if (dtError.Rows.Count > 0)
                        {
                            throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
                        }


                        if (ds.Tables.Contains("AggregateNX33"))
                        {
                            oSResponse.SubmitResult.BureauResponse.AggregateNX33 = new XDSPortalLibrary.Entity_Layer.AggregateNX33[1];
                            oSResponse.SubmitResult.BureauResponse.AggregateNX05 = new XDSPortalLibrary.Entity_Layer.AggregateNX05[1];
                            oSResponse.SubmitResult.BureauResponse.AggregateNX09 = new XDSPortalLibrary.Entity_Layer.AggregateNX09[1];

                            foreach (DataRow dr in ds.Tables["AggregateNX33"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.AggregateNX33 o = new XDSPortalLibrary.Entity_Layer.AggregateNX33();

                                o.MonthsSinceOldestCPAProfile = dr["MonthsSinceOldestCPAProfile"].ToString();
                                o.WorstNLRArrears = dr["WorstNLRArrears"].ToString();
                                o.Percentage0ArrearsLast24Months = dr["Percentage0ArrearsLast24Months"].ToString();
                                o.WorstCPAArrearsLast90Days = dr["WorstCPAArrearsLast90Days"].ToString();
                                o.PercentageZeroArrearsLast3Months = dr["PercentageZeroArrearsLast3Months"].ToString();
                                o.NumberOfNLRPaymentProfilesOpenedLastYear = dr["NumberOfNLRPaymentProfilesOpenedLastYear"].ToString();
                                o.MonthsSinceHighestNegative = dr["MonthsSinceHighestNegative"].ToString();
                                o.Percentage3PlusArrearsLast3Months = dr["Percentage3PlusArrearsLast3Months"].ToString();
                                o.Percentage3PlusArrearsLast24Months = dr["Percentage3PlusArrearsLast24Months"].ToString();
                                o.WorstArrearsLastYear = dr["WorstArrearsLastYear"].ToString();
                                o.TotalAdverseEver = dr["TotalAdverseEver"].ToString();
                                o.TotalAccountsCPA = dr["TotalAccountsCPA"].ToString();
                                o.TotalAccountsNLR = dr["TotalAccountsNLR"].ToString();
                                o.NegativeEver = dr["NegativeEver"].ToString();
                                o.TotalInstallmentCPAOpenAccounts = Math.Round(float.Parse(dr["TotalInstallmentCPAOpenAccounts"].ToString()), 0).ToString();
                                o.TotalInstallmentNLROpenAccounts = Math.Round(float.Parse(dr["TotalInstallmentNLROpenAccounts"].ToString()),0).ToString();
                                o.RecentNegativeMonths = dr["RecentNegativeMonths"].ToString();
                                o.RecentNegativeValue = dr["RecentNegativeValue"].ToString();
                                o.WorstMPSAArrearsLast90Days = dr["WorstMPSAArrearsLast90Days"].ToString();
                                o.HighestAdverseValue = dr["HighestAdverseValue"].ToString();

                                oSResponse.SubmitResult.BureauResponse.AggregateNX33[0] = o;

                                XDSPortalLibrary.Entity_Layer.AggregateNX05 oAggregateNX05 = new XDSPortalLibrary.Entity_Layer.AggregateNX05();

                                oAggregateNX05.NoWriteOff = dr["NoWriteOff"].ToString();

                                oSResponse.SubmitResult.BureauResponse.AggregateNX05[0] = oAggregateNX05;

                                XDSPortalLibrary.Entity_Layer.AggregateNX09 oAggregateNX09 = new XDSPortalLibrary.Entity_Layer.AggregateNX09();
                                oAggregateNX09.PPWorstArrears6Months = dr["PPWorstArrears6Months"].ToString();
                                oAggregateNX09.PPWorstArrears24Months = dr["PPWorstArrears24Months"].ToString();

                                oSResponse.SubmitResult.BureauResponse.AggregateNX09[0] = oAggregateNX09;

                            }

                        }

                        if (ds.Tables.Contains("ConsumerDetail"))
                        {
                            int count = ds.Tables["ConsumerDetail"].Rows.Count;
                            oSResponse.SubmitResult.BureauResponse.ConsumerInfoNO04 = new XDSPortalLibrary.Entity_Layer.ConsumerInfoNO04[count];
                            oSResponse.SubmitResult.BureauResponse.ConsumerInfoNO05 = new XDSPortalLibrary.Entity_Layer.ConsumerInfoNO05[count];
                            int i = 0;
                            foreach (DataRow dr in ds.Tables["ConsumerDetail"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.ConsumerInfoNO04 o = new XDSPortalLibrary.Entity_Layer.ConsumerInfoNO04();
                                o.ConsumerNo = ObjSearch.ConsumerID.ToString();
                                o.DateOfBirth = dr["BirthDate"].ToString();
                                o.Forename1 = dr["FirstName"].ToString();
                                o.Forename2 = dr["SecondName"].ToString();
                                o.Forename3 = dr["ThirdName"].ToString();
                                o.Gender = dr["Gender"].ToString();
                                o.IdentityNo1 = dr["IDNo"].ToString();
                                o.IdentityNo2 = dr["PassportNo"].ToString();
                                o.MaritalStatusDesc = dr["MaritalStatusDesc"].ToString();
                                o.SpouseName1 = dr["SpouseFirstName"].ToString();
                                o.SpouseName2 = dr["SpouseSurName"].ToString();
                                o.Surname = dr["Surname"].ToString();
                                o.DeceasedDate = dr["Deceaseddate"].ToString();

                                forename1 = dr["FirstName"].ToString();
                                forename2 = dr["SecondName"].ToString();
                                forename3 = dr["ThirdName"].ToString();
                                surName = dr["Surname"].ToString();
                                IDno = dr["IDNo"].ToString();


                                XDSPortalLibrary.Entity_Layer.ConsumerInfoNO05 oData = new XDSPortalLibrary.Entity_Layer.ConsumerInfoNO05();
                                oData.ConsumerNo = ObjSearch.ConsumerID.ToString();
                                oData.DateOfBirth = dr["BirthDate"].ToString();
                                oData.Forename1 = dr["FirstName"].ToString();
                                oData.Forename2 = dr["SecondName"].ToString();
                                oData.Forename3 = dr["ThirdName"].ToString();
                                oData.Gender = dr["Gender"].ToString();
                                oData.IdentityNo1 = dr["IDNo"].ToString();
                                oData.IdentityNo2 = dr["PassportNo"].ToString();
                                oData.MaritalStatusDesc = dr["MaritalStatusDesc"].ToString();
                                oData.SpouseName1 = dr["SpouseFirstName"].ToString();
                                oData.SpouseName2 = dr["SpouseSurName"].ToString();
                                oData.Surname = surName;

                                oSResponse.SubmitResult.BureauResponse.ConsumerInfoNO04[i] = o;
                                oSResponse.SubmitResult.BureauResponse.ConsumerInfoNO05[i] = oData;
                                i++;
                            }
                        }


                        if (ds.Tables.Contains("Disputeinfo"))
                        {
                            int count = ds.Tables["Disputeinfo"].Rows.Count;
                            oSResponse.SubmitResult.BureauResponse.DisputeIndicatorDI01 = new XDSPortalLibrary.Entity_Layer.DisputeIndicatorDI01[count];
                            int i = 0;
                            foreach (DataRow dr in ds.Tables["Disputeinfo"].Rows)
                            {
                                XDSPortalLibrary.Entity_Layer.DisputeIndicatorDI01 o = new XDSPortalLibrary.Entity_Layer.DisputeIndicatorDI01();
                                o.CaptureDate = dr["Capturedate"].ToString();
                                o.ConsumerNo = ObjSearch.ConsumerID.ToString();
                                o.DaysToExpiry = dr["DaystoExpiry"].ToString();
                                o.ExpiryDate = dr["ExpiryDate"].ToString();
                                o.Forename1 = forename1;
                                o.Forename2 = forename2;
                                o.Forename3 = forename3;
                                o.Surname = surName;
                                o.IdentityNo = IDno;
                                oSResponse.SubmitResult.BureauResponse.DisputeIndicatorDI01[i] = o;
                                i++;
                            }

                        }



                    


                        if (ds.Tables.Contains("ConsumerAddressHistory"))
                            {

                              //  File.AppendAllText(@"C:\Log\Response.txt", "Step1\n");
                                int count = ds.Tables["ConsumerAddressHistory"].Rows.Count;

                              //  File.AppendAllText(@"C:\Log\Response.txt", count.ToString()+"\n");
                                oSResponse.SubmitResult.BureauResponse.AddressNA07 = new XDSPortalLibrary.Entity_Layer.AddressNA07[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerAddressHistory"].Rows)
                                {
                                   // File.AppendAllText(@"C:\Log\Response.txt", count.ToString() + "\n");
                                    XDSPortalLibrary.Entity_Layer.AddressNA07 o = new XDSPortalLibrary.Entity_Layer.AddressNA07();
                                    o.ConsumerNo = ObjSearch.ConsumerID.ToString();
                                    o.InformationDate = dr["LastUpdatedDate"].ToString();
                                    o.Line1 = dr["Address1"].ToString();
                                    o.Line2 = dr["Address2"].ToString();
                                    o.PostalCode = dr["PostalCode"].ToString();
                                    o.OwnerTenant = dr["OccupantTypeInd"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.AddressNA07[i] = o;
                                    i++;
                                }
                            }
                        

                        
                            if (ds.Tables.Contains("AKANames"))
                            {
                                int count = ds.Tables["AKANames"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.AKANamesNK04 = new XDSPortalLibrary.Entity_Layer.AKANamesNK04[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["AKANames"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.AKANamesNK04 o = new XDSPortalLibrary.Entity_Layer.AKANamesNK04();
                                    o.AKAName = dr["Finalname"].ToString();
                                    o.InformationDate = dr["LastUpdateddate"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.AKANamesNK04[i] = o;
                                    i++;
                                }
                            }
                        

                       
                            if (ds.Tables.Contains("ConsumerDefaultAlert"))
                            {
                                int count = ds.Tables["ConsumerDefaultAlert"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.DefaultsD701Part1 = new XDSPortalLibrary.Entity_Layer.DefaultD701Part1[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerDefaultAlert"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.DefaultD701Part1 o = new XDSPortalLibrary.Entity_Layer.DefaultD701Part1();
                                    o.AccountNo = dr["AccountNo"].ToString();
                                    o.InformationDate = dr["DateLoaded"].ToString();
                                    o.DefaultAmount = Math.Round(float.Parse(dr["Amount"].ToString()), 2).ToString();
                                    o.DefaultType = dr["Statuscode"].ToString();
                                    o.SupplierName = dr["Company"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.DefaultsD701Part1[i] = o;
                                    i++;

                                }
                            }                        

                        
                            if (ds.Tables.Contains("ConsumerFraudIndicators"))
                            {
                                int count = ds.Tables["ConsumerFraudIndicators"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.SAFPSNF01 = new XDSPortalLibrary.Entity_Layer.SAFPSNF01[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerFraudIndicators"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.SAFPSNF01 o = new XDSPortalLibrary.Entity_Layer.SAFPSNF01();
                                    o.FPSRefNo = dr["Shamwari_Ref_No"].ToString();
                                    o.FraudCaseDate = dr["Date_Of_Filing"].ToString();
                                    o.FraudCategory = dr["Category"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.SAFPSNF01[i] = o;
                                    i++;
                                }
                            }
                        

                       
                            if (ds.Tables.Contains("Noticessummary"))
                            {
                                int count = ds.Tables["Noticessummary"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.NoticesNN08 = new XDSPortalLibrary.Entity_Layer.NoticesNN08[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["Noticessummary"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.NoticesNN08 o = new XDSPortalLibrary.Entity_Layer.NoticesNN08();
                                    o.NoticeTypeCode = dr["JudgmentTypeCodeJD"].ToString();
                                    o.NoticeTypeCodevalue = dr["totaloccurences"].ToString();


                                    oSResponse.SubmitResult.BureauResponse.NoticesNN08[i] = o;
                                    i++;
                                }
                            }
                        

                       
                            if (ds.Tables.Contains("ConsumerDebtReview"))
                            {
                                int count = ds.Tables["ConsumerDebtReview"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.DebtCounsellingDC01 = new XDSPortalLibrary.Entity_Layer.DebtCounsellingDC01[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerDebtReview"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.DebtCounsellingDC01 o = new XDSPortalLibrary.Entity_Layer.DebtCounsellingDC01();
                                    o.DebtCounsellingDate = dr["DebtCounsellingDate"].ToString();


                                    oSResponse.SubmitResult.BureauResponse.DebtCounsellingDC01[i] = o;
                                    i++;
                                }
                            }
                        

                      
                            if (ds.Tables.Contains("ConsumerJudgement"))
                            {
                                int count = ds.Tables["ConsumerJudgement"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.JudgementsNJ07 = new XDSPortalLibrary.Entity_Layer.JudgementsNJ07[count];
                                int i = 0;


                                if (oSResponse.SubmitResult.BureauResponse.AggregateNX05 != null)
                                {
                                    if (oSResponse.SubmitResult.BureauResponse.AggregateNX05.Count() > 0)
                                    {
                                        oSResponse.SubmitResult.BureauResponse.AggregateNX05[0].NoJudgements = ds.Tables["ConsumerJudgement"].Rows.Count.ToString();
                                    }
                                }
                                else
                                {
                                    oSResponse.SubmitResult.BureauResponse.AggregateNX05 = new XDSPortalLibrary.Entity_Layer.AggregateNX05[1];
                                    XDSPortalLibrary.Entity_Layer.AggregateNX05 oAggregateNX05 = new XDSPortalLibrary.Entity_Layer.AggregateNX05();
                                    oAggregateNX05.NoJudgements = ds.Tables["ConsumerJudgement"].Rows.Count.ToString();
                                    oSResponse.SubmitResult.BureauResponse.AggregateNX05[0] = oAggregateNX05;
                                }

                                foreach (DataRow dr in ds.Tables["ConsumerJudgement"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.JudgementsNJ07 o = new XDSPortalLibrary.Entity_Layer.JudgementsNJ07();
                                    o.JudgmentTypeCode = "J";
                                    o.JudgmentTypeDesc = "Judgment";
                                    o.Amount = Math.Round(float.Parse(dr["DisputeAmt"].ToString()), 0).ToString();
                                    o.CaseNo = dr["CaseNumber"].ToString();
                                    o.CourtNameDesc = dr["CourtName"].ToString();
                                    o.CourtTypeDesc = dr["CourtType"].ToString();
                                    o.JudgmentDate = dr["CaseFilingDate"].ToString();
                                    o.NatureOfDebtDesc = dr["Natureofdebt"].ToString();
                                    o.Plaintiff = dr["PlaintiffName"].ToString();


                                        oSResponse.SubmitResult.BureauResponse.JudgementsNJ07[i] = o;
                                    i++;
                                }
                            }
                        

                       
                            if (ds.Tables.Contains("ConsumerEmploymentHistory"))
                            {
                                int count = ds.Tables["ConsumerEmploymentHistory"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.EmploymentNM04 = new XDSPortalLibrary.Entity_Layer.EmploymentNM04[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerEmploymentHistory"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.EmploymentNM04 o = new XDSPortalLibrary.Entity_Layer.EmploymentNM04();
                                    o.EmployerName = dr["EmployerDetail"].ToString();
                                    o.Occupation = dr["occupation"].ToString();
                                    o.InformationDate = dr["LastUpdatedDate"].ToString();
                                    o.EmploymentPeriod = dr["EmploymentPeriod"].ToString();


                                    oSResponse.SubmitResult.BureauResponse.EmploymentNM04[i] = o;
                                    i++;
                                }
                            }
                        

                      
                            if (ds.Tables.Contains("Summary"))
                            {
                                int count = ds.Tables["Summary"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.ConsumerCountersNC04 = new XDSPortalLibrary.Entity_Layer.ConsumerCountersNC04[count];

                                oSResponse.SubmitResult.BureauResponse.NLRCounterSeqmentMC01 = new XDSPortalLibrary.Entity_Layer.NLRCounterSeqmentMC01[count];
                                

                                int i = 0;
                                foreach (DataRow dr in ds.Tables["Summary"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.ConsumerCountersNC04 o = new XDSPortalLibrary.Entity_Layer.ConsumerCountersNC04();
                                    o.OwnEnquiries1YrBack = dr["X_ENQ_NO_ENQ_L1YR_OWN"].ToString();
                                    o.OtherEnquiries1YrBack = dr["X_ENQ_NO_ENQ_L1YR_OTH"].ToString();
                                    o.Judgements1YrBack = dr["X_JUD_NO_JUD_L1YR"].ToString();
                                    o.Notices1YrBack = dr["X_NOT_NO_NOT_L1YR"].ToString();
                                    o.Defaults1YrBack = dr["X_DEF_NO_DEF_L1YR"].ToString();
                                    o.Defaults2YrsBack = dr["X_DEF_NO_DEF_L2YR"].ToString();
                                    o.Judgements2YrsBack = dr["X_JUD_NO_JUD_L2YR"].ToString();
                                    o.Notices2YrsBack = dr["X_NOT_NO_NOT_L2YR"].ToString();


                                    oSResponse.SubmitResult.BureauResponse.ConsumerCountersNC04[i] = o;
                                    i++;

                                    XDSPortalLibrary.Entity_Layer.NLRCounterSeqmentMC01 oNLR = new XDSPortalLibrary.Entity_Layer.NLRCounterSeqmentMC01();
                                    oNLR.CurrentYearEnquiriesClient = (int.Parse(dr["X_ENQ_NO_ENQ_L1YR_OWN"].ToString()) + int.Parse(dr["X_ENQ_NO_ENQ_L1YR_OTH"].ToString())).ToString();
                                    oNLR.PreviousYearEnquiriesClient = "0";
                                    oNLR.AllOtherYearEnquiriesClient = "0";
                                   
                                    oSResponse.SubmitResult.BureauResponse.NLRCounterSeqmentMC01[0] = oNLR;
                                }
                            }

                            //EnquiriesNE50
                            if (ds.Tables.Contains("ConsumerEnquiryHistory"))
                            {
                                int count = ds.Tables["ConsumerEnquiryHistory"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.EnquiriesNE50 = new XDSPortalLibrary.Entity_Layer.EnquiryNE50[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerEnquiryHistory"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.EnquiryNE50 o = new XDSPortalLibrary.Entity_Layer.EnquiryNE50();
                                    o.DateOfEnquiry = dr["EnquiryDate"].ToString();
                                    o.Contact = dr["SubscriberContact"].ToString();
                                    o.EnquiryTypeDescription = dr["CreditGrantorEnquiryReasonDesc"].ToString();
                                    o.EnquiryTypeCode = dr["ProductID"].ToString();
                                    o.Subscriber = dr["SubscriberName"].ToString();

                                    oSResponse.SubmitResult.BureauResponse.EnquiriesNE50[i] = o;
                                    i++;
                                }
                            }


                        //ConsEnqTransInfo0102

                      
                            if (ds.Tables.Contains("ConsumerNLRAccountStatus") && ds.Tables.Contains("ConsumerNLR24MonthlyPaymentHeader") && ds.Tables.Contains("ConsumerNLR24MonthlyPayment"))
                            {
                                int count = ds.Tables["ConsumerNLRAccountStatus"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.NLRAccountsInformationM701 = new XDSPortalLibrary.Entity_Layer.NLRAccountInformationM701[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerNLRAccountStatus"].Rows)
                                {

                                    XDSPortalLibrary.Entity_Layer.NLRAccountInformationM701 o = new XDSPortalLibrary.Entity_Layer.NLRAccountInformationM701();

                                    o.AccountNo = dr["AccountNo"].ToString();
                                    o.AccountOpenDate = dr["AccountOpenedDate"].ToString();
                                    o.AccountType = dr["AccountType"].ToString();
                                    o.CurrentBalance =Math.Round(float.Parse( dr["CurrentBalanceAmt"].ToString()), 2).ToString();
                                    o.Instalment = Math.Round(float.Parse(dr["MonthlyInstalmentAmt"].ToString()), 2).ToString();
                                    o.OpeningBalance = Math.Round(float.Parse(dr["CreditLimitAmt"].ToString()), 2).ToString();
                                    o.SubscriberName = dr["SubscriberName"].ToString();
                                    o.Terms = dr["Terms"].ToString();
                                    o.LastUpdateDate = dr["LastUpdatedDate"].ToString();
                                    o.EndUseCode = dr["LoanEndUseCode"].ToString();
                                    

                                    o.PaymentHistories = new XDSPortalLibrary.Entity_Layer.PaymentHistory[24];

                                    if (ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Rows.Count > 0 && ds.Tables["ConsumerNLR24MonthlyPayment"].Rows.Count > 0)
                                    {
                                        int colindex = 1;

                                        while (colindex <= 24)
                                        {
                                            string colname = colindex.ToString();

                                            colname = "M" + (colname.Length == 2 ? colname : "0" + colname);
                                            XDSPortalLibrary.Entity_Layer.PaymentHistory oHis = new XDSPortalLibrary.Entity_Layer.PaymentHistory();
                                            oHis.Date = ds.Tables["ConsumerNLR24MonthlyPaymentHeader"].Rows[0][colname].ToString();
                                            oHis.StatusCode = ds.Tables["ConsumerNLR24MonthlyPayment"].Rows[i][colname].ToString();

                                            o.PaymentHistories[colindex - 1] = oHis;

                                            colindex++;
                                        }

                                    }


                                    oSResponse.SubmitResult.BureauResponse.NLRAccountsInformationM701[i] = o;
                                    i++;
                                }
                            }
                        

                        oSResponse.SubmitResult.BureauResponse.EchoData0001 = new XDSPortalLibrary.Entity_Layer.EchoData0001[1];
                        XDSPortalLibrary.Entity_Layer.EchoData0001 oEcho = new XDSPortalLibrary.Entity_Layer.EchoData0001();
                        oEcho.ClientReference = ObjSearch.ExternalReference;
                        oSResponse.SubmitResult.BureauResponse.EchoData0001[0] = oEcho;

                        
                            if (ds.Tables.Contains("ConsumerScoring"))
                            {
                                int count = ds.Tables["ConsumerScoring"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.EmpiricaEM04 = new XDSPortalLibrary.Entity_Layer.EmpiricaEM04[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerScoring"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.EmpiricaEM04 o = new XDSPortalLibrary.Entity_Layer.EmpiricaEM04();
                                    o.ConsumerNo = ObjSearch.ConsumerID.ToString();
                                    o.EmpiricaScore = Math.Round(float.Parse(dr["FinalScore"].ToString()), 0).ToString();
                                    o.ExclusionCode = dr["Exception_Code"].ToString();
                                    o.ExclusionCodeDescription = dr["ExceptionDescription"].ToString();
                                   
                                    int rccount = 0;

                                    if (string.IsNullOrEmpty(o.ExclusionCode))
                                    {
                                        o.ReasonDescription = new string[3];
                                        while (rccount < 3)
                                        {
                                            string columnname = "ReasonCode" + (rccount + 1).ToString();
                                            o.ReasonDescription[rccount] = dr[columnname].ToString();

                                            rccount++;
                                        }
                                    }

                                    //o.ReasonDescription = new XDSPortalLibrary.Entity_Layer.@string[3];

                                    //int rccount = 0;
                                    //if (string.IsNullOrEmpty(o.ExclusionCode))
                                    //{
                                    //    while (rccount < 3)
                                    //    {
                                    //        string columnname = "ReasonCode" + (rccount + 1).ToString();
                                    //        XDSPortalLibrary.Entity_Layer.@string ostring = new XDSPortalLibrary.Entity_Layer.@string();

                                    //        ostring.Value = dr[columnname].ToString();
                                    //        o.ReasonDescription[rccount] = ostring;

                                    //        rccount++;
                                    //    }
                                    //}
                                    

                                    oSResponse.SubmitResult.BureauResponse.EmpiricaEM04[i] = o;
                                    i++;
                                }
                            }


                            if (ds.Tables.Contains("IDVdetails"))
                            {
                                int count = ds.Tables["IDVdetails"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.IdvNI01 = new XDSPortalLibrary.Entity_Layer.IdvNI01[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["IDVdetails"].Rows)
                                {
                                    XDSPortalLibrary.Entity_Layer.IdvNI01 o = new XDSPortalLibrary.Entity_Layer.IdvNI01();
                                    o.IDDesc = string.Empty;
                                    o.IDVerifiedCode = dr["IDVerifiedCode"].ToString();
                                    o.IDVerifiedDesc = dr["IDVerifiedDesc"].ToString();
                                    o.IDWarning = string.Empty;
                                    o.VerifiedForename1 = dr["VerifiedForename1"].ToString();
                                    o.VerifiedSurname = dr["VerifiedSurname"].ToString();
                                    o.DeceasedDate = dr["DeceasedDate"].ToString();

                                    //if (ObjSearch.IDType.ToUpper() != XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString())
                                    //{
                                    //    o.IDVerifiedCode = string.Empty;
                                    //    o.IDVerifiedDesc = string.Empty;
                                    //}


                                    oSResponse.SubmitResult.BureauResponse.IdvNI01[i] = o;
                                    i++;
                                }
                            }
                        

                       
                            if (ds.Tables.Contains("ConsumerAccountStatus") && ds.Tables.Contains("Consumer24MonthlyPaymentHeader") && ds.Tables.Contains("Consumer24MonthlyPayment"))
                            {
                                int count = ds.Tables["ConsumerAccountStatus"].Rows.Count;
                                oSResponse.SubmitResult.BureauResponse.PaymentProfilesP701 = new XDSPortalLibrary.Entity_Layer.PaymentProfileP701[count];
                                int i = 0;
                                foreach (DataRow dr in ds.Tables["ConsumerAccountStatus"].Rows)
                                {

                                    XDSPortalLibrary.Entity_Layer.PaymentProfileP701 o = new XDSPortalLibrary.Entity_Layer.PaymentProfileP701();
                                    o.ConsumerNo = ObjSearch.ConsumerID.ToString();
                                    o.SupplierName = dr["SubscriberName"].ToString();
                                    o.Industry = dr["Industry"].ToString();
                                    o.AccountTypeCode = dr["AccountType"].ToString();
                                    o.LastUpdateDate = dr["LastUpdatedDate"].ToString();
                                    o.DateOpened = dr["AccountOpenedDate"].ToString();
                                    o.Instalment = Math.Round(float.Parse(dr["MonthlyInstalmentAmt"].ToString()), 2).ToString();
                                    o.OpeningBalance = Math.Round(float.Parse(dr["CreditLimitAmt"].ToString()), 2).ToString();
                                    o.CurrentBalance = Math.Round(float.Parse(dr["CurrentBalanceAmt"].ToString()), 2).ToString();
                                    o.OverdueAmount = dr["ArrearsAmt"].ToString();
                                    o.Terms = dr["InstallmentTerms"].ToString();
                                    o.AccountTypeDesc = dr["AccountTypeDesc"].ToString();
                                    o.AccSoldTo3rdParty = dr["AccountSoldToThirdPartyInd"].ToString();
                                    o.NumberOfParticipantsInJointLoan = dr["NoOfParticipantsInJointLoan"].ToString();
                                    o.OwnershipType = dr["OwnershipTypeCode"].ToString();
                                    o.PaymentType = dr["PaymentTypeCode"].ToString();
                                    o.RepaymentFrequency = dr["RepaymentFrequencyCode"].ToString();
                                    o.DeferredPaymentDate = dr["DeferredPaymentDate"].ToString();
                                    o.ThirdPartyName = dr["ThirdPartyName"].ToString();

                                    o.PaymentHistories = new XDSPortalLibrary.Entity_Layer.PaymentHistory[24];

                                    if (ds.Tables["Consumer24MonthlyPaymentHeader"].Rows.Count > 0 && ds.Tables["Consumer24MonthlyPayment"].Rows.Count > 0)
                                    {
                                        int colindex = 1;

                                        while (colindex <= 24)
                                        {
                                            string colname = colindex.ToString();

                                            colname = "M" + (colname.Length == 2 ? colname : "0" + colname);
                                            XDSPortalLibrary.Entity_Layer.PaymentHistory oHis = new XDSPortalLibrary.Entity_Layer.PaymentHistory();
                                            oHis.Date = ds.Tables["Consumer24MonthlyPaymentHeader"].Rows[0][colname].ToString();
                                            oHis.StatusCode = ds.Tables["Consumer24MonthlyPayment"].Rows[i][colname].ToString();

                                            o.PaymentHistories[colindex - 1] = oHis;

                                            colindex++;
                                        }

                                    }


                                    oSResponse.SubmitResult.BureauResponse.PaymentProfilesP701[i] = o;
                                    i++;
                                }
                            }
                        
                       

                        oSResponse.SubmitResult.BureauResponse.ConsEnqTransInfo0102 = new XDSPortalLibrary.Entity_Layer.ConsEnqTransInfo0102[1];
                        XDSPortalLibrary.Entity_Layer.ConsEnqTransInfo0102 oENq = new XDSPortalLibrary.Entity_Layer.ConsEnqTransInfo0102();
                        oENq.DefiniteMatchCount = "1";

                        oSResponse.SubmitResult.BureauResponse.ConsEnqTransInfo0102[0] = oENq;

                       

                    }

                    oSResponse.SubmitResult.ConsumerNumber = ObjSearch.ConsumerID.ToString();

                    XmlSerializer serializer1 = new XmlSerializer(oSResponse.GetType());
                    System.IO.StringWriter sw1 = new System.IO.StringWriter();
                    serializer1.Serialize(sw1, oSResponse);
                    System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());

                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = xml + reader1.ReadToEnd();
                    ObjResponse.ResponseKey = ObjSearch.ConsumerID;
                    ObjResponse.ResponseKeyType = "C";


                    dtError.Clear();
                    ds.Clear();
                    dtError.Dispose();
                    ds.Dispose();
                }
                catch (Exception e)
                {
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = e.Message;


                    oSResponse.SubmitResult.Status = XDSPortalLibrary.Entity_Layer.SubmitResult.enumstatus.FAILURE;
                    oSResponse.SubmitResult.ErrorDescription = "Error While Processing your request, Please Contact XDS";
                }


               
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;


                oSResponse.SubmitResult.Status = XDSPortalLibrary.Entity_Layer.SubmitResult.enumstatus.FAILURE;
                oSResponse.SubmitResult.ErrorDescription = "Error While Processing your request, Please Contact XDS";
                
               
            }


            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response CustomvettingQ(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse, out XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput, int intCustomVettingOinputID)
        {

            
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            oSResponse = oinputResponse;
            try
            {

                //   File.AppendAllText(@"C:\Log\Response.txt", "EnterCustomvettingO");
                DataSet dsDataSegment = new DataSet();
                dsDataSegment.ReadXml(new StringReader(ObjSearch.DataSegments));


                string strReportID = "";
                strReportID = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();


                ds = new DataSet("XDSConnectReponse");

                // Thread thConsumerDetails = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thdemographics = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thdisputeInfo = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAkaNames = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thpublicdomain = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thNLRAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerScoringSummary = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerAccountLoad = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thAffordabilityActual = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thConsumerPropertyInformation = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thMTNScore = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thLinkage = new Thread(new ParameterizedThreadStart(ProcessReport));
                Thread thEmpirica07 = new Thread(new ParameterizedThreadStart(ProcessReport));

                if (ObjSearch.ConsumerID > 0)
                {

                    thdemographics.Start(string.Format("execute [dbo].[spCLR_JD_Demographics] {0}", ObjSearch.ConsumerID.ToString()));
                    thdisputeInfo.Start(string.Format("execute [dbo].[spCLR_JD_DisputeInfo] {0}", ObjSearch.ConsumerID.ToString()));

                    thpublicdomain.Start(string.Format("execute [dbo].[spCLR_JD_PublicDomain] {0},{1}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString()));
                    thNLRAccountLoad.Start(string.Format("execute [dbo].[spCLR_NLRConsumerAccountLoad] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID, "0"));
                    thConsumerAccountLoad.Start(string.Format("execute [dbo].[spCLR_ConsumerAccountLoad] {0},'{1}',{2},{3} ", ObjSearch.ConsumerID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID, "0"));
                    thAffordabilityActual.Start(string.Format("execute [dbo].[spCLR_ConsumerAffordability_V3]  {0},{1},{2},{3},{4},{5},{6},'{7}','{8}','{9}',{10},{11},{12},{13}", ObjSearch.subscriberID.ToString(), ObjSearch.ProductID.ToString(), strReportID, ObjSearch.EnquiryID.ToString(), ObjSearch.EnquiryResultID.ToString(), ObjSearch.ConsumerID.ToString(), "0", ObjSearch.AssociationTypeCode, ObjSearch.SubscriberAssociationCode, ObjSearch.UserName, ObjSearch.GrossMonthlyIncome, ObjSearch.EmploymentDuration, 0, 0));
                    thConsumerPropertyInformation.Start(string.Format("execute [dbo].[spCLR_ConsumerPropertyInformation_Custom] {0} ", ObjSearch.ConsumerID.ToString()));
                    thConsumerScoringSummary.Start(string.Format("execute [dbo].[spCLR_ConsumerScoringSummary] {0} ,{1},'{2}',{3}", ObjSearch.ConsumerID.ToString(), ObjSearch.subscriberID.ToString(), ObjSearch.SubscriberAssociationCode.ToString(), strReportID.ToString()));
                    thLinkage.Start(string.Format("execute spCLR_MTN_demographicLinkages {0},'{1}','{2}'",ObjSearch.ConsumerID,objInput.input.CellNumber,objInput.input.AddressLine1));
                    thEmpirica07.Start(string.Format("execute spCLR_MTN_EmpiricaEM07New {0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}'", ObjSearch.ConsumerID.ToString(), ObjSearch.IDno.ToString(), ObjSearch.Passportno.ToString(), ObjSearch.Surname.ToString(), "", objInput.input.CellNumber, objInput.input.BankAccountNumber, objInput.input.AddressLine1 + " " + objInput.input.AddressLine2 + " " + objInput.input.Suburb + " " + objInput.input.City, ObjSearch.PostalAddressLine1.ToString() + " " + ObjSearch.PostalAddressLine2.ToString() + " " + ObjSearch.PostalProvinceCode.ToString() + " " + ObjSearch.PostalCity.ToString() + " " + ObjSearch.PostalSuburb.ToString() + " " + ObjSearch.PostalCode.ToString(), "", objInput.input.HomeTelephoneCode + objInput.input.HomeTelephoneNumber, objInput.input.WorkTelephoneCode + objInput.input.WorkTelephoneNumber, "", objInput.input.PostalCode,intCustomVettingOinputID.ToString()));
                    thConsumerScoringSummary.Join();

                    // thConsumerDetails.Join();
                    thdemographics.Join();
                    thdisputeInfo.Join();

                    thpublicdomain.Join();
                    thNLRAccountLoad.Join();
                    thConsumerAccountLoad.Join();
                    thAffordabilityActual.Join();
                    thConsumerPropertyInformation.Join();