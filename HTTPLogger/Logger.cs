﻿using System;
using System.IO;
using System.Text;
using System.Web;

namespace HTTPLogger.HttpModules
{
    public class Logger : IHttpModule
    {
        private static Object thisLock = new Object();
        string LogFile;
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
            context.EndRequest += new EventHandler(context_EndRequest);
        }

        private void context_BeginRequest(object sender, EventArgs e)
        {
            Stream rStr = null;
            try
            {
                LogFile = HttpContext.Current.Request.MapPath(@"~\") + @"\HTTPLog.txt";
                CheckLogFileSize(LogFile);

                if (sender != null && sender is HttpApplication)
                {
                    HttpRequest request = (sender as HttpApplication).Request;

                    //string FilterIP = ConfigurationManager.AppSettings.Get("CommaDelimitedHttpRequestLoggerFilterIPs");
                    //if (!String.IsNullOrEmpty(FilterIP))
                    //{
                    //    bool Match = false;
                    //    string[] FIPs = FilterIP.Split(new char[] { ',' });
                    //    foreach (string FIP in FIPs)
                    //        if (FIP == request.UserHostAddress)
                    //            Match = true;
                    //    if (!Match)
                    //        return;
                    //}

                    lock (thisLock)
                    { File.AppendAllText(LogFile, String.Format("{0} Request from IP Address: {1}\r\n", DateTime.Now, request.UserHostAddress)); }
                    lock (thisLock)
                { File.AppendAllText(LogFile, String.Format("Request URL: {0}\r\n", request.Url));}
                    lock (thisLock)
                    { File.AppendAllText(LogFile, String.Format("Request HTTP Method: {0}\r\n", request.HttpMethod)); }

                    foreach (string key in request.Headers.Keys)
                        lock (thisLock)
                        { File.AppendAllText(LogFile, String.Format("Header Name: {0}, Value: {1}\r\n", key, request.Headers[key])); }

                    rStr = request.InputStream;
                    int Len = Convert.ToInt32(rStr.Length);
                    byte[] rStrArray = new byte[Len];
                    rStr.Read(rStrArray, 0, Len);
                    string rStrString = HttpUtility.HtmlDecode(Encoding.UTF8.GetString(rStrArray));
                    lock (thisLock)
                    { File.AppendAllText(LogFile, String.Format("Request Stream:\r\n{0}\r\n\r\n", rStrString)); }

                    //do this to add request stream to IIS log. Warning! will make IIS log huge.
                    //HttpResponse response = (sender as HttpApplication).Response;
                    //response.AppendToLog(rStrString);

                    return;
                }
            }
            catch { }
            finally
            {
                if (rStr != null)
                {
                    try { rStr.Position = 0; } //Must reset stream to beginning before it continues to the webservice or will fail!
                    catch { }
                }
            }
        }

        private void context_EndRequest(object sender,EventArgs e)
        {
            Stream rStr = null;
            try
            {
                LogFile = HttpContext.Current.Request.MapPath(@"~\") + @"\HTTPResponseLog.txt";
                CheckLogFileSize(LogFile);

                if (sender != null && sender is HttpApplication)
                {
                    HttpResponse response = (sender as HttpApplication).Response;

                    //string FilterIP = ConfigurationManager.AppSettings.Get("CommaDelimitedHttpRequestLoggerFilterIPs");
                    //if (!String.IsNullOrEmpty(FilterIP))
                    //{
                    //    bool Match = false;
                    //    string[] FIPs = FilterIP.Split(new char[] { ',' });
                    //    foreach (string FIP in FIPs)
                    //        if (FIP == request.UserHostAddress)
                    //            Match = true;
                    //    if (!Match)
                    //        return;
                    //}

                    //lock (thisLock)
                    //{ File.AppendAllText(LogFile, String.Format("{0} Request from IP Address: {1}\r\n", DateTime.Now, response.UserHostAddress)); }
                    //lock (thisLock)
                    //{ File.AppendAllText(LogFile, String.Format("Request URL: {0}\r\n", response.Url)); }
                    //lock (thisLock)
                    //{ File.AppendAllText(LogFile, String.Format("Request HTTP Method: {0}\r\n", response.HttpMethod)); }

                    //foreach (string key in response.Headers.Keys)
                    //    lock (thisLock)
                    //    { File.AppendAllText(LogFile, String.Format("Header Name: {0}, Value: {1}\r\n", key, response.Headers[key])); }

                    rStr = response.OutputStream;
                    int Len = Convert.ToInt32(rStr.Length);
                    byte[] rStrArray = new byte[Len];
                    rStr.Read(rStrArray, 0, Len);
                    string rStrString = HttpUtility.HtmlDecode(Encoding.UTF8.GetString(rStrArray));
                    lock (thisLock)
                    { File.AppendAllText(LogFile, String.Format("Request Stream:\r\n{0}\r\n\r\n", rStrString)); }

                    //do this to add request stream to IIS log. Warning! will make IIS log huge.
                    //HttpResponse response = (sender as HttpApplication).Response;
                    //response.AppendToLog(rStrString);

                    return;
                }
            }
            catch { }
            finally
            {
                if (rStr != null)
                {
                    try { rStr.Position = 0; } //Must reset stream to beginning before it continues to the webservice or will fail!
                    catch { }
                }
            }
        }

        private void CheckLogFileSize(string LogFile)
        {
            if (File.Exists(LogFile))
            {
                if (new FileInfo(LogFile).Length > 1048576L)
                    File.Move(LogFile, String.Format(@"{0}\{1} {2}.{3}",
                        Path.GetDirectoryName(LogFile),
                        Path.GetFileNameWithoutExtension(LogFile),
                        DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss"),
                        Path.GetExtension(LogFile)
                        ));
            }
        }
    }
}